﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class CreateOffer
    Inherits BasePage

#Region "Props"

    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property


    Protected Property IsCounterOffer As Boolean
        Get
            If (ViewState("IsCounterOffer") IsNot Nothing) Then
                Return ViewState("IsCounterOffer")
            End If
            Return False
        End Get
        Set(value As Boolean)
            ViewState("IsCounterOffer") = value
        End Set
    End Property


    Protected Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property


    Protected Property OfferZoneSet As Boolean
        Get
            Return ViewState("OfferZoneSet")
        End Get
        Set(value As Boolean)
            ViewState("OfferZoneSet") = value
        End Set
    End Property


    Protected Property IsLikeOffer As Boolean
        Get
            If (ViewState("IsLikeOffer") IsNot Nothing) Then
                Return ViewState("IsLikeOffer")
            End If
            Return False
        End Get
        Set(value As Boolean)
            ViewState("IsLikeOffer") = value
        End Set
    End Property


    Protected Property IsPokeOffer As Boolean
        Get
            If (ViewState("IsPokeOffer") IsNot Nothing) Then
                Return ViewState("IsPokeOffer")
            End If
            Return False
        End Get
        Set(value As Boolean)
            ViewState("IsPokeOffer") = value
        End Set
    End Property


    Protected Property memberReceivingOffer As EUS_Profile


#End Region

    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
                'If (Request("master") = "inner") Then
                '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
                'Else
                'End If
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblActionError.Text = ""

        If (Not Me.IsPostBack) Then
            LoadLAG()
            Me._LoadData()

            If (Request.UrlReferrer IsNot Nothing) Then
                lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
            ElseIf (Not String.IsNullOrEmpty(Request.QueryString("p"))) Then
                lnkBack.NavigateUrl = "~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Request.QueryString("p"))
            ElseIf (Not String.IsNullOrEmpty(Request.QueryString("offerto"))) Then
                lnkBack.NavigateUrl = "~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Request.QueryString("offerto"))
            Else
                lnkBack.NavigateUrl = "~/Members/"
            End If

        End If

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        Me._LoadData()
        UpdateUserControls(Me)
    End Sub


    Protected Sub LoadLAG()
        Try
            lnkBack.Text = CurrentPageData.GetCustomString(lnkBack.ID)

            If (Me.IsFemale) Then
                lblFirstDateAmountTitle.Text = CurrentPageData.GetCustomString("FEMALE_lblFirstDateAmountTitle")
                lblFirstDateEnterAmount.Text = CurrentPageData.GetCustomString("FEMALE_lblFirstDateEnterAmount")

                lblFirstDateAmountNote.Text = CurrentPageData.GetCustomString("FEMALE_lblFirstDateAmountNote")
                'divFirstDateWarning.Visible = True
                lblFirstDateInfoText.Text = CurrentPageData.GetCustomString("FEMALE_lblFirstDateInfoText")
            Else
                lblFirstDateAmountTitle.Text = CurrentPageData.GetCustomString("MALE_lblFirstDateAmountTitle")
                lblFirstDateEnterAmount.Text = CurrentPageData.GetCustomString("MALE_lblFirstDateEnterAmount")

                lblFirstDateAmountNote.Text = CurrentPageData.GetCustomString("MALE_lblFirstDateAmountNote")
                'divFirstDateWarning.Visible = False
                lblFirstDateInfoText.Text = CurrentPageData.GetCustomString("MALE_lblFirstDateInfoText")
            End If


            'lblYourMsgFrom.Text = CurrentPageData.GetCustomString(lblYourMsgFrom.ID)
            'lblYourMsgTo.Text = CurrentPageData.GetCustomString(lblYourMsgTo.ID)
            btnCreateOffer.Text = CurrentPageData.GetCustomString(btnCreateOffer.ID)
            'lnkSearchMembers.Text = CurrentPageData.GetCustomString("lnkSearchMembers")
            msg_SearchOurMembersText.Text = CurrentPageData.GetCustomString("msg_SearchOurMembersText")


            'SetControlsValue(Me, CurrentPageData)

            'lblItemsName.Text = Me.CurrentPageData.GetCustomString("CartCreateOfferView")
            lnkViewDescription.Text = Me.CurrentPageData.GetCustomString("CartCreateOfferWhatIsIt")
            lnkViewDescription.OnClientClick = OnMoreInfoClickFunc(
                lnkViewDescription.OnClientClick,
                ResolveUrl("~/Members/InfoWin.aspx?info=createoffer"),
                Me.CurrentPageData.GetCustomString("CartCreateOfferView"))
            WhatIsIt1.PopupControl.HeaderText = Me.CurrentPageData.GetCustomString("CartCreateOfferView")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Private Sub _LoadData()
        Dim offerid As Integer

        Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
        If (Not currentUserHasPhotos) Then
            mvMainCreateOffer.SetActiveView(vwNoPhoto)
            Return
        End If


        ' check for offerto QueryString param (user loginname)
        Try

            If (Not String.IsNullOrEmpty(Request.QueryString("offerto"))) Then
                memberReceivingOffer = DataHelpers.GetEUS_ProfileMasterByLoginName(Me.CMSDBDataContext, Request.QueryString("offerto"), ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                If (memberReceivingOffer IsNot Nothing) Then
                    If (memberReceivingOffer.ProfileID <> Me.MasterProfileId) Then

                        Me.UserIdInView = memberReceivingOffer.ProfileID
                        If (Me.UserIdInView > 0) Then SetOfferZoneOnUI()

                    End If
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "_LoadData")
        End Try


        ' check for offer QueryString param (offerid)
        Try
            If Not String.IsNullOrEmpty(Request("offer")) AndAlso Integer.TryParse(Request("offer"), offerid) Then
                ' current offer is reply to a wink
                If (offerid > 0) Then
                    Dim winkOffer As EUS_Offer = (From itm In Me.CMSDBDataContext.EUS_Offers
                                                     Where itm.OfferID = offerid AndAlso (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK OrElse itm.OfferTypeID = ProfileHelper.OfferTypeID_POKE) AndAlso itm.StatusID = ProfileHelper.OfferStatusID_PENDING
                                                    Select itm).SingleOrDefault()
                    If (winkOffer IsNot Nothing) Then
                        If (winkOffer.OfferTypeID = ProfileHelper.OfferTypeID_WINK) Then
                            Me.IsLikeOffer = True
                        ElseIf (winkOffer.OfferTypeID = ProfileHelper.OfferTypeID_POKE) Then
                            Me.IsPokeOffer = True
                        End If

                        If (winkOffer.FromProfileID <> Me.MasterProfileId) Then
                            Me.UserIdInView = winkOffer.FromProfileID
                        Else
                            Me.UserIdInView = winkOffer.ToProfileID
                        End If
                    End If

                    If (Me.UserIdInView > 0) Then SetOfferZoneOnUI()

                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "_LoadData")
        End Try


        ' check for counter QueryString param (offerid)
        Try
            If Not String.IsNullOrEmpty(Request("counter")) AndAlso Integer.TryParse(Request("counter"), offerid) Then
                ' current offer is a counter
                If (offerid > 0) Then
                    Dim newOffer As EUS_Offer = (From itm In Me.CMSDBDataContext.EUS_Offers
                                                     Where itm.OfferID = offerid AndAlso _
                                                     ((itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW) AndAlso (itm.StatusID = ProfileHelper.OfferStatusID_PENDING) OrElse _
                                                     (itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER) AndAlso (itm.StatusID = ProfileHelper.OfferStatusID_COUNTER))
                                                    Select itm).SingleOrDefault()
                    If (newOffer IsNot Nothing) Then
                        Me.IsCounterOffer = True
                        If (newOffer.FromProfileID <> Me.MasterProfileId) Then
                            Me.UserIdInView = newOffer.FromProfileID
                        Else
                            Me.UserIdInView = newOffer.ToProfileID
                        End If
                    End If

                    If (Me.UserIdInView > 0) Then SetOfferZoneOnUI()

                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "_LoadData")
        End Try


        Try
            Dim mayContinue As Boolean = False
            If (Me.UserIdInView > 0) Then
                ' check if there is a pending offer
                Dim pendingOffer As EUS_Offer = (From itm In Me.CMSDBDataContext.EUS_Offers
                                                 Where
                                                    (itm.FromProfileID = Me.MasterProfileId AndAlso itm.ToProfileID = Me.UserIdInView) AndAlso
                                                    (itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW) AndAlso
                                                    (itm.StatusID = ProfileHelper.OfferStatusID_PENDING)
                                                Select itm).SingleOrDefault()

                If (pendingOffer IsNot Nothing) Then
                    ' may happen, when browser's back button  was pressed
                    lblActionError.Text = Me.CurrentPageData.GetCustomString("ErrorPendingOfferAlreadyExists")
                    lnkSearchMembers.Visible = True
                    mvMainCreateOffer.SetActiveView(vwEmpty)

                Else
                    mayContinue = True
                End If

            Else
                If Not String.IsNullOrEmpty(Request("counter")) AndAlso Integer.TryParse(Request("counter"), offerid) Then
                    ' for some reason user wants to rerun offer
                    'TODO: change text
                    lblActionError.Text = Me.CurrentPageData.GetCustomString("ErrorMemberNotFoundOfferProhibited")
                    lnkSearchMembers.Visible = True
                Else
                    ' mainly hacked case 
                    lblActionError.Text = Me.CurrentPageData.GetCustomString("ErrorMemberNotFoundOfferProhibited")
                    lnkSearchMembers.Visible = True
                End If

            End If


            If (mayContinue) Then

                mvMainCreateOffer.SetActiveView(vwCreateOffer)
                btnCreateOffer.CommandArgument = Me.UserIdInView

                Dim defaultPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.MasterProfileId)
                Dim memberReceivingOfferPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(Me.UserIdInView)

                If (memberReceivingOffer Is Nothing) Then
                    memberReceivingOffer = (From itm In Me.CMSDBDataContext.EUS_Profiles
                                             Where itm.ProfileID = Me.UserIdInView
                                            Select itm).SingleOrDefault()
                End If

                Dim result As clsUserDoes.clsLoadProfilesViewsResult =
                    clsUserDoes.GetProfilesThumbViews(Me.GetCurrentProfile(), defaultPhoto, memberReceivingOffer, memberReceivingOfferPhoto, Me.IsHTTPS, PhotoSize.D150)

                lnkProfImg.NavigateUrl = result.CurrentMemberProfileViewUrl
                imgProf.ImageUrl = result.CurrentMemberImageUrl
                'lnkProfLogin.NavigateUrl = result.CurrentMemberProfileViewUrl
                'lnkProfLogin.Text = result.CurrentMemberLoginName

                lnkOtherProfImg.NavigateUrl = result.OtherMemberProfileViewUrl
                imgOtherProf.ImageUrl = result.OtherMemberImageUrl
                'lnkOtherProfLogin.NavigateUrl = result.OtherMemberProfileViewUrl
                'lnkOtherProfLogin.Text = result.OtherMemberLoginName



                lblFirstDateEnterAmount.Text = ReplaceCommonTookens(lblFirstDateEnterAmount.Text, "<span class=""login-name"">" & result.OtherMemberLoginName & "</span>")

                If (Me.IsCounterOffer) Then
                    If (ProfileHelper.IsFemale(memberReceivingOffer.GenderId)) Then
                        lblCreateOfferTitle.Text = CurrentPageData.GetCustomString("lblCreateOfferTitle_Counter_SendToFEMALE")
                    Else
                        lblCreateOfferTitle.Text = CurrentPageData.GetCustomString("lblCreateOfferTitle_Counter_SendToMALE")
                    End If
                Else
                    If (ProfileHelper.IsFemale(memberReceivingOffer.GenderId)) Then
                        lblCreateOfferTitle.Text = CurrentPageData.GetCustomString("lblCreateOfferTitle_SendToFEMALE")
                    Else
                        lblCreateOfferTitle.Text = CurrentPageData.GetCustomString("lblCreateOfferTitle_SendToMALE")
                    End If
                End If
                lblCreateOfferTitle.Text = ReplaceCommonTookens(lblCreateOfferTitle.Text, "<span class=""login-name"">" & result.OtherMemberLoginName & "</span>")


                Dim lastOffer As EUS_Offer = clsUserDoes.GetLastOfferWithAmount(Me.UserIdInView, Me.MasterProfileId)
                If (lastOffer IsNot Nothing) Then
                    divFirstDateWarning.Visible = False

                    If (lastOffer IsNot Nothing AndAlso lastOffer.Amount > 0) Then
                        lblLastOfferAmount.Text = CurrentPageData.GetCustomString("lblLastOfferAmount")
                        lblLastOfferAmount.Text = lblLastOfferAmount.Text.Replace("[LAST_OFFER_AMOUNT]", "&euro;" & lastOffer.Amount)
                    End If

                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "_LoadData")
        End Try

    End Sub


    Protected Sub btnCreateOffer_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles btnCreateOffer.Command
        Dim offerid As Integer
        Dim amount As Integer
        Dim parms As clsUserDoes.NewOfferParameters = Nothing

        pnlAmountErr.Visible = False
        Dim MaySendOffer As Boolean = True
        Dim _userIdReceiver As Integer

        Try
            _userIdReceiver = CType(e.CommandArgument, Integer)

            'If (Me.IsFemale) Then
            '    If (Me.IsReferrer) Then
            '        MaySendOffer = clsUserDoes.IsReferrerAllowedSendWink(_userIdReceiver, Me.MasterProfileId)
            '    End If
            'End If

            'If (Not MaySendOffer) Then
            '    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo"), "Error! Can't send", 650, 350)
            '    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
            'End If

            Dim MaySendOffer_CheckSetting As Boolean = True

            MaySendOffer_CheckSetting = Not clsProfilesPrivacySettings.GET_DisableReceivingOFFERSFromDifferentCountryFromTo(_userIdReceiver, Me.MasterProfileId)
            If (MaySendOffer_CheckSetting = False) Then
                MaySendOffer = False
            End If


            If (MaySendOffer AndAlso Me.IsFemale) Then
                If (Me.IsReferrer) Then
                    MaySendOffer = clsUserDoes.IsReferrerAllowedSendWink(_userIdReceiver, Me.MasterProfileId)
                End If
            End If

            If (Not MaySendOffer) Then
                If (MaySendOffer_CheckSetting = False) Then
                    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_Disabled_Offers_From_Diff_Country&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                Else
                    Dim popupMessage As String = WhatIsIt.OnMoreInfoClickFunc(ResolveUrl("~/Members/InfoWin.aspx?info=Error_Sending_Receiver_DoNot_Have_Photo&popup=popupWhatIs"), "Error! Can't send", 650, 350)
                    popupMessage = "function fnError_Sending_Receiver_DoNot_Have_Photo(){" & popupMessage & "};"
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Error_Sending_Receiver_DoNot_Have_Photo", popupMessage, True)
                End If
            End If


            If (MaySendOffer) Then

                ' check if there is a pending offer
                Dim pendingOffer As EUS_Offer = (From itm In Me.CMSDBDataContext.EUS_Offers
                                                 Where
                                                    (itm.FromProfileID = Me.MasterProfileId AndAlso itm.ToProfileID = Me.UserIdInView) AndAlso
                                                    (itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW) AndAlso
                                                    (itm.StatusID = ProfileHelper.OfferStatusID_PENDING)
                                                Select itm).SingleOrDefault()

                If (pendingOffer IsNot Nothing) Then
                    ' may happen, when browser's back button  was pressed
                    lblActionError.Text = Me.CurrentPageData.GetCustomString("ErrorPendingOfferAlreadyExists")
                    lnkSearchMembers.Visible = True
                    mvMainCreateOffer.SetActiveView(vwEmpty)

                    MaySendOffer = False
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        If (Not MaySendOffer) Then Return


        Try

            Integer.TryParse(txtAmount.Text, amount)

            Dim messageText1 As String = ""
            Dim messageText2 As String = ""
            Dim messageTruncated As String = vbCrLf & "User's message was truncated."

            'If (txtMessage.Text.Length > 295) Then
            '    messageText1 = txtMessage.Text.Substring(0, 295)

            '    If (txtMessage.Text.Length > 295 * 2) Then
            '        messageText2 = txtMessage.Text.Substring(295, ((295 * 2) - messageTruncated.Length)) & messageTruncated
            '    Else
            '        messageText2 = txtMessage.Text.Substring(295)
            '    End If

            'Else
            '    messageText1 = txtMessage.Text
            'End If



            If Not String.IsNullOrEmpty(Request("offer")) Then
                Integer.TryParse(Request("offer"), offerid)
            End If

            If Not String.IsNullOrEmpty(Request("counter")) Then
                Integer.TryParse(Request("counter"), offerid)
                Me.IsCounterOffer = True
            End If

            Dim offerZoneValid As Boolean
            Dim minOfferAmount As Integer, maxOfferAmount As Integer
            'If (Me.OfferZoneSet = True AndAlso amount > 0) Then
            offerZoneValid = ValidateOfferZoneOn(amount, minOfferAmount, maxOfferAmount)
            'ElseIf (Not Me.OfferZoneSet) Then
            '    offerZoneValid = True
            'End If

            If (amount > 0 AndAlso _userIdReceiver > 0 AndAlso offerZoneValid) Then
                parms = New clsUserDoes.NewOfferParameters()
                parms.messageText1 = messageText1
                parms.messageText2 = messageText2
                parms.offerAmount = amount
                parms.parentOfferId = offerid
                parms.userIdReceiver = _userIdReceiver
                parms.userIdWhoDid = Me.MasterProfileId

            End If

            If (Me.IsCounterOffer AndAlso offerZoneValid) Then
                clsUserDoes.CounterOffer(offerid, amount)
                Response.Redirect(ResolveUrl("~/Members/Offers3.aspx"), False)
                HttpContext.Current.ApplicationInstance.CompleteRequest()

            ElseIf (parms IsNot Nothing) Then
                If (Me.IsLikeOffer) Then
                    parms.childOfferId = clsUserDoes.MakeOffer(parms)
                    clsUserDoes.AcceptLike(parms, OfferStatusEnum.LIKE_ACCEEPTED_WITH_OFFER)

                    If (Me.IsMale) Then
                        Response.Redirect(ResolveUrl("~/Members/Offers3.aspx?vw=PENDING"), False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Else
                        Response.Redirect(ResolveUrl("~/Members/Offers3.aspx?vw=PENDING"), False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If

                ElseIf (Me.IsPokeOffer) Then
                    parms.childOfferId = clsUserDoes.MakeOffer(parms)
                    clsUserDoes.AcceptPoke(parms, OfferStatusEnum.POKE_ACCEEPTED_WITH_OFFER)

                    If (Me.IsMale) Then
                        Response.Redirect(ResolveUrl("~/Members/Dates.aspx"), False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Else
                        Response.Redirect(ResolveUrl("~/Members/Offers3.aspx?vw=PENDING"), False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If

                Else
                    parms.childOfferId = clsUserDoes.MakeOffer(parms)

                    If (Me.IsMale) Then
                        Response.Redirect(ResolveUrl("~/Members/Offers3.aspx?vw=PENDING"), False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    Else
                        Response.Redirect(ResolveUrl("~/Members/Offers3.aspx?vw=PENDING"), False)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If

                End If

            End If

            If (amount = 0) Then
                'mvMainCreateOffer.SetActiveView(vwEmpty)
                pnlAmountErr.Visible = True
                lblAmountErr.Text = Me.CurrentPageData.GetCustomString("ErrorOfferAmountIsInvalid")
            ElseIf (amount < 0) Then
                'mvMainCreateOffer.SetActiveView(vwEmpty)
                pnlAmountErr.Visible = True
                lblAmountErr.Text = Me.CurrentPageData.GetCustomString("ErrorOfferAmountIsNegative")
            End If

            If (Not offerZoneValid) Then
                pnlAmountErr.Visible = True
                lblAmountErr.Text = Me.CurrentPageData.GetCustomString("ErrorOfferAmountInvalidForCurrentZone")

                lblAmountErr.Text = lblAmountErr.Text.Replace("###MINOFFERAMOUNT###", minOfferAmount & "&euro;")
                lblAmountErr.Text = lblAmountErr.Text.Replace("###MAXOFFERAMOUNT###", maxOfferAmount & "&euro;")
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub SetOfferZoneOnUI()
        Try

            Dim distance As Single = 0
            Dim country As String = ""

            Dim moneyRange As String() = Me.GetOfferZoneSettings(distance, country)
            Dim minOfferAmount As Integer = moneyRange(0), maxOfferAmount As Integer = moneyRange(1)

            'If (distance <= 100) Then
            '    lblZoneInfo.Text = "ZoneA info " & " from " & moneyRange(0) & "&euro; to " & moneyRange(1) & "&euro;"

            'ElseIf (distance <= 600) Then
            '    lblZoneInfo.Text = "ZoneB info " & " from " & moneyRange(0) & "&euro; to " & moneyRange(1) & "&euro;"

            'Else
            '    lblZoneInfo.Text = "ZoneC info " & " from " & moneyRange(0) & "&euro; to " & moneyRange(1) & "&euro;"

            'End If
            lblZoneInfo.Text = Me.CurrentPageData.GetCustomString("OfferAmountsForCurrentZone")

            lblZoneInfo.Text = lblZoneInfo.Text.Replace("###MINOFFERAMOUNT###", minOfferAmount & "&euro;")
            lblZoneInfo.Text = lblZoneInfo.Text.Replace("###MAXOFFERAMOUNT###", maxOfferAmount & "&euro;")

            Me.OfferZoneSet = True

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Function ValidateOfferZoneOn(offerMoney As Integer, ByRef minOfferAmount As Integer, ByRef maxOfferAmount As Integer) As Boolean
        Dim success As Boolean
        Try

            Dim distance As Single = 0
            Dim country As String = ""

            Dim moneyRange As String() = Me.GetOfferZoneSettings(distance, country)
            minOfferAmount = moneyRange(0)
            maxOfferAmount = moneyRange(1)

            If (offerMoney >= minOfferAmount AndAlso offerMoney <= maxOfferAmount) Then
                success = True
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return success
    End Function




    Private Function GetOfferZoneSettings(ByRef distance As Single, ByRef country As String) As String()
        Dim moneyRange As String() = New String() {}
        Try
            distance = clsSearchHelper.GetMembersDistance(Me.MasterProfileId, Me.UserIdInView)
            country = ""

            If (ProfileHelper.IsMale(Me.SessionVariables.MemberData.GenderId)) Then
                country = Me.SessionVariables.MemberData.Country
            Else
                Dim memberReceivingOffer = (From itm In Me.CMSDBDataContext.EUS_Profiles
                                         Where itm.ProfileID = Me.UserIdInView
                                        Select New With {.GenderId = itm.GenderId, .Country = itm.Country}).SingleOrDefault()
                If (ProfileHelper.IsMale(memberReceivingOffer.GenderId)) Then
                    country = memberReceivingOffer.Country
                End If
            End If

            Dim __country = country

            ' BAD FIX
            If (__country = "-1") Then __country = "GR"

            '  Dim countryInfo As DSGEO.SYS_CountriesGEORow = Lists.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO.SingleOrDefault(Function(itm As DSGEO.SYS_CountriesGEORow) itm.Iso = __country.ToUpper())
            Dim countryInfo As SYS_CountriesGEO = Nothing
            Try
                countryInfo = (From itm In Me.CMSDBDataContext.SYS_CountriesGEOs
                                Where itm.Iso = __country.ToUpper()
                                Select itm).FirstOrDefault()
            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try

            Dim config As New clsConfigValues()
            If (distance <= 100) Then

                If (countryInfo IsNot Nothing AndAlso Not String.IsNullOrEmpty(countryInfo.ZoneA)) Then
                    moneyRange = countryInfo.ZoneA.Split("-"c)
                Else
                    moneyRange = config.zone_a.Split("-"c)
                End If

            ElseIf (distance <= 600) Then

                If (countryInfo IsNot Nothing AndAlso Not String.IsNullOrEmpty(countryInfo.ZoneB)) Then
                    moneyRange = countryInfo.ZoneB.Split("-"c)
                Else
                    moneyRange = config.zone_b.Split("-"c)
                End If

            Else

                If (countryInfo IsNot Nothing AndAlso Not String.IsNullOrEmpty(countryInfo.ZoneC)) Then
                    moneyRange = countryInfo.ZoneC.Split("-"c)
                Else
                    moneyRange = config.zone_c.Split("-"c)
                End If

            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return moneyRange
    End Function

    Private Sub CreateOffer_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If (lblActionError.Text.Length > 0) Then
            mvMainCreateOffer.SetActiveView(vwEmpty)
        End If
    End Sub


End Class
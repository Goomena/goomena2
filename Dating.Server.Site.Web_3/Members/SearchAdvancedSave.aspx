﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"
    MasterPageFile="~/Members/Members2Inner.Master"
    CodeBehind="SearchAdvancedSave.aspx.vb" Inherits="Dating.Server.Site.Web.SearchAdvancedSave" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<link href="/v1/css/old-styles.css" type="text/css" rel="Stylesheet" />--%>

    <style type="text/css">
        body { background-image: none; }
        .dontask-wrap { position:relative;margin-top:5px; }
        .dontask-check { position:absolute; }
    </style>
    <script type="text/javascript">

        function notifyParentSuccess() {
            top.isOkSavedSearchName=true;
            top.SetNewSearchName(txtSearchName.GetText());
            closePopup('<%= Request.QueryString("popup") %>');
        }

        function notifyParentNoSave() {
            top.isOkSavedSearchName = true;
            top.SetNewSearchName('#DO_NOT_SAVE#');
            closePopup('<%= Request.QueryString("popup") %>')
        }
        function txtSearchName_GotFocus(s, e) { 
            s.SelectAll();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="search-advanced-save">
        <br />
        <asp:Label ID="lblTitle" runat="server" Text="" Visible="false"><h2>Saved search name:</h2></asp:Label>
        <asp:Panel ID="pnlPasswordVerifyFailed" runat="server" CssClass="alert alert-danger"
            Visible="False" ViewStateMode="Disabled">
            <asp:Label ID="lblPasswordVerifyFailed" runat="server" Text="" />
        </asp:Panel>

        <asp:Panel ID="pnlSubmitAction" runat="server" DefaultButton="btnSave">
            <table class="center-table">
                <tr>
                    <td class="lbl-right">
                        <asp:Label ID="lblSearchName" runat="server" Text="">Search name:</asp:Label></td>
                    <td>&nbsp;</td>
                    <td>
                        <dx:ASPxTextBox ID="txtSearchName" runat="server" Width="230px"
                            Font-Size="14px" Height="18px" ClientInstanceName="txtSearchName">
                            <ClientSideEvents GotFocus="txtSearchName_GotFocus" />
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" ErrorText="" ErrorTextPosition="Left">
                                <RequiredField ErrorText="" IsRequired="True" />
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </td>
                </tr>
            </table>

            <div class="form-actions">
                <table class="center-table">
                    <tr>
                        <td valign="top"><dx:ASPxButton ID="btnSave" runat="server" CausesValidation="True"
                                CssClass="btn lnk391-blue" Text="Save"
                                Native="True" EncodeHtml="False">
                                            <ClientSideEvents Click="notifyParentSuccess" />
                                        </dx:ASPxButton>
                        </td>
                        <td>&nbsp;</td>
                        <td valign="top"><dx:ASPxButton ID="btnDontSave" runat="server" CausesValidation="false"
                                CssClass="btn lnk391-blue" Text="Just show me results!"
                                Native="True" EncodeHtml="False" />
                            <div class="dontask-wrap">
                                <div class="dontask-check">
                                    <dx:ASPxCheckBox ID="chkDontAsk" runat="server" EncodeHtml="False"
                                        Layout="Flow" Text="Don't ask me until next login." />
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>

    <script type="text/javascript">
        (function () {
            var itms = $("input,a", "#search-advanced-save");
            for (var c = 0; c < itms.length; c++) {
                if ($(itms[c]).is(".persist_css")) continue;
                if ($(itms[c]).is(".lnk172-blue") || $(itms[c]).is(".lnk391-blue")) {
                    $(itms[c]).css("width", "auto");
                    var w = $(itms[c]).width();
                    $(itms[c]).css("width", "").removeClass("lnk172-blue").removeClass("lnk391-blue")
                    if (w < 160)
                        $(itms[c]).addClass("lnk172-blue");
                    else
                        $(itms[c]).addClass("lnk391-blue");
                }
            }
        })();
    </script>

</asp:Content>

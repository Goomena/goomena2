﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2.master" CodeBehind="CouponNew.aspx.vb" Inherits="Dating.Server.Site.Web.CouponNew" %>

<%@ Register Src="~/UserControls/gifts/ucCouponCreate.ascx" TagName="CouponCreate" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/gifts/ucCouponPreview.ascx" TagName="CouponPreview" TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/gifts/ucCredits.ascx" TagPrefix="uc2" TagName="ucCredits" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--<script src="../Scripts/bootstrap-dropdown.js" type="text/javascript"></script>--%>
    <link href="../v1/CSS/GiftsStyle.css?v7" rel="stylesheet" />

    <script type="text/javascript">
        var lnkGetCreditsID2 = null;
        var lnkGetCreditsText2 = null;

        function setCreditsValue(val) {
            if (!stringIsEmpty(lnkGetCreditsText))
                $('#' + lnkGetCreditsID).text(lnkGetCreditsText.replace('[CREDITS]', val));
        }
        function spinCredits_Init(s, e) {
            setCreditsValue(s.GetValue());
        }
        function spinCredits_ValueChanged(s, e) {
            setCreditsValue(s.GetValue());
        }
        function getCreditsText() {
            if (stringIsEmpty(lnkGetCreditsText))
                lnkGetCreditsText = $('#' + lnkGetCreditsID).text();
            setCreditsValue(0);
        }
        function setCreditsValue2(val) {
            if (!stringIsEmpty(lnkGetCreditsID2))
                $('#' + lnkGetCreditsID2).text(Math.floor(val) + '$');
        }
        function spinCredits_Init2(s, e) {
            setCreditsValue2(s.GetValue());
        }
        function spinCredits_ValueChanged2(s, e) {
            setCreditsValue2(s.GetValue());
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            
            <uc3:CouponPreview ID="CouponPreview" runat="server" Visible="false" />
            <uc2:CouponCreate ID="CouponCreate" runat="server" Visible="false" />
            <uc2:ucCredits runat="server" ID="ucCredits" ShowCreditsHeader="true" />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>



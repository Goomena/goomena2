﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers
Imports Dating.Server.Site.Web.TWLib

Public Class DatesHistory
    Inherits BasePage


#Region "Props"

    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property


    Protected Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property


#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            'If clsCurrentContext.VerifyLogin() = True Then
            '    Me.MasterPageFile = "~/Members/Members2.Master"
            '    'If (Request("master") = "inner") Then
            '    '    Me.MasterPageFile = "~/Members/Members2Inner.Master"
            '    'Else
            '    'End If
            'End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            If (Not Page.IsPostBack) Then

                LoadLAG()
                LoadViews()

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Private Sub LoadViews()

        Try

            Dim otherLoginName = Request.QueryString("vw")
            If (Not String.IsNullOrEmpty(otherLoginName)) Then

                Dim profileInView As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(Me.CMSDBDataContext, otherLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
                If (profileInView IsNot Nothing) Then
                    Me.UserIdInView = profileInView.ProfileID
                    gvHistory.DataBind()
                    clsUserDoes.MarkDateOfferAsViewed(Me.UserIdInView, Me.MasterProfileId)
                End If
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        LoadViews()
    End Sub



    Protected Sub LoadLAG()
        Try

            SetControlsValue(Me, CurrentPageData)


            gvHistory.Columns("Date").Caption = CurrentPageData.GetCustomString("CommunicationHistoryTableCaption_Date")
            gvHistory.Columns("Action").Caption = CurrentPageData.GetCustomString("CommunicationHistoryTableCaption_Action")
            gvHistory.Columns("Description").Caption = CurrentPageData.GetCustomString("CommunicationHistoryTableCaption_Description")
            gvHistory.Columns("Direction").Caption = CurrentPageData.GetCustomString("CommunicationHistoryTableCaption_Direction")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub gvHistory_CustomColumnDisplayText(ByVal sender As Object, ByVal e As DevExpress.Web.ASPxGridView.ASPxGridViewColumnDisplayTextEventArgs) Handles gvHistory.CustomColumnDisplayText

        Try
            If e.Column.FieldName = "Action" Then

                'Dim dr As System.Data.DataRow = gvHistory.GetDataRow(e.VisibleRowIndex)
                'If (dr("CustomerTransactionID") > 0) Then
                '    If (dr("Expired") = True) Then
                '        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Expired")
                '    Else
                '        e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Credited")
                '    End If

                'ElseIf (dr("CreditsType") = "UNLOCK_CONVERSATION") Then
                '    e.DisplayText = Me.CurrentPageData.GetCustomString("CreditsAction_Used")
                'End If

            ElseIf e.Column.FieldName = "DateTimeToCreate" Then
                Dim dr As System.Data.DataRow = gvHistory.GetDataRow(e.VisibleRowIndex)
                If (Not dr.IsNull("DateTimeToCreate")) Then
                    'e.DisplayText = AppUtils.GetDateTimeString(dr("DateTimeToCreate"), "", "dd/MM/yyyy H:mm")
                    e.DisplayText = AppUtils.GetMessageDateDescription(dr("DateTimeToCreate"), Me.globalStrings)
                End If

            ElseIf e.Column.FieldName = "Description" Then
                Dim dr As System.Data.DataRow = gvHistory.GetDataRow(e.VisibleRowIndex)

                If (Not dr.IsNull("TypeTitle")) Then
                    Dim TypeTitle As String = dr("TypeTitle")
                    Select Case TypeTitle
                        Case "Message"
                            If (dr("StatusID") = 0 AndAlso Me.IsMale) Then
                                e.DisplayText = Me.CurrentPageData.GetCustomString("Message.Locked.Text")
                            Else
                                Dim Subject As String = dr("Subject")
                                If (Len(Subject) > 5) Then
                                    e.DisplayText = Subject
                                Else
                                    Dim Body As String = dr("Body")
                                    If (Len(Body) > 0) Then
                                        e.DisplayText = Body
                                    End If
                                End If
                            End If

                        Case "Make favorite"
                            If (dr("Direction") = "Sent") Then
                                e.DisplayText = Me.CurrentPageData.GetCustomString("Sent.Favorite")
                            Else
                                e.DisplayText = Me.CurrentPageData.GetCustomString("Received.Favorite")
                            End If

                        Case "Like"
                            If (dr("Direction") = "Sent") Then
                                e.DisplayText = Me.CurrentPageData.GetCustomString("Sent.Like")
                            Else
                                e.DisplayText = Me.CurrentPageData.GetCustomString("Received.Like")
                            End If

                        Case "Offer New", "Offer Counter"
                            Dim Amount As Double
                            If (Not dr.IsNull("Amount")) Then Amount = Convert.ToDouble(dr("Amount"))

                            If (dr("Direction") = "Sent") Then
                                e.DisplayText = Me.CurrentPageData.GetCustomString("Sent.Offer").Replace("[AMOUNT]", Amount)
                            Else
                                e.DisplayText = Me.CurrentPageData.GetCustomString("Received.Offer").Replace("[AMOUNT]", Amount)
                            End If
                        Case "Poke"
                            If (dr("Direction") = "Sent") Then
                                e.DisplayText = Me.CurrentPageData.GetCustomString("Sent.Poke")
                            Else
                                e.DisplayText = Me.CurrentPageData.GetCustomString("Received.Poke")
                            End If

                    End Select

                End If

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub

    Protected Sub sdsCommunicationHistory_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsCommunicationHistory.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters
            If (prm.ParameterName = "@MasterProfileId") Then
                prm.Value = Me.MasterProfileId
            End If
            If (prm.ParameterName = "@OtherProfileId") Then
                prm.Value = Me.UserIdInView
            End If
        Next
    End Sub

    Protected Sub sdsCommunicationHistory_Selected(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles sdsCommunicationHistory.Selected

    End Sub
End Class



﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="jsonMessagesList.aspx.vb" Inherits="Dating.Server.Site.Web.MessagesList" %>
<%@ Register src="~/UserControls/MessagesControlLeft.ascx" tagname="MessagesControlLeft" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <uc2:MessagesControlLeft ID="msgsL" runat="server" UsersListView="AllMessages" EnableViewState="false"/>
    </form>
</body>
</html>

﻿<%@ Page Title="" Language="vb" AutoEventWireup="true" MasterPageFile="~/Members/Members2Inner.Master" 
    CodeBehind="SelectProductSubscription.aspx.vb"  Inherits="Dating.Server.Site.Web.SelectProductSubscription" %>

<%--<%@ Register src="~/UserControls/UCSelectProductMembers2.ascx" tagname="UCSelectProductMembers2" tagprefix="uc1" %>
<%@ Register src="~/UserControls/UCSelectProductMembers2TR.ascx" tagname="UCSelectProductMembers2TR" tagprefix="uc1" %>--%>
<%@ Register src="~/UserControls/UCSelectProductSubscriptionMembers.ascx" tagname="UCSelectProductSubscriptionMembers" tagprefix="uc1" %>
<%@ Register src="~/UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc1" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>
<%@ Register src="~/UserControls/LiveZillaPublic.ascx" tagname="LiveZillaPublic" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="products-list">
    <div id="likes-list-top">
        <div class="list-title">
            <asp:Literal ID="lblItemsName" runat="server" />
            <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
        </div>
    </div>

    <div class="list-content3">
        <uc1:UCSelectProductSubscriptionMembers ID="ucProdSub" runat="server" visible="true" />
<%--        <uc1:UCSelectProductMembers2 ID="ucProdCredits" runat="server" visible="true" />
        <uc1:UCSelectProductMembers2TR ID="ucProdCreditsTR" runat="server" visible="false" />--%>
    </div>

    <uc2:WhatIsIt ID="WhatIsIt1" runat="server" />
</div>


</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cntBodyBottom" runat="server">
    <uc3:LiveZillaPublic ID="ctlLiveZillaPublic" runat="server" />
</asp:Content>

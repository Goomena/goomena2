﻿'Imports Sites.SharedSiteCode
'Imports SpiceLogicPayPalStandard
Imports Library.Public
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers


Public Class SelectPayment2
    Inherits BasePage



    Dim eus_PriceRec As EUS_Price
    Dim amount As String = ""
    Dim type As String = ""
    Dim quantity As String = ""



    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)

        Try
            If clsCurrentContext.VerifyLogin() = True Then
                Me.MasterPageFile = "~/Members/Members2.Master"
            Else
                clsCurrentContext.ClearSession(Session)
                If (Not Me.IsCallback) Then
                    Response.Redirect(ResolveUrl("~/Login.aspx"), False)
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            calculateTransactionData()
            'Dim promoCode As String = txPromoCode.Text
            'If Session("PromoCode") Is Nothing Or Session("PromoCode") <> promoCode Or Session("PromoCode") = "" Then
            '    Session("PromoCode") = promoCode
            'End If
            Dim product As String = quantity & type.ToLower()
            amount = "1.00"
            'Response.Redirect("pwSetPayment.aspx?amount=" & amount & "&product=" & product)
            Dim url = "payment.aspx?method=paymentWall&price=" & amount & "&product=" & product
            iframePayment.Attributes.Add("src", url)
            'Response.Redirect(url)
            'HttpContext.Current.ApplicationInstance.CompleteRequest()
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try



            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try


            If (Not Me.IsPostBack) Then
                LoadLAG()
                'If (Request.UrlReferrer IsNot Nothing) Then
                '    lnkBack.NavigateUrl = Request.UrlReferrer.ToString()
                'Else
                '    lnkBack.NavigateUrl = "~/Members/"
                'End If

                If (Me.MasterProfileId = 269 OrElse Me.MasterProfileId = 371) Then
                    pntTest.Visible = True
                    btnPayTestUser.Text = btnPayTestUser.Text.Replace("#USER#", Me.GetCurrentProfile.LoginName)
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Sub calculateTransactionData()

        Dim choise As String = Request.QueryString("choice").ToLower
        If choise.Contains("credits") Then

            type = "credits"
            quantity = choise.Replace("credits", "")

            'Dim quantities() As String = quantity.Split(New String() {"t"}, StringSplitOptions.RemoveEmptyEntries)
            'If (quantities.Length = 2) Then
            '    quantity = quantities(1)
            'Else
            '    quantity = "0"
            'End If
        End If

        eus_PriceRec = clsPricing.GetPriceFromCredits(quantity)
        amount = eus_PriceRec.Amount

    End Sub



    Public Overrides Sub Master_LanguageChanged()

        If (Me.Visible) Then
            Me._pageData = Nothing
            LoadLAG()
        End If

    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            lblPaymentDescription.Text = CurrentPageData.GetCustomString("lbProductInfo")
            lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###CREDITSAMOUNT###", eus_PriceRec.Credits)

            Dim durStr As String = clsCustomer.GetDurationString(eus_PriceRec.duration, GetLag())
            lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###DURATION###", durStr)

            lblPaymentDescription.Text = lblPaymentDescription.Text.Replace("###PAYMENTAMOUNT###", eus_PriceRec.Amount)

            lbPaymentExtraInfo.Text = CurrentPageData.GetCustomString("lbPaymentExtraInfo")
            'lnkBack.Text = CurrentPageData.GetCustomString(lnkBack.ID)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Protected Sub btnPayTestUser_Click(sender As Object, e As EventArgs) Handles btnPayTestUser.Click
        Dim srvc As New UniPAYIPN()
        Dim cDataRecordIPN As New clsDataRecordIPN()
        cDataRecordIPN.BuyerInfo = New clsDataRecordBuyerInfo With {
            .PayerID = "PayerID", _
            .FirstName = Me.GetCurrentProfile.FirstName, _
            .LastName = Me.GetCurrentProfile.LastName, _
            .PayerEmail = Me.GetCurrentProfile.eMail _
        }
        cDataRecordIPN.CustomerID = Me.GetCurrentProfile().ProfileID
        cDataRecordIPN.CustomReferrer = Me.GetCurrentProfile().CustomReferrer
        cDataRecordIPN.custopmerIP = Session("IP")
        cDataRecordIPN.PaymentDateTime = Date.UtcNow
        cDataRecordIPN.PayProviderAmount = System.Convert.ToInt32(eus_PriceRec.Amount)
        cDataRecordIPN.PayProviderID = 2222
        cDataRecordIPN.PayProviderTransactionID = 3333
        cDataRecordIPN.PayTransactionID = 4444
        cDataRecordIPN.PromoCode = "PromoCode"
        cDataRecordIPN.SaleDescription = quantity & type.ToLower()
        cDataRecordIPN.SaleQuantity = eus_PriceRec.Credits
        cDataRecordIPN.SalesSiteID = ConfigurationManager.AppSettings("siteID")
        cDataRecordIPN.TransactionTypeID = 1111
        cDataRecordIPN.Currency = eus_PriceRec.Currency


        Dim sData As String
        sData = cDataRecordIPN.PayProviderAmount & "+" & cDataRecordIPN.PaymentDateTime & "+" & cDataRecordIPN.PayTransactionID & "+" & cDataRecordIPN.CustomerID & "+" & cDataRecordIPN.SalesSiteProductID & "Extr@Ded0men@"
        Dim h As New Library.Public.clsHash
        Dim Code As String = Library.Public.clsHash.ComputeHash(sData, "SHA512")
        cDataRecordIPN.VerifyHASH = Code


        Dim cDataRecordIPNReturn As clsDataRecordIPNReturn = srvc.IPNEventRaised(cDataRecordIPN)
        txtOutput.Text = txtOutput.Text & "ErrorCode : " & cDataRecordIPNReturn.ErrorCode & vbCrLf
        txtOutput.Text = txtOutput.Text & "HasErrors : " & cDataRecordIPNReturn.HasErrors & vbCrLf
        txtOutput.Text = txtOutput.Text & "Message : " & cDataRecordIPNReturn.Message & vbCrLf
        txtOutput.Text = txtOutput.Text & "ExtraMessage : " & cDataRecordIPNReturn.ExtraMessage & vbCrLf
    End Sub
End Class
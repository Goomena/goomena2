﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2Inner.master"
    CodeBehind="Report.aspx.vb" Inherits="Dating.Server.Site.Web.Report" %>
<%@ Register Assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register src="~/UserControls/MemberLeftPanel.ascx" tagname="MemberLeftPanel" tagprefix="uc2" %>
<%@ Register src="~/UserControls/WhatIsIt.ascx" tagname="WhatIsIt" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="report-member" class="simple-form">

    <script type="text/javascript">
        // <![CDATA[
        function OnReportReasonChanged(cbCountry) {
            HideLoading();
            cbpnlReport.PerformCallback(cbReportReason.GetValue().toString());
            ShowLoading();
        }
        function checkArea() {
            if ($('textarea:visible').length > 0) {
                $('.grey-box-content').addClass('with-textarea');
            }
            else {
                $('.grey-box-content').removeClass('with-textarea');
            }
        }
        // ]]> 
    </script>

    <h2 class="back-link">
        <asp:HyperLink ID="lnkBack" runat="server" Text="Back" CssClass="btn">
        </asp:HyperLink>
    </h2>
    <div class="clear"></div>

    <div class="top-box">
        <div class="your-offer-title">
            <div class="white-box-top">
                <div class="what-is"><asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;"/></div>
                <div class="photo-text">
                    <asp:Label ID="lblReportingTitle" runat="server" Text="Report"/>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="grey-box">
            <div id="divReportForm" runat="server">
                <div class="grey-box-content">
                    <div class="photo">
                        <div class="pr-photo-107-noshad">
                            <asp:HyperLink ID="lnkOtherProf" runat="server"><asp:Image ID="imgOtherProf" runat="server" CssClass="round-img-107" /></asp:HyperLink>
                        </div>
                    </div>
                    <div class="report_form">
                	    <h3 class="why-question"><asp:Label ID="lblWhyReporting" runat="server" Text="Why Are You Reporting"/></h3>
                        <dx:ASPxCallbackPanel ID="cbpnlReport" runat="server" ClientInstanceName="cbpnlReport"
                        CssFilePath="~/App_Themes/DevEx/{0}/styles.css" CssPostfix="DevEx" ShowLoadingPanel="False"
                        ShowLoadingPanelImage="False">
                        <ClientSideEvents EndCallback="function(s, e) {
	HideLoading();
    checkArea();
}" CallbackError="function(s, e) {
	HideLoading();
}" />
                        <LoadingPanelImage Url="~/App_Themes/DevEx/Web/Loading.gif">
                        </LoadingPanelImage>
                        <LoadingPanelStyle ImageSpacing="5px">
                        </LoadingPanelStyle>
                        <PanelCollection>
                            <dx:PanelContent ID="PanelContent1" runat="server" SupportsDisabledAttribute="True">
                                <dx:ASPxComboBox ID="cbReportReason" runat="server" ClientInstanceName="cbReportReason" Width="270">
                                    <ClientSideEvents SelectedIndexChanged="function(s, e) {  OnReportReasonChanged(s); }"
                                        EndCallback="function(s, e) {ShowLoading();}" />
                                </dx:ASPxComboBox>
                                <br />
                                <dx:ASPxMemo ID="txtBody" runat="server">
                                </dx:ASPxMemo>
                            </dx:PanelContent>
                        </PanelCollection>
                    </dx:ASPxCallbackPanel>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>

            <asp:Label ID="lblMessageSent" runat="server" ViewStateMode="Disabled" CssClass="report-sent" Visible="false"></asp:Label>
        </div>
        <div class="white-box-bottom">
            <div class="center">
                <asp:Button ID="btnSubmitReport" runat="server" CssClass="btn" Text="Submit Report" />
            </div>
        </div>
    </div>
        
    <div class="bottom-box">
        <div class="offer-notes">
            <div class="top-arrow"></div>
            <asp:Label ID="lblAdditionalInfo" runat="server" Text="">
            </asp:Label>
        </div>
    </div>
	
</div>

	<uc2:whatisit ID="WhatIsIt1" runat="server" />
</asp:Content>

﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Members/Members2.master" CodeBehind="Coupon.aspx.vb" Inherits="Dating.Server.Site.Web.Coupon" %>


<%@ Register Src="~/UserControls/gifts/ucCredits.ascx" TagPrefix="uc2" TagName="ucCredits" %>
<%@ Register Src="~/UserControls/gifts/ucRewardGiftsCards.ascx" TagPrefix="uc1" TagName="ucRewardGiftsCards" %>
<%@ Register Src="~/UserControls/gifts/ucRewardGiftsAvaliable.ascx" TagPrefix="uc1" TagName="ucRewardGiftsAvaliable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 
    <link href="../v1/CSS/GiftsStyle.css?v113.9" rel="stylesheet" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
             <asp:Panel ID="pnlSelectGiftCard" runat="server">
        <div id="SelectGiftCardContainer">
            
            <div class="crtlSelectGiftCard">
                <uc1:ucRewardGiftsAvaliable runat="server" ID="ucRewardGiftsAvaliable" visible="false" />
                 <uc1:ucRewardGiftsCards runat="server" ID="ucRewardGiftsCards" Visible="false"  />
            </div>
            <div class="crtlConfirm">

            </div>
        </div>
            <script type="text/javascript" >


                $('body').on("click", "#SelectGiftCardContainer .ReadMoreLink", function () {
                    $(".Bigtext").hide();
                    $(this).siblings(".Bigtext").fadeIn(200)
                });
                $('body').on("click", "#SelectGiftCardContainer .ClosebtnBigText", function () {
                    $(".Bigtext").fadeOut(100)
                });

                $('body').on("mouseover", "#SelectGiftCardContainer .btnSelectGiftCard.Disabled", function () {
                    $(".SmallPopupNotEnoughCreditsContainer").hide();
                    $(this).siblings(".SmallPopupNotEnoughCreditsContainer").fadeIn(200)
                });
                $('body').on("mouseleave", "#SelectGiftCardContainer .btnSelectGiftCard.Disabled", function () {
                    $(".SmallPopupNotEnoughCreditsContainer").fadeOut(100)
                });

                </script>
    </asp:Panel>
 
            <uc2:ucCredits runat="server" ID="ucCredits" ShowCreditsHeader="true" />
        </ContentTemplate>
    </asp:UpdatePanel>
      
</asp:Content>



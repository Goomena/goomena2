﻿Imports System.Net
Imports System.IO

Public Class LoginWithGoogle
    Inherits BasePage



    Public Email_address As String
    Public Google_ID As String
    Public firstName As String
    Public LastName As String

    Public Client_ID As String
    Public Return_url As String

    Public Property GoogleUser As GoogleUserInfo
        Get
            If (Session("GoogleUser") IsNot Nothing) Then
                Return Session("GoogleUser")
            End If
            Return Nothing
        End Get
        Set(value As GoogleUserInfo)
            Session("GoogleUser") = value
        End Set
    End Property



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            Client_ID = ConfigurationManager.AppSettings("google_login_clientId").ToString()
            Return_url = ConfigurationManager.AppSettings("google_login_RedirectUrl").ToString()
        End If

        If Request.QueryString("access_token") IsNot Nothing Then
            GoogleUser = GoogleLib.GetUser(Request.QueryString("access_token"))
            If (Session("log=google") = True) Then
                Session("log=google") = Nothing
                Response.Redirect(ResolveUrl("~/Register.aspx?log=google"))
            Else
                Response.Redirect(ResolveUrl("~/Register.aspx"))
            End If
        End If
    End Sub


End Class
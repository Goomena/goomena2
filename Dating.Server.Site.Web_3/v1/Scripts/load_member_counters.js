
var loadMemberActionCountersTimer;
var previousCounters = new function () {
    var t = this;
    t.CreditsCounter = 0;
    t.HasSubscription = false;
    t.WhoViewedMeCounter = 0;
    t.WhoFavoritedMeCounter = 0;
    t.WhoSharedPhotosCounter = 0;
    t.NewDates = 0;
    t.NewWinks = 0;
    t.NewOffers = 0;
    t.NewMessages = 0;
}
function loadMemberActionCounters(counters) {
    var creditsCounter = counters.CreditsCounter;
    var HasSubscription = counters.HasSubscription;
    var whoViewedMeCounter = counters.WhoViewedMeCounter;
    var whoFavoritedMeCounter = counters.WhoFavoritedMeCounter;
    var whoSharedPhotosCounter = counters.WhoSharedPhotosCounter;
    var newDates = counters.NewDates;
    var newWinks = counters.NewWinks;
    var newOffers = counters.NewOffers;
    var newMessages = counters.NewMessages;

    HasSubscription = (HasSubscription == true || HasSubscription == "true");
    //alert(data.Message);
    //console.log(counters);

    try {
        /////Credits

        if (creditsCounter > 0) {
            if ($('.lnkCredits span#notificationsCountWrapper').length === 0)
                $('.lnkCredits').append('<span id="notificationsCountWrapper"><span class="countValue"></span></span>');
            if ($('.lnkBilling span#notificationsCountWrapper').length === 0)
                $('.lnkBilling').append('<span id="notificationsCountWrapper" class="jewelCount"><span class="countValue"></span></span>');

            $('.lnkCredits span.countValue').html(creditsCounter);
            $('.lnkBilling span.countValue').html('(' + creditsCounter + ')');
        }
        else if (HasSubscription == true && creditsCounter == -1) {

            if ($('.lnkCredits span#notificationsCountWrapper').length === 0)
                $('.lnkCredits').append('<span id="notificationsCountWrapper"><span class="countValue"></span></span>');
            if ($('.lnkBilling span#notificationsCountWrapper').length === 0)
                $('.lnkBilling').append('<span id="notificationsCountWrapper" class="jewelCount"><span class="countValue"></span></span>');

        }
        else {
            if ($('.lnkCredits span#notificationsCountWrapper').length > 0) {
                $('.lnkCredits span#notificationsCountWrapper').remove();
                $('.lnkBilling span#notificationsCountWrapper').remove();
            }
        }


        /////WhoViewedMe

        if (whoViewedMeCounter > 0) {
            if ($('.lnkWhoViewedMe span#notificationsCountWrapper').length === 0) {
                $('.lnkWhoViewedMe').append('<span id="notificationsCountWrapper" class="jewelCount"><span class="countValue"></span></span>');
            }

            $('.lnkWhoViewedMe span.countValue').html(whoViewedMeCounter);
        }
        else {
            if ($('.lnkWhoViewedMe span#notificationsCountWrapper').length > 0) {
                $('.lnkWhoViewedMe span#notificationsCountWrapper').remove();
            }
        }


        /////WhoFavoritedMe

        if (whoFavoritedMeCounter > 0) {
            if ($('.lnkWhoFavoritedMe span#notificationsCountWrapper').length === 0) {
                $('.lnkWhoFavoritedMe').append('<span id="notificationsCountWrapper" class="jewelCount"><span class="countValue"></span></span>');
            }

            $('.lnkWhoFavoritedMe span.countValue').html(whoFavoritedMeCounter);
        }
        else {
            if ($('.lnkWhoFavoritedMe span#notificationsCountWrapper').length > 0) {
                $('.lnkWhoFavoritedMe span#notificationsCountWrapper').remove();
            }
        }


        /////WhoSharedPhotos

        if (whoSharedPhotosCounter > 0) {
            if ($('.lnkWhoSharedPhotos span#notificationsCountWrapper').length === 0) {
                $('.lnkWhoSharedPhotos').append('<span id="notificationsCountWrapper" class="jewelCount"><span class="countValue"></span></span>');
            }

            $('.lnkWhoSharedPhotos span.countValue').html(whoSharedPhotosCounter);
        }
        else {
            if ($('.lnkWhoSharedPhotos span#notificationsCountWrapper').length > 0) {
                $('.lnkWhoSharedPhotos span#notificationsCountWrapper').remove();
            }
        }
    }
    catch (e) { }
    try {
        if (typeof __fixQuickLinks !== 'undefined') __fixQuickLinks();
    }
    catch (e) { }

    if (previousCounters.NewDates != counters.NewDates ||
        previousCounters.NewWinks != counters.NewWinks ||
        previousCounters.NewOffers != counters.NewOffers ||
        previousCounters.NewMessages != counters.NewMessages) {

        /////Winks, Messages, Offers, Dates
        cbpTopBar.PerformCallback();
    }

    previousCounters.HasSubscription = counters.HasSubscription;
    previousCounters.CreditsCounter = counters.CreditsCounter;
    previousCounters.WhoViewedMeCounter = counters.WhoViewedMeCounter;
    previousCounters.WhoFavoritedMeCounter = counters.WhoFavoritedMeCounter;
    previousCounters.WhoSharedPhotosCounter = counters.WhoSharedPhotosCounter;
    previousCounters.NewDates = counters.NewDates;
    previousCounters.NewWinks = counters.NewWinks;
    previousCounters.NewOffers = counters.NewOffers;
    previousCounters.NewMessages = counters.NewMessages;
}

function req_loadMemberActionCounters() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "../Members/json.aspx/loadMemberActionCounters",
        data: '{ "data": "data" }',
        dataType: "json",
        success: function (msg) { req_success_loadMemberActionCounters(msg) }
    });
}


function req_success_loadMemberActionCounters(msg) {
    try {
        var counters = eval("(" + msg.d + ")");
        loadMemberActionCounters(counters)
        loadMemberActionCounters_poll(); //Poll the server again
    }
    catch (e) { }
}

function loadMemberActionCounters_poll() {
    clearTimeout(loadMemberActionCountersTimer);
    loadMemberActionCountersTimer = setTimeout(req_loadMemberActionCounters, 60000);
}


jQuery(function ($) {
    loadMemberActionCounters_poll();
});



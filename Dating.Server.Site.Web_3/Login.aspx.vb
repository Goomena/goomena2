﻿Imports Library.Public
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Core.DLL

Public Class Login
    Inherits BasePage


    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("Login.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("Login.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property



    Protected Overrides Sub OnPreInit(ByVal e As EventArgs)
        Try
            Dim check As Boolean = (Request.QueryString("check") = "1")
            If (check) Then
                Return
            End If

            If (Not Page.IsPostBack) Then

                '' redirect user to HTTPS url
                'If (Not Me.IsHTTPS AndAlso
                '    clsCurrentContext.UseHTTPSLogin()) Then
                '    Dim newUrl As String = UrlUtils.GetHTTPSUrl(Request.Url)
                '    Response.Redirect(newUrl)
                'End If

                If (clsCurrentContext.VerifyLogin() = True) Then
                    clsCurrentContext.RedirectToMembersDefault()
                Else
                    If (HttpContext.Current.User.Identity.IsAuthenticated) Then
                        'clear session
                        FormsAuthentication.SignOut()
                        Session.Abandon()

                    End If
                End If

                MyBase.OnPreInit(e)
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "OnPreInit")
        End Try
    End Sub

    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            If (Session("LoginRedirectToDefault") = 1) Then
                Session("LoginRedirectToDefault") = Nothing

                Dim url As String = ResolveUrl("~/Default.aspx")
                url = LanguageHelper.GetNewLAGUrl(Request.Url, GetLag(), url)
                Response.Redirect(url)
            End If

            If (Session("MAX_LOGIN_RETRIES") = 1 OrElse Session("MAX_LOGIN_RETRIES") = 2) Then

                Dim recaptcha As New Recaptcha.RecaptchaControl()
                recaptcha.ID = "recaptcha"
                recaptcha.PublicKey = "6LeqLOsSAAAAACbrctSmamfQ20Jc8BJrBynH-jKt"
                recaptcha.PrivateKey = "6LeqLOsSAAAAAM4HuHzL6W4zmNtKYCOn1iS0AwK7"

                Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
                Dim phRecaptcha As PlaceHolder = MainLogin.FindControl("phRecaptcha")
                If (phRecaptcha IsNot Nothing) Then
                    phRecaptcha.Controls.Add(recaptcha)
                End If

                Dim lblFillRecaptcha As Label = MainLogin.FindControl("lblFillRecaptcha")
                If (lblFillRecaptcha IsNot Nothing) Then
                    lblFillRecaptcha.Text = CurrentPageData.GetCustomString("lblFillRecaptcha")
                End If

            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "OnPreInit")
        End Try
    End Sub


    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If (CurrentPageData Is Nothing) Then Throw New PageDataException()

        Try


            If clsCurrentContext.VerifyLogin() = True Then
                clsCurrentContext.RedirectToMembersDefault(True)

                'Dim url As String = Page.ResolveUrl("~/Members/default.aspx")
                'url = clsCurrentContext.FormatRedirectUrl(url)
                'Response.Redirect(url)

            End If


            Try
                Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_Load")
            End Try


            If (Not Me.IsPostBack) Then
                LoadLAG()
            End If

            Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
            Dim Password As ASPxTextBox = MainLogin.FindControl("Password")
            If (Password IsNot Nothing) Then
                AddHandler Password.PreRender, AddressOf Password_PreRender
            End If

            'Page.Validate()
            'If (Not Page.IsValid) Then

            '        Dim recaptcha As Recaptcha.RecaptchaControl = MainLogin.FindControl("recaptcha")
            '        Dim pbTarget As HtmlGenericControl = MainLogin.FindControl("pbTarget")
            '        ScriptManager.RegisterClientScriptBlock(
            '  recaptcha,
            '  recaptcha.GetType(),
            '  "recaptcha",
            '  "try{" & _
            '  "Recaptcha._init_options(RecaptchaOptions);" & _
            '   "if ( RecaptchaOptions && ""custom"" == RecaptchaOptions.theme )" & _
            '   "{" & _
            '   "  if ( RecaptchaOptions.custom_theme_widget )" & _
            '   "  {" & _
            '   "    Recaptcha.widget = Recaptcha.$(RecaptchaOptions.custom_theme_widget);" & _
            '   "    Recaptcha.challenge_callback();" & _
            '   "  }" & _
            '   "} else {" & _
            '   "  if ( Recaptcha.widget == null || !document.getElementById(""recaptcha_widget_div"") )" & _
            '   "  {" & _
            '   "    jQuery(""#" + pbTarget.ClientID + """).html('<div id=""recaptcha_widget_div"" style=""display:none""></div>');" & _
            '   "    Recaptcha.widget = Recaptcha.$(""recaptcha_widget_div"");" & _
            '   "  }" & _
            '   "  Recaptcha.reload();" & _
            '   "  Recaptcha.challenge_callback();" & _
            '   "}" & _
            '   "}catch(e){}",
            '  True
            ')
            'End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try
            If (Me.IsPostBack) Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "HideLoading", "HideLoading();", True)
            End If

            Dim lblErrorUC1 As Control = ucLogin1.FindControl("lblError")
            If (lblErrorUC1.GetType() Is GetType(ASPxLabel)) Then

                Dim lblErrorUC As ASPxLabel = lblErrorUC1
                If (lblErrorUC IsNot Nothing AndAlso lblErrorUC.Text.Length > 0) Then
                    Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
                    Dim lblError As ASPxLabel = MainLogin.FindControl("lblError")
                    If (lblError IsNot Nothing) Then
                        lblError.Text = lblErrorUC.Text
                        lblError.Visible = True
                        lblErrorUC.Text = ""
                        lblErrorUC.Visible = False
                    End If
                End If

            ElseIf (lblErrorUC1.GetType() Is GetType(Label)) Then

                Dim lblErrorUC As Label = lblErrorUC1
                If (lblErrorUC IsNot Nothing AndAlso lblErrorUC.Text.Length > 0) Then
                    Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
                    Dim lblError As ASPxLabel = MainLogin.FindControl("lblError")
                    If (lblError IsNot Nothing) Then
                        lblError.Text = lblErrorUC.Text
                        lblError.Visible = True
                        lblErrorUC.Text = ""
                        lblErrorUC.Visible = False
                    End If
                End If

            End If


        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            lbHTMLBody.Text = Me.FixHTMLBodyAnchors(cPageBasic.BodyHTM)
            lblHeader.Text = CurrentPageData.GetCustomString("lblHeader")
            lblWelcome.Text = CurrentPageData.GetCustomString("lblWelcome")
            lnkRestore.Text = CurrentPageData.GetCustomString("lnkRestore")
            lnkCreateAccount.Text = CurrentPageData.GetCustomString("lnkCreateAccount")
            'lblNotMemberQuestion.Text = CurrentPageData.GetCustomString("lblNotMemberQuestion")
            'lnkCreateAccountRight.Text = CurrentPageData.GetCustomString("lnkCreateAccountRight")
            'lblHtmlBodyRight.Text = CurrentPageData.GetCustomString("lblHtmlBodyRight")

            lblHeaderRight.Text = CurrentPageData.GetCustomString("lblHeaderRight")
            lblTextRight.Text = CurrentPageData.GetCustomString("lblTextRight")

            lnkCreateAccount.NavigateUrl = "~/Register.aspx"
            If (Not String.IsNullOrEmpty(Request.QueryString("ReturnUrl"))) Then
                lnkCreateAccount.NavigateUrl = lnkCreateAccount.NavigateUrl & "?ReturnUrl=" & HttpUtility.UrlEncode(Request.QueryString("ReturnUrl"))
            End If


            LoadLAG_LoginControl()

            AppUtils.setNaviLabel(Me, "lbPublicNavi2Page", cPageBasic.PageTitle)

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub LoadLAG_LoginControl()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            Dim MainLogin As System.Web.UI.WebControls.Login = ucLogin1.LoginControl
            'MainLogin.UserNameLabelText = CurrentPageData.GetCustomString("txtUserNameLabelText")
            MainLogin.PasswordLabelText = CurrentPageData.GetCustomString("txtPasswordLabelText")
            MainLogin.RememberMeText = CurrentPageData.GetCustomString("txtRememberMeText")
            MainLogin.LoginButtonText = CurrentPageData.GetCustomString("txtLoginButtonText")
            MainLogin.UserNameRequiredErrorMessage = CurrentPageData.GetCustomString("txtUserNameRequiredErrorMessage")
            MainLogin.TitleText = CurrentPageData.GetCustomString("txtLoginTitleText")

            Dim lbForgotPassword As ASPxHyperLink = MainLogin.FindControl("lbForgotPassword")
            If (lbForgotPassword IsNot Nothing) Then
                lbForgotPassword.Text = CurrentPageData.GetCustomString("lbForgotPassword")
            End If

            Dim lbLoginFB As ASPxHyperLink = MainLogin.FindControl("lbLoginFB")
            If (lbLoginFB IsNot Nothing) Then
                lbLoginFB.Text = CurrentPageData.GetCustomString("lbLoginFB")
            End If

            Dim btnJoinToday As ASPxButton = MainLogin.FindControl("btnJoinToday")
            If (btnJoinToday IsNot Nothing) Then
                btnJoinToday.Text = CurrentPageData.GetCustomString("btnJoinToday")
            End If

            Dim UserName As ASPxTextBox = MainLogin.FindControl("UserName")
            If (UserName IsNot Nothing) Then
                'UserName.NullText = CurrentPageData.GetCustomString("UserName.NullText")
                UserName.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("UserName.ValidationSettings.RequiredField.ErrorText")
            End If

            Dim Password As ASPxTextBox = MainLogin.FindControl("Password")
            If (Password IsNot Nothing) Then
                'Password.NullText = CurrentPageData.GetCustomString("Password.NullText")
                Password.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("Password.ValidationSettings.RequiredField.ErrorText")
            End If


            Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
            If (btnLogin IsNot Nothing) Then
                btnLogin.Text = CurrentPageData.GetCustomString("btnLogin")
            End If

            Dim RememberMe As CheckBox = MainLogin.FindControl("RememberMe")
            If (RememberMe IsNot Nothing) Then
                RememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If

            Dim chkRememberMe As ASPxCheckBox = MainLogin.FindControl("chkRememberMe")
            If (chkRememberMe IsNot Nothing) Then
                chkRememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If

            '' controls found on login.aspx page
            Dim lblUserName As Label = MainLogin.FindControl("lblUserName")
            If (lblUserName IsNot Nothing) Then
                lblUserName.Text = CurrentPageData.GetCustomString("lblUserName")
            End If


            Dim lblPassword As Label = MainLogin.FindControl("lblPassword")
            If (lblPassword IsNot Nothing) Then
                lblPassword.Text = CurrentPageData.GetCustomString("lblPassword")
            End If

            Dim btnLoginNow As Button = MainLogin.FindControl("btnLoginNow")
            'Dim btnLoginNow As ASPxButton = MainLogin.FindControl("btnLoginNow")
            If (btnLoginNow IsNot Nothing) Then
                btnLoginNow.Text = CurrentPageData.GetCustomString("btnLoginNow")
                btnLoginNow.Text = btnLoginNow.Text.Replace("&nbsp;", " ")
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub Password_PreRender(sender As Object, e As EventArgs)
        Dim Password As ASPxTextBox = sender
        Password.ClientSideEvents.Init = "function(s, e) {s.SetText('" + Password.Text + "');}"
    End Sub

End Class
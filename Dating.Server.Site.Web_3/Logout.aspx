﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="Logout.aspx.vb" Inherits="Dating.Server.Site.Web.Logout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Logging out...</title>
</head>
<body>
    <form id="form1" runat="server">

    <section class="grid-block" id="bottom-b">
            <div class="grid-box width100 grid-h" style="width: 653px;">
                <div class="module mod-line deepest" style="min-height: 254px;">

		    <h3 class="module-title"><span class="color"><dx:ASPxLabel ID="lblFooterMenuHeader" runat="server" 
                CssClass="footerHeader" EncodeHtml="false" Text="Our Company" EnableDefaultAppearance="False" EnableTheming="False"></dx:ASPxLabel></span></h3>		
        <dx:ASPxMenu ID="mnuFooter" runat="server" AutoSeparators="None" 
                EnableDefaultAppearance="false" EnableTheming="false"
            ApplyItemStyleToTemplates="true" CssClass="arrow" RenderMode="Lightweight">
        </dx:ASPxMenu>

            <div class="grid-box width33 grid-h" style="width: 327px;"><div class="module mod-line deepest" style="min-height: 254px;">

		    <h3 class="module-title"><dx:ASPxLabel ID="lblChartTitle" runat="server" 
                CssClass="footerHeader" EncodeHtml="false" Text="Popular Cities" EnableDefaultAppearance="False" EnableTheming="False"></dx:ASPxLabel></h3>
            </div>
            </div>

            </div>
            </div>
    </section>
    </form>
</body>
</html>

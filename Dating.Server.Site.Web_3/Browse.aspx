﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="Browse.aspx.vb" Inherits="Dating.Server.Site.Web.Browse" %>
<%@ Register src="UserControls/UsersList.ascx" tagname="UsersList" tagprefix="uc1" %>
<%@ Register src="UserControls/OfferControl3.ascx" tagname="OfferControl3" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/v1/css/main3.css?v=7" rel="stylesheet" type="text/css" />
    <script src="//cdn.goomena.com/Scripts/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="https://cdn.goomena.com/Scripts/jquery-ui-1.10.0.js"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
        <div class="clear"></div>
        <%--<div class="container_12" id="tabs-outter">
            <div class="grid_12 top_tabs">
            </div>
        </div>--%>
        <div class="clear"></div>
		<div class="container_12" id="content_top">


		</div>
	    <div class="container_12" id="content">
            <div class="grid_3" id="sidebar-outter">
                <div class="left_tab">
                    <div class="top">
                    </div>
                    <div id="mail" class="middle">
						<div class="cta_text" style="margin-left:10px;">
<%--<asp:HyperLink ID="lnkJoin"  runat="server" NavigateUrl="~/Register.aspx" CssClass="btn btn-success btn-large" style="width: auto; font-size: 14px;">Join Now</asp:HyperLink>--%>
<dx:ASPxButton ID="lnkJoin" runat="server" Text="Join Now"
                                            EncodeHtml="False" Height="31px"
                                            CssFilePath="~/App_Themes/SoftOrange/{0}/styles.css" 
                                            CssPostfix="SoftOrange" Font-Bold="True" 
                                            SpriteCssFilePath="~/App_Themes/SoftOrange/{0}/sprite.css" 
                                            PostBackUrl="~/Register.aspx" UseSubmitBehavior="false">
                                            <Paddings Padding="0px"></Paddings>
                                            <Border BorderWidth="1px"></Border>
                                            <ClientSideEvents Click="
function(s, e) {
    //window.location.href='Register.aspx';
    //e.processOnServer=false;
    ShowLoading();
}"  />
                                        </dx:ASPxButton></div>
					</div>
                    <div class="bottom">
                    </div>
                </div>

		    </div>
            <div class="grid_9 body">
                <div class="middle" id="content-outter">
                    <div class="s_wrap">
                    <asp:UpdatePanel ID="updOffers" runat="server">
                        <ContentTemplate>
                            <uc2:OfferControl3 ID="browseMembers" runat="server" 
                                    UsersListView="SearchingOffers" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </div>
                    <div class="pagination">
                        <div class="float-right">
                            <dx:ASPxPager ID="Pager" runat="server" 
                                ItemCount="3" 
                                ItemsPerPage="1" 
                                RenderMode="Lightweight" 
                                CssFilePath="~/App_Themes/Aqua/{0}/styles.css" 
                                CssPostfix="Aqua" 
                                CurrentPageNumberFormat="{0}" 
                                SpriteCssFilePath="~/App_Themes/Aqua/{0}/sprite.css"
                                SeoFriendly="Enabled">
                                <LastPageButton Visible="True">
                                </LastPageButton>
                                <FirstPageButton Visible="True">
                                </FirstPageButton>
                                <Summary Position="Left" Text="Page {0} of {1} ({2} members)" Visible="false" />
                            </dx:ASPxPager>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
                <div class="bottom">
                </div>
            </div>
		    <div class="clear"></div>
	    </div>
	    <div class="container_12" id="content_bottom">
			
	    </div>
</asp:Content>

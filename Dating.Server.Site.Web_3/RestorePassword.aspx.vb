﻿Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Public Class RestorePassword
    Inherits BasePage


    'Dim _pageData As clsPageData
    'Protected ReadOnly Property CurrentPageData As clsPageData
    '    Get
    '        If (_pageData Is Nothing) Then
    '            _pageData = New clsPageData(Context)
    '            AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
    '        End If
    '        Return _pageData
    '    End Get
    'End Property


    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try


            If (Not Me.IsPostBack) Then
                LoadLAG()
                pnlSubmitResult.Visible = False
                pnlSubmitFormContainer.Visible = True
            End If
            lblSubmitResult.ForeColor = Drawing.Color.Empty

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try


        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            ' lblWelcome.Text = CurrentPageData.GetCustomString("lblWelcome")
            lblHeader.Text = CurrentPageData.GetCustomString("lblHeader")
            lnkCreateAccount.Text = CurrentPageData.GetCustomString("lnkCreateAccount")
            lblRestoreInstructions.Text = CurrentPageData.GetCustomString("lblRestoreInstructions")
            lblUsernameText.Text = CurrentPageData.GetCustomString("lblUsernameText")
            btnLogin.Text = CurrentPageData.GetCustomString("btnLogin")

            lblHeaderRight.Text = CurrentPageData.GetCustomString("lblHeaderRight")
            lblTextRight.Text = CurrentPageData.GetCustomString("lblTextRight")

            txtEmail.NullText = CurrentPageData.GetCustomString("txtEmail.NullText")
            txtEmail.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("txtEmail.ValidationSettings.RequiredField.ErrorText")

            'AppUtils.setNaviLabel(Me, "lbPublicNavi2Page", cPageBasic.PageTitle)
            If (TypeOf Page.Master Is INavigation) Then
                CType(Page.Master, INavigation).AddNaviLink(Me.CurrentPageData.Title, "")
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Try
            Dim success As Boolean = False

            If (txtEmail.Text.Trim().Length > 0) Then
                Dim pass As EUS_Profile = Nothing

                Try
                    ' retrieve user password
                    pass = DataHelpers.GetEUS_Profile_ForPasswordRestore(Me.CMSDBDataContext, txtEmail.Text.Trim())
                    '(From itm In Me.CMSDBDataContext.EUS_Profiles
                    'Where (itm.LoginName.ToUpper() = txtEmail.Text.ToUpper() OrElse _
                    '       itm.eMail.ToUpper() = txtEmail.Text.ToUpper()) _
                    'AndAlso _
                    '      (itm.Status = ProfileStatusEnum.Approved OrElse _
                    '         itm.Status = ProfileStatusEnum.NewProfile OrElse _
                    '         itm.Status = ProfileStatusEnum.Rejected) _
                    'Select itm).FirstOrDefault()

                    '
                    If (pass IsNot Nothing) Then

                        Dim subject As String = CurrentPageData.GetCustomString("RecoveryPassword_EmailSubject")
                        Dim content As String = CurrentPageData.GetCustomString("RecoveryPassword_EmailBody")

                        content = content.Replace("[PASSWORD]", pass.Password)
                        'content = vbCrLf & _
                        '    "Your password for goomena.com account is:" & vbCrLf & _
                        '    pass.Password

                        clsMyMail.SendMail(pass.eMail, subject, content)


                        pnlSubmitResult.Visible = True
                        pnlSubmitFormContainer.Visible = False

                        lblHeader.Text = CurrentPageData.GetCustomString("lblHeader_MessageSent")
                        lblSubmitResult.Text = CurrentPageData.GetCustomString("RecoveryPassword_EmailWasSentUserMessage")
                    Else
                        pnlSubmitResult.Visible = True
                        pnlSubmitFormContainer.Visible = True

                        lblSubmitResult.Text = CurrentPageData.GetCustomString("RecoveryPassword_ErrorEmailNotSentInvalidInput")
                        lblSubmitResult.ForeColor = Drawing.Color.Red
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try


            End If
        Catch ex As Exception

        End Try
    End Sub

End Class
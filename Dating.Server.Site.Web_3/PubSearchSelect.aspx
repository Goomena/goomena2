﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Root.Master" CodeBehind="PubSearchSelect.aspx.vb" 
Inherits="Dating.Server.Site.Web.PubSearchSelect" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    /*p{margin:0; padding:0; }*/
    body {
    font-family: "Segoe UI","Arial";
    font-weight:lighter;
    }
    #hp_content {
    width: 100%;
    background-color: #EEEEEE;
    position: relative;
    padding-top: 1px;
    padding-bottom: 12px;
    border-top: 4px solid #BF1D2A;
    margin-top:20px;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="insidemosaiccontainer" runat="server">
    <asp:Label ID="lblinside" runat="server" Text=""></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="content" runat="server">
    <div class="mid_bg pageContainer">
        <div class="hp_cr" id="hp_top">
        </div>
        <div id="hp_content">
            <div class="container_12">
                <div class="cms_page">
                    <div class="cms_content">
                        <div class="ab_block">
                            <div class="pe_wrapper">
                                <div class="pe_box">
                                    <h2 style="text-shadow: none; border: none; font-family: Lucida Grande,Lucida Sans Unicode,Verdana,Arial,sans-serif;">
                                        <asp:Label ID="lblHeader" runat="server" /><span class="loginHereLink"></span></h2>
                                    <dx:ASPxLabel ID="lbHTMLBody" runat="server" ClientIDMode="AutoID" EncodeHtml="False">
                                    </dx:ASPxLabel>

                                    <div style="margin:60px auto;width:900px">
                                        <a href="PubSearch.aspx?show=2" class="lfloat" onclick="ShowLoading();" style="text-decoration:none;display:block;background:url('images2/new/box1.png') no-repeat scroll 0 0; width:385px;height:465px;">
                                            <h2 style="margin-top: 35px;font-size:24px;color:#fff;font-weight:normal;text-align:center;">
                                                <asp:Literal ID="lblWantToMeetWoman" runat="server">Want to meet Woman</asp:Literal></h2>
                                        </a>
                                        <a href="PubSearch.aspx?show=1" class="rfloat" onclick="ShowLoading();" style="text-decoration:none;display:block;background:url('images2/new/box2.png') no-repeat scroll 0 0; width:385px;height:465px;">
                                            <h2 style="margin-top: 35px;font-size:24px;color:#fff;font-weight:normal;text-align:center;">
                                                <asp:Literal ID="lblWantToMeetMan" runat="server">Want to meet succesfull Man</asp:Literal></h2>
                                        </a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
        </div>
    </div>

 
</asp:Content>

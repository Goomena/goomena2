﻿Imports Dating.Server.Core.DLL
Imports System.ComponentModel
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class ucLogin
    Inherits BaseUserControl


    Public Event LoggingIn(e As System.Web.UI.WebControls.LoginCancelEventArgs)
    Public Event LoggingInFailed(ByRef sender As Object, ByRef e As LoggingInFailedEventArgs)

    Public Property LogginFailReason As LogginFailReasonEnum

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("Login.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("Login.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property



    Public Enum LoginViewEnum
        Standard = 1
        'Templated = 2
        LoggedIn = 3
    End Enum


    Public Property LoginView As LoginViewEnum
        Get

            'If (MultiView1.Views(MultiView1.ActiveViewIndex) Is vwTemplated) Then
            '    Return LoginViewEnum.Templated
            'Else
            If (MultiView1.Views(MultiView1.ActiveViewIndex) Is vwLoggedInUser) Then
                Return LoginViewEnum.LoggedIn
            End If

            Return LoginViewEnum.Standard
        End Get
        Set(ByVal value As LoginViewEnum)

            'If (value = LoginViewEnum.Templated) Then
            '    MultiView1.SetActiveView(vwTemplated)
            'Else
            If (value = LoginViewEnum.Standard) Then
                MultiView1.SetActiveView(vwStandard)
            ElseIf (value = LoginViewEnum.LoggedIn) Then
                MultiView1.SetActiveView(vwLoggedInUser)
            End If

        End Set
    End Property


    <PersistenceMode(PersistenceMode.InnerDefaultProperty), _
    Browsable(True), _
Category("Behavior"), _
Editor("System.Web.UI.WebControls.Login", GetType(System.Web.UI.WebControls.Login))> _
    Public ReadOnly Property LoginControl As System.Web.UI.WebControls.Login
        Get
            Return MainLogin
        End Get
    End Property

    Private Property CurrentLoginPage As String

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CurrentLoginPage = "Login.aspx"
        If (Me.Page.Master IsNot Nothing AndAlso
            Me.Page.Master.GetType().Name = "root2_master") Then
            CurrentLoginPage = "Login2.aspx"
        End If

        Dim check As Boolean = (Request.QueryString("check") = "1")
        If (check) Then
            PerformLoginCheck()

        ElseIf (Not String.IsNullOrEmpty(Request("hash")) AndAlso Not String.IsNullOrEmpty(Request("login"))) Then
            Dim result As String = PerformLogin_ByRequestParams()
            If (result = "ok") Then

                ' login successfull
                Return

            End If
        End If


        If (clsCurrentContext.VerifyLogin()) Then
            LoginView = LoginViewEnum.LoggedIn
        Else
            LoginView = LoginViewEnum.Standard
        End If

        If (Not Me.IsPostBack) Then
            LoadLAG()
        End If

    End Sub


    Public Overrides Sub Master_LanguageChanged()

        If (Me.Visible) Then
            Me._pageData = Nothing
            LoadLAG()
        End If

    End Sub

    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            'btnLogout.Text = CurrentPageData.GetCustomString("btnLogout")

            LN.FormatString = CurrentPageData.GetCustomString("WelcomeUser")
            LN.FormatString = LN.FormatString.Replace("###LOGINNAME###", "{0}")


            If (Me.GetCurrentProfile(True) IsNot Nothing) Then
                lblLoginDate.Text = CurrentPageData.GetCustomString("lblLoginDate")
                If (Me.GetCurrentProfile().LastLoginDateTime.HasValue) Then
                    Dim localTime As String = Me.GetCurrentProfile().LastLoginDateTime.Value.ToLocalTime().ToString("dd/MM/yyyy HH:mm")
                    lblLoginDate.Text = lblLoginDate.Text.Replace("###LOGINDATE###", localTime)
                Else
                    lblLoginDate.Text = lblLoginDate.Text.Replace("###LOGINDATE###", "")
                End If
            End If

            lblMembersArea.Text = CurrentPageData.GetCustomString("lblMembersArea")


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub LoadLAG_old()
        Try
            Dim cPageBasic As Dating.Server.Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            Dim lbForgotPassword As ASPxHyperLink = MainLogin.FindControl("lbForgotPassword")
            If (lbForgotPassword IsNot Nothing) Then
                lbForgotPassword.Text = CurrentPageData.GetCustomString("lbForgotPassword")
            End If

            Dim lbLoginFB As ASPxHyperLink = MainLogin.FindControl("lbLoginFB")
            If (lbLoginFB IsNot Nothing) Then
                lbLoginFB.Text = CurrentPageData.GetCustomString("lbLoginFB")
            End If

            Dim btnJoinToday As ASPxButton = MainLogin.FindControl("btnJoinToday")
            If (btnJoinToday IsNot Nothing) Then
                btnJoinToday.Text = CurrentPageData.GetCustomString("btnJoinToday")
            End If

            Dim UserName As ASPxTextBox = MainLogin.FindControl("UserName")
            If (UserName IsNot Nothing) Then
                UserName.NullText = CurrentPageData.GetCustomString("UserName.NullText")
                UserName.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("UserName.ValidationSettings.RequiredField.ErrorText")
            End If

            Dim Password As ASPxTextBox = MainLogin.FindControl("Password")
            If (Password IsNot Nothing) Then
                Password.NullText = CurrentPageData.GetCustomString("Password.NullText")
                Password.ValidationSettings.RequiredField.ErrorText = CurrentPageData.GetCustomString("Password.ValidationSettings.RequiredField.ErrorText")
            End If


            Dim btnLogin As ASPxButton = MainLogin.FindControl("btnLogin")
            If (btnLogin IsNot Nothing) Then
                btnLogin.Text = CurrentPageData.GetCustomString("btnLogin")
            End If

            Dim RememberMe As CheckBox = MainLogin.FindControl("RememberMe")
            If (RememberMe IsNot Nothing) Then
                RememberMe.Text = CurrentPageData.GetCustomString("RememberMe")
            End If


            '' controls found on login.aspx page
            Dim lblUserName As Literal = MainLogin.FindControl("lblUserName")
            If (lblUserName IsNot Nothing) Then
                lblUserName.Text = CurrentPageData.GetCustomString("lblUserName")
            End If


            Dim lblPassword As Literal = MainLogin.FindControl("lblPassword")
            If (lblPassword IsNot Nothing) Then
                lblPassword.Text = CurrentPageData.GetCustomString("lblPassword")
            End If

            Dim btnLoginNow As ASPxButton = MainLogin.FindControl("btnLoginNow")
            If (btnLoginNow IsNot Nothing) Then
                btnLoginNow.Text = CurrentPageData.GetCustomString("btnLoginNow")
                btnLoginNow.Text = btnLoginNow.Text.Replace("&nbsp;", " ")
            End If

            'btnLogout.Text = CurrentPageData.GetCustomString("btnLogout")

            LN.FormatString = CurrentPageData.GetCustomString("WelcomeUser")
            LN.FormatString = LN.FormatString.Replace("###LOGINNAME###", "{0}")

            'AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Private Sub Login1_LoggingIn(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.LoginCancelEventArgs) Handles MainLogin.LoggingIn
        Try

            e.Cancel = True
            Dim continueAuth As Boolean = True
            Dim hdfHideRecaptcha As HiddenField = MainLogin.FindControl("hdfHideRecaptcha")


            Dim recaptcha As Recaptcha.RecaptchaControl = MainLogin.FindControl("recaptcha")
            If (recaptcha IsNot Nothing) Then
                Dim captchaValidation As Boolean = True
                If (hdfHideRecaptcha IsNot Nothing) Then captchaValidation = False

                If (captchaValidation) Then
                    If (Session("MAX_LOGIN_RETRIES") > 1) Then
                        recaptcha.Validate()
                        If (Not recaptcha.IsValid) Then
                            lblError.Text = Me.CurrentPageData.GetCustomString("Error.ReCaptcha.Invalid")
                            lblError.Visible = True
                            continueAuth = False
                        End If
                    ElseIf (Session("MAX_LOGIN_RETRIES") = 1) Then
                        ' captcha just created
                        Session("MAX_LOGIN_RETRIES") = 2
                    End If
                End If

            End If


            If (continueAuth) Then
                Dim userName As String = MainLogin.UserName
                Dim password As String = MainLogin.Password
                Dim rememberUserName As Boolean = MainLogin.RememberMeSet


                Dim ctrlUserName As ASPxTextBox = MainLogin.FindControl("UserName")
                If (ctrlUserName IsNot Nothing) Then
                    userName = ctrlUserName.Text
                End If

                Dim ctrlPassword As ASPxTextBox = MainLogin.FindControl("Password")
                If (ctrlPassword IsNot Nothing) Then
                    password = ctrlPassword.Text
                End If

                'Dim RememberMe As CheckBox = MainLogin.FindControl("RememberMe")
                'If (RememberMe IsNot Nothing) Then
                '    rememberUserName = RememberMe.Text
                'End If

                Dim RememberMe As ASPxCheckBox = MainLogin.FindControl("chkRememberMe")
                If (RememberMe IsNot Nothing) Then
                    rememberUserName = RememberMe.Checked
                End If


                PerformLogin(userName, password, rememberUserName)
            End If

            Dim check As Boolean = (Request.QueryString("check") = "1")
            If (Not check AndAlso Not String.IsNullOrEmpty(lblError.Text)) Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "failed-auth", "HideLoading();", True)
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
    End Sub

    Private Sub PerformLoginCheck()
        Dim result As String = ""
        Try

            result = PerformLogin_ByRequestParams()

        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        Finally
            Try

                Response.ClearContent()
                Response.Write(result)
                Response.Flush()
                Response.End()

            Catch ex As System.Threading.ThreadAbortException
            Catch ex As Exception
                WebErrorSendEmail(ex, "")
            End Try
        End Try
    End Sub

    Private Function PerformLogin_ByRequestParams() As String
        Dim result As String = Me.CurrentPageData.GetCustomString("ErrorUserNameOrPasswordInvalid")
        Try

            Dim success As Boolean
            Dim login As String = Request("login")
            If (Not String.IsNullOrEmpty(Request("hash")) AndAlso Not String.IsNullOrEmpty(login)) Then
                Dim ds As DSMembers = DataHelpers.GetEUS_Profiles_ByLoginName(login)
                If (ds.EUS_Profiles.Rows.Count > 0) Then
                    Dim dr As DSMembers.EUS_ProfilesRow = ds.EUS_Profiles.Rows(0)
                    Dim hash As String = AppUtils.MD5_GetString(login & "5686-android")
                    If (Request("hash").ToUpper() = hash.ToUpper()) Then
                        clsCurrentContext.ClearSession(Session)
                        result = PerformLogin(login, dr.Password, False)
                        If (String.IsNullOrEmpty(result)) Then
                            success = True
                            result = "ok"

                        End If
                    End If

                End If
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorSendEmail(ex, "")
        End Try
        Return result
    End Function



    Private Function PerformLogin(ByVal userName As String, ByVal password As String, ByVal rememberUserName As Boolean) As String
        Dim returnUrl As String = Nothing
        Dim success As String = ""
        lblError.Text = ""

        'Fetch the role
        Dim role As String = String.Empty
        Dim check As Boolean = (Request.QueryString("check") = "1")

        If (Not String.IsNullOrEmpty(userName) AndAlso Not String.IsNullOrEmpty(password)) Then

            userName = userName.Trim()
            password = password.Trim()

            Session("CustomerAvailableCredits") = Nothing
            Dim DataRecordLoginReturn As clsDataRecordLoginMemberReturn = gLogin.PerfomSiteLogin(userName, password, rememberUserName)

            Try
                Dim LogProfileAccessID As Long = 0
                LogProfileAccessID = DataHelpers.LogProfileAccess(DataRecordLoginReturn.ProfileID, userName, password,
                                                                             DataRecordLoginReturn.IsValid,
                                                                             "Login", Nothing,
                                                                               Session("lagCookie"),
                                                                               Request.ServerVariables("REMOTE_ADDR"),
                                                                               Session("IsMobileAccess"))

                If (Session("IsMobileAccess") Is Nothing) Then
                    clsCurrentContext.AddAccessID(LogProfileAccessID)
                End If
            Catch ex As Exception
                'WebErrorMessageBox(Me, ex, "")
            End Try


            If DataRecordLoginReturn.IsValid Then
                If (check) Then
                    'do nothing
                Else
                    'get the requested page
                    returnUrl = Request.QueryString("ReturnUrl")
                    If returnUrl IsNot Nothing Then
                        Try

                            Dim currentHost As String = UrlUtils.GetCurrentHostURL(Request.Url)
                            Dim _uri As System.Uri = UrlUtils.GetFullURI(currentHost, returnUrl)

                            If ((Not Context.User.Identity.IsAuthenticated) OrElse Not (Context.User.Identity.AuthenticationType = "Forms")) Then
                                ' user is not authenticated
                                Dim lagFromURL As String = clsLanguageHelper.GetLAGFromURL(Request.Url.ToString())
                                returnUrl = LanguageHelper.GetNewLAGUrl2(Request.Url, lagFromURL, _uri.ToString())
                                'returnUrl = clsLanguageHelper.GetNewLAGUrl_WithLagParam(returnUrl, GetLag())
                            Else
                                ' user is authenticated
                                returnUrl = clsCurrentContext.FormatRedirectUrl(_uri.ToString())
                            End If


                        Catch
                        End Try
                    Else
                        returnUrl = Page.ResolveUrl("~/Members/Default.aspx")
                    End If
                End If
            Else

                If Not DataRecordLoginReturn.IsValid Then
                    lblError.Text = Me.CurrentPageData.GetCustomString("ErrorUserNameOrPasswordInvalid")
                    lblError.Visible = True
                    LogginFailReason = LogginFailReasonEnum.UserNameOrPasswordInvalid

                End If

                If DataRecordLoginReturn.HasErrors Then

                    If (DataRecordLoginReturn.Message = ProfileStatusEnum.Deleted.ToString()) Then
                        lblError.Text = Me.CurrentPageData.GetCustomString("ErrorProfileDeleted")
                        lblError.Visible = True
                        LogginFailReason = LogginFailReasonEnum.UserDeleted

                    Else
                        lblError.Text = Me.CurrentPageData.GetCustomString("ErrorOccuredRetry")
                        lblError.Visible = True
                        LogginFailReason = LogginFailReasonEnum.OtherErrorOcurredRetry

                    End If

                End If

                FormsAuthentication.SignOut()

              
            End If
        ElseIf (String.IsNullOrEmpty(userName)) Then
            lblError.Text = Me.CurrentPageData.GetCustomString("UserName.ValidationSettings.RequiredField.ErrorText")
            lblError.Visible = True
            LogginFailReason = LogginFailReasonEnum.UserNameEmpty

        ElseIf (String.IsNullOrEmpty(password)) Then
            lblError.Text = Me.CurrentPageData.GetCustomString("Password.ValidationSettings.RequiredField.ErrorText")
            lblError.Visible = True
            LogginFailReason = LogginFailReasonEnum.PasswordEmpty

        End If

        success = lblError.Text
        If (Not check AndAlso Not String.IsNullOrEmpty(success)) Then

            If (Session("LOGIN_RETRIES") Is Nothing) Then
                Session("LOGIN_RETRIES") = 0
            Else
                Dim r As Integer = Session("LOGIN_RETRIES")
                Session("LOGIN_RETRIES") = r + 1
            End If

            If (Request.Url.LocalPath <> "/" & CurrentLoginPage) Then
                If (Session("LOGIN_RETRIES") >= 2) Then
                    If (Session("MAX_LOGIN_RETRIES") Is Nothing) Then Session("MAX_LOGIN_RETRIES") = 1
                    Dim url As String = ResolveUrl("~/" & CurrentLoginPage)
                    url = LanguageHelper.GetPublicURL(url, GetLag())
                    Response.Redirect(url, False)
                End If
            ElseIf (Request.Url.LocalPath = "/" & CurrentLoginPage) Then
                If (Session("LOGIN_RETRIES") >= 1) Then
                    If (Session("MAX_LOGIN_RETRIES") Is Nothing) Then Session("MAX_LOGIN_RETRIES") = 1
                End If
            End If

            Dim e As New LoggingInFailedEventArgs(LogginFailReason, lblError.Text)
            RaiseEvent LoggingInFailed(Me, e)
            If (e.Handled) Then
                lblError.Visible = False
            End If
        End If

        If (Not String.IsNullOrEmpty(returnUrl)) Then
            Response.Redirect(returnUrl)
        End If

        Return success
    End Function

    Protected Sub MainLogin_LoginError(sender As Object, e As EventArgs) Handles MainLogin.LoginError

    End Sub

    Protected Sub LN_Load(sender As Object, e As EventArgs) Handles LN.Load
        'Dim a = DirectCast(sender, System.Web.UI.WebControls.LoginName).ToString()
        '.UserName()
    End Sub

    Protected Sub MultiView1_ActiveViewChanged(ByVal sender As Object, ByVal e As EventArgs) Handles MultiView1.ActiveViewChanged

    End Sub
End Class
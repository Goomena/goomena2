﻿Imports System.Data.SqlClient
Imports Dating.Server.Core.DLL

Public Class Landingc
    Inherits BasePage
    'Inherits System.Web.UI.Page

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData(Context, Me.Page, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Protected Overrides Sub OnPreInit(ByVal e As EventArgs)
        MyBase.OnPreInit(e)
    End Sub

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init

    End Sub


    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Dim cPageBasic As clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        Try
            If (CurrentPageData.IsQueryStringParamDetected) Then
                ' create canonical link only when pageid param is present

                Dim canonicalHref As String = "/landing/cities/" & CurrentPageData.SitePageID
                MyBase.CreateCanonicalLink(canonicalHref)

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "PreRender->canonical link")
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            LoadLAG()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try


        '        Dim myConn As SqlConnection
        '        Dim myCmd As SqlCommand
        '        Dim myReader As SqlDataReader
        '        Dim address As String = Me.Request.Url.Scheme & "://photos.goomena.com/"
        '        Dim htmlup As String = ""
        '        Dim htmldown As String = ""
        '        Dim counter As Integer
        '        Dim photoid As String
        '        Dim photoname As String
        '        Dim profilename As String
        '        Dim address2 As String
        '        Dim src As String
        '        myConn = New SqlConnection(ConfigurationManager.ConnectionStrings("AppDBconnectionString").ConnectionString)
        '        myCmd = myConn.CreateCommand
        '        myCmd.CommandText = <sql><![CDATA[
        '    SELECT TOP 12 
        '        EUS_CustomerPhotos.CustomerID, 
        '        EUS_CustomerPhotos.FileName, 
        '        EUS_Profiles.LoginName
        '    FROM EUS_CustomerPhotos 
        '    with (nolock)
        '    JOIN EUS_Profiles ON EUS_CustomerPhotos.CustomerID = EUS_Profiles.ProfileID 
        '    WHERE(
        '        EUS_CustomerPhotos.ShowOnGrid=1 
        '	And DisplayLevel=0
        '    And HasAproved=1 
        '    And ISNULL(HasDeclined,0)=0 
        '    AND ISNULL(IsDeleted,0)=0  
        '    And GenderId=2  
        '    AND (EUS_Profiles.Status = 2 OR EUS_Profiles.Status = 4) 
        '    and ISNULL(EUS_Profiles.PrivacySettings_DontShowOnPhotosGrid,0)=0
        '    AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0)=0
        ') 
        'ORDER BY newid() 
        ']]></sql>.Value
        '        myConn.Open()
        '        myReader = myCmd.ExecuteReader

        '        Try

        '            Dim checkList As New List(Of Integer)
        '            counter = 0
        '            Do While myReader.Read()

        '                Dim CustomerID As Integer = myReader.GetInt32(0)
        '                If (checkList.Contains(CustomerID)) Then
        '                    Continue Do
        '                End If
        '                checkList.Add(CustomerID)

        '                If counter < 4 Then
        '                    photoid = myReader.GetValue(0).ToString
        '                    photoname = myReader.GetString(1)
        '                    profilename = myReader.GetString(2)
        '                    address2 = address + photoid
        '                    src = address2 + "/thumbs/" + photoname
        '                    htmlup += "<div class=""landingFacesContainer""><a class=""landingimgborder"" href=""PubProfile.aspx?p=" & HttpUtility.UrlEncode(profilename.ToString) & """ onclick=""ShowLoading();""><img class=""FaceImg"" src=""" & src & """/></a><p>" & profilename & "</p></div>"
        '                Else
        '                    photoid = myReader.GetValue(0).ToString
        '                    photoname = myReader.GetString(1)
        '                    profilename = myReader.GetString(2)
        '                    address2 = address + photoid
        '                    src = address2 + "/thumbs/" + photoname
        '                    htmldown += "<div class=""landingFacesContainer""><a class=""landingimgborder"" href=""PubProfile.aspx?p=" & HttpUtility.UrlEncode(profilename.ToString) & """ onclick=""ShowLoading();""><img class=""FaceImg"" src=""" & src & """/></a><p>" & profilename & "</p></div>"
        '                End If
        '                counter = counter + 1

        '                'stis 8 photos exit
        '                If (counter > 7) Then Exit Do

        '            Loop
        '            lblFacesUp.Text = htmlup
        '            lblFacesDown.Text = htmldown

        '        Catch ex As Exception
        '            Throw New Exception(ex.Message, ex)
        '        Finally

        '            myReader.Close()
        '            myConn.Dispose()
        '            myCmd.Dispose()

        '        End Try


    End Sub

    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            lblHeader.Text = CurrentPageData.GetCustomString("lblHeader")
            'lblCloseRdv.Text = CurrentPageData.GetCustomString("lblCloseRdv")
            'btnRegister.Text = CurrentPageData.GetCustomString("btnRegister")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


End Class
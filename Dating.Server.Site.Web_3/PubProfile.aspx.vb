﻿Imports Library.Public
Imports System.Data.SqlClient
Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL.DSMembers

Public Class PubProfile
    Inherits BasePage


#Region "Props"

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/control.ProfileView", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Protected _pageData_SEO As clsPageData
    Protected ReadOnly Property CurrentPageData_SEO As clsPageData
        Get
            If (_pageData_SEO Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent
                coe = coe Or clsPageData.CacheOptionsEnum.DisabledCacheForPageBasics

                _pageData_SEO = New clsPageData("PubProfile.aspx", Context, coe)
                AddHandler _pageData_SEO.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData_SEO
        End Get
    End Property


    Protected Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property


    Protected Property UserLoginNameInView As String
        Get
            Return ViewState("UserLoginNameInView")
        End Get
        Set(value As String)
            ViewState("UserLoginNameInView") = value
        End Set
    End Property


    Private Property CurrentProfile As EUS_Profile

#End Region


    Protected Overrides Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreInit
        SetMasterPage(CurrentPageData)
        MyBase.Page_PreInit(sender, e)
    End Sub



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            divNoResultsChangeCriteria.Visible = False
            pnlResults.Visible = True

            If (Not Me.IsPostBack) Then
                LoadLAG()
                LoadView()
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData_SEO.cPageBasic
            ModifySEOStrings(cPageBasic)
            AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_PreRender")
        End Try

        If (Me.UserIdInView > 1) Then
            Try
                Dim canonicalHref As String = "/profile/" & UrlUtils.GetValidRoutingProfileURI(HttpUtility.UrlEncode(Me.UserLoginNameInView)) 'HttpUtility.UrlEncode(Me.UserLoginNameInView).Replace("+", "%20")
                MyBase.CreateCanonicalLink(canonicalHref)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "PreRender->canonical link")
            End Try
        End If

    End Sub


    Private Sub ModifySEOStrings(ByRef cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn)
        Try
            Dim result As String = ""
            If (Me.CurrentProfile IsNot Nothing AndAlso
                ((Not String.IsNullOrEmpty(cPageBasic.PageTitle) AndAlso cPageBasic.PageTitle.IndexOf("[LOGIN-NAME]") > -1) OrElse
                 (Not String.IsNullOrEmpty(cPageBasic.MetaDesciption) AndAlso cPageBasic.MetaDesciption.IndexOf("[LOGIN-NAME]") > -1) OrElse
                 (Not String.IsNullOrEmpty(cPageBasic.MetaKeywords) AndAlso cPageBasic.MetaKeywords.IndexOf("[LOGIN-NAME]") > -1))) Then

                'cPageBasic.PageTitle = "[LOGIN-NAME] en línea de [PROFILE-INFO] at Goomena.com"
                'cPageBasic.MetaDesciption = "En Goomena.com, [LOGIN-NAME] está buscando gente en [PROFILE-INFO], usted puede encontrar más personas dispuestas a viajar desde su ubicación para [OTHER-LOGIN-NAME]"
                'cPageBasic.MetaKeywords = "Goomena.com, [LOGIN-NAME] citas en línea, [LOGIN-NAME] buscando"

                Dim login As String = Me.UserLoginNameInView
                Dim country As String = ""
                If (Not String.IsNullOrEmpty(Me.CurrentProfile.Country)) Then country = Me.CurrentProfile.Country

                Dim city As String = ""
                If (Not String.IsNullOrEmpty(Me.CurrentProfile.City)) Then city = Me.CurrentProfile.City

                Dim _region As String = ""
                If (Not String.IsNullOrEmpty(Me.CurrentProfile.Region)) Then _region = Me.CurrentProfile.Region

                Dim countryTo As String = MyBase.GetGeoCountry()
                If (city.Length > 0) Then result = result & city & ", "
                If (_region.Length > 0) Then result = result & _region & ", "
                If (countryTo <> country AndAlso country.Length > 0) Then
                    result = result & ProfileHelper.GetCountryName(country) & ", "
                End If

                result = result.Trim().TrimEnd({","c})

                cPageBasic.PageTitle = cPageBasic.PageTitle.
                         Replace("[LOGIN-NAME]", Me.UserLoginNameInView).
                         Replace("[PROFILE-INFO]", result)


                ' get random profile
                Dim otherMemberLogin As String = GetRandomLoginName()
                cPageBasic.MetaDesciption = cPageBasic.MetaDesciption.
                        Replace("[LOGIN-NAME]", Me.UserLoginNameInView).
                        Replace("[PROFILE-INFO]", result).
                        Replace("[OTHER-LOGIN-NAME]", otherMemberLogin)

                cPageBasic.MetaKeywords = cPageBasic.MetaKeywords.
                         Replace("[LOGIN-NAME]", Me.UserLoginNameInView)


            Else
                If (Not String.IsNullOrEmpty(cPageBasic.PageTitle)) Then
                    cPageBasic.PageTitle = cPageBasic.PageTitle.
                        Replace("[LOGIN-NAME]", "").
                        Replace("[PROFILE-INFO]", "")
                End If

                If (Not String.IsNullOrEmpty(cPageBasic.MetaDesciption)) Then
                    cPageBasic.MetaDesciption = cPageBasic.MetaDesciption.
                           Replace("[LOGIN-NAME]", "").
                           Replace("[PROFILE-INFO]", "").
                           Replace("[OTHER-LOGIN-NAME", "")
                End If

                If (Not String.IsNullOrEmpty(cPageBasic.MetaKeywords)) Then
                    cPageBasic.MetaKeywords = cPageBasic.MetaKeywords.
                        Replace("[LOGIN-NAME]", "")
                End If

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "PreRender->SEO variables")
        End Try

    End Sub


    Private Function GetRandomLoginName() As String
        Dim randomLogin As String = ""
        Try

            Dim sql As String = <sql><![CDATA[
            and EUS_Profiles.ProfileID in (
				select top(50) ProfileID
				from (
					select top(1000) ProfileID 
					from EUS_Profiles p1
					where p1.IsMaster=1
					and p1.Status in (1,2,4)
					and GenderId=[GENDERID]
					order by LastActivityDateTime
				)	as t1
				order by NEWID()
            )
]]></sql>.Value

            If (Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID) Then
                sql = sql.Replace("[GENDERID]", "2")
            Else
                sql = sql.Replace("[GENDERID]", "1")
            End If

            Dim NumberOfRecordsToReturn As Integer = 1
            Dim prms As New clsSearchHelperParameters()
            prms.AdditionalWhereClause = sql
            prms.rowNumberMin = 0
            prms.rowNumberMax = 1
            prms.SearchSort = SearchSortEnum.None
            FillPublicSearchParams(prms)
            prms.performCount = False

            Dim countRows As Integer
            Dim ds As DataSet = clsSearchHelper.GetMembersToSearchDataTable_Public(prms)
            If (prms.performCount) Then
                countRows = ds.Tables(0).Rows(0)(0)
                If (countRows > 0) Then
                    Dim dt As DataTable = ds.Tables(1)
                    randomLogin = dt.Rows(0)("LoginName").ToString()
                End If
            Else
                countRows = ds.Tables(0).Rows.Count
                If (countRows > 0) Then
                    Dim dt As DataTable = ds.Tables(0)
                    randomLogin = dt.Rows(0)("LoginName").ToString()
                End If
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "PreRender->SEO variables search")
        End Try
        Return randomLogin
    End Function

    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        LoadView()
    End Sub



    Protected Sub LoadLAG()
        Try
            'Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic

            SetControlsValue(Me, CurrentPageData)
            'AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)

            lnkFav.Text = CurrentPageData.GetCustomString("lnkFavorite")
            lnkOffer.Text = CurrentPageData.GetCustomString("lnkMakeOffer")
            lnkMsg.Text = CurrentPageData.GetCustomString("lnkMessage")
            lnkLike.Text = CurrentPageData.GetCustomString("lnkWink")
            'lnkMakeOfferF.Text = CurrentPageData.GetCustomString("lnkMakeOffer")
            'lnkUnlockOfferF.Text = CurrentPageData.GetCustomString("OfferAcceptedUnlockText")
            'lnkWinkF.Text = CurrentPageData.GetCustomString("lnkWink")
            'lnkUnWinkF.Text = CurrentPageData.GetCustomString("lnkWink")

            'lnkMessageM.Text = CurrentPageData.GetCustomString("lnkMessage")
            'lnkFavoriteM.Text = CurrentPageData.GetCustomString("lnkFavorite")
            'lnkUnFavoriteM.Text = CurrentPageData.GetCustomString("lnkUnFavorite")
            'lnkMakeOfferM.Text = CurrentPageData.GetCustomString("lnkMakeOffer")
            'lnkUnlockOfferM.Text = CurrentPageData.GetCustomString("OfferAcceptedUnlockText")
            'lnkWinkM.Text = CurrentPageData.GetCustomString("lnkWink")
            'lnkUnWinkM.Text = CurrentPageData.GetCustomString("lnkWink")
            'msg_ActionsText.Text = CurrentPageData.GetCustomString("lnkMessage")

            'cmdHideGifts.Text = CurrentPageData.GetCustomString("cmdHideGifts")
            'lnkReport.Text = CurrentPageData.GetCustomString("lnkReport")
            'lnkBlock.Text = CurrentPageData.GetCustomString("lnkBlock")
            'lnkUnWink.Text = CurrentPageData.GetCustomString("lnkUnWink")

            'lblAllowAccessPhotosLevel.Text = CurrentPageData.GetCustomString(lblAllowAccessPhotosLevel.ID)
            'lblAllowAccessPhotosLevelIcon.Text = CurrentPageData.GetCustomString(lblAllowAccessPhotosLevel.ID)

            'setLevelPopUp.HeaderText = CurrentPageData.GetCustomString("popupWindows.PhotoLevelTitle")

            'msg_LastUpdatedDT.Text = CurrentPageData.GetCustomString(msg_LastUpdatedDT.ID)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub LoadView()

        Try
            Me.UserIdInView = 0

            Try
                Dim currentLoginName As String = Request.QueryString("p")
                If (String.IsNullOrEmpty(currentLoginName) AndAlso
                    Page.RouteData.Values("loginname") IsNot Nothing) Then
                    currentLoginName = Page.RouteData.Values("loginname").Trim()
                    currentLoginName = currentLoginName.Replace("+", " ")
                End If

                If (Not String.IsNullOrEmpty(currentLoginName)) Then
                    currentLoginName = UrlUtils.RevertValidProfileURI(currentLoginName)
                    Dim prof As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(Me.CMSDBDataContext, currentLoginName, ProfileStatusEnum.Approved)
                    If (prof IsNot Nothing AndAlso
                        clsNullable.NullTo(prof.PrivacySettings_DontShowOnPhotosGrid) = False AndAlso
                        clsNullable.NullTo(prof.PrivacySettings_HideMeFromSearchResults) = False) Then

                        Me.UserIdInView = prof.ProfileID
                        Me.UserLoginNameInView = prof.LoginName
                        Me.CurrentProfile = prof

                        If (prof.LookingFor_ToMeetMaleID.HasValue) Then Me.SessionVariables.MemberData.LookingFor_ToMeetMaleID = prof.LookingFor_ToMeetMaleID
                        If (prof.LookingFor_ToMeetFemaleID.HasValue) Then Me.SessionVariables.MemberData.LookingFor_ToMeetFemaleID = prof.LookingFor_ToMeetFemaleID

                    End If

                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try


            If (Me.UserIdInView > 1) Then
                Me.LoadProfile(Me.UserIdInView)
                Dim done As Boolean = clsUserDoes.MarkAsViewed(Me.UserIdInView, Me.MasterProfileId, False)
                If (done) Then
                    Dim task As New PageOnUnloadTask()
                    task.FromProfileID = Me.MasterProfileId
                    task.ToProfileID = Me.UserIdInView
                    task.NotificationType = NotificationType.ProfileViewed
                    MyBase.UnloadingTasks.Add(task)
                End If
            Else

                divNoResultsChangeCriteria.Visible = True
                pnlResults.Visible = False

            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub






    Sub LoadProfile(ByVal profileId As Integer)
        Try

            Dim allowDisplayLevel = 0

            'Dim masterRow As EUS_ProfilesRow = Nothing
            Dim _profilerows As New clsProfileRows(profileId)
            'Dim currentRow As EUS_ProfilesRow = _profilerows.GetMirrorRow()

            Dim usersHasCommunication As Boolean = False
            Dim currentRow As EUS_ProfilesRow = Nothing
            If (Me.MasterProfileId = profileId) Then
                currentRow = _profilerows.GetMirrorRow()
                usersHasCommunication = True

            Else
                currentRow = _profilerows.GetMasterRow()
                usersHasCommunication = clsUserDoes.HasCommunication(profileId, Me.MasterProfileId)

                Dim phtLvl As EUS_ProfilePhotosLevel = (From itm In Me.CMSDBDataContext.EUS_ProfilePhotosLevels
                                                       Where itm.FromProfileID = Me.UserIdInView AndAlso itm.ToProfileID = Me.MasterProfileId
                                                       Select itm).FirstOrDefault()


                If (phtLvl IsNot Nothing) Then
                    allowDisplayLevel = phtLvl.PhotoLevelID
                End If
            End If


            '''''''''''''''''''''''''''''''''''''
            ' load profile photos
            '''''''''''''''''''''''''''''''''''''

            imgProfile.ImageUrl = ""

            Dim dtPhotos As EUS_CustomerPhotosDataTable = DataHelpers.GetEUS_CustomerPhotos_ByProfileOrMirrorID(profileId).EUS_CustomerPhotos
            Dim otherUserPhotos1 As New List(Of clsUserListItem)
            'Dim otherUserPhotos2 As New List(Of clsUserListItem)

            Dim defaultPhoto As New clsUserListItem()
            defaultPhoto.ImageUrlOnClick = ""
            defaultPhoto.LoginName = currentRow.LoginName
            defaultPhoto.ProfileViewUrl = ResolveUrl("~/Register.aspx")

            For Each dr As EUS_CustomerPhotosRow In dtPhotos

                If (Me.MasterProfileId <> profileId AndAlso dr.DisplayLevel > allowDisplayLevel) Then
                    Continue For
                End If

                If (dr.HasAproved = True) Then

                    Dim uli As New clsUserListItem()
                    uli.ImageUrlOnClick = ""
                    uli.LoginName = currentRow.LoginName
                    uli.ProfileViewUrl = ResolveUrl("~/Register.aspx")
                    uli.ImageUrl = ProfileHelper.GetProfileImageURL(dr.CustomerID, dr.FileName, currentRow.GenderId, False, Me.IsHTTPS)
                    uli.ImageThumbUrl = ProfileHelper.GetProfileImageURL(dr.CustomerID, dr.FileName, currentRow.GenderId, True, Me.IsHTTPS)
                    uli.ImageUploadDate = dr.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
                    If (Not dr.IsDefault) Then
                        otherUserPhotos1.Add(uli)
                    Else
                        imgProfile.ImageUrl = uli.ImageThumbUrl
                    End If

                End If
            Next


            If (otherUserPhotos1.Count = 0 AndAlso String.IsNullOrEmpty(imgProfile.ImageUrl)) Then

                Dim uli As New clsUserListItem()
                uli.ImageUrlOnClick = ""
                uli.LoginName = currentRow.LoginName
                uli.ProfileViewUrl = ResolveUrl("~/Register.aspx")
                uli.ImageUrl = ProfileHelper.GetDefaultImageURL(currentRow.GenderId)
                uli.ImageThumbUrl = ProfileHelper.GetDefaultImageURL(currentRow.GenderId)
                'uli.ImageUploadDate = dr.DateTimeToUploading.ToLocalTime().ToString("d MMM, yyyy H:mm")
                otherUserPhotos1.Add(uli)

            End If


            ' set photo's list 
            lvPhotos.DataSource = otherUserPhotos1
            lvPhotos.DataBind()


            ' load profile text content
            '''''''''''''''''''''''''''''''''''''

            If (Me.MasterProfileId <> profileId) Then
                '' the viewing profile is owned by another member
                ' '''''''''''''''''''''''''''''''''''''''''''''

                'profileOwner.Visible = False

                'If ProfileHelper.IsMale(currentRow.GenderId) Then
                '    lblPhotoLevel.Text = CurrentPageData.GetCustomString("MALE_lblPhotoLevelTitle")
                'Else
                '    lblPhotoLevel.Text = CurrentPageData.GetCustomString("FEMALE_lblPhotoLevelTitle")
                'End If
                'lblPhotoLevel.Text = lblPhotoLevel.Text.Replace("###LOGINNAME###", currentRow.LoginName)
                'lblPhotoLevel.Text = lblPhotoLevel.Text.Replace(" ", "&nbsp;")

                'lblAllowAccessPhotosLevel.Text = CurrentPageData.GetCustomString("lblAllowAccessPhotosLevel")
                'lblAllowAccessPhotosLevel.Text = lblAllowAccessPhotosLevel.Text.Replace("###LOGINNAME###", currentRow.LoginName)

                'btnSave.Text = CurrentPageData.GetCustomString("btnSave")

                'If (currentRow.IsPrivacySettings_HideMyLastLoginDateTimeNull() OrElse currentRow.PrivacySettings_HideMyLastLoginDateTime = False) Then
                '    pLastLoggedInDT.Visible = True
                'Else
                '    pLastLoggedInDT.Visible = False
                'End If

            End If


            lblLogin.Text = currentRow.LoginName


            If ProfileHelper.IsMale(currentRow.GenderId) Then
                'liAnnualIncome.Visible = True

                '    If (ProfileHelper.IsGenerous(currentRow.AccountTypeId)) Then
                '        lblAccountType.Text = globalStrings.GetCustomString("msg_GenerousMale")
                '    ElseIf (ProfileHelper.IsAttractive(currentRow.AccountTypeId)) Then
                '        lblAccountType.Text = globalStrings.GetCustomString("msg_AttractiveMale")
                '    End If

            ElseIf ProfileHelper.IsFemale(currentRow.GenderId) Then
                'liAnnualIncome.Visible = False

                '    If (ProfileHelper.IsGenerous(currentRow.AccountTypeId)) Then
                '        lblAccountType.Text = globalStrings.GetCustomString("msg_GenerousFemale")
                '    ElseIf (ProfileHelper.IsAttractive(currentRow.AccountTypeId)) Then
                '        lblAccountType.Text = globalStrings.GetCustomString("msg_AttractiveFemale")
                '    End If

            End If

            'lblAccountType.Text = "(" & lblAccountType.Text & ")"


            Dim mins As Integer = 20
            Dim config As New clsConfigValues()
            If (Not Integer.TryParse(config.members_online_minutes, mins)) Then mins = 20
            Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-mins)

            If (Not currentRow.IsLastActivityDateTimeNull() AndAlso Not currentRow.IsIsOnlineNull()) Then
                If (currentRow.LastActivityDateTime >= LastActivityUTCDate) Then
                    imgOnline.CssClass = "image-online"
                End If
            End If



            lblWillTravel.Visible = False
            If (Not currentRow.IsAreYouWillingToTravelNull()) Then
                If (currentRow.AreYouWillingToTravel) Then
                    lblWillTravel.Text = CurrentPageData.GetCustomString("lblWillTravel")
                    lblWillTravel.Visible = True
                End If
            End If


            lblAboutMeHeading.Visible = False
            If (Not currentRow.IsAboutMe_HeadingNull()) Then
                lblAboutMeHeading.Text = currentRow.AboutMe_Heading
                lblAboutMeHeading.Visible = True
            End If


            If (Not currentRow.IsAboutMe_DescribeYourselfNull()) Then
                lblAboutMeDescr.Text = currentRow.AboutMe_DescribeYourself
            End If
            divAboutMe.Visible = (lblAboutMeDescr.Text.Trim() <> "")


            If (Not currentRow.IsAboutMe_DescribeAnIdealFirstDateNull()) Then
                lblFirstDateExpectationDescr.Text = currentRow.AboutMe_DescribeAnIdealFirstDate
            End If
            divFirstDateExpectation.Visible = (lblFirstDateExpectationDescr.Text.Trim() <> "")


            SetLocationText(currentRow)
            If (Not currentRow.IsBirthdayNull()) Then
                lblAge.Text = Me.CurrentPageData.GetCustomString("lblAgeYearsOld")
                lblAge.Text = lblAge.Text.Replace("###AGE###", ProfileHelper.GetCurrentAge(currentRow.Birthday))

                Dim zodiac As String = globalStrings.GetCustomString("Zodiac" & ProfileHelper.ZodiacName(currentRow.Birthday), Session("LAGID"))
                lblZwdio.Text = String.Format("({0})", zodiac)
            End If

            If (Not currentRow.IsPersonalInfo_HeightIDNull()) Then
                lblHeight.Text = ProfileHelper.GetHeightString(currentRow.PersonalInfo_HeightID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_BodyTypeIDNull()) Then
                lblBodyType.Text = ProfileHelper.GetBodyTypeString(currentRow.PersonalInfo_BodyTypeID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_HairColorIDNull()) Then
                lblHairClr.Text = ProfileHelper.GetHairColorString(currentRow.PersonalInfo_HairColorID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_EyeColorIDNull()) Then
                lblEyeClr.Text = ProfileHelper.GetEyeColorString(currentRow.PersonalInfo_EyeColorID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsOtherDetails_EducationIDNull()) Then
                lblEducation.Text = ProfileHelper.GetEducationString(currentRow.OtherDetails_EducationID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_ChildrenIDNull()) Then
                lblChildren.Text = ProfileHelper.GetChildrenNumberString(currentRow.PersonalInfo_ChildrenID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_EthnicityIDNull()) Then
                lblEthnicity.Text = ProfileHelper.GetEthnicityString(currentRow.PersonalInfo_EthnicityID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_ReligionIDNull()) Then
                lblReligion.Text = ProfileHelper.GetReligionString(currentRow.PersonalInfo_ReligionID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_SmokingHabitIDNull()) Then
                lblSmoking.Text = ProfileHelper.GetSmokingString(currentRow.PersonalInfo_SmokingHabitID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsPersonalInfo_DrinkingHabitIDNull()) Then
                lblDrinking.Text = ProfileHelper.GetDrinkingString(currentRow.PersonalInfo_DrinkingHabitID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsLastLoginDateTimeNull()) Then
                lblLastLoginDT.Text = currentRow.LastLoginDateTime.ToLocalTime().ToString("d MMM, yyyy H:mm")
            End If

            If (Not currentRow.IsOtherDetails_OccupationNull()) Then
                lblOccupation.Text = currentRow.OtherDetails_Occupation
            End If


            If (Not currentRow.IsOtherDetails_AnnualIncomeIDNull()) Then
                lblIncomeLevel.Text = ProfileHelper.GetIncomeString(currentRow.OtherDetails_AnnualIncomeID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsOtherDetails_NetWorthIDNull()) Then
                lblNetWorth.Text = ProfileHelper.GetNetWorthString(currentRow.OtherDetails_NetWorthID, Me.Session("LagID"))
            End If


            If (Not currentRow.IsLookingFor_RelationshipStatusIDNull()) Then
                lblRelationshipStatus.Text = ProfileHelper.GetRelationshipStatusString(currentRow.LookingFor_RelationshipStatusID, Me.Session("LagID"))
            End If


            lblLookingToMeet.Text = ""
            If (Not currentRow.IsLookingFor_ToMeetMaleIDNull() AndAlso currentRow.LookingFor_ToMeetMaleID) Then
                lblLookingToMeet.Text = lblLookingToMeet.Text & ProfileHelper.GetLookingToMeet_Male_String(Me.Session("LagID")) & ", "
            End If

            If (Not currentRow.IsLookingFor_ToMeetFemaleIDNull() AndAlso currentRow.LookingFor_ToMeetFemaleID) Then
                lblLookingToMeet.Text = lblLookingToMeet.Text & ProfileHelper.GetLookingToMeet_Female_String(Me.Session("LagID")) & ", "
            End If

            lblLookingToMeet.Text = lblLookingToMeet.Text.TrimEnd(" "c, ","c)



            Try

                Dim _dt As New DataTable
                _dt.Columns.Add("TypeOfDateText")

                Dim i As Integer = 0
                For i = 0 To (Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count - 1)

                    'While i < Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows.Count
                    Try
                        Dim txt As String = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)(GetLag()).ToString()
                        If (String.IsNullOrEmpty(txt)) Then txt = Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)("US")


                        Select Case Lists.gDSLists.EUS_LISTS_TypeOfDating.Rows(i)("TypeOfDatingId")
                            Case TypeOfDatingEnum.AdultDating_Casual
                                If (Not currentRow.IsLookingFor_TypeOfDating_AdultDating_CasualNull() AndAlso currentRow.LookingFor_TypeOfDating_AdultDating_Casual = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                            Case TypeOfDatingEnum.Friendship
                                If (Not currentRow.IsLookingFor_TypeOfDating_FriendshipNull() AndAlso currentRow.LookingFor_TypeOfDating_Friendship = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                            Case TypeOfDatingEnum.LongTermRelationship
                                If (Not currentRow.IsLookingFor_TypeOfDating_LongTermRelationshipNull() AndAlso currentRow.LookingFor_TypeOfDating_LongTermRelationship = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                            Case TypeOfDatingEnum.MarriedDating
                                If (Not currentRow.IsLookingFor_TypeOfDating_MarriedDatingNull() AndAlso currentRow.LookingFor_TypeOfDating_MarriedDating = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                            Case TypeOfDatingEnum.MutuallyBeneficialArrangements
                                If (Not currentRow.IsLookingFor_TypeOfDating_MutuallyBeneficialArrangementsNull() AndAlso currentRow.LookingFor_TypeOfDating_MutuallyBeneficialArrangements = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                            Case TypeOfDatingEnum.ShortTermRelationship
                                If (Not currentRow.IsLookingFor_TypeOfDating_ShortTermRelationshipNull() AndAlso currentRow.LookingFor_TypeOfDating_ShortTermRelationship = True) Then
                                    Dim dr As DataRow = _dt.NewRow()
                                    dr("TypeOfDateText") = txt
                                    _dt.Rows.Add(dr)
                                End If

                        End Select

                    Catch ex As Exception
                        WebErrorMessageBox(Me, ex, "")
                    End Try

                    '    i = i + 1
                    'End While
                Next

                lvTypeOfDate.DataSource = _dt
                lvTypeOfDate.DataBind()
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            'If (lvTypeOfDate.Items.Count = 0) Then
            '    msg_InterestedIn.Visible = False
            'Else
            '    msg_InterestedIn.Visible = True
            'End If

            If (clsCurrentContext.VerifyLogin()) Then

                lnkFav.NavigateUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(currentRow.LoginName))
                lnkOffer.NavigateUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(currentRow.LoginName))
                lnkMsg.NavigateUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(currentRow.LoginName))
                lnkLike.NavigateUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(currentRow.LoginName))
                lnkPhoto.NavigateUrl = ResolveUrl("~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(currentRow.LoginName))

            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub SetLocationText(currentRow As EUS_ProfilesRow)
        Dim _locationStrCity = ""
        Dim _locationStrRegion = ""
        Dim _locationStrCountry = ""
        Dim _birthDay = ""


        If (Not currentRow.IsCityNull()) Then
            _locationStrCity = currentRow.City
        End If

        If (Not currentRow.Is_RegionNull()) Then
            _locationStrRegion = currentRow._Region
        End If

        If (Not currentRow.IsCountryNull()) Then
            _locationStrCountry = ProfileHelper.GetCountryName(currentRow.Country)
        End If



        lblBirth.Text = ""
        If (Not currentRow.IsBirthdayNull()) Then
            Dim bornon As String = Me.CurrentPageData.VerifyCustomString("BornOn")
            bornon = bornon.Replace("###DATE###", currentRow.Birthday.ToString("d MMM, yyyy"))
            lblBirth.Text = bornon
        End If


        lblLocation.Text = ""

        If (_locationStrCity <> "") Then
            lblLocation.Text = _locationStrCity
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCity <> "") Then
            lblLocation.Text = lblLocation.Text & ", "
        End If
        If (_locationStrRegion <> "") Then
            lblLocation.Text = lblLocation.Text & _locationStrRegion
        End If
        If (_locationStrRegion <> "" AndAlso _locationStrCountry <> "") Then
            lblLocation.Text = lblLocation.Text & ", "
        End If
        If (_locationStrCountry <> "") Then
            Dim fromStr = globalStrings.GetCustomString("FromWord", GetLag())
            lblLocation.Text = fromStr & ":&nbsp;" & lblLocation.Text & _locationStrCountry
        End If
    End Sub


End Class
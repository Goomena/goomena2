﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class QuickSearchCriteriasMan

    '''<summary>
    '''pnlQUserName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlQUserName As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''txtUserNameQ control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtUserNameQ As Global.DevExpress.Web.ASPxEditors.ASPxTextBox

    '''<summary>
    '''imgSearchByUserName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgSearchByUserName As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''lnkAdvanced control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkAdvanced As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''msg_UserUserNameOrFiltersQ_ND control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_UserUserNameOrFiltersQ_ND As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlQUserName1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlQUserName1 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''ageMin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ageMin As Global.Dating.Server.Site.Web.AgeDropDown

    '''<summary>
    '''msg_ToSmall control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_ToSmall As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ageMax control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ageMax As Global.Dating.Server.Site.Web.AgeDropDown

    '''<summary>
    '''lblYears control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblYears As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''msg_DistanceFromMe_ND control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_DistanceFromMe_ND As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlDistance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDistance As Global.Dating.Server.Site.Web.DistanceDropDown

    '''<summary>
    '''pnlQFilters control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlQFilters As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''chkPhotos control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkPhotos As Global.DevExpress.Web.ASPxEditors.ASPxCheckBox

    '''<summary>
    '''chkOnline control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkOnline As Global.DevExpress.Web.ASPxEditors.ASPxCheckBox

    '''<summary>
    '''chkVIP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkVIP As Global.DevExpress.Web.ASPxEditors.ASPxCheckBox

    '''<summary>
    '''msg_BirthdayNote control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents msg_BirthdayNote As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnSearch_ND control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearch_ND As Global.System.Web.UI.WebControls.Button
End Class

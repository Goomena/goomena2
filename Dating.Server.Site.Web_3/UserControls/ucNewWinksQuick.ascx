﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucNewWinksQuick.ascx.vb" Inherits="Dating.Server.Site.Web.ucNewWinksQuick" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>

<dx:ASPxDataView ID="dvWinks" runat="server" ColumnCount="1" EnableDefaultAppearance="false" EnableTheming="False" 
    ItemSpacing="0px" RowPerPage="10" Width="100%" 
    EmptyDataText="Can't find any new wink.">
    <ItemTemplate>
    <div class="quick_list m_item newest ql-likes">
        <div class="linkNotifWrapper" onclick="jsLoad('<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Likes.aspx?scrl=") & HttpUtility.UrlEncode(Eval("LoginName"))%>');">
            <div class="pic-round-img-75 lfloat">
                <asp:HyperLink ID="lnkFromImage" runat="server" 
                    ImageUrl='<%# Dating.Server.Site.Web.AppUtils.GetImage(Eval("ProfileID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False, Me.IsHTTPS,Dating.Server.Core.DLL.PhotoSize.D150) %>' 
                    NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Likes.aspx?scrl=") & HttpUtility.UrlEncode(Eval("LoginName"))%>'
                    onclick="ShowLoading()">
                </asp:HyperLink>
            </div>
            <div class="lfloat login-link">
                <asp:HyperLink ID="lnkFromLogin" runat="server" 
                    NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Likes.aspx?scrl=") & HttpUtility.UrlEncode(Eval("LoginName"))%>' 
                    Text='<%# Eval("LoginName") %>' CssClass="login-name"
                    onclick="ShowLoading()">
                </asp:HyperLink>
                <div><asp:Literal ID="lblYouReceivedWinkOfferText" runat="server"/></div>
                <p id="pWink"><asp:Literal ID="lblWantToKnowFirstDatePriceText" runat="server" Visible="false" /></p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear">
    </div>
    </ItemTemplate>
    <Paddings Padding="0px" />
    <ItemStyle>
    <Paddings Padding="0px" />
    </ItemStyle>
</dx:ASPxDataView>

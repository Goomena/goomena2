﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CheckUserMessagesPopup.ascx.vb" 
    Inherits="Dating.Server.Site.Web.CheckUserMessagesPopup" %>
<dx:ASPxPopupControl runat="server"
        Height="185px"
        ID="popupCM"
        ClientInstanceName="popupCM" 
        ClientIDMode="AutoID" 
        AllowDragging="True" 
        PopupVerticalAlign="TopSides" 
        PopupHorizontalAlign="LeftSides" 
        Modal="True" 
        AllowResize="True" 
        CloseAction="CloseButton"
        ShowShadow="False">
    <ContentStyle HorizontalAlign="Left" VerticalAlign="Middle">
        <Paddings Padding="0px" PaddingTop="15px" />
        <Paddings Padding="0px" PaddingTop="15px" ></Paddings>
    </ContentStyle>
    <CloseButtonStyle BackColor="White">
        <HoverStyle>
            <BackgroundImage ImageUrl="~/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
                VerticalPosition="center"></BackgroundImage>
        </HoverStyle>
        <BackgroundImage ImageUrl="~/Images2/popup-wrn/close.png" Repeat="NoRepeat" HorizontalPosition="center"
            VerticalPosition="center"></BackgroundImage>
        <BorderLeft BorderColor="White" BorderStyle="Solid" BorderWidth="10px" />
    </CloseButtonStyle>
    <CloseButtonImage Url="~/Images/spacer10.png" Height="43px" Width="43px">
    </CloseButtonImage>
        <SizeGripImage Height="40px" Url="~/Images/resize.png" Width="40px">
        </SizeGripImage>
    <HeaderStyle>
        <Paddings Padding="0px" PaddingLeft="0px" />
        <Paddings PaddingTop="0px" PaddingBottom="0px"></Paddings>
        <BorderTop BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderRight BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
        <BorderBottom BorderColor="White" BorderStyle="Solid" BorderWidth="5px" />
    </HeaderStyle>
<ModalBackgroundStyle BackColor="Black" Opacity="70"></ModalBackgroundStyle>
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>


<script type="text/javascript">

    function m__setPopupClass__popupCM() {
        var popup = popupCM
        var divId = popup.name + '_PW-1';
        $('#' + divId).addClass('fancybox-quickmessage-popup').prepend($('<div class="header-line"></div>'));
        var closeId = popup.name + '_HCB-1Img';
        $('#' + closeId).addClass('fancybox-popup-close');
        var hdrId = popup.name + '_PWH-1T';
        $('#' + hdrId).addClass('fancybox-popup-header').css('text-align', 'left');
        //var frmId = popup.name + '_CSD-1';
        //$('#' + frmId).addClass('iframe-container');
        //ctl00_cntBodyBottom_popupConfirmAction_CIFD-1
    }

    $(function () {
        setTimeout(m__setPopupClass__popupCM, 300);
    });
    
    function openPopupCheckMessageByID(QueueID) {
        if (QueueID > 0) {
            var contentUrl = '<%= ResolveUrl("~/Members/UserMessages1.aspx")%>?itemid=' + QueueID;
            var headerText = null;
            var wdt = 200;
            var hgt = 200;
            openPopupCheckMessage(contentUrl, headerText, wdt, hgt)
        }
    }

    function openPopupCheckMessage(contentUrl, headerText, wdt, hgt) {
        var popup = window["popupCM"]
        popup.SetContentUrl(contentUrl);
        if (headerText != null) popup.SetHeaderText(headerText);
        popup.Show();

        if (typeof wdt !== 'undefined' && wdt != null) {
            if (typeof hgt !== 'undefined' && hgt != null) {
                popup.SetSize(wdt, hgt);
            }
        }

        popup.UpdatePosition();
        popup.Closing.AddHandler(__Closing_Remove);
        //popup.CloseUp.AddHandler(__CloseUp_Remove);

        function __Closing_Remove(s, e) {
            try {
                    s.SetContentUrl('about:blank');
                    s.Closing.RemoveHandler(__Closing_Remove);
                }
                catch (e) { }
            }
        }
</script>

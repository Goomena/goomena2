﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucLandingProfiles2014.ascx.vb" Inherits="Dating.Server.Site.Web.ucLandingProfiles2014" %>

<%--<link rel="stylesheet" type="text/css" href="/v1/CSS/landing2014.css" />--%>
<div class="landing-bottom-container">
    <div class="landing-helper">
        <div class="landing-bottom">
            <div style="height: 1px;">&nbsp;</div>
            <div class="landing-bottom-up">
                <asp:Label ID="lblFacesUp" runat="server" Text=""></asp:Label>
                <div class="clear"></div>
            </div>
            <div class="landing-bottom-down">
                <asp:Label ID="lblFacesDown" runat="server" Text=""></asp:Label>
                <div class="landing-bottom-register">
                    <asp:Label ID="btnRegister" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblCloseRdv" runat="server" Text="" CssClass="register-text"></asp:Label>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div style="height: 50px;">&nbsp;</div>
        </div>
    </div>
</div>


<div class="landing-connect-buttons">
    <div class="landing-helper">
        <div class="landing-bottom">
            <div class="register-email lfloat"><asp:Literal ID="lblRegisterEmail" runat="server" Text="" /></div>
            <div class="register-fb lfloat"><asp:Literal ID="lblRegisterFB" runat="server" Text="" /></div>
            <div class="register-gplus lfloat"><asp:Literal ID="lblRegisterGplus" runat="server" Text="" /></div>
            <div class="clear"></div>
            <div class="connect-text">
                <div class="fb-wall"><asp:Literal ID="lbFBNote" runat="server" Text="Don't worry, we won't publish on your wall" /></div>
            </div>
        </div>
    </div>
</div>

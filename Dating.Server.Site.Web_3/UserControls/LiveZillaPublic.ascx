﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="LiveZillaPublic.ascx.vb" Inherits="Dating.Server.Site.Web.LiveZillaPublic" %>
  

<div id="LiveZilla_btn" style="position:fixed;bottom:50px;right:1px;z-index:2;" runat="server" clientidmode="Static">
<%
    If (Request.Url.Scheme = "https") Then
%>
<!-- LiveZilla Chat Button Link Code (ALWAYS PLACE IN BODY ELEMENT) -->
<div style="text-align:center;width:317px;height:122px;">
<a href="javascript:void(window.open('https://chat.goomena.com/chat.php','','width=590,height=610,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))"><img src="https://chat.goomena.com/image.php?id=01&amp;type=inlay" width="317" height="122" border="0" alt="LiveZilla Live Help"></a><div style="margin-top:2px;"><%--<a href="https://www.livezilla.net" target="_blank" title="LiveZilla Live Chat" style="font-size:10px;color:#bfbfbf;text-decoration:none;font-family:verdana,arial,tahoma;">LiveZilla Live Chat</a>--%></div></div>
<!-- http://www.LiveZilla.net Chat Button Link Code -->
<%
    Else
%>
<!-- LiveZilla Chat Button Link Code (ALWAYS PLACE IN BODY ELEMENT) -->
<div style="text-align:center;width:317px;height:122px;">
<a href="javascript:void(window.open('http://chat.goomena.com/chat.php','','width=590,height=610,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))"><img src="http://chat.goomena.com/image.php?id=01&amp;type=inlay" width="317" height="122" border="0" alt="LiveZilla Live Help"></a><div style="margin-top:2px;"><%--<a href="http://www.livezilla.net" target="_blank" title="LiveZilla Live Chat" style="font-size:10px;color:#bfbfbf;text-decoration:none;font-family:verdana,arial,tahoma;">LiveZilla Live Chat</a>--%></div></div>
<!-- http://www.LiveZilla.net Chat Button Link Code -->
<%
    End If
%>
<div style="position:absolute;right:3px;top:-11px;">
    <a onclick="closeLiveZilla('#LiveZilla_btn')" title="Close LiveZilla chat window"><img src="/images2/new/close2.png" style="border:none;width:76px;cursor:pointer;" /></a>
</div>
</div>


<div id="livezilla_tracking" style="display:none" runat="server" clientidmode="Static">

<%
    If (Request.Url.Scheme = "https") Then
%>
<!-- LiveZilla Tracking Code (ALWAYS PLACE IN BODY ELEMENT) -->
<%--<script type="text/javascript">
    var script;
    function runExtLinks10() {
        script = document.createElement("script"); 
        script.type = "text/javascript";
        var src = "https://chat.goomena.com/server.php?request=track&output=jcrpt&nse=" + Math.random();
        setTimeout("script.src=src;document.getElementById('livezilla_tracking').appendChild(script)", 1);
    }
</script>--%>
<noscript><img src="https://chat.goomena.com/server.php?request=track&amp;output=nojcrpt" width="0" height="0" style="visibility:hidden;" alt=""></noscript>
<!-- http://www.LiveZilla.net Tracking Code -->
<%
    Else
%>
<!-- LiveZilla Tracking Code (ALWAYS PLACE IN BODY ELEMENT) -->
<%--<script type="text/javascript">
    var script;
    function runExtLinks10() {
        script = document.createElement("script");
        script.type = "text/javascript";
        var src = "http://chat.goomena.com/server.php?request=track&output=jcrpt&nse=" + Math.random();
        setTimeout("script.src=src;document.getElementById('livezilla_tracking').appendChild(script)", 1);
    }
</script>--%>
<noscript><img src="http://chat.goomena.com/server.php?request=track&amp;output=nojcrpt" width="0" height="0" style="visibility:hidden;" alt=""></noscript>
<!-- http://www.LiveZilla.net Tracking Code -->
<%
    End If
%>

</div>
<script type="text/javascript">
    var script;
    var src;
    function runExtLinks10() {
        var isHttps = ('<%= (Request.Url.Scheme = "https").ToString().ToLower() %>' == 'true' ? true : false);

        script = document.createElement("script");
        script.type = "text/javascript";
        src = "http://chat.goomena.com/server.php?request=track&output=jcrpt&nse=" + Math.random();
        if (isHttps)
            src = "https://chat.goomena.com/server.php?request=track&output=jcrpt&nse=" + Math.random();

        setTimeout("script.src=src;document.getElementById('livezilla_tracking').appendChild(script)", 1);
    }

    if (document.getElementById('livezilla_tracking') != null) {
        $(function () {
            setTimeout(runExtLinks10, 900);
        });
    }
</script>


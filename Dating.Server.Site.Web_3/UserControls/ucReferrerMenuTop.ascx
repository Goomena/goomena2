﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucReferrerMenuTop.ascx.vb"
    Inherits="Dating.Server.Site.Web.ucReferrerMenuTop" %>

    <div>
    
<dx:ASPxMenu ID="mnu1" runat="server" AutoSeparators="RootOnly" 
    ItemAutoWidth="False" Width="100%" 
            SeparatorWidth="0px" BackColor="White" EncodeHtml="False">
    <GutterBackgroundImage Repeat="NoRepeat" />
    <Items>
        <dx:MenuItem NavigateUrl="~/Members/Referrer.aspx" Text="Dashboard">
            <Image Url="~/Images/referrer/menu-1.png">
            </Image>
            <ItemStyle>
            <BackgroundImage Repeat="NoRepeat" />
            </ItemStyle>
        </dx:MenuItem>
        <dx:MenuItem NavigateUrl="javascript:void(0);" Text="Tasks" DropDownMode="True">
            <Items>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerFamilyTree.aspx" 
                    Text="Οικογενειακό δένδρο">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerAnalytics.aspx" 
                    Text="Αναλυτική κατάσταση">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerSupplies.aspx" 
                    Text="Προμήθειες μου">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerStatistics.aspx" Text="Στατιστικά">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerPayments.aspx" Text="Πληρωμές μου">
                </dx:MenuItem>
            </Items>
            <Image Url="~/Images/referrer/menu-2.png">
            </Image>
        </dx:MenuItem>
        <dx:MenuItem DropDownMode="True" NavigateUrl="javascript:void(0);" Text="Options">
            <Items>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerSettings.aspx" Text="Ρυθμίσεις">
                </dx:MenuItem>
                <dx:MenuItem NavigateUrl="~/Members/ReferrerPercents.aspx" Text="Ποσοστά μου">
                </dx:MenuItem>
            </Items>
            <Image Url="~/Images/referrer/menu-3.png">
            </Image>
        </dx:MenuItem>
        <dx:MenuItem Text="My Payments">
            <Image Url="~/Images/referrer/menu-4.png">
            </Image>
        </dx:MenuItem>
        <dx:MenuItem Text="Help guide">
        </dx:MenuItem>
    </Items>
    <LoadingPanelImage Url="~/App_Themes/PlasticBlue/Web/Loading.gif">
    </LoadingPanelImage>
    <ItemSubMenuOffset FirstItemY="-1" LastItemY="-1" Y="-1" />
    <RootItemSubMenuOffset FirstItemX="1" LastItemX="1" X="1" />
    <ItemStyle DropDownButtonSpacing="13px" ToolbarDropDownButtonSpacing="5px" 
        ToolbarPopOutImageSpacing="5px" >
    <DropDownButtonStyle>
        <BackgroundImage HorizontalPosition="center" 
            ImageUrl="~/Images/referrer/arrow.png" Repeat="NoRepeat" 
            VerticalPosition="center" />
    </DropDownButtonStyle>
    <HoverStyle>
        <Border BorderColor="#8C8C8C" BorderStyle="Solid" BorderWidth="1px" />
    </HoverStyle>
    <Paddings PaddingLeft="6px" PaddingRight="6px" PaddingBottom="6px" PaddingTop="6px" />
    <BackgroundImage ImageUrl="~/Images/referrer/menu-bg.png" />
    <Border BorderColor="#8C8C8C" BorderStyle="Solid" BorderWidth="1px" />
    </ItemStyle>
    <SubMenuItemStyle Height="20px" Width="150px">
        <HoverStyle>
            <Border BorderWidth="1px" />
        </HoverStyle>
        <Border BorderStyle="None" />
        <BorderBottom BorderStyle="None" BorderWidth="0px" />
    </SubMenuItemStyle>
    <SubMenuStyle GutterWidth="0px" />
    <Border BorderStyle="None" />
</dx:ASPxMenu>
<%--<dx:ASPxMenu ID="mnu2" runat="server">
    <Items>
    </Items>
</dx:ASPxMenu>--%>

    </div>

﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class ProfileIconCurrent
    Inherits BaseUserControl

    Public Enum NavigateToEnum
        None = 0
        ProfilePhotos = 1
        Profile = 2
    End Enum



    'Public Enum ImageSizeViewEnum
    '    Normal = 0
    '    Tiny = 1
    '    Thumb = 2
    'End Enum



    Public Property NavigateTo As NavigateToEnum
        Get
            If (ViewState("NavigateTo") IsNot Nothing) Then
                Return ViewState("NavigateTo")
            End If
            Return NavigateToEnum.None
        End Get
        Set(value As NavigateToEnum)
            ViewState("NavigateTo") = value
        End Set
    End Property


    Public Property ShowName As Boolean
        Get
            If (ViewState("ShowName") IsNot Nothing) Then
                Return ViewState("ShowName")
            End If
            Return False
        End Get
        Set(value As Boolean)
            ViewState("ShowName") = value
        End Set
    End Property



    Public Property CssClass As String
        Get
            Return lnkPhoto.CssClass
        End Get
        Set(value As String)
            lnkPhoto.CssClass = value
        End Set
    End Property



    'Public Property CssClass As String
    '    Get
    '        If (ViewState("CssClass") IsNot Nothing) Then
    '            Return ViewState("CssClass")
    '        End If
    '        Return ""
    '    End Get
    '    Set(value As String)

    '        If (ViewState("CssClass") <> value) Then

    '            Dim oldStyle As String = ViewState("CssClass")

    '            If (Not String.IsNullOrEmpty(oldStyle)) Then
    '                lnkPhoto.CssClass.Replace(oldStyle, "")
    '            End If

    '            If (Not String.IsNullOrEmpty(value)) Then
    '                lnkPhoto.CssClass = lnkPhoto.CssClass & " " & value
    '                'imgPhoto.CssClass = imgPhoto.CssClass & " " & value
    '                'lblName.CssClass = lblName.CssClass & " " & value
    '            End If

    '            ViewState("CssClass") = value
    '        End If

    '    End Set
    'End Property



    'Protected Property ImageSizeView As ImageSizeViewEnum
    '    Get
    '        If (ViewState("ImageSizeView") IsNot Nothing) Then
    '            Return ViewState("ImageSizeView")
    '        End If
    '        Return ImageSizeViewEnum.Normal
    '    End Get
    '    Set(value As ImageSizeViewEnum)
    '        ViewState("ImageSizeView") = value
    '    End Set
    'End Property



    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            ' If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileView", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/control.ProfileView", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()

                'Dim profileId As Integer = Me.Session("ProfileID")
                Me.LoadProfile()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

        If (Not Page.IsPostBack) Then
        End If

    End Sub

    Protected Overrides Sub OnPreRender(e As System.EventArgs)
        MyBase.OnPreRender(e)

        lblName.Visible = Me.ShowName

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            'Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub




    Public Sub RefreshThumb()

        'Dim profileId As Integer = Me.Session("ProfileID")
        Me.LoadProfile()

    End Sub



    Sub LoadProfile()

        Dim profileId As Integer = Me.MasterProfileId
        Try
            If (profileId = Me.MasterProfileId) Then
                ' view icon of current user, that has logged in 

                lblName.Text = Me.SessionVariables.MemberData.LoginName

                Dim fileName As String = Nothing
                Dim photo As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(profileId)
                If (photo IsNot Nothing) Then
                    imgPhoto.ImageUrl = ProfileHelper.GetProfileImageURL(photo.CustomerID, photo.FileName, Me.SessionVariables.MemberData.GenderId, True, Me.IsHTTPS)
                Else
                    imgPhoto.ImageUrl = ProfileHelper.GetDefaultImageURL(Me.SessionVariables.MemberData.GenderId)
                End If

                '~/Members/Profile.aspx?do=edit
                If (Me.NavigateTo = NavigateToEnum.Profile) Then
                    lnkPhoto.NavigateUrl = ResolveUrl("~/Members/Profile.aspx?do=edit")
                ElseIf (Me.NavigateTo = NavigateToEnum.ProfilePhotos) Then
                    lnkPhoto.NavigateUrl = ResolveUrl("~/Members/Photos.aspx")
                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


End Class
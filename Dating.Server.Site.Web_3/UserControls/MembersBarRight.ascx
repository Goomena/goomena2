﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MembersBarRight.ascx.vb" Inherits="Dating.Server.Site.Web.MembersBarRight" %>
<%@ Register Src="~/UserControls/ucNewWinksQuick.ascx" TagName="ucNewWinksQuick" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ucNewOffersQuick.ascx" TagName="ucNewOffersQuick" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ucNewMessagesQuick.ascx" TagName="ucNewMessagesQuick" TagPrefix="uc4" %>
<%@ Register Src="~/UserControls/ucNewDatesQuick.ascx" TagName="ucNewDatesQuick" TagPrefix="uc5" %>


<div class="rfloat membersBar">
<div class="rfloat mbr-menu-item" id="SettingsPopupContent">
    <asp:HyperLink ID="btnSettings" runat="server" 
        CssClass="menuPulldown hdr-link dropdown-toggle" 
        ViewStateMode="Disabled" data-toggle="dropdown">More</asp:HyperLink>

    <div class="mbr-menu-item-inner"></div>

    <asp:Panel ID="pnlSettingsPopup" runat="server" style="position:relative;">
        <div id="divListQSt" class="dropdown-menu">
            <div class="popup-arrow"></div>
            <div class="dxpcControl" style="min-height:120px;">
                <ul id="userNavigation" class="navigation defaultStyle">
                    <li>
                        <asp:HyperLink ID="lnkAccSettings" runat="server" Text="Account Settings" CssClass="navSubmenu lnkAccSettings"
                            NavigateUrl="~/Members/Settings.aspx" onClick="ShowLoading();">
                        </asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="lnkPubDefault" runat="server" Text="Public default" CssClass="navSubmenu lnkPubDefault"
                            NavigateUrl="~/Default.aspx?vw=pub" onClick="ShowLoading();">
                        </asp:HyperLink>
                    </li>
                    <li>
                        <asp:LoginStatus ID="LoginStatus1" runat="server" CssClass="navSubmenu LoginStatus1" />
                    </li>
                    <li class="menuDivider"></li>
                    <li>
                        <asp:HyperLink ID="lnkAccDelete" runat="server" Text="Delete Account" CssClass="navSubmenu lnkAccDelete"
                            NavigateUrl="~/Members/Settings.aspx?account=remove" onClick="ShowLoading();">
                        </asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="btnHelp" runat="server" Text="Help" CssClass="navSubmenu btnHelp"
                            NavigateUrl="~/CmsPage.aspx?PageId=158" onClick="ShowLoading();">
                        </asp:HyperLink>
                    </li>
                </ul>
            </div>
        </div>
    </asp:Panel>
</div>
<div class="clear"></div>
</div>


﻿Imports DevExpress.Web.ASPxEditors

Public Class SMSInfoPopup
    Inherits BaseUserControl


    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.UCSelectProductMembers", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UCSelectProductMembers", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If (Not Me.IsPostBack AndAlso Me.Visible) Then
                LoadLAG()
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        'LoadView()
    End Sub


    Public Sub LoadLAG() 'ByVal LAGID As String, ByVal NoCache As Boolean
        Try
            popupSMSInfo.HeaderText = Me.CurrentPageData.GetCustomString("SMS.Info.Popup.HeaderText")
            lblContent.Text = Me.CurrentPageData.GetCustomString("SMS.Info.Popup")

        Catch ex As Exception
            WebErrorMessageBox(ex, "")
        End Try

    End Sub


End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UcRefferalCouponHistory.ascx.vb" Inherits="Dating.Server.Site.Web.UcRefferalCouponHistory" %>

<div >
    <h3 class="page-title">
        <asp:Label ID="lblPageTitle" runat="server" Text="Mycoupons"></asp:Label>
          <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
    </h3>
</div>
<div class="Coupons-history">


<dx:ASPxGridView ID="gv" runat="server" AutoGenerateColumns="False" 
    DataSourceID="sdsCouponHistory" KeyFieldName="CouponId" 
    Width="100%">
    <Columns>

        <dx:GridViewDataTextColumn FieldName="CouponId" ReadOnly="True" 
            VisibleIndex="0" ShowInCustomizationForm="False" Visible="False" CellStyle-HorizontalAlign="Center">
            <EditFormSettings Visible="False" />
             <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="True" />

<CellStyle HorizontalAlign="Center"></CellStyle>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataDateColumn FieldName="DatetimeCreated" VisibleIndex="1" ReadOnly="True" CellStyle-HorizontalAlign="Center" ShowInCustomizationForm="False" Caption="Creation Date" Width="90px" Name="DatetimeCreated">
            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm" NullText=" ">
            </PropertiesDateEdit>
             <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="True" />

<CellStyle HorizontalAlign="Center"></CellStyle>
        </dx:GridViewDataDateColumn>
        <dx:GridViewDataTextColumn FieldName="CreditAmmount" ReadOnly="True" ShowInCustomizationForm="False" CellStyle-HorizontalAlign="Center" VisibleIndex="2" Caption="Ammount" Width="60px" Name="CreditAmmount">
            <PropertiesTextEdit DisplayFormatString="N">
            </PropertiesTextEdit>
             <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="True" />

<CellStyle HorizontalAlign="Center"></CellStyle>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="CouponName" ReadOnly="True" ShowInCustomizationForm="False" VisibleIndex="3" Name="CouponName">
         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Font-Bold="True"/>
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="CouponCode" ReadOnly="True" CellStyle-HorizontalAlign="Center"  ShowInCustomizationForm="False" VisibleIndex="4" Width="125px" Name="CouponCode">
       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Font-Bold="True" />

<CellStyle HorizontalAlign="Center"></CellStyle>
              </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="CashedOut" Caption="Active" ReadOnly="True" ShowInCustomizationForm="False" VisibleIndex="5" Width="45px" Name="CashedOut">
            <DataItemTemplate>   <div class="CouponActive">

                                   <span class="tooltip tooltip_CouponActive" style="display:none;">
                        <span class="ttptext CouponActive"> <%= Me.lblTooltipCashOutText%>
                        <%# GetDateTOString(Eval("CashedOutDateTime"))%></span>
             <span class="ttpbottom  left-arrow CouponActive"></span>
                    </span>
                <div Class='<%# If(GetDbBoolean(Eval("CashedOut")) = True, "CouponActive  tt_enabled", "CouponActive")%> '>
                
                <span ID="imgCouponActive" runat="server" Class='<%# If(GetDbBoolean(Eval("CashedOut")) = True, "imgCouponActive CouponActive Disabled", "imgCouponActive CouponActive Enabled")%> '
                 
                   />
              
                <%--<asp:Label  ID="lblCouponCashOutDate" runat="server" CssClass='<%# If(GetDbBoolean(Eval("CashedOut")) = True, "lblCouponCashOutDate CouponActive Disabled", "lblCouponCashOutDate CouponActive Enabled")%> '><%# GetDateTOString(Eval("CashedOutDateTime"))%></asp:Label>--%>
            </div>
            </div>
               
            </DataItemTemplate>
       <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Font-Bold="True" />
       </dx:GridViewDataTextColumn>
       <dx:GridViewDataDateColumn FieldName="CashedOutDateTime" VisibleIndex="6"  Visible="false"  ReadOnly="True" Width="100px">
            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm">
         
                   </PropertiesDateEdit>
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle"  Font-Bold="True"/>
        </dx:GridViewDataDateColumn>

    </Columns>
    <SettingsBehavior AllowClientEventsOnLoad="False" AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
    <SettingsPager PageSize="40">
    </SettingsPager>
    <SettingsDetail ShowDetailButtons="False" />
    <Styles>
        <Row BackColor="#f8f8f8"></Row>
        <AlternatingRow BackColor="#ffffff" Enabled="True">
        </AlternatingRow>
        <Cell>
            <Paddings PaddingBottom="5px" PaddingLeft="5px" PaddingTop="5px" />
        </Cell>
    </Styles>
</dx:ASPxGridView>

<asp:SqlDataSource ID="sdsCouponHistory" runat="server" 
    ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>" 
    SelectCommand="GetAffCouponHistory" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter DefaultValue="12168" Name="ProfileId" />
    </SelectParameters>
</asp:SqlDataSource>

</div>
<%--<dx:GridViewDataTextColumn FieldName="CustomerCreditsId" ReadOnly="True" 
    Visible="False" VisibleIndex="0">
    <EditFormSettings Visible="False" />
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="CustomerId" Visible="False" 
    VisibleIndex="1">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="Credits" Visible="False" VisibleIndex="2">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="CustomerTransactionID" Visible="False" 
    VisibleIndex="4">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="CreditsTypeId" Visible="False" 
    VisibleIndex="5">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="ConversationWithCustomerId" 
    Visible="False" VisibleIndex="6">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="ConversationWithLoginName" 
    Visible="False" VisibleIndex="7">
</dx:GridViewDataTextColumn>
<dx:GridViewDataTextColumn FieldName="CreditsType" Visible="False" 
    VisibleIndex="8">
</dx:GridViewDataTextColumn>--%>

﻿<%@ Control Language="vb" AutoEventWireup="false" 
    CodeBehind="NewMessageControl.ascx.vb" 
    Inherits="Dating.Server.Site.Web.NewMessageControl" %>


<div id="msg-edit" class="middle">

    <div id="msg-edit-header">
        <div class="lfloat">
            <h2>
                <asp:Label ID="lblSendMessageToTitle" runat="server" Text="Send a Message to ###LOGINNAME###"/>
            </h2>
        </div>
        <div class="rfloat">
            <asp:HyperLink ID="lnkBack" runat="server" CssClass="back_btn" Text="Back" Visible="False" onClick="ShowLoading()">
            </asp:HyperLink>
        </div>
        <div class="clear"></div>
    </div>

    <div class="items_none" id="pnlSendMessageErr" runat="server" visible="false">
        <asp:Literal ID="lblSendMessageErr" runat="server"></asp:Literal>&nbsp;&nbsp;&nbsp;<img id="SiteRulesTIP" runat="server" visible="false" alt="" src="//cdn.goomena.com/Images2/payment/erwtimatiko.png" style="vertical-align:text-top;text-align:right;" />
        <p><asp:HyperLink ID="lnkSendMessageAltAction" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/SelectProduct.aspx" onclick="ShowLoading();"/></p>
        <div class="clear"></div>
    </div>

    <div class="t_wrap">
        <div class="send_msg t_to msg">

            <div class="left">
                <div class="message">
                    <div class="arrow">
                    </div>
                    <div class="msg_form">
                        <p>
                            <asp:Label ID="lblYourMsgSubject" runat="server" Text="Subject:">
                            </asp:Label>
                        </p>
                        <asp:Panel ID="pnlSubjectErr" runat="server" CssClass="alert alert-danger" Visible="False" ViewStateMode="Disabled">
                            <asp:Label ID="lblSubjectErr" runat="server" Text=""></asp:Label>
                        </asp:Panel>
                        <dx:ASPxTextBox ID="txtMessageSubject" runat="server" Width="170px">
                        </dx:ASPxTextBox>
                        <div class="clearboth big_padding">
                        </div>
                        <p>
                            <asp:Label ID="lblYourMsgBody" runat="server" Text="Your Message:" />
                        </p>
                        <dx:ASPxMemo ID="txtMessageText" runat="server" Rows="7" Width="170px"/>
                        <div id="Div2" class="alert alert-error hidden"></div>
                    </div>
                </div>
                <div class="msg_info">
                    <h2><asp:Label ID="lblYourFirstDateStartedTitle" runat="server" Text="Getting Your First Date Started"/></h2>
                        <asp:Label ID="lblYourFirstDateStartedText" runat="server" Text="" CssClass="bottom_text" />
                </div>
            </div>

            <div class="right">
                <div class="photo">
                    <asp:HyperLink ID="lnkProfImg" runat="server" onclick="ShowLoading();"><asp:Image ID="imgProf" runat="server" /></asp:HyperLink>
                </div>
                <div class="info">
                    <p>
                        <b><asp:Label ID="lblYourMsgFrom" runat="server" Text="From:">
                            </asp:Label></b></p>
                    <asp:HyperLink ID="lnkProfLogin" runat="server" 
                        Text=""  CssClass="loginNameRight" ClientSideEvents-Click="ShowLoadingOnMenu" />
                </div>
                <div class="clearboth padding">
                </div>
                <div class="photo">                                                
                    <asp:HyperLink ID="lnkOtherProfImg" runat="server" onclick="ShowLoading();"><asp:Image ID="imgOtherProf" runat="server" /></asp:HyperLink>
                </div>
                <div class="info">
                    <p>
                        <b><asp:Label ID="lblYourMsgTo" runat="server" Text="To:">
                            </asp:Label></b></p>
                        <asp:HyperLink ID="lnkOtherProfLogin" runat="server" Text=""  CssClass="loginNameRight" onclick="ShowLoading();" />
                </div>
                <div class="clearboth padding">
                </div>
                <div class="fdate" id="divDatingAmount" runat="server">
                    <asp:Label ID="lblDatingAmount" runat="server" Text=""/>
                </div>
            </div>

            <div class="clear"></div>
        </div>
    </div>

    <div class="form-actions">
        <asp:Button ID="btnSendMessage" runat="server" CssClass="btn btn-primary btn-large" Text="Send Message" UseSubmitBehavior="false" OnClientClick="ShowLoading();" />
    </div>
    <div class="clear"></div>
</div>
<script type="text/javascript">
    function onclickDisableSend() {
        $('#<%=btnSendMessage.ClientID %>').click(function () {
            $('#<%=btnSendMessage.ClientID %>').attr('disabled', 'disabled');
        })
    }
    onclickDisableSend();
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(onclickDisableSend);
</script>
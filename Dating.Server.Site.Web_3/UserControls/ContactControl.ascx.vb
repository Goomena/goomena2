﻿Imports DevExpress.Web.ASPxPopupControl
Imports Dating.Server.Core.DLL

Public Class ContactControl
    Inherits BaseUserControl



    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.ContactControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.ContactControl", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Public Property EmailSubject As String
    Public Property ScrollToElem As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try


            Try
                Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
                AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "Page_Load")
            End Try


            If (Not Me.IsPostBack) Then

                If (Not String.IsNullOrEmpty(EmailSubject)) Then txtSubject.Text = EmailSubject
                LoadLAG()

                If (Me.MasterProfileId > 0) Then
                    lblYouremailRead.Text = Me.GetCurrentProfile().eMail
                    lblYouremailRead.Visible = True
                    txtYouremail.Visible = False

                    liYourName.Visible = False
                    'lblYourNameRead.Visible = False
                    'txtYourName.Visible = False


                    'Dim fname As String = Me.GetCurrentProfile().FirstName
                    'Dim lname As String = Me.GetCurrentProfile().LastName
                    'If (Not String.IsNullOrEmpty(fname) AndAlso Not String.IsNullOrEmpty(lname)) Then
                    '    lblYourNameRead.Text = fname & " " & lname
                    '    lblYourNameRead.Visible = True
                    '    txtYourName.Visible = False
                    'End If

                End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub



    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try
            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = Me.CurrentPageData.cPageBasic

            lbYouremail.Text = Me.CurrentPageData.GetCustomString("lbYouremail")
            lbYourName.Text = Me.CurrentPageData.GetCustomString("lbYourName")
            lbSubject.Text = Me.CurrentPageData.GetCustomString("lbSubject")
            lbMessage.Text = Me.CurrentPageData.GetCustomString("lbMessage")

            cmdSend.Text = Me.CurrentPageData.GetCustomString("cmdSend")
            cmdReset.Value = Me.CurrentPageData.GetCustomString("cmdReset")
            'linkReportAbuse.Text = Me.CurrentPageData.GetCustomString("linkReportAbuse")

            'AppUtils.setNaviLabel(Me, "lbPublicNavi2Page", cPageBasic.PageTitle)
            If (TypeOf Page.Master Is INavigation) Then
                CType(Page.Master, INavigation).AddNaviLink(Me.CurrentPageData.Title, "")
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Function ValidateInput() As Boolean
        Dim isvalid = True
        Try

            If (String.IsNullOrEmpty(lblYouremailRead.Text)) Then
                pnlYouremailErr.Visible = False
                Dim regMatch As Boolean
                If (clsConfigValues.Get__validate_email_address_normal()) Then
                    regMatch = Regex.IsMatch(txtYouremail.Text, AppUtils.gEmailAddressValidationRegex)
                Else
                    regMatch = Regex.IsMatch(txtYouremail.Text, AppUtils.gEmailAddressValidationRegex_Simple)
                End If
                If (txtYouremail.Text.Trim() = "" OrElse Not regMatch) Then
                    pnlYouremailErr.Visible = True
                    lbYouremailErr.Text = CurrentPageData.GetCustomString("txtYouremail_Validation_ErrorText")
                    txtYouremail.Focus()
                    isvalid = False
                End If
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return isvalid
    End Function


    Protected Sub cmdSend_Click(sender As Object, e As EventArgs) Handles cmdSend.Click
        If (Not ValidateInput()) Then Return

        Try
            Dim messageText As String = <message><![CDATA[
Contact message from [SITE_NAME]

From: ###SENDEREMAIL###
Name: ###SENDERNAME###
Subject: ###SUBJECT###
Text: ###MESSAGEBODY###
]]></message>.Value

            messageText = messageText.Replace("[SITE_NAME]", ConfigurationManager.AppSettings("gSiteName"))

            If (Me.MasterProfileId > 0) Then
                messageText = messageText.Replace("###SENDEREMAIL###", lblYouremailRead.Text)
                messageText = messageText.Replace("###SENDERNAME###", Me.SessionVariables.MemberData.LoginName)
            Else
                messageText = messageText.Replace("###SENDEREMAIL###", txtYouremail.Text)
                messageText = messageText.Replace("###SENDERNAME###", txtYourName.Text)
            End If

            messageText = messageText.Replace("###MESSAGEBODY###", txtMessage.Text)


            If (Not String.IsNullOrEmpty(txtSubject.Text.Trim())) Then
                messageText = messageText.Replace("###SUBJECT###", txtSubject.Text)
            Else
                messageText = messageText.Replace("###SUBJECT###", "")
            End If

            Dim subject As String = txtSubject.Text
            If (String.IsNullOrEmpty(txtSubject.Text.Trim())) Then
                subject = "Contact message from [SITE_NAME]"
                subject = subject.Replace("[SITE_NAME]", ConfigurationManager.AppSettings("gSiteName"))
                txtSubject.Text = subject
            End If


            Try

                '                Dim systemInfo As String = <sql><![CDATA[
                '<table>
                '    <tr><td colspan="2"><hr></td></tr>
                '    <tr><td align="right"><strong>IP:</strong></td><td>[REMOTE_ADDR]</td></tr>
                '    <tr><td align="right"><strong>IP GEO:</strong></td><td>[IP_GEO]</td></tr>
                '    <tr><td align="right"><strong>User Agent:</strong></td><td>[HTTP_USER_AGENT]</td></tr>
                '    <tr><td align="right"><strong>HTTP Referer:</strong></td><td>[HTTP_REFERER]</td></tr>
                '</table>
                ']]></sql>.Value
                Dim systemInfo As String = <sql><![CDATA[

------------------------------
System Info

IP: [REMOTE_ADDR]
IP GEO: [IP_GEO]
LAGID: [LAGID]
User Agent: [HTTP_USER_AGENT]

]]></sql>.Value

                systemInfo = systemInfo.
                    Replace("[REMOTE_ADDR]", Request.ServerVariables("REMOTE_ADDR")).
                    Replace("[IP_GEO]", Session("GEO_COUNTRY_CODE")).
                    Replace("[HTTP_USER_AGENT]", Request.Params("HTTP_USER_AGENT")).
                    Replace("[LAGID]", Session("LAGID"))

                messageText = messageText & systemInfo
                'Replace("[HTTP_REFERER]", Request.ServerVariables("HTTP_REFERER")).
            Catch
            End Try



            'Dim sendToEmailAddress As String = ConfigurationManager.AppSettings("gToEmail")
            Dating.Server.Core.DLL.clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), txtSubject.Text, messageText)

            pnlSendMessageForm.Visible = False
            pnlSendMessageWrap.Visible = True
            lblMessageSent.Text = CurrentPageData.GetCustomString("MessageSentSuccessfully")
            ScrollToElem = lblMessageSent.ClientID
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
            lblMessageSent.Text = ShowUserErrorMsg(CurrentPageData.GetCustomString("MessageSendFailed"))
            ScrollToElem = lblMessageSent.ClientID
        End Try
    End Sub


End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SearchControl.ascx.vb" 
    Inherits="Dating.Server.Site.Web.SearchControl" %>
<script type="text/javascript">
    var animating_elemnts_list = new Array();
    var img_zodiacs = new Array();
</script>
<div id="<%= Mybase.UsersListView.toString() %>">
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">

    <asp:View ID="View1" runat="server">
    </asp:View>
    
    
    <asp:View ID="vwNoPhoto" runat="server" EnableViewState="false">
        <asp:FormView ID="fvNoPhoto" runat="server">
        <ItemTemplate>
<div id="ph-edit" class="no-photo">
    <div class="items_none items_hard">
            <div class="items_none_wrap">
        <div class="items_none_text">
            <%# Eval("HasNoPhotosText")%>
        </div>
        <div class="search_members_button_right"><asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn lighter" NavigateUrl="~/Members/Photos.aspx" onclick="ShowLoading();"><%# Eval("AddPhotosText")%><i class="icon-chevron-right"></i></asp:HyperLink></div>
        <div class="clear"></div>
    </div>
    </div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>
    
    
    <asp:View ID="vwNoOffers" runat="server">
        <asp:FormView ID="fvNoOffers" runat="server" EnableViewState="false">
        <ItemTemplate>
            <div class="items_none">
                <div id="offers_icon" class="middle_icon"></div>
                <div class="items_none_text">
                    <%# Eval("NoOfferText")%>
	                <div class="clear"></div>
                    <div class="search_members_button">
                    <asp:HyperLink ID="lnk4" runat="server" NavigateUrl="~/Members/Search.aspx"><%# Eval("SearchOurMembersText")%><i class="icon-chevron-right"></i></asp:HyperLink>
                    </div>
                </div>
            </div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>


    <asp:View ID="vwSearchingOffers" runat="server">
<div id="search-results">
<asp:Repeater ID="rptSearch" runat="server">
<ItemTemplate>
<div class="s_item <%# Eval("AddItemCssClass")%>" login="<%# Eval("OtherMemberLoginName")%>">
    <div class="left" ID="dLeft" runat="server" EnableViewState="false">
        <div class="photo">
            <div class="pr-photo-116-noshad <%# Eval("PhotoCssClass")%>">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>'><img src="<%# Eval("OtherMemberImageUrl") %>" class="round-img-116" /></asp:HyperLink>
            </div>
            <div class="tooltip tooltip_search_photo">
                <div class="tooltip_text left-arrow"><%# Eval("PhotosCountString")%> </div>
            </div>
        </div>
        <div class="info">
            <h2><asp:HyperLink ID="hl" runat="server" CssClass="login-name" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>' /></h2>
            <%--<p><%# Eval("OtherMemberHeading")%></p>--%>
            <div class="stats" style="position:relative;">
                <%# MyBase.WriteSearch_MemberInfo(Container.DataItem)%>
                <div class="tooltip tooltip_search_ZodiacName">
                    <div class="tooltip_text w-arrow"><%# Eval("ZodiacString")%></div>
                </div>
            </div>
        </div>
    </div>
    <div class="right <%# Eval("OtherMemberGenderClass")%>">
        <div class="indicators">
            <asp:Panel runat="server" ID="pnlIsOnline" EnableViewState="false" Visible='<%# MyBase.ShowIsOnline(Container.DataItem)%>' class="rfloat">
                <div id="pnlIsOnline<%# Eval("OtherMemberProfileID") %>" 
                    class="online-indicator <%# if(Eval("OtherMemberIsVip").ToString()="True","with-vip","") %><%# If(Eval("OtherMemberIsOnlineRecently").ToString() = "True", " online-recent", "")%>"><asp:Image ID="imgOn" runat="server" ImageUrl="//cdn.goomena.com/Images/spacer10.png" AlternateText='<%# Eval("OtherMemberIsOnlineText") %>' ToolTip='<%# Eval("OtherMemberIsOnlineText") %>' /><asp:Label
                                    ID="lblOnline" runat="server" Text='<%# Eval("OtherMemberIsOnlineText") %>'></asp:Label>
                </div>
                <script type="text/javascript">
                   if('<%# Eval("OtherMemberIsOnline").ToString() %>' == 'True') animating_elemnts_list.push('pnlIsOnline<%# Eval("OtherMemberProfileID") %>');
                </script>
            </asp:Panel>
            <div class="clear"></div>
        </div>
        <div id="info_icons" class="info_icons" runat="server" EnableViewState="false" Visible='<%# Eval("OtherMemberShowInfoIcons") %>'>
            <div id="pnlStatus" class="rfloat status_info info-item" runat="server" Visible='<%# if(not string.isnullOrEmpty(Eval("OtherMemberRelationshipTooltip")),true,false) %>'>
                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("OtherMemberRelationshipClass")%>" />
                <div class="tooltip tooltip_status_info">
                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberRelationshipTooltip")%></div>
                </div>
            </div>
            <div id="pnlHair" class="rfloat hair info-item" runat="server" Visible='<%# if(not string.isnullOrEmpty(Eval("OtherMemberHairClass")),true,false) %>'>
                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("OtherMemberHairClass")%>" />
                <div class="tooltip tooltip_hair">
                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberHairTooltip")%></div>
                </div>
            </div> 
            <div id="pnlWantVisit" class="rfloat want-visit info-item" runat="server" Visible='<%# if(not string.isnullOrEmpty(Eval("OtherMemberWantVisitText")),true,false) %>'>
                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled" />
                <div class="tooltip tooltip_want_visit">
                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberWantVisitText")%></div>
                </div>
            </div> 
            <div id="pnlWantJob" class="rfloat want-job info-item" runat="server" Visible='<%# if(not string.isnullOrEmpty(Eval("OtherMemberWantJobTooltip")),true,false) %>'>
                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled" />
                <div class="tooltip tooltip_want_job">
                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberWantJobTooltip")%></div>
                </div>
            </div> 
            <div class="rfloat zodiac_info">
                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("ZodiacName")%>" />
                <div class="tooltip tooltip_zodiac">
                    <div class="tooltip_text right-arrow"><%# Eval("ZodiacString")%></div>
                </div>
            </div> 
            <div id="pnlDrink" class="rfloat drinking info-item" runat="server" Visible='<%# if(not string.isnullOrEmpty(Eval("OtherMemberDrinkingText")),true,false) %>'>
                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("OtherMemberDrinkingClass")%>" />
                <div class="tooltip tooltip_drinking">
                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberDrinkingText")%></div>
                </div>
            </div> 
            <div id="pnlSmoke" class="rfloat smoking info-item" runat="server" Visible='<%# if(not string.isnullOrEmpty(Eval("OtherMemberSmokingText")),true,false) %>'>
                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("OtherMemberSmokingClass")%>" />
                <div class="tooltip tooltip_smoking">
                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberSmokingText")%></div>
                </div>
            </div>
            <div id="pnlBreastSz" class="rfloat breast info-item" runat="server" Visible='<%# if(not string.isnullOrEmpty(Eval("OtherMemberBreastSizeTooltip")),true,false) %>'>
                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" class="tt_enabled <%# Eval("OtherMemberBreastSizeClass")%>" />
                <div class="tooltip tooltip_breast">
                    <div class="tooltip_text right-arrow"><%# Eval("OtherMemberBreastSizeTooltip")%></div>
                </div>
            </div> 
            <div id="pnlVip" class="rfloat vip_info info-item" runat="server" Visible='<%# Eval("OtherMemberIsVip") %>'>
                <img alt="" src="//cdn.goomena.com/Images/spacer10.png" title="" class="vip-png" />
            </div>
            <%--
             --%>
        </div>
        <div class="bot_right_actions">
            <div class="indicators-bottom rfloat">
                <asp:Panel runat="server" ID="divTooltipholder" EnableViewState="false" 
                    CssClass="rfloat icon_tooltipholder"
                    Visible='<%# Eval("AllowTooltipPopupSearch")%>'>
                    <asp:LinkButton ID="lnkWhatIs" runat="server" OnClientClick="OnMoreInfoClick('[contentUrl]',this,'[headerText]');return false;"> <img runat="server" id="icon_tip" src="//cdn.goomena.com/Images/spacer102.png" alt="" class="questmark"/></asp:LinkButton>
                </asp:Panel>
                <div class="clear"></div>
            </div>
            <div class="btn-group" ID="ActionsMenu" runat="server" Visible='<%# Eval("AllowActionsMenu") %>'>
                <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#"><%# Eval("ActionsText")%> <span class="caret"></span></a>
                <ul class="dropdown-menu right-menu">
                    <li ID="liActsSendMsgOnce" runat="server" visible='<%# Eval("AllowActionsSendMessageOnce")%>' EnableViewState="false"><asp:HyperLink 
                            ID="lnkSendMsgOnce" runat="server" NavigateUrl='<%# Eval("SendMessageUrlOnce")%>' 
                            CssClass="btn"><%# Eval("SendMessageOnceText")%></asp:HyperLink></li>

                    <li ID="liActsSendMsgMany" runat="server" visible="false" EnableViewState="false"><asp:HyperLink 
                            ID="lnkSendMsgMany" runat="server"  NavigateUrl='<%# Eval("SendMessageUrlMany")%>' 
                            CssClass="btn"><%# Eval("SendMessageManyText")%></asp:HyperLink></li>

                    <li ID="liActsUnlock" runat="server" visible='<%# Eval("AllowActionsUnlock")%>' EnableViewState="true"><asp:LinkButton 
                            ID="lnkActsUnlock" runat="server" 
                            CommandName="UNLOCKOFFER" CommandArgument='<%# Eval("OfferId") %>' 
                            CssClass="btn"><%# Eval("ActionsUnlockText")%></asp:LinkButton></li>

                    <li ID="liActsMakeOffer" runat="server" visible='<%# Eval("AllowActionsCreateOffer") %>' EnableViewState="false"><asp:HyperLink 
                            ID="lnkNewMakeOffer2" runat="server"  NavigateUrl='<%# Eval("CreateOfferUrl") %>'
                            CssClass="btn"><span><%# Eval("ActionsMakeOfferText")%></span></asp:HyperLink></li>

                    <li ID="liActsSendMsg" runat="server" visible='<%# Eval("AllowActionsSendMessage")%>' EnableViewState="false"><asp:HyperLink 
                            ID="lnkSendMsg2" runat="server" NavigateUrl='<%# Eval("SendMessageUrl")%>'
                            CssClass="btn"><%# Eval("SendMessageText")%></asp:HyperLink></li>

                    <li ID="liActsDelOffr" runat="server" visible='<%# Eval("AllowActionsDeleteOffer")%>' EnableViewState="true"><asp:LinkButton 
                            ID="lnkDelOffr" runat="server" 
                            CommandName="DELETEOFFER" CommandArgument='<%# Eval("OfferId") %>' OnClientClick='<%#  Eval("OfferDeleteConfirmMessage")  %>'
                            CssClass="btn"><%# Eval("DeleteOfferText")%></asp:LinkButton></li>
                </ul>
            </div>
            <div class="rfloat">
                <asp:LinkButton ID="lnkUnblock" runat="server" CssClass="btn lnkUnblock" 
                EnableViewState="true"
                Visible='<%# Eval("AllowUnblock") %>' CommandArgument='<%# Eval("OtherMemberProfileID") %>' 
                CommandName="UNBLOCK"><%# Eval("UnblockText")%></asp:LinkButton>
            </div>
            <div class="rfloat">
                <asp:LinkButton ID="lnkUnFavorite" runat="server" CssClass="btn lnkUnFavorite" 
                EnableViewState="true" 
                Visible='<%# Eval("AllowUnfavorite") %>' CommandArgument='<%# Eval("OtherMemberProfileID") %>' 
                CommandName="UNFAVORITE"><%# Eval("UnfavoriteText")%></asp:LinkButton>
            </div>
            <div class="rfloat">
                <asp:LinkButton ID="hlFav" runat="server" CssClass="btn lnkFavorite"
                EnableViewState="true" 
                Visible='<%# Eval("AllowFavorite") %>' CommandArgument='<%# Eval("OtherMemberProfileID") %>' 
                CommandName="FAVORITE"><%# Eval("FavoriteText")%></asp:LinkButton>
            </div>
            <div class="rfloat">
                <asp:HyperLink ID="lnk1" runat="server" EnableViewState="false" Visible='<%# Eval("AllowSendMessage")%>' 
                    CssClass="btn lnkSendMessage lighter"
                    NavigateUrl='<%# Eval("SendMessageUrl")%>'><%# Eval("SendMessageText")%><i class="icon-chevron-right"></i></asp:HyperLink>
            </div>
            <div class="rfloat">
                <asp:LinkButton ID="lnkWink" runat="server" 
                    EnableViewState="true" 
                    CssClass="btn lnkWink" Style="margin:0;" 
                    Visible='<%# Eval("AllowWink") %>' CommandArgument='<%# Eval("OtherMemberProfileID") %>' CommandName="WINK"><%# Eval("WinkText")%></asp:LinkButton>
            </div>
            <div class="rfloat">
                <asp:LinkButton ID="lnkUnWink" runat="server" 
                    EnableViewState="true" 
                    CssClass="btn lnkUnWink disabled opacity70"
                    Visible='<%# Eval("AllowUnWink") %>' Enabled="false" CommandArgument='<%# Eval("OtherMemberProfileID") %>' CommandName="UNWINK"><%# Eval("WinkText")%></asp:LinkButton>
            </div>
            <div ID="divDistance" class='<%# "tt_enabled distance " & Eval("DistanceCss") %>' runat="server" EnableViewState="false"></div>
            <div class="tooltip tooltip_search">
                <div class="tooltip_text right-arrow"><%# Eval("MilesAwayText")%></div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
</ItemTemplate>
</asp:Repeater>
</div>
    </asp:View>


</asp:MultiView>


<script type="text/javascript">
    function LoadSearch() {

        $(".vip-png", '#search-results').tooltip({
            tooltipClass: "tooltip-VIP",
            content: "<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString(Me.VIPTooltip) %>",
            show: { delay: 1500 }
        });
        $("img.round-img-116", '#search-results').mouseenter(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('a', '.info h2').addClass('hovered')
            }
        }).mouseleave(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('a', '.info h2').removeClass('hovered')
            }
        });
        $("a.login-name", '#search-results').mouseenter(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('.pr-photo-116-noshad', '.photo').addClass('hovered')
            }
        }).mouseleave(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('.pr-photo-116-noshad', '.photo').removeClass('hovered')
            }
        });



        var scope = "#offers-list .l_new", scope2;
        scope = (($(".l_pending", "#offers-list").length > 0) ? "#offers-list .l_pending" : scope);
        scope = (($(".l_rejected", "#offers-list").length > 0) ? "#offers-list .l_rejected" : scope);
        scope = (($(".l_accepted", "#offers-list").length > 0) ? "#offers-list .l_accepted" : scope);
        scope2 = (($(".s_item", "#MyBlockedList").length > 0) ? "#MyBlockedList .s_item" : scope);

        var itms = $("a.btn", scope + " .left").css("width", "auto");
        if (itms.length == 0) { itms = $("a.btn", scope2 + " .bot_right_actions").css("width", "auto"); }
        for (var c = 0; c < itms.length; c++) {
            var w = $(itms[c]).width();
            $(itms[c]).css("width", "").removeClass("lnk130").removeClass("lnk180");
            if (w < 120)
                $(itms[c]).addClass("lnk130");
            else
                $(itms[c]).addClass("lnk180");
        }

        itms = $("a.dropdown-toggle", scope + " .left .btn-group").css("width", "auto");
        for (c = 0; c < itms.length; c++) {
            var w = $(itms[c]).width();
            $(itms[c]).css("width", "").removeClass("lnk130").removeClass("lnk180").removeClass("lnk107").removeClass("lnk116");
            if (w < 83)
                $(itms[c]).addClass("lnk107");
            else
                $(itms[c]).addClass("lnk116");
        }
    }

    function startAnimating() {
        if (animating_elemnts_list.length > 0) { setTimeout(startAnimateOnlineImg, 500); }
    }
    function processAnimating() {
        try {
            animating_elemnts_list = new Array();
            var online = $('.online-indicator', '#search-results');
            if (online.length > 0) {
                for (var c = 0; c < online.length; c++) {
                    animating_elemnts_list.push(online[c].id)
                }
                setTimeout(startAnimateOnlineImg, 500);
            }
        }
        catch (e) { }
    }
    (function () {
        LoadSearch()
        startAnimating();
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(processAnimating);
    })();
</script>

</div>












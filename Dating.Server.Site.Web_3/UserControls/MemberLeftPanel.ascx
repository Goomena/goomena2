﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MemberLeftPanel.ascx.vb" Inherits="Dating.Server.Site.Web.MemberLeftPanel" %>
<%@ Register assembly="DevExpress.Web.v13.1, Version=13.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" 
    namespace="DevExpress.Web.ASPxDataView" tagprefix="dx" %>
<style>
.carnival  #<%= pnlPhoto.ClientID %>{margin-top:60px;}
.me-female.mayday  #<%= pnlPhoto.ClientID %>{margin-top:22px;}
.me-female.mayday  #<%= pnlPhoto.ClientID %> .pr-photo-181{background-color: transparent;}
</style>
<script type="text/javascript">
    var imgPhotoID = '<%= imgPhoto.ClientID %>';
</script>
<asp:Panel ID="pnlPhoto" runat="server" CssClass="profile-self my-photo" style="">
    <div class="pr-photo-181">
        <img id="imgPhotoOverlM" src="//cdn.goomena.com/images2/profile/men-hat.png" class="male-mask" alt="" />
        <img id="imgPhotoOverlF" src="//cdn.goomena.com/images2/profile/mask.png" class="female-mask" alt="" />
        <asp:HyperLink ID="lnkPhoto" NavigateUrl="~/Members/Photos.aspx" runat="server" CssClass="profile-picture"  onClick="ShowLoading();">
            <img id="imgPhotoOverlFlowerF" src="//cdn.goomena.com/images2/profile/flower.png" class="mayday-female" alt="" />
            <asp:Image ID="imgPhoto" runat="server"  ImageUrl="~/Images/guy.jpg" CssClass="round-img-181" />
        </asp:HyperLink>
    </div>
</asp:Panel>
<asp:Panel ID="pnlProfile" runat="server" CssClass="profile-self my-login">
    <div class="sub_info">
        <div class="buttonWrap"></div>
        <asp:Label ID="ltrLogin" runat="server" Text="" Font-Size="16px" Font-Bold="True" CssClass="login-name" />
    </div>
</asp:Panel>

<asp:Panel ID="pnlProfile2" runat="server" CssClass="profile-self my-menu">
    <ul class="info">
        <li>
            <div class="list-item-inner"></div>
            <asp:HyperLink ID="lnkDashboard" runat="server" Text="Dashboard" 
                NavigateUrl="~/Members/Default.aspx" 
                onclick="ShowLoading();"
                CssClass="lnkDashboard icon" />
        </li>
        <li>
            <div class="list-item-inner"></div>
            <asp:HyperLink ID="lnkProfile" runat="server" Text="My Profile" NavigateUrl="~/Members/Profile.aspx"
                CssClass="lnkProfile icon" />
        </li>
        <li>
            <div class="list-item-inner"></div>
            <asp:HyperLink ID="lnkEditPhotos" runat="server" Text="Photos" 
                NavigateUrl="~/Members/Photos.aspx" 
                onclick="ShowLoading();"
                CssClass="lnkEditPhotos icon" />
        </li>
        <li ID="liLikes" runat="server">
            <div class="list-item-inner"></div>
            <asp:HyperLink ID="lnkLikes" runat="server" Text="Like" 
                NavigateUrl="~/Members/Likes.aspx?vw=likes" 
                onclick="ShowLoading();"
                CssClass="lnkLike icon" />
        </li>
        <li ID="liOffers" runat="server">
            <div class="list-item-inner"></div>
            <asp:HyperLink ID="lnkOffers" runat="server" Text="Offers" 
                NavigateUrl="~/Members/Offers3.aspx?vw=newoffers" 
                onclick="ShowLoading();"
                CssClass="lnkOffers icon" />
        </li>
        <li ID="liMessages" runat="server">
            <div class="list-item-inner"></div>
            <asp:HyperLink ID="lnkMessages" runat="server" Text="Messages" 
                NavigateUrl="~/Members/Messages.aspx" 
                onclick="ShowLoading();"
                CssClass="lnkMessages icon" />
        </li>
        <li ID="liDates" runat="server">
            <div class="list-item-inner"></div>
            <asp:HyperLink ID="lnkDates" runat="server" Text="Dates" 
                NavigateUrl="~/Members/Dates.aspx?vw=accepted" 
                onclick="ShowLoading();"
                CssClass="lnkDates icon" />
        </li>
        <li runat="server" id="liBilling">
            <div class="list-item-inner"></div>
            <asp:HyperLink ID="lnkBilling" runat="server" Text="Standard Member" 
                NavigateUrl="~/Members/SelectProduct.aspx" 
                onclick="ShowLoading();"
                CssClass="lnkBilling icon" />
        </li>
        <li>
            <div class="list-item-inner"></div>
            <asp:HyperLink ID="lnkNotifications" runat="server" Text="Dates" 
                NavigateUrl="~/Members/settings.aspx?vw=notifications" 
                onclick="ShowLoading();"
                CssClass="lnkNotifications icon" />
        </li>
          <li runat="server" id="liGiftCards">
            <div class="list-item-inner"></div>
            <asp:HyperLink ID="lnkGiftCards" runat="server" Text="Gift Cards" 
                NavigateUrl="~/Members/Coupon.aspx" 
                onclick="ShowLoading();"
                CssClass="lnkGiftCards icon" />
        </li>
        <%--<li runat="server" id="liBillingSubscription" visible="false">
            <div class="list-item-inner"></div>
            <asp:HyperLink ID="lnkBilling2" runat="server" Text="SelectProductSubscription" 
                NavigateUrl="~/Members/SelectProductSubscription.aspx" 
                onclick="ShowLoading();"
                CssClass="lnkBilling icon" />
        </li>--%>
    </ul>
    <div class="clear"></div>
    <div style="height:11px"></div>
</asp:Panel>

<asp:Panel ID="pnlQuickLinks" runat="server" CssClass="profile-self my-quick-links">
    <h3 class="quick-links-header">
        <span class="block lfloat">
            <asp:HyperLink ID="lblHeaderQuickLinks" runat="server" CssClass="quickLinksHeader" 
                NavigateUrl="~/Members/MyLists.aspx?vw=whoviewedme"  onclick="ShowLoading();" />
        </span>
        <span class="block rfloat">
            <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
        </span>
        <span class="clear"></span>
    </h3>
    
    <ul class="quick-links">
        <li ID="liWhoViewedMe" runat="server">
            <asp:HyperLink ID="lnkWhoViewedMe" runat="server" Text="Who Viewed Me" 
                NavigateUrl="~/Members/MyLists.aspx?vw=whoviewedme" 
                onclick="ShowLoading();"
                CssClass="lnkWhoViewedMe" />
        </li>
        <li ID="liWhoFavoritedMe" runat="server">
            <asp:HyperLink ID="lnkWhoFavoritedMe" runat="server" Text="Who Favorited Me" 
                NavigateUrl="~/Members/MyLists.aspx?vw=whofavoritedme" 
                onclick="ShowLoading();"
                CssClass="lnkWhoFavoritedMe" />
        </li>
        <li ID="liWhoSharedPhotos" runat="server" class="rows2">
            <asp:HyperLink ID="lnkWhoSharedPhotos" runat="server" Text="Who Shared photos" 
                NavigateUrl="~/Members/MyLists.aspx?vw=whosharedphotos" 
                onclick="ShowLoading();"
                CssClass="lnkWhoSharedPhotos" />
        </li>
        <li ID="liSharedPhotosWithWhom" runat="server" class="rows2">
            <asp:HyperLink ID="lnkSharedPhotosWithWhom" runat="server" Text="To whom have i given photo access" 
                NavigateUrl="~/Members/MyLists_sharedphotosbyme.aspx" 
                onclick="ShowLoading();"
                CssClass="lnkSharedPhotosWithWhom" />
        </li>
        <li ID="liMyFavoriteList" runat="server">
            <asp:HyperLink ID="lnkMyFavoriteList" runat="server" Text="My Favorite List" 
                NavigateUrl="~/Members/MyLists.aspx?vw=myfavoritelist" 
                onclick="ShowLoading();" />
        </li>
        <li ID="liMyBlockedList" runat="server">
            <asp:HyperLink ID="lnkMyBlockedList" runat="server" Text="My Blocked List" 
                NavigateUrl="~/Members/MyLists.aspx?vw=myblockedlist" 
                onclick="ShowLoading();" />
        </li>
        <li ID="liMyViewedList" runat="server">
            <asp:HyperLink ID="lnkMyViewedList" runat="server" Text="My Viewed List" 
                NavigateUrl="~/Members/MyLists.aspx?vw=myviewedlist" 
                onclick="ShowLoading();" />
        </li>
    </ul>
    <div class="clear">
    </div>
    <div style="height:44px"></div>
    <script type="text/javascript">
        function __fixQuickLinks () {
            var itms = $("a", "#<%= pnlQuickLinks.ClientID %>").css("width", "auto");
            for (var c = 0; c < itms.length; c++) {
                var o=$(itms[c]);
                var w = o.width();
                var h = o.height();
                if (w < 173 && h < 30) {
                    o.parent().removeClass("rows2").removeClass("with-number");
                }
                else if (w > 173 || h > 30) {
                    o.parent().removeClass("rows2").removeClass("with-number").addClass("rows2");
                }
                if ($('#notificationsCountWrapper', $(itms[c])).length > 0 && !o.parent().is(".with-number")) {
                    o.parent().addClass("with-number");
                }
            }
        }
        __fixQuickLinks();
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(__fixQuickLinks);
    </script>
</asp:Panel>


<div class="ld_block profile-self" id="divMembersNewestAll" runat="server">
	<h3>
        <span><asp:HyperLink ID="lnkMembersNewestAll" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEWEST" onclick="ShowLoading();"></asp:HyperLink><asp:HyperLink ID="lnkMembersNewest_InCountry" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEWESTINCOUNTRY" onclick="ShowLoading();" Visible="false"></asp:HyperLink></span>
    </h3>
	<div class="new-profiles">

<dx:ASPxDataView ID="dvNewest" runat="server" ColumnCount="1" 
    RowPerPage="10" DataSourceID="sdsNewest" 
    Width="220px" 
    EnableTheming="False" EnableDefaultAppearance="false" 
    ItemSpacing="0px" Layout="Flow">
    <ItemTemplate>
    <div class="m_item newest" onclick='<%# MyBase.GetOnClickJS(Eval("LoginName").Tostring()) %>'>
        <div class="pic-round-img-75 lfloat">
            <asp:HyperLink ID="lnkFromImage" runat="server" 
                NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'
                ImageUrl='<%# Dating.Server.Site.Web.AppUtils.GetImage(Eval("ProfileID").Tostring(),Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False, Me.IsHTTPS,Dating.Server.Core.DLL.PhotoSize.D150) %>' 
                onclick="ShowLoading();" />
        </div>
        <div class="lfloat login-link">
            <asp:HyperLink id="lnkFromLogin" runat="server" 
                Text='<%# Eval("LoginName") %>' 
                NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>' 
                CssClass="login-name"                
                onclick="ShowLoading();" />
               <%# evaluateAge(Eval("Birthday")) %>
            <div class="member_area_info">
                <%# Eval("City")%>, <br />
                <%# Eval("Region")%>, <%# Dating.Server.Core.DLL.ProfileHelper.GetCountryName(Eval("Country"))%>
            </div>
            
            <div id="divIsOnline" runat="server" class="online-indicator" 
                visible='<%# (Eval("IsOnlineNow").Tostring()="True") %>'><img src="//cdn.goomena.com/Images2/mbr2/online-cycle-small.png" alt="online" title="online" /><asp:Label
                ID="lblOnline" runat="server" Text="online"></asp:Label>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    </ItemTemplate>
        <Paddings Padding="0px" />
    <ItemStyle>
    <Paddings Padding="0px" />
    </ItemStyle>
        </dx:ASPxDataView>
	</div>
</div>
<asp:SqlDataSource ID="sdsNewest" runat="server" ConnectionString="<%$ ConnectionStrings:AppDBconnectionString %>"
    SelectCommand="GetNewestMembers" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="CurrentProfileId" Type="Int32" />
        <asp:Parameter DefaultValue="4" Name="ReturnRecordsWithStatus" Type="Int32" />
        <asp:Parameter DefaultValue="5" Name="NumberOfRecordsToReturn" Type="Int32" />
        <asp:Parameter Name="LastActivityUTCDate" Type="DateTime" />
    </SelectParameters>
</asp:SqlDataSource>

<div class="ld_block profile-self" id="divMembersNearAll" runat="server" visible="False">
	<h3>
        <span><asp:HyperLink ID="lnkMembersNearAll" runat="server" NavigateUrl="~/Members/Search.aspx?vw=NEAREST" onclick="ShowLoading();"></asp:HyperLink></span>
    </h3>
	<div class="d_winks_profiles">

<dx:ASPxDataView ID="dvNearest" runat="server" ColumnCount="1" 
    RowPerPage="10" Width="100%" 
    EnableTheming="False" EnableDefaultAppearance="false" 
    ItemSpacing="0px" Layout="Flow">
    <ItemTemplate>
    <div class="m_item newest">
        <div class="clear"></div>
        <div class="pic lfloat">
            <asp:HyperLink ID="lnkFromImage" runat="server"
                NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>'
                ImageUrl='<%# Dating.Server.Site.Web.AppUtils.GetImage(Eval("ProfileID").Tostring(), Eval("FileName").Tostring(), Eval("GenderId").Tostring(), False,Me.IsHTTPS,Dating.Server.Core.DLL.PhotoSize.D150) %>' 
                onclick="ShowLoading();" />
        </div>
        <asp:HyperLink ID="lnkFromLogin" runat="server" Text='<%# Eval("LoginName") %>' 
            NavigateUrl='<%# System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=") &  HttpUtility.UrlEncode(Eval("LoginName").Tostring()) %>' 
            CssClass="loginName lfloat"                
            onclick="ShowLoading();" />
    </div>
    <div class="clear"></div>
    </ItemTemplate>
        <Paddings Padding="0px" />
    <ItemStyle>
    <Paddings Padding="0px" />
    </ItemStyle>
        </dx:ASPxDataView>
	</div>
</div>

<asp:Panel ID="pnlBottom" runat="server" CssClass="profile-self" style="position:relative;" Visible="false">
    <ul class="info">
        <li ID="liHowWorksMale" runat="server" visible="false">
            <div class="buttonWrap"></div>
            <asp:HyperLink ID="lnkHowWorksMale" runat="server" Text="How it Works" 
                NavigateUrl="~/cmspage.aspx?pageid=185&title=HowItWorksman" 
                onclick="ShowLoading();"
                CssClass="lnkHowWorks" />
        </li>
        <li ID="liHowWorksFemale" runat="server" visible="false">
            <div class="buttonWrap"></div>
            <asp:HyperLink ID="lnkHowWorksFemale" runat="server" Text="How it Works" 
                NavigateUrl="~/cmspage.aspx?pageid=186&title=HowItWorksWoman" 
                onclick="ShowLoading();"
                CssClass="lnkHowWorks"  />
        </li>
        <li ID="liAffiliate" runat="server" visible="false">
            <div class="buttonWrap"></div>
            <asp:HyperLink ID="lnkAffiliate" runat="server" Text="Συνεργάτης μας" 
                NavigateUrl="~/Members/Affiliate.aspx" 
                onclick="ShowLoading();"
                CssClass="lnkAffiliate" />

        </li>
    </ul>

    <div class="clear">
    </div>
</asp:Panel>

    
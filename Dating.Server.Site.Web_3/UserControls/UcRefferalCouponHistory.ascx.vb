﻿Imports Dating.Server.Core.DLL

Public Class UcRefferalCouponHistory
    Inherits BaseUserControl


#Region "Props"


    Protected Overloads Property CurrentPageData As clsPageData
        Get
            ' let it read custom strings from container page
            'If (_pageData Is Nothing) Then _pageData = New clsPageData(Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UcRefferalCouponHistory", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
        Set(value As clsPageData)
            _pageData = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Not Page.IsPostBack) Then
                LoadLAG()
            End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
    End Sub

    Protected lblTooltipCashOutText As String = "Το Κουπόνι εξαργηρώθηκε στις:"
    Protected Sub LoadLAG()
        Try
            gv.Columns("DatetimeCreated").Caption = CurrentPageData.GetCustomString("DatetimeCreated")

            gv.Columns("CreditAmmount").Caption = CurrentPageData.GetCustomString("CreditAmmount")
            gv.Columns("CouponName").Caption = CurrentPageData.GetCustomString("CouponName")
            gv.Columns("CouponCode").Caption = CurrentPageData.GetCustomString("CouponCode")
            gv.Columns("CashedOut").Caption = CurrentPageData.GetCustomString("Active")
            lblPageTitle.Text = CurrentPageData.GetCustomString("lblPageTitle")
            lblTooltipCashOutText = CurrentPageData.GetCustomString("lblTooltipCashOut")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    Public Sub Bind()
        gv.DataBind()
    End Sub
    Protected Friend Function GetDateTOString(ByVal val As Object) As String
        Dim re As String = ""
        Try
            If val Is DBNull.Value OrElse val Is Nothing OrElse val.ToString = "" Then
                re = ""
            Else
                re = Format(val, "dd/MM/yyyy HH:mm")
            End If
        Catch ex As Exception
        End Try
        Return re
    End Function
    Protected Friend Function GetDbBoolean(ByVal val As Object) As Boolean
        Dim re As Boolean = False
        Try
            If val Is DBNull.Value OrElse val Is Nothing Then
                re = False
            Else
                re = val
            End If
        Catch ex As Exception
        End Try
        Return re
    End Function
    

    Protected Sub sdsCreditsHistory_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsCouponHistory.Selecting
        For Each prm As SqlClient.SqlParameter In e.Command.Parameters
            If (prm.ParameterName = "@ProfileId") Then
                prm.Value = Me.MasterProfileId
                Exit For
            End If
        Next
    End Sub




End Class
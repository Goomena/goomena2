﻿Public Class ucReferrerMenuTop
    Inherits BaseUserControl


    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.UCSelectProduct", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.UCSelectProduct", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If (Not Me.IsPostBack) Then
                LoadLAG()
                'LoadView()
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try

    End Sub


    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        LoadLAG()
        'LoadView()
    End Sub


    Public Sub LoadLAG() 'ByVal LAGID As String, ByVal NoCache As Boolean
        Try

            clsMenuHelper.PopulateMenuData(mnu1, "AffiliateMenuTop")
            For Each item As DevExpress.Web.ASPxMenu.MenuItem In mnu1.Items
                If (item.HasVisibleChildren) Then item.DropDownMode = True
            Next

        Catch ex As Exception
            AppUtils.ErrorMsgBox(ex, "")
        End Try

    End Sub

End Class
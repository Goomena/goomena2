﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL
Imports Dating.Server.Site.Web.OfferControl3
Imports DevExpress.Web.ASPxEditors

Public Class MessagesControlLeft
    Inherits BaseUserControl




    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/Messages.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Messages.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Public Property ShowNoPhotoText As Boolean
    Public Property ShowEmptyListText As Boolean

    Public Property UsersListView As MessagesListViewEnum
        Get
            If (Me.ViewState("MessagesListViewEnum") IsNot Nothing) Then Return Me.ViewState("MessagesListViewEnum")
            Return MessagesListViewEnum.None
        End Get
        Set(value As MessagesListViewEnum)
            Me.ViewState("MessagesListViewEnum") = value
        End Set
    End Property


    Private _list As List(Of clsMessageUserListItem)
    Public Property UsersList As List(Of clsMessageUserListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsMessageUserListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsMessageUserListItem))
            Me._list = value
        End Set
    End Property



    <PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    Public ReadOnly Property Repeater As Repeater
        Get
            If (UsersListView = MessagesListViewEnum.AllMessages) Then
                Return Me.rptNew

            ElseIf (UsersListView = MessagesListViewEnum.Inbox) Then
                Return Me.rptNew

            ElseIf (UsersListView = MessagesListViewEnum.NewMessages) Then
                Return Me.rptNew

            ElseIf (UsersListView = MessagesListViewEnum.SentMessages) Then
                Return Me.rptNew

            ElseIf (UsersListView = MessagesListViewEnum.Trash) Then
                Return Me.rptNew
            Else
                Return Nothing
            End If
        End Get
    End Property


    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/jquery-1.7.min.js")
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/bootstrap-dropdown.js")
    End Sub



    Public Overrides Sub DataBind()

     If (Me.ShowEmptyListText) Then

            Dim oNoOffer As New clsMessageUserListItem()
            If (UsersListView = MessagesListViewEnum.AllMessages) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("AcceptedOffersListEmptyText_ND")

            ElseIf (UsersListView = MessagesListViewEnum.Inbox) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("AcceptedOffersListEmptyText_ND")

            ElseIf (UsersListView = MessagesListViewEnum.NewMessages) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("NewOffersListEmptyText_ND")

            ElseIf (UsersListView = MessagesListViewEnum.SentMessages) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("WinksListEmptyText")

            ElseIf (UsersListView = MessagesListViewEnum.Trash) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("PokesListEmptyText")
            End If

            oNoOffer.SearchOurMembersText = Me.CurrentPageData.GetCustomString("SearchOurMembersText")

            Dim os As New List(Of clsMessageUserListItem)
            os.Add(oNoOffer)

        Else

        End If




        If (Me.Repeater IsNot Nothing) Then

            Try
                For Each itm As clsMessageUserListItem In Me.UsersList
                    itm.FillTextResourceProperties(Me.CurrentPageData)
                Next
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Me.Repeater.DataSource = Me.UsersList
            Me.Repeater.DataBind()
        End If

    End Sub


    Protected Sub lvMsgs_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptNew.ItemDataBound

        Try

            Dim itm As clsMessageUserListItem = e.Item.DataItem
            Dim hdfProfileID As HiddenField = CType(e.Item.FindControl("hdfProfileID"), HiddenField)

            Dim divTooltipholder As Panel = CType(e.Item.FindControl("divTooltipholder"), Panel)
            Dim lnkTooltip As HyperLink = CType(e.Item.FindControl("lnkTooltip"), HyperLink)
            Dim lnkLogin As ASPxHyperLink = CType(e.Item.FindControl("lnkLogin"), ASPxHyperLink)

            'Dim lblCommStat As Label = CType(e.Item.FindControl("lblCommStat"), Label)
            'Dim comStat As Long = 0
            'Dim comIsOpen = False
            'If (Not String.IsNullOrEmpty(lblCommStat.Text)) Then
            '    If (Long.TryParse(lblCommStat.Text, comStat)) Then
            '        If (comStat > 0) Then comIsOpen = True
            '    End If
            'End If

            'If (itm.MessagesView = MessagesViewEnum.NEWMESSAGES) Then
            '    If (Me.IsMale) Then

            '        If (hdfProfileID.Value = "1") Then
            '            lblCommStat.Text = ""
            '        ElseIf (comIsOpen) Then
            '            lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
            '            lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
            '            lblCommStat.CssClass = lblCommStat.CssClass & " open"
            '        Else
            '            lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationLocked")
            '            lblCommStat.CssClass = lblCommStat.CssClass.Replace("open", "")
            '            lblCommStat.CssClass = lblCommStat.CssClass & " closed"
            '        End If
            '    ElseIf (Me.IsFemale) Then

            '        lblCommStat.Text = ""
            '        If (comIsOpen) Then
            '            lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
            '            lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
            '            lblCommStat.CssClass = lblCommStat.CssClass & " open"
            '            'lblCommStat.ForeColor = Color.Green
            '        End If

            '    End If

            'ElseIf (itm.MessagesView = MessagesViewEnum.INBOX) Then
            '    If (Me.IsMale) Then

            '        'messages are read, don't display status of communication
            '        lblCommStat.Text = ""

            '    ElseIf (Me.IsFemale) Then

            '        lblCommStat.Text = ""
            '        If (comIsOpen) Then
            '            lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
            '            lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
            '            lblCommStat.CssClass = lblCommStat.CssClass & " open"
            '        End If

            '    End If

            'ElseIf (itm.MessagesView = MessagesViewEnum.TRASH) Then
            '    If (Me.IsMale) Then

            '        'messages are read, don't display status of communication
            '        lblCommStat.Text = ""

            '    ElseIf (Me.IsFemale) Then

            '        lblCommStat.Text = ""
            '        If (comIsOpen) Then
            '            lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
            '            lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
            '            lblCommStat.CssClass = lblCommStat.CssClass & " open"
            '        End If

            '    End If

            'Else

            '    lblCommStat.Text = ""

            '    If (hdfProfileID.Value = "1") Then
            '        lblCommStat.Text = ""
            '        'lblCommStat.ForeColor = Color.Green
            '    ElseIf (comIsOpen) Then
            '        lblCommStat.Text = CurrentPageData.GetCustomString("CommunicationOpen")
            '        lblCommStat.CssClass = lblCommStat.CssClass.Replace("closed", "")
            '        lblCommStat.CssClass = lblCommStat.CssClass & " open"
            '        'lblCommStat.ForeColor = Color.Green
            '    End If
            'End If

            'Dim lblOfferAmount As Label = CType(e.Item.FindControl("lblOfferAmount"), Label)
            'If (Not String.IsNullOrEmpty(lblCommStat.Text) AndAlso Not String.IsNullOrEmpty(lblOfferAmount.Text)) Then
            '    lblOfferAmount.Text = lblOfferAmount.Text & " / "
            'End If

            Dim lblReadStat As Label = CType(e.Item.FindControl("lblReadStat"), Label)
            Dim lblSentDate As ASPxLabel = CType(e.Item.FindControl("lblSentDate"), ASPxLabel)
            'If (itm.MessagesView = MessagesViewEnum.INBOX) Then
            '    Dim hdfMessageRead As HiddenField = CType(e.Item.FindControl("hdfMessageRead"), HiddenField)
            '    If (hdfMessageRead.Value = "read") Then
            '        lblReadStat.CssClass = "open"
            '    Else
            '        lblReadStat.CssClass = "closed"
            '    End If
            'Else
            '    lblReadStat.Visible = False
            '    Dim lblSeparatorReadDate = e.Item.FindControl("lblSeparatorReadDate")
            '    lblSeparatorReadDate.Visible = False
            'End If

            'Dim btnDelete As LinkButton = e.Item.FindControl("btnDeleteAllInConversation")
            'btnDelete.OnClientClick = String.Format(<js><![CDATA[return confirm('{0}');]]></js>.Value, CurrentPageData.GetCustomString("MessagesAllDeleteConfirmationText").Replace("'", "\'"))
            'btnDelete.OnClientClick = BasePage.ReplaceCommonTookens(btnDelete.OnClientClick, itm.OtherMemberLoginName)

            'Select Case itm.MessagesView
            '    Case MessagesViewEnum.INBOX
            '        btnDelete.OnClientClick = Regex.Replace(btnDelete.OnClientClick, "###VIEW###", CurrentPageData.GetCustomString("lnkInbox"))

            '    Case MessagesViewEnum.NEWMESSAGES
            '        btnDelete.OnClientClick = Regex.Replace(btnDelete.OnClientClick, "###VIEW###", CurrentPageData.GetCustomString("lnkNew"))

            '    Case MessagesViewEnum.SENT
            '        btnDelete.OnClientClick = Regex.Replace(btnDelete.OnClientClick, "###VIEW###", CurrentPageData.GetCustomString("lnkSent"))

            '    Case MessagesViewEnum.TRASH
            '        btnDelete.OnClientClick = String.Format(<js><![CDATA[return confirm('{0}');]]></js>.Value, CurrentPageData.GetCustomString("MessagesAllDeletePermanentlyConfirmationText").Replace("'", "\'"))
            '        btnDelete.OnClientClick = BasePage.ReplaceCommonTookens(btnDelete.OnClientClick, lnkLogin.Text)
            '        btnDelete.OnClientClick = Regex.Replace(btnDelete.OnClientClick, "###VIEW###", CurrentPageData.GetCustomString("lnkTrash"))

            'End Select

            'btnDelete.ToolTip = Me.CurrentPageData.GetCustomString("ListItemDeleteButtonText")


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Function GetCountMessagesString(ItemsCount As Object) As String

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 0) Then Return ItemsCount
            Catch ex As Exception

            End Try
        End If

        Return ""
    End Function


    'Protected Function GetLoginForCssClass(Login As Object) As String

    '    If (Not AppUtils.IsDBNullOrNothingOrEmpty(Login)) Then
    '        Try

    '            Login = CType(Login, String).Replace(" ", "")
    '        Catch
    '        End Try
    '    End If

    '    Return Login
    'End Function


    Protected Function IsVisible(ItemsCount As Object) As Boolean

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 1) Then Return True
            Catch ex As Exception

            End Try
        End If

        Return False
    End Function



    Protected Function Write_MemberInfo(DataItem As Dating.Server.Site.Web.clsMessageUserListItem) As String
        If (DataItem.OtherMemberProfileID = 1) Then Return ""

        Dim sb As New StringBuilder()
        Try
            sb.Append("<span class=""info2"">")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<strong>")
                sb.Append(DataItem.OtherMemberAge)
                sb.Append(" " & DataItem.YearsOldText & " ")
                sb.Append("</strong>")
            End If


            If (Not String.IsNullOrEmpty(DataItem.OtherMemberCity)) Then
                sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
                If (Not String.IsNullOrEmpty(DataItem.OtherMemberRegion)) Then sb.Append(",  ")
            End If

            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")
            sb.Append("</span>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


End Class
﻿Imports DevExpress.Web.ASPxPager
Imports Dating.Server.Core.DLL

Public Class DatesControl
    Inherits BaseUserControl




    Public Enum WinkUsersListViewEnum
        None = 0

        'LikesOffers = 12
        'NewOffers = 1
        'PendingOffers = 2
        'RejectedOffers = 3
        'AcceptedOffers = 4
        'PokesOffers = 14

        DatesOffers = 5

        'WhoViewedMeList = 6
        'WhoFavoritedMeList = 7
        'MyFavoriteList = 8
        'MyBlockedList = 9
        'MyViewedList = 10
    End Enum



    Public Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.DatesControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.DatesControl", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Public Property ShowNoPhotoText As Boolean
    Public Property ShowEmptyListText As Boolean

    Public Property UsersListView As WinkUsersListViewEnum
        Get
            If (Me.ViewState("WinkUsersListViewEnum") IsNot Nothing) Then Return Me.ViewState("WinkUsersListViewEnum")
            Return WinkUsersListViewEnum.None
        End Get
        Set(value As WinkUsersListViewEnum)
            Me.ViewState("WinkUsersListViewEnum") = value
        End Set
    End Property


    Private _list As List(Of clsWinkUserListItem)
    Public Property UsersList As List(Of clsWinkUserListItem)
        Get
            If (Me._list Is Nothing) Then Me._list = New List(Of clsWinkUserListItem)
            Return Me._list
        End Get
        Set(value As List(Of clsWinkUserListItem))
            Me._list = value
        End Set
    End Property



    <PersistenceMode(PersistenceMode.InnerDefaultProperty)>
    Public ReadOnly Property Repeater As Repeater
        Get
            'If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
            '    Return Me.rptAccepted

            'ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
            '    Return Me.rptNew

            'ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
            '    Return Me.rptNew

            'ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
            '    Return Me.rptNew

            'ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
            '    Return Me.rptPending

            'ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
            '    Return Me.rptRejected

            'Else
            If (UsersListView = WinkUsersListViewEnum.DatesOffers) Then
                Return Me.rptDates

            Else
                Return Nothing
            End If
        End Get
    End Property


    Protected Overrides Sub OnInit(e As System.EventArgs)
        MyBase.OnInit(e)
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/jquery-1.7.min.js")
        'ModGlobals.AddScriptResourceToHeader(Me.Page, "~/Scripts/bootstrap-dropdown.js")
    End Sub



    Public Overrides Sub DataBind()

        If (Me.ShowNoPhotoText) Then

            Dim oNoPhoto As New clsWinkUserListItem()
            oNoPhoto.HasNoPhotosText = Me.CurrentPageData.GetCustomString("HasNoPhotosText")
            oNoPhoto.AddPhotosText = Me.CurrentPageData.GetCustomString("AddPhotosText")

            Dim os As New List(Of clsWinkUserListItem)
            os.Add(oNoPhoto)

            fvNoPhoto.DataSource = os
            fvNoPhoto.DataBind()

            Me.MultiView1.SetActiveView(vwNoPhoto)


        ElseIf (Me.ShowEmptyListText) Then

            Dim oNoOffer As New clsWinkUserListItem()
            If (UsersListView = WinkUsersListViewEnum.DatesOffers) Then
                oNoOffer.NoOfferText = Me.CurrentPageData.GetCustomString("AcceptedOffersListEmptyText_ND")
            End If

            oNoOffer.SearchOurMembersText = Me.CurrentPageData.GetCustomString("SearchOurMembersText")

            Dim os As New List(Of clsWinkUserListItem)
            os.Add(oNoOffer)

            fvNoOffers.DataSource = os
            fvNoOffers.DataBind()

            Me.MultiView1.SetActiveView(vwNoOffers)

        Else

            'If (UsersListView = WinkUsersListViewEnum.AcceptedOffers) Then
            '    Me.MultiView1.SetActiveView(vwAcceptedOffers)

            'ElseIf (UsersListView = WinkUsersListViewEnum.NewOffers) Then
            '    Me.MultiView1.SetActiveView(vwNewOffers)

            'ElseIf (UsersListView = WinkUsersListViewEnum.LikesOffers) Then
            '    Me.MultiView1.SetActiveView(vwNewOffers)

            'ElseIf (UsersListView = WinkUsersListViewEnum.PokesOffers) Then
            '    Me.MultiView1.SetActiveView(vwNewOffers)

            'ElseIf (UsersListView = WinkUsersListViewEnum.PendingOffers) Then
            '    Me.MultiView1.SetActiveView(vwPendingOffers)

            'ElseIf (UsersListView = WinkUsersListViewEnum.RejectedOffers) Then
            '    Me.MultiView1.SetActiveView(vwRejectedOffers)

            'Else
            If (UsersListView = WinkUsersListViewEnum.DatesOffers) Then
                Me.MultiView1.SetActiveView(vwDatesOffers)

            Else
                Me.MultiView1.SetActiveView(View1)

            End If
        End If




        If (Me.Repeater IsNot Nothing) Then

            Try
                For Each itm As clsWinkUserListItem In Me.UsersList
                    itm.FillTextResourceProperties(Me.CurrentPageData)
                    If (itm.OtherMemberBirthday.HasValue AndAlso itm.OtherMemberBirthday.Value > DateTime.MinValue) Then
                        itm.ZodiacString = globalStrings.GetCustomString("Zodiac" & ProfileHelper.ZodiacName(itm.OtherMemberBirthday), itm.LAGID)
                    End If
                Next
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            Me.Repeater.DataSource = Me.UsersList
            Me.Repeater.DataBind()
        End If

    End Sub


    'Public Sub FillTextResourceProperties(itm As clsWinkUserListItem, pageData As clsPageData)

    '    If (String.IsNullOrEmpty(itm.HasNoPhotosText)) Then itm.HasNoPhotosText = pageData.GetCustomString("HasNoPhotosText")
    '    If (String.IsNullOrEmpty(itm.SearchOurMembersText)) Then itm.SearchOurMembersText = pageData.GetCustomString("SearchOurMembersText")
    '    If (String.IsNullOrEmpty(itm.YouReceivedWinkOfferText)) Then itm.YouReceivedWinkOfferText = pageData.GetCustomString("YouReceivedWinkText")
    '    If (String.IsNullOrEmpty(itm.WantToKnowFirstDatePriceText)) Then itm.WantToKnowFirstDatePriceText = pageData.GetCustomString("WantToKnowFirstDatePriceText")
    '    If (String.IsNullOrEmpty(itm.MakeOfferText)) Then itm.MakeOfferText = pageData.GetCustomString("MakeOfferText")
    '    If (String.IsNullOrEmpty(itm.AcceptText)) Then itm.AcceptText = pageData.GetCustomString("AcceptText")
    '    If (String.IsNullOrEmpty(itm.CounterText)) Then itm.CounterText = pageData.GetCustomString("CounterText")
    '    If (String.IsNullOrEmpty(itm.NotInterestedText)) Then itm.NotInterestedText = pageData.GetCustomString("NotInterestedText")
    '    If (String.IsNullOrEmpty(itm.TooFarAwayText)) Then itm.TooFarAwayText = pageData.GetCustomString("TooFarAwayText")
    '    If (String.IsNullOrEmpty(itm.NotEnoughInfoText)) Then itm.NotEnoughInfoText = pageData.GetCustomString("NotEnoughInfoText")
    '    If (String.IsNullOrEmpty(itm.DifferentExpectationsText)) Then itm.DifferentExpectationsText = pageData.GetCustomString("DifferentExpectationsText")
    '    If (String.IsNullOrEmpty(itm.YearsOldText)) Then itm.YearsOldText = pageData.GetCustomString("YearsOldText")

    '    If (String.IsNullOrEmpty(itm.DeleteOfferText)) Then itm.DeleteOfferText = pageData.GetCustomString("DeleteOfferText")
    '    If (String.IsNullOrEmpty(itm.DeleteWinkText)) Then itm.DeleteWinkText = pageData.GetCustomString("DeleteWinkText")

    '    If (String.IsNullOrEmpty(itm.SendMessageText)) Then itm.SendMessageText = pageData.GetCustomString("SendMessageText")

    '    If (String.IsNullOrEmpty(itm.WinkText)) Then itm.WinkText = pageData.GetCustomString("WinkText")
    '    If (String.IsNullOrEmpty(itm.UnWinkText)) Then itm.UnWinkText = pageData.GetCustomString("UnWinkText")

    '    If (String.IsNullOrEmpty(itm.FavoriteText)) Then itm.FavoriteText = pageData.GetCustomString("FavoriteText")
    '    If (String.IsNullOrEmpty(itm.UnfavoriteText)) Then itm.UnfavoriteText = pageData.GetCustomString("UnfavoriteText")

    '    If (String.IsNullOrEmpty(itm.YearsOldFromText)) Then itm.YearsOldFromText = pageData.GetCustomString("YearsOldFromText")
    '    If (String.IsNullOrEmpty(itm.TryWinkText)) Then itm.TryWinkText = pageData.GetCustomString("TryWinkText")
    '    If (String.IsNullOrEmpty(itm.MakeNewOfferText)) Then itm.MakeNewOfferText = pageData.GetCustomString("MakeNewOfferText")
    '    If (String.IsNullOrEmpty(itm.YourWinkSentText)) Then itm.YourWinkSentText = pageData.GetCustomString("YourWinkSentText")
    '    If (String.IsNullOrEmpty(itm.AwaitingResponseText)) Then itm.AwaitingResponseText = pageData.GetCustomString("AwaitingResponseText")
    '    If (String.IsNullOrEmpty(itm.AddPhotosText)) Then itm.AddPhotosText = pageData.GetCustomString("AddPhotosText")

    '    If (String.IsNullOrEmpty(itm.UnblockText)) Then itm.UnblockText = pageData.GetCustomString("UnblockText")
    '    If (String.IsNullOrEmpty(itm.PokeText)) Then itm.PokeText = pageData.GetCustomString("PokeText")
    '    If (String.IsNullOrEmpty(itm.ActionsText)) Then itm.ActionsText = pageData.GetCustomString("ActionsText")
    '    If (String.IsNullOrEmpty(itm.ActionsMakeOfferText)) Then itm.ActionsMakeOfferText = pageData.GetCustomString("ActionsMakeOfferText")
    '    If (String.IsNullOrEmpty(itm.CancelPokeText)) Then itm.CancelPokeText = pageData.GetCustomString("CancelPokeText")


    '    ''''''''''''''''''''''''''''
    '    'canceled/rejected
    '    ''''''''''''''''''''''''''''
    '    If (String.IsNullOrEmpty(itm.RejectText)) Then itm.RejectText = pageData.GetCustomString("RejectText")

    '    If (String.IsNullOrEmpty(itm.RejectDeleteConversationText)) Then
    '        itm.RejectDeleteConversationText = pageData.GetCustomString("RejectDeleteConversationText")
    '        itm.RejectDeleteConversationText = itm.ReplaceCommonTookens(itm.RejectDeleteConversationText)
    '    End If

    '    If (String.IsNullOrEmpty(itm.RejectBlockText)) Then
    '        itm.RejectBlockText = pageData.GetCustomString("RejectBlockText")
    '        itm.RejectBlockText = itm.ReplaceCommonTookens(itm.RejectBlockText)
    '    End If

    '    If (String.IsNullOrEmpty(itm.CancelWinkText)) Then itm.CancelWinkText = pageData.GetCustomString("CancelWinkText")
    '    If (String.IsNullOrEmpty(itm.CancelOfferText)) Then itm.CancelOfferText = pageData.GetCustomString("CancelOfferText")

    '    If (String.IsNullOrEmpty(itm.CancelledRejectedTitleText)) Then itm.CancelledRejectedTitleText = pageData.GetCustomString("WinkCancelledText")

    '    If (String.IsNullOrEmpty(itm.CancelledRejectedDescriptionText)) Then
    '        itm.CancelledRejectedDescriptionText = pageData.GetCustomString("YouCancelledWinkToText")
    '        itm.CancelledRejectedDescriptionText = itm.ReplaceCommonTookens(itm.CancelledRejectedDescriptionText)
    '    End If



    '    If (String.IsNullOrEmpty(itm.MilesAwayText)) Then
    '        itm.MilesAwayText = pageData.GetCustomString("MilesAwayText")
    '        itm.MilesAwayText = itm.ReplaceCommonTookens(itm.MilesAwayText)
    '    End If


    '    If (String.IsNullOrEmpty(itm.OfferDeleteConfirmMessage)) Then
    '        itm.OfferDeleteConfirmMessage = pageData.GetCustomString("OfferDeleteConfirmMessage")
    '        If (Not String.IsNullOrEmpty(itm.OfferDeleteConfirmMessage)) Then
    '            itm.OfferDeleteConfirmMessage = itm.OfferDeleteConfirmMessage.Replace("'", "\'")
    '            itm.OfferDeleteConfirmMessage = "return confirm('" & itm.OfferDeleteConfirmMessage & "');"
    '        End If
    '    End If


    '    If (String.IsNullOrEmpty(itm.YouReceivedPokeDescriptionText)) Then
    '        itm.YouReceivedPokeDescriptionText = pageData.GetCustomString("YouReceivedPokeDescriptionText")
    '        itm.YouReceivedPokeDescriptionText = itm.ReplaceCommonTookens(itm.YouReceivedPokeDescriptionText)
    '    End If


    '    If (String.IsNullOrEmpty(itm.WillYouAcceptDateWithForAmountText)) Then
    '        itm.WillYouAcceptDateWithForAmountText = pageData.GetCustomString("WillYouAcceptDateWithForAmountText")
    '        itm.WillYouAcceptDateWithForAmountText = itm.ReplaceCommonTookens(itm.WillYouAcceptDateWithForAmountText)
    '    End If


    '    If (String.IsNullOrEmpty(itm.OfferAcceptedHowToContinueText)) Then
    '        itm.OfferAcceptedHowToContinueText = pageData.GetCustomString("OfferAcceptedHowToContinueText")
    '        itm.OfferAcceptedHowToContinueText = itm.ReplaceCommonTookens(itm.OfferAcceptedHowToContinueText)
    '    End If

    '    If (String.IsNullOrEmpty(itm.OfferAcceptedUnlockText)) Then
    '        itm.OfferAcceptedUnlockText = pageData.GetCustomString("OfferAcceptedUnlockText")
    '        itm.OfferAcceptedUnlockText = itm.ReplaceCommonTookens(itm.OfferAcceptedUnlockText)
    '    End If

    '    If (String.IsNullOrEmpty(itm.ActionsUnlockText)) Then
    '        itm.ActionsUnlockText = pageData.GetCustomString("ActionsUnlockText")
    '        itm.ActionsUnlockText = itm.ReplaceCommonTookens(itm.ActionsUnlockText)
    '    End If

    '    If (String.IsNullOrEmpty(itm.UnlockNotice)) Then
    '        itm.UnlockNotice = pageData.GetCustomString("UnlockNotice")
    '        itm.UnlockNotice = itm.ReplaceCommonTookens(itm.UnlockNotice)
    '    End If

    '    If (itm.RejectedByProfileID = itm.OtherMemberProfileID) Then
    '        itm.RejectCssClass = "deal_right_rej"
    '    Else
    '        itm.RejectCssClass = "deal_left"
    '    End If


    '    ''''''''''''''''''''''''''''
    '    'accepted
    '    ''''''''''''''''''''''''''''
    '    If (String.IsNullOrEmpty(itm.MessageSentNotice)) Then itm.MessageSentNotice = pageData.GetCustomString("MessageSentNotice")

    '    If (String.IsNullOrEmpty(itm.ConversationText)) Then itm.ConversationText = pageData.GetCustomString("ConversationText")
    '    If (String.IsNullOrEmpty(itm.HistoryText)) Then itm.HistoryText = pageData.GetCustomString("HistoryText")

    '    If (String.IsNullOrEmpty(itm.CommunicationStatusText)) Then itm.CommunicationStatusText = pageData.GetCustomString("CommunicationStatusText")


    'End Sub

    Protected Function WriteOffer_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As Object
        'Dim str As String = "<ul>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberAge") & "</b> " & DataItem.YearsOldText") & "</li>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberCity") & ", " & DataItem.OtherMemberRegion") & "</b></li>" & vbCrLf & _
        '    "</ul>" & vbCrLf

        'Return str


        Dim sb As New StringBuilder()
        Try
            sb.Append("<ul>")
            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<b>" & DataItem.OtherMemberAge & "</b>")
                sb.Append(" " & DataItem.YearsOldText & " ")
            End If

            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("<b>")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
            '    sb.Append(DataItem.OtherMemberCity)
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(",  ")
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(DataItem.OtherMemberRegion)
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCountry) Then
            '    sb.Append(",  ")
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCountry) Then
            '    sb.Append(DataItem.OtherMemberCountry)
            'End If
            'sb.Replace(",  " & ",  ", ",  ")

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


    Protected Function WriteSearch_MemberInfo(DataItem As Dating.Server.Site.Web.clsWinkUserListItem) As Object

        'Dim str As String = "<ul>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberAge & "</b> " & DataItem.YearsOldText & " <b>" & Eval("OtherMemberCity & ", " & Eval("OtherMemberRegion & "</b></li>" & vbCrLf & _
        '    "<li><b>" & DataItem.OtherMemberHeight & " - " & DataItem.OtherMemberPersonType & "</b></li>" & vbCrLf & _
        '    "<li>" & DataItem.OtherMemberHair & ", " & DataItem.OtherMemberEyes & ", " & DataItem.OtherMemberEthnicity & "</li>" & vbCrLf & _
        '    "</ul>" & vbCrLf

        Dim sb As New StringBuilder()
        Try

            sb.Append("<ul>")

            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberAge) Then
                sb.Append("<b>" & DataItem.OtherMemberAge & "</b>")
                sb.Append(" " & DataItem.YearsOldText & " ")
            End If

            sb.Append("<b>")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCity), DataItem.OtherMemberCity, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberRegion), DataItem.OtherMemberRegion, ""))
            sb.Append(",  ")
            sb.Append(IIf(Not String.IsNullOrEmpty(DataItem.OtherMemberCountry), ProfileHelper.GetCountryName(DataItem.OtherMemberCountry), ""))
            sb.Replace(",  " & ",  ", ",  ")
            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) Then
            '    sb.Append(DataItem.OtherMemberCity)
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCity) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(",  ")
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) Then
            '    sb.Append(DataItem.OtherMemberRegion)
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberRegion) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCountry) Then
            '    sb.Append(",  ")
            'End If

            'If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberCountry) Then
            '    sb.Append(DataItem.OtherMemberCountry)
            'End If
            'sb.Replace(",  " & ",  ", ",  ")

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("<li>")
            sb.Append("<b>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) Then
                sb.Append(DataItem.OtherMemberHeight)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHeight) AndAlso Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(" - ")
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberPersonType) Then
                sb.Append(DataItem.OtherMemberPersonType)
            End If

            sb.Append("</b>")
            sb.Append("</li>")


            sb.Append("<li>")

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then
                sb.Append(DataItem.OtherMemberHair)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEyes)
            End If

            If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEthnicity) Then
                If Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberEyes) OrElse Not AppUtils.IsDBNullOrNothingOrEmpty(DataItem.OtherMemberHair) Then sb.Append(", ")
                sb.Append(DataItem.OtherMemberEthnicity)
            End If

            sb.Append("</li>")
            sb.Append("</ul>")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

        Return sb.ToString()
    End Function


    Protected Sub rptDates_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDates.ItemDataBound
        Try

            Dim lnkWhatIsUNL As LinkButton = e.Item.FindControl("lnkWhatIsUNL")
            If (lnkWhatIsUNL IsNot Nothing) Then
                lnkWhatIsUNL.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsUnlimitedText")
                lnkWhatIsUNL.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIsUNL.OnClientClick,
                                             ResolveUrl("~/Members/InfoWin.aspx?info=datesunlimited"),
                                             Me.CurrentPageData.GetCustomString("WhatIsUnlimitedText"))
            End If


            Dim lnkWhatIsLTD As LinkButton = e.Item.FindControl("lnkWhatIsLTD")
            If (lnkWhatIsLTD IsNot Nothing) Then
                lnkWhatIsLTD.ToolTip = Me.CurrentPageData.GetCustomString("WhatIsLimitedText")
                lnkWhatIsLTD.OnClientClick = WhatIsIt.OnMoreInfoClickFunc(lnkWhatIsLTD.OnClientClick,
                                             ResolveUrl("~/Members/InfoWin.aspx?info=dateslimited"),
                                             Me.CurrentPageData.GetCustomString("WhatIsLimitedText"))
            End If

            Dim lnkDelConv As LinkButton = e.Item.FindControl("lnkDelConv")
            If (lnkDelConv IsNot Nothing) Then
                Dim wrd As String = AppUtils.JS_PrepareAlertString(Me.CurrentPageData.GetCustomString("Warning_RejectDeleteConversationText"))
                wrd = BasePage.ReplaceCommonTookens(wrd, DirectCast(e.Item.DataItem, Dating.Server.Site.Web.clsWinkUserListItem).OtherMemberLoginName)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "key" & lnkDelConv.ClientID, "key" & lnkDelConv.ClientID & "='" & wrd & "';" & vbCrLf, True)
                lnkDelConv.OnClientClick = lnkDelConv.OnClientClick.Replace("{0}", "key" & lnkDelConv.ClientID)
            End If

        Catch ex As Exception

        End Try
    End Sub
End Class
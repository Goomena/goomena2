﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="UCPrice2.ascx.vb" Inherits="Dating.Server.Site.Web.Price2" %>

<asp:MultiView ID="mv" runat="server" ActiveViewIndex="0">

    <asp:View ID="vwPublic" runat="server">
<div class="product-tab <%= MyBase.CssClass%>"
    align="center">
    <div class="pricecol-credits"><%= MyBase.Credits%><div class="pricecol-credits-text"><%= MyBase.CreditsText%></div></div>
    <div class="pricecol-price<%= MyBase.SmallClass%>"><div class="pricecol-price-text"><%= MyBase.MoneySymb%><span style="font-size:14px;">&nbsp;</span><%= MyBase.PriceString%></div></div>
    <div class="pricecol-expire<%= MyBase.SmallClass%>"><div class="pricecol-expire-text"><asp:Literal ID="lblExpireAfterPub" runat="server">Expire after [DURATION] Days</asp:Literal></div></div>
&nbsp;
    <asp:ImageButton ID="imgCreditsP" runat="server" align="center"
        ImageUrl="//cdn.goomena.com/goomena-kostas/pricing/order-button.png" 
        CssClass="ordbut" 
        OnClientClick="ShowLoading();" />
</div>
    </asp:View>


    <asp:View ID="vwPublicRecom" runat="server">
<div class="product-tab <%= MyBase.CssClass%>" align="center">
    <div class="pricecol-credits"><%= MyBase.Credits%><div class="pricecol-credits-text"><%= MyBase.CreditsText%></div></div>
    <div class="pricecol-price<%= MyBase.SmallClass%>"><div class="pricecol-price-text"><%= MyBase.MoneySymb%><span style="font-size:14px;">&nbsp;</span><%= MyBase.PriceString%></div></div>
    <div class="pricecol-expire<%= MyBase.SmallClass%>"><div class="pricecol-expire-text"><asp:Literal ID="lblExpireAfterPubRecom" runat="server">Expire after [DURATION] Days</asp:Literal></div></div>
&nbsp;
    <asp:ImageButton ID="imgCreditsPR" runat="server" align="center"
        ImageUrl="//cdn.goomena.com/goomena-kostas/pricing/order-button.png" CssClass="ordbut" OnClientClick="ShowLoading();" />
</div>
    </asp:View>


    <asp:View ID="vwMember" runat="server">
        <div class="product-tab <%= MyBase.CssClass%>">
            <div class="pricecol"></div>       
            <div class="pricecol-credits"><%= MyBase.Credits%><div class="pricecol-credits-text"><%= MyBase.CreditsText%></div></div>
            <div class="pricecol-price<%= MyBase.SmallClass%>"><div class="pricecol-price-text"><%= MyBase.MoneySymb%><span style="font-size:14px;">&nbsp;</span><%= MyBase.PriceString%></div></div>

            <div class="pricecol-expire-wrap">
                <div class="pricecol-expire"><div class="pricecol-expire-text"><asp:Literal ID="lblExpireAfterMemb" runat="server">Expire after [DURATION] Days</asp:Literal></div></div>
                <div class="pricecol-expire-line2" style="display:none;"><div class="pricecol-expire-line2-text"><asp:Literal ID="lblDiscountAfterMemb" runat="server"></asp:Literal></div></div>
                <div class="pricecol-expire-line3" style="display:none;"><div class="pricecol-expire-line3-text"><asp:Literal ID="lblBestDealAfterMemb" runat="server">Best Deal</asp:Literal></div></div>
            </div>
            <asp:LinkButton  ID="imgCreditsM" runat="server" OnClientClick="ShowLoading();" CssClass="pricecol-order-btn">Order</asp:LinkButton>
        </div>
    </asp:View>


</asp:MultiView>

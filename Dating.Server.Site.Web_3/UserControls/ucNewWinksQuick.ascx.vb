﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors

Public Class ucNewWinksQuick
    Inherits BaseUserControl

    Private _BindControlDataComplete As Boolean

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.OfferControl", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Public Property ItemsPerPage As Integer
        Get
            If (Me.ViewState("ItemsPerPage") IsNot Nothing) Then Return Me.ViewState("ItemsPerPage")
            Return 10
        End Get
        Set(value As Integer)
            Me.ViewState("ItemsPerPage") = value
        End Set
    End Property




    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        BindControlData()
    End Sub

    Public Function BindControlData() As Integer
        Dim recordsFound As Integer = 0
        If (Not _BindControlDataComplete) Then
            Try
                Dim dtResults As DataTable = clsSearchHelper.GetLikesQuickDataTable(Me.MasterProfileId,
                                                                                 ProfileStatusEnum.Approved,
                                                                                 OffersSortEnum.RecentOffers,
                                                                                 Me.SessionVariables.MemberData.Zip,
                                                                                 Me.SessionVariables.MemberData.latitude,
                                                                                 Me.SessionVariables.MemberData.longitude,
                                                                                 clsSearchHelper.DISTANCE_DEFAULT,
                                                                                 Me.ItemsPerPage)
                dvWinks.DataSource = dtResults
                dvWinks.DataBind()

                If (dtResults IsNot Nothing) Then
                    recordsFound = dtResults.Rows.Count
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            _BindControlDataComplete = True
        Else
            Dim dtResults As DataTable = dvWinks.DataSource
            If (dtResults IsNot Nothing) Then
                recordsFound = dtResults.Rows.Count
            End If
        End If

        Return recordsFound
    End Function

    'Protected Overrides Sub OnPreRender(e As System.EventArgs)
    '    dvWinks.RowPerPage = Me.ItemsPerPage
    '    MyBase.OnPreRender(e)
    'End Sub



    'Protected Sub sdsWinks_Selecting(sender As Object, e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles sdsWinks.Selecting
    '    For Each prm As SqlClient.SqlParameter In e.Command.Parameters

    '        If (prm.ParameterName = "@CurrentProfileId") Then
    '            Dim MasterProfileId = Me.MasterProfileId
    '            prm.Value = MasterProfileId
    '        ElseIf (prm.ParameterName = "@ReturnRecordsWithStatus") Then
    '            prm.Value = ProfileStatusEnum.Approved
    '        ElseIf (prm.ParameterName = "@NumberOfRecordsToReturn") Then
    '            prm.Value = Me.ItemsPerPage
    '        End If

    '    Next
    'End Sub




    Protected Sub dvWinks_DataBound(sender As Object, e As EventArgs) Handles dvWinks.DataBound
        '<%# Eval("OtherMemberLoginName")%> <%# Eval("WantToKnowFirstDatePriceText")%>
        Try
            For Each dvi As DevExpress.Web.ASPxDataView.DataViewItem In dvWinks.Items
                Try

                    Dim dr As DataRowView = dvi.DataItem

                    '' CHECK OFFER CASES
                    Dim OfferID = dr("OfferID")
                    Dim OfferTypeID = dr("OffersOfferTypeID")
                    Dim OffersStatusID = dr("OffersStatusID")
                    Dim OffersFromProfileID = dr("OffersFromProfileID")
                    Dim OffersToProfileID = dr("OffersToProfileID")
                    Dim OfferAmount = dr("OffersAmount")

                    Dim lblYouReceivedWinkOfferText As Literal = dvWinks.FindItemControl("lblYouReceivedWinkOfferText", dvi)
                    Dim lblWantToKnowFirstDatePriceText As Literal = dvWinks.FindItemControl("lblWantToKnowFirstDatePriceText", dvi)

                    If (OfferTypeID = ProfileHelper.OfferTypeID_POKE) Then
                        lblYouReceivedWinkOfferText.Text = Me.CurrentPageData.GetCustomString("YouReceivedPokeText")
                        lblYouReceivedWinkOfferText.Text = lblYouReceivedWinkOfferText.Text.Replace("###LOGINNAME###", dr("LoginName"))

                        lblWantToKnowFirstDatePriceText.Text = Me.CurrentPageData.GetCustomString("YouReceivedPokeDescriptionText")
                        lblWantToKnowFirstDatePriceText.Text = lblWantToKnowFirstDatePriceText.Text.Replace("###LOGINNAME###", dr("LoginName"))
                    Else
                        lblYouReceivedWinkOfferText.Text = Me.CurrentPageData.GetCustomString("YouReceivedWinkText")
                        lblYouReceivedWinkOfferText.Text = lblYouReceivedWinkOfferText.Text.Replace("###LOGINNAME###", dr("LoginName"))

                        lblWantToKnowFirstDatePriceText.Text = Me.CurrentPageData.GetCustomString("WantToKnowFirstDatePriceText")
                        lblWantToKnowFirstDatePriceText.Text = lblWantToKnowFirstDatePriceText.Text.Replace("###LOGINNAME###", dr("LoginName"))
                    End If

                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "")
                End Try
            Next

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DistanceDropDown.ascx.vb"
    Inherits="Dating.Server.Site.Web.DistanceDropDown" %>
<dx:ASPxComboBox ID="ddl" runat="server" EncodeHtml="False">
    <Items>
        <dx:ListEditItem Value="0" Selected="true" Text="Any Distance" />
        <dx:ListEditItem Value="20" Text="20 km" />
        <dx:ListEditItem Value="60" Text="60 km" />
        <dx:ListEditItem Value="150" Text="150 km" />
        <dx:ListEditItem Value="400" Text="400 km"/>
        <dx:ListEditItem Value="700" Text="700 km"/>
        <dx:ListEditItem Value="1200" Text="1200 km"/>
    </Items>
</dx:ASPxComboBox>
<%--<asp:DropDownList ID="ddl" runat="server" AutoPostBack="true" 
    CssClass="input-small">
    <asp:ListItem Value="0">Any Distance</asp:ListItem>
    <asp:ListItem Value="10">10 km</asp:ListItem>
    <asp:ListItem Value="50">50 km</asp:ListItem>
    <asp:ListItem Value="100" Selected="true">100 km</asp:ListItem>
    <asp:ListItem Value="250">250 km</asp:ListItem>
    <asp:ListItem Value="500">500 km</asp:ListItem>
    <asp:ListItem Value="1000">1000 km</asp:ListItem>
</asp:DropDownList>--%>

﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucRewardGiftsAvaliable.ascx.vb" Inherits="Dating.Server.Site.Web.ucRewardGiftsAvaliable" %>
<%@ Import Namespace="Dating.Server.Core.DLL" %>

          <div class="list-title">
                     <asp:Literal ID="lblTitle" runat="server"  Text ="select"></asp:Literal>
                     <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
          </div>
          <div class="rptOrderGiftCards">
             <asp:Literal ID="lblTopText" runat="server"  Text ="Description"></asp:Literal>
     <asp:Repeater ID="rptOrderGiftCards" runat="server">
<ItemTemplate>
       <div class="GiftCardBox">
           <div class="GiftCardInnerBox">
                        <%--<div class="Header">
                          <%--  <asp:Label ID="lblTitle" runat="server" CssClass="lblTitle" Text='<%# Eval("Title")%>'></asp:Label>
                        </div>
                           <div class="Image">
                               <img Class ="imgGiftCard"  src ='<%# clsGiftCardHelper.ImageUrl(Eval("ImageUrl"))%>'/>
                           </div>--%>
                            <div class="Amount" style='<%# GetbgColor()%>' >
                                <asp:Label ID="lblAmount" CssClass ="lblAmmount" runat="server" Text='<%# clsNullable.DBNullToInteger(Eval("Amount")) &"€"%>'></asp:Label>
                             </div>
                            <div class="Description">
                                <asp:literal ID="ltrDescription"  runat="server" Text='<%# GetText(Eval("Description"))%>'></asp:literal>
                            </div>
               <div class="btnSelect">
                   <asp:LinkButton ID="btnSelect" CssClass='<%# If(Eval("Points")<=me.AvaliableCredits,"btnSelectGiftCard","btnSelectGiftCard Disabled") %>' Enabled='<%# If(Eval("Points")<=Me.AvaliableCredits,True,False )%>'
                       CommandArgument='<%# Eval("RewardsGiftCardsAvaliableId")%>'
                       CommandName="Order"
                        runat="server"><span class="btnSelectText"><%# Me._SelectLabel%></span>
                       </asp:LinkButton>
                   <asp:literal ID="ltrPopup"  runat="server" Text='<%# GetPopupMessage(Eval("Points"))%>'></asp:literal>
               </div>
       
  
        </div>
     
     
                </div>
 


</ItemTemplate>
</asp:Repeater>
           <div class="clear"></div></div>

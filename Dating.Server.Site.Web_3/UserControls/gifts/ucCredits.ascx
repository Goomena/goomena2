﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCredits.ascx.vb" Inherits="Dating.Server.Site.Web.ucCredits" %>

<%--<link href="/v1/css/gifts.css?v103" type="text/css" rel="Stylesheet" />--%>

<script type="text/javascript">
var lnkGetCreditsID = '<%= lnkGetCredits.ClientID%>';
var lnkGetCreditsText = '';


</script>
<asp:Panel ID="pnlTopHeader" CssClass="gifts-credits-Container" runat="server" Visible="true" >
     <div class="list-title">
                     <asp:Literal ID="lblTitle" runat="server"  Text ="Points Page Title"></asp:Literal>
                     <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
   </div>
    <div class="TopTextContainer">
           <asp:Literal ID="lblTopText" runat="server"  Text ="Points Page Title"></asp:Literal>
    </div>
</asp:Panel>
  
<div class="gifts-credits-page">
<div class="gifts-credits-content">
    <asp:Panel ID="pnlCreditsHeader" runat="server" CssClass="header-box">
        <asp:Literal ID="lblHeader" runat="server">
        </asp:Literal>
    </asp:Panel>

    <asp:Panel ID="pnlCreditsAnalysis" runat="server">
  <%--  <div class="your-Friends">
        
          <asp:Literal ID="lblYourFriends" runat="server">
        </asp:Literal>
    </div>--%>
           <div class="your-credits">
        
         <asp:Literal ID="lblYourCredits" runat="server">
        </asp:Literal>
    </div>
    <div class="credits-analysis <%= MyBase.FriendsCountCssClass %> v2">
        <div class="lfloat left-box">
             <div class="profile ">
                 <div class="lfloat PhotoContainer">
                     <asp:HyperLink ID="lnkPhoto" NavigateUrl="~/Members/Photos.aspx" runat="server" CssClass="profile-picture"  onClick="ShowLoading();"><asp:Image ID="imgPhoto" runat="server"  ImageUrl="~/Images/guy.jpg" CssClass="round-img-78" /></asp:HyperLink>
                 <div class="Name">
                         <asp:Label ID="ltrLogin" runat="server" CssClass="login-name" text="sdfasdfas" />
                     </div>
                     </div>
                 <div class="lfloat Details">
                    
                     <div class="RefCodeContainer">
                         <asp:Label ID="lblRefCodeLabel" runat="server" CssClass="Ref-CodeTitle" text="Your Code" />
                         <asp:Label ID="lblRefCode" runat="server" CssClass="Ref-Code" text="RefCode" />
                      <%-- <asp:Image ID="imgHelpRefCode" runat="server" Visible="false"  ImageUrl="//cdn.goomena.com/images2/gifts/help-icon-small.png" ToolTip="" CssClass="quest-small" />--%>
                          <asp:Label ID="lblRefCodeHelp" runat="server" CssClass="Ref-CodeHelp"  text="Αυτόν τον κωδικό δίνεις στις φίλες σου." />
                     </div>

                    </div>
                 <div class="clear"></div>
                        
                </div>
            
      <%-- <asp:Literal ID="lblFriendsDescr" runat="server" Text="" >
            </asp:Literal>--%>
            <asp:Literal ID="liFriendsDescr" runat="server"></asp:Literal>
              </div>
        <div class="lfloat center-box">
        </div>
        <div class="lfloat right-box">
            
            <asp:MultiView ID="mvFriends" runat="server" ActiveViewIndex="0">

                <asp:View ID="vwNoFriends" runat="server">
                    <div class="friends-list-empty"  >
                        <asp:Literal ID="linoFriendsDescr" runat="server"></asp:Literal>
                <%--    <div class="action-link">
                        <asp:HyperLink ID="lnkCall1" Visible="false"  runat="server">ΠΡΟΣΚΑΛΕΣΕ</asp:HyperLink>
                    </div>--%>
                    </div>
                </asp:View>

                <asp:View ID="vwFriends" runat="server">
            <div class="friends-list ">

                <asp:Panel ID="pnlFriend1" runat="server" class="one-box">
                    <div class="lfloat">
                        <div class="pr-photo-43-noshad">
                            <asp:HyperLink ID="lnkFriend1" NavigateUrl="#" runat="server" CssClass="profile-picture"  onClick="ShowLoading();">
                                <asp:Image ID="lnkFriend1Img" runat="server"  ImageUrl="~/Images/guy.jpg" CssClass="round-img-43" />
                            </asp:HyperLink>
                        </div>
                        <table cellpadding="0" cellspacing="0" class="login-wrap"><tr>
                            <td><asp:Label ID="lnkFriend1Login" runat="server" CssClass="login-name" text="" /></td>
                        </tr></table>

                    </div>
                    <div class="rfloat"><asp:Label ID="lnkFriend1Count" runat="server" CssClass="" text="45" /></div>
                    <div class="clear"></div>
                    <asp:Literal ID="lioneFriendsDescr" runat="server"></asp:Literal>
                </asp:Panel>

                <asp:Panel ID="pnlFriend2" runat="server" class="two-box">
                    <div class="lfloat">
                        <div class="pr-photo-43-noshad">
                            <asp:HyperLink ID="lnkFriend2" NavigateUrl="#" runat="server" CssClass="profile-picture"  onClick="ShowLoading();">
                                <asp:Image ID="lnkFriend2Img" runat="server"  ImageUrl="~/Images/guy.jpg" CssClass="round-img-43" />
                            </asp:HyperLink>
                        </div>
                        <table cellpadding="0" cellspacing="0" class="login-wrap"><tr>
                            <td><asp:Label ID="lnkFriend2Login" runat="server" CssClass="login-name" text="" /></td>
                        </tr></table>
                    </div>
                    <div class="rfloat"><asp:Label ID="lnkFriend2Count" runat="server" CssClass="" text="45" /></div>
                    <div class="clear"></div>
                </asp:Panel>

                <asp:Panel ID="pnlFriend3" runat="server" class="three-box">
                    <div class="lfloat">
                        <div class="pr-photo-43-noshad">
                            <asp:HyperLink ID="lnkFriend3" NavigateUrl="#" runat="server" CssClass="profile-picture"  onClick="ShowLoading();">
                                <asp:Image ID="lnkFriend3Img" runat="server"  ImageUrl="~/Images/guy.jpg" CssClass="round-img-43" />
                            </asp:HyperLink>
                        </div>
                        <table cellpadding="0" cellspacing="0" class="login-wrap"><tr>
                            <td><asp:Label ID="lnkFriend3Login" runat="server" CssClass="login-name" text="" /></td>
                        </tr></table>
                    </div>
                    <div class="rfloat"><asp:Label ID="lnkFriend3Count" runat="server" CssClass="" text="45" /></div>
                    <div class="clear"></div>
                </asp:Panel>
               
              

<%--                <div class="last-box">
                    <div class="action-link">
                        <asp:HyperLink ID="lnkCall" runat="server">ΠΡΟΣΚΑΛΕΣΕ</asp:HyperLink>
                        <img src="http://localhost:61234/cdn.goomena.com/images2/gifts/help-icon-small.png" alt="" class="quest-small hidden" />
                    </div>
                    <div class="rfloat"></div>
                    <div class="clear"></div>
                </div>--%>

            </div>
                </asp:View>

            </asp:MultiView>
             <asp:Panel ID="pnlSeeMore" runat="server" Visible="false" >
                      <div class="more-box">
                    <div class="action-link">
                        <asp:HyperLink ID="lnkMore" runat="server" CssClass="darker">...</asp:HyperLink>
                    </div>
                    <div class="rfloat"></div>
                    <div class="clear"></div>
                </div>
                </asp:Panel>
        </div>
        <div class="clear"></div>
    </div>
        <div class="credits-analysis OuterBox">
           
        
             <div class="credits-analysis InnerBox">
            <div class="top-part">
            
                <div class="details v2">
                    <div class="lfloat">
                        <div class="InnerPoints">
                        <div class="FreeCountContainer">
                            <div class="InnerFreeCountContainer">
                            <asp:Label ID="lblFree" CssClass="lblFreeTop" runat="server" Text="Points" />
                             <asp:Label ID="lblFreeCount" CssClass="lblFreeCountTop" runat="server" Text="121" />

                            </div>
                        </div>
                           <div class="bottom-part">
               
                <div class="action-link <%= MyBase.CreditsDisabledCssClass %>">
                    <asp:LinkButton ID="lnkGetCredits" runat="server">ΕΞΑΡΓΥΡΩΣΗ</asp:LinkButton>
                    <asp:Label ID="lnkGetPointsNotEnouph" CssClass="lnkGetPointsNotEnouph" runat="server">ΕΞΑΡΓΥΡΩΣΗ</asp:Label>
                  <%--  <asp:Label ID="lnkNotEnoughCredits" CssClass="lnkNotEnoughCredits" runat="server">Not enough credits</asp:Label>--%>
                
                </div>
                </div>
                            </div>
                    </div>
                   
                    <div class="lfloat v2">
                        <div class="InnerAnalysis">
                        <div class="lblAnalysis-Title">
                            <asp:literal ID="lblAnalysisTitle"  runat="server" Text="Αναλυτικά"></asp:literal></div>
                             <div class="Analysis">

                                  <div class="free">
                                     <asp:Label ID="lblFree2" CssClass="lblLabel"  runat="server" Text="ΕΛΕΥΘΕΡΑ" />
                                     <asp:Label ID="lblFreeCount2" CssClass="lblValue" runat="server" Text="121" />
                                   <asp:Image ID="imgHelpFree" runat="server" ImageUrl="//cdn.goomena.com/images2/gifts/help-icon-small.png" ToolTip="" CssClass="quest-small" />
                                 </div>
                                 <div class="Reserved">
                                     <asp:Label ID="lblReserved" CssClass="lblLabel" runat="server" Text="ΔΕΣΜΕΥΜΕΝΑ" />
                                     <asp:Label ID="lblReservedCount" CssClass="lblValue" runat="server" Text="121" />
                                 <asp:Image ID="imgHelpReserved" runat="server" ImageUrl="//cdn.goomena.com/images2/gifts/help-icon-small.png" ToolTip="" CssClass="quest-small" />
                                 </div>
                                 <div class="Total">
                                     <asp:Label ID="lblTotal" CssClass="lblLabel Total" runat="server" Text="Συνολικά Credits" />
                                     <asp:Label ID="lblTotalCount" CssClass="lblValue Total" runat="server" Text="123" />
                                 </div>
                              </div>
                    </div></div>
                     <div class="BookShadow"></div>
                                </div>
            </div>
         
            </div>
            </div>
            <asp:Panel ID="pnlMyCoupons" runat="server">
                
                  <div class="my-coupons">
                <div class="lfloat lcorner"></div>
                <div class="lfloat action-link"><asp:HyperLink ID="lnkMyCoupons" runat="server" CssClass="darker" NavigateUrl="~/Members/CouponsList.aspx">ΤΑ ΚΟΥΠΟΝΙΑ ΜΟΥ</asp:HyperLink></div>
                <div class="rfloat rcorner"></div>
                <div class="clear"></div>
            </div>
            </asp:Panel>
    </asp:Panel>

    <div class="logos">
        <img class="quest hidden" alt="" src="//cdn.goomena.com/images2/gifts/help-icon.png" />
        <asp:Literal ID="lblLogos" runat="server">
        </asp:Literal>
    </div>

</div>
</div>
<dx:ASPxPopupControl ID="popupHelp2" runat="server"
    CloseAction="MouseOut" 
    PopupVerticalAlign="NotSet"
    ShowHeader="False" 
    Width="300px" 
    CssClass="tooltip_Popup" 
    ForeColor="White" 
    BackColor="Black">
     <Windows>
             <dx:PopupWindow PopupElementID="lnkGetPointsNotEnouph"  Name="winNoCredits">
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" SupportsDisabledAttribute="True">
                    <asp:Label ID="lblHasNoCreditsPopup" runat="server" Text="" ForeColor="White"></asp:Label>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:PopupWindow>
    </Windows>
    <ContentStyle>
        <Paddings Padding="10px" />
    </ContentStyle>
    <HeaderStyle>
        <Paddings Padding="10px" />
    </HeaderStyle>
    <FooterStyle>
        <Paddings Padding="10px" />
    </FooterStyle>
    <ContentCollection>
        <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>


<dx:ASPxPopupControl ID="popupHelp" runat="server"
    CloseAction="MouseOut" 
    PopupAction="MouseOver" 
    PopupVerticalAlign="NotSet"
    ShowHeader="False" 
    Width="300px" 
    CssClass="tooltip_Popup" 
    ForeColor="White" 
    BackColor="Black">
    <Windows>
        <dx:PopupWindow PopupElementID="imgHelpFree">
            <ContentCollection>
                <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
                    <asp:Label ID="lblFreeCreditsPopup" runat="server" Text="" ForeColor="White"></asp:Label>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:PopupWindow>
        <dx:PopupWindow PopupElementID="imgHelpReserved">
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                    <asp:Label ID="lblReservedCreditsPopup" runat="server" Text="" ForeColor="White"></asp:Label>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:PopupWindow>
        <dx:PopupWindow PopupElementID="imgHelpRefCode"  >
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server" SupportsDisabledAttribute="True">
                    <asp:Label ID="lblRefCodePopup" runat="server" Text="" ForeColor="White"></asp:Label>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:PopupWindow>
     
    </Windows>
    <ContentStyle>
        <Paddings Padding="10px" />
    </ContentStyle>
    <HeaderStyle>
        <Paddings Padding="10px" />
    </HeaderStyle>
    <FooterStyle>
        <Paddings Padding="10px" />
    </FooterStyle>
    <ContentCollection>
        <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>

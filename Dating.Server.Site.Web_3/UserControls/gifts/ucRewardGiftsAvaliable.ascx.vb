﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors

Public Class ucRewardGiftsAvaliable
    Inherits BaseUserControl
    Public Event OrderGiftCard(ByVal id As Integer)
    Private _BindControlDataComplete As Boolean

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.ucRewardGiftsAvaliable", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    
    Private _RewardId As Integer = 1
    Public Property RewardId As Integer
        Get
            Return _RewardId
        End Get
        Set(value As Integer)
            _RewardId = value
        End Set
    End Property
    Public Property ItemsPerPage As Integer
        Get
            If (Me.ViewState("ItemsPerPage") IsNot Nothing) Then Return Me.ViewState("ItemsPerPage")
            Return 10
        End Get
        Set(value As Integer)
            Me.ViewState("ItemsPerPage") = value
        End Set
    End Property
    Private _AvaliableCredits = 0
    Public Property AvaliableCredits As Integer
        Get
            Return _AvaliableCredits
        End Get
        Set(value As Integer)
            _AvaliableCredits = value
        End Set
    End Property
    Public _SelectLabel As String = "Select"
    Public _readMoreLabel As String = "Read More..."
    Public Function GetText(ByVal txt As Object) As String
        Dim re As String = clsNullable.DBNullToString(txt)
        Try
            re = re.Replace("###date###", DateTime.Now.AddMonths(2).ToString("dd/MM/yyyy"))
            If re.Length > 50 Then
                Dim tmp As String = re.Substring(0, 49)
                Dim wholeText As String = re
                re = "<span class=""Bigtext"" style=""display:none;""><span class=""BigTextTextArea"">" & wholeText & "</span><span class=""ClosebtnBigText""></span></span>" & _
"<span class=""Smalltext WithMore"">" & tmp & "</span>" & "<span Class=""ReadMoreLink"" >" & _readMoreLabel & "</span>"

            Else
                re = "<span class=""Smalltext"">" & re & "</span>"

            End If

        Catch ex As Exception
        End Try

        Return re
    End Function
    Public Function GetPopupMessage(ByVal Points As Object) As String
        Dim re As String = ""
        Try
            Dim CardPoints As Integer = clsNullable.DBNullToInteger(Points)
            If CardPoints <= AvaliableCredits Then
                re = ""
            Else

                ' Dim tmp As String = re.Substring(0, 49)

                re = "<span class=""SmallPopupNotEnoughCreditsContainer"" style=""display:none;""><span class=""SmallPopupNotEnoughInner""><span class=""Triangle""></span><span class=""PopupText"">" & _NotEnoughCreditsPopupText.Replace("###Points###", CardPoints) & "</span><span class=""ClosebtnBigText""></span></span></span>" '& _


              
            End If


        Catch ex As Exception
        End Try

        Return re
    End Function
    Private _NotEnoughCreditsPopupText As String = "den Εχεις αρκετα points.."

    Protected Sub LoadLAG()
        Try
            lblTitle.Text = CurrentPageData.VerifyCustomString("lblTitle")
            _SelectLabel = CurrentPageData.VerifyCustomString("SelectLabel")
            _readMoreLabel = CurrentPageData.VerifyCustomString("readMoreLabel")
            lblTopText.Text = CurrentPageData.VerifyCustomString("lblTopBox")
            _NotEnoughCreditsPopupText = clsNullable.DBNullToString(CurrentPageData.VerifyCustomString("lblNotEnoughCreditsPopUp"), "")

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        LoadLAG()
    End Sub
    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        BindControlData()
    End Sub
    Dim RememberSet As New HashSet(Of Integer)
    Private Function GetRandom(ByVal fromNumber As Integer, ByVal toNumber As Integer)
        Dim RandomClass As New Random()
        Dim re As Integer
        Dim RandomNumber As Integer
        Dim xt As Boolean = False
        Do
            RandomNumber = RandomClass.Next(fromNumber, toNumber)
            If RememberSet.Add(RandomNumber) Then
                For i As Integer = RandomNumber - 10 To RandomNumber + 10
                    RememberSet.Add(i)
                Next
                re = RandomNumber
                Debug.WriteLine(RandomNumber)
                xt = True
            End If
        Loop Until xt
        Return re
    End Function
    Public Function GetbgColor() As String
        Dim rand As New Random
        Dim re As String = "background-color:rgb(" & GetRandom(0, 256) & ", " & GetRandom(0, 256) & "," & GetRandom(0, 256) & ");"
        Return re
    End Function
    Public Function BindControlData() As Integer
        Dim recordsFound As Integer = 0
        If (Not _BindControlDataComplete) Then
            Try
                Dim dtResults As DataTable = Nothing
                Dim dtResults2 As DataTable = Nothing
                Dim sql As String = <sql><![CDATA[
SELECT av.[RewardsGiftCardsAvaliableId]
      ,av.[Amount]
      ,av.[Points]
      ,isnull(av.[Image],gf.ImageUrl ) as ImageUrl
      ,av.[Description]
	  ,gf.Title 
        
  FROM [dbo].[EUS_RewardsGiftCardsAvaliable] as av left join
  [dbo].[vw_EUS_RewardGiftCards]  as gf on av.RewardGiftsCardsId=gf.RewardGiftsCardsId 
  where av.RewardGiftsCardsId=@RewardGiftsCardsId and av.[Enabled]=1 --and av.[Points]<=@Credits
order by av.[Amount] asc
]]></sql>
                Dim sql2 As String = <sql><![CDATA[
SELECT 
     gf.ImageUrl 
    	  ,gf.Title 
        
  FROM [dbo].[vw_EUS_RewardGiftCards]  as gf 
  where gf.RewardGiftsCardsId =@RewardGiftsCardsId 
]]></sql>
                Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
                cmd.Parameters.AddWithValue("@RewardGiftsCardsId", Me.RewardId)
                '  cmd.Parameters.AddWithValue("@Credits", Me.AvaliableCredits)
                dtResults = DataHelpers.GetDataTable(cmd)
                Dim cmd2 As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql2)
                cmd2.Parameters.AddWithValue("@RewardGiftsCardsId", Me.RewardId)
                dtResults2 = DataHelpers.GetDataTable(cmd2)
                If dtResults2.Rows.Count > 0 Then
                    Dim tmpTitle As String = clsNullable.DBNullToString(dtResults2.Rows(0)("Title"))
                    Dim tmpImgUrl As String = clsGiftCardHelper.ImageUrl(clsNullable.DBNullToString(dtResults2.Rows(0)("ImageUrl")))
                    lblTopText.Text = lblTopText.Text.Replace("###GiftCard###", tmpTitle).Replace("###ImageUrl###", tmpImgUrl)
                End If
                rptOrderGiftCards.DataSource = dtResults
                rptOrderGiftCards.DataBind()
                If (dtResults IsNot Nothing) Then
                    recordsFound = dtResults.Rows.Count
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            _BindControlDataComplete = True
        Else
            Dim dtResults As DataTable = rptOrderGiftCards.DataSource
            If (dtResults IsNot Nothing) Then
                recordsFound = dtResults.Rows.Count
            End If
        End If
        Return recordsFound
    End Function

    Private Sub rptGiftCards_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptOrderGiftCards.ItemCommand
        RaiseEvent OrderGiftCard(CInt(e.CommandArgument))
        Response.Redirect(ResolveUrl("~/members/Coupon.aspx"), False)
    End Sub
End Class
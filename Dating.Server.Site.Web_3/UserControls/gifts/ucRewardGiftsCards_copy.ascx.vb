﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors

Public Class ucRewardGiftsCards
    Inherits BaseUserControl
    Public Event GiftcardSelected(ByVal RewardGiftsCardsId As Integer, ByVal RewardsGiftCardsAvaliableId As Integer)
    Private _BindControlDataComplete As Boolean

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("control.ucRewardGiftsCards", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Public Property LastRowId As Integer
        Get
            If (Me.ViewState("LastRowId") IsNot Nothing) Then Return Me.ViewState("LastRowId")
            Return -1
        End Get
        Set(value As Integer)
            Me.ViewState("LastRowId") = value
        End Set
    End Property
    Public Property ItemsPerPage As Integer
        Get
            If (Me.ViewState("ItemsPerPage") IsNot Nothing) Then Return Me.ViewState("ItemsPerPage")
            Return 10
        End Get
        Set(value As Integer)
            Me.ViewState("ItemsPerPage") = value
        End Set
    End Property
    Private _AvaliableCredits = 300
    Public Property AvaliableCredits As Integer
        Get
            Return _AvaliableCredits
        End Get
        Set(value As Integer)
            _AvaliableCredits = value
        End Set
    End Property
    Public _SelectLabel As String = "Select"
    Public _readMoreLabel As String = "Read More..."
    Public Function GetText(ByVal txt As Object) As String
        Dim re As String = clsNullable.DBNullToString(txt)
        Try
            If re.Length > 50 Then
                Dim tmp As String = re.Substring(0, 49)
                Dim wholeText As String = re
                re = "<span class=""Bigtext"" style=""display:none;""><span class=""BigTextTextArea"">" & wholeText & "</span><span class=""ClosebtnBigText""></span></span>" & _
"<span class=""Smalltext WithMore"">" & tmp & "</span>" & "<span Class=""ReadMoreLink"" >" & _readMoreLabel & "</span>"

            Else
                re = "<span class=""Smalltext"">" & re & "</span>"

            End If

        Catch ex As Exception
        End Try

        Return re
    End Function

    Protected Sub LoadLAG()
        Try
            lblTitle.Text = CurrentPageData.VerifyCustomString("lblTitle")
            _SelectLabel = CurrentPageData.VerifyCustomString("SelectLabel")
            _readMoreLabel = CurrentPageData.VerifyCustomString("readMoreLabel")
            lblTopText.Text = CurrentPageData.VerifyCustomString("lblTopBox")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            LastRowId = -1
        End If
        LoadLAG()
    End Sub


    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        BindControlData()
    End Sub
    Public Function GetCommandArgument(ByVal RewardGiftsCardsId As Object, ByVal RewardsGiftCardsAvaliableId As Object)
        Dim re As String = "-1|-1"
        Try
            re = clsNullable.DBNullToString(RewardGiftsCardsId, -1) & "|" & clsNullable.DBNullToString(RewardsGiftCardsAvaliableId, -1)
        Catch ex As Exception

        End Try
        Return re
    End Function
    Public Function GetTitleWithAmmount(ByVal Title As Object, ByVal Ammount As Object)
        Dim re As String = " 0€"
        Try
            re = clsNullable.DBNullToString(Title) & " " & clsNullable.DBNullToInteger(Ammount).ToString("N0") & " €"
        Catch ex As Exception

        End Try
        Return re
    End Function
    Public Function BindControlData() As Integer
        Dim recordsFound As Integer = 0
        If (Not _BindControlDataComplete) Then
            Try
                Dim dtResults As DataTable = Nothing



                Dim sql As String = <sql><![CDATA[
SELECT  av.[RewardsGiftCardsAvaliableId]
      ,av.[RewardGiftsCardsId]
      ,av.[Amount]
      ,av.[Points]
      ,isnull(av.[Image],gf.[ImageUrl] ) as ImageUrl
      ,gf.[Description]
  ,gf.Title 
  FROM [dbo].[EUS_RewardsGiftCardsAvaliable] as av
 inner join [dbo].[EUS_RewardsGiftsCards]  as gf on av.RewardGiftsCardsId=gf.RewardGiftsCardsId 
   where av.[Enabled]=1 and av.[Points]<=@Credits and gf.[Enabled] =1
   order by Amount
]]></sql>

                Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
                cmd.Parameters.AddWithValue("@Credits", Me.AvaliableCredits)

                dtResults = DataHelpers.GetDataTable(cmd)


                If dtResults.Rows.Count > 0 Then
                    LastRowId = dtResults.Rows(dtResults.Rows.Count - 1)("RewardGiftsCardsId")
                End If
                rptGiftCards.DataSource = dtResults
                rptGiftCards.DataBind()

                If (dtResults IsNot Nothing) Then
                    recordsFound = dtResults.Rows.Count
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            _BindControlDataComplete = True
        Else
            Dim dtResults As DataTable = rptGiftCards.DataSource
            If (dtResults IsNot Nothing) Then
                recordsFound = dtResults.Rows.Count
            End If
        End If

        Return recordsFound
    End Function
    Private Sub rptGiftCards_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptGiftCards.ItemCommand
        Dim id1, id2 As Integer
        id1 = -1
        id2 = -2
        Dim tmp As String() = e.CommandArgument.ToString.Split("|")
        If tmp.Length > 0 Then
            id1 = CInt(tmp(0))
        End If
        If tmp.Length > 1 Then
            id2 = CInt(tmp(1))
        End If
        RaiseEvent GiftcardSelected(id1, id2)

    End Sub
End Class
﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucRewardGiftsCards.ascx.vb" Inherits="Dating.Server.Site.Web.ucRewardGiftsCards" %>
<%@ Import Namespace="Dating.Server.Core.DLL" %>

<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="2">

    <asp:View ID="View1" runat="server">
    </asp:View>
    
    
    <asp:View ID="vwNoCoupons" runat="server">
        <asp:FormView ID="fvNoCoupons" runat="server">
        <ItemTemplate>
<div class="items_none">
    <div id="dates_icon" class="middle_icon"></div>
    <div class="items_none_text">
        <%# Eval("NoOfferText")%>
        <div class="search_members_button">
	        <asp:HyperLink ID="lnk4" runat="server" CssClass="btn lighter" EnableViewState="true"
                  CommandName="Select"  NavigateUrl="~/Members/Search.aspx" onclick="ShowLoading();"><%# Eval("CouponsNotFoundText")%><i class="icon-chevron-right"></i></asp:HyperLink>
        </div>
	    <div class="clear"></div>
    </div>
</div>
        </ItemTemplate>
        </asp:FormView>
    </asp:View>

      <asp:View ID="vwSelectGiftCard" runat="server">
          <div class="list-title">
                     <asp:Literal ID="lblTitle" runat="server"  Text ="select"></asp:Literal>
                     <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
          </div>
          <div class="rptContainer">
              <asp:Literal ID="lblTopText" runat="server"  Text ="Description"></asp:Literal>

<asp:Repeater ID="rptGiftCards" runat="server">
<ItemTemplate>
       <div class="GiftCardBox">
           <div class="GiftCardInnerBox">
                        <div class="Header">
                            <asp:Label ID="lblTitle" runat="server" CssClass="lblTitle" Text='<%# GetTitleWithAmmount(Eval("Title"), Eval("Amount"))%>'></asp:Label>
                        </div>
                           <div class="Image">
                               <img Class ="imgGiftCard"  src ='<%# clsGiftCardHelper.ImageUrl(Eval("ImageUrl"))%>'/>
                           </div>
                            <div class="Description">
                                <asp:literal ID="ltrDescription"  runat="server" Text='<%# GetText(Eval("Description"))%>'></asp:literal>
                            </div>
               <div class="btnSelect">
                   <asp:LinkButton ID="btnSelect" CssClass="btnSelectGiftCard" 
                       CommandArgument='<%# GetCommandArgument(Eval("RewardGiftsCardsId"), Eval("RewardsGiftCardsAvaliableId"))%>'
                       CommandName="Select"
                        runat="server"><span class="btnSelectText"><%# Me._SelectLabel%></span>

                   </asp:LinkButton>
               </div>
          <%--             <div class="lfloat Preview">
            <div class="CouponFirstLine">
                <asp:Label ID="lblCouponFirstLine" CssClass="lblCouponFirstLine" runat="server" Text='<%# Eval("CouponFirstLine")%>'>
                </asp:Label>
            </div>--%>
         <%--   <div class="PreviewCouponcredits">
                <div class="PreviewCouponcreditsCont">
                    <asp:Label ID="lblPreviewCouponCredits" CssClass="lblPreviewCouponCredits" runat="server" Text='<%# Eval("CreditAmmountText")%>'>
                    </asp:Label>
                    <asp:Label ID="lblCouponCreditsΤext" CssClass="lblCouponCreditsΤext" EnableViewState="true"
                         runat="server" Text='<%# Eval("CouponCreditsΤext")%>'>
                    </asp:Label>
                </div>
            </div>--%>

     <%--       <div class="PreviewCouponName">
                <asp:Label ID="lblPreviewCouponName" CssClass="lblPreviewCouponName" runat="server" Text='<%# Eval("CouponName")%>'>
                </asp:Label>
            </div>--%>

        <%--    <div class="CouponBarcode">
            </div>--%>
         <%--   <div class="CouponBotomNotes">
                <asp:Label ID="lblCouponBotomNotes" CssClass="lblCouponBotomNotes" runat="server" Text='<%# Eval("CouponBotomNotes")%>'>
                </asp:Label>
            </div>--%>
  
        </div>
     <%--   <div class="lfloat PreviewOnlyPrint">
            <div class="PreviewCouponMoney">
                <asp:Label ID="lblCouponMoney" CssClass="PreviewlblCouponMoney" runat="server" Text='<%# Eval("CreditAmmountText")%>'></asp:Label>
            </div>
            <div class="CouponCode">
                <asp:Label ID="lblCouponCode" CssClass="PreviewlblCouponCode" runat="server" Text='<%# Eval("CouponCode")%>'></asp:Label>
            </div>
        </div>--%>
     
                </div>
   <%-- </div>--%>


</ItemTemplate>
</asp:Repeater>
           <div class="clear"></div></div>
 </asp:View>


</asp:MultiView>
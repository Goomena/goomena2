﻿Imports Dating.Server.Core.DLL
Imports DevExpress.Web.ASPxEditors

Public Class ucRewardGiftsCards
    Inherits BaseUserControl
    Public Event GiftcardSelected(ByVal id As Integer)
    Private _BindControlDataComplete As Boolean

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("control.OfferControl", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent
                _pageData = New clsPageData("control.ucRewardGiftsCards", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property

    Public Property LastRowId As Integer
        Get
            If (Me.ViewState("LastRowId") IsNot Nothing) Then Return Me.ViewState("LastRowId")
            Return -1
        End Get
        Set(value As Integer)
            Me.ViewState("LastRowId") = value
        End Set
    End Property
    Public Property ItemsPerPage As Integer
        Get
            If (Me.ViewState("ItemsPerPage") IsNot Nothing) Then Return Me.ViewState("ItemsPerPage")
            Return 10
        End Get
        Set(value As Integer)
            Me.ViewState("ItemsPerPage") = value
        End Set
    End Property
    Private _AvaliableCredits = 0
    Public Property AvaliableCredits As Integer
        Get
            Return _AvaliableCredits
        End Get
        Set(value As Integer)
            _AvaliableCredits = value
        End Set
    End Property
    Public _SelectLabel As String = "Select"
    Public _readMoreLabel As String = "Read More..."
    Public Function GetText(ByVal txt As Object) As String
        Dim re As String = clsNullable.DBNullToString(txt)
        Try
            If re.Length > 50 Then
                Dim tmp As String = re.Substring(0, 49)
                Dim wholeText As String = re
                re = "<span class=""Bigtext"" style=""display:none;""><span class=""BigTextTextArea"">" & wholeText & "</span><span class=""ClosebtnBigText""></span></span>" & _
"<span class=""Smalltext WithMore"">" & tmp & "</span>" & "<span Class=""ReadMoreLink"" >" & _readMoreLabel & "</span>"

            Else
                re = "<span class=""Smalltext"">" & re & "</span>"

            End If

        Catch ex As Exception
        End Try

        Return re
    End Function

    Protected Sub LoadLAG()
        Try
            lblTitle.Text = CurrentPageData.VerifyCustomString("lblTitle")
            _SelectLabel = CurrentPageData.VerifyCustomString("SelectLabel")
            _readMoreLabel = CurrentPageData.VerifyCustomString("readMoreLabel")
            lblTopText.Text = CurrentPageData.VerifyCustomString("lblTopBox")
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub
    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            LastRowId = -1
        End If
        LoadLAG()
    End Sub
    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        BindControlData()
    End Sub

    Public Function BindControlData() As Integer
        Dim recordsFound As Integer = 0
        If (Not _BindControlDataComplete) Then
            Try
                Dim dtResults As DataTable = Nothing



                Dim sql As String = <sql><![CDATA[
SELECT TOP (@RowCount) Gf.[RewardGiftsCardsId]
      ,Gf.[Title]
      ,Gf.[Description]
      ,Gf.[ImageUrl]
       FROM [dbo].[vw_EUS_RewardGiftCards] as Gf
  where  Gf.[enabled]=1 and Gf.[RewardGiftsCardsId]>@lastrowId -- and Gf.minPoints<=@Credits
  order by RewardGiftsCardsId
]]></sql>

                Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
                cmd.Parameters.AddWithValue("@RowCount", Me.ItemsPerPage)
                cmd.Parameters.AddWithValue("@lastrowId", Me.LastRowId)
                '   cmd.Parameters.AddWithValue("@Credits", Me.AvaliableCredits)

                dtResults = DataHelpers.GetDataTable(cmd)


                If dtResults.Rows.Count > 0 Then
                    LastRowId = dtResults.Rows(dtResults.Rows.Count - 1)("RewardGiftsCardsId")
                End If
                rptGiftCards.DataSource = dtResults
                rptGiftCards.DataBind()

                If (dtResults IsNot Nothing) Then
                    recordsFound = dtResults.Rows.Count
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

            _BindControlDataComplete = True
        Else
            Dim dtResults As DataTable = rptGiftCards.DataSource
            If (dtResults IsNot Nothing) Then
                recordsFound = dtResults.Rows.Count
            End If
        End If

        Return recordsFound
    End Function
    Private Sub rptGiftCards_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptGiftCards.ItemCommand
        RaiseEvent GiftcardSelected(CInt(e.CommandArgument)
                                     )

    End Sub
End Class
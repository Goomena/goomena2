﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCredits.ascx.vb" Inherits="Dating.Server.Site.Web.ucCredits" %>

<%--<link href="/v1/css/gifts.css?v103" type="text/css" rel="Stylesheet" />--%>

<script type="text/javascript">
var lnkGetCreditsID = '<%= lnkGetCredits.ClientID%>';
var lnkGetCreditsText = '';


</script>
   <div class="list-title">
                     <asp:Literal ID="lblTitle" runat="server"  Text ="Pvs"></asp:Literal>
                     <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
   </div>
<div class="gifts-credits-page">
<div class="gifts-credits-content">
    <asp:Panel ID="pnlCreditsHeader" runat="server" CssClass="header-box">
        <asp:Literal ID="lblHeader" runat="server">
        </asp:Literal>
    </asp:Panel>

    <asp:Panel ID="pnlCreditsAnalysis" runat="server">
    <div class="your-credits">
        <asp:Literal ID="lblYourCredits" runat="server">
        </asp:Literal>
    </div>
    <div class="credits-analysis <%= MyBase.FriendsCountCssClass %>">
        <div class="lfloat left-box">
            <div class="top-part">
                <div class="profile">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width:1%;"><div class="pr-photo-78-noshad">
                                    <asp:HyperLink ID="lnkPhoto" NavigateUrl="~/Members/Photos.aspx" runat="server" CssClass="profile-picture"  onClick="ShowLoading();">
                                        <asp:Image ID="imgPhoto" runat="server"  ImageUrl="~/Images/guy.jpg" CssClass="round-img-78" />
                                    </asp:HyperLink>
                                </div></td>
                            <td style="width:1%;">&nbsp;&nbsp;</td>
                            <td style="width:98%;"><asp:Label ID="ltrLogin" runat="server" CssClass="login-name" text="sdfasdfas" /></td>
                        </tr>
                    </table>
                </div>
                <div class="details">
                    <div class="total">
                        <div class="lfloat"><asp:Label ID="lblTotal" runat="server" Text="Συνολικά Credits" /></div>
                        <div class="rfloat"><asp:Label ID="lblTotalCount" runat="server" Text="123" /></div>
                        <div class="clear"></div>
                    </div>
                    <div class="free">
                        <div class="lfloat"><asp:Label ID="lblFree" runat="server" Text="ΕΛΕΥΘΕΡΑ" /></div>
                        <div class="rfloat"><asp:Label ID="lblFreeCount" runat="server" Text="121" /></div>
                        <div class="clear"></div>
                        <asp:Image ID="imgHelpFree" runat="server" ImageUrl="//cdn.goomena.com/images2/gifts/help-icon-small.png" ToolTip="" CssClass="quest-small" />
                    </div>
                    <div class="reserved">
                        <div class="lfloat"><asp:Label ID="lblReserved" runat="server" Text="ΔΕΣΜΕΥΜΕΝΑ" /></div>
                        <div class="rfloat"><asp:Label ID="lblReservedCount" runat="server" Text="121" /></div>
                        <div class="clear"></div>
                        <asp:Image ID="imgHelpReserved" runat="server" ImageUrl="//cdn.goomena.com/images2/gifts/help-icon-small.png" ToolTip="" CssClass="quest-small" />
                    </div>
                    <asp:Panel ID="pnlFriendsCount" runat="server" Visible="false" class="friends">
                        <div class="lfloat"><asp:Label ID="lblFriends" runat="server" Text="ΑΠΟ ΦΙΛΕΣ" /></div>
                        <div class="rfloat"><asp:Label ID="lblFriendsCount" runat="server" Text="12" /></div>
                        <div class="clear"></div>
                    </asp:Panel>
                    <asp:Panel ID="pnlBonusCount" runat="server" Visible="false" class="bonus">
                        <div class="lfloat"><asp:Label ID="lblBonus" runat="server" Text="BONUS" /></div>
                        <div class="rfloat"><asp:Label ID="lblBonusCount" runat="server" Text="12" /></div>
                        <div class="clear"></div>
                    </asp:Panel>
                </div>
            </div>
            <div class="bottom-part">
                <%--<div class="selection">
                    <asp:Literal ID="lblSelectionTitle" runat="server" Text="" >
                    </asp:Literal>
                    <div class="spin-wrap">
                        <img src="//cdn.goomena.com/images2/gifts/help-icon-small.png" alt="" class="quest-small" />
                        <div class="HorLineInButtons"></div>
                        <div class="<%= MyBase.CreditsDisabledCssClass %>">
                        <dx:ASPxSpinEdit ID="spinCredits" runat="server" Height="36px" Number="100" MinValue="100" MaxValue="1000" Width="150px"
                            Font-Bold="True" CssClass="spin-control" 
                            DisplayFormatString="# ' Credits'" 
                            Increment="10" 
                            LargeIncrement="20">
                            <SpinButtons>
                                <IncrementImage Url="~/Images2/gifts/white-arrow-up.png">
                                </IncrementImage>
                               
                                <DecrementImage Url="~/Images2/gifts/white-arrow-down.png">
                                </DecrementImage>
                            </SpinButtons>
                            <IncrementButtonStyle HorizontalAlign="Center" Width="26px">
                                <Border BorderStyle="None" />
                            </IncrementButtonStyle>
                            <DecrementButtonStyle HorizontalAlign="Center" Width="26px">
                                <Border BorderStyle="None" />
                            </DecrementButtonStyle>
                            <Border BorderStyle="None" />
                            <ClientSideEvents Init="spinCredits_Init" ValueChanged="spinCredits_ValueChanged" />
                                           
                        </dx:ASPxSpinEdit>
                        </div>
                    </div>

                </div> 
                    --%>
                <div class="action-link <%= MyBase.CreditsDisabledCssClass %>">
                    <asp:LinkButton ID="lnkGetCredits" runat="server">ΕΞΑΡΓΥΡΩΣΗ</asp:LinkButton>
                    <asp:Label ID="lnkNotEnoughCredits" CssClass="lnkNotEnoughCredits" runat="server">Not enough credits</asp:Label>
                    <%--<script type="text/javascript">
                        getCreditsText();
                    </script>--%>
                </div>
            </div>
            <asp:Panel ID="pnlMyCoupons" runat="server">
                
                  <div class="my-coupons">
                <div class="lfloat lcorner"></div>
                <div class="lfloat action-link"><asp:HyperLink ID="lnkMyCoupons" runat="server" CssClass="darker" NavigateUrl="~/Members/CouponsList.aspx">ΤΑ ΚΟΥΠΟΝΙΑ ΜΟΥ</asp:HyperLink></div>
                <div class="rfloat rcorner"></div>
                <div class="clear"></div>
            </div>
            </asp:Panel>
              </div>
        <div class="lfloat center-box">
        </div>
        <div class="lfloat right-box">
            <asp:Literal ID="lblFriendsDescr" runat="server" Text="" >
            </asp:Literal>
            <asp:MultiView ID="mvFriends" runat="server" ActiveViewIndex="0">

                <asp:View ID="vwNoFriends" runat="server">
                    <div class="friends-list-empty">
                    <div class="action-link">
                        <asp:HyperLink ID="lnkCall1" runat="server">ΠΡΟΣΚΑΛΕΣΕ</asp:HyperLink>
                    </div>
                    </div>
                </asp:View>

                <asp:View ID="vwFriends" runat="server">
            <div class="friends-list ">

                <asp:Panel ID="pnlFriend1" runat="server" class="one-box">
                    <div class="lfloat">
                        <div class="pr-photo-43-noshad">
                            <asp:HyperLink ID="lnkFriend1" NavigateUrl="#" runat="server" CssClass="profile-picture"  onClick="ShowLoading();">
                                <asp:Image ID="lnkFriend1Img" runat="server"  ImageUrl="~/Images/guy.jpg" CssClass="round-img-43" />
                            </asp:HyperLink>
                        </div>
                        <table cellpadding="0" cellspacing="0" class="login-wrap"><tr>
                            <td><asp:Label ID="lnkFriend1Login" runat="server" CssClass="login-name" text="" /></td>
                        </tr></table>
                    </div>
                    <div class="rfloat"><asp:Label ID="lnkFriend1Count" runat="server" CssClass="" text="45" /></div>
                    <div class="clear"></div>
                </asp:Panel>

                <asp:Panel ID="pnlFriend2" runat="server" class="two-box">
                    <div class="lfloat">
                        <div class="pr-photo-43-noshad">
                            <asp:HyperLink ID="lnkFriend2" NavigateUrl="#" runat="server" CssClass="profile-picture"  onClick="ShowLoading();">
                                <asp:Image ID="lnkFriend2Img" runat="server"  ImageUrl="~/Images/guy.jpg" CssClass="round-img-43" />
                            </asp:HyperLink>
                        </div>
                        <table cellpadding="0" cellspacing="0" class="login-wrap"><tr>
                            <td><asp:Label ID="lnkFriend2Login" runat="server" CssClass="login-name" text="" /></td>
                        </tr></table>
                    </div>
                    <div class="rfloat"><asp:Label ID="lnkFriend2Count" runat="server" CssClass="" text="45" /></div>
                    <div class="clear"></div>
                </asp:Panel>

                <asp:Panel ID="pnlFriend3" runat="server" class="two-box">
                    <div class="lfloat">
                        <div class="pr-photo-43-noshad">
                            <asp:HyperLink ID="lnkFriend3" NavigateUrl="#" runat="server" CssClass="profile-picture"  onClick="ShowLoading();">
                                <asp:Image ID="lnkFriend3Img" runat="server"  ImageUrl="~/Images/guy.jpg" CssClass="round-img-43" />
                            </asp:HyperLink>
                        </div>
                        <table cellpadding="0" cellspacing="0" class="login-wrap"><tr>
                            <td><asp:Label ID="lnkFriend3Login" runat="server" CssClass="login-name" text="" /></td>
                        </tr></table>
                    </div>
                    <div class="rfloat"><asp:Label ID="lnkFriend3Count" runat="server" CssClass="" text="45" /></div>
                    <div class="clear"></div>
                </asp:Panel>

                <div class="more-box">
                    <div class="action-link">
                        <asp:HyperLink ID="lnkMore" runat="server" CssClass="darker">ΠΕΡΙΣΣΟΤΕΡΑ</asp:HyperLink>
                    </div>
                    <div class="rfloat"></div>
                    <div class="clear"></div>
                </div>

                <div class="last-box">
                    <div class="action-link">
                        <asp:HyperLink ID="lnkCall" runat="server">ΠΡΟΣΚΑΛΕΣΕ</asp:HyperLink>
                        <img src="http://localhost:61234/cdn.goomena.com/images2/gifts/help-icon-small.png" alt="" class="quest-small hidden" />
                    </div>
                    <div class="rfloat"></div>
                    <div class="clear"></div>
                </div>

            </div>
                </asp:View>

            </asp:MultiView>
        </div>
        <div class="clear"></div>
    </div>
    </asp:Panel>

    <div class="logos">
        <img class="quest hidden" alt="" src="//cdn.goomena.com/images2/gifts/help-icon.png" />
        <asp:Literal ID="lblLogos" runat="server">
        </asp:Literal>
    </div>

</div>
</div>
<dx:ASPxPopupControl ID="popupHelp" runat="server"
    CloseAction="MouseOut" 
    PopupAction="MouseOver" 
    PopupVerticalAlign="NotSet"
    ShowHeader="False" 
    Width="300px" 
    CssClass="tooltip_Popup" 
    ForeColor="White" 
    BackColor="Black">
    <Windows>
        <dx:PopupWindow PopupElementID="imgHelpFree">
            <ContentCollection>
                <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
                    <asp:Label ID="lblFreeCreditsPopup" runat="server" Text="" ForeColor="White"></asp:Label>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:PopupWindow>
        <dx:PopupWindow PopupElementID="imgHelpReserved">
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
                    <asp:Label ID="lblReservedCreditsPopup" runat="server" Text="" ForeColor="White"></asp:Label>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:PopupWindow>
        <dx:PopupWindow PopupElementID="lnkNotEnoughCredits"  Name="winNoCredits">
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server" SupportsDisabledAttribute="True">
                    <asp:Label ID="lblHasNoCreditsPopup" runat="server" Text="" ForeColor="White"></asp:Label>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:PopupWindow>
    </Windows>
    <ContentStyle>
        <Paddings Padding="10px" />
    </ContentStyle>
    <HeaderStyle>
        <Paddings Padding="10px" />
    </HeaderStyle>
    <FooterStyle>
        <Paddings Padding="10px" />
    </FooterStyle>
    <ContentCollection>
        <dx:PopupControlContentControl runat="server" SupportsDisabledAttribute="True">
        </dx:PopupControlContentControl>
    </ContentCollection>
</dx:ASPxPopupControl>

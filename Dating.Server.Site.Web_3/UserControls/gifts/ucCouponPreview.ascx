﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucCouponPreview.ascx.vb"
    Inherits="Dating.Server.Site.Web.ucCouponPreview" %>


<div class="PreviewCouponContainer">
    <div class="PreviewCouponTitle">
        <div class="Toprow">
            <div class="lblPreviewCouponTitle">
                <asp:Literal ID="lblPreviewCouponTitle" runat="server" Text="To Κουπόνι σου δημιουργήθηκε με επιτυχία">

                </asp:Literal>

            </div>
        </div>
        <div class="SecondRow">
            <div class="lblPreviewCouponNotes1">
                <asp:Literal ID="lblPreviewCouponNotes1" runat="server" Text="Έχει Αποσταλεί">
                </asp:Literal>
            </div>
        </div>
        <div class="ThrirdRow">
            <div class="lblPreviewCouponNotes2">
                <asp:Literal ID="lblPreviewCouponNotes2" runat="server" Text="Μπορε">
                </asp:Literal>
            </div>
        </div>
    </div>

    <div class="PreviewCouponArea">
        <div class="lfloat Preview">
            <div class="CouponFirstLine">
                <asp:Label ID="lblCouponFirstLine" CssClass="lblCouponFirstLine" runat="server" Text="Lorem ipsum is simply dummy text">

                </asp:Label>
            </div>
            <div class="PreviewCouponcredits">
                <div class="PreviewCouponcreditsCont">

                    <asp:Label ID="lblPreviewCouponCredits" CssClass="lblPreviewCouponCredits" runat="server" Text="140">
                    </asp:Label>


                    <asp:Label ID="lblCouponCreditsΤext" CssClass="lblCouponCreditsΤext" runat="server" Text="Credits">
                    </asp:Label>



                </div>
            </div>

            <div class="PreviewCouponName">
                <asp:Label ID="lblPreviewCouponName" CssClass="lblPreviewCouponName" runat="server" Text="140 Credits">
                </asp:Label>
            </div>

            <div class="CouponBarcode">
            </div>
            <div class="CouponBotomNotes">
                <asp:Label ID="lblCouponBotomNotes" CssClass="lblCouponBotomNotes" runat="server" Text="Το παρόνο κούπόνι....">
                </asp:Label>
            </div>


        </div>
        <div class="lfloat PreviewOnlyPrint">
            <div class="PreviewCouponMoney">
                <asp:Label ID="lblCouponMoney" CssClass="PreviewlblCouponMoney" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="CouponCode">
                <asp:Label ID="lblCouponCode" CssClass="PreviewlblCouponCode" runat="server" Text="Label"></asp:Label>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <div class="PreviewCouponButtons">
        <div class="ContainerButtons">
           <%-- <asp:HyperLink ID="btnDownload" CssClass="PreviewDownloadButton" runat="server">
                <asp:Label ID="lblDownloadText" CssClass="lblDownloadText" runat="server" Text="Κατέβασμα">
                </asp:Label>

            </asp:HyperLink>--%>
            <asp:HyperLink ID="btnResend" CssClass="PreviewResendButton" runat="server">
                <asp:Label ID="lblResendText" CssClass="lblDownloadText" runat="server" Text="Αποστολή ξανά">
                </asp:Label>
            </asp:HyperLink>
        </div>
    </div>
</div>























﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucRewardGiftCardConfirm.ascx.vb" Inherits="Dating.Server.Site.Web.ucRewardGiftCardConfirm" %>
<%@ Import Namespace="Dating.Server.Core.DLL" %>

 <div class="list-title">
                     <asp:Literal ID="lblTitle" runat="server"  Text ="Step 3 Confirm your order"></asp:Literal>
                     <asp:LinkButton ID="lnkViewDescription" runat="server" CssClass="more_info2" OnClientClick="OnMoreInfoClick('[contentUrl]', this, '[headerText]');return false;" />
          </div>
<div Id="GiftCardConfirmContainer">
    <div class="GiftCardConfirmContainerInner">
        <div class="OrderContainer">
            <div class="ProductContainer">

                 <table style="width:100%;">
                            <tr>
                                <td><asp:Label ID="lblDescription" CssClass="lblDescription Label" runat="server" Text="Description"></asp:Label></td>
                                <td><asp:Label ID="lblAmount" CssClass="lblAmount Label" runat="server" Text="Amount"></asp:Label></td>
                                <td><asp:Label ID="lblQuantity" CssClass="lblQuantity Label" runat="server" Text="Quantity"></asp:Label></td>
                                <td><asp:Label ID="lblCost" CssClass="lblCost Label" runat="server" Text="Cost"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                       <div class="Description">
                                          <asp:Label ID="valTitle" CssClass ="lblTitle Value" runat="server" Text="Amazon"></asp:Label>
                                        </div>
                                   
                                    <div Class="SelectedGiftCardImageContainer">
                                          <div class="Image">
                       
                                            </div>

                                      </div>

                                </td>
                                <td><asp:Label ID="valAmount" CssClass ="lblAmount Value" runat="server" Text="100€"></asp:Label></td>
                                <td><asp:Label ID="valQuantity" CssClass ="valQuantity Value" runat="server" Text="1"></asp:Label></td>
                                 <td><asp:Label ID="valCost" CssClass ="valCost Value" runat="server" Text="1"></asp:Label></td>
                            </tr>
                           
                        </table>

                
             
                 <div class="Amount">
                       
                    
                </div>
                 <div class="Quantity">
                     
                    
                </div>
                 <div class="Quantity">
                     <asp:Label ID="Label1" CssClass="lblQuantity Label" runat="server" Text="Quantity"></asp:Label>
                    <asp:Label ID="Label2" CssClass ="valQuantity Value" runat="server" Text="1"></asp:Label>
                </div>
            </div>
            
        </div>
        <div class="Buttons">
            <asp:LinkButton ID="lnkConfirm" CssClass="ConfirmOrder" runat="server">Confirm</asp:LinkButton>
             <asp:LinkButton ID="lnkCancelOrder" CssClass="CancelOrder" runat="server">Cancel</asp:LinkButton>
        </div>
    </div>
</div>


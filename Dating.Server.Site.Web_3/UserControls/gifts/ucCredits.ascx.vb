﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL

Public Class ucCredits
    Inherits BaseUserControl


    Public Event CreateCouponEvent()

    Private affCredits As clsReferrerCredits

    Protected Property FriendsCountCssClass As String = "no-friends"
    Protected Property CreditsDisabledCssClass As String = "opacity60"
    Private _HasCredits As Boolean = False
    Public ReadOnly Property HasCredits As Boolean
        Get
            Return _HasCredits
        End Get
    End Property
    Public Property ShowCreditsHeader As Boolean
        Get
            Return pnlCreditsHeader.Visible
        End Get
        Set(value As Boolean)
            pnlCreditsHeader.Visible = value
        End Set
    End Property
    Public Property ShowCreditsAnalysis As Boolean
        Get
            Return pnlCreditsAnalysis.Visible
        End Get
        Set(value As Boolean)
            pnlCreditsAnalysis.Visible = value
        End Set
    End Property
  
    Public Property ShowTopHeader As Boolean
        Get
            Return pnlTopHeader.Visible
        End Get
        Set(value As Boolean)
            pnlTopHeader.Visible = value
        End Set
    End Property
    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            ' If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/control.ProfileEdit", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("Members.control.Gifts.Credits", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property



    Private Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Try

            If (clsCurrentContext.VerifyLogin() = False) Then
                FormsAuthentication.SignOut()
                Response.Redirect(Page.ResolveUrl("~/Login.aspx"))
            End If


            'Try
            '    Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            '    AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
            'Catch ex As Exception
            '    WebErrorMessageBox(Me, ex, "Page_Load")
            'End Try


            If (Not Me.IsPostBack) Then
                LoadLAG()
                SetReferrerUI()
            End If

        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "Page_Load")
        End Try
    End Sub



    'Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender

    '    Try
    '        Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
    '        AppUtils.setSEOPageData(Me.Page, cPageBasic.PageTitle, cPageBasic.MetaDesciption, cPageBasic.MetaKeywords)
    '    Catch ex As Exception
    '        WebErrorMessageBox(Me, ex, "Page_Load")
    '    End Try

    'End Sub


    Dim referrer_payout_min_balance As Integer = -1
    Public _friendsDescr As String = ""
    Protected Sub LoadLAG()
        Try
            If (CurrentPageData.LagID <> GetLag()) Then
                Me._pageData = Nothing
            End If

            lblTotalCount.Text = 0.ToString("0")
            lblFreeCount.Text = 0.ToString("0")
            lblReservedCount.Text = 0.ToString("0")
            'lblFriendsCount.Text = 0.ToString("0")
            '   lblBonusCount.Text = 0.ToString("0")

            Dim cPageBasic As Core.DLL.clsSiteLAG.clsPageBasicReturn = CurrentPageData.cPageBasic
            lblHeader.Text = CurrentPageData.GetCustomString("lblHeader")
            lblYourCredits.Text = CurrentPageData.GetCustomString("lblYourCredits")
            'lblSelectionTitle.Text = CurrentPageData.GetCustomString("lblSelectionTitle")
            _friendsDescr = CurrentPageData.VerifyCustomString("lblFriendsDescr2")
            lblLogos.Text = CurrentPageData.GetCustomString("lblLogos")
            _moreText = CurrentPageData.GetCustomString("lnkMore")
            '  lnkCall.Text = CurrentPageData.GetCustomString("lnkCall")
            lnkGetCredits.Text = CurrentPageData.VerifyCustomString("lnkGetCredits")
            lnkMyCoupons.Text = CurrentPageData.GetCustomString("lnkMyCoupons")
            lblFree.Text = CurrentPageData.GetCustomString("lblFree")
            lblReserved.Text = CurrentPageData.GetCustomString("lblReserved")
            lblFreeCreditsPopup.Text = CurrentPageData.GetCustomString("lblFreeCreditsPopup")
            lblReservedCreditsPopup.Text = CurrentPageData.GetCustomString("lblReservedCreditsPopup")
            lblHasNoCreditsPopup.Text = CurrentPageData.GetCustomString("lblHasNoCreditsPopup")
            '  lnkNotEnoughCredits.Text = CurrentPageData.GetCustomString("lnkNotEnoughCredits")
            lblFree2.Text = lblFree.Text
            lblFreeCount2.Text = lblFreeCount.Text
            'lnkCall1.Text = lnkCall.TextlblTotal
            lblTotal.Text = CurrentPageData.GetCustomString("lblTotal")
            ltrLogin.Text = Me.SessionVariables.MemberData.LoginName
            lblRefCode.Text = clsReferrer.CreateReferrerCode(Me.ProfileLoginName, Me.MasterProfileId)
            Dim referrer_friend_gift_credits As Integer = clsConfigValues.Get__referrer_friend_gift_credits()
            _friendsDescr = _friendsDescr.
            Replace("[LOGIN-NAME]", Me.SessionVariables.MemberData.LoginName).
            Replace("[GIFT-CREDITS]", referrer_friend_gift_credits)
            lblTitle.Text = CurrentPageData.GetCustomString("lblTitle")
            lblTopText.Text = CurrentPageData.GetCustomString("lblTopText")
            lblRefCodeLabel.Text = CurrentPageData.GetCustomString("lblRefCodeLabel")
            lnkGetPointsNotEnouph.Text = lnkGetCredits.Text
            lblRefCodeHelp.Text = CurrentPageData.GetCustomString("lblRefCodeHelp")
            lblRefCodeLabel.Text = CurrentPageData.GetCustomString("lblRefCodeLabel")
            '  lblYourFriends.Text = CurrentPageData.GetCustomString("lblYourFriends")
            lblAnalysisTitle.Text = CurrentPageData.GetCustomString("lblAnalysisTitle")
            'spinCredits.Enabled = False
            lnkGetCredits.Enabled = False
            CreditsDisabledCssClass = "opacity60"

            pnlFriend1.Visible = False
            pnlFriend2.Visible = False
            pnlFriend3.Visible = False

            mvFriends.ActiveViewIndex = 0
            FriendsCountCssClass = "no-friends"


            Dim profile As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(Me.MasterProfileId)
            If (Not profile.IsREF_Payout_MinBalanceNull()) Then
                referrer_payout_min_balance = profile.REF_Payout_MinBalance

            Else
                referrer_payout_min_balance = clsConfigValues.Get__referrer_payout_min_balance()

            End If
            If (referrer_payout_min_balance > 0) Then
                lblHasNoCreditsPopup.Text = lblHasNoCreditsPopup.Text.Replace("[PAYOUT_MIN]", referrer_payout_min_balance)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadLAG")
        End Try
    End Sub
    Public Sub RefreshData()
        SetReferrerUI()
    End Sub

    Private Sub SetReferrerUI()
        affCredits = GetReferrerCreditsObject(New DateTime(2010, 1, 1), DateTime.UtcNow.AddDays(1))
        LoadData()
    End Sub
    Private _moreText As String = "More"

    Protected Function GetReferrerCreditsObject(dateFrom As DateTime?, dateTo As DateTime?)
        If (affCredits Is Nothing) Then

            Try
                If (affCredits Is Nothing) Then
                    Dim _dtFrom As DateTime?
                    Dim _dtTo As DateTime?
                    If (dateFrom.HasValue) Then _dtFrom = dateFrom.Value.Date
                    If (dateTo.HasValue) Then _dtTo = dateTo.Value.Date.AddDays(1).AddMilliseconds(-1)

                    affCredits = New clsReferrerCredits(Me.MasterProfileId, Me.SessionVariables.MemberData.LoginName, _dtFrom, _dtTo)
                    affCredits.Load()
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try
        End If

        Return affCredits
    End Function


    Public Function GetProfilePhotoUrl(profileId As Integer, photoSize As PhotoSize) As String
        Dim imageUrl As String = ""
        Try
            Dim GenderId As Integer = DataHelpers.GetEUS_Profile_GenderId_ByProfileID(Me.CMSDBDataContext, profileId)
            Dim photo As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(profileId)
            If (photo IsNot Nothing) Then
                imageUrl = ProfileHelper.GetProfileImageURL(profileId, photo.FileName, GenderId, True, Me.IsHTTPS, photoSize)
            Else
                imageUrl = ProfileHelper.GetProfileImageURL(profileId, Nothing, GenderId, True, Me.IsHTTPS, photoSize)

                'If (imageUrl.Contains(ProfileHelper.Male_DefaultImagePrivate.Replace("~/", ""))) Then
                '    imageUrl = imageUrl.Replace(ProfileHelper.Male_DefaultImagePrivate.Replace("~/", ""), ProfileHelper.Male_DefaultImagePrivate180.Replace("~/", ""))

                'ElseIf (imageUrl.Contains(ProfileHelper.Female_DefaultImagePrivate.Replace("~/", ""))) Then
                '    imageUrl = imageUrl.Replace(ProfileHelper.Female_DefaultImagePrivate.Replace("~/", ""), ProfileHelper.Female_DefaultImagePrivate180.Replace("~/", ""))

                'End If
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "LoadData")
        End Try
        Return imageUrl
    End Function

    Protected Sub LoadData()

        '' load photo of current user
        imgPhoto.ImageUrl = GetProfilePhotoUrl(Me.MasterProfileId, PhotoSize.D150)

        Dim amount As Dictionary(Of String, Object) = affCredits.GetTotalPayment(Me.MasterProfileId)

        Dim LastPaymentAmount As Double = amount("LastPaymentAmount")
        Dim LastPaymentDate As DateTime = amount("LastPaymentDate")
        Dim TotalDebitAmount As Double = amount("TotalDebitAmount")
        Dim totalMoneyToBePaid As Double = affCredits.CommissionCreditsTotal - TotalDebitAmount
        Dim AvailableAmount As Double = affCredits.CommissionCreditsAvailable - TotalDebitAmount
        Dim PendingAmount As Double = affCredits.CommissionCreditsPending
        If (AvailableAmount < 0) Then
            PendingAmount = PendingAmount + AvailableAmount
            AvailableAmount = 0
        End If


        Dim referrer_friend_gift_creditsTotal As Integer = 0
        Dim referrer_friend_gift_credits As Integer = clsConfigValues.Get__referrer_friend_gift_credits()
        Dim drs As DSCustom.ReferrersRepositoryDataRow() = affCredits.GetReferrersForLevel(2)
        '  Dim totalreff As Integer = affCredits.GetReferrersForLevel(2).Count + affCredits.GetReferrersForLevel(3).Count + affCredits.GetReferrersForLevel(4).Count
        Dim ndx As Integer = 0
        If (drs.Length > 0) Then
            Dim drs2 As DataRow() = affCredits.GetReferrersForLevel_SortMaxCredits(2)

            If (drs2.Length > ndx AndAlso clsNullable.DBNullToDouble(drs2(ndx)("CommissionCredits")) > 0) Then
                pnlFriend1.Visible = True
                mvFriends.ActiveViewIndex = 1
                '    FriendsCountCssClass = "one-friend"

                lnkFriend1Login.Text = drs2(ndx)("LoginName")
                lnkFriend1Img.ImageUrl = GetProfilePhotoUrl(drs2(ndx)("ProfileID"), PhotoSize.D150)
                lnkFriend1.NavigateUrl = ProfileHelper.GetProfileNavigateUrl(drs2(ndx)("LoginName"))
                lnkFriend1Count.Text = Math.Floor(clsNullable.DBNullToDouble(drs2(ndx)("CommissionCredits"))).ToString("0")
                ndx = 1
            End If


            If (drs2.Length > ndx AndAlso clsNullable.DBNullToDouble(drs2(ndx)("CommissionCredits")) > 0) Then
                pnlFriend2.Visible = True
                mvFriends.ActiveViewIndex = 1
                '  FriendsCountCssClass = "two-friends"

                lnkFriend2Login.Text = drs2(ndx)("LoginName")
                lnkFriend2Img.ImageUrl = GetProfilePhotoUrl(drs2(ndx)("ProfileID"), PhotoSize.D150)
                lnkFriend2.NavigateUrl = ProfileHelper.GetProfileNavigateUrl(drs2(ndx)("LoginName"))
                lnkFriend2Count.Text = Math.Floor(clsNullable.DBNullToDouble(drs2(ndx)("CommissionCredits"))).ToString("0")
                ndx = 2
            End If


            If (drs2.Length > ndx AndAlso clsNullable.DBNullToDouble(drs2(ndx)("CommissionCredits")) > 0) Then
                pnlFriend3.Visible = True
                mvFriends.ActiveViewIndex = 1
                '  FriendsCountCssClass = "two-friends"

                lnkFriend3Login.Text = drs2(ndx)("LoginName")
                lnkFriend3Img.ImageUrl = GetProfilePhotoUrl(drs2(ndx)("ProfileID"), PhotoSize.D150)
                lnkFriend3.NavigateUrl = ProfileHelper.GetProfileNavigateUrl(drs2(ndx)("LoginName"))
                lnkFriend3Count.Text = Math.Floor(clsNullable.DBNullToDouble(drs2(ndx)("CommissionCredits"))).ToString("0")
                ndx = 3
            End If

            If drs.Length > 3 Then
                pnlSeeMore.Visible = True
                lnkMore.Text = _moreText.Replace("###Num###", drs.Length() - 3)
                '  ndx = 4
            Else
                pnlSeeMore.Visible = False
            End If

        End If
        If ndx = 0 Then
            FriendsCountCssClass = "no-friends"
            linoFriendsDescr.Text = Me._friendsDescr
            linoFriendsDescr.Visible = True
            liFriendsDescr.Visible = False
            lioneFriendsDescr.Visible = False
        ElseIf ndx = 1 Then
            FriendsCountCssClass = "one-friends"
            lioneFriendsDescr.Text = Me._friendsDescr
            lioneFriendsDescr.Visible = True
            linoFriendsDescr.Visible = False
            liFriendsDescr.Visible = False
        ElseIf ndx = 2 Then

            FriendsCountCssClass = "two-friends"
            liFriendsDescr.Text = Me._friendsDescr
            linoFriendsDescr.Visible = False
            liFriendsDescr.Visible = True
            lioneFriendsDescr.Visible = False
        ElseIf ndx = 3 Then
            FriendsCountCssClass = "three-friends"
            liFriendsDescr.Text = Me._friendsDescr
            linoFriendsDescr.Visible = False
            liFriendsDescr.Visible = True
            lioneFriendsDescr.Visible = False
        Else
            FriendsCountCssClass = "more-friends"


        End If
   
        'If (ndx = 1) Then
        '    FriendsCountCssClass = "two-friends"
        'End If

        Dim AvailableAmount2 As Double = AvailableAmount + referrer_friend_gift_creditsTotal


        If (PendingAmount > 0) Then
            lblReservedCount.Text = Math.Floor(PendingAmount).ToString("0")
        End If

        ' If (AvailableAmount2 > 0) Then

        lblTotalCount.Text = Math.Floor(AvailableAmount2).ToString("0")
        '    lblBonusCount.Text = 0.ToString("0")

        lblFreeCount.Text = Math.Floor(AvailableAmount2 - PendingAmount).ToString("0")
        lblFreeCount2.Text = lblFreeCount.Text
        '  lblFriendsCount.Text = referrer_friend_gift_creditsTotal.ToString("0")

        'spinCredits.Enabled = True
        ' If AvailableAmount >= referrer_payout_min_balance Then

        _HasCredits = True
        lnkGetCredits.Enabled = True
        CreditsDisabledCssClass = ""
        lnkGetPointsNotEnouph.Visible = False
        lnkGetCredits.Visible = True
        Dim win1 As DevExpress.Web.ASPxPopupControl.PopupWindow = popupHelp.Windows.FindByName("winNoCredits")
        If (win1 IsNot Nothing) Then
            popupHelp.Windows.Remove(win1)
        End If
        'Else
        '    '"opacity60"
        '    lnkGetPointsNotEnouph.Visible = True
        '    lnkGetCredits.Visible = False
        '    _HasCredits = False
        '    lnkGetCredits.Enabled = False
        '    CreditsDisabledCssClass = ""
        '    'CreditsDisabledCssClass = "opacity60"

        'End If


        'Dim profile As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(Me.MasterProfileId)
        'If (Not profile.IsREF_Payout_MinBalanceNull()) Then
        '    spinCredits.Number = profile.REF_Payout_MinBalance
        '    spinCredits.MinValue = profile.REF_Payout_MinBalance
        'End If
        'If spinCredits.MinValue < AvailableAmount2 Then
        '    spinCredits.MaxValue = AvailableAmount2
        '    CreditsDisabledCssClass = ""
        '    _HasCredits = True
        'Else
        '    spinCredits.Enabled = False
        'End If

        'Else

        '    spinCredits.Number = 0
        '    spinCredits.MinValue = 0
        '    spinCredits.MaxValue = 0
        'Else
        '    lnkGetPointsNotEnouph.Visible = True
        '    lnkGetCredits.Visible = False
        '    _HasCredits = False
        '    lnkGetCredits.Enabled = False
        '    CreditsDisabledCssClass = ""
        'End If
        If ProfileHelper.HasCoupons(Me.MasterProfileId) Then
            pnlMyCoupons.Visible = True
        Else
            pnlMyCoupons.Visible = False
        End If
    End Sub



    Private Sub lnkGetCredits_Click(sender As Object, e As EventArgs) Handles lnkGetCredits.Click
        RaiseEvent CreateCouponEvent()
    End Sub
End Class

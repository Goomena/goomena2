﻿Imports System.Drawing
Imports DevExpress.Web.ASPxEditors
Imports Dating.Server.Core.DLL
Imports Dating.Server.Core.DLL.clsUserDoes
Imports Dating.Server.Datasets.DLL

Public Class MessagesConversation
    Inherits BaseUserControl



    Private __freeMessagesHelper As clsFreeAndSubscriptionMessagesHelper
    Private ReadOnly Property freeMessagesHelper As clsFreeAndSubscriptionMessagesHelper
        Get
            If (__freeMessagesHelper Is Nothing) Then
                __freeMessagesHelper = New clsFreeAndSubscriptionMessagesHelper(Me.MasterProfileId,
                                                               Me.SessionVariables.MemberData.Country)

            End If
            Return __freeMessagesHelper
        End Get
    End Property

#Region "Props"

    Public Property ScrollToElem As String
    Public Property CurrentLoginName As String
    Public Property WarningPopup_HeaderText As String

    Protected Overloads ReadOnly Property CurrentPageData As clsPageData
        Get
            'If (_pageData Is Nothing) Then _pageData = New clsPageData("~/Members/Messages.aspx", Context)
            If (_pageData Is Nothing) Then
                Dim coe As clsPageData.CacheOptionsEnum
                If (Context.Request.Url.Query = "?pagenocache" OrElse Context.Request.Url.Query.Contains("&pagenocache")) Then coe = clsPageData.CacheOptionsEnum.DisabledCacheForContent

                _pageData = New clsPageData("~/Members/Messages.aspx", Context, coe)
                AddHandler _pageData.CustomStringRetrievalComplete, AddressOf Page_CustomStringRetrievalComplete
            End If
            Return _pageData
        End Get
    End Property


    Public Property OfferId As Long
        Get
            Return ViewState("OfferId")
        End Get
        Set(value As Long)
            ViewState("OfferId") = value
        End Set
    End Property


    Public Property RowNumberMin As Long?
        Get
            Return ViewState("RowNumberMin")
        End Get
        Set(value As Long?)
            ViewState("RowNumberMin") = value
        End Set
    End Property


    Public Property RowNumberMax As Long?
        Get
            Return ViewState("RowNumberMax")
        End Get
        Set(value As Long?)
            ViewState("RowNumberMax") = value
        End Set
    End Property


    Public Property UserIdInView As Integer
        Get
            Return ViewState("UserIdInView")
        End Get
        Set(value As Integer)
            ViewState("UserIdInView") = value
        End Set
    End Property


    Public Property SubjectInView As String
        Get
            Return ViewState("SubjectInView")
        End Get
        Set(value As String)
            ViewState("SubjectInView") = value
        End Set
    End Property


    Public Property MessageIdInView As Long
        Get
            Return ViewState("MessageIdInView")
        End Get
        Set(value As Long)
            ViewState("MessageIdInView") = value
        End Set
    End Property

    ' used with delete message action
    Public Property MessageIdListInView As List(Of Long)
        Get
            Return ViewState("MessageIdListInView")
        End Get
        Set(value As List(Of Long))
            ViewState("MessageIdListInView") = value
        End Set
    End Property


    'Public Property BackUrl As String
    '    Get
    '        Return ViewState("BackUrl")
    '    End Get
    '    Set(value As String)
    '        ViewState("BackUrl") = value
    '    End Set
    'End Property


    'Public Property SendAnotherMessage As Boolean
    '    Get
    '        Return ViewState("SendAnotherMessage")
    '    End Get
    '    Set(value As Boolean)
    '        ViewState("SendAnotherMessage") = value
    '    End Set
    'End Property


    ''' <summary>
    ''' Recipient id.
    ''' Used when current user opens it's sent message and wants to send another one.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property SendAnotherMessageToRecipient As Integer
        Get
            Return ViewState("SendAnotherMessageToRecipient")
        End Get
        Set(value As Integer)
            ViewState("SendAnotherMessageToRecipient") = value
        End Set
    End Property

    Public ReadOnly Property Count As Integer
        Get
            Return dvConversation.Items.Count
        End Get
    End Property


    Public Property FromRight_NavigateUrl As String
        Get
            Return ViewState("lnkFromRight_NavigateUrl")
        End Get
        Set(value As String)
            ViewState("lnkFromRight_NavigateUrl") = value
        End Set
    End Property


    Public Property FromRight_ImageSrc As String
        Get
            Return ViewState("FromRight_ImageSrc")
        End Get
        Set(value As String)
            ViewState("FromRight_ImageSrc") = value
        End Set
    End Property


    Public Property SuppressWarning_Country As String
        Get
            Return ViewState("SuppressWarning_Country")
        End Get
        Private Set(value As String)
            ViewState("SuppressWarning_Country") = value
        End Set
    End Property

    Public Property IsThereMoreMessages As Boolean
        Get
            Return ViewState("IsThereMoreMessages")
        End Get
        Set(value As Boolean)
            ViewState("IsThereMoreMessages") = value
        End Set
    End Property

    Public Property OlderMsgIdAvailable As Long?
        Get
            Return ViewState("OlderMsgIdAvailable")
        End Get
        Set(value As Long?)
            ViewState("OlderMsgIdAvailable") = value
        End Set
    End Property

    Public Property LoadMessageOnly As Boolean
        Get
            Return ViewState("LoadMessageOnly")
        End Get
        Set(value As Boolean)
            ViewState("LoadMessageOnly") = value
        End Set
    End Property

    Public Property FreeMessagesOnView As Integer
        Get
            Return ViewState("FreeMessagesOnView")
        End Get
        Set(value As Integer)
            ViewState("FreeMessagesOnView") = value
        End Set
    End Property

    Public Property LoadUntilDate As DateTime?
        Get
            Return ViewState("LoadUntilDate")
        End Get
        Set(value As DateTime?)
            ViewState("LoadUntilDate") = value
        End Set
    End Property

    Public Property LoadAllMessages As Boolean
        Get
            Return ViewState("LoadAllMessages")
        End Get
        Set(value As Boolean)
            ViewState("LoadAllMessages") = value
        End Set
    End Property

    Public Property OlderMessageDate As DateTime
        Get
            Return ViewState("OlderMessageDate")
        End Get
        Set(value As DateTime)
            ViewState("OlderMessageDate") = value
        End Set
    End Property



    Public Property ListData As String
        Get
            Return ViewState("ListData")
        End Get
        Set(value As String)
            ViewState("ListData") = value
        End Set
    End Property

#End Region



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try


            'Dim viewLoaded As Boolean = False

            'Dim unlockMessage As Boolean = MyBase.UnlimitedMsgID > 0 AndAlso Not String.IsNullOrEmpty(Request.QueryString("vw"))
            'Dim viewMessage As Boolean = Not MyBase.UnlimitedMsgID > 0 AndAlso (Not String.IsNullOrEmpty(Request.QueryString("vw")) OrElse Not String.IsNullOrEmpty(Request.QueryString("msg")))
            'Dim sendMessageTo As Boolean = Not String.IsNullOrEmpty(Request.QueryString("p"))
            'Dim tabChanged As Boolean = Not String.IsNullOrEmpty(Request.QueryString("t"))
            LoadLAG()


            'If (Not Page.IsPostBack) Then
            '    LoadLAG()

            '    'If (viewMessage) Then
            '    '    '' load conversation for requested and current profiles
            '    '    'LoadViews()

            '    '    If (Not String.IsNullOrEmpty(Request.QueryString("vw"))) Then
            '    '        Dim otherLoginName = Request.QueryString("vw")

            '    '        Dim memberReceivingMessage As EUS_Profile = DataHelpers.GetEUS_ProfileMasterByLoginName(Me.CMSDBDataContext, otherLoginName, ProfileStatusEnum.Approved, ProfileStatusEnum.LIMITED)
            '    '        If (memberReceivingMessage IsNot Nothing) Then
            '    '            CurrentLoginName = memberReceivingMessage.LoginName

            '    '            Dim HasCommunication As Boolean = GetHasCommunicationDict(memberReceivingMessage.ProfileID)
            '    '            Dim message As EUS_Message = clsUserDoes.GetLastMessageForProfiles(Me.MasterProfileId, memberReceivingMessage.ProfileID)
            '    '            If (message IsNot Nothing) Then
            '    '                Me.UserIdInView = memberReceivingMessage.ProfileID
            '    '                Me.MessageIdInView = message.EUS_MessageID
            '    '            Else
            '    '                ' there are no messages among users
            '    '                Me.UserIdInView = memberReceivingMessage.ProfileID
            '    '                Me.MessageIdInView = 0
            '    '            End If

            '    '            ExecCmd("VIEWMSG", Me.MessageIdInView)
            '    '            viewLoaded = True

            '    '        End If
            '    '    End If



            '    '    Dim messageID As Integer
            '    '    If (Not String.IsNullOrEmpty(Request.QueryString("msg")) AndAlso Integer.TryParse(Request.QueryString("msg"), messageID)) Then
            '    '        If (messageID > 0) Then
            '    '            Dim message As EUS_Message = clsUserDoes.GetMessage(messageID)

            '    '            If (message Is Nothing OrElse message.ProfileIDOwner <> Me.MasterProfileId) Then
            '    '                ' message not found or message is owned by another user


            '    '            ElseIf (message.ProfileIDOwner = Me.MasterProfileId) Then
            '    '                If (message.FromProfileID = Me.MasterProfileId) Then
            '    '                    Me.UserIdInView = message.ToProfileID
            '    '                Else
            '    '                    Me.UserIdInView = message.FromProfileID
            '    '                End If
            '    '                Me.MessageIdInView = message.EUS_MessageID
            '    '                ExecCmd("VIEWMSG", Me.MessageIdInView)
            '    '                viewLoaded = True

            '    '            End If
            '    '        End If
            '    '    End If

            '    'End If



            '    'If (Not viewLoaded AndAlso sendMessageTo) Then

            '    '    '' send message to profile
            '    '    LoadLAG()
            '    '    'LoadViews()
            '    '    LoadNewMessageView()
            '    '    viewLoaded = True

            '    'End If

            '    'If (Not viewLoaded AndAlso unlockMessage) Then
            '    '    LoadLAG()
            '    '    PerformMessageUnlock()
            '    '    viewLoaded = True
            '    'End If

            '    'If (Not viewLoaded) Then

            '    'End If
            'End If

            ''If (Page.IsPostBack) Then
            'If (Not viewLoaded AndAlso unlockMessage) Then
            '    PerformMessageUnlock()
            'End If
            ''End If
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try

    End Sub


    Private Sub Conversation_Unload(sender As Object, e As EventArgs) Handles Me.Unload
        If (__freeMessagesHelper IsNot Nothing) Then __freeMessagesHelper.Dispose()
    End Sub


    'Private Function GetMembersFreeMessagesAvailableToREAD(fromProfileID As Integer) As Integer
    '    Dim myFreeMessages As Integer = 0
    '    Dim fromProfileIDFreeMessages As Integer = 0
    '    If (ConfigurationManager.AppSettings("AllowFreeMessagesToRead") = "1") Then
    '        If (freeMessagesHelper.CreditsPackagesBonusEnabled) Then

    '            Dim cls As clsFreeMessagesHelper = clsFreeMessagesHelper.GetData(Me.MasterProfileId, fromProfileID, Me.SessionVariables.MemberData.Country)
    '            If (Not cls.IsProfilesReadCountLimit OrElse cls.HasProfileConversation) Then
    '                myFreeMessages = cls.Credits_FreeMessagesReadFromProfile - cls.MessagesReadFromProfile
    '            End If

    '        End If
    '    End If
    '    Return myFreeMessages
    'End Function
    Private __clsFreeMessagesHelper As CustomerFreeMessagesAvailableResult
    Private Function GetMembersFreeMessagesAvailableToREAD(manProfileId As Integer?, womanProfileId As Integer?, manCountry As String) As Integer
        Dim freeMessagesCount As Integer = 0
        If (__clsFreeMessagesHelper Is Nothing) Then
            If (ConfigurationManager.AppSettings("AllowFreeMessagesToRead") = "1" AndAlso
                freeMessagesHelper.CreditsPackagesBonusEnabled) Then

                __clsFreeMessagesHelper = Me.CMSDBDataContext.CustomerFreeMessagesAvailable(manProfileId, womanProfileId, manCountry).FirstOrDefault()
                '__clsFreeMessagesHelper = clsFreeMessagesHelper.GetData(Me.MasterProfileId, fromProfileID, Me.SessionVariables.MemberData.Country)

            End If
        End If

        If (__clsFreeMessagesHelper IsNot Nothing) Then
            If (Not __clsFreeMessagesHelper.IsProfilesReadCountLimit OrElse __clsFreeMessagesHelper.HasProfileConversation) Then
                freeMessagesCount = __clsFreeMessagesHelper.Credits_FreeMessagesReadFromProfile - __clsFreeMessagesHelper.MessagesReadFromProfile
            End If
        End If

        Return freeMessagesCount
    End Function

    'Private Function GetSubsriptionAvailableMessages(fromProfileID As Integer) As Integer
    '    Dim mySubscriptionMessages As Integer = 0
    '    Dim tmp As Integer = 0
    '    If (ConfigurationManager.AppSettings("AllowSubscription") = "1") Then
    '        If (freeMessagesHelper.HasSubscription) Then

    '            Dim cls As clsSubscriptionMessagesHelper = clsSubscriptionMessagesHelper.GetData(Me.MasterProfileId, fromProfileID, Me.SessionVariables.MemberData.Country)
    '            If (Not cls.IsProfilesCountLimit OrElse cls.HasProfileConversationToday) Then
    '                mySubscriptionMessages = cls.TotalMessagesAvailableForProfile
    '            End If

    '        End If
    '    End If
    '    Return mySubscriptionMessages
    'End Function
    'Private __SubscriptionMessagesHelper As clsSubscriptionMessagesHelper

    Private __SubscriptionMessagesHelper As CustomerSubscriptionAvailableMessages2Result
    Private Function GetSubsriptionAvailableMessages(manProfileId As Integer?,
                                                     womanProfileId As Integer?,
                                                     manCountry As String,
                                                     Optional dateFrom As Date? = Nothing,
                                                     Optional dateTo As Date? = Nothing) As Integer
        Dim subsriptionMessagesCount As Integer = 0

        If (__SubscriptionMessagesHelper Is Nothing) Then
            If (ConfigurationManager.AppSettings("AllowSubscription") = "1" AndAlso
                freeMessagesHelper.HasSubscription) Then

                __SubscriptionMessagesHelper = Me.CMSDBDataContext.CustomerSubscriptionAvailableMessages2(manProfileId, womanProfileId, manCountry, dateFrom, dateTo).FirstOrDefault()
                '__SubscriptionMessagesHelper = clsSubscriptionMessagesHelper.GetData(Me.MasterProfileId, fromProfileID, Me.SessionVariables.MemberData.Country)

            End If
        End If

        If (__SubscriptionMessagesHelper IsNot Nothing) Then
            If (Not __SubscriptionMessagesHelper.IsProfilesCountLimit OrElse __SubscriptionMessagesHelper.HasProfileConversationToday) Then
                subsriptionMessagesCount = __SubscriptionMessagesHelper.TotalMessagesAvailableForProfile
            End If
        End If
        Return subsriptionMessagesCount
    End Function

    Public Sub LoadUsersConversation(messageId As Integer)
        Me.MessageIdInView = messageId

        If (messageId > 0) Then
            BindConversations()
        Else

            If (Me.UserIdInView > 0) Then
                dvConversation.Visible = False
            End If

        End If


        ' when message id specified, Me.UserIdInView set on  dvConversation DataBound
        Dim otherUser As Integer = Me.UserIdInView
        If (Me.UserIdInView > 1) OrElse (Me.SendAnotherMessageToRecipient > 0) Then
            otherUser = Me.UserIdInView
            If (otherUser = 0) Then otherUser = Me.SendAnotherMessageToRecipient
        End If

        If (otherUser > 0) Then
            Dim commandsSet As Boolean = False

            ' view to male two buttons with credits
            If (Me.IsMale) Then
                Dim HasCommunication As Boolean = GetHasCommunicationDict(otherUser, True)

                'If (Not clsUserDoes.HasCommunication(otherUser, Me.MasterProfileId)) Then
                If (Not HasCommunication) Then
                    Dim login As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(Me.CMSDBDataContext, otherUser)
                    CurrentLoginName = login
                    commandsSet = True
                End If
            End If

            ' if male may send unlimited messages or it's female, 
            ' then show button "Send another message"
            If (Not commandsSet) Then
                Dim login As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(Me.CMSDBDataContext, otherUser)
                CurrentLoginName = login
                Dim url As String = ResolveUrl("~/Members/Conversation.aspx?p=" & HttpUtility.UrlEncode(login))
            End If

        End If
        dvConversation.Visible = (dvConversation.Items.Count > 0)

    End Sub

    Public Overrides Sub Master_LanguageChanged()
        Me._pageData = Nothing
        'MessagesView = MessagesViewEnum.INBOX
        LoadLAG()
    End Sub


    Protected Sub LoadLAG()
        Try


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub



    Protected Sub dvConversation_ItemCommand(source As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles dvConversation.ItemCommand
        Try
            ExecCmd(e.CommandName, e.CommandArgument)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Protected Sub offersRepeater_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs)
        Try
            ExecCmd(e.CommandName, e.CommandArgument)
        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    Private Sub ExecCmd(CommandName As String, CommandArgument As String)
        Try

            If (CommandName = "DELETEMSG") Then


                Dim MsgID As Long
                Integer.TryParse(CommandArgument, MsgID)
                If (clsNullable.NullTo(Me.GetCurrentProfile().ReferrerParentId) > 0) Then
                    clsUserDoes.DeleteMessage(Me.MasterProfileId, MsgID, True)
                Else
                    clsUserDoes.DeleteMessage(Me.MasterProfileId, MsgID, False)
                End If
                Me.MessageIdListInView.Remove(MsgID)
                If (Me.MessageIdListInView.Count > 0) Then
                    MsgID = Me.MessageIdListInView(0)
                    LoadUsersConversation(MsgID)
                Else
                    'MessagesView = MessagesViewEnum.INBOX
                    'Me.MessageIdInView = 0
                    'LoadMessagesList()
                    LoadUsersConversation(0)
                    BindConversations()
                    dvConversation.Visible = (dvConversation.Items.Count > 0)
                End If
            End If


            If (CommandName = "VIEWMSG") Then

                Dim MsgID As Integer
                Integer.TryParse(CommandArgument, MsgID)
                LoadUsersConversation(MsgID)
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try


        If (CommandName = "REJECT_DELETECONV" OrElse CommandName = "REJECT_BLOCK") Then


            ' commands of Dates control, copied from dates.aspx
            Dim success = False
            Dim showNoPhotoNote = False

            Try
                Dim currentUserHasPhotos As Boolean = clsUserDoes.HasPhotos(Me.MasterProfileId, Me.MirrorProfileId)
                If (currentUserHasPhotos) Then

                    ''''''''''''''''''''''
                    'photo aware actions
                    ''''''''''''''''''''''
                    If (CommandName = "REJECT_DELETECONV") Then

                        ''''''''''''''''''''''
                        'photo aware action
                        ''''''''''''''''''''''

                        Dim OtherMemberProfileID As Integer
                        Integer.TryParse(CommandArgument.ToString(), OtherMemberProfileID)

                        If (OtherMemberProfileID > 0) Then
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, Me.MasterProfileId, OtherMemberProfileID, MessagesViewEnum.SENT, (IsReferrer = True))
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.INBOX, (IsReferrer = True))
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.NEWMESSAGES, (IsReferrer = True))
                            clsMessagesHelper.DeleteMessagesFromMemberInView(Me.MasterProfileId, OtherMemberProfileID, Me.MasterProfileId, MessagesViewEnum.TRASH, (IsReferrer = True))

                            clsUserDoes.MarkDateOfferAsViewed(OtherMemberProfileID, Me.MasterProfileId)
                        End If

                        success = True

                    ElseIf (CommandName = "REJECT_BLOCK") Then

                        Dim OtherMemberProfileID As Integer
                        Integer.TryParse(CommandArgument.ToString(), OtherMemberProfileID)

                        If (OtherMemberProfileID > 0) Then
                            clsUserDoes.MarkAsBlocked(OtherMemberProfileID, Me.MasterProfileId)
                            clsUserDoes.MarkDateOfferAsViewed(OtherMemberProfileID, Me.MasterProfileId)
                        End If

                        success = True

                    End If

                Else
                    showNoPhotoNote = True
                End If

            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try



            Try

                If (showNoPhotoNote) Then
                    Response.Redirect("~/Members/Photos.aspx", False)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()

                ElseIf (success) Then
                    LoadUsersConversation(Me.MessageIdInView)
                End If
            Catch ex As Exception
                WebErrorMessageBox(Me, ex, "")
            End Try

        End If


    End Sub



    Private Sub BindConversations()
        Try

            Dim sql As String = "exec [GetConversationWithMessage3] @messageID=@messageID,@ownerProfileID=@ownerProfileID,@rowNumberMin=@rowNumberMin,@rowNumberMax=@rowNumberMax,@performCount=@performCount,@loadMessageOnly=@loadMessageOnly,@loadUntilDate=@loadUntilDate"
            Dim cmd As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@messageID", Me.MessageIdInView)
            cmd.Parameters.AddWithValue("@ownerProfileID", Me.MasterProfileId)
            cmd.Parameters.AddWithValue("@performCount", True)
            cmd.Parameters.AddWithValue("@loadMessageOnly", LoadMessageOnly)

            If (clsNullable.NullTo(Me.RowNumberMax, -1) > -1) Then
                cmd.Parameters.AddWithValue("@rowNumberMax", clsNullable.NullTo(Me.RowNumberMax))
            Else
                cmd.Parameters.AddWithValue("@rowNumberMax", System.DBNull.Value)
            End If

            If (clsNullable.NullTo(Me.RowNumberMin, -1) > -1) Then
                cmd.Parameters.AddWithValue("@rowNumberMin", clsNullable.NullTo(Me.RowNumberMin))
            Else
                cmd.Parameters.AddWithValue("@rowNumberMin", System.DBNull.Value)
            End If

            If (Me.LoadUntilDate IsNot Nothing) Then
                cmd.Parameters.AddWithValue("@loadUntilDate", Me.LoadUntilDate.Value)
            Else
                cmd.Parameters.AddWithValue("@loadUntilDate", System.DBNull.Value)
            End If

            Dim ds As DataSet = DataHelpers.GetDataSet(cmd)
            Dim dt As DataTable = ds.Tables(1)

            If (OlderMsgIdAvailable > 0) Then
                Dim drs As DataRow() = dt.Select("EUS_MessageID=" & OlderMsgIdAvailable)
                If (drs.Length > 0) Then
                    ' perform query , because initial messages list was changed
                    cmd.Parameters("@rowNumberMin").Value = 0
                    ds = DataHelpers.GetDataSet(cmd)
                    dt = ds.Tables(1)

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "reset-initial-list", "function resetInitialList(){return true;}", True)
                End If
            End If

            dvConversation.Visible = True
            dvConversation.DataSourceID = ""
            dvConversation.DataSource = dt
            dvConversation.DataBind()

            Dim totalCount As Integer = ds.Tables(0).Rows(0)(0)
            Dim _rowNumberMax = dt.Compute("MAX(RowNumber)", "")

            If (_rowNumberMax IsNot DBNull.Value) Then
                Me.IsThereMoreMessages = _rowNumberMax < totalCount
            End If

            If (dt.Rows.Count > 0) Then
                Try
                    If (Not ds.Tables(0).Rows(0).IsNull("OlderMessageDate")) Then
                        OlderMessageDate = ds.Tables(0).Rows(0)("OlderMessageDate")
                    Else
                        OlderMessageDate = DateTime.UtcNow
                    End If

                    Dim requestedItems As Integer = -1
                    If (clsNullable.NullTo(Me.RowNumberMax, -1) > -1 AndAlso clsNullable.NullTo(Me.RowNumberMin, -1) > -1) Then
                        requestedItems = Me.RowNumberMax - Me.RowNumberMin
                    End If

                    Dim MinDate As DateTime = dt.Compute("Min(DateTimeToCreate)", "")
                    Dim MinDateDiff As Integer = (DateTime.UtcNow - MinDate).TotalDays
                    Dim OlderMessageDateDiff As Integer = (DateTime.UtcNow - OlderMessageDate).TotalDays

                    Dim totalItems As Integer = 0
                    If (ds.Tables(0).Rows.Count > 0) Then totalItems = ds.Tables(0).Rows(0)(0)
                    Dim json As String = _
                       "{" & getJSON_LI("MinDate", MinDate.ToString("yyyy-MM-dd")) & _
                        "," & getJSON_LI("DaysDiff", MinDateDiff) & _
                        "," & getJSON_LI("items", totalItems) & _
                        "," & getJSON_LI("more", Me.IsThereMoreMessages.ToString().ToLower()) & _
                        "," & getJSON_LI("rowNumberMax", _rowNumberMax) & _
                        "," & getJSON_LI("resultCount", dt.Rows.Count) & _
                        "," & getJSON_LI("requestedItems", requestedItems) & _
                        "," & getJSON_LI("olderMessageDays", OlderMessageDateDiff) & _
                        "}"
                    ListData = json.Replace("""", "&quot;")

                    '"{""MinDate"":""" & MinDate.ToString("yyyy-MM-dd") & """,""DaysDiff"":""" & MinDateDiff & """,""items"":""" & totalItems & """,""more"":""" & Me.IsThereMoreMessages.ToString().ToLower() & """,""rowNumberMax"":""" & _rowNumberMax & """}"
                    'hdfListData.Value = "{""MinDate"":""" & MinDate.ToString("yyyy-MM-dd") & """,""DaysDiff"":""" & MinDateDiff & """,""items"":""" & totalItems & """,""more"":""" & Me.IsThereMoreMessages.ToString().ToLower() & """,""rowNumberMax"":""" & _rowNumberMax & """}"
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "BindConversations -- Fill ListData")
                End Try
            End If


        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "BindConversations")
        End Try

    End Sub

    Private Function getJSON_LI(name As String, value As String) As String
        Return """" & name & """:""" & value & """"
    End Function

    Protected Sub dvConversation_DataBound(sender As Object, e As EventArgs) Handles dvConversation.DataBound
        SetConversationFields()

    End Sub


    Private dvConversationData As List(Of DataRowView)
    Protected Sub dvConversation_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.ListViewItemEventArgs) Handles dvConversation.ItemDataBound
        If (dvConversationData Is Nothing) Then dvConversationData = New List(Of DataRowView)
        dvConversationData.Add(e.Item.DataItem)
    End Sub



    Protected Function GetCountMessagesString(ItemsCount As Object) As String

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 0) Then Return "(" & ItemsCount & ")"
            Catch ex As Exception

            End Try
        End If

        Return ""
    End Function


    Protected Function IsVisible(ItemsCount As Object) As Boolean

        If (Not AppUtils.IsDBNullOrNothingOrEmpty(ItemsCount)) Then
            Try
                If (ItemsCount > 1) Then Return True
            Catch ex As Exception

            End Try
        End If

        Return False
    End Function


    Private Sub SetConversationFields()
        Try
            Me.UserIdInView = 0
            Me.SubjectInView = ""
            'Me.MessageIdInView = 0
            Me.MessageIdListInView = New List(Of Long)

            Dim loginName As String = Me.CurrentLoginName
            Dim HasCommunication As Boolean = False
            Dim HasRequiredCredits_To_UnlockMessage As Boolean = HasRequiredCredits(UnlockType.UNLOCK_MESSAGE_READ)
            Dim country As String = Nothing
            Dim SuppressWarning As Boolean?


            Dim credits As Integer = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS
            Dim mostRecentMessageIndex As Integer = -1
            Dim EUS_MessageID As Long = 0
            Dim StatusID As Integer = 0

            Dim cnt As Integer
            For cnt = 0 To dvConversation.Items.Count - 1
                'For Each dvi As DevExpress.Web.ASPxDataView.DataViewItem In dvConversation.Items
                Try

                    Dim dvi As System.Web.UI.WebControls.ListViewDataItem = dvConversation.Items(cnt)
                    Dim dr As DataRowView = dvConversationData(dvi.DataItemIndex)
                    EUS_MessageID = dr("EUS_MessageID")
                    StatusID = dr("StatusID")

                    If (Me.MessageIdInView > 0) Then

                        If (Me.MessageIdInView = EUS_MessageID) Then
                            Me.SubjectInView = dr("Subject")
                            Me.MessageIdInView = EUS_MessageID

                        End If

                        If (dr("FromProfileId") <> Me.MasterProfileId) Then
                            Me.UserIdInView = dr("FromProfileId")
                            loginName = dr("LoginName")
                            'HasCommunication = clsUserDoes.HasCommunication(Me.UserIdInView, Me.MasterProfileId)
                            HasCommunication = Me.GetHasCommunicationDict(Me.UserIdInView)

                        ElseIf (dr("ToProfileId") <> Me.MasterProfileId) Then
                            ' used in case when user opens its sent item, and wants to send another message
                            Me.SendAnotherMessageToRecipient = dr("ToProfileId")
                            Me.UserIdInView = dr("ToProfileId")
                        End If

                    End If

                    Me.MessageIdListInView.Add(EUS_MessageID)

                    Dim lblAmountDate As Label = dvi.FindControl("lblAmountDate")
                    If (lblAmountDate IsNot Nothing) Then
                        If (dr("Amount") IsNot System.DBNull.Value AndAlso dr("Amount") > 0) Then
                            lblAmountDate.Text = "&euro;" & dr("Amount") & "&nbsp;" & Me.CurrentPageData.GetCustomString("OfferAmountDateWord")
                        Else
                            lblAmountDate.Visible = False
                        End If
                    End If


                    Dim imgFromLeft_dv As HtmlImage = dvi.FindControl("imgFromLeft")
                    imgFromLeft_dv.Src = ProfileHelper.GetProfileImageURL(dr("ProfileID"), dr("PhotoFileName"), dr("GenderId"), True, Me.IsHTTPS, PhotoSize.D150)

                    Dim lnkFromLeft_dv As HyperLink = dvi.FindControl("lnkFromLeft")
                    lnkFromLeft_dv.NavigateUrl = "/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(dr("LoginName").ToString()) & "&t=1"


                    Dim lblPhotoLevelHover_dv As Label = dvi.FindControl("lblPhotoLevelHover")
                    lblPhotoLevelHover_dv.Text = GetPhotoLevelText()
                    Dim lblPhotoLevelHover2_dv As Label = dvi.FindControl("lblPhotoLevelHover2")
                    lblPhotoLevelHover2_dv.Text = GetPhotoLevelText2()

                    Dim imgFromRight_dv As HtmlImage = dvi.FindControl("imgFromRight")
                    imgFromRight_dv.Src = imgFromLeft_dv.Src

                    Dim lnkFromRight_dv As HyperLink = dvi.FindControl("lnkFromRight")
                    lnkFromRight_dv.NavigateUrl = "javascript:void(0);" ' "~/Members/Profile.aspx?p=" & HttpUtility.UrlEncode(Eval("LoginName").ToString())

                    If (FromRight_NavigateUrl = "#" AndAlso dr("ProfileID") = Me.MasterProfileId) Then
                        FromRight_NavigateUrl = lnkFromRight_dv.NavigateUrl
                        FromRight_ImageSrc = imgFromRight_dv.Src
                    End If

                    Dim lblDateTime As Literal = dvi.FindControl("lblDateTime")
                    Dim dateDescrpt As String = AppUtils.GetMessageDateDescription(dr("DateTimeToCreate"), Me.globalStrings)
                    lblDateTime.Text = dateDescrpt

                    'Dim lblSubjectTitle As Label = dvi.FindControl("lblSubjectTitle")
                    'lblSubjectTitle.Text = CurrentPageData.GetCustomString("lblYourMsgSubject")

                    Dim lblTextTitle As Literal = dvi.FindControl("lblTextTitle")
                    lblTextTitle.Text = CurrentPageData.GetCustomString("ConversationMessageTextTitle")

                    'Dim lblCR As Label = dvi.FindControl("lblCR")
                    'lblCR.Text = CurrentPageData.GetCustomString("lblCopyMessageRead")

                    Dim btnDelete As HyperLink = dvi.FindControl("btnDelete")
                    btnDelete.Text = CurrentPageData.GetCustomString("btnDelete")
                    btnDelete.ToolTip = Me.CurrentPageData.GetCustomString("ListItemDeleteButtonText")
                    btnDelete.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?d=1&onemsg=1&vw=" & HttpUtility.UrlEncode(loginName) & "&msg=" & EUS_MessageID & "&rowFrom=" & Me.RowNumberMin & "&rowTo=" & Me.RowNumberMax)
                    If (dr("IsDeleted").ToString() = "1" OrElse dr("IsDeleted").ToString() = "True") Then
                        btnDelete.Attributes.Add("onclick", "return deleteMessage(this,event,1);")
                    Else
                        btnDelete.Attributes.Add("onclick", "return deleteMessage(this,event,0);")
                    End If

                    Dim lblScrollto As Label = dvi.FindControl("lblScrollto")
                    If (cnt = dvConversation.Items.Count - 1) Then
                        lblScrollto.Visible = True
                        ScrollToElem = lblScrollto.ClientID
                    Else
                        lblScrollto.Visible = False
                    End If
                    'ScrollToElem = lblScrollto.ClientID
                    'ScrollToElem = updCnv.ClientID


                    Dim btnOpen As HyperLink = dvi.FindControl("btnOpen")
                    btnOpen.Visible = False

                    ' no communication allowed and message is unread



                    If (StatusID = 0 AndAlso Not HasCommunication AndAlso Me.IsMale) Then
                        mostRecentMessageIndex = cnt
                        btnOpen.Visible = True


                        '' when the message is sent by women from different country
                        '' display warning message
                        '' to man
                        If HasRequiredCredits_To_UnlockMessage Then

                            If (country Is Nothing) Then
                                country = Me.GetCurrentProfile().Country
                                'lblWarningMessage.Text = Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.Message")
                                'lblWarningMessage.Text = lblWarningMessage.Text.Replace("[COUNTRY]", ProfileHelper.GetCountryName(dr("Country").ToString()))
                                'chkSuppressWarning.Text = Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.Message.Checkbox")
                                'btnContinue.Text = Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.Message.Continue")
                                'btnCancel.Text = Me.CurrentPageData.GetCustomString("SuppressWarning_WhenReadingMessageFromDifferentCountry.Warning.Message.Cancel")
                            End If

                            If (dr("Country").ToString() <> country) Then
                                If (SuppressWarning Is Nothing) Then
                                    SuppressWarning_Country = dr("Country").ToString()
                                    SuppressWarning = clsProfilesPrivacySettings.GET_SuppressWarning_WhenReadingMessageFromDifferentCountry(Me.MasterProfileId)
                                End If
                                If (clsNullable.NullTo(SuppressWarning) = False) Then
                                    Dim popupMessage As String = "return showWarning(event,this);"
                                    btnOpen.Attributes.Add("onclick", popupMessage)
                                End If
                            End If
                        End If

                        Dim lblSubject As Label = dvi.FindControl("lblSubject")
                        lblSubject.Text = "" 'Me.CurrentPageData.GetCustomString("Message.Locked.Subject.Text") ' "Subject - Open to read"

                        Dim lblText As Label = dvi.FindControl("lblText")
                        lblText.Text = Me.CurrentPageData.GetCustomString("Message.Locked.Body.Text") '"Text - Open to read"

                        'ElseIf (StatusID = 0 AndAlso Me.IsFemale) Then

                        '    btnOpen.Visible = True
                        '    btnOpen.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?read=once&onemsg=1&vw=" & HttpUtility.UrlEncode(loginName) & "&unmsg=" & EUS_MessageID & "&rowFrom=" & Me.RowNumberMin & "&rowTo=" & Me.RowNumberMax)
                        '    btnOpen.Text = Me.CurrentPageData.GetCustomString("CommandViewMessageText")

                        '    Dim lblSubject As Label = dvi.FindControl("lblSubject")
                        '    lblSubject.Text = "" 'Me.CurrentPageData.GetCustomString("Message.Locked.Subject.Text") ' "Subject - Open to read"

                        '    Dim lblText As Label = dvi.FindControl("lblText")
                        '    lblText.Text = Me.CurrentPageData.GetCustomString("Message.Locked.Body.Text") '"Text - Open to read"

                    Else

                        Dim lblSubject As Label = dvi.FindControl("lblSubject")
                        lblSubject.Text = dr("subject")

                        Dim lblText As Label = dvi.FindControl("lblText")
                        lblText.Text = dr("Body")

                    End If


                    If (HasCommunication AndAlso StatusID = 0 AndAlso EUS_MessageID > 0) Then
                        Dim DateTimeToCreate As DateTime? = clsUserDoes.GetMessage_DateTimeToCreate(EUS_MessageID)

                        Dim markRead As Boolean = True
                        'If ((DateTime.UtcNow - DateTimeToCreate.Value).TotalSeconds < 30) Then
                        '    ' try to avoid setting message to read, if it was created just few seconds before
                        '    If (Not SuspendedMessage.IsSuspended(EUS_MessageID)) Then
                        '        markRead = False
                        '        SuspendedMessage.AddMessageID(EUS_MessageID)
                        '        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "SuspendedMessage", "notifyAboutSuspendedMessage();" & vbCrLf, True)
                        '    Else
                        '        SuspendedMessage.RemoveMessageID(EUS_MessageID)
                        '    End If
                        'End If

                        'If (markRead) Then
                        clsUserDoes.MarkMessageRead(EUS_MessageID)
                        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "NewMessage", "notifyAboutNewMessage(" & EUS_MessageID & ");" & vbCrLf, True)
                        'End If
                    End If

                Catch ex As NullReferenceException
                    'WebErrorMessageBox(Me, ex, "dvConversation_DataBound")
                Catch ex As Exception
                    WebErrorMessageBox(Me, ex, "dvConversation_DataBound")
                End Try
            Next

            If (mostRecentMessageIndex > -1) Then
                Dim availableMessages As Integer = 0
                Dim isFreeMessages As Boolean = False
                Dim isSubscriptionMessages As Boolean = False


                If (Not HasRequiredCredits_To_UnlockMessage) Then
                    availableMessages = GetSubsriptionAvailableMessages(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                    If (availableMessages > 0) Then isSubscriptionMessages = True

                    If (Not isSubscriptionMessages) Then
                        availableMessages = Me.GetMembersFreeMessagesAvailableToREAD(Me.MasterProfileId, Me.UserIdInView, Me.SessionVariables.MemberData.Country)
                        If (availableMessages > 0) Then isFreeMessages = True
                    End If
                End If

                'If (availableMessages < 1) Then
                '    mostRecentMessageIndex = -1
                'End If
                If (mostRecentMessageIndex > 0) Then
                    availableMessages = mostRecentMessageIndex + 1
                End If

                For cnt = mostRecentMessageIndex To 0 Step -1
                    Dim dvi As System.Web.UI.WebControls.ListViewDataItem = dvConversation.Items(cnt)
                    Dim dr As DataRowView = dvConversationData(dvi.DataItemIndex)
                    Dim btnOpen As HyperLink = dvi.FindControl("btnOpen")
                    EUS_MessageID = dr("EUS_MessageID")

                    If (btnOpen.Visible) Then

                        If (Not isSubscriptionMessages AndAlso Not isFreeMessages) Then
                            btnOpen.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?read=once&onemsg=1&vw=" & HttpUtility.UrlEncode(loginName) & "&unmsg=" & EUS_MessageID & "&rowFrom=" & Me.RowNumberMin & "&rowTo=" & Me.RowNumberMax)
                            btnOpen.Text = Me.CurrentPageData.GetCustomString("UnlockMessageOnce_Short_Text")
                            btnOpen.Text = BasePage.ReplaceCommonTookens(btnOpen.Text, loginName)

                        ElseIf (isSubscriptionMessages) Then
                            btnOpen.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?read=once&subscription=1&onemsg=1&vw=" & HttpUtility.UrlEncode(loginName) & "&unmsg=" & EUS_MessageID & "&rowFrom=" & Me.RowNumberMin & "&rowTo=" & Me.RowNumberMax)
                            btnOpen.Text = Me.CurrentPageData.GetCustomString("UnlockMessageWithSubscription_Short_Text")
                            btnOpen.CssClass = btnOpen.CssClass & " subscription-free-msg"

                        ElseIf (isFreeMessages) Then
                            btnOpen.NavigateUrl = ResolveUrl("~/Members/Conversation.aspx?read=once&free=1&onemsg=1&vw=" & HttpUtility.UrlEncode(loginName) & "&unmsg=" & EUS_MessageID & "&rowFrom=" & Me.RowNumberMin & "&rowTo=" & Me.RowNumberMax)
                            btnOpen.Text = Me.CurrentPageData.GetCustomString("ReadFreeMessage_Short_Text")
                            btnOpen.CssClass = btnOpen.CssClass & " free-msg"
                        End If

                        availableMessages = availableMessages - 1
                        If (availableMessages = 0) Then Exit For

                    End If
                Next
            End If

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub

    'Public Sub SuspendMessageRead(messageID As Long)
    '    If (Session("SuspendMessageRead") IsNot Nothing) Then

    '    End If
    'End Sub

    Protected Function IsMessageHidden(DataItem As DataRowView) As String
        Dim class1 As String = ""
        Dim HasCommunication As Boolean = GetHasCommunicationDict(DataItem("ProfileID"))
        If (Not HasCommunication AndAlso DataItem("StatusID") = 0 AndAlso Me.IsMale) Then
            class1 = "closed"
        ElseIf (DataItem("StatusID") = 0 AndAlso Me.IsFemale) Then
            class1 = "closed"
        End If

        Return class1
    End Function

    Protected Function IsMessageNew(DataItem As DataRowView) As String
        Dim class1 As String = ""
        If (DataItem("StatusID") = 0 AndAlso Me.IsFemale) Then
            class1 = "new-message"
        End If

        Return class1
    End Function

    Private Property HasCommunicationDict As Dictionary(Of Integer, Boolean)
    Protected Function GetHasCommunicationDict(otherProfileID As Integer, Optional force As Boolean = False) As Boolean
        Dim HasCommunication As Boolean

        If (HasCommunicationDict Is Nothing) Then HasCommunicationDict = New Dictionary(Of Integer, Boolean)
        If (Not HasCommunicationDict.ContainsKey(otherProfileID) OrElse force) Then

            HasCommunication = GetHasCommunication(otherProfileID)
            If (force) Then
                HasCommunicationDict(otherProfileID) = HasCommunication
            Else
                HasCommunicationDict.Add(otherProfileID, HasCommunication)
            End If

        ElseIf (HasCommunicationDict.ContainsKey(otherProfileID)) Then

            HasCommunication = HasCommunicationDict(otherProfileID)

        End If

        Return HasCommunication
    End Function
    Private Function GetEUS_ProfilePhotosLevel() As EUS_ProfilePhotosLevel
        Dim phtLvl As EUS_ProfilePhotosLevel = Nothing
        Try

            phtLvl = (From itm In Me.CMSDBDataContext.EUS_ProfilePhotosLevels
                                                   Where itm.FromProfileID = Me.UserIdInView AndAlso itm.ToProfileID = Me.MasterProfileId
                                                   Select itm).FirstOrDefault()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return phtLvl
    End Function
    Private Function GetEUS_ProfilePhotosLevel2() As EUS_ProfilePhotosLevel
        Dim phtLvl As EUS_ProfilePhotosLevel = Nothing
        Try

            phtLvl = (From itm In Me.CMSDBDataContext.EUS_ProfilePhotosLevels
                                                   Where itm.FromProfileID = Me.MasterProfileId AndAlso itm.ToProfileID = Me.UserIdInView
                                                   Select itm).FirstOrDefault()

        Catch ex As Exception
            WebErrorMessageBox(Me, ex, "")
        End Try
        Return phtLvl
    End Function
    Protected Property UserInView_IsMale As Boolean
        Get
            Return ViewState("UserInView_IsMale")
        End Get
        Set(value As Boolean)
            ViewState("UserInView_IsMale") = value
        End Set
    End Property
    Private Function GetPhotoLevelText() As String
        Dim tmp As String = ""
        Try


            Dim photoLevelSet As Integer = 0
            Dim phtLvl As EUS_ProfilePhotosLevel = GetEUS_ProfilePhotosLevel()
            If (phtLvl IsNot Nothing AndAlso phtLvl.PhotoLevelID > 0) Then
                photoLevelSet = phtLvl.PhotoLevelID
            End If

            If (photoLevelSet > 0) Then
                If Me.UserInView_IsMale Then
                    tmp = CurrentPageData.VerifyCustomString("MALE_lblPhotoLevelTitle.Private")
                Else
                    tmp = CurrentPageData.VerifyCustomString("FEMALE_lblPhotoLevelTitle.Private")
                End If
            Else
                If Me.UserInView_IsMale Then
                    tmp = CurrentPageData.VerifyCustomString("MALE_lblPhotoLevelTitle.Public")
                Else
                    tmp = CurrentPageData.VerifyCustomString("FEMALE_lblPhotoLevelTitle.Public")
                End If
            End If
            If (tmp.Trim() = "") Then
                If Me.UserInView_IsMale Then
                    tmp = CurrentPageData.VerifyCustomString("MALE_lblPhotoLevelTitle_ND")
                Else
                    tmp = CurrentPageData.VerifyCustomString("FEMALE_lblPhotoLevelTitle_ND")
                End If
            End If

            tmp = tmp.
            Replace("[LEVEL]", photoLevelSet).
                        Replace(" ", "&nbsp;")
        Catch ex As Exception

        End Try
        Return tmp
    End Function
    Private Function GetPhotoLevelText2() As String
        Dim tmp As String = ""
        Dim username As String = ""
        Dim Ismale As Boolean = False
        Try
          

            Dim photoLevelSet As Integer = 0
            Dim phtLvl As EUS_ProfilePhotosLevel = GetEUS_ProfilePhotosLevel2()
            If (phtLvl IsNot Nothing AndAlso phtLvl.PhotoLevelID > 0) Then
                photoLevelSet = phtLvl.PhotoLevelID
            End If

            If (photoLevelSet > 0) Then
                If Me.IsMale Then
                    tmp = CurrentPageData.VerifyCustomString("MALE_lblPhotoLevelTitleOther.Private")
                Else
                    tmp = CurrentPageData.VerifyCustomString("FEMALE_lblPhotoLevelTitleOther.Private")
                End If
            Else
                If Me.IsMale Then
                    tmp = CurrentPageData.VerifyCustomString("MALE_lblPhotoLevelTitleOther.Public")
                Else
                    tmp = CurrentPageData.VerifyCustomString("FEMALE_lblPhotoLevelTitleOther.Public")
                End If
            End If
            If (tmp.Trim() = "") Then
                If Me.IsMale Then
                    tmp = CurrentPageData.VerifyCustomString("MALE_lblPhotoLevelTitle_NDOther")
                Else
                    tmp = CurrentPageData.VerifyCustomString("FEMALE_lblPhotoLevelTitle_NDOther")
                End If
            End If
         
            Try
                ''  Dim pr As EUS_Profile = Nothing
                username = (From itm In Me.CMSDBDataContext.EUS_Profiles
                Where itm.MirrorProfileID = Me.UserIdInView OrElse itm.ProfileID = Me.UserIdInView
                                                   Select itm.LoginName).FirstOrDefault()
                'If pr IsNot Nothing Then
                '    username = pr.LoginName
                '    Ismale = IsFemale
                '    Ismale = Me.IsMale
                'End If
            Catch ex As Exception

            End Try
            tmp = tmp.
            Replace("[LEVEL]", photoLevelSet).
                        Replace(" ", "&nbsp;").Replace("[UserName]", username)

        Catch ex As Exception

        End Try
        Return tmp
    End Function
End Class




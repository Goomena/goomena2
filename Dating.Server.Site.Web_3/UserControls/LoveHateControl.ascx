﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="LoveHateControl.ascx.vb" 
    Inherits="Dating.Server.Site.Web.LoveHateControl" %>
<script type="text/javascript">
    var animating_elemnts_list = new Array();
    var img_zodiacs = new Array();
   

</script>
<div id="<%= Mybase.UsersListView.toString() %>">

  
    <asp:panel ID="vwLoveHateVote" runat="server">
       
<div id="LoveHate-results">
<asp:Repeater ID="rptLoveHate" runat="server">
   <ItemTemplate>
<div class="s_item <%# Eval("AddItemCssClass")%>" login="<%# Eval("OtherMemberLoginName")%>">
    <div class="left" ID="dLeft" runat="server" EnableViewState="false">
         <div class="info">
            <h2><asp:HyperLink ID="hl" runat="server" CssClass="login-name" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>' /></h2>
            <div class="clear"></div>
             <asp:label ID="LblAge" runat="server" CssClass="Age"  Text='<%# Eval("OtherMemberAge")%>' />&nbsp;<asp:label ID="lblAgeText" runat="server" CssClass="AgeText"  Text='<%# Eval("YearsOldText")%>' />
         </div>
        <div class="photo">
            <div class="d_big_photo <%# Eval("PhotoCssClass")%>">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>
                    '>
                    <img src="<%# Eval("OtherMemberImageUrl") %>" class="LoveHateProfileImg"/>
                    <span class="<%# Eval("PhotoOverlayCssClass")%>"></span>
                 </asp:HyperLink>
           <span class="PlusSymbol">+1</span>
                 <div class="tooltip tooltip_LoveHate_photo">
                <div class="tooltip_text block"><%# Eval("PhotosCountString")%></div>
            </div>
           </div>
           
        </div>
        <div id="LoveHateButtons" class="lvb <%# Eval("BtnVoteCssClass")%>">
           
                <asp:LinkButton ID="lnkLove" runat="server" CssClass='<%# Eval("btnlnkLoveCssClass")%>' 
                EnableViewState='<%# Eval("AllowVote")%>'
                     Enabled='<%# Eval("AllowVote")%>'
                Visible=true CommandArgument='<%# Eval("OtherMemberProfileID") %>' 
                CommandName="Love"> <div class="LoveHateCounter" >
                <div class="Box">
                   <asp:Label ID="Label1" runat ="server" CssClass="LoveCounter" Text='<%# Eval("OtherProfileLoves")%>'></asp:Label>
                 </div>
              </div></asp:LinkButton>
         
            <div id="GraySpace"></div>
                  <asp:LinkButton ID="lnkHate" runat="server" CssClass='<%# Eval("btnlnkHateCssClass")%>' 
                EnableViewState='<%# Eval("AllowVote")%>'
                      Enabled='<%# Eval("AllowVote")%>'
                Visible=true CommandArgument='<%# Eval("OtherMemberProfileID") %>' 
                CommandName="Hate"><div  class="LoveHateCounter" >
                 <div class="Box">
                     <asp:Label ID="Label2" runat ="server" CssClass="HateCounter" Text='<%# Eval("OtherProfileHates")%>'></asp:Label>
                 </div>
             </div></asp:LinkButton>
                    <div class="LoveHateNoVote" style="display:none;">
                       <div class="LoveHateNoVoteText"><%# Eval("DissallowVoteText")%></div>
                     </div>  
             </div>
             
    </div>
   
    
</div>
</ItemTemplate>
</asp:Repeater>
    <div class="clear"></div>
</div>
    </asp:panel>

    <%--<asp:View ID="vwLoveHateWinners" runat="server">
       
<div id="LoveHate-Winners">
<asp:Repeater ID="rptWinners" runat="server">
   <ItemTemplate>
<div class="s_item <%# Eval("AddItemCssClass")%>" login="<%# Eval("OtherMemberLoginName")%>">
    <div class="left" ID="dLeft" runat="server" EnableViewState="false">
         <div class="info">
            <h2><asp:HyperLink ID="hl" runat="server" CssClass="login-name" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>' Text='<%# Eval("OtherMemberLoginName")%>' /></h2>
            <div class="clear"></div>
             <asp:label ID="LblAge" runat="server" CssClass="Age"  Text='<%# Eval("OtherMemberAge")%>' />&nbsp;<asp:label ID="lblAgeText" runat="server" CssClass="AgeText"  Text='<%# Eval("YearsOldText")%>' />
         </div>
        <div class="photo">
            <div class="d_big_photo <%# Eval("PhotoCssClass")%>">
                <asp:HyperLink ID="hl2" runat="server" NavigateUrl='<%# Eval("OtherMemberProfileViewUrl") %>'>
                    <img src="<%# Eval("OtherMemberImageUrl") %>" class="LoveHateProfileImg"/>
                    <span class="<%# Eval("PhotoOverlayCssClass")%>"></span>
                 </asp:HyperLink>
           <span class="PlusSymbol">+1</span>
                 <div class="tooltip tooltip_LoveHate_photo">
                <div class="tooltip_text block"><%# Eval("PhotosCountString")%></div>
            </div>
           </div>
           
        </div>
     </div>
   
    
</div>
</ItemTemplate>
</asp:Repeater>
    <div class="clear"></div>
</div>
    </asp:View>
--%>


          
 



<%--<script type="text/javascript">
    function LoadSearch() {

        $(".vip-png", '#search-results').tooltip({
            tooltipClass: "tooltip-VIP",
            content: "<%= Dating.Server.Site.Web.AppUtils.JS_PrepareString(Me.VIPTooltip) %>",
            show: { delay: 1500 }
        });
        $("img.round-img-116", '#search-results').mouseenter(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('a', '.info h2').addClass('hovered')
            }
        }).mouseleave(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('a', '.info h2').removeClass('hovered')
            }
        });
        $("a.login-name", '#search-results').mouseenter(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('.pr-photo-116-noshad', '.photo').addClass('hovered')
            }
        }).mouseleave(function () {
            var t = this;
            var p = $(t).parent();
            while (!p.is('.left')) { p = $(p).parent(); }
            if (p.length > 0) {
                p.find('.pr-photo-116-noshad', '.photo').removeClass('hovered')
            }
        });



        var scope = "#offers-list .l_new", scope2;
        scope = (($(".l_pending", "#offers-list").length > 0) ? "#offers-list .l_pending" : scope);
        scope = (($(".l_rejected", "#offers-list").length > 0) ? "#offers-list .l_rejected" : scope);
        scope = (($(".l_accepted", "#offers-list").length > 0) ? "#offers-list .l_accepted" : scope);
        scope2 = (($(".s_item", "#MyBlockedList").length > 0) ? "#MyBlockedList .s_item" : scope);

        var itms = $("a.btn", scope + " .left").css("width", "auto");
        if (itms.length == 0) { itms = $("a.btn", scope2 + " .bot_right_actions").css("width", "auto"); }
        for (var c = 0; c < itms.length; c++) {
            var w = $(itms[c]).width();
            $(itms[c]).css("width", "").removeClass("lnk130").removeClass("lnk180");
            if (w < 120)
                $(itms[c]).addClass("lnk130");
            else
                $(itms[c]).addClass("lnk180");
        }

        itms = $("a.dropdown-toggle", scope + " .left .btn-group").css("width", "auto");
        for (c = 0; c < itms.length; c++) {
            var w = $(itms[c]).width();
            $(itms[c]).css("width", "").removeClass("lnk130").removeClass("lnk180").removeClass("lnk107").removeClass("lnk116");
            if (w < 83)
                $(itms[c]).addClass("lnk107");
            else
                $(itms[c]).addClass("lnk116");
        }
    }

    function startAnimating() {
        if (animating_elemnts_list.length > 0) { setTimeout(startAnimateOnlineImg, 500); }
    }
    function processAnimating() {
        try {
            animating_elemnts_list = new Array();
            var online = $('.online-indicator', '#search-results');
            if (online.length > 0) {
                for (var c = 0; c < online.length; c++) {
                    animating_elemnts_list.push(online[c].id)
                }
                setTimeout(startAnimateOnlineImg, 500);
            }
        }
        catch (e) { }
    }
    (function () {
        LoadSearch()
        startAnimating();
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(processAnimating);
    })();
</script>--%>

</div>












<?php
/****************************************************************************************
* LiveZilla functions.internal.man.inc.php
* 
* Copyright 2012 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
* 
* Improper changes to this file may cause critical errors.
***************************************************************************************/ 

if(!defined("IN_LIVEZILLA"))
	die();

function setAvailability($_available)
{
	global $INTERNAL,$RESPONSE;
	if($INTERNAL[CALLER_SYSTEM_ID]->Level==USER_LEVEL_ADMIN)
	{
		if(!empty($_POST["p_del_ws"]) && file_exists(str_replace("config.inc","config.".$_POST["p_del_ws"].".inc",FILE_CONFIG)))
			@unlink(str_replace("config.inc","config.".$_POST["p_del_ws"].".inc",FILE_CONFIG));
		if(!empty($_available) && file_exists(FILE_SERVER_DISABLED))
			@unlink(FILE_SERVER_DISABLED);
		else if(empty($_available))
			createFile(FILE_SERVER_DISABLED,time(),false);
		$RESPONSE->SetStandardResponse(1,"");
	}
}

function setIdle($_idle)
{
	global $INTERNAL,$RESPONSE;
	if($INTERNAL[CALLER_SYSTEM_ID]->Level==USER_LEVEL_ADMIN)
	{
		if(empty($_idle) && file_exists(FILE_SERVER_IDLE))
			@unlink(FILE_SERVER_IDLE);
		else if(!empty($_idle))
			createFile(FILE_SERVER_IDLE,time(),true);
		$RESPONSE->SetStandardResponse(1,"");
	}
}

function getBannerList($list = "")
{
	global $VISITOR,$CONFIG,$RESPONSE;
	$result = queryDB(true,"SELECT * FROM `".DB_PREFIX.DATABASE_IMAGES."` ORDER BY `id` ASC,`online` DESC;");
	while($row = mysql_fetch_array($result, MYSQL_BOTH))
		$list .= "<button type=\"".base64_encode($row["button_type"])."\" name=\"".base64_encode($row["button_type"]."_".$row["id"]."_".$row["online"].".".$row["image_type"])."\" data=\"".base64_encode($row["data"])."\" />\r\n";
	$RESPONSE->SetStandardResponse(1,"<button_list>".$list."</button_list>");
}

function getTranslationData($translation = "")
{
	global $LZLANG,$RESPONSE;
	if(!(isset($_POST["p_int_trans_iso"]) && (strlen($_POST["p_int_trans_iso"])==2||strlen($_POST["p_int_trans_iso"])==5)))
	{
		$RESPONSE->SetStandardResponse(1,"");
		return;
	}
	$langid = $_POST["p_int_trans_iso"];
	if(strpos($langid,"..") === false && strlen($langid) <= 6)
	{
		$file = getLocalizationFileString($langid);
	
		include($file);
		$translation .= "<language key=\"".base64_encode($langid)."\">\r\n";
		foreach($LZLANG as $key => $value)
			$translation .= "<val key=\"".base64_encode($key)."\">".base64_encode($value)."</val>\r\n";
		$translation .= "</language>\r\n";
		$RESPONSE->SetStandardResponse(1,$translation);
	}
	else
		$RESPONSE->SetStandardResponse(0,$translation);
}

function updatePredefinedMessages($_prefix,$_counter = 0)
{
	global $GROUPS,$INTERNAL;
	queryDB(true,"DELETE FROM `".$_prefix.DATABASE_PREDEFINED."`");
	
	$tpm_types = array("g"=>$GROUPS,"u"=>$INTERNAL);
	$pms = array();
	foreach($tpm_types as $type => $objectlist)
		if(!empty($objectlist))
			foreach($objectlist as $id => $object)
			{
				$pms[$type.$id] = array();
				foreach($_POST as $key => $value)
					if(strpos($key,"p_db_pm_".$type."_" . $id . "_")===0)
					{
						$parts = explode("_",$key);
						if(!isset($pms[$type.$id][$parts[5]]))
						{
							$pms[$type.$id][$parts[5]] = new PredefinedMessage();
							$pms[$type.$id][$parts[5]]->GroupId = ($type=="g") ? $id : "";
							$pms[$type.$id][$parts[5]]->UserId = ($type=="u") ? $id : "";
							$pms[$type.$id][$parts[5]]->LangISO = $parts[5];
						}
						$pms[$type.$id][$parts[5]]->XMLParamAlloc($parts[6],$value);
					}
			}

	foreach($pms as $oid => $messages)
		foreach($messages as $iso => $message)
		{
			$message->Id = $_counter++;
			$message->Save($_prefix);
		}
}

function setManagement($_prefix,$_create=false,$_test=false)
{
	global $INTERNAL,$RESPONSE,$GROUPS,$CONFIG;
	if($INTERNAL[CALLER_SYSTEM_ID]->Level == USER_LEVEL_ADMIN || in_array($CONFIG["gl_host"],$INTERNAL[CALLER_SYSTEM_ID]->WebsitesUsers))
	{
		$count = 0;
		$processed = "";
		if(empty($_POST["p_operators_" . $count . "_id"]))
			return;

		while(isset($_POST["p_operators_" . $count . "_id"]))
		{
			if(!($res = queryDB(true,"INSERT INTO `".$_prefix.DATABASE_OPERATORS."` (`id`, `system_id`, `fullname`, `description`, `email`, `permissions`, `webspace`, `password`, `level`, `visitor_file_sizes`, `groups`, `groups_status`, `groups_hidden`,`reposts`, `languages`, `auto_accept_chats`, `login_ip_range`, `websites_users`, `websites_config`) VALUES ('".@mysql_real_escape_string($_POST["p_operators_" . $count . "_id"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_system_id"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_fullname"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_description"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_email"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_permissions"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_webspace"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_password"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_level"])."','','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_groups"])."','','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_groups_hidden"])."','','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_languages"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_aac"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_lipr"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_websites_users"])."','".@mysql_real_escape_string($_POST["p_operators_" . $count . "_websites_config"])."');")))
				queryDB(true,"UPDATE `".$_prefix.DATABASE_OPERATORS."` SET `fullname`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_fullname"])."',`description`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_description"])."',`email`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_email"])."',`permissions`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_permissions"])."',`webspace`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_webspace"])."',`level`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_level"])."',`groups`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_groups"])."',`groups_hidden`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_groups_hidden"])."',`languages`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_languages"])."',`login_ip_range`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_lipr"])."',`auto_accept_chats`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_aac"])."',`websites_users`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_websites_users"])."',`websites_config`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_websites_config"])."' WHERE `id`='".@mysql_real_escape_string($_POST["p_operators_" . $count . "_id"])."' LIMIT 1;");
			if(!empty($processed))
				$processed .= " AND ";
			$processed .= "`id` != '".@mysql_real_escape_string($_POST["p_operators_" . $count . "_id"])."'";
			$count++;
		}
		queryDB(true,"DELETE FROM `".$_prefix.DATABASE_OPERATORS."` WHERE (".$processed.");");
		
		$processed = "";
		$count = $counter = 0;
		
		while(isset($_POST["p_groups_" . $count . "_id"]))
		{
			if(!($res = queryDB(true,"INSERT INTO `".$_prefix.DATABASE_GROUPS."` (`id`, `dynamic`, `description`, `external`, `internal`, `created`, `email`, `standard`, `opening_hours`, `functions`, `chat_inputs_hidden`, `ticket_inputs_hidden`, `chat_inputs_required`, `ticket_inputs_required`, `max_chats`, `hide_chat_group_selection`, `hide_ticket_group_selection`, `visitor_filters`) VALUES ('".@mysql_real_escape_string($_POST["p_groups_" . $count . "_id"])."',0,'".@mysql_real_escape_string($_POST["p_groups_" . $count . "_description"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_external"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_internal"])."',".time().",'".@mysql_real_escape_string($_POST["p_groups_" . $count . "_email"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_standard"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_opening_hours"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_functions"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_chat_inputs_hidden"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_ticket_inputs_hidden"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_chat_inputs_required"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_ticket_inputs_required"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_max_chats"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_hcgs"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_htgs"])."','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_visitor_filters"])."');")))
				queryDB(true,"UPDATE `".$_prefix.DATABASE_GROUPS."` SET `id`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_id"])."',`name`='',`owner`='',`dynamic`=0,`description`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_description"])."',`external`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_external"])."',`internal`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_internal"])."',`email`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_email"])."',`standard`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_standard"])."',`opening_hours`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_opening_hours"])."',`functions`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_functions"])."',`chat_inputs_hidden`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_chat_inputs_hidden"])."',`ticket_inputs_hidden`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_ticket_inputs_hidden"])."',`chat_inputs_required`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_chat_inputs_required"])."',`ticket_inputs_required`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_ticket_inputs_required"])."',`max_chats`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_max_chats"])."',`hide_chat_group_selection`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_hcgs"])."',`hide_ticket_group_selection`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_htgs"])."',`visitor_filters`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_visitor_filters"])."' WHERE `id`='".@mysql_real_escape_string($_POST["p_groups_" . $count . "_id"])."' LIMIT 1;");
			
			if(!empty($processed))
				$processed .= " AND ";
			$processed .= "`id` != '".@mysql_real_escape_string($_POST["p_groups_" . $count . "_id"])."'";
			if($_create)
			{
				$pdm = new PredefinedMessage($counter++,'',$_POST["p_groups_" . $count . "_id"], 'EN', 'Hello, my name is %name%. Do you need help? Start Live-Chat now to get assistance.', 'Hello, my name is %name%. Do you need help? Start Live-Chat now to get assistance.','Hello %external_name%, my name is %name%, how may I help you?', 'Website Operator %name% would like to redirect you to this URL:\r\n\r\n%url%', 'Website Operator %name% would like to redirect you to this URL:\r\n\r\n%url%','','', '1', '1', '1', '1',"Chat Transcript\r\n%website_name% / %group_description%\r\n\r\nDate: %localdate%\r\n-------------------------------------------------------------\r\n%details%\r\nChat reference number: %chat_id%\r\n-------------------------------------------------------------\r\n%mailtext%","Thank you, we have received your message!\r\nWe will get in touch with you as soon as possible.\r\n-------------------------------------------------------------\r\nDate: %localdate%\r\n-------------------------------------------------------------\r\n%details%\r\nGroup: %group_description%\r\n-------------------------------------------------------------\r\n%mailtext%");
				$pdm->Save($_prefix);
				
				$pdm = new PredefinedMessage($counter++,'',$_POST["p_groups_" . $count . "_id"], 'DE', utf8_encode("Guten Tag, meine Name ist %name%. Ben�tigen Sie Hilfe? Gerne berate ich Sie in einem Live Chat."), utf8_encode("Guten Tag, meine Name ist %name%. Ben�tigen Sie Hilfe? Gerne berate ich Sie in einem Live Chat."),"Guten Tag %external_name%, mein Name ist %name% wie kann ich Ihnen helfen?", utf8_encode("Ein Betreuer dieser Webseite (%name%) m�chte Sie auf einen anderen Bereich weiterleiten:\r\n\r\n%url%"),utf8_encode("Ein Betreuer dieser Webseite (%name%) m�chte Sie auf einen anderen Bereich weiterleiten:\r\n\r\n%url%"),'','', '1', '0', '1', '1',"Mitschrift Ihres Chats\r\n%website_name% / %group_description%\r\n\r\nDatum: %localdate%\r\n-------------------------------------------------------------\r\n%details%\r\nChat Referenz-Nummer: %chat_id%\r\n-------------------------------------------------------------\r\n%mailtext%","Vielen Dank, wir haben Ihre Nachricht erhalten und werden uns umgehend mit Ihnen in Verbindung setzen.\r\n-------------------------------------------------------------\r\nDatum: %localdate%\r\n-------------------------------------------------------------\r\n%details%\r\nAbteilung: %group_description%\r\n-------------------------------------------------------------\r\n%mailtext%");
				$pdm->Save($_prefix);
				
				//queryDB(true,"INSERT INTO `".$_prefix.DATABASE_PREDEFINED."` (`id` ,`internal_id` ,`group_id` ,`lang_iso` ,`invitation_manual`, `invitation_auto` ,`welcome` ,`website_push_manual`, `website_push_auto`, `chat_info`, `ticket_info` ,`browser_ident` ,`is_default` ,`auto_welcome` ,`editable`, `email_chat_transcript`, `email_ticket`)VALUES ('".@mysql_real_escape_string($counter++)."', '','".@mysql_real_escape_string($_POST["p_groups_" . $count . "_id"])."', 'DE', '".@mysql_real_escape_string(utf8_encode("Guten Tag, meine Name ist %name%. Ben�tigen Sie Hilfe? Gerne berate ich Sie in einem Live Chat."))."', '".@mysql_real_escape_string(utf8_encode("Guten Tag, meine Name ist %name%. Ben�tigen Sie Hilfe? Gerne berate ich Sie in einem Live Chat."))."','Guten Tag %external_name%, mein Name ist %name% wie kann ich Ihnen helfen?', '".@mysql_real_escape_string(utf8_encode("Ein Betreuer dieser Webseite (%name%) m�chte Sie auf einen anderen Bereich weiterleiten:\r\n\r\n%url%"))."','".@mysql_real_escape_string(utf8_encode("Ein Betreuer dieser Webseite (%name%) m�chte Sie auf einen anderen Bereich weiterleiten:\r\n\r\n%url%"))."','','', '1', '0', '1', '1','".@mysql_real_escape_string("Mitschrift Ihres Chats\r\n%website_name% / %group_description%\r\n\r\nDatum: %localdate%\r\n-------------------------------------------------------------\r\n%details%\r\nChat Referenz-Nummer: %chat_id%\r\n-------------------------------------------------------------\r\n%mailtext%")."','".@mysql_real_escape_string("Vielen Dank, wir haben Ihre Nachricht erhalten und werden uns umgehend mit Ihnen in Verbindung setzen.\r\n-------------------------------------------------------------\r\nDatum: %localdate%\r\n-------------------------------------------------------------\r\n%details%\r\nAbteilung: %group_description%\r\n-------------------------------------------------------------\r\n%mailtext%")."');");
			}
			$count++;
		}
		queryDB(true,"DELETE FROM `".$_prefix.DATABASE_GROUPS."` WHERE (".$processed.") AND `dynamic`=0;");
		queryDB(true,"DELETE FROM `".$_prefix.DATABASE_OPERATOR_LOGINS."`;");
		getData(true,true,true,false);
		
		if((!$_test && !$_create) || !empty($_POST["p_db_pm"]))
			updatePredefinedMessages($_prefix);

		if(isset($_POST[POST_INTERN_EDIT_USER]))
		{
			$combos = explode(";",$_POST[POST_INTERN_EDIT_USER]);
			for($i=0;$i<count($combos);$i++)
				if(strpos($combos[$i],",") !== false)
				{
					$vals = explode(",",$combos[$i]);
					if(strlen($vals[1])>0)
						$INTERNAL[$vals[0]]->ChangePassword($vals[1],true);
					if($vals[2] == 1)
						$INTERNAL[$vals[0]]->SetPasswordChangeNeeded(true);
				}
		}
		setIdle(0);
		$RESPONSE->SetStandardResponse(1,"");
	}
}

function setConfig($id = 0)
{
	global $INTERNAL,$RESPONSE,$STATS,$CONFIG;
	if($INTERNAL[CALLER_SYSTEM_ID]->Level == USER_LEVEL_ADMIN || in_array($CONFIG["gl_host"],$INTERNAL[CALLER_SYSTEM_ID]->WebsitesConfig))
	{
		if(STATS_ACTIVE && isset($_POST[POST_INTERN_RESET_STATS]) && $_POST[POST_INTERN_RESET_STATS]=="1")
			$STATS->ResetAll();
	
		$file = (ISSUBSITE || $INTERNAL[CALLER_SYSTEM_ID]->Level != USER_LEVEL_ADMIN) ? str_replace("config.inc","config.".SUBSITEHOST.".inc",FILE_CONFIG) : FILE_CONFIG;
		$id = createFile($file,base64_decode($_POST[POST_INTERN_UPLOAD_VALUE]),true);
		@touch(FILE_CONFIG);
		if(isset($_POST[POST_INTERN_SERVER_AVAILABILITY]))
			setAvailability($_POST[POST_INTERN_SERVER_AVAILABILITY]);
		
		$int = 1;
		while(isset($_POST["p_int_trans_iso" . "_" . $int]) && strpos($_POST["p_int_trans_iso" . "_" . $int],"..") === false)
		{
			$file = getLocalizationFileString($_POST["p_int_trans_iso" . "_" . $int],false);
			if(!isset($_POST[POST_INTERN_DOWNLOAD_TRANSLATION_DELETE . "_" . $int]))
				createFile($file, slashesStrip($_POST[POST_INTERN_DOWNLOAD_TRANSLATION_CONTENT . "_" . $int]), true);
			else
			{
				if(file_exists($file))
					@unlink($file);
				if(empty($CONFIG["gl_root"]))
					createFile($file,"",true);
			}
			$int++;
		}
	}
	removeSSpanFile(true);
	setIdle(0);
	$RESPONSE->SetStandardResponse($id,"");
}

function dataBaseTest($id=0)
{
	global $RESPONSE;
	$res = testDataBase($_POST[POST_INTERN_DATABASE_HOST],$_POST[POST_INTERN_DATABASE_USER],$_POST[POST_INTERN_DATABASE_PASS],$_POST[POST_INTERN_DATABASE_NAME],$_POST[POST_INTERN_DATABASE_PREFIX]);
	if(empty($res))
		$RESPONSE->SetStandardResponse(1,base64_encode(""));
	else
		$RESPONSE->SetStandardResponse(2,base64_encode($res));
}

function sendTestMail()
{
	global $RESPONSE,$CONFIG;
	$return = sendMail($CONFIG["gl_mail_sender"],$CONFIG["gl_mail_sender"],$CONFIG["gl_mail_sender"],"LiveZilla Test Mail","LiveZilla Test Mail");
	if($return==1)
		$RESPONSE->SetStandardResponse(1,base64_encode(""));
	else
		$RESPONSE->SetStandardResponse(2,base64_encode($return));
}

function createTables($id=0)
{
	global $RESPONSE,$GROUPS,$INTERNAL,$DB_CONNECTOR;
	if($INTERNAL[CALLER_SYSTEM_ID]->Level==USER_LEVEL_ADMIN)
	{
		$conndetails = array($_POST[POST_INTERN_DATABASE_HOST],$_POST[POST_INTERN_DATABASE_USER],$_POST[POST_INTERN_DATABASE_PASS]);
		$connection = @mysql_connect($conndetails[0],$conndetails[1],$conndetails[2]);
		//mysql_query("SET NAMES 'utf8'", $connection);
		if(!$connection)
		{
			$error = mysql_error();
			$RESPONSE->SetStandardResponse($id,base64_encode("Can't connect to database. Invalid host or login! (" . mysql_errno() . ((!empty($error)) ? ": " . $error : "") . ")"));
			return false;
		}
		else
		{
			mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'", $connection);
			$db_selected = mysql_select_db(@mysql_real_escape_string($_POST[POST_INTERN_DATABASE_NAME]),$connection);
			if(!$db_selected)
			{
				if(!empty($_POST[POST_INTERN_DATABASE_CREATE]))
				{
					$resultcr = @mysql_query("CREATE DATABASE `".@mysql_real_escape_string($_POST[POST_INTERN_DATABASE_NAME])."`",$connection);
					if(!$resultcr)
						$RESPONSE->SetStandardResponse($id,base64_encode(mysql_errno() . ": " . mysql_error()));
					else
					{
						unset($_POST[POST_INTERN_DATABASE_CREATE]);
						return createTables();
					}
				}
				else
	    			$RESPONSE->SetStandardResponse(2,base64_encode(mysql_errno() . ": " . mysql_error()));
			}
			else
			{
				$resultvc = @mysql_query("SELECT `version`,`chat_id`,`ticket_id` FROM `".@mysql_real_escape_string($_POST[POST_INTERN_DATABASE_PREFIX]).DATABASE_INFO."` ORDER BY `version` DESC LIMIT 1",$connection);
				if($rowvc = @mysql_fetch_array($resultvc, MYSQL_BOTH))
				{
					if(VERSION != $rowvc["version"] && !empty($rowvc["version"]))
					{
						$upres = initUpdateDatabase($rowvc["version"],$connection,$_POST[POST_INTERN_DATABASE_PREFIX]);
						if($upres === true)
						{
							$RESPONSE->SetStandardResponse(1,base64_encode(""));
							return true;
						}
					}
				}

				$resultv = @mysql_query("SELECT VERSION() as `mysql_version`",$connection);
				if(!$resultv)
				{
					$RESPONSE->SetStandardResponse($id,base64_encode(mysql_errno() . ": " . mysql_error() . "\r\n\r\nSQL: " . $sql));
					return false;
				}
				else
				{
					$mrow = @mysql_fetch_array($resultv, MYSQL_BOTH);
					$mversion = explode(".",$mrow["mysql_version"]);
					if(count($mversion) > 0 && $mversion[0] < MYSQL_NEEDED_MAJOR)
					{
						$RESPONSE->SetStandardResponse($id,base64_encode("LiveZilla requires MySQL version ".MYSQL_NEEDED_MAJOR." or greater. The MySQL version installed on your server is " . $mrow["mysql_version"]."."));
						return false;
					}
				}
				
				$resulti = @mysql_query("SHOW VARIABLES LIKE 'have_innodb'",$connection);
				if(!$resulti)
				{
					$RESPONSE->SetStandardResponse($id,base64_encode(mysql_errno() . ": " . mysql_error() . "\r\n\r\nSQL: " . $sql));
					return false;
				}
				else
				{
					$irow = @mysql_fetch_array($resulti, MYSQL_BOTH);
					if(strtolower($irow["Value"])!="yes")
					{
						$RESPONSE->SetStandardResponse($id,base64_encode("The MySQL storage engine InnoDB is disabled on your webserver. Please allow it and try again."));
						return false;
					}
				}
			
				$commands = explode("###",str_replace("<!--version-->",VERSION,str_replace("<!--prefix-->",$_POST[POST_INTERN_DATABASE_PREFIX],file_get_contents(LIVEZILLA_PATH . "_definitions/dump.lsql"))));
				foreach($commands as $sql)
				{
					if(empty($sql))
						continue;

					$result = mysql_query(trim($sql),$connection);
					if(!$result && mysql_errno() != 1050 && mysql_errno() != 1005 && mysql_errno() != 1062)
					{
						$RESPONSE->SetStandardResponse($id,base64_encode(mysql_errno() . ": " . mysql_error() . "\r\n\r\nSQL: " . $sql));
						return false;
					}
				}

				importButtons(PATH_IMAGES . "buttons/",$_POST[POST_INTERN_DATABASE_PREFIX],$connection);
				$DB_CONNECTOR = $connection;
				$RESPONSE->SetStandardResponse(1,base64_encode(""));
				return true;
			}
		}
	}
	return false;
}

function importButtons($_folder,$_prefix,$_connection)
{
	try
	{
		$buttons = getDirectory($_folder,".php",true);
		foreach($buttons as $button)
		{
			$parts = explode("_",$button);
			if(count($parts) == 3)
			{
				$type = ($parts[0]=="overlay") ? $parts[0] : "inlay";
				$id = intval($parts[1]);
				$online = explode(".",$parts[2]);
				$online = $online[0];
				$parts = explode(".",$button);
				$itype = $parts[1];
				mysql_query("INSERT INTO `".@mysql_real_escape_string($_prefix).DATABASE_IMAGES."` (`id`,`online`,`button_type`,`image_type`,`data`) VALUES ('".@mysql_real_escape_string($id)."','".@mysql_real_escape_string($online)."','".@mysql_real_escape_string($type)."','".@mysql_real_escape_string($itype)."','".@mysql_real_escape_string(fileToBase64($_folder . $button))."');",$_connection);
			}
		}
	}
	catch (Exception $e)
	{
		logit(serialize($e));
	}
}

function createTable($_sql,$_connection)
{
	$sql = "CREATE TABLE `".@mysql_real_escape_string($_POST[POST_INTERN_DATABASE_PREFIX]).$_sql;
	$result = mysql_query($sql,$_connection);
	if(!$result && mysql_errno() != 1050)
	{
		$RESPONSE->SetStandardResponse($id,base64_encode(mysql_errno() . ": " . mysql_error() . "\r\n\r\nSQL: " . $sql));
		return false;
	}
	return true;
}

function testDataBase($_host,$_user,$_pass,$_dbname,$_prefix)
{
	global $DB_CONNECTOR;
	if(!function_exists("mysql_connect"))
		return "PHP/MySQL extension is missing (php_mysql.dll)";
		
	$connection = @mysql_connect($_host,$_user,$_pass);
	@mysql_query("SET NAMES 'utf8'", $connection);
	if(!$connection)
	{
		$error = mysql_error();
		return "Can't connect to database. Invalid host or login! (" . mysql_errno() . ((!empty($error)) ? ": " . $error : "") . ")";
	}
	else
	{
		$db_selected = @mysql_select_db(@mysql_real_escape_string($_dbname),$connection);
		if (!$db_selected) 
    		return mysql_errno() . ": " . mysql_error();
		else
		{
			$resultv = @mysql_query("SELECT VERSION() as `mysql_version`",$connection);
			if(!$resultv)
				return mysql_errno() . ": " . mysql_error();
			else
			{
				$mrow = @mysql_fetch_array($resultv, MYSQL_BOTH);
				$mversion = explode(".",$mrow["mysql_version"]);
				if(count($mversion) > 0 && $mversion[0] < MYSQL_NEEDED_MAJOR)
					return "LiveZilla requires MySQL version ".MYSQL_NEEDED_MAJOR." or greater. The MySQL version installed on your server is " . $mrow["mysql_version"].".";
			}
			
			$result = @mysql_query("SELECT `version`,`chat_id`,`ticket_id` FROM `".@mysql_real_escape_string($_prefix).DATABASE_INFO."` ORDER BY `version` DESC LIMIT 1",$connection);
			$row = @mysql_fetch_array($result, MYSQL_BOTH);
			$version = $row["version"];
			if(!$result || empty($version))
				return "Cannot read the LiveZilla Database version. Please try to recreate the table structure. If you experience this message during installation process, please try to setup a prefix (for example lz_).";
				
			if($version != VERSION && SERVERSETUP)
			{
				$upres = initUpdateDatabase($version,$connection,$_prefix);
				if($upres !== true)
					return "Cannot update database structure from [".$_version."] to [".VERSION."]. Please make sure that the user " . $_user . " has the MySQL permission to ALTER tables in " . $_dbname .".\r\n\r\nError: " . $upres;
			}
			else if($version != VERSION)
				return "Invalid database version: ".$_version." (required: ".VERSION."). Please validate the database in the server administration panel first.\r\n\r\n";

			$DB_CONNECTOR = $connection;
			$result = @mysql_query("SELECT * FROM `".@mysql_real_escape_string($_prefix).DATABASE_OPERATORS."`",$connection);
			if(@mysql_num_rows($result) == 0)
				setManagement($_prefix,false,true);

			$rowmci = @mysql_fetch_array(@mysql_query("SELECT MAX(`chat_id`) as `mcid` FROM `".@mysql_real_escape_string($_prefix).DATABASE_CHAT_ARCHIVE."`",$connection), MYSQL_BOTH);
			$rowmti = @mysql_fetch_array(@mysql_query("SELECT MAX(`id`) as `mtid` FROM `".@mysql_real_escape_string($_prefix).DATABASE_TICKETS."`",$connection), MYSQL_BOTH);
			
			if(!empty($rowmci["mcid"]) && is_numeric($rowmci["mcid"]) && $rowmci["mcid"] > $row["chat_id"])
				@mysql_query("UPDATE `".@mysql_real_escape_string($_prefix).DATABASE_INFO."` SET `chat_id`=".@mysql_real_escape_string($rowmci["mcid"]),$connection);
			
			if(!empty($rowmti["mtid"]) && is_numeric($rowmti["mtid"]) && $rowmti["mtid"] > $row["ticket_id"])
				@mysql_query("UPDATE `".@mysql_real_escape_string($_prefix).DATABASE_INFO."` SET `ticket_id`=".@mysql_real_escape_string($rowmti["mtid"]),$connection);

			return null;
		}
	}
}

function initUpdateDatabase($_version,$_connection,$_prefix)
{
	require_once("./_lib/functions.data.db.update.inc.php");
	$upres = updateDatabase($_version,$_connection,$_prefix);
	return $upres;
}

?>

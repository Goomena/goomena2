<?php
/****************************************************************************************
* LiveZilla functions.external.inc.php
* 
* Copyright 2012 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
* 
* Improper changes to this file may cause critical errors.
***************************************************************************************/

if(!defined("IN_LIVEZILLA"))
	die();

function listen($_user,$init=false)
{
	global $CONFIG,$GROUPS,$INTERNAL,$USER,$INTLIST,$INTBUSY;
	$USER = $_user;
	if(!IS_FILTERED)
	{
		if(empty($USER->Browsers[0]->ChatId))
		{
			$result = queryDB(true,"SELECT visit_id FROM `".DB_PREFIX.DATABASE_VISITOR_BROWSERS."` WHERE `visitor_id`='".@mysql_real_escape_string($USER->Browsers[0]->UserId)."' AND `id`='".@mysql_real_escape_string($USER->Browsers[0]->BrowserId)."' LIMIT 1;");
			if($result && ($row = mysql_fetch_array($result, MYSQL_BOTH)) && $row["visit_id"] != $USER->Browsers[0]->VisitId && !empty($USER->Browsers[0]->VisitId))
			{
				$USER->Browsers[0]->CloseChat(2);
				$USER->AddFunctionCall("lz_chat_set_status(lz_chat_data.STATUS_STOPPED);",false);
				$USER->AddFunctionCall("lz_chat_add_system_text(99,'".base64_encode("Your browser session has expired (" . $row["visit_id"] . "-" . $USER->Browsers[0]->VisitId . "). Please close this browser instance and try again.")."');",false);
				$USER->AddFunctionCall("lz_chat_stop_system();",false);
				return $USER;
			}
			$USER->Browsers[0]->SetChatId();
			$init = true;
		}
		
		if($USER->Browsers[0]->Status == CHAT_STATUS_OPEN)
		{
			initData(true,false,false,false);
			if(!empty($_POST[POST_EXTERN_USER_GROUP]) && (empty($USER->Browsers[0]->DesiredChatGroup) || $init))
				$USER->Browsers[0]->DesiredChatGroup = base64UrlDecode($_POST[POST_EXTERN_USER_GROUP]);
				
			$USER->Browsers[0]->SetCookieGroup();
			getInternal();
			if((count($INTLIST) + $INTBUSY) > 0)
			{
				$USER->AddFunctionCall("lz_chat_set_id('".$USER->Browsers[0]->ChatId."');",false);
				$chatPosition = getQueuePosition($USER->UserId,$USER->Browsers[0]->DesiredChatGroup);
				$chatWaitingTime = getQueueWaitingTime($chatPosition,$INTBUSY);
				login();
				$USER->Browsers[0]->SetWaiting(!($chatPosition == 1 && count($INTLIST) > 0 && !(!empty($USER->Browsers[0]->DesiredChatPartner) && $INTERNAL[$USER->Browsers[0]->DesiredChatPartner]->Status == USER_STATUS_BUSY)));
				if(!$USER->Browsers[0]->Waiting)
				{
					$USER->AddFunctionCall("lz_chat_show_connected();",false);
					$USER->AddFunctionCall("lz_chat_set_status(lz_chat_data.STATUS_ALLOCATED);",false);
					if($CONFIG["gl_alloc_mode"] != ALLOCATION_MODE_ALL || !empty($USER->Browsers[0]->DesiredChatPartner))
					{
						$USER->Browsers[0]->CreateChat($INTERNAL[$USER->Browsers[0]->DesiredChatPartner],$USER,true);
					}
					else
					{
						foreach($INTLIST as $intid => $am)
							$USER->Browsers[0]->CreateChat($INTERNAL[$intid],$USER,false);
					}
				}
				else
				{
					if(!empty($CONFIG["gl_mqwt"]) && (time()-$USER->Browsers[0]->FirstActive) > ($CONFIG["gl_mqwt"]*60))
					{
						displayDeclined();
						return $USER;
					}
					
					$pdm = getPredefinedMessage($GROUPS[$USER->Browsers[0]->DesiredChatGroup]->PredefinedMessages,$USER);
					if($pdm != null && !empty($pdm->QueueMessage) && (time()-$USER->Browsers[0]->FirstActive) > $pdm->QueueMessageTime && !$USER->Browsers[0]->QueueMessageShown)
					{
						$USER->Browsers[0]->QueueMessageShown = true;
						$USER->AddFunctionCall("lz_chat_add_system_text(99,'".base64_encode($pdm->QueueMessage)."');",false);
					}
					$USER->AddFunctionCall("lz_chat_show_queue_position(".$chatPosition.",".min($chatWaitingTime,30).");",false);
				}
			}
		}
		else
		{
			if(!$USER->Browsers[0]->ArchiveCreated)
				$USER->Browsers[0]->CreateChat($INTERNAL[$USER->Browsers[0]->DesiredChatPartner],$USER,true);
			activeListen();
		}
	}
	else
		displayFiltered();
	return $USER;
}

function getPredefinedMessage($_list, $_user)
{
	$sel_message = null;
	foreach($_list as $message)
	{
		if(($message->IsDefault && (!$message->BrowserIdentification || empty($_user->Language))) || ($message->BrowserIdentification && $_user->Language == $message->LangISO))
		{
			$sel_message = $message;
			$break = true;
			break;
		}
		else if($message->IsDefault)
		{
			$sel_message = $message;
		}
	}
	return $sel_message;
}

function activeListen($runs=1,$isPost=false)
{
	global $CONFIG,$GROUPS,$INTERNAL,$USER;
	$start = time();
	$USER->Browsers[0]->Typing = isset($_POST[POST_EXTERN_TYPING]);
	
	if(!(!empty($USER->Browsers[0]->InternalUser) && $USER->Browsers[0]->InternalUser->LastActive > (time()-$CONFIG["timeout_clients"]) && $USER->Browsers[0]->InternalUser->Status != USER_STATUS_OFFLINE))
		$USER->Browsers[0]->CloseChat(4);
	else
	{
		foreach($USER->Browsers[0]->Members as $sid => $member)
			if($INTERNAL[$sid]->LastActive < (time()-$CONFIG["timeout_clients"]))
				$USER->Browsers[0]->LeaveChat($sid);
				
		if($USER->Browsers[0]->InternalUser->SystemId != $USER->Browsers[0]->DesiredChatPartner)
			$USER->Browsers[0]->DesiredChatPartner = $USER->Browsers[0]->InternalUser->SystemId;
	}
	
	while($runs == 1)
	{
		processForward();
		if(!empty($USER->Browsers[0]->Declined))
		{
			if($USER->Browsers[0]->Declined < (time()-($CONFIG["poll_frequency_clients"]*2)))
				displayDeclined();
			return $USER;
		}
		else if($USER->Browsers[0]->Closed || empty($USER->Browsers[0]->InternalUser))
		{
			displayQuit();
			return $USER;
		}
		else if($USER->Browsers[0]->Activated == CHAT_STATUS_WAITING && !(!empty($USER->Browsers[0]->Forward) && !$USER->Browsers[0]->Forward->Processed))
		{
			beginnConversation();
		}
		if($USER->Browsers[0]->Activated >= CHAT_STATUS_WAITING && !(!empty($USER->Browsers[0]->Forward) && !$USER->Browsers[0]->Forward->Processed))
		{
			refreshPicture();
			processTyping();
		}
		if($runs == 1 && isset($_POST[POST_EXTERN_USER_FILE_UPLOAD_NAME]) && !isset($_POST[POST_EXTERN_USER_FILE_UPLOAD_ERROR]) && !(!empty($USER->Browsers[0]->Forward) && !$USER->Browsers[0]->Forward->Processed))
			$USER = $USER->Browsers[0]->RequestFileUpload($USER,base64UrlDecode($_POST[POST_EXTERN_USER_FILE_UPLOAD_NAME]));
		else if($runs == 1 && isset($_POST[POST_EXTERN_USER_FILE_UPLOAD_NAME]) && isset($_POST[POST_EXTERN_USER_FILE_UPLOAD_ERROR]))
			$USER = $USER->Browsers[0]->AbortFileUpload($USER,namebase(base64UrlDecode($_POST[POST_EXTERN_USER_FILE_UPLOAD_NAME])),base64UrlDecode($_POST[POST_EXTERN_USER_FILE_UPLOAD_ERROR]));

		if($runs++ == 1 && isset($_POST[POST_GLOBAL_SHOUT]))
		{
			processPosts();
		}
		if($USER->Browsers[0]->Activated == CHAT_STATUS_ACTIVE)
		{
			$isPost = getNewPosts();
			$USER->Browsers[0]->SetStatus(CHAT_STATUS_ACTIVE);
		}
			 
		if(isset($_POST[POST_GLOBAL_SHOUT]) || isset($_POST[POST_GLOBAL_NO_LONG_POLL]) || $isPost || (!empty($USER->Browsers[0]->Forward) && !$USER->Browsers[0]->Forward->Processed))
		{
			break;
		}
		else if(md5($USER->Response) != base64UrlDecode($_POST[POST_GLOBAL_XMLCLIP_HASH_ALL]))
		{
			$_POST[POST_GLOBAL_XMLCLIP_HASH_ALL] = md5($USER->Response);
			$USER->AddFunctionCall("lz_chat_listen_hash('". md5($USER->Response) . "','".getId(5)."');",false);
			break;
		}
		else
		{
			$USER->Response = "";
			break;
		}
	}
}

function processForward()
{
	global $USER,$CONFIG;
	$USER->Browsers[0]->LoadForward();
	if(!empty($USER->Browsers[0]->Forward) && !$USER->Browsers[0]->Forward->Invite && !empty($USER->Browsers[0]->Forward->TargetGroupId) && !$USER->Browsers[0]->Forward->Processed)
	{
		$USER->AddFunctionCall("lz_chat_initiate_forwarding('".base64_encode($USER->Browsers[0]->Forward->TargetGroupId)."');",false);
		$USER->Browsers[0]->LeaveChat($USER->Browsers[0]->Forward->InitiatorSystemId);
		$USER->Browsers[0]->Forward->Save(true);
		$USER->Browsers[0]->ExternalClose();
		$USER->Browsers[0]->DesiredChatGroup = $USER->Browsers[0]->Forward->TargetGroupId;
		$USER->Browsers[0]->DesiredChatPartner = $USER->Browsers[0]->Forward->TargetSessId;
		$USER->Browsers[0]->FirstActive=time();
		$USER->Browsers[0]->Save(true);
		$USER->Browsers[0]->SetCookieGroup();
	}
}

function getNewPosts()
{
	global $USER,$INTERNAL,$GROUPS;
	$isPost = false;
	foreach($USER->Browsers[0]->GetPosts() as $post)
	{
		$USER->AddFunctionCall($post->GetCommand($post->SenderName),false);
		$isPost = true;
	}
	return $isPost;
}

function processPosts($counter=0)
{
	global $USER,$STATS,$GROUPS,$INTERNAL;
	while(isset($_POST["p_p" . $counter]))
	{
		if(STATS_ACTIVE)
			$STATS->ProcessAction(ST_ACTION_EXTERNAL_POST);

		$id = md5($USER->Browsers[0]->SystemId . base64UrlDecode($_POST[POST_EXTERN_CHAT_ID]) . base64UrlDecode($_POST["p_i" . $counter]));
		$post = new Post($id,$USER->Browsers[0]->SystemId,"",base64UrlDecode($_POST["p_p" . $counter]),time(),$USER->Browsers[0]->ChatId,$USER->Browsers[0]->Fullname);
		
		foreach($GROUPS as $groupid => $group)
			if($group->IsDynamic && !empty($group->Members[$USER->Browsers[0]->SystemId]))
			{
				foreach($group->Members as $member)
					if($member != $USER->Browsers[0]->SystemId)
					{
						if(!empty($INTERNAL[$member]))
							processPost($id,$post,$member,$counter,$groupid,$USER->Browsers[0]->ChatId);
						else
							processPost($id,$post,$member,$counter,$groupid,getValueBySystemId($member,"chat_id",""));
					}
				$pGroup=$group;
			}
		foreach($USER->Browsers[0]->Members as $systemid => $member)
		{
			if(!empty($member->Declined))
				continue;
				
			if(!empty($INTERNAL[$systemid]) && !empty($pGroup->Members[$systemid]))
				continue;
				
			if(!(!empty($pGroup) && !empty($INTERNAL[$systemid])))
				processPost($id,$post,$systemid,$counter,$USER->Browsers[0]->SystemId,$USER->Browsers[0]->ChatId);
		}
		$USER->AddFunctionCall("lz_chat_release_post('".base64UrlDecode($_POST["p_i" . $counter])."');",false);
		$counter++;
	}
	
	$counter=0;
	while(isset($_POST["pr_i" . $counter]))
	{
		markPostReceived(base64UrlDecode($_POST["pr_i" . $counter]),$USER->Browsers[0]->SystemId);
		$USER->AddFunctionCall("lz_chat_message_set_received('".base64UrlDecode($_POST["pr_i" . $counter])."');",false);
		$counter++;
	}
}

function processPost($id,$post,$systemid,$counter,$rgroup,$chatid)
{
	$post->Id = $id;
	if(isset($_POST["p_pt" . $counter]))
	{
		$post->Translation = base64UrlDecode($_POST["p_pt" . $counter]);
		$post->TranslationISO = base64UrlDecode($_POST["p_ptiso" . $counter]);
	}
	$post->ChatId = $chatid;
	$post->ReceiverOriginal =
	$post->Receiver = $systemid;
	$post->ReceiverGroup = $rgroup;
	$post->Save();
}

function login()
{
	global $INTERNAL,$USER,$CONFIG,$INPUTS;
	initData(false,false,false,false,false,false,false,true);
	if(empty($_POST[POST_EXTERN_USER_NAME]) && !isnull(getCookieValue("form_111")) && $INPUTS[111]->Cookie)
		$USER->Browsers[0]->Fullname = cutString(getCookieValue("form_111"),255);
	else
		$USER->Browsers[0]->Fullname = cutString(base64UrlDecode($_POST[POST_EXTERN_USER_NAME]),255);
		
	if(empty($_POST[POST_EXTERN_USER_EMAIL]) && !isnull(getCookieValue("form_112")) && $INPUTS[112]->Cookie)
		$USER->Browsers[0]->Email = cutString(getCookieValue("form_112"),255);
	else
		$USER->Browsers[0]->Email = cutString(base64UrlDecode($_POST[POST_EXTERN_USER_EMAIL]),255);
		
	if(empty($_POST[POST_EXTERN_USER_COMPANY]) && !isnull(getCookieValue("form_113")) && $INPUTS[113]->Cookie)
		$USER->Browsers[0]->Company = cutString(getCookieValue("form_113"),255);
	else
		$USER->Browsers[0]->Company = cutString(base64UrlDecode($_POST[POST_EXTERN_USER_COMPANY]),255);
		
	if(empty($_POST[POST_EXTERN_USER_QUESTION]) && !isnull(getCookieValue("form_114")) && $INPUTS[114]->Cookie)
		$USER->Browsers[0]->Question = cutString(getCookieValue("form_114"),16777216);
	else
		$USER->Browsers[0]->Question = cutString(base64UrlDecode($_POST[POST_EXTERN_USER_QUESTION]),16777216);

	foreach($INPUTS as $index => $input)
	{
		if($input->Active && $input->Custom)
		{
			if(isset($_POST["p_cf".$index]))
			{
				$USER->Browsers[0]->Customs[$index] = base64UrlDecode($_POST["p_cf".$index]);
				if($input->Cookie)
					setCookieValue("cf_".$index,base64UrlDecode($_POST["p_cf".$index]));
			}
		}
		else if($index == 111 && $input->Cookie && isset($_POST[POST_EXTERN_USER_NAME]) && !empty($_POST[POST_EXTERN_USER_NAME]))
			setCookieValue("form_" . $index,$USER->Browsers[0]->Fullname);
		else if($index == 112 && $input->Cookie && isset($_POST[POST_EXTERN_USER_EMAIL]) && !empty($_POST[POST_EXTERN_USER_EMAIL]))
			setCookieValue("form_" . $index,$USER->Browsers[0]->Email);
		else if($index == 113 && $input->Cookie && isset($_POST[POST_EXTERN_USER_COMPANY]) && !empty($_POST[POST_EXTERN_USER_COMPANY]))
			setCookieValue("form_" . $index,$USER->Browsers[0]->Company);
		else if($index == 114 && $input->Cookie && isset($_POST[POST_EXTERN_USER_COMPANY]) && !empty($_POST[POST_EXTERN_USER_QUESTION]))
			setCookieValue("form_" . $index,$USER->Browsers[0]->Question);
	}
	$USER->Browsers[0]->SaveLoginData();
	$USER->AddFunctionCall("lz_chat_set_status(lz_chat_data.STATUS_INIT);",false);
}

function replaceLoginDetails($_user,$values="",$keys="",$comma="")
{
	global $CONFIG,$INPUTS;
	initData(false,false,false,false,false,false,false,true);
	foreach($INPUTS as $index => $input)
	{
		$data = $input->GetValue($_user->Browsers[0]);
		$data = (!empty($_GET[$input->GetIndexName()])) ? base64UrlDecode($_GET[$input->GetIndexName()]) : ((!empty($data)) ? $data : (($input->Cookie && !isnull($input->GetCookieValue())) ? $input->GetCookieValue() : ""));
		$values .= $comma . $input->GetJavascript($data);
		$keys .= $comma . "'".$index."'";
		$comma = ",";
	}
	$_user->AddFunctionCall("if(lz_chat_data.InputFieldIndices==null)lz_chat_data.InputFieldIndices = new Array(".$keys.");",false);
	$_user->AddFunctionCall("if(lz_chat_data.InputFieldValues==null)lz_chat_data.InputFieldValues = new Array(".$values.");",false);
	return $_user;
}

function getChatLoginInputs($_html,$_maxlength,$inputshtml="")
{
	global $INPUTS;
	initData(false,false,false,false,false,false,false,true);
	foreach($INPUTS as $index => $dinput)
		$inputshtml .= $dinput->GetHTML($_maxlength);
	return str_replace("<!--chat_login_inputs-->",$inputshtml,$_html);
}

function refreshPicture()
{
	global $CONFIG,$USER;
	$USER->Browsers[0]->InternalUser->LoadPictures();
	if(!empty($USER->Browsers[0]->InternalUser->WebcamPicture))
		$edited = $USER->Browsers[0]->InternalUser->WebcamPictureTime;
	else if(!empty($USER->Browsers[0]->InternalUser->ProfilePicture))
		$edited = $USER->Browsers[0]->InternalUser->ProfilePictureTime;
	else
		$edited = 0;
	$USER->AddFunctionCall("lz_chat_set_intern_image(".$edited.",'" . $USER->Browsers[0]->InternalUser->GetOperatorPictureFile() . "',false);",false);
	$USER->AddFunctionCall("lz_chat_set_config(".$CONFIG["timeout_clients"].",".$CONFIG["poll_frequency_clients"].");",false);
}

function processTyping($_clients="",$_dgroup="")
{
	global $CONFIG,$USER,$GROUPS,$INTERNAL,$LZLANG;
	$USER->Browsers[0]->InternalUser->LoadProfile();
	$groupname = addslashes($GROUPS[$USER->Browsers[0]->DesiredChatGroup]->Description);
	foreach($GROUPS as $groupid => $group)
		if($group->IsDynamic && !empty($group->Members[$USER->Browsers[0]->SystemId]))
		{
			$_dgroup = $group->Descriptions["EN"];
			foreach($group->Members as $member)
				if($member != $USER->Browsers[0]->SystemId)
				{
					$name = "";
					if(!empty($INTERNAL[$member]))
					{
						if($INTERNAL[$member]->Status==USER_STATUS_OFFLINE)
							continue;
						$name = $INTERNAL[$member]->Fullname;
					}
					else
						$name = getValueBySystemId($member,"fullname",$LZLANG["client_guest"]);
					$USER->AddFunctionCall("lz_chat_set_room_member('".base64_encode($member)."','".base64_encode($name)."');",false);
				}
		}
	foreach($USER->Browsers[0]->Members as $sysid => $chatm)
		if($chatm->Status < 2 && empty($chatm->Declined))
		{
			$USER->AddFunctionCall("lz_chat_set_room_member('".base64_encode($sysid)."','".base64_encode($INTERNAL[$sysid]->Fullname)."');",false);
		}
	$USER->AddFunctionCall("lz_chat_set_host(\"".base64_encode($USER->Browsers[0]->InternalUser->UserId)."\",\"".base64_encode(addslashes($USER->Browsers[0]->InternalUser->Fullname))."\",\"". base64_encode($groupname)."\",\"".strtolower($USER->Browsers[0]->InternalUser->Language)."\",".parseBool($USER->Browsers[0]->InternalUser->Typing==$USER->Browsers[0]->SystemId).",".parseBool(!empty($USER->Browsers[0]->InternalUser->Profile) && $USER->Browsers[0]->InternalUser->Profile->Public).",\"". base64_encode($_dgroup)."\");",false);
}

function beginnConversation()
{
	global $USER,$CONFIG;
	$USER->Browsers[0]->ExternalActivate();
	if(!empty($CONFIG["gl_save_op"]))
		setCookieValue("internal_user",$USER->Browsers[0]->InternalUser->UserId);
	$USER->Browsers[0]->DesiredChatPartner = $USER->Browsers[0]->InternalUser->SystemId;
	$USER->AddFunctionCall("lz_chat_set_status(lz_chat_data.STATUS_ACTIVE);",false);
	$USER->AddFunctionCall("lz_chat_shout(1);",false);
}

function displayFiltered()
{
	global $FILTERS,$USER;
	$USER->Browsers[0]->CloseChat(0);
	$USER->AddFunctionCall("lz_chat_set_host('','','','',false,false,'');",false);
	$USER->AddFunctionCall("lz_chat_set_status(lz_chat_data.STATUS_STOPPED);",false);
	$USER->AddFunctionCall("lz_chat_add_system_text(2,'".base64_encode("&nbsp;<b>".$FILTERS->Filters[ACTIVE_FILTER_ID]->Reason."</b>")."');",false);
	$USER->AddFunctionCall("lz_chat_stop_system();",false);
}

function displayQuit()
{
	global $GROUPS,$USER;
	$USER->Browsers[0]->CloseChat(1);
	$USER->AddFunctionCall("lz_chat_set_host('','','','',false,false,'');",false);
	$USER->AddFunctionCall("lz_chat_set_status(lz_chat_data.STATUS_STOPPED);",false);
	$USER->AddFunctionCall("lz_chat_add_system_text(3,null);",false);
	$USER->AddFunctionCall("lz_chat_stop_system();",false);
}

function displayDeclined()
{
	global $GROUPS,$USER;
	$USER->Browsers[0]->CloseChat(2);
	$USER->AddFunctionCall("lz_chat_set_host('','','','',false,false,'');",false);
	$USER->AddFunctionCall("lz_chat_set_status(lz_chat_data.STATUS_STOPPED);",false);
	$USER->AddFunctionCall("lz_chat_add_system_text(4,null);",false);
	$USER->AddFunctionCall("lz_chat_stop_system();",false);
}

function buildLoginErrorField($error="",$addition = "")
{
	global $FILTERS,$LZLANG,$CONFIG;
	if(!getAvailability())
		return $LZLANG["client_error_deactivated"];
		
	if(!DB_CONNECTION || !empty($CONFIG["gl_stmo"]))
		return $LZLANG["client_error_unavailable"];

	if(IS_FILTERED)
	{
		$error = $LZLANG["client_error_unavailable"];
		if(isset($FILTERS->Message) && strlen($FILTERS->Message) > 0)
			$addition = "<br><br>" . $FILTERS->Message;
	}
	return $error . $addition;
}

function reloadGroups($_user)
{
	global $CONFIG,$INTERNAL,$GROUPS;
	initData(true,false,false,true);
	if(!empty($_GET[GET_EXTERN_GROUP]))
		$_user->Browsers[0]->DesiredChatGroup = base64UrlDecode(getParam(GET_EXTERN_GROUP));
		
	if(!empty($_GET[GET_EXTERN_INTERN_USER_ID]))
		$_user->Browsers[0]->DesiredChatPartner = base64UrlDecode(getParam(GET_EXTERN_INTERN_USER_ID));
	
	$groupbuilder = new GroupBuilder($INTERNAL,$GROUPS,$CONFIG,$_user->Browsers[0]->DesiredChatGroup,$_user->Browsers[0]->DesiredChatPartner);
	$groupbuilder->Generate();
	
	if(isset($_POST[POST_EXTERN_REQUESTED_INTERNID]) && !empty($_POST[POST_EXTERN_REQUESTED_INTERNID]))
		$_user->Browsers[0]->DesiredChatPartner = getInternalSystemIdByUserId(base64UrlDecode($_POST[POST_EXTERN_REQUESTED_INTERNID]));

	$_user->AddFunctionCall("lz_chat_set_groups(\"" . $groupbuilder->Result . "\" ,". $groupbuilder->ErrorHTML .");",false);
	$_user->AddFunctionCall("lz_chat_release(".parseBool(($groupbuilder->GroupAvailable || (isset($_POST[GET_EXTERN_RESET]) && strlen($groupbuilder->ErrorHTML) <= 2))).",".$groupbuilder->ErrorHTML.");",false);
	return $_user;
}

function getInternal($desired = "",$util = 0,$fromCookie = null)
{
	global $CONFIG,$INTERNAL,$GROUPS,$USER,$INTLIST,$INTBUSY;
	$INTLIST = array();
	$INTBUSY = 0;
	$backup_target = null;
	$fromDepartment = $fromDepartmentBusy = false;
	
	if(!empty($USER->Browsers[0]->DesiredChatPartner) && isset($INTERNAL[$USER->Browsers[0]->DesiredChatPartner]) && $INTERNAL[$USER->Browsers[0]->DesiredChatPartner]->Status < USER_STATUS_OFFLINE)
	{
		if(!(!empty($USER->Browsers[0]->DesiredChatGroup) && !in_array($USER->Browsers[0]->DesiredChatGroup,$INTERNAL[$USER->Browsers[0]->DesiredChatPartner]->GetGroupList(true))))
			$desired = $USER->Browsers[0]->DesiredChatPartner;
	}
	else
	{
		$USER->Browsers[0]->DesiredChatPartner = null;
		if(isset($_POST[POST_EXTERN_REQUESTED_INTERNID]) && !empty($_POST[POST_EXTERN_REQUESTED_INTERNID]))
			$desired = getInternalSystemIdByUserId(base64UrlDecode($_POST[POST_EXTERN_REQUESTED_INTERNID]));
		else if(!isnull(getCookieValue("internal_user")) && !empty($CONFIG["gl_save_op"]))
		{
			$desired = getInternalSystemIdByUserId(getCookieValue("internal_user"));
			if(!empty($INTERNAL[$desired]) && !(!empty($USER->Browsers[0]->DesiredChatGroup) && !in_array($USER->Browsers[0]->DesiredChatGroup,$INTERNAL[$desired]->GetGroupList(true))))
			{
				$fromCookie = $desired;
			}
			else
				$desired = "";
		}
	}
	foreach($GROUPS as $id => $group)
		$utilization[$id] = 0;
	foreach($INTERNAL as $sessId => $internal)
	{
		if($internal->LastActive > (time()-$CONFIG["timeout_clients"]))
		{
			$intstatus = $internal->Status;
			$group_chats[$sessId] = $internal->GetExternalChatAmount();
			$group_names[$sessId] = $internal->Fullname;
			$group_available[$sessId] = GROUP_STATUS_UNAVAILABLE;

			if(in_array($USER->Browsers[0]->DesiredChatGroup,$internal->GetGroupList(true)))
			{
				if($intstatus < USER_STATUS_OFFLINE && $GROUPS[$USER->Browsers[0]->DesiredChatGroup]->MaxChats > -1 && $GROUPS[$USER->Browsers[0]->DesiredChatGroup]->MaxChats <= $group_chats[$sessId])
					$intstatus = GROUP_STATUS_BUSY;
				if($intstatus == USER_STATUS_ONLINE && $internal->LastChatAllocation < (time()-($CONFIG["poll_frequency_clients"]*3)))
					$group_available[$sessId] = GROUP_STATUS_AVAILABLE;
				elseif($intstatus == USER_STATUS_BUSY || $internal->LastChatAllocation >= (time()-($CONFIG["poll_frequency_clients"]*3)))
				{
					$group_available[$sessId] = GROUP_STATUS_BUSY;
					$INTBUSY++;
					
					if(empty($fromCookie) && $desired == $sessId)
						return;
				}
			}
			else
			{
				if($intstatus == USER_STATUS_ONLINE)
					$backup_target = $internal;
				else if($intstatus == USER_STATUS_BUSY && empty($backup_target))
					$backup_target = $internal;
					
				if(!empty($USER->Browsers[0]->DesiredChatPartner) && $USER->Browsers[0]->DesiredChatPartner == $sessId)
					$USER->Browsers[0]->DesiredChatPartner = null;
			}
			$igroups = $internal->GetGroupList(true);
			for($count=0;$count<count($igroups);$count++)
			{
				if($USER->Browsers[0]->DesiredChatGroup == $igroups[$count])
				{
					if(!is_array($utilization[$igroups[$count]]))
						$utilization[$igroups[$count]] = Array();
					if($group_available[$sessId] == GROUP_STATUS_AVAILABLE)
						$utilization[$igroups[$count]][$sessId] = $group_chats[$sessId];
				}
			}
		}
	}
	
	if(isset($utilization[$USER->Browsers[0]->DesiredChatGroup]) && is_array($utilization[$USER->Browsers[0]->DesiredChatGroup]))
	{
		arsort($utilization[$USER->Browsers[0]->DesiredChatGroup]);
		reset($utilization[$USER->Browsers[0]->DesiredChatGroup]);
		$util = end($utilization[$USER->Browsers[0]->DesiredChatGroup]);
		$INTLIST = $utilization[$USER->Browsers[0]->DesiredChatGroup];
	}
	
	if(isset($group_available) && is_array($group_available) && in_array(GROUP_STATUS_AVAILABLE,$group_available))
		$fromDepartment = true;
	elseif(isset($group_available) && is_array($group_available) && in_array(GROUP_STATUS_BUSY,$group_available))
		$fromDepartmentBusy = true;

	if(isset($group_chats) && is_array($group_chats) && isset($fromDepartment) && $fromDepartment)
		foreach($group_chats as $sessId => $amount)
		{
			if(($group_available[$sessId] == GROUP_STATUS_AVAILABLE  && $amount <= $util) || ((!empty($USER->Browsers[0]->Forward) && $USER->Browsers[0]->Forward->Processed) && isset($desired) && $sessId == $desired))
			{
				$available_internals[] = $sessId;
			}
		}

	if($fromDepartment && sizeof($available_internals) > 0)
	{
		if(is_array($available_internals))
		{
			if(!empty($desired) && (in_array($desired,$available_internals) || $INTERNAL[$desired]->Status == USER_STATUS_ONLINE))
				$matching_internal = $desired;
			else
			{
				if(!isnull($inv_sender = $USER->Browsers[0]->GetLastInvitationSender()) && in_array($inv_sender,$available_internals))
				{
					$matching_internal = $inv_sender;
				}
				else
				{
					$matching_internal = array_rand($available_internals,1);
					$matching_internal = $available_internals[$matching_internal];
				}
			}
		}
		if($CONFIG["gl_alloc_mode"] != ALLOCATION_MODE_ALL || $fromCookie == $matching_internal)
			$USER->Browsers[0]->DesiredChatPartner = $matching_internal;
	}
	elseif($fromDepartmentBusy)
	{	
		if(!$USER->Browsers[0]->Waiting)
			$USER->Browsers[0]->Waiting = true;
	}
	else
	{
		$USER->AddFunctionCall("lz_chat_add_system_text(8,null);",false);
		$USER->AddFunctionCall("lz_chat_stop_system();",false);
		$USER->Browsers[0]->CloseChat(3);
		$INTLIST = null;
	}
}

function getSessionId()
{
	global $CONFIG;
	if(!isnull(getCookieValue("userid")))
		$session = substr(getCookieValue("userid"),0,USER_ID_LENGTH);
	else
		setCookieValue("userid",$session = getId(USER_ID_LENGTH));
	return $session;
}

function getQueueWaitingTime($_position,$_intamount,$min=1)
{
	global $CONFIG;
	if($_intamount == 0)
		$_intamount++;
		
	$result = queryDB(true,"SELECT AVG(`endtime`-`time`) AS `waitingtime` FROM `".DB_PREFIX.DATABASE_CHAT_ARCHIVE."` WHERE `endtime`>0 AND `endtime`>`time` AND `endtime`-`time` < 3600;");
	if($result)
	{
		$row = mysql_fetch_array($result, MYSQL_BOTH);
		if(!empty($row["waitingtime"]))
			$min = ($row["waitingtime"]/60)/$_intamount;
		else
			$min = $min/$_intamount;
		$minb = $min;
		for($i = 1;$i < $_position; $i++)
		{
			$minb *= 0.9;
			$min += $minb;
		}
		$min /= $CONFIG["gl_sim_ch"];
		$min -= (time() - CHAT_START_TIME) / 60;
		if($min <= 0)
			$min = 1;
	}
	return ceil($min);
}

function getQueuePosition($_creatorId,$_targetGroup,$_startTime=0,$_position = 1)
{
	global $CONFIG,$USER;
	$USER->Browsers[0]->SetStatus(CHAT_STATUS_OPEN);
	queryDB(true,"UPDATE `".DB_PREFIX.DATABASE_VISITOR_CHATS."` SET `qpenalty`=`qpenalty`+60 WHERE `last_active`>".(time()-$CONFIG["timeout_clients"])." AND `status`=0 AND `exit`=0 AND `last_active`<" . @mysql_real_escape_string(time()-max(20,($CONFIG["poll_frequency_clients"]*2))));
	$result = queryDB(true,"SELECT `priority`,`request_operator`,`request_group`,`chat_id`,`first_active`,`qpenalty`+`first_active` as `sfirst` FROM `".DB_PREFIX.DATABASE_VISITOR_CHATS."` WHERE `status`='0' AND `exit`='0' AND `chat_id`>0 AND `last_active`>".(time()-$CONFIG["timeout_clients"])." ORDER BY `priority` DESC,`sfirst` ASC;");
	if($result)
	{
		while($row = mysql_fetch_array($result, MYSQL_BOTH))
		{
			if($row["chat_id"] == $USER->Browsers[0]->ChatId)
			{
				$_startTime = $row["sfirst"];
				break;
			}
			else if($row["request_group"]==$_targetGroup && $row["request_operator"]==$USER->Browsers[0]->DesiredChatPartner)
			{
				$_position++;
			}
			else if($row["request_group"]==$_targetGroup && ($row["request_operator"]!=$USER->Browsers[0]->DesiredChatPartner && empty($row["request_operator"])))
			{
				$_position++;
			}
			else if(!empty($USER->Browsers[0]->DesiredChatPartner) && $USER->Browsers[0]->DesiredChatPartner==$row["request_operator"])
			{
				$_position++;
			}
		}
	}
	define("CHAT_START_TIME",$_startTime);
	return $_position;
}

function isRatingFlood()
{
	$result = queryDB(true,"SELECT count(id) as rating_count FROM `".DB_PREFIX.DATABASE_RATINGS."` WHERE time>".@mysql_real_escape_string(time()-86400)." AND ip='".@mysql_real_escape_string(getIP())."';");
	if($result)
	{
		$row = mysql_fetch_array($result, MYSQL_BOTH);
		return ($row["rating_count"] >= MAX_RATES_PER_DAY);
	}
	else
		return true;
}

function saveRating($_rating)
{
	$time = time();
	while(true)
	{
		queryDB(true,"SELECT time FROM `".DB_PREFIX.DATABASE_RATINGS."` WHERE time=".@mysql_real_escape_string($time).";");
		if(@mysql_affected_rows() > 0)
			$time++;
		else
			break;
	}
	queryDB(true,"INSERT INTO `".DB_PREFIX.DATABASE_RATINGS."` (`id` ,`time` ,`user_id` ,`internal_id` ,`fullname` ,`email` ,`company` ,`qualification` ,`politeness` ,`comment` ,`ip`) VALUES ('".@mysql_real_escape_string($_rating->Id)."', ".@mysql_real_escape_string($time)." , '".@mysql_real_escape_string($_rating->UserId)."', '".@mysql_real_escape_string($_rating->InternId)."', '".@mysql_real_escape_string($_rating->Fullname)."', '".@mysql_real_escape_string($_rating->Email)."', '".@mysql_real_escape_string($_rating->Company)."', '".@mysql_real_escape_string($_rating->RateQualification)."', '".@mysql_real_escape_string($_rating->RatePoliteness)."', '".@mysql_real_escape_string($_rating->RateComment)."', '".@mysql_real_escape_string(getIP())."');");
}

function isTicketFlood()
{
	$result = queryDB(true,"SELECT count(id) as ticket_count FROM `".DB_PREFIX.DATABASE_TICKET_MESSAGES."` WHERE time>".@mysql_real_escape_string(time()-86400)." AND ip='".@mysql_real_escape_string(getIP())."';");
	if($result)
	{
		$row = mysql_fetch_array($result, MYSQL_BOTH);
		return ($row["ticket_count"] > MAX_MAIL_PER_DAY);
	}
	else
		return true;
}

function getTicketId()
{
	$result = queryDB(true,"SELECT `ticket_id`,(SELECT MAX(`id`) FROM `".DB_PREFIX.DATABASE_TICKETS."`) as `used_ticket_id` FROM `".DB_PREFIX.DATABASE_INFO."`");
	$row = mysql_fetch_array($result, MYSQL_BOTH);
	$max = max($row["ticket_id"],$row["used_ticket_id"]);
	$tid = $max+1;
	queryDB(true,"UPDATE `".DB_PREFIX.DATABASE_INFO."` SET ticket_id='".@mysql_real_escape_string($tid)."' WHERE ticket_id='".@mysql_real_escape_string($max)."'");
	return $tid;
}

function getSubject($_chatTranscript,$_email,$_username,$_group,$_chatid,$_company,$_question,$_customs=null)
{
	global $CONFIG,$INPUTS,$LZLANG;
	if($_chatTranscript)
		$subject = $CONFIG["gl_subjct"];
	else
		$subject = $CONFIG["gl_subjom"];
	$subject = str_replace("%SERVERNAME%",$CONFIG["gl_site_name"],$subject);
	$subject = str_replace("%USERNAME%",$_username,$subject);
	$subject = str_replace("%USEREMAIL%",$_email,$subject);
	$subject = str_replace("%USERCOMPANY%",$_company,$subject);
	$subject = str_replace("%USERQUESTION%",$_question,$subject);
	$subject = str_replace("%TARGETGROUP%",$_group,$subject);
	$subject = str_replace("%CHATID%",$_chatid,$subject);
	
	if(!empty($_customs))
		foreach($INPUTS as $index => $input)
			if($input->Active && $input->Custom)
			{
				if($input->Type == "CheckBox")
					$subject = str_replace("%CUSTOM".($index+1)."%",((!empty($_customs[$index])) ? $LZLANG["client_yes"] : $LZLANG["client_no"]),$subject);
				else if(!empty($_customs[$index]))
					$subject = str_replace("%CUSTOM".($index+1)."%",$input->GetClientValue($_customs[$index]),$subject);
				else
					$subject = str_replace("%CUSTOM".($index+1)."%","",$subject);
			}
			else
				$subject = str_replace("%CUSTOM".($index+1)."%","",$subject);
	return applyReplacements(str_replace("\n","",$subject),true,false);
}
?>

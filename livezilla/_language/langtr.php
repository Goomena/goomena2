<?php

/*********************************************************************************
* LiveZilla langtr.php
* 
* Copyright 2012 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
*
* Please report errors here: http://www.livezilla.net/forum/
* DO NOT REMOVE/ALTER <!--placeholders-->
* 
********************************************************************************/

$LZLANG["client_join_group"] = "Bu gruba katıldı <!--group_name-->.";
$LZLANG["client_leave_group"] = "Bu gruptan ayrıldı <!--group_name-->.";
$LZLANG["client_accepted"] = "Kabul edildi";
$LZLANG["client_rejected"] = "Reddedildi";
$LZLANG["client_forwarding_to"] = "Yönlendiriliyor:";
$LZLANG["client_date"] = "Tarih";
$LZLANG["client_chat_reference_number"] = "Konuşma referans no";
$LZLANG["client_invalid_data"] = "Verilen bilgiler geçersiz.";
$LZLANG["client_leave_message"] = "bir mesaj bırakın";
$LZLANG["client_no"] = "Hayır";
$LZLANG["client_yes"] = "Evet";
$LZLANG["client_ticket_header"] = "Lütfen mesaj bırakınız";
$LZLANG["client_ticket_information"] = "Lütfen bir mesaj bırakın biz de size en yakın zamanda dönelim. Lütfen kişisel bilgilerinizle beraber mümkün olduğunca detay verin.";
$LZLANG["client_request_chat_transcript"] = "Evet, bu konuşmanın bir kopyasını email adresime gönderin.";
$LZLANG["client_use_auto_translation_service"] = "Otomatik-tercüme kullan. Dilim:";
$LZLANG["client_fill_mandatory_fields"] = "Lütfen tüm gerekli alanları doldurun.";
$LZLANG["client_your_question"] = "Sorunuz:";
$LZLANG["client_queue_message"] = "Kuyruktaki şu anki durumunuz <!--queue_position--> Yaklaşık bekleme süresi: <!--queue_waiting_time--> dakikadır.";
$LZLANG["client_queue_next_operator"] = "Sıradaki görevli size yardımcı olacaktır.";
$LZLANG["client_chat_invitation"] = "Konuşma daveti";
$LZLANG["client_chat_transcript"] = "Konuşma raporu";
$LZLANG["client_new_message"] = "Yeni mesaj";
$LZLANG["client_system"] = "Sistem";
$LZLANG["client_required_field"] = "Gerekli alanlar";
$LZLANG["client_int_is_connected"] = "Bir görevli bağlanacaktır, lütfen bekleyiniz.";
$LZLANG["client_ints_are_busy"] = "Şu anda tüm görevliler meşgul, lütfen bekleyiniz.";
$LZLANG["client_int_left"] = "Görevli konuşmadan ayrıldı. Bir sorunuz varsa lütfen mesaj bırakınız.";
$LZLANG["client_intern_left"] = "<!--intern_name--> konuşma odasından çıktı.";
$LZLANG["client_int_declined"] = "Şu anda tüm görevliler meşgul. İsteğiniz şu anda karşılanamamaktadır. Bu sorundan dolayı üzgünüz. Lütfen daha sonra tekrar deneyin veya bir mesaj bırakın:";
$LZLANG["client_no_intern_users"] = "Bu bölümdeki görevliler şu anda müsait değil. Lütfen daha sonra tekrar deneyin veya başka bir bölümü deneyin. İsterseniz bir mesaj da bırakabilirsiniz:";
$LZLANG["client_no_intern_users_short"] = "Bu bölümdeki görevliler şu anda müsait değil. Lütfen daha sonra deneyiniz veya bir mesaj bırakınız:";
$LZLANG["client_thank_you"] = "Teşekkürler";
$LZLANG["client_con_broken"] = "Bağlantı hatası, tekrar bağlanmayı deniyor.. Lütfen bekleyiniz..";
$LZLANG["client_guide_request"] = "Bir görevli sizi şu sayfaya yönlendirmek istiyor:";
$LZLANG["client_still_waiting_int"] = "Hala bağlantıyı sağlamaya çalışıyoruz, lütfen bekleyiniz. Beklemek istemiyorsanız bir mesaj bırakınız.";
$LZLANG["client_request"] = "Talep";
$LZLANG["client_loading"] = "Yükleniyor";
$LZLANG["client_forwarding"] = "Başka bir görevliye aktarılıyorsunuz, lütfen bekleyiniz";
$LZLANG["client_vcard"] = "Ziyaret kartı";
$LZLANG["client_guest"] = "Misafir";
$LZLANG["client_topic"] = "Konu";
$LZLANG["client_send_file"] = "Dosya gönder";
$LZLANG["client_representative_is_typing"] = "Görevli yazıyor";
$LZLANG["client_save_visitcard"] = "Ziyaret kartını indir";
$LZLANG["client_please_enter_name_and_email"] = "Lütfen adınızı ve email adresinizi yazınız.";
$LZLANG["client_start_system"] = "Sistem başlatılıyor.";
$LZLANG["client_your_email"] = "E-Mail";
$LZLANG["client_your_company"] = "Şirket";
$LZLANG["client_your_name"] = "İsminiz";
$LZLANG["client_your_message"] = "Mesajınız";
$LZLANG["client_group"] = "Departman";
$LZLANG["client_send_message"] = "Mesaj gönder";
$LZLANG["client_error_groups"] = "Bu departman, harici bağlantıları kabul etmemektedir.";
$LZLANG["client_error_deactivated"] = "Bu servis şu anda aktif değil.";
$LZLANG["client_error_unavailable"] = "Bu servis şu anda sağlanamıyor.";
$LZLANG["client_select_valid_group"] = "Lütfen geçerli bir departman seçiniz.";
$LZLANG["client_important_hint"] = "Önemli bilgi";
$LZLANG["client_print"] = "Yazıcıya yolla";
$LZLANG["client_close_window"] = "Pencereyi kapat";
$LZLANG["client_rate_representative"] = "Görevliyi derecelendir";
$LZLANG["client_bookmark"] = "Favorilere ekle";
$LZLANG["client_insert_smiley"] = "Gülücük ekle";
$LZLANG["client_goto_login"] = "Giriş ekranına git";
$LZLANG["client_switch_sounds"] = "Sesi aç/kapat";
$LZLANG["client_intern_arrives"] = "<!--intern_name--> konuşmaya girdi.";
$LZLANG["client_start_chat"] = "Konuşmayı başlat";
$LZLANG["client_start_chat_information"] = "Online desteğe hoşgeldiniz! Size daha iyi hizmet etmemize yardımcı olmak için, lütfen bir görevliyle konuşmaya başlamadan önce bize bilgi veriniz.";
$LZLANG["client_new_messages"] = "Yeni mesajlar";
$LZLANG["client_message_too_long"] = "Mesaj uzunluğu çok fazla. Bu mesajı gönderebilmek için kısaltmalısınız";
$LZLANG["client_message_received"] = "Teşekkürler! Sizinle kısa zamanda iletişime geçeceğiz.";
$LZLANG["client_message_flood"] = "Çok fazla mesaj yolladınız. Lütfen bir süre bekleyin ve tekrar deneyin.";
$LZLANG["client_really_close"] = "Bu konuşmadan çıkmak istediğinize emin misiniz?";
$LZLANG["client_file_request_rejected"] = "Görevli talebinizi kabul etmedi.";
$LZLANG["client_file_upload_oversized"] = "Dosya kabul edilmedi. (Dosya boyutu çok büyük veya uzantı yasak)!";
$LZLANG["client_file_upload_provided"] = "Dosyanız yetkiliye ulaştı!";
$LZLANG["client_file_upload_requesting"] = "Yükleme için izin isteği..";
$LZLANG["client_file_upload_send_file"] = "Seçilen dosyayı yüklemek için GÖNDER e basın.";
$LZLANG["client_file_upload_select_file"] = "Yüklenecek dosyayı seçiniz";
$LZLANG["client_wait_for_representative"] = "Lütfen bir görevli bekleyiniz.";
$LZLANG["client_no_representative"] = "Görevli konuşmadan ayrıldı.";
$LZLANG["client_transmitting_file"] = "Dosyanız aktarılıyor.";
$LZLANG["client_please_rate"] = "Lütfen önce derecelendirme seçiniz.";
$LZLANG["client_rate_reason"] = "Lütfen yorumunuzu buraya yazınız";
$LZLANG["client_rate_qualification"] = "Yetkinlik";
$LZLANG["client_rate_politeness"] = "Nezaket";
$LZLANG["client_rate_success"] = "Teşekkürler, derecelendirmenizi aldık. Cevabınız hizmetimizi iyileştirmede bize yardımcı olacaktır. Bildiriminizi takdir ediyoruz.";
$LZLANG["client_rate_max"] = "Günde 3 kereden fazla oylama yapamazsınız.";
$LZLANG["client_file"] = "Dosya";
$LZLANG["client_welcome"] = "Hoşgeldiniz";
$LZLANG["client_send"] = "Gönder";
$LZLANG["client_abort"] = "Vazgeç";
$LZLANG["index_close"] = "Kapat";
$LZLANG["index_not_available"] = "Mevcut Değil";
$LZLANG["index_description_demochat"] = "Deneme sohbeti başlatıp giriş yapmış kullanıcılarla konuşmaya başlayabilirsiniz. Bunu her çeşit Livezilla testi için kullanabilirsiniz.";
?>
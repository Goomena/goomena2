<?php

/*********************************************************************************
* LiveZilla langda.php
* 
* Copyright 2012 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
*
* Please report errors here: http://www.livezilla.net/forum/
* DO NOT REMOVE/ALTER <!--placeholders-->
* 
********************************************************************************/

$LZLANG["client_join_group"] = "Du er med i gruppen <!--group_name-->.";
$LZLANG["client_leave_group"] = "Du har forladt gruppen <!--group_name-->.";
$LZLANG["client_accepted"] = "Godkendt";
$LZLANG["client_rejected"] = "Afvist";
$LZLANG["client_forwarding_to"] = "Videresend til";
$LZLANG["client_date"] = "Dato";
$LZLANG["client_chat_reference_number"] = "Chat ref. nr.";
$LZLANG["client_invalid_data"] = "Den indtastede information er ugyldig:";
$LZLANG["client_leave_message"] = "Læg en besked";
$LZLANG["client_no"] = "Nej";
$LZLANG["client_yes"] = "Ja";
$LZLANG["client_ticket_header"] = "Læg en besked";
$LZLANG["client_ticket_information"] = "Læg en besked, og vi vender tilbage snarest.";
$LZLANG["client_request_chat_transcript"] = "Ja Tak - send en udskrift af denne chat til min emailadresse:";
$LZLANG["client_use_auto_translation_service"] = "Brug automatisk oversættelsesservice. Mit sprog er:";
$LZLANG["client_fill_mandatory_fields"] = "Udfyld alle krævede felter.";
$LZLANG["client_your_question"] = "Din forespørgsel";
$LZLANG["client_queue_message"] = "Der er kø, dit nr i køen er <!--queue_position-->. Forventet ventetid: <!--queue_waiting_time--> minuter(s).";
$LZLANG["client_queue_next_operator"] = "Du vil få svar af den næste ledige supporter.";
$LZLANG["client_chat_invitation"] = "Chat invitation";
$LZLANG["client_chat_transcript"] = "Chat udskrift";
$LZLANG["client_new_message"] = "Ny besked fra";
$LZLANG["client_system"] = "System ";
$LZLANG["client_required_field"] = "Påkrævede Felter";
$LZLANG["client_int_is_connected"] = "En supporter vil kontakte dig om et øjeblik, vent venligst.";
$LZLANG["client_ints_are_busy"] = "Alle vores support medarbejdere er desværre optaget lige nu, vent venligst.";
$LZLANG["client_int_left"] = "Supporteren har forladt samtalen. Hvis du har brug for yderligere hjælp så læg venligst en besked:";
$LZLANG["client_intern_left"] = "<!--intern_name--> har forladt chatten.";
$LZLANG["client_int_declined"] = "Alle vores supportere er optaget. Din anmodning kunne ikke godkendes. Vi undskylder ulejligheden. Vend venligst tilbage senere eller læg en besked:";
$LZLANG["client_no_intern_users"] = "Ingen supportere for denne afdeling er ledig. Forsøg venligst igen senere eller vælg en anden afdeling. Du kan lægge en besked:";
$LZLANG["client_no_intern_users_short"] = "Ingen ledige supportere i denne afdeling. Forsøg venligst senere eller prøv en anden afdeling.";
$LZLANG["client_thank_you"] = "Tak!";
$LZLANG["client_con_broken"] = "Forbindelses fejl, vent venligst....";
$LZLANG["client_guide_request"] = "En supporter vil gerne sende dig til denne side:";
$LZLANG["client_still_waiting_int"] = "Vi arbejder stadig med at oprette forbindelse, vent venligst. Hvis du ikke vil vente læg da en besked:";
$LZLANG["client_request"] = "Anmodning";
$LZLANG["client_loading"] = "Indlæser";
$LZLANG["client_forwarding"] = "Du vil blive viderestillet til en anden side, vent venligst.";
$LZLANG["client_vcard"] = "Visitkort";
$LZLANG["client_guest"] = "Gæst";
$LZLANG["client_topic"] = "Emne";
$LZLANG["client_send_file"] = "Send en fil";
$LZLANG["client_representative_is_typing"] = "Supporteren skriver...";
$LZLANG["client_save_visitcard"] = "Hent visitkort";
$LZLANG["client_please_enter_name_and_email"] = "Skriv venligst dit brugernavn og email adresse";
$LZLANG["client_start_system"] = "System starter op.";
$LZLANG["client_your_email"] = " email";
$LZLANG["client_your_company"] = "Firma";
$LZLANG["client_your_name"] = "Dit navn";
$LZLANG["client_your_message"] = "Din Besked";
$LZLANG["client_group"] = "Afdeling";
$LZLANG["client_send_message"] = "Send Besked";
$LZLANG["client_error_groups"] = "Denne service er ikke mulig pga.: Ingen afdelinger kan i øjeblikket modtage opkald. Vi beklager.";
$LZLANG["client_error_deactivated"] = "Denne service er ikke mulig endnu. Vi beklager.";
$LZLANG["client_error_unavailable"] = "Denne service er ikke mulig. Vi beklager.";
$LZLANG["client_select_valid_group"] = "Vælg venligst den afdeling du ønsker at kontakte";
$LZLANG["client_important_hint"] = "Vigtigt TIP";
$LZLANG["client_print"] = "Udskriv";
$LZLANG["client_close_window"] = "Luk vindue";
$LZLANG["client_rate_representative"] = "Vurdér supporteren";
$LZLANG["client_bookmark"] = "Favorit";
$LZLANG["client_insert_smiley"] = "Brug smiley";
$LZLANG["client_goto_login"] = "Gå til Login siden";
$LZLANG["client_switch_sounds"] = "Lyd tændt/slukket";
$LZLANG["client_intern_arrives"] = "<!--intern_name--> er på vej ind for at hjælpe dig.";
$LZLANG["client_start_chat"] = "Start Support";
$LZLANG["client_start_chat_information"] = "Velkommen til vores online support. For at kunne tilbyde dig den bedste hjælp, bedes du venligst indtaste dine informationer, inden du starter chatten med vores salgsafdeling.";
$LZLANG["client_new_messages"] = "Ny Besked";
$LZLANG["client_message_too_long"] = "Denne besked indeholder desværre for mange tegn, brug venligst færre tegn ad gangen.";
$LZLANG["client_message_received"] = "Tak for din henvendelse. Vi vil vende tilbage til dig snarest.";
$LZLANG["client_message_flood"] = "Du har sendt for mange beskeder. Du er desværre nødt til at vente lidt inden du sender flere.";
$LZLANG["client_really_close"] = "Bekræft venligst at du ønsker at lukke vinduet.";
$LZLANG["client_file_request_rejected"] = "Supporteren godkendte ikke din anmodning.";
$LZLANG["client_file_upload_oversized"] = "Din fil blev ikke godkendt af vores server - filstørrelsen er over det tilladte.";
$LZLANG["client_file_upload_provided"] = "Din fil er sendt til vores supporter!";
$LZLANG["client_file_upload_requesting"] = "Admodning om upload...";
$LZLANG["client_file_upload_send_file"] = "Klik SEND for at sende den valgte fil";
$LZLANG["client_file_upload_select_file"] = "Find filen du vil sende.";
$LZLANG["client_wait_for_representative"] = "Vent venligst på supporteren!";
$LZLANG["client_no_representative"] = "Supporteren har allerede forladt samtalen.";
$LZLANG["client_transmitting_file"] = "Din fil sendes...";
$LZLANG["client_please_rate"] = "Vælg vurdéring først!";
$LZLANG["client_rate_reason"] = "Skriv venligst hvorfor du giver denne vurdéring";
$LZLANG["client_rate_qualification"] = "Kvalifikation";
$LZLANG["client_rate_politeness"] = "Høflighed";
$LZLANG["client_rate_success"] = "Tak, vi har modtaget din stemme. Din stemme hjælper os med at blive bedre. Tak for din vurdéring.";
$LZLANG["client_rate_max"] = "Du har ikke lov til at bruge vores vurdérings service mere end 3 gange dagligt.";
$LZLANG["client_file"] = "Fil";
$LZLANG["client_welcome"] = "Velkommen";
$LZLANG["client_send"] = "Send";
$LZLANG["client_abort"] = "Afbryd";
$LZLANG["index_close"] = "Luk";
$LZLANG["index_not_available"] = "Ikke tilgængelig";
$LZLANG["index_description_demochat"] = "Start en demo chat og kommunikér med interne brugere der er online på nuværende tidspunkt. Dette kan bruges i forhold til mange forskellige måder at teste LiveZilla på.";
?>
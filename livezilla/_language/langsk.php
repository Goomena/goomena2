<?php

/*********************************************************************************
* LiveZilla langsk.php
* 
* Copyright 2012 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
*
* Please report errors here: http://www.livezilla.net/forum/
* DO NOT REMOVE/ALTER <!--placeholders-->
* 
********************************************************************************/

$LZLANG["client_join_group"] = "Pripojil si sa do miesnosti <!--group_name-->";
$LZLANG["client_leave_group"] = "Opustil si miesnosť <!--group_name-->";
$LZLANG["client_accepted"] = "Akceptované";
$LZLANG["client_rejected"] = "Odmietnuté";
$LZLANG["client_forwarding_to"] = "Presmerovanie na";
$LZLANG["client_date"] = "Dátum";
$LZLANG["client_chat_reference_number"] = "Chat číslo";
$LZLANG["client_invalid_data"] = "The information given is invalid:";
$LZLANG["client_leave_message"] = "Zanechať odkaz";
$LZLANG["client_no"] = "Nie";
$LZLANG["client_yes"] = "Áno";
$LZLANG["client_ticket_header"] = "Niesme prítomný, nechajte nám prosím odkaz";
$LZLANG["client_ticket_information"] = "Momentálne niesme ONLINE, v dohľadnej dobe budeme prítomný. Nechajte nám prosím odkaz s čo najpodrobneším popisom, aby sme Vám mohli čo najskôr odpovedať.";
$LZLANG["client_request_chat_transcript"] = "Áno, pošlite prepis tohto chatu na môj e-mailový účet: ";
$LZLANG["client_use_auto_translation_service"] = "Používajte automatické prekladateľské služby. Môj jazyk:";
$LZLANG["client_fill_mandatory_fields"] = "Prosím vyplňte všetky povinné polia";
$LZLANG["client_your_question"] = "Váš dotaz";
$LZLANG["client_queue_message"] = "Vaša aktuálna pozícia vo fronte je <!--queue_position--> . Približná doba čakania: <!--queue_waiting_time-->  minúta (y)";
$LZLANG["client_queue_next_operator"] = "Ďalší zástupca k dispozícii jetu pre vás.";
$LZLANG["client_chat_invitation"] = "Chat pozvánka";
$LZLANG["client_chat_transcript"] = "Chat prepis";
$LZLANG["client_new_message"] = "Nová správa od";
$LZLANG["client_system"] = "Systém";
$LZLANG["client_required_field"] = "Povinné polia";
$LZLANG["client_int_is_connected"] = "Zástupca bude pripojený, prosím, buďte trpezliví.";
$LZLANG["client_ints_are_busy"] = "Všetci zástupcovia majú plné ruky práce, čakajte prosím.";
$LZLANG["client_int_left"] = "Zástupca opustil rozhovor. Ak máte ďalšie otázky, prosím zanechajte odkaz:";
$LZLANG["client_intern_left"] = "<!--intern_name--> opustil miestnosť.";
$LZLANG["client_int_declined"] = "Všetci dostupný zástupcovia sú v súčasnej dobe obsadený. Vaša žiadosť nemohla byť doručená. Ospravedlňujeme sa za prípadné nepríjemnosti, ktoré môže spôsobiť. Skúste to prosím neskôr alebo zanechajte odkaz: ";
$LZLANG["client_no_intern_users"] = "Žiadny zástupca tohto oddelenia nie je k dispozícii. Skúste to prosím neskôr alebo prepnutie do iného oddelenia. Môžete tiež zanechať odkaz:";
$LZLANG["client_no_intern_users_short"] = "Žiadny zástupca tohto oddelenia je k dispozícii. Skúste to prosím neskôr alebo prepnutie do iného oddelenia.";
$LZLANG["client_thank_you"] = "Ďakujeme!";
$LZLANG["client_con_broken"] = "Chyba pripojenia, snaží sa znova. Prosím, buďte trpezliví ...";
$LZLANG["client_guide_request"] = "Zástupca ta chce presmerovať na túto stránku:";
$LZLANG["client_still_waiting_int"] = "Stále sa snažíme pripojiť, prosím, buďte trpezliví. Ak si nechcete čakať, prosím zanechajte odkaz: ";
$LZLANG["client_request"] = "Žiadosť";
$LZLANG["client_loading"] = "Načítavam";
$LZLANG["client_forwarding"] = "Budete odovzdaný k inému zástupcovi, prosím, buďte trpezliví. ";
$LZLANG["client_vcard"] = "Vizitka";
$LZLANG["client_guest"] = "Hosť";
$LZLANG["client_topic"] = "Téma";
$LZLANG["client_send_file"] = "Odoslať súbor";
$LZLANG["client_representative_is_typing"] = "Zástupca, píše ... ";
$LZLANG["client_save_visitcard"] = "Stiahni vizitku";
$LZLANG["client_please_enter_name_and_email"] = "Prosím, zadajte svoje meno a e-mailovú adresu!";
$LZLANG["client_start_system"] = "Systém sa štartuje";
$LZLANG["client_your_email"] = "E-mail";
$LZLANG["client_your_company"] = "Firma";
$LZLANG["client_your_name"] = "Meno";
$LZLANG["client_your_message"] = "Správa";
$LZLANG["client_group"] = "Oddelenie";
$LZLANG["client_send_message"] = "Poslať správu";
$LZLANG["client_error_groups"] = "Táto služba je deaktivovaná z nasledujúceho dôvodu: No oddelenie je dovolené prijať externý chaty. Ospravedlňujeme sa za prípadné nepríjemnosti, ktoré môže spôsobiť.";
$LZLANG["client_error_deactivated"] = "Táto služba je momentálne neaktívna. Ospravedlňujeme sa za prípadné nepríjemnosti, ktoré môže spôsobiť. ";
$LZLANG["client_error_unavailable"] = "Táto služba nie je k dispozícii. Ospravedlňujeme sa za prípadné nepríjemnosti, ktoré môže spôsobiť. ";
$LZLANG["client_select_valid_group"] = "Prosím, zvoľte Platné oddelenie. ";
$LZLANG["client_important_hint"] = "Dôležité upozornenie";
$LZLANG["client_print"] = "Tlačiť";
$LZLANG["client_close_window"] = "Zavrieť okno";
$LZLANG["client_rate_representative"] = "Ohodnoť zástupcu";
$LZLANG["client_bookmark"] = "Pridať k obľúbeným";
$LZLANG["client_insert_smiley"] = "Vlož smajlíka";
$LZLANG["client_goto_login"] = "Na prihlasovaciu stránku";
$LZLANG["client_switch_sounds"] = "Zvuky zapni/vipni";
$LZLANG["client_intern_arrives"] = "<!--intern_name--> vstupuje do miestnosti";
$LZLANG["client_start_chat"] = "Štart chatu";
$LZLANG["client_start_chat_information"] = "Vitajte v našej online podpore! Opýtajte sa nás čo chcete vedieť. Odpovedia Vám naši zástupcovia.";
$LZLANG["client_new_messages"] = "Nová správa";
$LZLANG["client_message_too_long"] = "Táto správa je príliš dlhá. Ak chcete správu odoslať musíte ju skrátiť";
$LZLANG["client_message_received"] = "Ďakujeme Vám. Budeme Vás kontaktovať.";
$LZLANG["client_message_flood"] = "Odoslali príliš veľa správ. Prosím, počkajte nejakú dobu a skúste to znova.";
$LZLANG["client_really_close"] = "Naozaj chcete zatvoriť tento chat?";
$LZLANG["client_file_request_rejected"] = "Zástupca neprijal vašu požiadavku!";
$LZLANG["client_file_upload_oversized"] = "Váš súbor nebol prijatý na serveri (veľkosť súboru je príliš veľký alebo má zakázanú príponou)!";
$LZLANG["client_file_upload_provided"] = "Váš súbor bol odoslaný zástupcovi";
$LZLANG["client_file_upload_requesting"] = "Požadujúce povolenie na odoslanie";
$LZLANG["client_file_upload_send_file"] = "Kliknite na Odoslať nahrať vybraný súbor ";
$LZLANG["client_file_upload_select_file"] = "Prosím, vyberte súbor pre upload. ";
$LZLANG["client_wait_for_representative"] = "Počkajte prosím zástupcu!";
$LZLANG["client_no_representative"] = "Zástupca už opustil tento rozhovor.";
$LZLANG["client_transmitting_file"] = "Prenos súboru ...";
$LZLANG["client_please_rate"] = "Prosím, vyberte hodnotenie prvý! ";
$LZLANG["client_rate_reason"] = "Prosím, povedzte nám, prečo ste ohodnotili našu podporu týmto spôsobom";
$LZLANG["client_rate_qualification"] = "Kvalifikácia";
$LZLANG["client_rate_politeness"] = "Zdvorilosť";
$LZLANG["client_rate_success"] = "Ďakujem vám, sme dostali Vaše hodnotenie. Vaše reakcie nám pomáha zlepšiť túto službu. Veľmi si vážime vašu spätnej väzby.";
$LZLANG["client_rate_max"] = "Nie je dovolené hodnotiť naše služby viac ako 3 krát denne. ";
$LZLANG["client_file"] = "Súbor";
$LZLANG["client_welcome"] = "Vitaj!";
$LZLANG["client_send"] = "Poslať";
$LZLANG["client_abort"] = "Prerušiť";
$LZLANG["index_close"] = "Zavrieť";
$LZLANG["index_not_available"] = "Nie je k dispozícii ";
$LZLANG["index_description_demochat"] = "Spustite demo chat a komunikujte s internými užívateľmi, ktorí sú práve online. Táto voľba umožnuje otestovať všetky vlastnosti LiveZilla.";
?>
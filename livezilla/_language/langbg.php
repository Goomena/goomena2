<?php

/*********************************************************************************
* LiveZilla langbg.php
* 
* Copyright 2012 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
*
* Please report errors here: http://www.livezilla.net/forum/
* DO NOT REMOVE/ALTER <!--placeholders-->
* 
********************************************************************************/

$LZLANG["client_join_group"] = "Присъединихте се към група <!--group_name-->.";
$LZLANG["client_leave_group"] = "Напуснахте група <!--group_name-->.";
$LZLANG["client_accepted"] = "Приет";
$LZLANG["client_rejected"] = "Отхвърлен";
$LZLANG["client_forwarding_to"] = "Препращане към";
$LZLANG["client_date"] = "Дата";
$LZLANG["client_chat_reference_number"] = "Номер на разговора";
$LZLANG["client_invalid_data"] = "Посочените данни са невалидни";
$LZLANG["client_leave_message"] = "оставете съобщение";
$LZLANG["client_no"] = "Да";
$LZLANG["client_yes"] = "Не";
$LZLANG["client_ticket_header"] = "В момента не сме на линия. Моля оставете съобщение";
$LZLANG["client_ticket_information"] = "Моля оставете съобщение и ще се свържем с вас скоро. Посочете колкото се може повече детайли за вашия въпрос.";
$LZLANG["client_request_chat_transcript"] = "Да, изпрати копие от този чат на моя e-mail адрес:";
$LZLANG["client_use_auto_translation_service"] = "Използвай автоматичен превод. Моят език :";
$LZLANG["client_fill_mandatory_fields"] = "Попълнете всички задължителни полета.";
$LZLANG["client_your_question"] = "Вашият въпрос";
$LZLANG["client_queue_message"] = "Текущата Ви позиция на опашката е <!--queue_position-->. Приблизително време за изчакване: <!--queue_waiting_time--> минута/и.";
$LZLANG["client_queue_next_operator"] = "Следващият свободен консултант е запазен за вас";
$LZLANG["client_chat_invitation"] = "Покана за разговор";
$LZLANG["client_chat_transcript"] = "Стенограма на разговора";
$LZLANG["client_new_message"] = "Ново съобщение от";
$LZLANG["client_system"] = "Система";
$LZLANG["client_required_field"] = "Задължителни полета";
$LZLANG["client_int_is_connected"] = "Наш консултант ще ви помогне всеки момент.";
$LZLANG["client_ints_are_busy"] = "Всички консултанти са заети в момента. Моля изчакайте.";
$LZLANG["client_int_left"] = "Консултантът напусна разговора. Ако имате допълнителни въпроси, моля да оставите съобщение:";
$LZLANG["client_intern_left"] = "<!--intern_name--> has left the chatroom.";
$LZLANG["client_int_declined"] = "Всички консултанти са заети. Вашето запитване не може да бъде обслужено. Извиняваме се за неудобството. Моля, опитайте отново или оставете съобщение:";
$LZLANG["client_no_intern_users"] = "Всички оператори от този отдел са извън линия. Моля, опитайте да се свържете по-късно или изберете друг отдел. Също така може да оставите съобщение:";
$LZLANG["client_no_intern_users_short"] = "Всички оператори от този отдел са извън линия. Моля, опитайте да се свържете по-късно или изберете друг отдел.";
$LZLANG["client_thank_you"] = "Благодаря!";
$LZLANG["client_con_broken"] = "Проблем с връзката, опит за повторно свързване. Моля изчакйте...";
$LZLANG["client_guide_request"] = "Консултантът иска да ви насочи към тази страница:";
$LZLANG["client_still_waiting_int"] = "Свързване. Ако не искате да чакате, моля да оставите съобщение:";
$LZLANG["client_request"] = "Заявка";
$LZLANG["client_loading"] = "Зареждане";
$LZLANG["client_forwarding"] = "В момента Ви пренасочеваме към друг оператор, моля имайте търпение.";
$LZLANG["client_vcard"] = "Визитка";
$LZLANG["client_guest"] = "Посетител";
$LZLANG["client_topic"] = "Тема";
$LZLANG["client_send_file"] = "Изпратете файл";
$LZLANG["client_representative_is_typing"] = "Консултантът пише съобщение...";
$LZLANG["client_save_visitcard"] = "Изтегли визитка";
$LZLANG["client_please_enter_name_and_email"] = "Моля, посочете името си и e-mail адрес!";
$LZLANG["client_start_system"] = "Системата стартира";
$LZLANG["client_your_email"] = "Вашият Е-мейл";
$LZLANG["client_your_company"] = "Фирма";
$LZLANG["client_your_name"] = "Вашето име";
$LZLANG["client_your_message"] = "Вашето съобщение";
$LZLANG["client_group"] = "Отдел";
$LZLANG["client_send_message"] = "Изпрати";
$LZLANG["client_error_groups"] = "Тази услуга е изключена поради следната причина: Всички отдели имат забрана за приемане на външни разговори. Извиняваме се за създаденото неудобство.";
$LZLANG["client_error_deactivated"] = "Тази услуга в момента е изключена. Извиняваме се за създаденото неудобство.";
$LZLANG["client_error_unavailable"] = "Тази услуга е изключена. Извиняваме се за създаденото неудобство";
$LZLANG["client_select_valid_group"] = "Моля, изберете валиден отдел.";
$LZLANG["client_important_hint"] = "Важни Съвети";
$LZLANG["client_print"] = "Принтирай";
$LZLANG["client_close_window"] = "Затвори прозореца";
$LZLANG["client_rate_representative"] = "Оценете консултанта";
$LZLANG["client_bookmark"] = "Добави в любими";
$LZLANG["client_insert_smiley"] = "Добавете усмивка";
$LZLANG["client_goto_login"] = "Върнете се на началния екран";
$LZLANG["client_switch_sounds"] = "Вклюете/изключете звука";
$LZLANG["client_intern_arrives"] = "<!--intern_name--> се включи в разговора.";
$LZLANG["client_start_chat"] = "Разговор";
$LZLANG["client_start_chat_information"] = "Welcome to our Online Support! To help us serve you better, please provide some information before initiating the chat with a representative.";
$LZLANG["client_new_messages"] = "Нови съобщения";
$LZLANG["client_message_too_long"] = "Това съобщение е прекалено дълго. За да бъде изпратено трябва да се съкрати.";
$LZLANG["client_message_received"] = "Благодарим ви. Ще се свържем с вас скоро.";
$LZLANG["client_message_flood"] = "Изпратили сте прекалено много съобщения. Моля изчакайте малко и опитайте пак.";
$LZLANG["client_really_close"] = "Сигурни ли сте, че искате да затворите този прозорец?";
$LZLANG["client_file_request_rejected"] = "Консултантът не прие заявката ви!";
$LZLANG["client_file_upload_oversized"] = "Файлът не беше приет от сървъра (прекалено е голям или е със забранено разширение.)";
$LZLANG["client_file_upload_provided"] = "Файлът беше предоставен на консултантът";
$LZLANG["client_file_upload_requesting"] = "Изчаква разрешение за качване ...";
$LZLANG["client_file_upload_send_file"] = "Кликнете ИЗПРАТИ, за да качите файла";
$LZLANG["client_file_upload_select_file"] = "Изберете файл за изпращане.";
$LZLANG["client_wait_for_representative"] = "Изчакайте консултант!";
$LZLANG["client_no_representative"] = "Консултантът е напуснал разговора.";
$LZLANG["client_transmitting_file"] = "Изпращане на файла...";
$LZLANG["client_please_rate"] = "Моля, първо изберете оценка!";
$LZLANG["client_rate_reason"] = "Моля, споделете с нас, защо оценихте нашия оператор по този начин";
$LZLANG["client_rate_qualification"] = "Квалификация";
$LZLANG["client_rate_politeness"] = "Вежливост";
$LZLANG["client_rate_success"] = "Благодарим Ви за направената оценка. По този начин Вие ни помагате да подобрим качеството на нашите услуги. Високо ценим Вашата оценка.";
$LZLANG["client_rate_max"] = "Нямате право да оценявате услуги повече от 3 пъти на ден";
$LZLANG["client_file"] = "Файл";
$LZLANG["client_welcome"] = "Здравейте";
$LZLANG["client_send"] = "Изпрати";
$LZLANG["client_abort"] = "Прекрати";
$LZLANG["index_close"] = "Затвори";
$LZLANG["index_not_available"] = "Не е достъпен";
$LZLANG["index_description_demochat"] = "Стартирайте Демо разговор и говорете с операторите, който в момента са включени.";
?>
<?php

/*********************************************************************************
* LiveZilla langpt.php
* 
* Copyright 2012 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
*
* Please report errors here: http://www.livezilla.net/forum/
* DO NOT REMOVE/ALTER <!--placeholders-->
* 
********************************************************************************/

$LZLANG["client_join_group"] = "Juntou-se ao grupo <!--group_name-->.";
$LZLANG["client_leave_group"] = "Saiu do grupo <!--group_name-->.";
$LZLANG["client_accepted"] = "Aceite";
$LZLANG["client_rejected"] = "Rejeitado";
$LZLANG["client_forwarding_to"] = "Encaminhar para";
$LZLANG["client_date"] = "Data";
$LZLANG["client_chat_reference_number"] = "Número de Referência do Chat";
$LZLANG["client_invalid_data"] = "A informação fornecida é invalida:";
$LZLANG["client_leave_message"] = "Deixe a sua mensagem";
$LZLANG["client_no"] = "Não";
$LZLANG["client_yes"] = "Sim";
$LZLANG["client_ticket_header"] = "Estamos offline, por favor deixe a sua mensagem";
$LZLANG["client_ticket_information"] = "Por favor, deixe a sua mensagem e vamos responder o mais breve possivel. Forneça o máximo de detalhes possíveis, juntamente com as suas informações de contato.";
$LZLANG["client_request_chat_transcript"] = "Sim, envie uma transcrição deste chat para o meu e-mail:";
$LZLANG["client_use_auto_translation_service"] = "Usar o serviço de auto tradução. A minha linguagem é:";
$LZLANG["client_fill_mandatory_fields"] = "Por favor, preencha todos os campos necessários.";
$LZLANG["client_your_question"] = "Sua questão";
$LZLANG["client_queue_message"] = "Sua posição atual na fila é <!--queue_position-->. Tempo de espera aproximado: <!--queue_waiting_time--> minuto (s).";
$LZLANG["client_queue_next_operator"] = "O próximo representante disponível está reservado para você.";
$LZLANG["client_chat_invitation"] = "Convite de Chat";
$LZLANG["client_chat_transcript"] = "Transcrição de Chat";
$LZLANG["client_new_message"] = "Nova mensagem de";
$LZLANG["client_system"] = "Sistema";
$LZLANG["client_required_field"] = "Campos obrigatorios";
$LZLANG["client_int_is_connected"] = "Um representante será conectado, por favor, seja paciente.";
$LZLANG["client_ints_are_busy"] = "Todos os representantes estão ocupados de momento, espere por favor.";
$LZLANG["client_int_left"] = "O representante abandonou a conversa. Se estiver alguma questão deixe a sua mensagem.";
$LZLANG["client_intern_left"] = "<!--intern_name--> deixou a sala de chat";
$LZLANG["client_int_declined"] = "Todos os representantes disponíveis estão atualmente ocupados. Seu pedido não pôde ser atendido. Pedimos desculpas por qualquer inconveniente que isso possa causar. Por favor, tente novamente mais tarde ou deixe a sua mensagem:";
$LZLANG["client_no_intern_users"] = "Nenhum representante deste departamento está disponível. Por favor, tente novamente mais tarde ou mude para outro departamento. Você também pode deixar a sua mensagem:";
$LZLANG["client_no_intern_users_short"] = "Nenhum representante deste departamento está disponível. Por favor, tente novamente mais tarde ou mudar para outro departamento.";
$LZLANG["client_thank_you"] = "Obrigado!";
$LZLANG["client_con_broken"] = "Erro de ligação, tentando ligar novamente. Por favor seja paciente ...";
$LZLANG["client_guide_request"] = "Um representante quer redirecioná-lo para esta página:";
$LZLANG["client_still_waiting_int"] = "Estamos ainda a tentar ligar-lhe, por favor, seja paciente. Se você não quiser esperar por favor deixe a sua mensagem:";
$LZLANG["client_request"] = "Pedido";
$LZLANG["client_loading"] = "Carregando";
$LZLANG["client_forwarding"] = "Vai ser encaminhado para outro representante, aguarde por favor.";
$LZLANG["client_vcard"] = "Cartão de Visita";
$LZLANG["client_guest"] = "Visitante";
$LZLANG["client_topic"] = "Topico";
$LZLANG["client_send_file"] = "Enviar ficheiro";
$LZLANG["client_representative_is_typing"] = "Representante está a escrever ...";
$LZLANG["client_save_visitcard"] = "Descarregar Cartão de Visita";
$LZLANG["client_please_enter_name_and_email"] = "Por favor insira o seu name e o seu e-mail!";
$LZLANG["client_start_system"] = "O sistema está a carregar.";
$LZLANG["client_your_email"] = "Email";
$LZLANG["client_your_company"] = "Empresa";
$LZLANG["client_your_name"] = "Nome";
$LZLANG["client_your_message"] = "Mensagem";
$LZLANG["client_group"] = "Departamento";
$LZLANG["client_send_message"] = "Enviar";
$LZLANG["client_error_groups"] = "Este serviço está desativado pelo seguinte motivo: Não é permitido nenhum departamento aceitar chats externos. Pedimos desculpas por qualquer inconveniente que isso possa causar.";
$LZLANG["client_error_deactivated"] = "Este serviço está atualmente desativado. Pedimos desculpas por qualquer inconveniente que isso possa causar.";
$LZLANG["client_error_unavailable"] = "Este serviço não está disponível. Pedimos desculpas por qualquer inconveniente que isso possa causar.";
$LZLANG["client_select_valid_group"] = "Por favor selecione um departamento valido";
$LZLANG["client_important_hint"] = "Dica Importante";
$LZLANG["client_print"] = "Imprimir";
$LZLANG["client_close_window"] = "Fechar a janela";
$LZLANG["client_rate_representative"] = "Avalie o representante";
$LZLANG["client_bookmark"] = "Add aos favoritos";
$LZLANG["client_insert_smiley"] = "Inserir smile";
$LZLANG["client_goto_login"] = "Ir para a area de login";
$LZLANG["client_switch_sounds"] = "Som on/off";
$LZLANG["client_intern_arrives"] = "<!--intern_name--> entrou na sala de chat.";
$LZLANG["client_start_chat"] = "Iniciar Chat";
$LZLANG["client_start_chat_information"] = "Bem-vindo ao nosso suporte on-line! Para ajudar-nos a servi-lo melhor, por favor, forneca algumas informações antes de iniciar o chat com um representante.";
$LZLANG["client_new_messages"] = "Nova mensagem";
$LZLANG["client_message_too_long"] = "Esta mensagem é muito longa. Para enviar esta mensagem deve ser encurtada.";
$LZLANG["client_message_received"] = "Obrigado! Entraremos em contato com você em breve.";
$LZLANG["client_message_flood"] = "Enviou muitas mensagens seguidas. Por favor, aguarde algum tempo e tente novamente.";
$LZLANG["client_really_close"] = "Realmente deseja fechar o chat agora?";
$LZLANG["client_file_request_rejected"] = "O representante não aceitou o seu pedido!";
$LZLANG["client_file_upload_oversized"] = "O ficheiro não foi aceite pelo servidor (tamanho muito grande ou extensão proibida)!";
$LZLANG["client_file_upload_provided"] = "O seu ficheiro foi fornecido para o nosso representante!";
$LZLANG["client_file_upload_requesting"] = "Solicitando permissão para upload";
$LZLANG["client_file_upload_send_file"] = "Clique em ENVIAR para fazer upload do ficheiro selecionado";
$LZLANG["client_file_upload_select_file"] = "Por favor selecione o ficheiro para upload.";
$LZLANG["client_wait_for_representative"] = "Aguarde um representante!";
$LZLANG["client_no_representative"] = "O representante já saiu desta conversa.";
$LZLANG["client_transmitting_file"] = "A enviar o seu ficheiro ...";
$LZLANG["client_please_rate"] = "Por favor selecione o score primeiro!";
$LZLANG["client_rate_reason"] = "Por favor diga-nos por que avaliou o nosso apoio dessa forma";
$LZLANG["client_rate_qualification"] = "Qualificação";
$LZLANG["client_rate_politeness"] = "Polidez";
$LZLANG["client_rate_success"] = "Obrigado, recebemos a sua avaliação. A sua avaliação ajuda-nos a melhorar o nosso serviço. Agradecemos imensamente o seu feedback.";
$LZLANG["client_rate_max"] = "Não pode avaliar o nosso serviço mais que 3 vezes por dia.";
$LZLANG["client_file"] = "Ficheiro";
$LZLANG["client_welcome"] = "Bem vindo";
$LZLANG["client_send"] = "Enviar";
$LZLANG["client_abort"] = "Cancelar";
$LZLANG["index_close"] = "Fechar";
$LZLANG["index_not_available"] = "Não disponivel";
$LZLANG["index_description_demochat"] = "Iniciar um chat de demonstração para conversar com os representantes internos, que estão online de momento. Isto pode ser usado para todos os tipos de testes.";
?>
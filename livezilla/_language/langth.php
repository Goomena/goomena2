<?php

/*********************************************************************************
* LiveZilla langth.php
* 
* Copyright 2012 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
*
* Please report errors here: http://www.livezilla.net/forum/
* DO NOT REMOVE/ALTER <!--placeholders-->
* 
********************************************************************************/

$LZLANG["client_join_group"] = "คุณได้เข้าร่วมกลุ่ม <!--group_name-->";
$LZLANG["client_leave_group"] = "คุณออกจากกลุ่ม <!--group_name-->";
$LZLANG["client_accepted"] = "ตอบรับ";
$LZLANG["client_rejected"] = "ปฏิเสธ";
$LZLANG["client_forwarding_to"] = "ส่งต่อไปยัง";
$LZLANG["client_date"] = "วันที่";
$LZLANG["client_chat_reference_number"] = "เลขที่อ้างอิงการสนทนา";
$LZLANG["client_invalid_data"] = "คุณระบุข้อมูลที่ให้มาไม่ถูกต้อง:";
$LZLANG["client_leave_message"] = "ฝากข้อความ";
$LZLANG["client_no"] = "ไม่";
$LZLANG["client_yes"] = "ใช่";
$LZLANG["client_ticket_header"] = "Leave a message";
$LZLANG["client_ticket_information"] = "กรุณาฝากข้อความไว้ แล้วพนักงานจะติดต่อกลับไปภายหลังค่ะ";
$LZLANG["client_request_chat_transcript"] = "ใช่, ส่งหลักฐานของการแชทนี้ไปยังบัญชีอีเมลของฉัน";
$LZLANG["client_use_auto_translation_service"] = "ใช้บริการแปลภาษาอัตโนมัติ ภาษาของฉัน";
$LZLANG["client_fill_mandatory_fields"] = "กรุณากรอกฟิลด์ที่จำเป็นทั้งหมด";
$LZLANG["client_your_question"] = "คำถามของคุณ";
$LZLANG["client_queue_message"] = "ตำแหน่งปัจจุบันของคุณในคิวคือ <!--queue_position-->. โดยประมาณเวลารอคอย : <!--queue_waiting_time--> นาที";
$LZLANG["client_queue_next_operator"] = "พนักงานถัดไปใช้ได้สงวนไว้สำหรับคุณ";
$LZLANG["client_chat_invitation"] = "เชิญสนทนา";
$LZLANG["client_chat_transcript"] = "Chat Transcript";
$LZLANG["client_new_message"] = "ข้อความใหม่จาก";
$LZLANG["client_system"] = "ระบบอัตโนมัติ";
$LZLANG["client_required_field"] = "จำเป็นต้องกรอก";
$LZLANG["client_int_is_connected"] = "ระบบกำลังติดต่อกับเจ้าหน้าที่, กรุณารอสักครู่...";
$LZLANG["client_ints_are_busy"] = "พนักงานไม่ว่างในขณะนี้โปรดรอสักครู่";
$LZLANG["client_int_left"] = "ขณะนี้เจ้าหน้าที่ออกจากระบบสนทนาแล้ว. หากท่านมีคำถามเพิ่มเติมกรุณาฝากข้อความทิ้งไว้:";
$LZLANG["client_intern_left"] = "<!--intern_name--> ได้ออกจากห้องสนทนา";
$LZLANG["client_int_declined"] = "ขณะนี้เจ้าหน้าที่กำลังให้บริการลูกค้าท่านอื่นอยู่ จึงไม่สามารถสนทนากับท่านได้. ต้องกราบขออภัยในความไม่สะดวก. กรุณาติดต่อใหม่อีกครั้ง หรือ ฝากข้อความทิ้งไว้:";
$LZLANG["client_no_intern_users"] = "ขณะนี้ไม่มีเจ้าหน้าที่ในฝ่ายที่ท่านต้องการติดต่อ. กรุณาติดต่อใหม่อีกครั้งโดยเลือกฝ่ายอื่นแทน หรือ ฝากข้อความทิ้งไว้:";
$LZLANG["client_no_intern_users_short"] = "พนักงานของแผนกนี้ไม่สามารถใช้ได้ โปรดลองอีกครั้งในภายหลังหรือสลับไปยังแผนกอื่น";
$LZLANG["client_thank_you"] = "ขอบคุณมากค่ะ!";
$LZLANG["client_con_broken"] = "ระบบเชื่อมต่อขัดข้อง, กำลังพยายามติดต่อใหม่อีกครั้ง. กรุณารอสักครู่ ...";
$LZLANG["client_guide_request"] = "ท่านสามารถดูรายละเอียดเพิ่มเติมได้ที่หน้านี้:";
$LZLANG["client_still_waiting_int"] = "เจ้าหน้าที่กำลังพยายามติดต่อกับท่าน, กรุณารอสักครู่. หากท่านไม่ต้องการรอ กรุณาฝากข้อความทิ้งไว้:";
$LZLANG["client_request"] = "ขอ";
$LZLANG["client_loading"] = "กำลังโหลด";
$LZLANG["client_forwarding"] = "ระบบกำลังโอนสายของท่านไปยังเจ้าหน้าที่ในฝ่ายที่เกี่ยวข้อง, กรุณารอสักครู่...";
$LZLANG["client_vcard"] = "ข้อมูลติดต่อ";
$LZLANG["client_guest"] = "แขกผู้มาเยือน";
$LZLANG["client_topic"] = "หัวเรื่อง";
$LZLANG["client_send_file"] = "ส่งไฟล์";
$LZLANG["client_representative_is_typing"] = "เจ้าหน้าที่กำลังพยายาม...";
$LZLANG["client_save_visitcard"] = "ดาวน์โหลดข้อมูลติดต่อ";
$LZLANG["client_please_enter_name_and_email"] = "กรุณากรอกชื่อและอีเมล์ของท่านด้วย!";
$LZLANG["client_start_system"] = "เริ่มต้นเข้าสู่ระบบการสนทนา.";
$LZLANG["client_your_email"] = "อีเมล์";
$LZLANG["client_your_company"] = "บริษัท";
$LZLANG["client_your_name"] = "ชื่อ";
$LZLANG["client_your_message"] = "ข้อความของท่าน";
$LZLANG["client_group"] = "ฝ่าย";
$LZLANG["client_send_message"] = "ส่งข้อความ";
$LZLANG["client_error_groups"] = "ระบบหยุดการให้บริการชั่วคราวเนื่องจาก: ขณะนี้ไม่มีเจ้าหน้าที่ฝ่ายบริการลูกค้าออนไลน์. ต้องกราบขออภัยในความไม่สะดวก";
$LZLANG["client_error_deactivated"] = "ระบบหยุดการให้บริการชั่วคราว. ต้องกราบขออภัยในความไม่สะดวก.";
$LZLANG["client_error_unavailable"] = "ระบบหยุดการให้บริการชั่วคราว. ต้องกราบขออภัยในความไม่สะดวก.";
$LZLANG["client_select_valid_group"] = "กรุณาเลือกฝ่ายที่ต้องการติดต่อ";
$LZLANG["client_important_hint"] = "คำแนะนำที่สำคัญ";
$LZLANG["client_print"] = "พิมพ์";
$LZLANG["client_close_window"] = "ปิดหน้าต่าง";
$LZLANG["client_rate_representative"] = "ให้คะแนนเจ้าหน้าที่";
$LZLANG["client_bookmark"] = "เพิ่มในรายการโปรด";
$LZLANG["client_insert_smiley"] = "ใส่สัญญลักษณ์";
$LZLANG["client_goto_login"] = "ไปที่หน้า login";
$LZLANG["client_switch_sounds"] = "เปิด/ปิด เสียง";
$LZLANG["client_intern_arrives"] = "<!--intern_name--> เข้าสู่ห้องสนทนา";
$LZLANG["client_start_chat"] = "เริ่มสนทนา";
$LZLANG["client_start_chat_information"] = "ยินดีต้อนรับเข้าสู่บริการออนไลน์ของเรา! เพื่อช่วยให้เราบริการคุณดีขึ้นโปรดให้ข้อมูลบางอย่างก่อนที่จะเริ่มสนทนากับเจ้าหน้าที่";
$LZLANG["client_new_messages"] = "ข้อความใหม่";
$LZLANG["client_message_too_long"] = "ข้อความยาวเกินกำหนด. กรุณาพิมพ์ข้อความให้กระชับ";
$LZLANG["client_message_received"] = "ขอขอบคุณ! ทางทีมงานจะติดต่อกลับหาท่านโดยเร็วที่สุด";
$LZLANG["client_message_flood"] = "ท่านส่งข้อความเป็นจำนวนมาก. กรุณารอสักครู่ หรือ ติดต่อกลับมาใหม่อีกครั้ง";
$LZLANG["client_really_close"] = "ท่านต้องการปิดหน้าต่างสนทนานี้ใช่ไหม?";
$LZLANG["client_file_request_rejected"] = "เจ้าหน้าที่ปฏิเสธคำร้องขอของท่าน!";
$LZLANG["client_file_upload_oversized"] = "ไฟล์ที่ท่านต้องการ upload มีขนาดใหญ่เกินกำหนด!";
$LZLANG["client_file_upload_provided"] = "ไฟล์ของท่านได้ถูกส่งให้เจ้าหน้าที่ของเราเรียบร้อยแล้ว!";
$LZLANG["client_file_upload_requesting"] = "ส่งคำร้องขออนุญาตเพื่อ upload ไฟล์ ...";
$LZLANG["client_file_upload_send_file"] = "คลิกที่ปุ่ม SEND เพื่อ upload ไฟล์ที่ท่านต้องการ";
$LZLANG["client_file_upload_select_file"] = "กรุณาเลือกไฟล์ที่ท่านต้องการ upload.";
$LZLANG["client_wait_for_representative"] = "กรุณารอสักครู่!";
$LZLANG["client_no_representative"] = "ขณะนี้เจ้าหน้าที่ได้ออกจากการสนทนาไปแล้ว.";
$LZLANG["client_transmitting_file"] = "ระบบกำลัง upload ไฟล์ของท่าน ...";
$LZLANG["client_please_rate"] = "กรุณาเลือกคะแนนก่อน!";
$LZLANG["client_rate_reason"] = "กรุณาแจ้งเหตุผลว่าทำไมท่านจึงให้คะแนนเจ้าหน้าที่ผ่านทางระบบนี้";
$LZLANG["client_rate_qualification"] = "ความสามารถในการตอบคำถาม";
$LZLANG["client_rate_politeness"] = "ความสุภาพอ่อนโยน";
$LZLANG["client_rate_success"] = "ขอขอบคุณ, ทางทีมงานได้รับคำติชมจากท่านแล้ว. คำติชมของท่านจะช่วยให้พวกเราสามารถพัฒนาการให้บริการให้ดียิ่งขึ้น. พวกเรายินดีเป็นอย่างยิ่งที่ได้รับคำติชมจากท่าน.";
$LZLANG["client_rate_max"] = "ท่านสามารถให้คะแนนเจ้าหน้าที่ได้ ไม่เกิน 3 ครั้งต่อวัน";
$LZLANG["client_file"] = "ไฟล์";
$LZLANG["client_welcome"] = "ยินดีต้อนรับ";
$LZLANG["client_send"] = "ส่ง";
$LZLANG["client_abort"] = "ยกเลิก";
$LZLANG["index_close"] = "ปิด";
$LZLANG["index_not_available"] = "ไม่สามารถให้บริการได้";
$LZLANG["index_description_demochat"] = "เริ่มต้นทดลองใช้งาน โดยการสนทนา หรือ พูดคุยกับ พนักงานภายในองค์กร ซึ่งกำลังออนไลน์อยู่ในขณะนี้. ท่านสามารถใช้งานในส่วนต่างๆ เพื่อทดสอบระบบ LiveZilla.";
?>
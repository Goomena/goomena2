<?php

/*********************************************************************************
* LiveZilla langpl.php
* 
* Copyright 2012 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
*
* Please report errors here: http://www.livezilla.net/forum/
* DO NOT REMOVE/ALTER <!--placeholders-->
* 
********************************************************************************/

$LZLANG["client_join_group"] = "Dołączyłeś do grupy <!--group_name-->.";
$LZLANG["client_leave_group"] = "Opuściłeś grupę <!--group_name-->.";
$LZLANG["client_accepted"] = "Zaakceptowane";
$LZLANG["client_rejected"] = "Odrzucone";
$LZLANG["client_forwarding_to"] = "Przekazane do";
$LZLANG["client_date"] = "Data";
$LZLANG["client_chat_reference_number"] = "Pokój rozmów numer";
$LZLANG["client_invalid_data"] = "Podana informacja jest nieprawidłowa";
$LZLANG["client_leave_message"] = "Zostaw wiadomość";
$LZLANG["client_no"] = "Nie";
$LZLANG["client_yes"] = "Tak";
$LZLANG["client_ticket_header"] = "Jestem offline, zostaw wiadomość";
$LZLANG["client_ticket_information"] = "Przepraszamy, żaden z naszych konsultantów nie jest teraz obecny, proszę zostaw wiadomość a my jak najszybciej odpiszemy na Twój problem!";
$LZLANG["client_request_chat_transcript"] = "Tak, wyślij zapis z tej rozmowy na moje konto email:";
$LZLANG["client_use_auto_translation_service"] = "Użyj automatycznego tłumaczenia. Mój język to:";
$LZLANG["client_fill_mandatory_fields"] = "Wypełnij wszystkie wymagane pola";
$LZLANG["client_your_question"] = "Twoje pytanie";
$LZLANG["client_queue_message"] = "Liczba oczekujących klientów <!--queue_position-->. Szacowany czas oczekiwania: <!--queue_waiting_time--> minut(y).";
$LZLANG["client_queue_next_operator"] = "Kolejny konsultant jest dostępny.";
$LZLANG["client_chat_invitation"] = "Zaproszenie do rozmowy";
$LZLANG["client_chat_transcript"] = "Tłumaczenie czatu";
$LZLANG["client_new_message"] = "Nowa wiadomośc od";
$LZLANG["client_system"] = "System ";
$LZLANG["client_required_field"] = "Wymagane pola";
$LZLANG["client_int_is_connected"] = "Proszę czekać na połączenie z konsultantem";
$LZLANG["client_ints_are_busy"] = "Wszyscy konsultanci sa aktualnie zajęci";
$LZLANG["client_int_left"] = "Konsultant zakończył rozmowe";
$LZLANG["client_intern_left"] = "	<!--intern_name--> opuścił pokój rozmów";
$LZLANG["client_int_declined"] = "Wszyscy nasi konsultanci są obecnie zajęci. Przepraszamy, ale Twoje połączenie nie może być zrealizowane w tej chwili. Prosimy spróbować ponownie lub pozostawić wiadomość:";
$LZLANG["client_no_intern_users"] = "Obecnie żaden z konsultantów w tym dziale nie jest dostępny. Prosimy spróbować ponownie za chwilę,lub wybrać inny dział. Możesz także zostawić nam wiadomość:";
$LZLANG["client_no_intern_users_short"] = "Obecnie żaden z konsultantów w tym dziale nie jest dostępny. Prosimy spróbować ponownie za chwilę,lub wybrać inny dział.";
$LZLANG["client_thank_you"] = "Dziękujemy!";
$LZLANG["client_con_broken"] = "Wystąpił błąd połączenia z serwerem. Trwa próba ponownego połączenia...";
$LZLANG["client_guide_request"] = "Nasz Konsultant spróbuje teraz przekierować Cię na stronę:";
$LZLANG["client_still_waiting_int"] = "Jeżeli uważasz, że czas oczekiwania jest zbyt długi-pozostaw nam wiadomoć, skontaktujemy się z tobą poprzez adres e-mail:";
$LZLANG["client_request"] = "Żądanie";
$LZLANG["client_loading"] = "Ładowanie";
$LZLANG["client_forwarding"] = "Trwa przeniesienie do innego konsultanta, prosimy o chwilę cierpliwości.";
$LZLANG["client_vcard"] = "Wizytówka";
$LZLANG["client_guest"] = "Gość";
$LZLANG["client_topic"] = "Temat";
$LZLANG["client_send_file"] = "Wyślij plik";
$LZLANG["client_representative_is_typing"] = "Konsultant pisze ...";
$LZLANG["client_save_visitcard"] = "Pobierz Wizytówkę";
$LZLANG["client_please_enter_name_and_email"] = "Podaj swoje Imię, Nazwisko oraz Email";
$LZLANG["client_start_system"] = "System startuje";
$LZLANG["client_your_email"] = "Twój Email";
$LZLANG["client_your_company"] = "Firma";
$LZLANG["client_your_name"] = "Imię i Nazwisko";
$LZLANG["client_your_message"] = "Twoja wiadomość";
$LZLANG["client_group"] = "Departament";
$LZLANG["client_send_message"] = "Wyślij wiadomość";
$LZLANG["client_error_groups"] = "Usługa nie jest obecnie aktywna..";
$LZLANG["client_error_deactivated"] = "Usługa nie jest obecnie uaktywniona.";
$LZLANG["client_error_unavailable"] = "Usługa nie jest obecnie dostępna.";
$LZLANG["client_select_valid_group"] = "Prosze wybrać odpowiedni departament";
$LZLANG["client_important_hint"] = "Ważna wskazówka";
$LZLANG["client_print"] = "Drukuj";
$LZLANG["client_close_window"] = "Zamknij okno";
$LZLANG["client_rate_representative"] = "Oceń Konsultanta";
$LZLANG["client_bookmark"] = "Dodaj do ulubionych";
$LZLANG["client_insert_smiley"] = "Wstaw usmieszek";
$LZLANG["client_goto_login"] = "Ekran logowania";
$LZLANG["client_switch_sounds"] = "Wyłącz dziwięk";
$LZLANG["client_intern_arrives"] = "Konsultant <!--intern_name--> jest obecnie dostępny.";
$LZLANG["client_start_chat"] = "Zacznij rozmowe";
$LZLANG["client_start_chat_information"] = "Witamy w naszej Pomocy Technicznej! Aby zapewnić najwyższą jakość usług, proszę podać niektóe informacje, które pozwolą naszemu konsultantowi lepiej Państwa obsłużyć.";
$LZLANG["client_new_messages"] = "Nowa wiadomość";
$LZLANG["client_message_too_long"] = "Wiadomość jest za długa";
$LZLANG["client_message_received"] = "Dziekujemy! Wkrótce się skontaktujemy.";
$LZLANG["client_message_flood"] = "Ilość wysłanych przez Ciebie wiadomości przekroczyła dozwolony limit. Prosimy spróbować później.";
$LZLANG["client_really_close"] = "Czy na pewno chcesz zakończyć rozmowę?";
$LZLANG["client_file_request_rejected"] = "Przepraszamy, konsultant nie zaakceptował Twojego zgłoszenia.";
$LZLANG["client_file_upload_oversized"] = "Przesłany plik przekroczył maksymalny rozmiar!";
$LZLANG["client_file_upload_provided"] = "Plik został pomyślnie przesłany do Konsultanta.";
$LZLANG["client_file_upload_requesting"] = "Oczekiwanie na uprawnienia do przesłania pliku ...";
$LZLANG["client_file_upload_send_file"] = "Aby wysłać wybrany plik, kliknij na WYŚLIJ";
$LZLANG["client_file_upload_select_file"] = "Proszę wybrać plik do wysłania.";
$LZLANG["client_wait_for_representative"] = "Prosimy o chwilę cierpliwości ...";
$LZLANG["client_no_representative"] = "Konsultant zakończył rozmowę.";
$LZLANG["client_transmitting_file"] = "Trwa przesyłanie pliku ...";
$LZLANG["client_please_rate"] = "Prosimy o ocenę naszej obsługi";
$LZLANG["client_rate_reason"] = "Jeżeli masz zastrzeżenia do jakości naszej obsługi, prosimy o kontakt e-mail";
$LZLANG["client_rate_qualification"] = "Kwalifikacje";
$LZLANG["client_rate_politeness"] = "Uprzejmość";
$LZLANG["client_rate_success"] = "Dziękujemy za ocenę i zapraszamy do skorzystania z naszej oferty.";
$LZLANG["client_rate_max"] = "Dzienny limit jest ograniczony do trzech ocen.";
$LZLANG["client_file"] = "Plik";
$LZLANG["client_welcome"] = "Witamy";
$LZLANG["client_send"] = "Wyślij";
$LZLANG["client_abort"] = "Przerwij";
$LZLANG["index_close"] = "Zamknij";
$LZLANG["index_not_available"] = "Niedostępne";
$LZLANG["index_description_demochat"] = "Rozpocznij czat demo, aby spróbować rozmowy z innymi użytkownikami którzy są aktualnie zalogowani. W ten sposób sprawdzisz możliwości aplikacji.";
?>
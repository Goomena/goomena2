<?php

/*********************************************************************************
* LiveZilla langpt-br.php
* 
* Copyright 2012 LiveZilla GmbH
* All rights reserved.
* LiveZilla is a registered trademark.
*
* Please report errors here: http://www.livezilla.net/forum/
* DO NOT REMOVE/ALTER <!--placeholders-->
* 
********************************************************************************/

$LZLANG["client_join_group"] = "Você se juntou ao grupo <!--group_name-->";
$LZLANG["client_leave_group"] = "Você deixou o grupo <!--group_name-->";
$LZLANG["client_accepted"] = "Aceitou";
$LZLANG["client_rejected"] = "Rejeitou";
$LZLANG["client_forwarding_to"] = "Redirecionando para";
$LZLANG["client_date"] = "Data";
$LZLANG["client_chat_reference_number"] = "Número de Referencia do Chat";
$LZLANG["client_invalid_data"] = "A Informação dada é invalida:";
$LZLANG["client_leave_message"] = "deixe uma mensagem";
$LZLANG["client_no"] = "Não";
$LZLANG["client_yes"] = "Sim";
$LZLANG["client_ticket_header"] = "Estamos offline, por favor deixe uma mensagem";
$LZLANG["client_ticket_information"] = "Nossos atendentes estão indisponíveis no momento. Por favor, informe abaixo sua dúvida que responderemos o mais brevemente possível.";
$LZLANG["client_request_chat_transcript"] = "Sim, envie uma cópia deste atendimento para o meu e-mail:";
$LZLANG["client_use_auto_translation_service"] = "Use o serviço auto-tradução. Meu idioma é:";
$LZLANG["client_fill_mandatory_fields"] = "Por favor, preencha os campos obrigatórios.";
$LZLANG["client_your_question"] = "Dúvida";
$LZLANG["client_queue_message"] = "Sua posição na fila é <!--queue_position-->. Em aproximadamente <!--queue_waiting_time--> minuto(s) você será atendido.";
$LZLANG["client_queue_next_operator"] = "O próximo atendente disponível está reservado para você.";
$LZLANG["client_chat_invitation"] = "Convite do Atendimento";
$LZLANG["client_chat_transcript"] = "Cópia da Conversa";
$LZLANG["client_new_message"] = "Nova mensagem de";
$LZLANG["client_system"] = "Sistema";
$LZLANG["client_required_field"] = "Campos obrigatórios";
$LZLANG["client_int_is_connected"] = "Um atendente está se conectando. Aguarde um instante, por favor.";
$LZLANG["client_ints_are_busy"] = "Todos os atendentes estão ocupados no momento. Aguarde um instante, por favor.";
$LZLANG["client_int_left"] = "O atendimento foi finalizado. Se ainda tiver alguma dúvida, chame novamente ou deixe uma mensagem:";
$LZLANG["client_intern_left"] = "<!--intern_name--> deixou a sala.	";
$LZLANG["client_int_declined"] = "Todos os representantes estão ocupados no momento. Seu pedido de atendimento não pode ser atendido. Nos desculpamos por qualquer  inconveniente que isto possa ter causado. Por favor tente novamente mais tarde ou deixe uma mensagem:";
$LZLANG["client_no_intern_users"] = "Nenhum atendente desse departamento está disponível. Por favor, tente novamente mais tarde ou escolha outro departamento. Você também pode deixar uma mensagem:";
$LZLANG["client_no_intern_users_short"] = "Nenhum operador desse departamento está disponível.";
$LZLANG["client_thank_you"] = "Obrigado!";
$LZLANG["client_con_broken"] = "Erro de conexão, tentando reconectar. Por favor, seja paciente ... ";
$LZLANG["client_guide_request"] = "Um atendente deseja direcioná-lo(a) para:";
$LZLANG["client_still_waiting_int"] = "Ainda estamos conectando-o(a) a um atendente. Por favor, aguarde um instante ou deixe uma mensagem:";
$LZLANG["client_request"] = "Pedido";
$LZLANG["client_loading"] = "Carregando";
$LZLANG["client_forwarding"] = "Você será redirecionado para outro atendente. Por favor, aguarde.";
$LZLANG["client_vcard"] = "Cartão de Visita";
$LZLANG["client_guest"] = "Convidado";
$LZLANG["client_topic"] = "Tópico";
$LZLANG["client_send_file"] = "Enviar Arquivo";
$LZLANG["client_representative_is_typing"] = "O atendente está digitando...";
$LZLANG["client_save_visitcard"] = "Download do cartão de visita";
$LZLANG["client_please_enter_name_and_email"] = "Por favor, informe seu nome e e-mail!";
$LZLANG["client_start_system"] = "Sistema iniciando";
$LZLANG["client_your_email"] = "Email";
$LZLANG["client_your_company"] = "Nome";
$LZLANG["client_your_name"] = "Seu nome";
$LZLANG["client_your_message"] = "Mensagem";
$LZLANG["client_group"] = "Departamento";
$LZLANG["client_send_message"] = "Enviar Mensagem";
$LZLANG["client_error_groups"] = "No momento o atendimento está offline pela seguinte razão: nenhum departamento está autorizado a aceitar conversas externas. As nossas desculpas pelo inconveniente.";
$LZLANG["client_error_deactivated"] = "No momento o atendimento está offline. Lamentamos pelo inconveniente.";
$LZLANG["client_error_unavailable"] = "Este serviço não está disponível. Lamentamos pelo inconveniente.";
$LZLANG["client_select_valid_group"] = "Por favor, selecione um departamento válido.";
$LZLANG["client_important_hint"] = "Dica Importante";
$LZLANG["client_print"] = "Imprimir";
$LZLANG["client_close_window"] = "Fechar Janela";
$LZLANG["client_rate_representative"] = "Avaliar Representante";
$LZLANG["client_bookmark"] = "Adicionar aos Favoritos";
$LZLANG["client_insert_smiley"] = "Inserir Emoticons";
$LZLANG["client_goto_login"] = "Ir para o login ";
$LZLANG["client_switch_sounds"] = "Ligar/Desligar Som";
$LZLANG["client_intern_arrives"] = "<!--intern_name--> entrou no atendimento.";
$LZLANG["client_start_chat"] = "Iniciar Chat";
$LZLANG["client_start_chat_information"] = "Bem vindo ao nosso Atendimento Online! Para nos ajudar a te atender melhor, por favor forneça algumas informações nates de iniciar um chat com um Atendente.";
$LZLANG["client_new_messages"] = "Novas Mensagens";
$LZLANG["client_message_too_long"] = "Esta mensagem excedeu o limite de caracteres. Por favor, reduza a mensagem antes do envio.";
$LZLANG["client_message_received"] = "Obrigado! Logo que possível um de nossos atendentes entrará em contato.";
$LZLANG["client_message_flood"] = "Você enviou muitas mensagens. Por favor, aguarde alguns instantes e tente novamente.";
$LZLANG["client_really_close"] = "O atendimento será fechado! Deseja continuar?";
$LZLANG["client_file_request_rejected"] = "O atendente não aceitou o seu pedido!";
$LZLANG["client_file_upload_oversized"] = "Seu arquivo não foi aceito pelo nosso servidor - o tamanho do arquivo excede o limite de envio ou a extensão não é permitida!";
$LZLANG["client_file_upload_provided"] = "Seu arquivo foi enviado ao atendente!";
$LZLANG["client_file_upload_requesting"] = "Solicitando autorização para o envio do arquivo...";
$LZLANG["client_file_upload_send_file"] = "Clique em ENVIAR para iniciar a transmissão";
$LZLANG["client_file_upload_select_file"] = "Por favor, selecione o arquivo para envio.";
$LZLANG["client_wait_for_representative"] = "Por favor, aguarde por um atendente.";
$LZLANG["client_no_representative"] = "O atendente foi desconectado.";
$LZLANG["client_transmitting_file"] = "Enviando seu arquivo...";
$LZLANG["client_please_rate"] = "Por favor, faça a avaliação primeiro.";
$LZLANG["client_rate_reason"] = "Por favor, informe por que avaliou nosso atendimento dessa forma";
$LZLANG["client_rate_qualification"] = "Eficácia";
$LZLANG["client_rate_politeness"] = "Educação";
$LZLANG["client_rate_success"] = "Obrigado! Sua avaliação é muito importante e ajudará a melhorar nosso serviço. Agradecemos imensamente o seu feedback.";
$LZLANG["client_rate_max"] = "Você não tem permissão para classificar o nosso serviço por mais de 3 vezes por dia.";
$LZLANG["client_file"] = "Arquivo";
$LZLANG["client_welcome"] = "Bem-vindo(a)";
$LZLANG["client_send"] = "Enviar";
$LZLANG["client_abort"] = "Cancelar";
$LZLANG["index_close"] = "Fechar";
$LZLANG["index_not_available"] = "Não disponível";
$LZLANG["index_description_demochat"] = "Inicie um atendimento de demonstração e converse com usuários internos que estão logados. Isso pode ser usado para todos os tipos de testes no LiveZilla.";
?>
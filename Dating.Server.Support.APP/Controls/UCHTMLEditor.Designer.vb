﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCHTMLEditor
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UCHTMLEditor))
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.TABPreview = New DevExpress.XtraTab.XtraTabPage()
        Me.WebBrowserBODY = New System.Windows.Forms.WebBrowser()
        Me.TABDesign = New DevExpress.XtraTab.XtraTabPage()
        Me.HtmlEditor1 = New SpiceLogic.WinHTMLEditor.HTMLEditor()
        Me.TABHTML = New DevExpress.XtraTab.XtraTabPage()
        Me.txBodyHTML = New DevExpress.XtraEditors.MemoEdit()
        Me.TABExternalEditor = New DevExpress.XtraTab.XtraTabPage()
        Me.txEditorEXE = New DevExpress.XtraEditors.ButtonEdit()
        Me.CmdSave = New System.Windows.Forms.Button()
        Me.lbEditorEXE = New DevExpress.XtraEditors.LabelControl()
        Me.CmdEditHTML = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdImportHTML = New DevExpress.XtraEditors.SimpleButton()
        Me.lbExportedFileName = New DevExpress.XtraEditors.LabelControl()
        Me.CmdExportHTML = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.TABPreview.SuspendLayout()
        Me.TABDesign.SuspendLayout()
        CType(Me.HtmlEditor1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.HtmlEditor1.SuspendLayout()
        Me.TABHTML.SuspendLayout()
        CType(Me.txBodyHTML.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TABExternalEditor.SuspendLayout()
        CType(Me.txEditorEXE.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.AppearancePage.Header.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.XtraTabControl1.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.TABPreview
        Me.XtraTabControl1.Size = New System.Drawing.Size(672, 319)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.TABPreview, Me.TABDesign, Me.TABHTML, Me.TABExternalEditor})
        '
        'TABPreview
        '
        Me.TABPreview.Controls.Add(Me.WebBrowserBODY)
        Me.TABPreview.Name = "TABPreview"
        Me.TABPreview.Size = New System.Drawing.Size(667, 293)
        Me.TABPreview.Text = "Preview"
        '
        'WebBrowserBODY
        '
        Me.WebBrowserBODY.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebBrowserBODY.Location = New System.Drawing.Point(0, 0)
        Me.WebBrowserBODY.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowserBODY.Name = "WebBrowserBODY"
        Me.WebBrowserBODY.Size = New System.Drawing.Size(667, 293)
        Me.WebBrowserBODY.TabIndex = 1
        '
        'TABDesign
        '
        Me.TABDesign.Controls.Add(Me.HtmlEditor1)
        Me.TABDesign.Name = "TABDesign"
        Me.TABDesign.Size = New System.Drawing.Size(667, 293)
        Me.TABDesign.Text = "Design"
        '
        'HtmlEditor1
        '
        Me.HtmlEditor1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.HtmlEditor1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.HtmlEditor1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.HtmlEditor1.BackgroundImagePath = ""
        Me.HtmlEditor1.BaseUrl = ""
        Me.HtmlEditor1.BodyColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.HtmlEditor1.BodyHtml = Nothing
        Me.HtmlEditor1.BodyStyle = Nothing
        Me.HtmlEditor1.Charset = "unicode"
        Me.HtmlEditor1.Cursor = System.Windows.Forms.Cursors.Default
        Me.HtmlEditor1.DefaultForeColor = System.Drawing.Color.Black
        Me.HtmlEditor1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HtmlEditor1.DocumentHtml = resources.GetString("HtmlEditor1.DocumentHtml")
        Me.HtmlEditor1.DocumentTitle = ""
        Me.HtmlEditor1.EditorContextMenuStrip = Nothing
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_New", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "New", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "New", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Open", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Open", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems1"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Open", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Save", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Save", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems2"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Save", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(26, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator1", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Font Name", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Font Name", Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Verdana", 8.0!), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(160, 24)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Font Size", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Font Size", Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator2", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Cut", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Cut", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems3"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Cut", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(26, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Copy", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Copy", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems4"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Copy", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(27, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Paste", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Paste", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems5"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Paste", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(27, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator3", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Bold", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Bold", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems6"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Bold", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Italic", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Italic", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems7"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Italic", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Underline", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Underline", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems8"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Underline", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Horizontal Rule", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Horizontal Rule", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems9"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Horizontal Rule", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Format Reset", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Format Reset", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems10"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Format Reset", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(34, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Undo", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Undo", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems11"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Undo", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Redo", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Redo", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems12"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Redo", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Edit Source", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Edit Source", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems13"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Edit Source", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(34, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Spell Check", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Spell Check", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems14"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Spell Check", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Title Insert", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Title Insert", Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(105, 29)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Text Highlight Color", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Text Highlight Color", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems15"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Text Highlight Color", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Font Color", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Font Color", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems16"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Font Color", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator4", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_HyperLink", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "HyperLink", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems17"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "HyperLink", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Image", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Image", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems18"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Image", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator5", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Insert Ordered List", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Insert Ordered List", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems19"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Insert Ordered List", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Insert Unordered List", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Insert Unordered List", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems20"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Insert Unordered List", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator6", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Left", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Left", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems21"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Left", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(26, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Center", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Center", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems22"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Center", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(26, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Right", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Right", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems23"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Right", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(26, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator7", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Table", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Table", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems24"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Table", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator8", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Outdent", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Outdent", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems25"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Outdent", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Indent", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Indent", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems26"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Indent", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator9", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Strike through", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Strike through", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems27"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Strike through", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_SuperScript", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "SuperScript", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems28"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "SuperScript", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(27, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Subscript", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Subscript", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems29"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Subscript", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(27, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Body Style", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Body Style", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems30"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Body Style", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(34, 26)))
        Me.HtmlEditor1.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Print", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Print", CType(resources.GetObject("HtmlEditor1.FactoryToolbarItems31"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Print", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.HtmlEditor1.LicenseKey = "12FB-8710-6C11-09BE-930A-6463-3358-8A93"
        Me.HtmlEditor1.Location = New System.Drawing.Point(0, 0)
        Me.HtmlEditor1.Name = "HtmlEditor1"
        Me.HtmlEditor1.ScrollBars = SpiceLogic.WinHTMLEditor.ScrollBarVisibility.[Default]
        Me.HtmlEditor1.Size = New System.Drawing.Size(667, 293)
        '
        '
        '
        Me.HtmlEditor1.SpellCheckDictionary.DictionaryFile = "el-GR.dic"
        '
        '
        '
        Me.HtmlEditor1.SpellChecker.Dictionary = Me.HtmlEditor1.SpellCheckDictionary
        Me.HtmlEditor1.TabIndex = 0
        '
        'HtmlEditor1.HtmlEditorToolbar1
        '
        Me.HtmlEditor1.Toolbar1.Location = New System.Drawing.Point(0, 0)
        Me.HtmlEditor1.Toolbar1.Name = "HtmlEditorToolbar1"
        Me.HtmlEditor1.Toolbar1.Size = New System.Drawing.Size(667, 27)
        Me.HtmlEditor1.Toolbar1.TabIndex = 0
        Me.HtmlEditor1.Toolbar1.Text = "toolStrip1"
        '
        'HtmlEditor1.HtmlEditorToolbar2
        '
        Me.HtmlEditor1.Toolbar2.Location = New System.Drawing.Point(0, 27)
        Me.HtmlEditor1.Toolbar2.Name = "HtmlEditorToolbar2"
        Me.HtmlEditor1.Toolbar2.Size = New System.Drawing.Size(667, 29)
        Me.HtmlEditor1.Toolbar2.TabIndex = 1
        Me.HtmlEditor1.Toolbar2.Text = "toolStrip2"
        Me.HtmlEditor1.ToolbarContextMenuStrip = Nothing
        Me.HtmlEditor1.ToolbarCursor = System.Windows.Forms.Cursors.Default
        '
        'TABHTML
        '
        Me.TABHTML.Controls.Add(Me.txBodyHTML)
        Me.TABHTML.Name = "TABHTML"
        Me.TABHTML.Size = New System.Drawing.Size(667, 293)
        Me.TABHTML.Text = "HTML"
        '
        'txBodyHTML
        '
        Me.txBodyHTML.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txBodyHTML.Location = New System.Drawing.Point(0, 0)
        Me.txBodyHTML.Name = "txBodyHTML"
        Me.txBodyHTML.Size = New System.Drawing.Size(667, 293)
        Me.txBodyHTML.TabIndex = 1
        '
        'TABExternalEditor
        '
        Me.TABExternalEditor.Controls.Add(Me.txEditorEXE)
        Me.TABExternalEditor.Controls.Add(Me.CmdSave)
        Me.TABExternalEditor.Controls.Add(Me.lbEditorEXE)
        Me.TABExternalEditor.Controls.Add(Me.CmdEditHTML)
        Me.TABExternalEditor.Controls.Add(Me.CmdImportHTML)
        Me.TABExternalEditor.Controls.Add(Me.lbExportedFileName)
        Me.TABExternalEditor.Controls.Add(Me.CmdExportHTML)
        Me.TABExternalEditor.Name = "TABExternalEditor"
        Me.TABExternalEditor.Size = New System.Drawing.Size(667, 293)
        Me.TABExternalEditor.Text = "External"
        '
        'txEditorEXE
        '
        Me.txEditorEXE.Location = New System.Drawing.Point(18, 159)
        Me.txEditorEXE.Name = "txEditorEXE"
        Me.txEditorEXE.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.txEditorEXE.Size = New System.Drawing.Size(628, 20)
        Me.txEditorEXE.TabIndex = 7
        '
        'CmdSave
        '
        Me.CmdSave.Location = New System.Drawing.Point(571, 185)
        Me.CmdSave.Name = "CmdSave"
        Me.CmdSave.Size = New System.Drawing.Size(75, 23)
        Me.CmdSave.TabIndex = 6
        Me.CmdSave.Text = "Save"
        Me.CmdSave.UseVisualStyleBackColor = True
        '
        'lbEditorEXE
        '
        Me.lbEditorEXE.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbEditorEXE.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lbEditorEXE.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lbEditorEXE.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbEditorEXE.Location = New System.Drawing.Point(18, 227)
        Me.lbEditorEXE.Name = "lbEditorEXE"
        Me.lbEditorEXE.Size = New System.Drawing.Size(628, 22)
        Me.lbEditorEXE.TabIndex = 5
        Me.lbEditorEXE.Text = "lbEditorEXE"
        '
        'CmdEditHTML
        '
        Me.CmdEditHTML.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdEditHTML.Location = New System.Drawing.Point(18, 55)
        Me.CmdEditHTML.Name = "CmdEditHTML"
        Me.CmdEditHTML.Size = New System.Drawing.Size(628, 30)
        Me.CmdEditHTML.TabIndex = 4
        Me.CmdEditHTML.Text = "Edit Existing HTML File"
        '
        'CmdImportHTML
        '
        Me.CmdImportHTML.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdImportHTML.Location = New System.Drawing.Point(18, 91)
        Me.CmdImportHTML.Name = "CmdImportHTML"
        Me.CmdImportHTML.Size = New System.Drawing.Size(628, 30)
        Me.CmdImportHTML.TabIndex = 3
        Me.CmdImportHTML.Text = "Read && Update HTML Code From External File"
        '
        'lbExportedFileName
        '
        Me.lbExportedFileName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbExportedFileName.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lbExportedFileName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lbExportedFileName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbExportedFileName.Location = New System.Drawing.Point(18, 255)
        Me.lbExportedFileName.Name = "lbExportedFileName"
        Me.lbExportedFileName.Size = New System.Drawing.Size(628, 22)
        Me.lbExportedFileName.TabIndex = 2
        Me.lbExportedFileName.Text = "lbExportedFileName"
        '
        'CmdExportHTML
        '
        Me.CmdExportHTML.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdExportHTML.Location = New System.Drawing.Point(18, 19)
        Me.CmdExportHTML.Name = "CmdExportHTML"
        Me.CmdExportHTML.Size = New System.Drawing.Size(628, 30)
        Me.CmdExportHTML.TabIndex = 0
        Me.CmdExportHTML.Text = "Export as HTML and Edit With External Editor"
        '
        'UCHTMLEditor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.XtraTabControl1)
        Me.Name = "UCHTMLEditor"
        Me.Size = New System.Drawing.Size(672, 319)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.TABPreview.ResumeLayout(False)
        Me.TABDesign.ResumeLayout(False)
        CType(Me.HtmlEditor1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.HtmlEditor1.ResumeLayout(False)
        Me.HtmlEditor1.PerformLayout()
        Me.TABHTML.ResumeLayout(False)
        CType(Me.txBodyHTML.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TABExternalEditor.ResumeLayout(False)
        CType(Me.txEditorEXE.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents TABPreview As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents TABDesign As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents TABHTML As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents HtmlEditor1 As SpiceLogic.WinHTMLEditor.HTMLEditor
    Friend WithEvents WebBrowserBODY As System.Windows.Forms.WebBrowser
    Friend WithEvents txBodyHTML As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TABExternalEditor As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents CmdExportHTML As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lbExportedFileName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CmdImportHTML As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdEditHTML As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents lbEditorEXE As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txEditorEXE As DevExpress.XtraEditors.ButtonEdit
    Friend WithEvents CmdSave As System.Windows.Forms.Button

End Class

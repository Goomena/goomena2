﻿Public Class UCHTMLEditor

    Public HTMLBody As String
    Public PageID As Integer = 0
    Public MessageID As Integer = 0
    Public FileName As String = ""


    Private Sub XtraTabControl1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles XtraTabControl1.Click

    End Sub

    Public Sub RefreshEditorToDB()
        Try
            Select Case XtraTabControl1.SelectedTabPage.Name
                Case "TABDesign"
                    My.Application.DoEvents()
                    Dim o As String = HtmlEditor1.BodyHtml
                    HTMLBody = HtmlEditor1.BodyHtml
                Case "TABHTML"
                    HTMLBody = txBodyHTML.EditValue
                Case ""
            End Select

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Public Sub RefreshEditor()
        Try

            WebBrowserBODY.DocumentText = Library.Public.Common.CheckNULL(HTMLBody, "")
            HtmlEditor1.BodyHtml = Library.Public.Common.CheckNULL(HTMLBody, "")
            txBodyHTML.EditValue = Library.Public.Common.CheckNULL(HTMLBody, "")

            Dim gWorkingSiteFolder = My.Application.Info.DirectoryPath & "\" & gWorkingSiteName
            If Not My.Computer.FileSystem.DirectoryExists(gWorkingSiteFolder) Then
                My.Computer.FileSystem.CreateDirectory(gWorkingSiteFolder)
            End If

            Dim gWorkingSiteFolderImages = My.Application.Info.DirectoryPath & "\" & gWorkingSiteName & "\Images"
            If Not My.Computer.FileSystem.DirectoryExists(gWorkingSiteFolderImages) Then
                My.Computer.FileSystem.CreateDirectory(gWorkingSiteFolderImages)
            End If


            lbExportedFileName.Text = gWorkingSiteFolder & "\" & FileName & "." & PageID & "." & MessageID & ".HTML"
            lbEditorEXE.Text = My.Settings.ExteralHTMLEditor & " Edit from .Config file"
            txEditorEXE.EditValue = My.Settings.ExteralHTMLEditor

            My.Application.DoEvents()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub XtraTabControlBODY_SelectedPageChanged(ByVal sender As System.Object, ByVal e As DevExpress.XtraTab.TabPageChangedEventArgs) Handles XtraTabControl1.SelectedPageChanged
        Try
            If e.PrevPage.Name = "TABHTML" And e.Page.Name = "TABDesign" Then
                HtmlEditor1.BodyHtml = txBodyHTML.EditValue
                HTMLBody = txBodyHTML.EditValue
            End If

            If e.PrevPage.Name = "TABDesign" And e.Page.Name = "TABHTML" Then
                Dim szBodyHTML As String = HtmlEditor1.BodyHtml
                txBodyHTML.EditValue = szBodyHTML
                HTMLBody = HtmlEditor1.BodyHtml
            End If

            If e.PrevPage.Name = "TABDesign" And e.Page.Name = "TABExternalEditor" Then
                Dim szBodyHTML As String = HtmlEditor1.BodyHtml
                txBodyHTML.EditValue = szBodyHTML
                HTMLBody = HtmlEditor1.BodyHtml
            End If


            WebBrowserBODY.DocumentText = Library.Public.Common.CheckNULL(HTMLBody, "")
            My.Application.DoEvents()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdExportHTML_Click(sender As System.Object, e As System.EventArgs) Handles CmdExportHTML.Click
        Try
            RefreshEditorToDB()
            My.Computer.FileSystem.WriteAllText(lbExportedFileName.Text, HTMLBody, False)
            Dim ExteralHTMLEditor As String = My.Settings.ExteralHTMLEditor
            System.Diagnostics.Process.Start(ExteralHTMLEditor, lbExportedFileName.Text)
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdEditHTML_Click(sender As System.Object, e As System.EventArgs) Handles CmdEditHTML.Click
        Try
            Dim ExteralHTMLEditor As String = My.Settings.ExteralHTMLEditor
            System.Diagnostics.Process.Start(ExteralHTMLEditor, "")
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdImportHTML_Click(sender As System.Object, e As System.EventArgs) Handles CmdImportHTML.Click
        Try
            If My.Computer.FileSystem.FileExists(lbExportedFileName.Text) Then
                HTMLBody = My.Computer.FileSystem.ReadAllText(lbExportedFileName.Text)
                txBodyHTML.EditValue = HTMLBody
                HtmlEditor1.BodyHtml = txBodyHTML.EditValue

                MsgBox("Reading HTML File: " & lbExportedFileName.Text)
            End If

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub TABExternalEditor_Paint(sender As System.Object, e As System.Windows.Forms.PaintEventArgs) Handles TABExternalEditor.Paint

    End Sub

    Private Sub CmdSave_Click(sender As System.Object, e As System.EventArgs) Handles CmdSave.Click
        Try
            My.Settings.ExteralHTMLEditor = txEditorEXE.EditValue
            My.Settings.Save()
            My.Application.DoEvents()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub
End Class

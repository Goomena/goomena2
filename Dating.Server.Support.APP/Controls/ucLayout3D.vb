﻿Imports Dating.Client.Admin.APP.AdminWS
Imports System.IO
Imports System.Linq
Imports Dating.Server.Core.DLL

Public Class ucLayout3D

    Const DeletePass As String = "000"

    Public Property DefaultView As PhotosFilterEnum
    Public Property ShowEditProfileButton As Boolean

    Public ProfileID As Integer
    Private _ProfileImagesList As New clsProfileImagesList()
    Private _PhotosFilter As PhotosFilterEnum
    Public DefaultPhotoIndex As Boolean

    Private Sub ucLayout3D_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        Try
            _ProfileImagesList.Clear()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub ucLayout3D_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub


    Public Sub Init()
        Try
            BarEditItemDateFrom.EditValue = DateTime.Now.AddMonths(-1)
            BarEditItemDateTo.EditValue = DateTime.Now.AddDays(1)

            BarButtonItemApprovePhoto.Enabled = False
            BarButtonItemDeclinePhoto.Enabled = False


            If (ShowEditProfileButton) Then
                BarButtonItemEditProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                BarButtonItemEditProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If


            If (DefaultView = PhotosFilterEnum.AllPhotos) Then
                'icbPhotosFilter.SelectedIndex = 0
                BarButtonItemAllPhotos_ItemClick(BarButtonItemAllPhotos, New DevExpress.XtraBars.ItemClickEventArgs(BarButtonItemAllPhotos, Nothing))
            ElseIf (DefaultView = PhotosFilterEnum.ApprovedPhotos) Then
                'icbPhotosFilter.SelectedIndex = 2
                'cmdApprovedPhotos_Click(cmdApprovedPhotos, New System.EventArgs())
                BarButtonItemApproved_ItemClick(BarButtonItemApproved, New DevExpress.XtraBars.ItemClickEventArgs(BarButtonItemApproved, Nothing))
            ElseIf (DefaultView = PhotosFilterEnum.RejectedPhotos) Then
                'icbPhotosFilter.SelectedIndex = 3
                'cmdRejectedPhotos_Click(cmdRejectedPhotos, New System.EventArgs())
                BarButtonItemRejected_ItemClick(BarButtonItemRejected, New DevExpress.XtraBars.ItemClickEventArgs(BarButtonItemRejected, Nothing))
            ElseIf (DefaultView = PhotosFilterEnum.DeletedPhotos) Then
                'icbPhotosFilter.SelectedIndex = 4
                'cmdDeletedPhotos_Click(cmdDeletedPhotos, New System.EventArgs())
                BarButtonItemDeletedPhotos_ItemClick(BarButtonItemDeletedPhotos, New DevExpress.XtraBars.ItemClickEventArgs(BarButtonItemDeletedPhotos, Nothing))
            ElseIf (DefaultView = PhotosFilterEnum.AutoApprovedPhotos) Then
                'icbPhotosFilter.SelectedIndex = 5
                'cmdDeletedPhotos_Click(cmdDeletedPhotos, New System.EventArgs())
                BarButtonItemAutoApproved_ItemClick(BarButtonItemAutoApproved, New DevExpress.XtraBars.ItemClickEventArgs(BarButtonItemAutoApproved, Nothing))
            Else
                'icbPhotosFilter.SelectedIndex = 1
                'cmdNewPhotos_Click(cmdNewPhotos, New System.EventArgs())
                BarButtonItemNewPhotos_ItemClick(BarButtonItemNewPhotos, New DevExpress.XtraBars.ItemClickEventArgs(BarButtonItemNewPhotos, Nothing))
            End If

            Me.layoutViewPhotos.OptionsBehavior.Editable = False

            Me.layoutViewPhotos.OptionsCarouselMode.CardCount = 5
            Me.layoutViewPhotos.OptionsCarouselMode.PitchAngle = 1.500983 'CSng(105 / 360.0F * 2 * Math.PI)
            Me.layoutViewPhotos.OptionsCarouselMode.RollAngle = 0.3141593 'CSng(182 / 360.0F * 2 * Math.PI)
            Me.layoutViewPhotos.OptionsCarouselMode.BottomCardScale = 0.3 '0 / 100.0F
            Me.layoutViewPhotos.OptionsCarouselMode.BottomCardAlphaLevel = 49 / 100.0F
            Me.layoutViewPhotos.OptionsCarouselMode.BottomCardFading = 12 / 100.0F
            Me.layoutViewPhotos.OptionsCarouselMode.InterpolationMode = Drawing2D.InterpolationMode.Default

            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetEUS_LISTS_PhotosDisplayLevel")
            CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_PhotosDisplayLevel(gAdminWSHeaders, Me.DSLists))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetEUS_LISTS_PhotosDisplayLevel")

            ClsCombos.FillComboUsingDatatable(Me.DSLists.EUS_LISTS_PhotosDisplayLevel, "US", "PhotosDisplayLevelId", repositoryItemICBDisplayLevel, True)

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub _LoadPhotosLayout()
        ShowWaitWin("Retreiving photos...")

        Try

            Dim photosRetrieve As New PhotosRetrieveParams()
            photosRetrieve.UseParams = True
            photosRetrieve.MasterProfileId = Me.ProfileID
            photosRetrieve.PhotosFilter = Me._PhotosFilter
            photosRetrieve.DateFrom = Me.BarEditItemDateFrom.EditValue
            photosRetrieve.DateTo = Me.BarEditItemDateTo.EditValue


            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetEUS_CustomerPhotosWithParams")
            CheckWebServiceCallResult(gAdminWS.GetEUS_CustomerPhotosWithParams(gAdminWSHeaders, Me.DSMembers, photosRetrieve))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetEUS_CustomerPhotosWithParams")

            Me.EUS_CustomerPhotosBindingSource.DataSource = Me.DSMembers


            Dim dt As DataTable = Me.DSMembers.EUS_CustomerPhotos.Copy()
            dt.Columns.Add("Caption")
            dt.Columns.Add(New DataColumn("Photo", GetType(System.Drawing.Image)))
            dt.Columns.Add("PhotoURL")
            dt.Columns.Add("Dimentions")
            dt.Columns.Add("Size")
            dt.Columns.Add("Type")


            gcPhotos.DataSource = dt

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        Finally
            HideWaitWin()
        End Try
    End Sub


    'Private Sub icbPhotosFilter_SelectedIndexChanged(sender As System.Object, e As System.EventArgs)
    '    Select Case icbPhotosFilter.SelectedIndex
    '        Case 0 : Me._PhotosFilter = PhotosFilterEnum.AllPhotos
    '        Case 1 : Me._PhotosFilter = PhotosFilterEnum.NewPhotos
    '        Case 2 : Me._PhotosFilter = PhotosFilterEnum.ApprovedPhotos
    '        Case 3 : Me._PhotosFilter = PhotosFilterEnum.RejectedPhotos
    '        Case 4 : Me._PhotosFilter = PhotosFilterEnum.DeletedPhotos
    '        Case 5 : Me._PhotosFilter = PhotosFilterEnum.AutoApprovedPhotos
    '    End Select


    '    Me._LoadPhotosLayout()

    'End Sub

    Private Sub SetRowData(visibleRowIndex As Integer, processingRows As Integer)
        If (processingRows = 0) Then Exit Sub
        If (visibleRowIndex < 0 OrElse visibleRowIndex >= Me.DSMembers.EUS_CustomerPhotos.Rows.Count) Then Exit Sub

        Try
            'layoutViewPhotos.GetNextVisibleRow()

            Dim dbRow As Dating.Client.Admin.APP.AdminWS.DSMembers.EUS_CustomerPhotosRow = Me.DSMembers.EUS_CustomerPhotos.Rows(visibleRowIndex)
            Dim img As ProfileImage = _ProfileImagesList.GetItemByPhotoId(dbRow.CustomerPhotosID)
            If (img Is Nothing) Then
                ShowWaitWin("Loading images...")

                Try
                    img = New ProfileImage(dbRow)
                    _ProfileImagesList.Add(img)

                    Dim extraRow As System.Data.DataRowView = DirectCast(Me.layoutViewPhotos.GetRow(visibleRowIndex), System.Data.DataRowView)
                    extraRow("Caption") = img.FileName
                    extraRow("Photo") = img.Image
                    extraRow("PhotoURL") = img.FileURL
                    extraRow("Dimentions") = img.Width & " x " & img.Height & " (W,H)"
                    extraRow("Size") = img.FormattedFileSize
                    extraRow("Type") = img.FileType

                    Me.layoutViewPhotos.RefreshRow(visibleRowIndex)
                Catch ex As Exception
                    ErrorMsgBox(ex, "")
                End Try

                HideWaitWin()
            End If

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try

        Dim nextVisibleRow As Integer = layoutViewPhotos.GetNextVisibleRow(visibleRowIndex)
        If (nextVisibleRow > -1) Then
            SetRowData(nextVisibleRow, processingRows - 1)
        End If

    End Sub

    Private Sub layoutViewPhotos_FocusedRowChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles layoutViewPhotos.FocusedRowChanged
        If (e.FocusedRowHandle >= 0 AndAlso e.FocusedRowHandle < layoutViewPhotos.RecordCount) Then
            Dim dbRow As Dating.Client.Admin.APP.AdminWS.DSMembers.EUS_CustomerPhotosRow = Me.DSMembers.EUS_CustomerPhotos.Rows(e.FocusedRowHandle)
            BarButtonItemApprovePhoto.Enabled = (Not dbRow.HasAproved)
            BarButtonItemDeclinePhoto.Enabled = (Not dbRow.HasDeclined)
        End If
        'Dim sss As Integer = e.FocusedRowHandle - 2
        'If (sss < 0) Then sss = 0
        SetRowData(e.FocusedRowHandle, 10)
    End Sub


    Private Sub layoutViewPhotos_VisibleRecordIndexChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Layout.Events.LayoutViewVisibleRecordIndexChangedEventArgs) Handles layoutViewPhotos.VisibleRecordIndexChanged
        layoutViewPhotos.FocusedRowHandle = e.VisibleRecordIndex
    End Sub


    Private Sub BarButtonItemAllPhotos_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemAllPhotos.ItemClick

        BarButtonItemAllPhotos.Enabled = False
        BarButtonItemNewPhotos.Enabled = True
        BarButtonItemRejected.Enabled = True
        BarButtonItemApproved.Enabled = True
        BarButtonItemDeletedPhotos.Enabled = True
        BarButtonItemAutoApproved.Enabled = True
        Me._PhotosFilter = PhotosFilterEnum.AllPhotos
        Me._LoadPhotosLayout()

    End Sub

    Private Sub BarButtonItemRejected_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRejected.ItemClick

        BarButtonItemAllPhotos.Enabled = True
        BarButtonItemNewPhotos.Enabled = True
        BarButtonItemRejected.Enabled = False
        BarButtonItemApproved.Enabled = True
        BarButtonItemDeletedPhotos.Enabled = True
        BarButtonItemAutoApproved.Enabled = True
        Me._PhotosFilter = PhotosFilterEnum.RejectedPhotos
        Me._LoadPhotosLayout()

    End Sub

    Private Sub BarButtonItemNewPhotos_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemNewPhotos.ItemClick

        BarButtonItemAllPhotos.Enabled = True
        BarButtonItemNewPhotos.Enabled = False
        BarButtonItemRejected.Enabled = True
        BarButtonItemApproved.Enabled = True
        BarButtonItemDeletedPhotos.Enabled = True
        BarButtonItemAutoApproved.Enabled = True
        Me._PhotosFilter = PhotosFilterEnum.NewPhotos
        Me._LoadPhotosLayout()

    End Sub

    Private Sub BarButtonItemApproved_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemApproved.ItemClick

        BarButtonItemAllPhotos.Enabled = True
        BarButtonItemNewPhotos.Enabled = True
        BarButtonItemRejected.Enabled = True
        BarButtonItemApproved.Enabled = False
        BarButtonItemDeletedPhotos.Enabled = True
        BarButtonItemAutoApproved.Enabled = True
        Me._PhotosFilter = PhotosFilterEnum.ApprovedPhotos
        Me._LoadPhotosLayout()

    End Sub

    Private Sub BarButtonItemAutoApproved_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemAutoApproved.ItemClick

        BarButtonItemAllPhotos.Enabled = True
        BarButtonItemNewPhotos.Enabled = True
        BarButtonItemRejected.Enabled = True
        BarButtonItemApproved.Enabled = True
        BarButtonItemDeletedPhotos.Enabled = True
        BarButtonItemAutoApproved.Enabled = False
        Me._PhotosFilter = PhotosFilterEnum.AutoApprovedPhotos
        Me._LoadPhotosLayout()

    End Sub

    Private Sub BarButtonItemDeletedPhotos_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemDeletedPhotos.ItemClick

        BarButtonItemAllPhotos.Enabled = True
        BarButtonItemNewPhotos.Enabled = True
        BarButtonItemRejected.Enabled = True
        BarButtonItemApproved.Enabled = True
        BarButtonItemDeletedPhotos.Enabled = False
        BarButtonItemAutoApproved.Enabled = True
        Me._PhotosFilter = PhotosFilterEnum.DeletedPhotos
        Me._LoadPhotosLayout()

    End Sub

    Public Sub RefreshControl()
        Try

            Dim FocusedRowHandle As Integer = layoutViewPhotos.FocusedRowHandle
            Me._LoadPhotosLayout()
            Me.gcPhotos.Focus()

            If (FocusedRowHandle >= 0 AndAlso FocusedRowHandle < layoutViewPhotos.RecordCount) Then
                layoutViewPhotos.FocusedRowHandle = FocusedRowHandle

                Dim dbRow As Dating.Client.Admin.APP.AdminWS.DSMembers.EUS_CustomerPhotosRow = Me.DSMembers.EUS_CustomerPhotos.Rows(FocusedRowHandle)
                BarButtonItemApprovePhoto.Enabled = (Not dbRow.HasAproved)
                BarButtonItemDeclinePhoto.Enabled = (Not dbRow.HasDeclined)
            End If

            'fix to make refresh visible on screen
            _ProfileImagesList.Clear()
            _ProfileImagesList = New clsProfileImagesList()
            SetRowData(layoutViewPhotos.FocusedRowHandle, 10)

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try

    End Sub

    Private Sub BarButtonItemRefresh_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRefresh.ItemClick
        RefreshControl()
    End Sub


    Private Sub BarButtonItemApprovePhoto_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemApprovePhoto.ItemClick
        ShowWaitWin("Working please wait...")

        Try

            Dim dbRow As Dating.Client.Admin.APP.AdminWS.DSMembers.EUS_CustomerPhotosRow = Me.DSMembers.EUS_CustomerPhotos.Rows(layoutViewPhotos.FocusedRowHandle)
            dbRow.HasAproved = True
            dbRow.HasDeclined = False

            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateEUS_CustomerPhotos")
            CheckWebServiceCallResult(gAdminWS.UpdateEUS_CustomerPhotos(gAdminWSHeaders, Me.DSMembers))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateEUS_CustomerPhotos")


            Me.EUS_CustomerPhotosBindingSource.Current("HasAproved") = True
            Me.EUS_CustomerPhotosBindingSource.Current("HasDeclined") = False

            'If (DefaultPhotoIndex = -1) Then
            '    Me.EUS_CustomerPhotosBindingSource.Current("IsDefault") = True
            '    DefaultPhotoIndex()
            'End If

            'layoutViewPhotos.RefreshData()
            'layoutViewPhotos.Refresh()
            BarButtonItemApprovePhoto.Enabled = (Not dbRow.HasAproved)
            BarButtonItemDeclinePhoto.Enabled = (Not dbRow.HasDeclined)

            If (layoutViewPhotos.FocusedRowHandle < layoutViewPhotos.RecordCount - 1) Then
                layoutViewPhotos.FocusedRowHandle += 1
            End If

            If (Not DefaultPhotoIndex) Then
                System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " SetFirstDefaultEUS_CustomerPhotos")
                Dim webResult As AdminWS.clsDataRecordErrorsReturn = CheckWebServiceCallResult(gAdminWS.SetFirstDefaultEUS_CustomerPhotos(gAdminWSHeaders, dbRow.CustomerID))
                System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END SetFirstDefaultEUS_CustomerPhotos")

                If (webResult.Message = "SET") Then
                    Me._LoadPhotosLayout()
                    DefaultPhotoIndex = True
                End If
            End If

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try

        HideWaitWin()
    End Sub


    Private Sub BarButtonItemDeclinePhoto_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemDeclinePhoto.ItemClick
        ShowWaitWin("Working please wait...")

        Try

            Dim dbRow As Dating.Client.Admin.APP.AdminWS.DSMembers.EUS_CustomerPhotosRow = Me.DSMembers.EUS_CustomerPhotos.Rows(layoutViewPhotos.FocusedRowHandle)
            dbRow.HasAproved = False
            dbRow.HasDeclined = True
            dbRow.IsDefault = False

            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateEUS_CustomerPhotos")
            CheckWebServiceCallResult(gAdminWS.UpdateEUS_CustomerPhotos(gAdminWSHeaders, Me.DSMembers))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateEUS_CustomerPhotos")

            Me.EUS_CustomerPhotosBindingSource.Current("HasAproved") = False
            Me.EUS_CustomerPhotosBindingSource.Current("IsDefault") = False
            Me.EUS_CustomerPhotosBindingSource.Current("HasDeclined") = True

            layoutViewPhotos.RefreshData()
            layoutViewPhotos.Refresh()
            BarButtonItemApprovePhoto.Enabled = (Not dbRow.HasAproved)
            BarButtonItemDeclinePhoto.Enabled = (Not dbRow.HasDeclined)


            If (layoutViewPhotos.FocusedRowHandle < layoutViewPhotos.RecordCount - 1) Then
                layoutViewPhotos.FocusedRowHandle += 1
            End If

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try

        HideWaitWin()
    End Sub


    Private Sub BarButtonItemEditProfile_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemEditProfile.ItemClick
        Try
            'Dim FocusedRowHandle As Integer = layoutViewPhotos.FocusedRowHandle
            'G()
            'Dim customerId As Integer = layoutViewPhotos.GetFocusedRowCellValue(colCustomerID)
            'Dim customerId As Integer = Me.EUS_CustomerPhotosBindingSource.Current("CustomerId")
            Dim customerId As Integer = DirectCast(DirectCast(Me.layoutViewPhotos.GetFocusedRow(), System.Data.DataRowView).Row, Dating.Client.Admin.APP.AdminWS.DSMembers.EUS_CustomerPhotosRow).CustomerID
            Dim frm As New frmProfileCart
            frm.EDIT(customerId)
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarEditItemDateFrom_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles BarEditItemDateFrom.EditValueChanged
        Try
            _LoadPhotosLayout()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarEditItemDateTo_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles BarEditItemDateTo.EditValueChanged
        Try
            _LoadPhotosLayout()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub BarButtonItemDeletePhoto_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemDeletePhoto.ItemClick
        Try
            ShowWaitWin("Deleting photo...")

            Dim CustomerPhotosID As Integer = DirectCast(DirectCast(Me.layoutViewPhotos.GetFocusedRow(), System.Data.DataRowView).Row, AdminWS.DSMembers.EUS_CustomerPhotosRow).CustomerPhotosID
            Dim CustomerID As Integer = DirectCast(DirectCast(Me.layoutViewPhotos.GetFocusedRow(), System.Data.DataRowView).Row, AdminWS.DSMembers.EUS_CustomerPhotosRow).CustomerID
            If (MessageBox.Show("You are about to delete a photo with id " & CustomerPhotosID & vbCrLf & "Continue?", "Deleting photo " & CustomerPhotosID, MessageBoxButtons.YesNo)) Then

                Dim frm As New frmInputPasswordText()
                frm.Text = "Input password"
                frm.gcInputText.Text = "Input password to delete photo"
                frm.ShowDialog()
                If (frm.IsOK AndAlso frm.GetUserInput() = DeletePass) Then
                    System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " DeletePhoto")
                    Dim webResult As AdminWS.clsDataRecordErrorsReturn = CheckWebServiceCallResult(gAdminWS.DeletePhoto(gAdminWSHeaders, CustomerID, CustomerPhotosID))
                    System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END DeletePhoto")

                    If (Not webResult.HasErrors) Then
                        RefreshControl()
                    End If

                End If
            End If

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        Finally
            HideWaitWin()
        End Try
    End Sub


End Class





'Private Sub layoutViewPhotos_FocusedRowChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles layoutViewPhotos.FocusedRowChanged

'    Dim items As Integer = Me.DSMembers.EUS_CustomerPhotos.Rows.Count
'    Dim currentItem As Integer = e.FocusedRowHandle + 1

'    lblViewingPhoto.Text = String.Format("Viewing: {0} / {1}", currentItem, items)
'End Sub


'Dim rowIndex As Integer
'For Each dbRow As DSMembers.EUS_CustomerPhotosRow In Me.DSMembers.EUS_CustomerPhotos.Rows
'    Try
'        Dim img As ProfileImage = _ProfileImagesList.GetItemByRowIndex(rowIndex)
'        If (img Is Nothing) Then
'            img = New ProfileImage(dbRow, rowIndex)
'            _ProfileImagesList.Add(img)
'        End If

'        Dim extraRow As DSPhotoExtraData.PhotosRow = Me.DSPhotoExtraData.Photos.NewPhotosRow()

'        extraRow.Caption = img.FileName
'        extraRow.Photo = img.Image
'        extraRow.PhotoURL = img.FileURL
'        extraRow.Dimentions = img.Width & " x " & img.Height & " (W,H)"
'        extraRow.Size = img.FormattedFileSize
'        extraRow.Type = img.FileType

'        Me.DSPhotoExtraData.Photos.AddPhotosRow(extraRow)
'    Catch ex As Exception
'        ErrorMsgBox(ex, "")
'    End Try

'    rowIndex += 1
'Next


'Dim currentRowIndex As Integer
'Dim currentEvent As Integer
'Private Sub layoutViewPhotos_CustomUnboundColumnData(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs) Handles layoutViewPhotos.CustomUnboundColumnData

'    If (e.IsGetData) Then

'        If (currentRowIndex <> e.ListSourceRowIndex) Then
'            System.Diagnostics.Debug.Print(New String("-", 20))
'            currentRowIndex = e.ListSourceRowIndex
'        End If

'        currentEvent += 1
'        System.Diagnostics.Debug.Print(currentEvent & " Column.FieldName: " & e.Column.FieldName & " RowIndex: " & e.ListSourceRowIndex & " Value null: " & (e.Value Is Nothing))


'        e.Value = GetFieldValue(e.Column.FieldName, e.ListSourceRowIndex)
'    End If

'End Sub


'Private Function GetFieldValue(FieldName As String, rowIndex As Integer) As Object
'    Try

'        Dim img As ProfileImage = _ProfileImagesList.GetItemByRowIndex(rowIndex)
'        If (img Is Nothing) Then
'            Dim row As DSMembers.EUS_CustomerPhotosRow = Me.DSMembers.EUS_CustomerPhotos.Rows(rowIndex)
'            img = New ProfileImage(row, rowIndex)
'            _ProfileImagesList.Add(img)
'        End If

'        Select Case FieldName
'            Case "Caption"
'                Return img.FileName

'            Case "Photo"
'                Return img.Image

'            Case "PhotoURL"
'                Return img.FileURL

'            Case "Dimentions"
'                Return img.Width & " x " & img.Height & " (W,H)"

'            Case "Size"
'                Return img.FormattedFileSize

'            Case "Type"
'                Return img.FileType

'        End Select

'    Catch ex As Exception
'        ErrorMsgBox(ex, "")
'    End Try


'    Return Nothing
'End Function



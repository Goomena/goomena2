﻿Imports Dating.Client.Admin.APP.AdminWS

Public Class UCMessages

    Public Property ProfileID As Integer
    Public Property ShowSendingReceiving As Boolean = True

    Public Sub Init()
        Try

            If (ShowSendingReceiving) Then
                BarCheckItemSending.Checked = True
                BarCheckItemReceiving.Checked = False
                BarCheckItemSending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                BarCheckItemReceiving.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                BarCheckItemSending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                BarCheckItemReceiving.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If

            BarEditItemDateFrom.EditValue = DateTime.Now.AddMonths(-1)
            BarEditItemDateTo.EditValue = DateTime.Now.AddDays(1)

            Me.BarButtonItemEditSendingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            Me.BarButtonItemEditReceivingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Never

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub



    Public Sub RefreshGrid()
        Try
            ShowWaitWin("Working please wait...")
            Dim params As New MessagesRetrieveParams()

            If (Me.ProfileID = -9876) Then
                params.MasterProfileId = 0
            Else
                params.MasterProfileId = Me.ProfileID
            End If


            If (ShowSendingReceiving) Then
                params.isSent = BarCheckItemSending.Checked
                params.isReceived = BarCheckItemReceiving.Checked
            Else
                params.isSent = Nothing
                params.isReceived = Nothing
            End If

            params.DateFrom = Me.BarEditItemDateFrom.EditValue
            params.DateTo = Me.BarEditItemDateTo.EditValue


            Dim DSMembers As New AdminWS.DSMembers

            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetAdminMessages")
            CheckWebServiceCallResult(gAdminWS.GetAdminMessages(gAdminWSHeaders, DSMembers, params))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetAdminMessages")

            Me.GetAdminMessagesBindingSource.DataSource = DSMembers.GetAdminMessages


            If (ShowSendingReceiving) Then
                If (BarCheckItemSending.Checked) Then
                    colFromLoginName.Visible = False
                    colToLoginName.Visible = True
                    colToLoginName.VisibleIndex = 0
                ElseIf (BarCheckItemReceiving.Checked) Then
                    colFromLoginName.Visible = True
                    colToLoginName.Visible = False
                    colFromLoginName.VisibleIndex = 0
                End If
            Else
                colFromLoginName.Visible = True
                'colFromLoginName.VisibleIndex = 0
                colToLoginName.Visible = True
                'colToLoginName.VisibleIndex = 1
            End If



        Catch ex As Exception
            ErrorMsgBox(ex, "")
        Finally
            HideWaitWin()
        End Try
    End Sub


    Private Sub BarCheckItemSending_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarCheckItemSending.ItemClick
        If (BarCheckItemSending.Checked) Then
            BarCheckItemReceiving.Checked = False
            RefreshGrid()
        End If
    End Sub

    Private Sub BarCheckItemReceiving_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarCheckItemReceiving.ItemClick
        If (BarCheckItemReceiving.Checked) Then
            BarCheckItemSending.Checked = False
            RefreshGrid()
        End If
    End Sub

    Private Sub BarButtonItemEditSendingProfile_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemEditSendingProfile.ItemClick
        Try
            ' Dim fromProfileId As Integer = Me.gvMessages.GetFocusedRowCellValue(colFromProfileID)
            Dim fromProfileId As Integer = DirectCast(DirectCast(Me.gvMessages.GetFocusedRow(), System.Data.DataRowView).Row, Dating.Client.Admin.APP.AdminWS.DSMembers.GetAdminMessagesRow).FromProfileID
            Dim frm As New frmProfileCart
            frm.EDIT(fromProfileId)
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarButtonItemEditReceivingProfile_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemEditReceivingProfile.ItemClick
        Try
            'Dim toProfileId As Integer = Me.gvMessages.GetFocusedRowCellValue(colToProfileID)
            Dim toProfileId As Integer = DirectCast(DirectCast(Me.gvMessages.GetFocusedRow(), System.Data.DataRowView).Row, Dating.Client.Admin.APP.AdminWS.DSMembers.GetAdminMessagesRow).ToProfileID
            Dim frm As New frmProfileCart
            frm.EDIT(toProfileId)
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub gvMessages_FocusedRowChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gvMessages.FocusedRowChanged
        Try
            If (Me.gvMessages.GetFocusedRow() IsNot Nothing) Then
                'Dim fromProfileId As Integer = Me.gvMessages.GetFocusedRowCellValue(colFromProfileID)
                'Dim toProfileId As Integer = Me.gvMessages.GetFocusedRowCellValue(colToProfileID)
                Dim fromProfileId As Integer = DirectCast(DirectCast(Me.gvMessages.GetFocusedRow(), System.Data.DataRowView).Row, Dating.Client.Admin.APP.AdminWS.DSMembers.GetAdminMessagesRow).FromProfileID
                Dim toProfileId As Integer = DirectCast(DirectCast(Me.gvMessages.GetFocusedRow(), System.Data.DataRowView).Row, Dating.Client.Admin.APP.AdminWS.DSMembers.GetAdminMessagesRow).ToProfileID

                If (fromProfileId = Me.ProfileID) Then
                    Me.BarButtonItemEditSendingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                Else
                    Me.BarButtonItemEditSendingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                End If

                If (toProfileId = Me.ProfileID) Then
                    Me.BarButtonItemEditReceivingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                Else
                    Me.BarButtonItemEditReceivingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                End If
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarButtonItemRefresh_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRefresh.ItemClick
        RefreshGrid()
    End Sub
End Class

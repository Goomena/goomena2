﻿Imports Dating.Client.Admin.APP.AdminWS

Public Class UCLikes

    Public Property ProfileID As Integer
    Public Property ShowSendingReceiving As Boolean = True

    Public Sub Init()
        Try

            If (ShowSendingReceiving) Then
                BarCheckItemSending.Checked = True
                BarCheckItemReceiving.Checked = False
                BarCheckItemSending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                BarCheckItemReceiving.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
            Else
                BarCheckItemSending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                BarCheckItemReceiving.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            End If

            BarEditItemDateFrom.EditValue = DateTime.Now.AddMonths(-1)
            BarEditItemDateTo.EditValue = DateTime.Now.AddDays(1)

            Me.BarButtonItemEditSendingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            Me.BarButtonItemEditReceivingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Never

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub



    'Public Sub RefreshGrid()
    '    LoadControl()
    'End Sub



    Public Sub RefreshGrid()
        Try
            ShowWaitWin("Working please wait...")

            Dim params As New OffersRetrieveParams()

            If (ShowSendingReceiving) Then
                If (BarCheckItemSending.Checked) Then
                    params.FromProfileId = Me.ProfileID
                ElseIf (BarCheckItemReceiving.Checked) Then
                    params.ToProfileId = Me.ProfileID
                End If
            End If

            If (Me.ProfileID = -9876) Then
                params.FromProfileId = 0
                params.ToProfileId = 0
            End If

            params.DateFrom = Me.BarEditItemDateFrom.EditValue
            params.DateTo = Me.BarEditItemDateTo.EditValue


            Dim DSMembers As New AdminWS.DSMembers

            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetAdminWinks")
            CheckWebServiceCallResult(gAdminWS.GetAdminWinks(gAdminWSHeaders, DSMembers, params))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetAdminWinks")

            Me.GetAdminWinksBindingSource.DataSource = DSMembers.GetAdminWinks

            If (ShowSendingReceiving) Then
                If (BarCheckItemSending.Checked) Then
                    colFromLoginName.Visible = False
                    colToLoginName.Visible = True
                    colToLoginName.VisibleIndex = 0
                ElseIf (BarCheckItemReceiving.Checked) Then
                    colFromLoginName.Visible = True
                    colToLoginName.Visible = False
                    colFromLoginName.VisibleIndex = 0
                End If
            Else
                colFromLoginName.Visible = True
                'colFromLoginName.VisibleIndex = 0
                colToLoginName.Visible = True
                'colToLoginName.VisibleIndex = 1
            End If


        Catch ex As Exception
            ErrorMsgBox(ex, "")
        Finally
            HideWaitWin()
        End Try
    End Sub


    Private Sub BarCheckItemSending_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarCheckItemSending.ItemClick
        If (BarCheckItemSending.Checked) Then
            BarCheckItemReceiving.Checked = False
            RefreshGrid()
        End If
    End Sub

    Private Sub BarCheckItemReceiving_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarCheckItemReceiving.ItemClick
        If (BarCheckItemReceiving.Checked) Then
            BarCheckItemSending.Checked = False
            RefreshGrid()
        End If
    End Sub


    Private Sub BarButtonItemEditSendingProfile_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemEditSendingProfile.ItemClick
        Try
            'Dim fromProfileId As Integer = Me.gvLikes.GetFocusedRowCellValue(colFromProfileID)
            Dim fromProfileId As Integer = DirectCast(DirectCast(Me.gvLikes.GetFocusedRow(), System.Data.DataRowView).Row, Dating.Client.Admin.APP.AdminWS.DSMembers.GetAdminWinksRow).FromProfileID
            Dim frm As New frmProfileCart
            frm.EDIT(fromProfileId)
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarButtonItemEditReceivingProfile_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemEditReceivingProfile.ItemClick
        Try
            'Dim toProfileId As Integer = Me.gvLikes.GetFocusedRowCellValue(colToProfileID)
            Dim toProfileId As Integer = DirectCast(DirectCast(Me.gvLikes.GetFocusedRow(), System.Data.DataRowView).Row, Dating.Client.Admin.APP.AdminWS.DSMembers.GetAdminWinksRow).ToProfileID
            Dim frm As New frmProfileCart
            frm.EDIT(toProfileId)
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub gvLikes_FocusedRowChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gvLikes.FocusedRowChanged
        Try
            If (Me.gvLikes.GetFocusedRow() IsNot Nothing) Then
                'Dim fromProfileId As Integer = Me.gvLikes.GetFocusedRowCellValue(colFromProfileID)
                'Dim toProfileId As Integer = Me.gvLikes.GetFocusedRowCellValue(colToProfileID)
                Dim fromProfileId As Integer = DirectCast(DirectCast(Me.gvLikes.GetFocusedRow(), System.Data.DataRowView).Row, Dating.Client.Admin.APP.AdminWS.DSMembers.GetAdminWinksRow).FromProfileID
                Dim toProfileId As Integer = DirectCast(DirectCast(Me.gvLikes.GetFocusedRow(), System.Data.DataRowView).Row, Dating.Client.Admin.APP.AdminWS.DSMembers.GetAdminWinksRow).ToProfileID

                If (fromProfileId = Me.ProfileID) Then
                    Me.BarButtonItemEditSendingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                Else
                    Me.BarButtonItemEditSendingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                End If

                If (toProfileId = Me.ProfileID) Then
                    Me.BarButtonItemEditReceivingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
                Else
                    Me.BarButtonItemEditReceivingProfile.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
                End If
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub BarButtonItemRefresh_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRefresh.ItemClick
        RefreshGrid()
    End Sub
End Class

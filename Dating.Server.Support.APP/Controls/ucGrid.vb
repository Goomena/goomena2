﻿Public Class ucGrid

    Public Event ToolbarAddNewClicked(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
    Public Event ToolbarEditClicked(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
    Public Event ToolbarDeleteClicked(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
    Public Event ToolbarRefreshClicked(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)

    Public ReadOnly Property GridView As DevExpress.XtraGrid.Views.Grid.GridView
        Get
            Return Me.gv1
        End Get
    End Property


    'Public Property FocusedRowHandle As Integer
    '    Get
    '        Return Me.gv1.FocusedRowHandle
    '    End Get
    '    Set(value As Integer)
    '        Me.gv1.FocusedRowHandle = value
    '    End Set
    'End Property


    'Public ReadOnly Property AddNewButton As DevExpress.XtraBars.BarButtonItem
    '    Get
    '        Return Me.BarAddNew
    '    End Get
    'End Property


    'Public ReadOnly Property EditButton As DevExpress.XtraBars.BarButtonItem
    '    Get
    '        Return Me.BarEdit
    '    End Get
    'End Property


    'Public ReadOnly Property DeleteButton As DevExpress.XtraBars.BarButtonItem
    '    Get
    '        Return Me.BarDelete
    '    End Get
    'End Property


    'Public ReadOnly Property RefreshButton As DevExpress.XtraBars.BarButtonItem
    '    Get
    '        Return Me.BarRefresh
    '    End Get
    'End Property

    Public Property IsLoaded As Boolean

    Private Sub ucGrid_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Me.BarEdit.Enabled = False
        Me.BarDelete.Enabled = False

    End Sub



    Public Sub LoadControl(ByRef bindingSource As DataTable, Optional hiddenColumnsNames As List(Of String) = Nothing)
        Dim totalCols = bindingSource.Columns.Count - 1
        Dim columns As New List(Of DevExpress.XtraGrid.Columns.GridColumn)
        Dim cnt = 0
        If (hiddenColumnsNames Is Nothing) Then hiddenColumnsNames = New List(Of String)

        For Each dc As DataColumn In bindingSource.Columns

            If (Not hiddenColumnsNames.Contains(dc.ColumnName)) Then
                Dim col As New DevExpress.XtraGrid.Columns.GridColumn
                col.FieldName = dc.ColumnName
                col.Name = "col" & dc.ColumnName
                col.Visible = True
                col.VisibleIndex = cnt

                If (dc.DataType Is GetType(System.DateTime)) Then
                    col.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                    col.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"
                End If
                columns.Add(col)
                cnt += 1
            End If

        Next

        Me.gv1.Columns.Clear()
        Me.gv1.Columns.AddRange(columns.ToArray())
        Me.gvBindingSource.DataSource = bindingSource
        Me.IsLoaded = True
    End Sub


    Public Sub LoadControl(ByRef bindingSource As DataTable, ByRef columns As DevExpress.XtraGrid.Columns.GridColumn())
        Me.gv1.Columns.Clear()
        Me.gv1.Columns.AddRange(columns)
        Me.gvBindingSource.DataSource = bindingSource
    End Sub




    'Public Sub LoadControl(ByRef bindingSource As DataSet)
    '    LoadControl(bindingSource.Tables(0))
    'End Sub


    'Public Sub LoadControl(ByRef bindingSource As DataSet, ByRef columns As DevExpress.XtraGrid.Columns.GridColumn())
    '    Me.gv1.Columns.Clear()
    '    Me.gv1.Columns.AddRange(columns)
    '    Me.gvBindingSource.DataSource = bindingSource
    'End Sub


    Private Sub BarAddNew_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarAddNew.ItemClick
        RaiseEvent ToolbarAddNewClicked(sender, e)
    End Sub


    Private Sub BarEdit_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarEdit.ItemClick
        RaiseEvent ToolbarEditClicked(sender, e)
    End Sub


    Private Sub BarDelete_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarDelete.ItemClick
        RaiseEvent ToolbarDeleteClicked(sender, e)
    End Sub


    Private Sub BarRefresh_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarRefresh.ItemClick
        RaiseEvent ToolbarRefreshClicked(sender, e)
    End Sub

    Private Sub gv_DataSourceChanged(sender As System.Object, e As System.EventArgs) Handles gv.DataSourceChanged
        System.Diagnostics.Debug.Print("gv_DataSourceChanged")
    End Sub

    Private Sub gv1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gv1.RowUpdated
        System.Diagnostics.Debug.Print("gv1_RowUpdated")
    End Sub

    Private Sub gv1_FocusedRowChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gv1.FocusedRowChanged

        If (gv1.FocusedRowHandle > -1) Then
            Me.BarEdit.Enabled = True
            Me.BarDelete.Enabled = True
        End If

    End Sub
End Class

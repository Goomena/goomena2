﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucLayout3D
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucLayout3D))
        Dim SerializableAppearanceObject2 As DevExpress.Utils.SerializableAppearanceObject = New DevExpress.Utils.SerializableAppearanceObject()
        Me.repositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.gcPhotos = New DevExpress.XtraGrid.GridControl()
        Me.EUS_LISTS_PhotosDisplayLevelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DSLists = New Dating.Client.Admin.APP.AdminWS.DSLists()
        Me.layoutViewPhotos = New DevExpress.XtraGrid.Views.Layout.LayoutView()
        Me.colCaption = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.layoutViewField_colCaption = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colCustomerPhotosID = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.layoutViewField_colCustomerPhotosID = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colCustomerID = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.layoutViewField_colCustomerID = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colDateTimeToUploading = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.layoutViewField_colDateTimeToUploading = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colFileName = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.layoutViewField_colFileName = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colDisplayLevel = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.repositoryItemICBDisplayLevel = New DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox()
        Me.layoutViewField_colDisplayLevel = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colHasAproved = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.repositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.layoutViewField_colHasAproved = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colHasDeclined = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.repositoryItemCheckEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.layoutViewField_colHasDeclined = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colCheckedContextID = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.layoutViewField_colCheckedContextID = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colIsDefault = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.repositoryItemCheckEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.layoutViewField_colIsDefault = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colPhoto = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.repositoryItemPictureEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit()
        Me.layoutViewField_colPhoto = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colPhotoURL = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.repositoryItemTextEditPhotoURL = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.layoutViewField_colPhotoURL = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colDimentions = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.layoutViewField_colDimentions = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colSize = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.layoutViewField_colSize = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colType = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.layoutViewField_colType = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colCopyPhotoURL = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.repositoryItemButtonEditCopyPhotoURL = New DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit()
        Me.layoutViewField_colCopyPhotoURL = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colIsDeleted = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.RepositoryItemCheckEdit_IsDeleted = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.layoutViewField_colIsDeleted = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.colIsAutoApproved = New DevExpress.XtraGrid.Columns.LayoutViewColumn()
        Me.RepositoryItemCheckEdit_IsAutoApproved = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.layoutViewField_colIsAutoApproved = New DevExpress.XtraGrid.Views.Layout.LayoutViewField()
        Me.LayoutViewCard1 = New DevExpress.XtraGrid.Views.Layout.LayoutViewCard()
        Me.item4 = New DevExpress.XtraLayout.SimpleSeparator()
        Me.item3 = New DevExpress.XtraLayout.SimpleSeparator()
        Me.item1 = New DevExpress.XtraLayout.SimpleSeparator()
        Me.item6 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.item7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.item8 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.item2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.repositoryItemImageComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox()
        Me.EUS_CustomerPhotosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DSMembers = New Dating.Client.Admin.APP.AdminWS.DSMembers()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItemAllPhotos = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemNewPhotos = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemRejected = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemApproved = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemAutoApproved = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemDeletedPhotos = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemRefresh = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar3 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItemDeclinePhoto = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemApprovePhoto = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemEditProfile = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.BarEditItemDateFrom = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemDateEditFrom = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.BarEditItemDateTo = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemDateEditTo = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.RepositoryItemMonth1 = New DevExpress.XtraScheduler.UI.RepositoryItemMonth()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.BarButtonItemDeletePhoto = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.repositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcPhotos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_LISTS_PhotosDisplayLevelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewPhotos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colCaption, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colCustomerPhotosID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colCustomerID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colDateTimeToUploading, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colFileName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repositoryItemICBDisplayLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colDisplayLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colHasAproved, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colHasDeclined, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colCheckedContextID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repositoryItemCheckEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colIsDefault, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repositoryItemPictureEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repositoryItemTextEditPhotoURL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colPhotoURL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colDimentions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colSize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repositoryItemButtonEditCopyPhotoURL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colCopyPhotoURL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit_IsDeleted, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colIsDeleted, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit_IsAutoApproved, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutViewField_colIsAutoApproved, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutViewCard1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.item4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.item3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.item1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.item6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.item7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.item8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.item2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repositoryItemImageComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_CustomerPhotosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSMembers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEditFrom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEditFrom.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEditTo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEditTo.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMonth1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'repositoryItemTextEdit1
        '
        Me.repositoryItemTextEdit1.AutoHeight = False
        Me.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "About.ico")
        Me.ImageList1.Images.SetKeyName(1, "Accounts.ico")
        Me.ImageList1.Images.SetKeyName(2, "Address Book.ico")
        Me.ImageList1.Images.SetKeyName(3, "Air Brush.ico")
        Me.ImageList1.Images.SetKeyName(4, "Align-Centre-2.ico")
        Me.ImageList1.Images.SetKeyName(5, "Align-Centre.ico")
        Me.ImageList1.Images.SetKeyName(6, "Align-Full-2.ico")
        Me.ImageList1.Images.SetKeyName(7, "Align-Full.ico")
        Me.ImageList1.Images.SetKeyName(8, "Align-Left-2.ico")
        Me.ImageList1.Images.SetKeyName(9, "Align-Left.ico")
        Me.ImageList1.Images.SetKeyName(10, "Align-Right-2.ico")
        Me.ImageList1.Images.SetKeyName(11, "Align-Right.ico")
        Me.ImageList1.Images.SetKeyName(12, "Arrow-Left Right.ico")
        Me.ImageList1.Images.SetKeyName(13, "Arrow-Up Down.ico")
        Me.ImageList1.Images.SetKeyName(14, "Attachment.ico")
        Me.ImageList1.Images.SetKeyName(15, "Auto Correct-Options.ico")
        Me.ImageList1.Images.SetKeyName(16, "Auto-Type.ico")
        Me.ImageList1.Images.SetKeyName(17, "Back.ico")
        Me.ImageList1.Images.SetKeyName(18, "Bar Code.ico")
        Me.ImageList1.Images.SetKeyName(19, "Bold.ico")
        Me.ImageList1.Images.SetKeyName(20, "Book-Open.ico")
        Me.ImageList1.Images.SetKeyName(21, "Books.ico")
        Me.ImageList1.Images.SetKeyName(22, "Book-Search.ico")
        Me.ImageList1.Images.SetKeyName(23, "Borders and Shading.ico")
        Me.ImageList1.Images.SetKeyName(24, "Box-Closed-2.ico")
        Me.ImageList1.Images.SetKeyName(25, "Box-Closed.ico")
        Me.ImageList1.Images.SetKeyName(26, "Box-Open-2.ico")
        Me.ImageList1.Images.SetKeyName(27, "Box-Open.ico")
        Me.ImageList1.Images.SetKeyName(28, "Bullets-2.ico")
        Me.ImageList1.Images.SetKeyName(29, "Bullets.ico")
        Me.ImageList1.Images.SetKeyName(30, "Calculator.ico")
        Me.ImageList1.Images.SetKeyName(31, "Calendar.ico")
        Me.ImageList1.Images.SetKeyName(32, "Calendar-Go To Date.ico")
        Me.ImageList1.Images.SetKeyName(33, "Calendar-Search.ico")
        Me.ImageList1.Images.SetKeyName(34, "Calendar-Select Day.ico")
        Me.ImageList1.Images.SetKeyName(35, "Calendar-Select Month.ico")
        Me.ImageList1.Images.SetKeyName(36, "Calendar-Select Week.ico")
        Me.ImageList1.Images.SetKeyName(37, "Calendar-Select Work Week.ico")
        Me.ImageList1.Images.SetKeyName(38, "Camera.ico")
        Me.ImageList1.Images.SetKeyName(39, "Camera-Photo.ico")
        Me.ImageList1.Images.SetKeyName(40, "Card File.ico")
        Me.ImageList1.Images.SetKeyName(41, "Card.ico")
        Me.ImageList1.Images.SetKeyName(42, "Card-Add.ico")
        Me.ImageList1.Images.SetKeyName(43, "Card-Delete.ico")
        Me.ImageList1.Images.SetKeyName(44, "Card-Edit.ico")
        Me.ImageList1.Images.SetKeyName(45, "Card-New.ico")
        Me.ImageList1.Images.SetKeyName(46, "Card-Next.ico")
        Me.ImageList1.Images.SetKeyName(47, "Card-Previous.ico")
        Me.ImageList1.Images.SetKeyName(48, "Card-Remove.ico")
        Me.ImageList1.Images.SetKeyName(49, "Card-Search.ico")
        Me.ImageList1.Images.SetKeyName(50, "CD-Burn.ico")
        Me.ImageList1.Images.SetKeyName(51, "CD-In Case.ico")
        Me.ImageList1.Images.SetKeyName(52, "CD-ROM.ico")
        Me.ImageList1.Images.SetKeyName(53, "Center Across Cells.ico")
        Me.ImageList1.Images.SetKeyName(54, "Certificate.ico")
        Me.ImageList1.Images.SetKeyName(55, "Chart.ico")
        Me.ImageList1.Images.SetKeyName(56, "Chart-Bar.ico")
        Me.ImageList1.Images.SetKeyName(57, "Chart-Line.ico")
        Me.ImageList1.Images.SetKeyName(58, "Chart-Pie.ico")
        Me.ImageList1.Images.SetKeyName(59, "Chart-Point.ico")
        Me.ImageList1.Images.SetKeyName(60, "Clean.ico")
        Me.ImageList1.Images.SetKeyName(61, "Clear.ico")
        Me.ImageList1.Images.SetKeyName(62, "Clipboard.ico")
        Me.ImageList1.Images.SetKeyName(63, "Clock.ico")
        Me.ImageList1.Images.SetKeyName(64, "Close.ico")
        Me.ImageList1.Images.SetKeyName(65, "Colour Picker.ico")
        Me.ImageList1.Images.SetKeyName(66, "Columns-2.ico")
        Me.ImageList1.Images.SetKeyName(67, "Columns.ico")
        Me.ImageList1.Images.SetKeyName(68, "Command Prompt.ico")
        Me.ImageList1.Images.SetKeyName(69, "Compress.ico")
        Me.ImageList1.Images.SetKeyName(70, "Computer.ico")
        Me.ImageList1.Images.SetKeyName(71, "Connect.ico")
        Me.ImageList1.Images.SetKeyName(72, "Contact.ico")
        Me.ImageList1.Images.SetKeyName(73, "Contact-Add.ico")
        Me.ImageList1.Images.SetKeyName(74, "Contact-Delete.ico")
        Me.ImageList1.Images.SetKeyName(75, "Contact-Edit.ico")
        Me.ImageList1.Images.SetKeyName(76, "Contact-New.ico")
        Me.ImageList1.Images.SetKeyName(77, "Contact-Remove.ico")
        Me.ImageList1.Images.SetKeyName(78, "Contact-Search.ico")
        Me.ImageList1.Images.SetKeyName(79, "Copy.ico")
        Me.ImageList1.Images.SetKeyName(80, "Cross.ico")
        Me.ImageList1.Images.SetKeyName(81, "Currency.ico")
        Me.ImageList1.Images.SetKeyName(82, "Currency-Notes.ico")
        Me.ImageList1.Images.SetKeyName(83, "Cut.ico")
        Me.ImageList1.Images.SetKeyName(84, "Database.ico")
        Me.ImageList1.Images.SetKeyName(85, "Database-Add.ico")
        Me.ImageList1.Images.SetKeyName(86, "Database-Close.ico")
        Me.ImageList1.Images.SetKeyName(87, "Database-Delete.ico")
        Me.ImageList1.Images.SetKeyName(88, "Database-Edit.ico")
        Me.ImageList1.Images.SetKeyName(89, "Database-Filter.ico")
        Me.ImageList1.Images.SetKeyName(90, "Database-New.ico")
        Me.ImageList1.Images.SetKeyName(91, "Database-Open.ico")
        Me.ImageList1.Images.SetKeyName(92, "Database-Properties.ico")
        Me.ImageList1.Images.SetKeyName(93, "Database-Remove.ico")
        Me.ImageList1.Images.SetKeyName(94, "Database-Save.ico")
        Me.ImageList1.Images.SetKeyName(95, "Database-Schema.ico")
        Me.ImageList1.Images.SetKeyName(96, "Database-Search.ico")
        Me.ImageList1.Images.SetKeyName(97, "Database-Security.ico")
        Me.ImageList1.Images.SetKeyName(98, "Database-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(99, "Date-Time.ico")
        Me.ImageList1.Images.SetKeyName(100, "db-Add.ico")
        Me.ImageList1.Images.SetKeyName(101, "db-Cancel.ico")
        Me.ImageList1.Images.SetKeyName(102, "db-Delete.ico")
        Me.ImageList1.Images.SetKeyName(103, "db-Edit.ico")
        Me.ImageList1.Images.SetKeyName(104, "db-First.ico")
        Me.ImageList1.Images.SetKeyName(105, "db-Last.ico")
        Me.ImageList1.Images.SetKeyName(106, "db-Next.ico")
        Me.ImageList1.Images.SetKeyName(107, "db-Post.ico")
        Me.ImageList1.Images.SetKeyName(108, "db-Previous.ico")
        Me.ImageList1.Images.SetKeyName(109, "db-Refresh.ico")
        Me.ImageList1.Images.SetKeyName(110, "Decimals-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(111, "Decimals-Increase.ico")
        Me.ImageList1.Images.SetKeyName(112, "Decompress.ico")
        Me.ImageList1.Images.SetKeyName(113, "Delete.ico")
        Me.ImageList1.Images.SetKeyName(114, "Delete-Blue.ico")
        Me.ImageList1.Images.SetKeyName(115, "Design.ico")
        Me.ImageList1.Images.SetKeyName(116, "Desktop.ico")
        Me.ImageList1.Images.SetKeyName(117, "Dictionary.ico")
        Me.ImageList1.Images.SetKeyName(118, "Disconnect.ico")
        Me.ImageList1.Images.SetKeyName(119, "Discuss.ico")
        Me.ImageList1.Images.SetKeyName(120, "Doc-Access.ico")
        Me.ImageList1.Images.SetKeyName(121, "Doc-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(122, "Doc-Excel.ico")
        Me.ImageList1.Images.SetKeyName(123, "Doc-HTML.ico")
        Me.ImageList1.Images.SetKeyName(124, "Doc-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(125, "Doc-RTF.ico")
        Me.ImageList1.Images.SetKeyName(126, "Document-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(127, "Document-Protect.ico")
        Me.ImageList1.Images.SetKeyName(128, "Doc-Word.ico")
        Me.ImageList1.Images.SetKeyName(129, "Doc-XML.ico")
        Me.ImageList1.Images.SetKeyName(130, "Drawing.ico")
        Me.ImageList1.Images.SetKeyName(131, "Edit.ico")
        Me.ImageList1.Images.SetKeyName(132, "Equaliser.ico")
        Me.ImageList1.Images.SetKeyName(133, "Execute.ico")
        Me.ImageList1.Images.SetKeyName(134, "Exit.ico")
        Me.ImageList1.Images.SetKeyName(135, "Export.ico")
        Me.ImageList1.Images.SetKeyName(136, "Export-Access.ico")
        Me.ImageList1.Images.SetKeyName(137, "Export-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(138, "Export-CSV.ico")
        Me.ImageList1.Images.SetKeyName(139, "Export-Excel.ico")
        Me.ImageList1.Images.SetKeyName(140, "Export-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(141, "Export-HTML.ico")
        Me.ImageList1.Images.SetKeyName(142, "Export-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(143, "Export-RTF.ico")
        Me.ImageList1.Images.SetKeyName(144, "Export-Text.ico")
        Me.ImageList1.Images.SetKeyName(145, "Export-Word.ico")
        Me.ImageList1.Images.SetKeyName(146, "Export-XML.ico")
        Me.ImageList1.Images.SetKeyName(147, "Favorites.ico")
        Me.ImageList1.Images.SetKeyName(148, "Fill.ico")
        Me.ImageList1.Images.SetKeyName(149, "Fill-Down.ico")
        Me.ImageList1.Images.SetKeyName(150, "Fill-Left.ico")
        Me.ImageList1.Images.SetKeyName(151, "Fill-Right.ico")
        Me.ImageList1.Images.SetKeyName(152, "Fill-Up.ico")
        Me.ImageList1.Images.SetKeyName(153, "Filter.ico")
        Me.ImageList1.Images.SetKeyName(154, "Flag.ico")
        Me.ImageList1.Images.SetKeyName(155, "Flag-Delete.ico")
        Me.ImageList1.Images.SetKeyName(156, "Flag-Next.ico")
        Me.ImageList1.Images.SetKeyName(157, "Flag-Previous.ico")
        Me.ImageList1.Images.SetKeyName(158, "Flow Chart.ico")
        Me.ImageList1.Images.SetKeyName(159, "Folder List.ico")
        Me.ImageList1.Images.SetKeyName(160, "Folder.ico")
        Me.ImageList1.Images.SetKeyName(161, "Folder-Closed.ico")
        Me.ImageList1.Images.SetKeyName(162, "Folder-Delete.ico")
        Me.ImageList1.Images.SetKeyName(163, "Folder-New.ico")
        Me.ImageList1.Images.SetKeyName(164, "Folder-Options.ico")
        Me.ImageList1.Images.SetKeyName(165, "Folder-Search.ico")
        Me.ImageList1.Images.SetKeyName(166, "Footer.ico")
        Me.ImageList1.Images.SetKeyName(167, "Format Painter.ico")
        Me.ImageList1.Images.SetKeyName(168, "Format-Font Size.ico")
        Me.ImageList1.Images.SetKeyName(169, "Format-Font.ico")
        Me.ImageList1.Images.SetKeyName(170, "Format-Paragraph.ico")
        Me.ImageList1.Images.SetKeyName(171, "Format-Percentage.ico")
        Me.ImageList1.Images.SetKeyName(172, "Formatting-Remove.ico")
        Me.ImageList1.Images.SetKeyName(173, "Forward.ico")
        Me.ImageList1.Images.SetKeyName(174, "Full Screen.ico")
        Me.ImageList1.Images.SetKeyName(175, "Full Screen-New.ico")
        Me.ImageList1.Images.SetKeyName(176, "Function.ico")
        Me.ImageList1.Images.SetKeyName(177, "Go To Line.ico")
        Me.ImageList1.Images.SetKeyName(178, "Grid-2.ico")
        Me.ImageList1.Images.SetKeyName(179, "Grid.ico")
        Me.ImageList1.Images.SetKeyName(180, "Grid-Auto Format-2.ico")
        Me.ImageList1.Images.SetKeyName(181, "Grid-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(182, "Grid-Column Width.ico")
        Me.ImageList1.Images.SetKeyName(183, "Grid-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(184, "Grid-Delete Cell.ico")
        Me.ImageList1.Images.SetKeyName(185, "Grid-Delete Column.ico")
        Me.ImageList1.Images.SetKeyName(186, "Grid-Delete Row.ico")
        Me.ImageList1.Images.SetKeyName(187, "Grid-Delete.ico")
        Me.ImageList1.Images.SetKeyName(188, "Grid-Insert Cell.ico")
        Me.ImageList1.Images.SetKeyName(189, "Grid-Insert Column Left.ico")
        Me.ImageList1.Images.SetKeyName(190, "Grid-Insert Column Right.ico")
        Me.ImageList1.Images.SetKeyName(191, "Grid-Insert Column.ico")
        Me.ImageList1.Images.SetKeyName(192, "Grid-Insert Row Above.ico")
        Me.ImageList1.Images.SetKeyName(193, "Grid-Insert Row Below.ico")
        Me.ImageList1.Images.SetKeyName(194, "Grid-Insert Row.ico")
        Me.ImageList1.Images.SetKeyName(195, "Grid-Merge Cells.ico")
        Me.ImageList1.Images.SetKeyName(196, "Grid-New-2.ico")
        Me.ImageList1.Images.SetKeyName(197, "Grid-New.ico")
        Me.ImageList1.Images.SetKeyName(198, "Grid-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(199, "Grid-Options.ico")
        Me.ImageList1.Images.SetKeyName(200, "Grid-Row Height.ico")
        Me.ImageList1.Images.SetKeyName(201, "Grid-Select All-2.ico")
        Me.ImageList1.Images.SetKeyName(202, "Grid-Select All.ico")
        Me.ImageList1.Images.SetKeyName(203, "Grid-Select Cell-2.ico")
        Me.ImageList1.Images.SetKeyName(204, "Grid-Select Cell.ico")
        Me.ImageList1.Images.SetKeyName(205, "Grid-Select Column-2.ico")
        Me.ImageList1.Images.SetKeyName(206, "Grid-Select Column.ico")
        Me.ImageList1.Images.SetKeyName(207, "Grid-Select Row-2.ico")
        Me.ImageList1.Images.SetKeyName(208, "Grid-Select Row.ico")
        Me.ImageList1.Images.SetKeyName(209, "Grid-Split Cells.ico")
        Me.ImageList1.Images.SetKeyName(210, "Grid-Wizard-2.ico")
        Me.ImageList1.Images.SetKeyName(211, "Grid-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(212, "Hand Held.ico")
        Me.ImageList1.Images.SetKeyName(213, "Hand Shake.ico")
        Me.ImageList1.Images.SetKeyName(214, "Header and Footer.ico")
        Me.ImageList1.Images.SetKeyName(215, "Header and Footer-Toggle.ico")
        Me.ImageList1.Images.SetKeyName(216, "Header.ico")
        Me.ImageList1.Images.SetKeyName(217, "Header-Next.ico")
        Me.ImageList1.Images.SetKeyName(218, "Header-Previous.ico")
        Me.ImageList1.Images.SetKeyName(219, "Help.ico")
        Me.ImageList1.Images.SetKeyName(220, "Highlighter.ico")
        Me.ImageList1.Images.SetKeyName(221, "History.ico")
        Me.ImageList1.Images.SetKeyName(222, "Home.ico")
        Me.ImageList1.Images.SetKeyName(223, "Image.ico")
        Me.ImageList1.Images.SetKeyName(224, "Image-Brightness.ico")
        Me.ImageList1.Images.SetKeyName(225, "Image-Colour.ico")
        Me.ImageList1.Images.SetKeyName(226, "Image-Contrast.ico")
        Me.ImageList1.Images.SetKeyName(227, "Image-Crop.ico")
        Me.ImageList1.Images.SetKeyName(228, "Image-Flip.ico")
        Me.ImageList1.Images.SetKeyName(229, "Image-Mirror.ico")
        Me.ImageList1.Images.SetKeyName(230, "Image-Paint.ico")
        Me.ImageList1.Images.SetKeyName(231, "Image-Resize.ico")
        Me.ImageList1.Images.SetKeyName(232, "Image-Rotate.ico")
        Me.ImageList1.Images.SetKeyName(233, "Image-Select.ico")
        Me.ImageList1.Images.SetKeyName(234, "Import.ico")
        Me.ImageList1.Images.SetKeyName(235, "Import-Access.ico")
        Me.ImageList1.Images.SetKeyName(236, "Import-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(237, "Import-CSV.ico")
        Me.ImageList1.Images.SetKeyName(238, "Import-Excel.ico")
        Me.ImageList1.Images.SetKeyName(239, "Import-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(240, "Import-HTML.ico")
        Me.ImageList1.Images.SetKeyName(241, "Import-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(242, "Import-RTF.ico")
        Me.ImageList1.Images.SetKeyName(243, "Import-Text.ico")
        Me.ImageList1.Images.SetKeyName(244, "Import-Word.ico")
        Me.ImageList1.Images.SetKeyName(245, "Import-XML.ico")
        Me.ImageList1.Images.SetKeyName(246, "Indent-Decrease-2.ico")
        Me.ImageList1.Images.SetKeyName(247, "Indent-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(248, "Indent-Increase-2.ico")
        Me.ImageList1.Images.SetKeyName(249, "Indent-Increase.ico")
        Me.ImageList1.Images.SetKeyName(250, "Insert.ico")
        Me.ImageList1.Images.SetKeyName(251, "Insert-File.ico")
        Me.ImageList1.Images.SetKeyName(252, "Insert-Image.ico")
        Me.ImageList1.Images.SetKeyName(253, "Insert-Object.ico")
        Me.ImageList1.Images.SetKeyName(254, "Invoice.ico")
        Me.ImageList1.Images.SetKeyName(255, "Italic.ico")
        Me.ImageList1.Images.SetKeyName(256, "Key.ico")
        Me.ImageList1.Images.SetKeyName(257, "Landscape.ico")
        Me.ImageList1.Images.SetKeyName(258, "Launch.ico")
        Me.ImageList1.Images.SetKeyName(259, "Letter.ico")
        Me.ImageList1.Images.SetKeyName(260, "Line Spacing-2.ico")
        Me.ImageList1.Images.SetKeyName(261, "Line Spacing.ico")
        Me.ImageList1.Images.SetKeyName(262, "Link.ico")
        Me.ImageList1.Images.SetKeyName(263, "Lock.ico")
        Me.ImageList1.Images.SetKeyName(264, "Mail.ico")
        Me.ImageList1.Images.SetKeyName(265, "Mail-Forward.ico")
        Me.ImageList1.Images.SetKeyName(266, "Mail-In Box-2.ico")
        Me.ImageList1.Images.SetKeyName(267, "Mail-In Box.ico")
        Me.ImageList1.Images.SetKeyName(268, "Mail-In-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(269, "Mail-New.ico")
        Me.ImageList1.Images.SetKeyName(270, "Mail-Out Box-2.ico")
        Me.ImageList1.Images.SetKeyName(271, "Mail-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(272, "Mail-Reply To All.ico")
        Me.ImageList1.Images.SetKeyName(273, "Mail-Reply.ico")
        Me.ImageList1.Images.SetKeyName(274, "Mail-Search.ico")
        Me.ImageList1.Images.SetKeyName(275, "Mail-Send and Receive.ico")
        Me.ImageList1.Images.SetKeyName(276, "Media.ico")
        Me.ImageList1.Images.SetKeyName(277, "Microphone.ico")
        Me.ImageList1.Images.SetKeyName(278, "Midi Keyboard.ico")
        Me.ImageList1.Images.SetKeyName(279, "Minus.ico")
        Me.ImageList1.Images.SetKeyName(280, "mm-Eject.ico")
        Me.ImageList1.Images.SetKeyName(281, "mm-Fast Forward.ico")
        Me.ImageList1.Images.SetKeyName(282, "mm-First.ico")
        Me.ImageList1.Images.SetKeyName(283, "mm-Last.ico")
        Me.ImageList1.Images.SetKeyName(284, "mm-Pause.ico")
        Me.ImageList1.Images.SetKeyName(285, "mm-Play.ico")
        Me.ImageList1.Images.SetKeyName(286, "mm-Rewind.ico")
        Me.ImageList1.Images.SetKeyName(287, "mm-Stop.ico")
        Me.ImageList1.Images.SetKeyName(288, "Monitor.ico")
        Me.ImageList1.Images.SetKeyName(289, "Movie.ico")
        Me.ImageList1.Images.SetKeyName(290, "Musical Note-2.ico")
        Me.ImageList1.Images.SetKeyName(291, "Musical Note.ico")
        Me.ImageList1.Images.SetKeyName(292, "New.ico")
        Me.ImageList1.Images.SetKeyName(293, "Note.ico")
        Me.ImageList1.Images.SetKeyName(294, "Note-Add.ico")
        Me.ImageList1.Images.SetKeyName(295, "Note-Delete.ico")
        Me.ImageList1.Images.SetKeyName(296, "Note-Edit.ico")
        Me.ImageList1.Images.SetKeyName(297, "Note-New.ico")
        Me.ImageList1.Images.SetKeyName(298, "Note-Next.ico")
        Me.ImageList1.Images.SetKeyName(299, "Notepad.ico")
        Me.ImageList1.Images.SetKeyName(300, "Note-Previous.ico")
        Me.ImageList1.Images.SetKeyName(301, "Note-Remove.ico")
        Me.ImageList1.Images.SetKeyName(302, "Notes.ico")
        Me.ImageList1.Images.SetKeyName(303, "Note-Search.ico")
        Me.ImageList1.Images.SetKeyName(304, "Number of Pages.ico")
        Me.ImageList1.Images.SetKeyName(305, "Numbering-2.ico")
        Me.ImageList1.Images.SetKeyName(306, "Numbering.ico")
        Me.ImageList1.Images.SetKeyName(307, "Object-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(308, "Object-Select.ico")
        Me.ImageList1.Images.SetKeyName(309, "Object-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(310, "Open.ico")
        Me.ImageList1.Images.SetKeyName(311, "Outline-2.ico")
        Me.ImageList1.Images.SetKeyName(312, "Outline.ico")
        Me.ImageList1.Images.SetKeyName(313, "Page Number.ico")
        Me.ImageList1.Images.SetKeyName(314, "Page-Break.ico")
        Me.ImageList1.Images.SetKeyName(315, "Page-Invert.ico")
        Me.ImageList1.Images.SetKeyName(316, "Page-Next.ico")
        Me.ImageList1.Images.SetKeyName(317, "Page-Previous.ico")
        Me.ImageList1.Images.SetKeyName(318, "Page-Setup.ico")
        Me.ImageList1.Images.SetKeyName(319, "Paintbrush.ico")
        Me.ImageList1.Images.SetKeyName(320, "Parcel.ico")
        Me.ImageList1.Images.SetKeyName(321, "Paste.ico")
        Me.ImageList1.Images.SetKeyName(322, "Permission.ico")
        Me.ImageList1.Images.SetKeyName(323, "Phone.ico")
        Me.ImageList1.Images.SetKeyName(324, "Planner.ico")
        Me.ImageList1.Images.SetKeyName(325, "Plus.ico")
        Me.ImageList1.Images.SetKeyName(326, "Portrait.ico")
        Me.ImageList1.Images.SetKeyName(327, "Preview.ico")
        Me.ImageList1.Images.SetKeyName(328, "Print-2.ico")
        Me.ImageList1.Images.SetKeyName(329, "Print.ico")
        Me.ImageList1.Images.SetKeyName(330, "Print-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(331, "Print-Options.ico")
        Me.ImageList1.Images.SetKeyName(332, "Procedure.ico")
        Me.ImageList1.Images.SetKeyName(333, "Process.ico")
        Me.ImageList1.Images.SetKeyName(334, "Project.ico")
        Me.ImageList1.Images.SetKeyName(335, "Project-Gant Chart.ico")
        Me.ImageList1.Images.SetKeyName(336, "Properties.ico")
        Me.ImageList1.Images.SetKeyName(337, "Push Pin.ico")
        Me.ImageList1.Images.SetKeyName(338, "Recycle Bin.ico")
        Me.ImageList1.Images.SetKeyName(339, "Redo.ico")
        Me.ImageList1.Images.SetKeyName(340, "Refresh.ico")
        Me.ImageList1.Images.SetKeyName(341, "Remove.ico")
        Me.ImageList1.Images.SetKeyName(342, "Rename.ico")
        Me.ImageList1.Images.SetKeyName(343, "Report-2.ico")
        Me.ImageList1.Images.SetKeyName(344, "Report.ico")
        Me.ImageList1.Images.SetKeyName(345, "Report-Bound.ico")
        Me.ImageList1.Images.SetKeyName(346, "Research.ico")
        Me.ImageList1.Images.SetKeyName(347, "Ruler.ico")
        Me.ImageList1.Images.SetKeyName(348, "Ruler-Formatting.ico")
        Me.ImageList1.Images.SetKeyName(349, "Save All.ico")
        Me.ImageList1.Images.SetKeyName(350, "Save As HTML.ico")
        Me.ImageList1.Images.SetKeyName(351, "Save As.ico")
        Me.ImageList1.Images.SetKeyName(352, "Save Draft.ico")
        Me.ImageList1.Images.SetKeyName(353, "Save.ico")
        Me.ImageList1.Images.SetKeyName(354, "Script.ico")
        Me.ImageList1.Images.SetKeyName(355, "Search.ico")
        Me.ImageList1.Images.SetKeyName(356, "Search-Next.ico")
        Me.ImageList1.Images.SetKeyName(357, "Search-Previous.ico")
        Me.ImageList1.Images.SetKeyName(358, "Search-Replace.ico")
        Me.ImageList1.Images.SetKeyName(359, "Select All.ico")
        Me.ImageList1.Images.SetKeyName(360, "Slide Show.ico")
        Me.ImageList1.Images.SetKeyName(361, "Slide.ico")
        Me.ImageList1.Images.SetKeyName(362, "Software-Analyse.ico")
        Me.ImageList1.Images.SetKeyName(363, "Sort-Ascending.ico")
        Me.ImageList1.Images.SetKeyName(364, "Sort-Descending.ico")
        Me.ImageList1.Images.SetKeyName(365, "Sound-Delete.ico")
        Me.ImageList1.Images.SetKeyName(366, "Sound-Fade In.ico")
        Me.ImageList1.Images.SetKeyName(367, "Sound-Fade Out.ico")
        Me.ImageList1.Images.SetKeyName(368, "Sound-Join.ico")
        Me.ImageList1.Images.SetKeyName(369, "Sound-Mark End.ico")
        Me.ImageList1.Images.SetKeyName(370, "Sound-Mark Start.ico")
        Me.ImageList1.Images.SetKeyName(371, "Sound-Split.ico")
        Me.ImageList1.Images.SetKeyName(372, "Spell Check.ico")
        Me.ImageList1.Images.SetKeyName(373, "Stop.ico")
        Me.ImageList1.Images.SetKeyName(374, "Strikeout.ico")
        Me.ImageList1.Images.SetKeyName(375, "Subscript.ico")
        Me.ImageList1.Images.SetKeyName(376, "Sum.ico")
        Me.ImageList1.Images.SetKeyName(377, "Summary.ico")
        Me.ImageList1.Images.SetKeyName(378, "Superscript.ico")
        Me.ImageList1.Images.SetKeyName(379, "Support.ico")
        Me.ImageList1.Images.SetKeyName(380, "Tables and Borders.ico")
        Me.ImageList1.Images.SetKeyName(381, "Task.ico")
        Me.ImageList1.Images.SetKeyName(382, "Task-Search.ico")
        Me.ImageList1.Images.SetKeyName(383, "Thesaurus.ico")
        Me.ImageList1.Images.SetKeyName(384, "Tick.ico")
        Me.ImageList1.Images.SetKeyName(385, "Time Line.ico")
        Me.ImageList1.Images.SetKeyName(386, "Tip.ico")
        Me.ImageList1.Images.SetKeyName(387, "To Do List.ico")
        Me.ImageList1.Images.SetKeyName(388, "To Lowercase.ico")
        Me.ImageList1.Images.SetKeyName(389, "To Uppercase.ico")
        Me.ImageList1.Images.SetKeyName(390, "Tools.ico")
        Me.ImageList1.Images.SetKeyName(391, "Tree View.ico")
        Me.ImageList1.Images.SetKeyName(392, "Tree View-Add.ico")
        Me.ImageList1.Images.SetKeyName(393, "Tree View-Delete.ico")
        Me.ImageList1.Images.SetKeyName(394, "Tree View-Edit.ico")
        Me.ImageList1.Images.SetKeyName(395, "Tree View-New.ico")
        Me.ImageList1.Images.SetKeyName(396, "Tree View-Remove.ico")
        Me.ImageList1.Images.SetKeyName(397, "Tree View-Search.ico")
        Me.ImageList1.Images.SetKeyName(398, "Underline.ico")
        Me.ImageList1.Images.SetKeyName(399, "Undo.ico")
        Me.ImageList1.Images.SetKeyName(400, "Unlock.ico")
        Me.ImageList1.Images.SetKeyName(401, "User-2.ico")
        Me.ImageList1.Images.SetKeyName(402, "User.ico")
        Me.ImageList1.Images.SetKeyName(403, "User-Add-2.ico")
        Me.ImageList1.Images.SetKeyName(404, "User-Add.ico")
        Me.ImageList1.Images.SetKeyName(405, "User-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(406, "User-Delete.ico")
        Me.ImageList1.Images.SetKeyName(407, "User-Edit-2.ico")
        Me.ImageList1.Images.SetKeyName(408, "User-Edit.ico")
        Me.ImageList1.Images.SetKeyName(409, "User-New-2.ico")
        Me.ImageList1.Images.SetKeyName(410, "User-New.ico")
        Me.ImageList1.Images.SetKeyName(411, "User-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(412, "User-Options.ico")
        Me.ImageList1.Images.SetKeyName(413, "User-Password-2.ico")
        Me.ImageList1.Images.SetKeyName(414, "User-Password.ico")
        Me.ImageList1.Images.SetKeyName(415, "User-Remove-2.ico")
        Me.ImageList1.Images.SetKeyName(416, "User-Remove.ico")
        Me.ImageList1.Images.SetKeyName(417, "Users-2.ico")
        Me.ImageList1.Images.SetKeyName(418, "Users.ico")
        Me.ImageList1.Images.SetKeyName(419, "User-Search-2.ico")
        Me.ImageList1.Images.SetKeyName(420, "User-Search.ico")
        Me.ImageList1.Images.SetKeyName(421, "User-Security-2.ico")
        Me.ImageList1.Images.SetKeyName(422, "User-Security.ico")
        Me.ImageList1.Images.SetKeyName(423, "View-Details.ico")
        Me.ImageList1.Images.SetKeyName(424, "View-Large Icons.ico")
        Me.ImageList1.Images.SetKeyName(425, "View-List.ico")
        Me.ImageList1.Images.SetKeyName(426, "View-One Page.ico")
        Me.ImageList1.Images.SetKeyName(427, "View-Page Width.ico")
        Me.ImageList1.Images.SetKeyName(428, "Views.ico")
        Me.ImageList1.Images.SetKeyName(429, "View-Small Icons.ico")
        Me.ImageList1.Images.SetKeyName(430, "Volume.ico")
        Me.ImageList1.Images.SetKeyName(431, "Volume-Mute.ico")
        Me.ImageList1.Images.SetKeyName(432, "Warning.ico")
        Me.ImageList1.Images.SetKeyName(433, "Waste Bin.ico")
        Me.ImageList1.Images.SetKeyName(434, "Window.ico")
        Me.ImageList1.Images.SetKeyName(435, "Window-Add.ico")
        Me.ImageList1.Images.SetKeyName(436, "Window-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(437, "Window-Cascade.ico")
        Me.ImageList1.Images.SetKeyName(438, "Window-Close.ico")
        Me.ImageList1.Images.SetKeyName(439, "Window-Delete.ico")
        Me.ImageList1.Images.SetKeyName(440, "Window-Edit.ico")
        Me.ImageList1.Images.SetKeyName(441, "Window-Maximise.ico")
        Me.ImageList1.Images.SetKeyName(442, "Window-Minimise.ico")
        Me.ImageList1.Images.SetKeyName(443, "Window-New.ico")
        Me.ImageList1.Images.SetKeyName(444, "Window-Next.ico")
        Me.ImageList1.Images.SetKeyName(445, "Window-Options.ico")
        Me.ImageList1.Images.SetKeyName(446, "Window-Preview.ico")
        Me.ImageList1.Images.SetKeyName(447, "Window-Previous.ico")
        Me.ImageList1.Images.SetKeyName(448, "Window-Properties.ico")
        Me.ImageList1.Images.SetKeyName(449, "Window-Remove.ico")
        Me.ImageList1.Images.SetKeyName(450, "Window-Search.ico")
        Me.ImageList1.Images.SetKeyName(451, "Window-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(452, "Window-Split.ico")
        Me.ImageList1.Images.SetKeyName(453, "Window-Tile Horizontal.ico")
        Me.ImageList1.Images.SetKeyName(454, "Window-Tile Vertical.ico")
        Me.ImageList1.Images.SetKeyName(455, "Wizard.ico")
        Me.ImageList1.Images.SetKeyName(456, "Word Count.ico")
        Me.ImageList1.Images.SetKeyName(457, "Word Wrap-2.ico")
        Me.ImageList1.Images.SetKeyName(458, "Word Wrap.ico")
        Me.ImageList1.Images.SetKeyName(459, "Work Sheet.ico")
        Me.ImageList1.Images.SetKeyName(460, "Work Sheet-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(461, "Work Sheet-Delete.ico")
        Me.ImageList1.Images.SetKeyName(462, "Work Sheet-New.ico")
        Me.ImageList1.Images.SetKeyName(463, "Work Sheet-Options.ico")
        Me.ImageList1.Images.SetKeyName(464, "Work Sheet-Protect.ico")
        Me.ImageList1.Images.SetKeyName(465, "Work Sheet-Rename.ico")
        Me.ImageList1.Images.SetKeyName(466, "Zoom-In.ico")
        Me.ImageList1.Images.SetKeyName(467, "Zoom-Out.ico")
        '
        'gcPhotos
        '
        Me.gcPhotos.DataSource = Me.EUS_LISTS_PhotosDisplayLevelBindingSource
        Me.gcPhotos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcPhotos.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.gcPhotos.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.gcPhotos.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.gcPhotos.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.gcPhotos.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.gcPhotos.Location = New System.Drawing.Point(2, 2)
        Me.gcPhotos.MainView = Me.layoutViewPhotos
        Me.gcPhotos.Name = "gcPhotos"
        Me.gcPhotos.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.repositoryItemPictureEdit1, Me.repositoryItemICBDisplayLevel, Me.repositoryItemCheckEdit1, Me.repositoryItemCheckEdit2, Me.repositoryItemImageComboBox1, Me.repositoryItemCheckEdit3, Me.repositoryItemTextEditPhotoURL, Me.repositoryItemButtonEditCopyPhotoURL, Me.RepositoryItemCheckEdit_IsDeleted, Me.RepositoryItemCheckEdit_IsAutoApproved})
        Me.gcPhotos.Size = New System.Drawing.Size(956, 493)
        Me.gcPhotos.TabIndex = 252
        Me.gcPhotos.UseEmbeddedNavigator = True
        Me.gcPhotos.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.layoutViewPhotos})
        '
        'EUS_LISTS_PhotosDisplayLevelBindingSource
        '
        Me.EUS_LISTS_PhotosDisplayLevelBindingSource.DataMember = "EUS_LISTS_PhotosDisplayLevel"
        Me.EUS_LISTS_PhotosDisplayLevelBindingSource.DataSource = Me.DSLists
        '
        'DSLists
        '
        Me.DSLists.DataSetName = "DSLists"
        Me.DSLists.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'layoutViewPhotos
        '
        Me.layoutViewPhotos.Appearance.CardCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.layoutViewPhotos.Appearance.CardCaption.Options.UseFont = True
        Me.layoutViewPhotos.Appearance.CardCaption.Options.UseTextOptions = True
        Me.layoutViewPhotos.Appearance.CardCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.layoutViewPhotos.Appearance.CardCaption.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
        Me.layoutViewPhotos.Appearance.FocusedCardCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.layoutViewPhotos.Appearance.FocusedCardCaption.Options.UseFont = True
        Me.layoutViewPhotos.Appearance.FocusedCardCaption.Options.UseTextOptions = True
        Me.layoutViewPhotos.Appearance.FocusedCardCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.layoutViewPhotos.Appearance.FocusedCardCaption.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
        Me.layoutViewPhotos.Appearance.HideSelectionCardCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.layoutViewPhotos.Appearance.HideSelectionCardCaption.Options.UseFont = True
        Me.layoutViewPhotos.Appearance.HideSelectionCardCaption.Options.UseTextOptions = True
        Me.layoutViewPhotos.Appearance.HideSelectionCardCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.layoutViewPhotos.Appearance.HideSelectionCardCaption.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
        Me.layoutViewPhotos.Appearance.SelectedCardCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.layoutViewPhotos.Appearance.SelectedCardCaption.Options.UseFont = True
        Me.layoutViewPhotos.Appearance.SelectedCardCaption.Options.UseTextOptions = True
        Me.layoutViewPhotos.Appearance.SelectedCardCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.layoutViewPhotos.Appearance.SelectedCardCaption.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter
        Me.layoutViewPhotos.CardCaptionFormat = "{2}"
        Me.layoutViewPhotos.CardMinSize = New System.Drawing.Size(379, 364)
        Me.layoutViewPhotos.Columns.AddRange(New DevExpress.XtraGrid.Columns.LayoutViewColumn() {Me.colCaption, Me.colCustomerPhotosID, Me.colCustomerID, Me.colDateTimeToUploading, Me.colFileName, Me.colDisplayLevel, Me.colHasAproved, Me.colHasDeclined, Me.colCheckedContextID, Me.colIsDefault, Me.colPhoto, Me.colPhotoURL, Me.colDimentions, Me.colSize, Me.colType, Me.colCopyPhotoURL, Me.colIsDeleted, Me.colIsAutoApproved})
        Me.layoutViewPhotos.GridControl = Me.gcPhotos
        Me.layoutViewPhotos.HiddenItems.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.layoutViewField_colCaption, Me.layoutViewField_colCheckedContextID, Me.layoutViewField_colCopyPhotoURL})
        Me.layoutViewPhotos.Name = "layoutViewPhotos"
        Me.layoutViewPhotos.OptionsCarouselMode.PitchAngle = 1.9!
        Me.layoutViewPhotos.OptionsCarouselMode.RollAngle = 2.141593!
        Me.layoutViewPhotos.OptionsItemText.AlignMode = DevExpress.XtraGrid.Views.Layout.FieldTextAlignMode.AutoSize
        Me.layoutViewPhotos.OptionsItemText.TextToControlDistance = 3
        Me.layoutViewPhotos.OptionsView.AllowHotTrackFields = False
        Me.layoutViewPhotos.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.NeverAnimate
        Me.layoutViewPhotos.OptionsView.ShowCardExpandButton = False
        Me.layoutViewPhotos.OptionsView.ShowCardLines = False
        Me.layoutViewPhotos.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never
        Me.layoutViewPhotos.OptionsView.ViewMode = DevExpress.XtraGrid.Views.Layout.LayoutViewMode.Row
        Me.layoutViewPhotos.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCopyPhotoURL, DevExpress.Data.ColumnSortOrder.Ascending)})
        Me.layoutViewPhotos.TemplateCard = Me.LayoutViewCard1
        '
        'colCaption
        '
        Me.colCaption.Caption = "Caption"
        Me.colCaption.ColumnEdit = Me.repositoryItemTextEdit1
        Me.colCaption.CustomizationCaption = "Caption"
        Me.colCaption.FieldName = "Caption"
        Me.colCaption.LayoutViewField = Me.layoutViewField_colCaption
        Me.colCaption.Name = "colCaption"
        Me.colCaption.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colCaption.OptionsFilter.AllowFilter = False
        Me.colCaption.UnboundType = DevExpress.Data.UnboundColumnType.[Object]
        '
        'layoutViewField_colCaption
        '
        Me.layoutViewField_colCaption.EditorPreferredWidth = 25
        Me.layoutViewField_colCaption.Location = New System.Drawing.Point(0, 0)
        Me.layoutViewField_colCaption.Name = "layoutViewField_colCaption"
        Me.layoutViewField_colCaption.Size = New System.Drawing.Size(373, 333)
        Me.layoutViewField_colCaption.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colCaption.TextSize = New System.Drawing.Size(0, 0)
        Me.layoutViewField_colCaption.TextToControlDistance = 0
        Me.layoutViewField_colCaption.TextVisible = False
        '
        'colCustomerPhotosID
        '
        Me.colCustomerPhotosID.Caption = "Photo ID"
        Me.colCustomerPhotosID.CustomizationCaption = "Photo ID"
        Me.colCustomerPhotosID.FieldName = "CustomerPhotosID"
        Me.colCustomerPhotosID.LayoutViewField = Me.layoutViewField_colCustomerPhotosID
        Me.colCustomerPhotosID.Name = "colCustomerPhotosID"
        Me.colCustomerPhotosID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colCustomerPhotosID.OptionsFilter.AllowFilter = False
        '
        'layoutViewField_colCustomerPhotosID
        '
        Me.layoutViewField_colCustomerPhotosID.AppearanceItemCaption.Options.UseTextOptions = True
        Me.layoutViewField_colCustomerPhotosID.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.layoutViewField_colCustomerPhotosID.EditorPreferredWidth = 128
        Me.layoutViewField_colCustomerPhotosID.Location = New System.Drawing.Point(0, 0)
        Me.layoutViewField_colCustomerPhotosID.Name = "layoutViewField_colCustomerPhotosID"
        Me.layoutViewField_colCustomerPhotosID.Size = New System.Drawing.Size(185, 22)
        Me.layoutViewField_colCustomerPhotosID.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colCustomerPhotosID.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colCustomerPhotosID.TextSize = New System.Drawing.Size(46, 13)
        Me.layoutViewField_colCustomerPhotosID.TextToControlDistance = 5
        '
        'colCustomerID
        '
        Me.colCustomerID.Caption = "Profile ID"
        Me.colCustomerID.CustomizationCaption = "Profile ID"
        Me.colCustomerID.FieldName = "CustomerID"
        Me.colCustomerID.LayoutViewField = Me.layoutViewField_colCustomerID
        Me.colCustomerID.Name = "colCustomerID"
        Me.colCustomerID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colCustomerID.OptionsFilter.AllowFilter = False
        '
        'layoutViewField_colCustomerID
        '
        Me.layoutViewField_colCustomerID.AppearanceItemCaption.Options.UseTextOptions = True
        Me.layoutViewField_colCustomerID.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.layoutViewField_colCustomerID.EditorPreferredWidth = 129
        Me.layoutViewField_colCustomerID.Location = New System.Drawing.Point(185, 0)
        Me.layoutViewField_colCustomerID.Name = "layoutViewField_colCustomerID"
        Me.layoutViewField_colCustomerID.Size = New System.Drawing.Size(188, 22)
        Me.layoutViewField_colCustomerID.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colCustomerID.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colCustomerID.TextSize = New System.Drawing.Size(48, 13)
        Me.layoutViewField_colCustomerID.TextToControlDistance = 5
        '
        'colDateTimeToUploading
        '
        Me.colDateTimeToUploading.Caption = "Date Uploaded"
        Me.colDateTimeToUploading.CustomizationCaption = "Date Uploaded"
        Me.colDateTimeToUploading.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"
        Me.colDateTimeToUploading.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colDateTimeToUploading.FieldName = "DateTimeToUploading"
        Me.colDateTimeToUploading.LayoutViewField = Me.layoutViewField_colDateTimeToUploading
        Me.colDateTimeToUploading.Name = "colDateTimeToUploading"
        Me.colDateTimeToUploading.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colDateTimeToUploading.OptionsFilter.AllowFilter = False
        '
        'layoutViewField_colDateTimeToUploading
        '
        Me.layoutViewField_colDateTimeToUploading.EditorPreferredWidth = 99
        Me.layoutViewField_colDateTimeToUploading.Location = New System.Drawing.Point(0, 66)
        Me.layoutViewField_colDateTimeToUploading.Name = "layoutViewField_colDateTimeToUploading"
        Me.layoutViewField_colDateTimeToUploading.Size = New System.Drawing.Size(185, 22)
        Me.layoutViewField_colDateTimeToUploading.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colDateTimeToUploading.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colDateTimeToUploading.TextSize = New System.Drawing.Size(75, 13)
        Me.layoutViewField_colDateTimeToUploading.TextToControlDistance = 5
        '
        'colFileName
        '
        Me.colFileName.Caption = "File Name"
        Me.colFileName.CustomizationCaption = "File Name"
        Me.colFileName.FieldName = "FileName"
        Me.colFileName.LayoutViewField = Me.layoutViewField_colFileName
        Me.colFileName.Name = "colFileName"
        Me.colFileName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colFileName.OptionsFilter.AllowFilter = False
        '
        'layoutViewField_colFileName
        '
        Me.layoutViewField_colFileName.EditorPreferredWidth = 312
        Me.layoutViewField_colFileName.Location = New System.Drawing.Point(0, 22)
        Me.layoutViewField_colFileName.Name = "layoutViewField_colFileName"
        Me.layoutViewField_colFileName.Size = New System.Drawing.Size(373, 22)
        Me.layoutViewField_colFileName.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colFileName.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colFileName.TextSize = New System.Drawing.Size(50, 13)
        Me.layoutViewField_colFileName.TextToControlDistance = 5
        '
        'colDisplayLevel
        '
        Me.colDisplayLevel.Caption = "Display Level"
        Me.colDisplayLevel.ColumnEdit = Me.repositoryItemICBDisplayLevel
        Me.colDisplayLevel.CustomizationCaption = "Display Level"
        Me.colDisplayLevel.DisplayFormat.FormatString = "d"
        Me.colDisplayLevel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colDisplayLevel.FieldName = "DisplayLevel"
        Me.colDisplayLevel.LayoutViewField = Me.layoutViewField_colDisplayLevel
        Me.colDisplayLevel.Name = "colDisplayLevel"
        Me.colDisplayLevel.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colDisplayLevel.OptionsFilter.AllowFilter = False
        '
        'repositoryItemICBDisplayLevel
        '
        Me.repositoryItemICBDisplayLevel.AutoHeight = False
        Me.repositoryItemICBDisplayLevel.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.repositoryItemICBDisplayLevel.Name = "repositoryItemICBDisplayLevel"
        '
        'layoutViewField_colDisplayLevel
        '
        Me.layoutViewField_colDisplayLevel.EditorPreferredWidth = 111
        Me.layoutViewField_colDisplayLevel.Location = New System.Drawing.Point(185, 66)
        Me.layoutViewField_colDisplayLevel.Name = "layoutViewField_colDisplayLevel"
        Me.layoutViewField_colDisplayLevel.Size = New System.Drawing.Size(188, 22)
        Me.layoutViewField_colDisplayLevel.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colDisplayLevel.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colDisplayLevel.TextSize = New System.Drawing.Size(66, 13)
        Me.layoutViewField_colDisplayLevel.TextToControlDistance = 5
        '
        'colHasAproved
        '
        Me.colHasAproved.Caption = "Is Aproved"
        Me.colHasAproved.ColumnEdit = Me.repositoryItemCheckEdit1
        Me.colHasAproved.CustomizationCaption = "Is Aproved"
        Me.colHasAproved.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHasAproved.FieldName = "HasAproved"
        Me.colHasAproved.LayoutViewField = Me.layoutViewField_colHasAproved
        Me.colHasAproved.Name = "colHasAproved"
        Me.colHasAproved.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colHasAproved.OptionsFilter.AllowFilter = False
        '
        'repositoryItemCheckEdit1
        '
        Me.repositoryItemCheckEdit1.AutoHeight = False
        Me.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1"
        '
        'layoutViewField_colHasAproved
        '
        Me.layoutViewField_colHasAproved.AppearanceItemCaption.Options.UseTextOptions = True
        Me.layoutViewField_colHasAproved.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.layoutViewField_colHasAproved.EditorPreferredWidth = 20
        Me.layoutViewField_colHasAproved.Location = New System.Drawing.Point(0, 88)
        Me.layoutViewField_colHasAproved.MaxSize = New System.Drawing.Size(0, 22)
        Me.layoutViewField_colHasAproved.MinSize = New System.Drawing.Size(85, 22)
        Me.layoutViewField_colHasAproved.Name = "layoutViewField_colHasAproved"
        Me.layoutViewField_colHasAproved.Size = New System.Drawing.Size(88, 22)
        Me.layoutViewField_colHasAproved.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.layoutViewField_colHasAproved.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colHasAproved.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colHasAproved.TextLocation = DevExpress.Utils.Locations.Right
        Me.layoutViewField_colHasAproved.TextSize = New System.Drawing.Size(57, 13)
        Me.layoutViewField_colHasAproved.TextToControlDistance = 5
        '
        'colHasDeclined
        '
        Me.colHasDeclined.Caption = "Is Declined"
        Me.colHasDeclined.ColumnEdit = Me.repositoryItemCheckEdit2
        Me.colHasDeclined.CustomizationCaption = "Is Declined"
        Me.colHasDeclined.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHasDeclined.FieldName = "HasDeclined"
        Me.colHasDeclined.LayoutViewField = Me.layoutViewField_colHasDeclined
        Me.colHasDeclined.Name = "colHasDeclined"
        Me.colHasDeclined.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colHasDeclined.OptionsFilter.AllowFilter = False
        '
        'repositoryItemCheckEdit2
        '
        Me.repositoryItemCheckEdit2.AutoHeight = False
        Me.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2"
        '
        'layoutViewField_colHasDeclined
        '
        Me.layoutViewField_colHasDeclined.AppearanceItemCaption.Options.UseTextOptions = True
        Me.layoutViewField_colHasDeclined.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.layoutViewField_colHasDeclined.EditorPreferredWidth = 21
        Me.layoutViewField_colHasDeclined.Location = New System.Drawing.Point(0, 110)
        Me.layoutViewField_colHasDeclined.MaxSize = New System.Drawing.Size(0, 22)
        Me.layoutViewField_colHasDeclined.MinSize = New System.Drawing.Size(85, 22)
        Me.layoutViewField_colHasDeclined.Name = "layoutViewField_colHasDeclined"
        Me.layoutViewField_colHasDeclined.Size = New System.Drawing.Size(88, 22)
        Me.layoutViewField_colHasDeclined.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.layoutViewField_colHasDeclined.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colHasDeclined.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colHasDeclined.TextLocation = DevExpress.Utils.Locations.Right
        Me.layoutViewField_colHasDeclined.TextSize = New System.Drawing.Size(56, 13)
        Me.layoutViewField_colHasDeclined.TextToControlDistance = 5
        '
        'colCheckedContextID
        '
        Me.colCheckedContextID.Caption = "Checked ContextID"
        Me.colCheckedContextID.CustomizationCaption = "CheckedContextID"
        Me.colCheckedContextID.FieldName = "CheckedContextID"
        Me.colCheckedContextID.LayoutViewField = Me.layoutViewField_colCheckedContextID
        Me.colCheckedContextID.Name = "colCheckedContextID"
        Me.colCheckedContextID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colCheckedContextID.OptionsFilter.AllowFilter = False
        '
        'layoutViewField_colCheckedContextID
        '
        Me.layoutViewField_colCheckedContextID.EditorPreferredWidth = 10
        Me.layoutViewField_colCheckedContextID.Location = New System.Drawing.Point(0, 0)
        Me.layoutViewField_colCheckedContextID.Name = "layoutViewField_colCheckedContextID"
        Me.layoutViewField_colCheckedContextID.Size = New System.Drawing.Size(373, 333)
        Me.layoutViewField_colCheckedContextID.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colCheckedContextID.TextSize = New System.Drawing.Size(95, 13)
        Me.layoutViewField_colCheckedContextID.TextToControlDistance = 5
        '
        'colIsDefault
        '
        Me.colIsDefault.AppearanceCell.Options.UseTextOptions = True
        Me.colIsDefault.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.colIsDefault.AppearanceHeader.Options.UseTextOptions = True
        Me.colIsDefault.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.colIsDefault.Caption = "Is Default"
        Me.colIsDefault.ColumnEdit = Me.repositoryItemCheckEdit3
        Me.colIsDefault.CustomizationCaption = "Is Default"
        Me.colIsDefault.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colIsDefault.FieldName = "IsDefault"
        Me.colIsDefault.LayoutViewField = Me.layoutViewField_colIsDefault
        Me.colIsDefault.Name = "colIsDefault"
        Me.colIsDefault.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colIsDefault.OptionsFilter.AllowFilter = False
        '
        'repositoryItemCheckEdit3
        '
        Me.repositoryItemCheckEdit3.AutoHeight = False
        Me.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3"
        '
        'layoutViewField_colIsDefault
        '
        Me.layoutViewField_colIsDefault.AppearanceItemCaption.Options.UseTextOptions = True
        Me.layoutViewField_colIsDefault.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.layoutViewField_colIsDefault.EditorPreferredWidth = 23
        Me.layoutViewField_colIsDefault.Location = New System.Drawing.Point(0, 132)
        Me.layoutViewField_colIsDefault.MaxSize = New System.Drawing.Size(0, 22)
        Me.layoutViewField_colIsDefault.MinSize = New System.Drawing.Size(82, 22)
        Me.layoutViewField_colIsDefault.Name = "layoutViewField_colIsDefault"
        Me.layoutViewField_colIsDefault.Size = New System.Drawing.Size(85, 22)
        Me.layoutViewField_colIsDefault.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.layoutViewField_colIsDefault.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colIsDefault.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colIsDefault.TextLocation = DevExpress.Utils.Locations.Right
        Me.layoutViewField_colIsDefault.TextSize = New System.Drawing.Size(51, 13)
        Me.layoutViewField_colIsDefault.TextToControlDistance = 5
        '
        'colPhoto
        '
        Me.colPhoto.Caption = "Photo"
        Me.colPhoto.ColumnEdit = Me.repositoryItemPictureEdit1
        Me.colPhoto.CustomizationCaption = "Photo"
        Me.colPhoto.FieldName = "Photo"
        Me.colPhoto.LayoutViewField = Me.layoutViewField_colPhoto
        Me.colPhoto.Name = "colPhoto"
        Me.colPhoto.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colPhoto.OptionsFilter.AllowFilter = False
        '
        'repositoryItemPictureEdit1
        '
        Me.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1"
        Me.repositoryItemPictureEdit1.ReadOnly = True
        Me.repositoryItemPictureEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        '
        'layoutViewField_colPhoto
        '
        Me.layoutViewField_colPhoto.EditorPreferredWidth = 241
        Me.layoutViewField_colPhoto.Location = New System.Drawing.Point(126, 90)
        Me.layoutViewField_colPhoto.Name = "layoutViewField_colPhoto"
        Me.layoutViewField_colPhoto.Size = New System.Drawing.Size(247, 172)
        Me.layoutViewField_colPhoto.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colPhoto.TextSize = New System.Drawing.Size(0, 0)
        Me.layoutViewField_colPhoto.TextToControlDistance = 0
        Me.layoutViewField_colPhoto.TextVisible = False
        '
        'colPhotoURL
        '
        Me.colPhotoURL.Caption = "Photo URL"
        Me.colPhotoURL.ColumnEdit = Me.repositoryItemTextEditPhotoURL
        Me.colPhotoURL.CustomizationCaption = "Photo URL"
        Me.colPhotoURL.FieldName = "PhotoURL"
        Me.colPhotoURL.LayoutViewField = Me.layoutViewField_colPhotoURL
        Me.colPhotoURL.Name = "colPhotoURL"
        Me.colPhotoURL.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colPhotoURL.OptionsFilter.AllowFilter = False
        '
        'repositoryItemTextEditPhotoURL
        '
        Me.repositoryItemTextEditPhotoURL.AutoHeight = False
        Me.repositoryItemTextEditPhotoURL.Name = "repositoryItemTextEditPhotoURL"
        '
        'layoutViewField_colPhotoURL
        '
        Me.layoutViewField_colPhotoURL.EditorPreferredWidth = 308
        Me.layoutViewField_colPhotoURL.Location = New System.Drawing.Point(0, 44)
        Me.layoutViewField_colPhotoURL.Name = "layoutViewField_colPhotoURL"
        Me.layoutViewField_colPhotoURL.Size = New System.Drawing.Size(373, 22)
        Me.layoutViewField_colPhotoURL.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colPhotoURL.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colPhotoURL.TextSize = New System.Drawing.Size(54, 13)
        Me.layoutViewField_colPhotoURL.TextToControlDistance = 5
        '
        'colDimentions
        '
        Me.colDimentions.Caption = "Dimentions"
        Me.colDimentions.CustomizationCaption = "Dimentions"
        Me.colDimentions.FieldName = "Dimentions"
        Me.colDimentions.LayoutViewField = Me.layoutViewField_colDimentions
        Me.colDimentions.Name = "colDimentions"
        Me.colDimentions.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colDimentions.OptionsFilter.AllowFilter = False
        '
        'layoutViewField_colDimentions
        '
        Me.layoutViewField_colDimentions.EditorPreferredWidth = 118
        Me.layoutViewField_colDimentions.Location = New System.Drawing.Point(0, 196)
        Me.layoutViewField_colDimentions.Name = "layoutViewField_colDimentions"
        Me.layoutViewField_colDimentions.Size = New System.Drawing.Size(124, 22)
        Me.layoutViewField_colDimentions.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colDimentions.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colDimentions.TextSize = New System.Drawing.Size(0, 0)
        Me.layoutViewField_colDimentions.TextToControlDistance = 0
        Me.layoutViewField_colDimentions.TextVisible = False
        '
        'colSize
        '
        Me.colSize.Caption = "Size"
        Me.colSize.CustomizationCaption = "Size"
        Me.colSize.FieldName = "Size"
        Me.colSize.LayoutViewField = Me.layoutViewField_colSize
        Me.colSize.Name = "colSize"
        Me.colSize.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colSize.OptionsFilter.AllowFilter = False
        '
        'layoutViewField_colSize
        '
        Me.layoutViewField_colSize.EditorPreferredWidth = 90
        Me.layoutViewField_colSize.Location = New System.Drawing.Point(0, 218)
        Me.layoutViewField_colSize.Name = "layoutViewField_colSize"
        Me.layoutViewField_colSize.Size = New System.Drawing.Size(124, 22)
        Me.layoutViewField_colSize.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colSize.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colSize.TextSize = New System.Drawing.Size(23, 13)
        Me.layoutViewField_colSize.TextToControlDistance = 5
        '
        'colType
        '
        Me.colType.Caption = "Type"
        Me.colType.CustomizationCaption = "Type"
        Me.colType.FieldName = "Type"
        Me.colType.LayoutViewField = Me.layoutViewField_colType
        Me.colType.Name = "colType"
        Me.colType.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colType.OptionsFilter.AllowFilter = False
        '
        'layoutViewField_colType
        '
        Me.layoutViewField_colType.EditorPreferredWidth = 85
        Me.layoutViewField_colType.Location = New System.Drawing.Point(0, 240)
        Me.layoutViewField_colType.Name = "layoutViewField_colType"
        Me.layoutViewField_colType.Size = New System.Drawing.Size(124, 22)
        Me.layoutViewField_colType.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.layoutViewField_colType.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colType.TextSize = New System.Drawing.Size(28, 13)
        Me.layoutViewField_colType.TextToControlDistance = 5
        '
        'colCopyPhotoURL
        '
        Me.colCopyPhotoURL.Caption = "LayoutViewColumn1"
        Me.colCopyPhotoURL.ColumnEdit = Me.repositoryItemButtonEditCopyPhotoURL
        Me.colCopyPhotoURL.LayoutViewField = Me.layoutViewField_colCopyPhotoURL
        Me.colCopyPhotoURL.Name = "colCopyPhotoURL"
        Me.colCopyPhotoURL.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colCopyPhotoURL.OptionsFilter.AllowFilter = False
        '
        'repositoryItemButtonEditCopyPhotoURL
        '
        Me.repositoryItemButtonEditCopyPhotoURL.AutoHeight = False
        Me.repositoryItemButtonEditCopyPhotoURL.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "Copy", -1, True, True, False, DevExpress.XtraEditors.ImageLocation.MiddleCenter, Nothing, New DevExpress.Utils.KeyShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C)), SerializableAppearanceObject2, "", Nothing, Nothing, True)})
        Me.repositoryItemButtonEditCopyPhotoURL.Name = "repositoryItemButtonEditCopyPhotoURL"
        Me.repositoryItemButtonEditCopyPhotoURL.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor
        '
        'layoutViewField_colCopyPhotoURL
        '
        Me.layoutViewField_colCopyPhotoURL.EditorPreferredWidth = 20
        Me.layoutViewField_colCopyPhotoURL.Location = New System.Drawing.Point(0, 0)
        Me.layoutViewField_colCopyPhotoURL.Name = "layoutViewField_colCopyPhotoURL"
        Me.layoutViewField_colCopyPhotoURL.Size = New System.Drawing.Size(373, 333)
        Me.layoutViewField_colCopyPhotoURL.TextSize = New System.Drawing.Size(0, 0)
        Me.layoutViewField_colCopyPhotoURL.TextToControlDistance = 0
        Me.layoutViewField_colCopyPhotoURL.TextVisible = False
        '
        'colIsDeleted
        '
        Me.colIsDeleted.Caption = "Is Deleted"
        Me.colIsDeleted.ColumnEdit = Me.RepositoryItemCheckEdit_IsDeleted
        Me.colIsDeleted.FieldName = "IsDeleted"
        Me.colIsDeleted.LayoutViewField = Me.layoutViewField_colIsDeleted
        Me.colIsDeleted.Name = "colIsDeleted"
        Me.colIsDeleted.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colIsDeleted.OptionsFilter.AllowFilter = False
        '
        'RepositoryItemCheckEdit_IsDeleted
        '
        Me.RepositoryItemCheckEdit_IsDeleted.AutoHeight = False
        Me.RepositoryItemCheckEdit_IsDeleted.Name = "RepositoryItemCheckEdit_IsDeleted"
        '
        'layoutViewField_colIsDeleted
        '
        Me.layoutViewField_colIsDeleted.EditorPreferredWidth = 24
        Me.layoutViewField_colIsDeleted.Location = New System.Drawing.Point(0, 154)
        Me.layoutViewField_colIsDeleted.Name = "layoutViewField_colIsDeleted"
        Me.layoutViewField_colIsDeleted.Size = New System.Drawing.Size(85, 20)
        Me.layoutViewField_colIsDeleted.TextLocation = DevExpress.Utils.Locations.Right
        Me.layoutViewField_colIsDeleted.TextSize = New System.Drawing.Size(53, 13)
        '
        'colIsAutoApproved
        '
        Me.colIsAutoApproved.Caption = "Is  Auto Approved"
        Me.colIsAutoApproved.ColumnEdit = Me.RepositoryItemCheckEdit_IsAutoApproved
        Me.colIsAutoApproved.FieldName = "IsAutoApproved"
        Me.colIsAutoApproved.LayoutViewField = Me.layoutViewField_colIsAutoApproved
        Me.colIsAutoApproved.Name = "colIsAutoApproved"
        Me.colIsAutoApproved.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[False]
        Me.colIsAutoApproved.OptionsFilter.AllowFilter = False
        '
        'RepositoryItemCheckEdit_IsAutoApproved
        '
        Me.RepositoryItemCheckEdit_IsAutoApproved.AutoHeight = False
        Me.RepositoryItemCheckEdit_IsAutoApproved.Name = "RepositoryItemCheckEdit_IsAutoApproved"
        '
        'layoutViewField_colIsAutoApproved
        '
        Me.layoutViewField_colIsAutoApproved.EditorPreferredWidth = 23
        Me.layoutViewField_colIsAutoApproved.Location = New System.Drawing.Point(0, 176)
        Me.layoutViewField_colIsAutoApproved.Name = "layoutViewField_colIsAutoApproved"
        Me.layoutViewField_colIsAutoApproved.Size = New System.Drawing.Size(124, 20)
        Me.layoutViewField_colIsAutoApproved.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.layoutViewField_colIsAutoApproved.TextLocation = DevExpress.Utils.Locations.Right
        Me.layoutViewField_colIsAutoApproved.TextSize = New System.Drawing.Size(92, 13)
        Me.layoutViewField_colIsAutoApproved.TextToControlDistance = 5
        '
        'LayoutViewCard1
        '
        Me.LayoutViewCard1.CustomizationFormText = "layoutViewTemplateCard"
        Me.LayoutViewCard1.ExpandButtonLocation = DevExpress.Utils.GroupElementLocation.AfterText
        Me.LayoutViewCard1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.layoutViewField_colDimentions, Me.layoutViewField_colType, Me.layoutViewField_colPhoto, Me.layoutViewField_colCustomerPhotosID, Me.layoutViewField_colCustomerID, Me.layoutViewField_colFileName, Me.layoutViewField_colDisplayLevel, Me.layoutViewField_colDateTimeToUploading, Me.layoutViewField_colSize, Me.layoutViewField_colPhotoURL, Me.item4, Me.layoutViewField_colHasDeclined, Me.layoutViewField_colHasAproved, Me.item3, Me.item1, Me.item6, Me.item7, Me.item8, Me.layoutViewField_colIsDefault, Me.layoutViewField_colIsDeleted, Me.layoutViewField_colIsAutoApproved, Me.item2})
        Me.LayoutViewCard1.Name = "layoutViewTemplateCard"
        Me.LayoutViewCard1.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.LayoutViewCard1.Text = "TemplateCard"
        '
        'item4
        '
        Me.item4.AllowHotTrack = False
        Me.item4.CustomizationFormText = "item4"
        Me.item4.Location = New System.Drawing.Point(0, 174)
        Me.item4.Name = "item4"
        Me.item4.Size = New System.Drawing.Size(124, 2)
        Me.item4.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.item4.Text = "item4"
        '
        'item3
        '
        Me.item3.AllowHotTrack = False
        Me.item3.CustomizationFormText = "item3"
        Me.item3.Location = New System.Drawing.Point(124, 88)
        Me.item3.Name = "item3"
        Me.item3.Size = New System.Drawing.Size(2, 174)
        Me.item3.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.item3.Text = "item3"
        '
        'item1
        '
        Me.item1.AllowHotTrack = False
        Me.item1.CustomizationFormText = "item1"
        Me.item1.Location = New System.Drawing.Point(126, 88)
        Me.item1.Name = "item1"
        Me.item1.Size = New System.Drawing.Size(247, 2)
        Me.item1.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.item1.Text = "item1"
        '
        'item6
        '
        Me.item6.AllowHotTrack = False
        Me.item6.CustomizationFormText = "item6"
        Me.item6.Location = New System.Drawing.Point(88, 88)
        Me.item6.Name = "item6"
        Me.item6.Size = New System.Drawing.Size(36, 22)
        Me.item6.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.item6.Text = "item6"
        Me.item6.TextSize = New System.Drawing.Size(0, 0)
        '
        'item7
        '
        Me.item7.AllowHotTrack = False
        Me.item7.CustomizationFormText = "item7"
        Me.item7.Location = New System.Drawing.Point(88, 110)
        Me.item7.Name = "item7"
        Me.item7.Size = New System.Drawing.Size(36, 22)
        Me.item7.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.item7.Text = "item7"
        Me.item7.TextSize = New System.Drawing.Size(0, 0)
        '
        'item8
        '
        Me.item8.AllowHotTrack = False
        Me.item8.CustomizationFormText = "item8"
        Me.item8.Location = New System.Drawing.Point(85, 132)
        Me.item8.Name = "item8"
        Me.item8.Size = New System.Drawing.Size(39, 22)
        Me.item8.Spacing = New DevExpress.XtraLayout.Utils.Padding(1, 1, 1, 1)
        Me.item8.Text = "item8"
        Me.item8.TextSize = New System.Drawing.Size(0, 0)
        '
        'item2
        '
        Me.item2.AllowHotTrack = False
        Me.item2.CustomizationFormText = "item2"
        Me.item2.Location = New System.Drawing.Point(85, 154)
        Me.item2.Name = "item2"
        Me.item2.Size = New System.Drawing.Size(39, 20)
        Me.item2.Text = "item2"
        Me.item2.TextSize = New System.Drawing.Size(0, 0)
        '
        'repositoryItemImageComboBox1
        '
        Me.repositoryItemImageComboBox1.AutoHeight = False
        Me.repositoryItemImageComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1"
        '
        'EUS_CustomerPhotosBindingSource
        '
        Me.EUS_CustomerPhotosBindingSource.DataMember = "EUS_CustomerPhotos"
        Me.EUS_CustomerPhotosBindingSource.DataSource = Me.DSMembers
        '
        'DSMembers
        '
        Me.DSMembers.DataSetName = "DSMembers"
        Me.DSMembers.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1, Me.Bar3, Me.Bar2})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageList1
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItemApprovePhoto, Me.BarButtonItemDeclinePhoto, Me.BarButtonItemAllPhotos, Me.BarButtonItemNewPhotos, Me.BarButtonItemRejected, Me.BarButtonItemApproved, Me.BarButtonItemAutoApproved, Me.BarButtonItemRefresh, Me.BarButtonItemDeletedPhotos, Me.BarButtonItemEditProfile, Me.BarEditItemDateFrom, Me.BarEditItemDateTo, Me.BarButtonItemDeletePhoto})
        Me.BarManager1.MaxItemId = 14
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemMonth1, Me.RepositoryItemDateEditTo, Me.RepositoryItemDateEditFrom})
        Me.BarManager1.StatusBar = Me.Bar3
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemAllPhotos), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemNewPhotos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemRejected, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemApproved, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemAutoApproved, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemDeletedPhotos, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemRefresh, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemDeletePhoto, True)})
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'BarButtonItemAllPhotos
        '
        Me.BarButtonItemAllPhotos.Caption = "All"
        Me.BarButtonItemAllPhotos.Id = 2
        Me.BarButtonItemAllPhotos.ImageIndex = 59
        Me.BarButtonItemAllPhotos.Name = "BarButtonItemAllPhotos"
        Me.BarButtonItemAllPhotos.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemNewPhotos
        '
        Me.BarButtonItemNewPhotos.Caption = "New"
        Me.BarButtonItemNewPhotos.Id = 3
        Me.BarButtonItemNewPhotos.ImageIndex = 45
        Me.BarButtonItemNewPhotos.Name = "BarButtonItemNewPhotos"
        Me.BarButtonItemNewPhotos.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemRejected
        '
        Me.BarButtonItemRejected.Caption = "Rejected"
        Me.BarButtonItemRejected.Id = 4
        Me.BarButtonItemRejected.ImageIndex = 48
        Me.BarButtonItemRejected.Name = "BarButtonItemRejected"
        Me.BarButtonItemRejected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemApproved
        '
        Me.BarButtonItemApproved.Caption = "Approved"
        Me.BarButtonItemApproved.Id = 5
        Me.BarButtonItemApproved.ImageIndex = 42
        Me.BarButtonItemApproved.Name = "BarButtonItemApproved"
        Me.BarButtonItemApproved.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemAutoApproved
        '
        Me.BarButtonItemAutoApproved.Caption = "Auto Approved"
        Me.BarButtonItemAutoApproved.Id = 6
        Me.BarButtonItemAutoApproved.ImageIndex = 39
        Me.BarButtonItemAutoApproved.Name = "BarButtonItemAutoApproved"
        Me.BarButtonItemAutoApproved.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemDeletedPhotos
        '
        Me.BarButtonItemDeletedPhotos.Caption = "Deleted"
        Me.BarButtonItemDeletedPhotos.Id = 8
        Me.BarButtonItemDeletedPhotos.Name = "BarButtonItemDeletedPhotos"
        '
        'BarButtonItemRefresh
        '
        Me.BarButtonItemRefresh.Caption = "Refresh"
        Me.BarButtonItemRefresh.Id = 7
        Me.BarButtonItemRefresh.ImageIndex = 109
        Me.BarButtonItemRefresh.Name = "BarButtonItemRefresh"
        Me.BarButtonItemRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'Bar3
        '
        Me.Bar3.BarName = "Status bar"
        Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar3.DockCol = 0
        Me.Bar3.DockRow = 0
        Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar3.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemDeclinePhoto), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemApprovePhoto), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemEditProfile)})
        Me.Bar3.OptionsBar.AllowQuickCustomization = False
        Me.Bar3.OptionsBar.DrawDragBorder = False
        Me.Bar3.OptionsBar.UseWholeRow = True
        Me.Bar3.Text = "Status bar"
        '
        'BarButtonItemDeclinePhoto
        '
        Me.BarButtonItemDeclinePhoto.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left
        Me.BarButtonItemDeclinePhoto.Caption = "Decline Photo"
        Me.BarButtonItemDeclinePhoto.Id = 1
        Me.BarButtonItemDeclinePhoto.ImageIndex = 279
        Me.BarButtonItemDeclinePhoto.Name = "BarButtonItemDeclinePhoto"
        Me.BarButtonItemDeclinePhoto.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemApprovePhoto
        '
        Me.BarButtonItemApprovePhoto.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BarButtonItemApprovePhoto.Caption = "Approve Photo"
        Me.BarButtonItemApprovePhoto.Id = 0
        Me.BarButtonItemApprovePhoto.ImageIndex = 107
        Me.BarButtonItemApprovePhoto.Name = "BarButtonItemApprovePhoto"
        Me.BarButtonItemApprovePhoto.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemEditProfile
        '
        Me.BarButtonItemEditProfile.Caption = "Edit Profile"
        Me.BarButtonItemEditProfile.Id = 10
        Me.BarButtonItemEditProfile.Name = "BarButtonItemEditProfile"
        '
        'Bar2
        '
        Me.Bar2.BarName = "Custom 4"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 1
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarEditItemDateFrom), New DevExpress.XtraBars.LinkPersistInfo(Me.BarEditItemDateTo)})
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Custom 4"
        '
        'BarEditItemDateFrom
        '
        Me.BarEditItemDateFrom.Caption = "From Date:"
        Me.BarEditItemDateFrom.Edit = Me.RepositoryItemDateEditFrom
        Me.BarEditItemDateFrom.Id = 11
        Me.BarEditItemDateFrom.Name = "BarEditItemDateFrom"
        Me.BarEditItemDateFrom.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        Me.BarEditItemDateFrom.Width = 115
        '
        'RepositoryItemDateEditFrom
        '
        Me.RepositoryItemDateEditFrom.AutoHeight = False
        Me.RepositoryItemDateEditFrom.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEditFrom.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.RepositoryItemDateEditFrom.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEditFrom.EditFormat.FormatString = "dd/MM/yyyy"
        Me.RepositoryItemDateEditFrom.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEditFrom.Mask.EditMask = ""
        Me.RepositoryItemDateEditFrom.Name = "RepositoryItemDateEditFrom"
        Me.RepositoryItemDateEditFrom.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        '
        'BarEditItemDateTo
        '
        Me.BarEditItemDateTo.Caption = "To Date:"
        Me.BarEditItemDateTo.Edit = Me.RepositoryItemDateEditTo
        Me.BarEditItemDateTo.Id = 12
        Me.BarEditItemDateTo.Name = "BarEditItemDateTo"
        Me.BarEditItemDateTo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        Me.BarEditItemDateTo.Width = 106
        '
        'RepositoryItemDateEditTo
        '
        Me.RepositoryItemDateEditTo.AutoHeight = False
        Me.RepositoryItemDateEditTo.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEditTo.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.RepositoryItemDateEditTo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEditTo.EditFormat.FormatString = "dd/MM/yyyy"
        Me.RepositoryItemDateEditTo.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEditTo.Mask.EditMask = ""
        Me.RepositoryItemDateEditTo.Name = "RepositoryItemDateEditTo"
        Me.RepositoryItemDateEditTo.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(960, 68)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 565)
        Me.barDockControlBottom.Size = New System.Drawing.Size(960, 35)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 68)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 497)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(960, 68)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 497)
        '
        'RepositoryItemMonth1
        '
        Me.RepositoryItemMonth1.AutoHeight = False
        Me.RepositoryItemMonth1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemMonth1.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.RepositoryItemMonth1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemMonth1.Name = "RepositoryItemMonth1"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.gcPhotos)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 68)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(960, 497)
        Me.PanelControl1.TabIndex = 257
        '
        'BarButtonItemDeletePhoto
        '
        Me.BarButtonItemDeletePhoto.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BarButtonItemDeletePhoto.Caption = "Delete Photo"
        Me.BarButtonItemDeletePhoto.Id = 13
        Me.BarButtonItemDeletePhoto.Name = "BarButtonItemDeletePhoto"
        '
        'ucLayout3D
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "ucLayout3D"
        Me.Size = New System.Drawing.Size(960, 600)
        CType(Me.repositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcPhotos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_LISTS_PhotosDisplayLevelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewPhotos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colCaption, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colCustomerPhotosID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colCustomerID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colDateTimeToUploading, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colFileName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repositoryItemICBDisplayLevel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colDisplayLevel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colHasAproved, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repositoryItemCheckEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colHasDeclined, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colCheckedContextID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repositoryItemCheckEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colIsDefault, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repositoryItemPictureEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colPhoto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repositoryItemTextEditPhotoURL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colPhotoURL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colDimentions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colSize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repositoryItemButtonEditCopyPhotoURL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colCopyPhotoURL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit_IsDeleted, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colIsDeleted, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit_IsAutoApproved, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutViewField_colIsAutoApproved, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutViewCard1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.item4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.item3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.item1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.item6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.item7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.item8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.item2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repositoryItemImageComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_CustomerPhotosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSMembers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEditFrom.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEditFrom, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEditTo.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEditTo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMonth1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents ImageList1 As System.Windows.Forms.ImageList
    Private WithEvents layoutViewPhotos As DevExpress.XtraGrid.Views.Layout.LayoutView
    Private WithEvents repositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Private WithEvents gcPhotos As DevExpress.XtraGrid.GridControl
    Private WithEvents repositoryItemPictureEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit
    Private WithEvents colCaption As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colPhoto As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colCustomerPhotosID As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colCustomerID As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colDateTimeToUploading As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colFileName As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colDisplayLevel As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colHasAproved As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colHasDeclined As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colCheckedContextID As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colIsDefault As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colPhotoURL As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colDimentions As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colSize As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents colType As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents repositoryItemICBDisplayLevel As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
    Private WithEvents repositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Private WithEvents repositoryItemCheckEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Private WithEvents repositoryItemCheckEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Private WithEvents repositoryItemImageComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
    Private WithEvents EUS_CustomerPhotosBindingSource As System.Windows.Forms.BindingSource
    Private WithEvents DSMembers As Dating.Client.Admin.APP.AdminWS.DSMembers
    Private WithEvents repositoryItemTextEditPhotoURL As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colCopyPhotoURL As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Private WithEvents repositoryItemButtonEditCopyPhotoURL As DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit
    Friend WithEvents EUS_LISTS_PhotosDisplayLevelBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DSLists As Dating.Client.Admin.APP.AdminWS.DSLists
    Friend WithEvents layoutViewField_colCaption As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colCustomerPhotosID As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colCustomerID As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colDateTimeToUploading As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colFileName As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colDisplayLevel As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colHasAproved As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colHasDeclined As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colCheckedContextID As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colIsDefault As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colPhoto As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colPhotoURL As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colDimentions As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colSize As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colType As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents layoutViewField_colCopyPhotoURL As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents colIsDeleted As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Friend WithEvents RepositoryItemCheckEdit_IsDeleted As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents layoutViewField_colIsDeleted As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents colIsAutoApproved As DevExpress.XtraGrid.Columns.LayoutViewColumn
    Friend WithEvents RepositoryItemCheckEdit_IsAutoApproved As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents layoutViewField_colIsAutoApproved As DevExpress.XtraGrid.Views.Layout.LayoutViewField
    Friend WithEvents LayoutViewCard1 As DevExpress.XtraGrid.Views.Layout.LayoutViewCard
    Friend WithEvents item4 As DevExpress.XtraLayout.SimpleSeparator
    Friend WithEvents item3 As DevExpress.XtraLayout.SimpleSeparator
    Friend WithEvents item1 As DevExpress.XtraLayout.SimpleSeparator
    Friend WithEvents item6 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents item7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents item8 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents item2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
    Friend WithEvents BarButtonItemApprovePhoto As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemDeclinePhoto As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarButtonItemAllPhotos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemNewPhotos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemRejected As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemApproved As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemAutoApproved As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemRefresh As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemDeletedPhotos As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemEditProfile As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents BarEditItemDateFrom As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemMonth1 As DevExpress.XtraScheduler.UI.RepositoryItemMonth
    Friend WithEvents BarEditItemDateTo As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemDateEditTo As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents RepositoryItemDateEditFrom As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents BarButtonItemDeletePhoto As DevExpress.XtraBars.BarButtonItem

End Class

﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("C.A.S Central Administrate System Support Services")> 
<Assembly: AssemblyDescription("Central Administrate System Support Services for my Web Bussines")> 
<Assembly: AssemblyCompany("Dating-Deals.com")> 
<Assembly: AssemblyProduct("C.A.S Central Administrate System Support Services")> 
<Assembly: AssemblyCopyright("Copyright © Dating-Deals.com 2011-2014")> 
<Assembly: AssemblyTrademark("C.A.S Central Administrate System Support Services")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("ac3be45a-0448-468f-8986-636dc5aa73da")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("15.02.12.0")> 
<Assembly: AssemblyFileVersion("15.02.12.0")> 

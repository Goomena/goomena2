﻿Imports System.Net
Imports System.IO
Imports Dating.Server.Core.DLL
Imports Dating.Server.Datasets.DLL

Module Globals

    Public RegistryAPP As String = "Dating.Server.Support.APP"

    Public gER As New Library.Public.clsErrors(True, False)
    Public WithEvents gApplicationTAG As New License.Client.Core.clsApplicationTAG()

    '  Public gExitNow As Boolean
    '  Public gSending As Boolean

    ' Public gUserID As Integer
    Public gHardwareID As String = ""
    ' Public gClearToken As String

    ' Public gLAG As New Library.Public.clsLAGLocalization
    ' Public gLOG As New Library.Public.ClsLOG(RegistryAPP, True)

    ' Public m_ADODBConnectionString As String
    Public RegBase As String = "HKEY_CURRENT_USER\Software\" & My.Application.Info.AssemblyName



    '  Public m_UserRole As String = "Admin"

    'Public gAdminWS As New AdminWS.Admin()
    Public gRecordLogin As New clsDataRecordLogin
    ' Public gWorkingSiteName As String
    Public gLoginRec As clsDataRecordLoginReturn

    'Public gAdminWSHeaders As New AdminWS.clsWSHeaders()


    'Public ReadOnly Property gMemberPhotoURL As String
    '    Get
    '        Dim uri As New Uri(gAdminWS.Url)
    '        Return "http://" & uri.Host & "/photos/{0}/{1}"
    '    End Get
    'End Property


    'Public ReadOnly Property gSiteURL As String
    '    Get
    '        Dim uri As New Uri(gAdminWS.Url)
    '        Return "http://" & uri.Host & "/"
    '    End Get
    'End Property



    Public Sub ErrorMsgBox(ex As Exception, message As String)
        If (ex.Message.Contains("Server did not recognize the value of HTTP Header SOAPAction: ")) Then
            gER.ErrorMsgBox(ex, vbCrLf & vbCrLf & "Web services definition is outdated." & vbCrLf & "An updated version of web services are required." & vbCrLf & vbCrLf & message & vbCrLf & vbCrLf & ex.ToString())
        Else
            gER.ErrorMsgBox(ex, message & vbCrLf & vbCrLf & ex.ToString())
        End If
    End Sub

    Public Sub ErrorMsgBox(message As String)
        gER.ErrorMsgBox(message)
    End Sub


    Public Function FormatFileSize(ByVal FileSizeBytes As Long) As String

        Dim Resp As String = Server.Core.DLL.AppUtils.FormatFileSize(FileSizeBytes)
        Return Resp
    End Function


    Public Function GetWebFile(urlDownloadFile As String) As String
        Dim localFilePath As String = ""
        Try
            localFilePath = Server.Core.DLL.RemotePost.GetWebFile(urlDownloadFile)
        Catch ex As Exception
            If (Not ex.Message.Contains("(404) Not Found")) Then
                ErrorMsgBox(ex, "")
            End If
        End Try

        Return localFilePath
    End Function



    Public Sub Relogin()
        Try
            frmCASLogin.ShowDialog()
        Catch ex As Exception

        End Try
    End Sub


    Public Sub PerformLogin(LoginName As String, Password As String)

        gRecordLogin.LoginName = LoginName
        gRecordLogin.Password = Password
        gRecordLogin.HardwareID = gHardwareID

        'gAdminWSHeaders.LoginName = LoginName
        'gAdminWSHeaders.Password = Password
        'gAdminWSHeaders.HardwareID = gHardwareID

        gLoginRec = PerformLogin(gRecordLogin)

        If gLoginRec.HasErrors Or Not gLoginRec.IsValid Then
            MsgBox(gLoginRec.Message)
        End If
    End Sub


    'Public Sub PergormReLogin()

    '    gLoginRec = gAdminWS.Login(gAdminWSHeaders, gAdminWS_RecordLogin)

    '    If gLoginRec.HasErrors Then
    '        Relogin()
    '    End If
    'End Sub



    Private Function PerformLogin(DataRecordLogin As clsDataRecordLogin) As clsDataRecordLoginReturn
        Dim DataRecordLoginMemberReturn As New clsDataRecordLoginReturn
        DataRecordLoginMemberReturn.IsValid = False
        DataRecordLoginMemberReturn.HasErrors = False

        Try
            Using SYS_UsersDataTable As New DSSYSData.SYS_UsersDataTable
                Using ta As New DSSYSDataTableAdapters.SYS_UsersTableAdapter
                    ta.FillBy_SYS_Users_GetUsers(SYS_UsersDataTable, Nothing, DataRecordLogin.LoginName, DataRecordLogin.Password)
                    If SYS_UsersDataTable.Rows.Count > 0 Then

                        Dim rowIndex As Integer = 0

                        While rowIndex < SYS_UsersDataTable.Rows.Count
                            Dim SYS_UsersRow As DSSYSData.SYS_UsersRow = SYS_UsersDataTable.Rows(rowIndex)

                            If SYS_UsersRow.IsActive Then
                                Dim Token As String = Library.Public.clsHash.ComputeHash(DataRecordLogin.LoginName & "^Token$$$", "SHA1") 'DataRecordLogin.HardwareID & "^^" &

                                DataRecordLoginMemberReturn.UserID = SYS_UsersRow.UsersID
                                DataRecordLoginMemberReturn.UserRole = SYS_UsersRow.UserRole
                                DataRecordLoginMemberReturn.LoginName = SYS_UsersRow.LoginName

                                DataRecordLoginMemberReturn.Token = Token
                                DataRecordLoginMemberReturn.HasErrors = False
                                DataRecordLoginMemberReturn.Message = "OK"
                                DataRecordLoginMemberReturn.IsValid = True
                            Else
                                DataRecordLoginMemberReturn.HasErrors = True
                                DataRecordLoginMemberReturn.Message = "The user is not Active"
                            End If

                            rowIndex += 1
                        End While
                    Else
                        DataRecordLoginMemberReturn.HasErrors = True
                        DataRecordLoginMemberReturn.Message = "Error Login Or Password"
                    End If
                End Using
            End Using
          

            
        Catch ex As Exception
            DataRecordLoginMemberReturn.HasErrors = True
            DataRecordLoginMemberReturn.Message = ex.Message
        End Try
        Return DataRecordLoginMemberReturn
    End Function

    'Public Sub PergormReLogin()

    '    gLoginRec = gAdminWS.Login(gAdminWSHeaders, gAdminWS_RecordLogin)

    '    If gLoginRec.HasErrors Then
    '        Relogin()
    '    End If
    'End Sub





    'Public Function CheckWebServiceCallResult(webResult As AdminWS.clsDataRecordErrorsReturn) As AdminWS.clsDataRecordErrorsReturn
    '    If webResult.HasErrors Then
    '        If webResult.ErrorCode = 999 Then
    '            Relogin()
    '        Else
    '            MsgBox("WS Message:" & vbCrLf & webResult.Message)
    '        End If
    '    End If

    '    Return webResult
    'End Function


    ' Dim tmrWait As System.Timers.Timer
    Dim fwWait As New frmWaitProccess

    Public Sub ShowWaitWin(text As String, Optional ShowCancelCmd As Boolean = False)
        Try
            'If (tmrWait Is Nothing) Then
            '    tmrWait = New System.Timers.Timer()
            '    tmrWait.Interval = 500
            '    AddHandler tmrWait.Elapsed, AddressOf tmrWait_Tick
            'End If

            'fw = New frmWaitProccess
            fwWait.Text = text
            fwWait.CmdCancel.Visible = ShowCancelCmd
            If (Not fwWait.Visible) Then
                fwWait.ProgressBar.Properties.Maximum = 3
                fwWait.Show()
                Application.DoEvents()
                'tmrWait.Enabled = True
                'tmrWait.Start()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ShowWaitWin(text As String, position As Integer, Optional ShowCancelCmd As Boolean = False)
        Try
            fwWait.Text = text
            fwWait.CmdCancel.Visible = ShowCancelCmd
            fwWait.ProgressBar.Position = position
            If (Not fwWait.Visible) Then
                fwWait.ProgressBar.Properties.Maximum = 100
                fwWait.Show()
                Application.DoEvents()
                'tmrWait.Enabled = True
                'tmrWait.Start()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub HideWaitWin()
        Try
            fwWait.Hide()
            'InvokeWaitHide()

        Catch ex As Exception

        End Try
    End Sub



    Public Function GetFileName(fileName As String) As String
        If (Not String.IsNullOrEmpty(fileName)) Then
            Dim chars As Char() = fileName.ToCharArray()
            Dim cnt As Integer = 0

            While cnt < chars.Length
                Select Case chars(cnt)
                    Case "/"c, "\"c, "*"c, ":"c, "?"c, "<"c, ">"c, """"c, "!"c, " "c, "'"c, "("c, ")"c, "["c, "]"c, "."c, ";"c, ","c, "~"c
                        chars(cnt) = "_"c
                End Select

                cnt += 1
            End While
            fileName = New String(chars)
        End If


        Return fileName
    End Function


    Public Sub ReadSMTPSettings()
        Dim config As New clsConfigValues


        clsMyMail.MySMTPSettings.gSMTPOutPort = config.gSMTPOutPort
        clsMyMail.MySMTPSettings.gSMTPServerName = config.gSMTPServerName
        clsMyMail.MySMTPSettings.gSMTPLoginName = config.gSMTPLoginName
        clsMyMail.MySMTPSettings.gSMTPPassword = config.gSMTPPassword
        clsMyMail.MySMTPSettings.gSMTPenableSsl = config.gSMTPUseSSL



        clsMyMail.MySMTPSettingsSupport.gSMTPOutPort = config.gSMTPOutPortSupport
        clsMyMail.MySMTPSettingsSupport.gSMTPServerName = config.gSMTPServerNameSupport
        clsMyMail.MySMTPSettingsSupport.gSMTPLoginName = config.gSMTPLoginNameSupport
        clsMyMail.MySMTPSettingsSupport.gSMTPPassword = config.gSMTPPasswordSupport
        clsMyMail.MySMTPSettingsSupport.gSMTPenableSsl = config.gSMTPUseSSLSupport
    End Sub


End Module

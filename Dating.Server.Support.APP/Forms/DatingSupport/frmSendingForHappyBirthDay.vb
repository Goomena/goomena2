﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Text
Imports System.Linq
Imports System.Data.Linq
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient



Public Class frmSendingForHappyBirthDay

    Dim tmrTaskInterval As Integer = 21600000
    ' Dim isBusy_PerformImageComparison As Boolean
    Dim ConnectionString As String = My.Settings.AppDBconnectionString


    Private Sub frmSendingForCreditsExpiring_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GroupControl1.Text = Me.Text
        BarStaticItemStatus.Caption = "Not running"

        StartTimer()
    End Sub


    Private Sub frmSendingForCreditsExpiring_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

            bw.CancelAsync()
            bw.Dispose()
            bw = Nothing
            tmrTask.Enabled = False
            Me.Dispose()

        Catch ex As Exception
        End Try
    End Sub
    Private Class UserForHappyBirthday
        Public Property profileId As Integer
        Public Property MirrorprofileId As Integer
        Public Property LoginName As String
        Public Property LagId As String
        Public Property Email As String
        Public Property LastActivityIP As String
        Public Property Referrer As String
        Public Property CustomReferrer As String
    End Class

    Private Class UserForCheckInAdmin
        Public Property profileId As Integer
        Public Property MirrorprofileId As Integer
    End Class
    Private _credits As Integer
    Private Property credits As Integer
        Get
            Return _credits
        End Get
        Set(value As Integer)
            _credits = value
        End Set
    End Property
    Private Function AddToUsersForEmail(ByRef r As DataRow) As Boolean
        Dim re As Boolean = False
        Try
            Dim pr As New UserForHappyBirthday
            pr.profileId = r.Item("ProfileID")
            pr.MirrorprofileId = r.Item("MirrorProfileID")
            pr.LoginName = r.Item("LoginName")
            pr.LagId = r.Item("LagId")
            pr.Email = r.Item("eMail")
            pr.Referrer = r.Item("Referrer")
            pr.CustomReferrer = r.Item("CustomReferrer")
            pr.LastActivityIP = r.Item("LastActivityIP")
            If (credits > 0) Then
                Try
                    clsProfilesPrivacySettings.Update_LastBirthdayBonusSendToNow(pr.profileId)
                    'TransactionTypeID: 4 = Bonus CustomerTransactionTypeID
                    AddMessage("Free Happy Birthday credits to " & pr.LoginName)
                    clsCustomer.AddTransaction("Add free HappyBirthday bonus credits",
                                               0,
                                                "Free Happy Birthday credits " & credits,
                                                "",
                                                credits,
                                               pr.LastActivityIP,
                                                "Google App",
                                                pr.Referrer,
                                                 pr.profileId,
                                                pr.CustomReferrer,
                                                pr.Email,
                                                PaymentMethods.None,
                                                "HappyBirthday_GIFT",
                                                4,
                                                Nothing,
                                                "EUR",
                                                New clsDataRecordIPN(),
                                                0,
                                                False,
                                                45,
                                                0)


                    Try
                        AddMessage("Sending Happy Birthday email to " & pr.LoginName)
                        SendEmail(pr)
                    Catch ex As Exception
                    End Try
                Catch ex As Exception
                End Try
            End If
        Catch ex As Exception

        End Try
        Return re
    End Function
    Private Function SendEmail(ByVal User As UserForHappyBirthday) As Boolean
        Dim re As Boolean = False
        Try
            Dim subject As String = ""
            Dim body As String = ""

            Dim lagid As String = "US"
            If (Not String.IsNullOrEmpty(User.LagId)) Then lagid = User.LagId
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_HappyBirthDaySubject", ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "NotificationEmail_HappyBirthDayText", ConnectionString)
            body = body.Replace("###LOGINNAME###", User.LoginName)
            body = body.Replace("###Credits###", credits)
            Try
                clsUserDoes.SendMessageFromAdministrator(subject, body, User.profileId)
            Catch ex As Exception

            End Try

            clsMyMail.SendMailFromQueue(User.Email, subject, body, User.profileId, "NotificationType.HappyBirthDay", True, Nothing, Nothing)
            re = True

        Catch ex As Exception
            re = False
        End Try

        Return re
    End Function
    Private Function AddToUsersForCheck(ByRef r As DataRow) As Boolean
        Dim re As Boolean = False
        Try
            Dim pr As New UserForCheckInAdmin
            pr.profileId = r.Item("ProfileID")
            pr.MirrorprofileId = r.Item("MirrorProfileID")
            AddMessage("Added to list for Check from Admin: " & r.Item("LoginName"))
            clsProfilesPrivacySettings.Update_Eus_BirthdaysForCheck(pr.profileId)
        Catch ex As Exception
        End Try
        Return re
    End Function

    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork




        '  Dim UsersForEmail As New List(Of UserForHappyBirthday)
        '  Dim UsersForCheck As New List(Of UserForCheckInAdmin)
        credits = clsConfigValues.GetSYS_ConfigValueDouble2("HappyBirthdayBonus")
        Try
            credits = clsConfigValues.GetSYS_ConfigValueDouble2("HappyBirthdayBonus")
            If credits > 0 Then
                Dim sql As String = <sql><![CDATA[
            declare @dtAct datetime=dateadd(month,-3,GETUTCDATE())
            select p.*
            from(
            				select pr.ProfileID,
            				pr.MirrorProfileID,
            				pr.LoginName,
            				pr.DateTimeToRegister,
                            pr.[eMail],
            				pr.LAGID,
                            isnull(pr.[Referrer],'') as Referrer,
                            isnull(pr.[CustomReferrer],'') as CustomReferrer,
            				pr.Birthday,
                            isnull(pr.LastActivityIP,'') as LastActivityIP,

            				isnull(pr.AvailableCredits,0) as AvailableCredits  ,
            				HasPhoto =isnull((SELECT count([CustomerPhotosID])
            										 FROM [dbo].[EUS_CustomerPhotos] with (nolock)
            										 where ([CustomerID]=pr.MirrorProfileID or [CustomerID]=pr.ProfileID)
            										 and [HasAproved]=1 and [IsDeleted]=0),0),
            				HasSubscription = CAST((case  when  exists (
            															select top 1 CustomerCreditsId  
            															from EUS_CustomerCredits as cc  with (nolock) 
            															where (cc.CustomerId = pr.ProfileID or cc.CustomerId = pr.MirrorProfileID) and 
            																cc.IsSubscription=1 and cc.DateTimeExpiration>getutcdate()) 
            									 then 1
            									 else 0
            									  end) as bit),
            				LastTimeAccessed = isnull(ps.AccessedForHappyBirthday,dateadd(year,-2,GETUTCDATE())),
            				LastTimeSent = isnull(ps.SendedHappyBirthdayBonus ,dateadd(year,-2,GETUTCDATE()))


            				from EUS_Profiles  as pr with (nolock) 
            				left join [dbo].[EUS_ProfilesPrivacySettings] as ps with (nolock) 
            				on ps.[ProfileID]=pr.ProfileID or ps.[MirrorProfileID]=pr.MirrorProfileID
            				where datepart(mm,pr.birthday)=@Month and 
            						datepart(dd,pr.birthday) =@day and
            						((pr.LastActivityDateTime>=@dtAct or pr.LastActivityMobileDateTime>=@dtAct) and 
            						pr.IsMaster=1 and 
            						pr.GenderId=1 and 
            						(pr.Status=4 or pr.Status=2)

            						)) as p
            where DATEPART(yyyy,p.LastTimeAccessed)<DATEPART(yyyy,GETUTCDATE()) and DATEPART(yyyy,p.LastTimeSent)<DATEPART(yyyy,GETUTCDATE())
            ]]></sql>.Value
                Using con As New Data.SqlClient.SqlConnection(My.Settings.AppDBconnectionString)

                    Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                        cmd.Parameters.AddWithValue("@Month", Date.UtcNow.Month)
                        cmd.Parameters.AddWithValue("@day", Date.UtcNow.Day)
                        Using result = DataHelpers.GetDataTable(cmd)


                            If result.Rows.Count > 0 Then
                                For Each r As DataRow In result.Rows
                                    If bw.CancellationPending Then
                                        Exit For
                                    End If
                                    If r.Item("AvailableCredits") > 0 Or CBool(r.Item("HasSubscription")) Then
                                        AddToUsersForEmail(r)
                                    ElseIf r.Item("HasPhoto") > 0 And DateDiff(DateInterval.Day, CDate(r.Item("DateTimeToRegister")), Date.UtcNow) > 30 Then
                                        AddToUsersForEmail(r)
                                    Else
                                        AddToUsersForCheck(r)
                                    End If

                                Next
                            End If
                        End Using
                    End Using
                End Using
            End If





        Catch ex As Exception
            Throw
        Finally

        End Try


    End Sub

    Private Sub AddMessage(message As String)
        'txOutput.Text = "[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf & txOutput.Text
        If (message IsNot Nothing) Then
            Try
                If Me.InvokeRequired Then
                    ' calls itself, but on correct thread
                    Me.BeginInvoke(New Action(Of Object)(AddressOf txtOutput.MaskBox.AppendText), New Object() {"[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf})
                Else
                    txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf)
                End If
            Catch ex As InvalidOperationException
            End Try
        End If
        Try
            If (txtOutput.Lines.Length > 5001) Then
                Dim index As Integer = 0
                Dim linesCount As Integer = txtOutput.Lines.Length
                Dim txtOutputText As String = txtOutput.Text
                For cnt = 0 To linesCount - 5000
                    index = txtOutputText.IndexOf(vbCr, index) + vbCr.Length
                Next
                If (index > 0) Then
                    txtOutput.Text = txtOutput.Text.Remove(0, index).TrimStart({ControlChars.Cr, ControlChars.Lf})
                End If
            End If
        Catch
        End Try
    End Sub


    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        If (e.ProgressPercentage = -1) Then
            Me.BarStaticItemStatus.Caption = Mid(CStr(e.UserState), 1, 200) & "..."
        Else
            AddMessage(CStr(e.UserState))
        End If
    End Sub


    Private Sub TryAddMessage(progress As Integer, message As String)
        Try
            bw.ReportProgress(progress, message)
        Catch ex As InvalidOperationException
            AddMessage(message)
        Catch ex As Exception
        End Try
    End Sub


    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then
            Me.BarStaticItemStatus.Caption = "Canceled"
            tmrTask.Enabled = False
        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub



    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub

    Private Sub BarButtonItemRun_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRun.ItemClick
        Try
            tmrTask.Enabled = False

            If Not bw.IsBusy = True Then
                BarStaticItemStatus.Caption = "Running"
                bw.RunWorkerAsync()
                BarButtonItemRun.Enabled = False
            Else
                Try
                    bw.CancelAsync()
                    bw.RunWorkerAsync()
                    BarStaticItemStatus.Caption = "Running"
                Catch ex As Exception
                    AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick -> bw.IsBusy" & vbCrLf & ex.Message)
                End Try
            End If

        Catch ex As Exception
            AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick" & vbCrLf & ex.Message)
        Finally
            tmrTask.Enabled = True
            tmrTask.Interval = tmrTaskInterval
        End Try
    End Sub


    Private Sub BarButtonItemCancelJob_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCancelJob.ItemClick
        bw.CancelAsync()
        tmrTask.Enabled = False
        BarButtonItemRun.Enabled = True
    End Sub

    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        If Not bw.IsBusy = True Then
            BarStaticItemStatus.Caption = "Running"
            bw.RunWorkerAsync()
            BarButtonItemRun.Enabled = False
        End If
        tmrTask.Interval = tmrTaskInterval
    End Sub


    Private Sub StartTimer()
        Dim r As New Random(System.DateTime.Now.Millisecond)
        Dim interval As Integer = r.Next(3, 6)

        If (tmrTaskInterval = 0) Then tmrTaskInterval = tmrTask.Interval
        tmrTask.Interval = interval * 1000
        tmrTask.Enabled = True
        tmrTask.Start()

        BarStaticItemStatus.Caption = "First run in " & tmrTask.Interval / 1000 & " sec"
    End Sub




    Private Sub PerformImageComparison_ProgressChanged(sender As Object, e As GoomenaImageCompare.ProgressChangedEventArgs)
        TryAddMessage(0, "Image Comparison-->" & e.CurrentUrl)
    End Sub


    Private Sub PerformImageComparison_Complete(sender As Object, e As GoomenaImageCompare.AlertEventArgs)
        Try


            Dim userState As GoogleImageCheckState = e.UserState
            Dim customerIDList As Dictionary(Of String, System.Data.DataRow) = userState.customerIDList
            Dim emailForSending As SYS_EmailsQueue = userState.emailForSending

            TryAddMessage(0, emailForSending.EmailAction & "-->Complete")
            TryAddMessage(0, emailForSending.EmailAction & "-->Similar photos found " & e.uuiListData.Count)
            Using con As New Data.SqlClient.SqlConnection(My.Settings.AppDBconnectionString)


                Using command As System.Data.SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, "")


                    ' If (customerIDList.Count = 0) Then Return


                    Try

                        If (e.uuiListData.Count > 0) Then

                            Dim uniqueInputPhotos As New List(Of String)
                            For cnt = 0 To e.uuiListData.Count - 1
                                Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = e.uuiListData(cnt)
                                If (Not uniqueInputPhotos.Contains(itm.InputUrl)) Then
                                    uniqueInputPhotos.Add(itm.InputUrl)
                                End If
                            Next


                            For cnt = 0 To uniqueInputPhotos.Count - 1

                                Dim urlPhoto As String = uniqueInputPhotos(cnt)
                                If (Not customerIDList.ContainsKey(urlPhoto)) Then Continue For
                                Dim dr As System.Data.DataRow = customerIDList(urlPhoto)
                                Dim loginName As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(dr("CustomerID"))

                                Try


                                    Dim Content_Similar As String = <h><![CDATA[
<div>Similar [SIMILARITY]%</div>
<div><img src="[PHOTO-URL]"  style="width:250px" width="250"/></div>
<div><a href="[PHOTO-URL]" target="_blank">[PHOTO-URL]</a></div>
<br/> 
<br/>
]]></h>

                                    Dim htmlTable As New StringBuilder()
                                    '  Dim prevLogin As String = ""

                                    Dim itemsSimilar As List(Of GoomenaImageCompare.uuiSimilarUrlItem) = (From f In e.uuiListData Order By f.Similarity Descending).ToList()
                                    For f1 = 0 To itemsSimilar.Count - 1
                                        Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = itemsSimilar(f1)
                                        If (itm.InputUrl = urlPhoto) Then
                                            Dim simPhoto As String = Content_Similar.Replace("[PHOTO-URL]", itm.FoundUrl)
                                            simPhoto = simPhoto.Replace("[SIMILARITY]", Math.Round(itm.Similarity * 100, 2))
                                            htmlTable.AppendLine(simPhoto)
                                        End If
                                    Next

                                    htmlTable.Insert(0, emailForSending.Body & vbCrLf & "<br><br>" & vbCrLf)
                                    htmlTable.Insert(0, "<b>Similar photos (" & itemsSimilar.Count & ") found!</b><br>" & vbCrLf)
                                    htmlTable.Insert(0, "<html><head>" & "</head><body>")
                                    htmlTable.AppendLine("</body></html>")

                                    emailForSending.Body = htmlTable.ToString()
                                    emailForSending.Subject = emailForSending.Subject & " -- Similar photos (" & itemsSimilar.Count & ")"
                                    TryAddMessage(0, emailForSending.EmailAction & "-->Sending message for " & loginName & ", email action " & emailForSending.EmailAction)
                                    'clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Found similar images", Content, True)
                                Catch
                                End Try
                            Next



                            Using _CMSDBDataContext As New CMSDBDataContext(command.Connection.ConnectionString)


                                Try

                                    For cnt = 0 To e.uuiListData.Count - 1
                                        Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = e.uuiListData(cnt)
                                        If (Not customerIDList.ContainsKey(itm.InputUrl)) Then Continue For
                                        Dim dr As System.Data.DataRow = customerIDList(itm.InputUrl)

                                        Dim ps As New EUS_CustomerPhotosSimilar()
                                        ps.CustomerID = dr("CustomerID")
                                        ps.CustomerPhotosID = dr("CustomerPhotosID")
                                        ps.DateTimeCreated = Date.UtcNow
                                        ps.PhotoSimilarity = itm.Similarity
                                        ps.PhotoURL = itm.FoundUrl
                                        _CMSDBDataContext.EUS_CustomerPhotosSimilars.InsertOnSubmit(ps)

                                    Next

                                    _CMSDBDataContext.SubmitChanges()
                                Catch ex As Exception
                                    TryAddMessage(0, emailForSending.EmailAction & "-->" & ex.ToString())
                                    Throw New System.Exception(ex.Message, ex)
                                End Try
                            End Using
                            TryAddMessage(0, emailForSending.EmailAction & "-->EUS_CustomerPhotosSimilars updated")
                        End If

                        If (customerIDList.Count > 0) Then

                            Dim sb As New System.Text.StringBuilder()
                            For cnt = 0 To e.uuiListData.Count - 1
                                Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = e.uuiListData(cnt)

                                If (customerIDList.ContainsKey(itm.InputUrl)) Then
                                    Dim dr As System.Data.DataRow = customerIDList(itm.InputUrl)
                                    Dim CustomerPhotosID As Integer = dr("CustomerPhotosID")

                                    Dim sql As String = "update EUS_CustomerPhotos set PhotoSimilarity=@PhotoSimilarity[COUNT] where CustomerPhotosID=@CustomerPhotosID[COUNT];"
                                    sql = sql.Replace("[COUNT]", cnt)
                                    sb.AppendLine(sql)
                                    command.Parameters.AddWithValue("@PhotoSimilarity" & cnt, 1)
                                    command.Parameters.AddWithValue("@CustomerPhotosID" & cnt, CustomerPhotosID)
                                    customerIDList.Remove(itm.InputUrl)
                                End If
                            Next


                            Dim keys As String() = New String(customerIDList.Keys.Count - 1) {}
                            customerIDList.Keys.CopyTo(keys, 0)
                            For cnt = 0 To keys.Length - 1
                                Dim dr As System.Data.DataRow = customerIDList(keys(cnt))
                                Dim CustomerPhotosID As Integer = dr("CustomerPhotosID")

                                Dim index As Integer = e.uuiListData.Count + cnt + 10
                                Dim sql As String = "update EUS_CustomerPhotos set PhotoSimilarity=@PhotoSimilarity[COUNT] where CustomerPhotosID=@CustomerPhotosID[COUNT];"
                                sql = sql.Replace("[COUNT]", index)
                                sb.AppendLine(sql)
                                command.Parameters.AddWithValue("@PhotoSimilarity" & index, 0)
                                command.Parameters.AddWithValue("@CustomerPhotosID" & index, CustomerPhotosID)
                            Next

                            command.CommandText = sb.ToString()
                            DataHelpers.ExecuteNonQuery(command)
                            TryAddMessage(0, emailForSending.EmailAction & "-->Database updated")
                        End If

                    Catch ex As Exception
                        emailForSending.Exception = ex.ToString()
                        TryAddMessage(0, " Exc. " & ex.Message)
                        Try
                            Dim str As String = ""
                            str = "EmailTo: " & emailForSending.EmailTo & vbCrLf & _
                                 "Subject: " & emailForSending.Subject & vbCrLf & _
                                 "Email Action: " & emailForSending.EmailAction & vbCrLf & vbCrLf & _
                                 "Exception : " & emailForSending.Exception
                            clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Sending Emails Exception: " & ex.Message, str, False)
                        Catch
                        End Try
                    Finally

                        Using hdl As New clsSendingEmailsHandler()
                            Try
                                SendEmailInQueue(emailForSending)
                                clsSendingEmailsHandler.SetEmailSent(emailForSending)
                            Catch ex As Exception


                            End Try
                        End Using


                    End Try
                End Using
            End Using

        Catch ex As Exception
            TryAddMessage(0, " Exc. " & ex.Message)
            clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Sending Emails Exception: " & ex.Message, ex.ToString(), False)
        Finally
            '   isBusy_PerformImageComparison = False
        End Try
    End Sub


    Private Sub PerformImageComparison(CustomerPhotosID As Long, emailForSending As SYS_EmailsQueue)
        Try
            Dim DirectoryName As String = System.IO.Path.GetDirectoryName(Application.ExecutablePath)
            Dim logFile As String = System.IO.Path.Combine(DirectoryName, "ImageComparison.log")
            bw.ReportProgress(0, "Image Comparison-->log file " & vbTab & logFile)

            Dim userState As New GoogleImageCheckState()

            Try
                System.IO.File.Delete(logFile)
            Catch
            End Try

            'Dim sql As String = "EXEC [GetView_EUS_CustomerPhotos_ImageComparison] @recordsToSelect=10;"
            Dim sql As String = "EXEC [GetView_EUS_CustomerPhotos_ImageComparison] @CustomerPhotosID=" & CustomerPhotosID & ";"

            Dim urlList As New List(Of String)
            Using dt As DataTable = DataHelpers.GetDataTable(sql)



                bw.ReportProgress(0, emailForSending.EmailAction & "-->photos count:" & dt.Rows.Count)
                For cnt = 0 To dt.Rows.Count - 1
                    Dim fn As String = dt.Rows(cnt)("FileName")
                    Dim customerid As Integer = dt.Rows(cnt)("CustomerID")
                    Dim path As String = ProfileHelper.GetProfileImageURL(customerid, fn, 0, False, False)

                    urlList.Add(path)
                    userState.customerIDList.Add(path, dt.Rows(cnt))
                Next
            End Using
            bw.ReportProgress(0, emailForSending.EmailAction & "-->starting search...")

            Dim sd As New GoomenaImageCompare.ImageComparison(10000, urlList, logFile)
            AddHandler sd.CallAlert, AddressOf PerformImageComparison_Complete
            AddHandler sd.ProgressChanged, AddressOf PerformImageComparison_ProgressChanged
            userState.emailForSending = emailForSending
            sd.getResults(userState)

            '  isBusy_PerformImageComparison = True

        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        End Try
    End Sub

    Private Sub SendEmailInQueue(ByRef emailForSending As SYS_EmailsQueue)

        ' send email using specified credentials
        'Dim result As SYS_SMTPServer = Nothing
        'If (clsNullable.NullTo(emailForSending.SMTPServerCredentialsID, 0) > 0) Then
        '    result = DataHelpers.SYS_SMTPServers_GetByID(emailForSending.SMTPServerCredentialsID)
        'End If


        'If (result IsNot Nothing) Then
        '    Dim MySMTPSettings As New clsMyMail.SMTPSettings()
        '    MySMTPSettings.gSMTPOutPort = result.SMTPOutPort
        '    MySMTPSettings.gSMTPServerName = result.SMTPServerName
        '    MySMTPSettings.gSMTPLoginName = result.SMTPLoginName
        '    MySMTPSettings.gSMTPPassword = result.SMTPPassword

        '    clsMyMail.SendMail2(MySMTPSettings, result.SMTPFromEmail, emailForSending.EmailTo, emailForSending.Subject, emailForSending.Body, If(emailForSending.IsHtmlBody.HasValue, emailForSending.IsHtmlBody, True))
        '    emailForSending.DateSent = Date.UtcNow
        '    emailForSending.Exception = Nothing
        '    bw.ReportProgress(0, "sent to  " & emailForSending.EmailTo)

        'Else
        '    If (emailForSending.EmailAction.StartsWith("New.Photo.Google.Check.ID-")) Then
        '        ' send from support@goomena.com
        '        clsMyMail.SendMail2(My.Settings.gSMTPLoginName, emailForSending.EmailTo, emailForSending.Subject, emailForSending.Body, If(emailForSending.IsHtmlBody.HasValue, emailForSending.IsHtmlBody, True))
        '    Else
        '        clsMyMail.SendMail2(My.Settings.gToEmail, emailForSending.EmailTo, emailForSending.Subject, emailForSending.Body, If(emailForSending.IsHtmlBody.HasValue, emailForSending.IsHtmlBody, True))
        '    End If

        '    emailForSending.DateSent = Date.UtcNow
        '    emailForSending.Exception = Nothing
        '    TryAddMessage(0, "sent to  " & emailForSending.EmailTo)

        'End If

    End Sub


    Public Class GoogleImageCheckState
        Public Property emailForSending As SYS_EmailsQueue
        Public Property customerIDList As Dictionary(Of String, System.Data.DataRow)

        Public Sub New()
            customerIDList = New Dictionary(Of String, System.Data.DataRow)
        End Sub
    End Class

End Class
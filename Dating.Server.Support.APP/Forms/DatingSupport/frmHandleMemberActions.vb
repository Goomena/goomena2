﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Net
Imports System.IO
Imports System.Linq
Imports System.Data.Linq
Imports System.Data.SqlClient


Public Class frmHandleMemberActions

    Dim tmrTaskInterval As Integer

    Private Sub frm_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GroupControl1.Text = Me.Text
        BarStaticItemStatus.Caption = "Not running"

        StartTimer()
    End Sub


    Private Sub frm_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

            bw.CancelAsync()
            bw.Dispose()
            bw = Nothing
            tmrTask.Enabled = False
            Me.Dispose()

        Catch ex As Exception
        End Try
    End Sub


    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork
        Try

            Dim continueDate As DateTime = DateTime.UtcNow.AddMinutes(-1)

            While True
                While (continueDate > DateTime.UtcNow)
                    System.Threading.Thread.Sleep(200)
                    If bw.CancellationPending = True Then
                        e.Cancel = True
                        Exit Sub
                    End If
                End While

                If bw.CancellationPending = True Then
                    e.Cancel = True
                    Exit While
                End If


                Dim interval As Integer = 0
                Dim intervalTmp As Integer = 0

                Try
                    SendMobileAppNotifications()
                Catch ex As Exception
                    bw.ReportProgress(0, ex.Message)
                End Try
                interval = (NextDate_SendMobileAppNotifications - Date.UtcNow).TotalSeconds


                Try
                    MemberActionResponseHandler()
                Catch ex As Exception
                    bw.ReportProgress(0, ex.Message)
                End Try
                intervalTmp = (NextDate_MemberActionResponseHandler - Date.UtcNow).TotalSeconds
                If (interval = 0 OrElse
                     intervalTmp > 0 AndAlso intervalTmp < interval) Then

                    ' this event is nearest
                    interval = intervalTmp
                End If


                bw.ReportProgress(-1, " Waiting " & interval & " seconds")
                continueDate = DateTime.UtcNow.AddMilliseconds(interval * 1000)

            End While

        Catch ex As Exception
            bw.ReportProgress(0, ex.Message)
        End Try
    End Sub

    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        If (e.ProgressPercentage = -1) Then
            Me.BarStaticItemStatus.Caption = Mid(CStr(e.UserState), 1, 200) & "..."
        Else
            AddMessage(CStr(e.UserState))
        End If
    End Sub

    Private Sub AddMessage(message As String)
        'txOutput.Text = "[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf & txOutput.Text
        If (message IsNot Nothing) Then
            Try
                If Me.InvokeRequired Then
                    ' calls itself, but on correct thread
                    Me.BeginInvoke(New Action(Of Object)(AddressOf txtOutput.MaskBox.AppendText), New Object() {"[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf})
                Else
                    txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf)
                End If
            Catch ex As InvalidOperationException
            End Try
        End If
        Try
            If (txtOutput.Lines.Length > 5001) Then
                Dim index As Integer = 0
                Dim linesCount As Integer = txtOutput.Lines.Length
                Dim txtOutputText As String = txtOutput.Text
                For cnt = 0 To linesCount - 5000
                    index = txtOutputText.IndexOf(vbCr, index) + vbCr.Length
                Next
                If (index > 0) Then
                    txtOutput.Text = txtOutput.Text.Remove(0, index).TrimStart({ControlChars.Cr, ControlChars.Lf})
                End If
            End If
        Catch
        End Try
    End Sub

    Private Sub TryAddMessage(progress As Integer, message As String)
        Try
            bw.ReportProgress(progress, message)
        Catch ex As InvalidOperationException
            AddMessage(message)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then
            Me.BarStaticItemStatus.Caption = "Canceled"
            tmrTask.Enabled = False
        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub


    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub

    Private Sub BarButtonItemRun_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRun.ItemClick
        Try
            tmrTask.Enabled = False

            If Not bw.IsBusy = True Then
                BarStaticItemStatus.Caption = "Running"
                bw.RunWorkerAsync()
                BarButtonItemRun.Enabled = False
            Else
                Try
                    bw.CancelAsync()
                    bw.RunWorkerAsync()
                    BarStaticItemStatus.Caption = "Running"
                Catch ex As Exception
                    AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick -> bw.IsBusy" & vbCrLf & ex.Message)
                End Try
            End If

        Catch ex As Exception
            AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick" & vbCrLf & ex.Message)
        Finally
            tmrTask.Enabled = True
            tmrTask.Interval = tmrTaskInterval
        End Try
    End Sub


    Private Sub BarButtonItemCancelJob_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCancelJob.ItemClick
        bw.CancelAsync()
        tmrTask.Enabled = False
        BarButtonItemRun.Enabled = True
    End Sub



    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        If Not bw.IsBusy = True Then
            BarStaticItemStatus.Caption = "Running"
            bw.RunWorkerAsync()
            BarButtonItemRun.Enabled = False
        End If
        tmrTask.Interval = tmrTaskInterval
    End Sub


    Private Sub StartTimer()
        Dim r As New Random(System.DateTime.Now.Millisecond)
        Dim interval As Integer = r.Next(30, 60)

        If (tmrTaskInterval = 0) Then tmrTaskInterval = tmrTask.Interval
        tmrTask.Interval = interval * 1000
        tmrTask.Enabled = True

        tmrTask.Enabled = True
        tmrTask.Start()

        BarStaticItemStatus.Caption = "First run in " & tmrTask.Interval / 1000 & " sec"
    End Sub


#Region "SendMobileAppNotifications"

    Private NextDate_SendMobileAppNotifications As DateTime
    Private Sub SendMobileAppNotifications()
        Using hdl As New clsSendingMobileNotificationsHandler()



            Try

                Dim emailCount As Integer
                '  Dim notifyNextCheck As Boolean = False
                Dim androidPath As String = Nothing

                If (NextDate_SendMobileAppNotifications = DateTime.MinValue) Then
                    NextDate_SendMobileAppNotifications = DateTime.UtcNow.AddSeconds(-1)
                ElseIf (NextDate_SendMobileAppNotifications > DateTime.UtcNow) Then
                    '   notifyNextCheck = True
                End If


                While NextDate_SendMobileAppNotifications <= DateTime.UtcNow OrElse emailCount < 10

                    Dim emailForSending As SYS_MobileNotificationsQueue = Nothing
                    Try
                        emailForSending = hdl.GetNextNotification()
                        If (emailForSending Is Nothing) Then
                            Exit While
                        End If

                        'Dim url As String = "http://188.165.13.64/android/GoomenaLib.Service1.svc/json/notification/{fromProfileId}/{toProfileId}/{type}"
                        'Dim url As String = "http://188.165.13.64/android/GoomenaLib.Service1.svc/json/notification/{0}/{1}/{2}"
                        If (String.IsNullOrEmpty(androidPath)) Then
                            androidPath = clsConfigValues.GetSYS_ConfigValue("android_url_for_notification_service")
                        End If


                        Dim url As String = String.Format(androidPath, emailForSending.FromProfileID, emailForSending.ToProfileID, emailForSending.Type)

                        bw.ReportProgress(0, url)
                        Dim request As WebRequest = Nothing

                        Try
                            request = WebRequest.Create(url)
                            request.Credentials = CredentialCache.DefaultCredentials
                            Using response = request.GetResponse()



                                If (response IsNot Nothing) Then
                                    Using dataStream = response.GetResponseStream()


                                        If (dataStream IsNot Nothing) Then
                                            Using reader = New StreamReader(dataStream)


                                                Dim responseFromServer As String = reader.ReadToEnd()
                                                bw.ReportProgress(0, responseFromServer)

                                                hdl.DeleteSentNotification(emailForSending.QueueID)
                                                'emailForSending = hdl.GetNextNotification()
                                            End Using
                                        End If
                                    End Using
                                End If
                            End Using
                        Catch ex As Exception
                            Throw New Exception(ex.Message, ex)
                        End Try

                    Catch ex As Exception
                        ' emailForSending.Exception = ex.ToString()
                        bw.ReportProgress(0, " Exc. " & ex.Message)
                        Try
                            Dim str As String = ""
                            str = vbCrLf &
                                "Check mobile APP web service, it's probably stopped working!" & vbCrLf & vbCrLf & _
                                 "FromProfileID: " & emailForSending.FromProfileID & vbCrLf & _
                                 "ToProfileID: " & emailForSending.ToProfileID & vbCrLf & _
                                 "Type: " & emailForSending.Type & vbCrLf & vbCrLf & _
                                 "Exception : " & ex.ToString()
                            clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Sending Mobile Notification to android service Exception: " & ex.Message, str, False)

                            System.Threading.Thread.Sleep(120000)
                        Catch ex1 As Exception
                        End Try
                    End Try

                    emailCount = emailCount + 1
                    System.Threading.Thread.Sleep(500)
                End While

                Dim interval As Integer
                Dim r As New Random(System.DateTime.Now.Millisecond)
                interval = r.Next(20, 60)
                'bw.ReportProgress(0, "Send MobileApp Notifications-->Waiting " & interval & " seconds")
                NextDate_SendMobileAppNotifications = DateTime.UtcNow.AddMilliseconds(interval * 1000)

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try
        End Using

    End Sub



#End Region


#Region "MemberActionResponseHandler"

    Private NextDate_MemberActionResponseHandler As DateTime
    Public Sub MemberActionResponseHandler()
        Using hdl As New clsMemberActionResponseHandler()


            Try

                If (NextDate_MemberActionResponseHandler = DateTime.MinValue) Then
                    NextDate_MemberActionResponseHandler = DateTime.UtcNow.AddSeconds(-1)
                End If

                If (NextDate_MemberActionResponseHandler <= DateTime.UtcNow) Then
                    AddHandler hdl.ReportProgress, AddressOf hdl_ReportProgress
                    AddHandler hdl.SendingEmail, AddressOf hdl_SendingEmail
                    hdl.MemberActionResponseHandler()
                End If


                Dim interval As Integer
                Dim r As New Random(System.DateTime.Now.Millisecond)
                interval = r.Next(20, 60)
                NextDate_MemberActionResponseHandler = DateTime.UtcNow.AddMilliseconds(interval * 1000)

            Catch ex As Exception
                Throw New Exception(ex.Message, ex)
            End Try

        End Using

    End Sub


    Private Sub hdl_ReportProgress(sender As Object, e As TaskProgressEventArgs)
        Try
            bw.ReportProgress(e.Status, e.ProgressInfo)
        Catch
        End Try
    End Sub

    Private Sub hdl_SendingEmail(sender As Object, e As SendingEmailEventArgs)
        Try
            clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, e.Subject, e.Body, e.IsHtml)
        Catch
        End Try
    End Sub

    'Public Sub MemberActionResponseHandler_OLD()
    '    Dim hdl As New clsMemberActionResponseHandler()
    '    Dim ConnectionString As String = My.Settings.AppDBconnectionString
    '    Dim ctx As New CMSDBDataContext(ConnectionString)


    '    Try

    '        Dim minQueueid As Integer = 0
    '        Dim itemCount As Integer
    '        Dim notifyNextCheck As Boolean = False

    '        If (NextDate_MemberActionResponseHandler = DateTime.MinValue) Then
    '            NextDate_MemberActionResponseHandler = DateTime.UtcNow.AddSeconds(-1)
    '        ElseIf (NextDate_MemberActionResponseHandler > DateTime.UtcNow) Then
    '            notifyNextCheck = True
    '        End If

    '        Dim allowSending As Double?
    '        Dim LastActivityUTCDate As DateTime

    '        While (NextDate_MemberActionResponseHandler <= DateTime.UtcNow OrElse itemCount < 11)

    '            Dim item As SYS_MemberActionPerformQueue = Nothing
    '            Try
    '                item = hdl.GetNextEntry(minQueueid)
    '                If (item Is Nothing) Then
    '                    Exit While
    '                End If

    '                If (allowSending Is Nothing) Then
    '                    allowSending = clsConfigValues.GetSYS_ConfigValueDouble2("ready_messages_allow_sending")
    '                End If
    '                If (LastActivityUTCDate = DateTime.MinValue) Then
    '                    Dim mins As Integer = clsConfigValues.Get__members_online_minutes()
    '                    LastActivityUTCDate = Date.UtcNow.AddMinutes(-mins)
    '                End If

    '                Dim deleteEntry As Boolean = False
    '                If (CInt(allowSending) <> 1) Then deleteEntry = True


    '                Dim actionType As MemberActionTypeEnum = item.Type
    '                Dim messageActionsString As String = ""
    '                Dim profile = Nothing, profileWoman = Nothing

    '                If (Not deleteEntry) Then

    '                    If ((actionType And MemberActionTypeEnum.Likes) = MemberActionTypeEnum.Likes) Then messageActionsString = messageActionsString & CInt(ReadyMessagesActionsEnum.After_Man_Like) & ","
    '                    If ((actionType And MemberActionTypeEnum.Viewed) = MemberActionTypeEnum.Viewed) Then messageActionsString = messageActionsString & CInt(ReadyMessagesActionsEnum.After_profile_view) & ","
    '                    messageActionsString = messageActionsString.Trim(","c)

    '                    If (String.IsNullOrEmpty(messageActionsString)) Then deleteEntry = True
    '                End If


    '                If (Not deleteEntry) Then
    '                    profile = (From itm In ctx.EUS_Profiles
    '                                Where itm.ProfileID = item.FromProfileID AndAlso
    '                                itm.IsMaster = True AndAlso
    '                                itm.Status = ProfileStatusEnum.Approved AndAlso
    '                                itm.GenderId = 1
    '                                Select itm.ProfileID, itm.Birthday, itm.LoginName, itm.Country, itm.LAGID, itm.GenderId).FirstOrDefault()

    '                    profileWoman = (From itm In ctx.EUS_Profiles
    '                                Where itm.ProfileID = item.ToProfileID AndAlso
    '                                itm.IsMaster = True AndAlso
    '                                itm.Status = ProfileStatusEnum.Approved AndAlso
    '                                itm.GenderId = 2 AndAlso
    '                                (itm.ReferrerParentId IsNot Nothing AndAlso itm.ReferrerParentId > 0)
    '                                Select itm.ProfileID, itm.Birthday, itm.LoginName, itm.Country, itm.LAGID, itm.IsOnline, itm.LastActivityDateTime).FirstOrDefault()

    '                    If (profile Is Nothing) Then deleteEntry = True
    '                    If (profileWoman Is Nothing) Then deleteEntry = True

    '                    If (profileWoman IsNot Nothing) Then
    '                        Dim isOnline As Boolean = clsNullable.NullTo(profileWoman.IsOnline)
    '                        If (isOnline) Then
    '                            Dim __LastActivityDateTime As DateTime? = profileWoman.LastActivityDateTime
    '                            If (__LastActivityDateTime.HasValue) Then
    '                                isOnline = __LastActivityDateTime >= LastActivityUTCDate
    '                            Else
    '                                isOnline = False
    '                            End If
    '                        End If

    '                        If (Not isOnline) Then deleteEntry = True
    '                    End If

    '                End If


    '                If (deleteEntry) Then
    '                    hdl.DeleteEntry(item.QueueID)
    '                    Continue While
    '                End If


    '                Dim SendPeriodDay As Integer = DateTime.UtcNow.Day
    '                Dim SendPeriodMonth As Integer = DateTime.UtcNow.Month
    '                Dim SendPeriodHour As Integer = DateTime.UtcNow.Hour
    '                Dim SendPeriodMinute As Integer = DateTime.UtcNow.Minute
    '                Dim SendPeriodSecond As Integer = DateTime.UtcNow.Second
    '                Dim CurrentAge As Integer = ProfileHelper.GetCurrentAge(profile.Birthday)

    '                'Dim newSql As String = sqlReadyMessages.Replace("[ACTIONSID]", messageActionsString)
    '                Dim newSql As String = sqlReadyMessages

    '                Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(newSql)
    '                cmd.Parameters.AddWithValue("@SendPeriodDay", SendPeriodDay)
    '                cmd.Parameters.AddWithValue("@SendPeriodMonth", SendPeriodMonth)
    '                cmd.Parameters.AddWithValue("@SendPeriodHour", SendPeriodHour)
    '                cmd.Parameters.AddWithValue("@SendPeriodMinute", SendPeriodMinute)
    '                cmd.Parameters.AddWithValue("@CurrentAge", CurrentAge)
    '                cmd.Parameters.AddWithValue("@LAGID", profile.LAGID)
    '                cmd.Parameters.AddWithValue("@CurrentCountry", profile.Country)
    '                cmd.Parameters.AddWithValue("@ProfileID", profile.ProfileID)
    '                cmd.Parameters.AddWithValue("@ACTIONSID", messageActionsString)

    '                Dim minsDiff As Double = ((Date.UtcNow - item.DateTimeToCreate.Value).TotalSeconds / 60)
    '                cmd.Parameters.AddWithValue("@DelayMinutes", minsDiff)

    '                Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
    '                If (dt.Rows.Count > 0 AndAlso CDbl(dt.Rows(0)("ReadyMessageID")) > 0) Then

    '                    ' we have some results 

    '                    ' 1. update queue item's values
    '                    item.RecipientProfileID = profile.ProfileID
    '                    item.ReadyMessageID = dt.Rows(0)("ReadyMessageID")
    '                    item.DateSent = Date.UtcNow


    '                    ' 2. send message to member man and create sent message for woman
    '                    Dim msg As EUS_ReadyMessage = (From itm In ctx.EUS_ReadyMessages
    '                                                  Where itm.ReadyMessageID = item.ReadyMessageID
    '                                                  Select itm).FirstOrDefault()

    '                    If (msg IsNot Nothing) Then
    '                        Dim replyToMessageId As Integer = -1
    '                        Dim blockReceiving As Boolean = False
    '                        clsUserDoes.SendMessage("", msg.Text, item.ToProfileID, profile.ProfileID, False, replyToMessageId, blockReceiving)

    '                        Dim OfferID As Integer = -1
    '                        If ((actionType And MemberActionTypeEnum.Likes) = MemberActionTypeEnum.Likes) Then
    '                            Dim rec As EUS_Offer = clsUserDoes.GetAnyWinkPending(item.ToProfileID, item.FromProfileID) 'Me.MasterProfileId, Me.UserIdInView
    '                            If (rec IsNot Nothing) Then
    '                                OfferID = rec.OfferID
    '                            End If
    '                        End If

    '                        ' h gynaika pou apantaei sto minima, kleinei to like na min einai new
    '                        clsUserDoes.MakeNewDate(item.ToProfileID, profile.ProfileID, OfferID, False)
    '                    End If


    '                    hdl.UpdateEntry(item)

    '                Else
    '                    ' is there any message, that have delay on it
    '                    ' if such message exists, then keep current action

    '                    newSql = sqlReadyMessages_WithDelay '.Replace("[ACTIONSID]", messageActionsString)
    '                    cmd.CommandText = newSql
    '                    dt = DataHelpers.GetDataTable(cmd)

    '                    If (dt.Rows.Count > 0) Then
    '                        Dim DelayMinutesMin As Integer = CDbl(dt.Rows(0)("DelayMinutesMin"))
    '                        If (DelayMinutesMin > 0 AndAlso minsDiff < DelayMinutesMin - 1) Then
    '                            minQueueid = item.QueueID
    '                            bw.ReportProgress(0, " Current queue item  (SYS_MemberActionPerformQueue:QueueID " & minQueueid & ") will be checked in next iteration. Found message(s) that can be sent after some delay (EUS_ReadyMessages:DelayMinutesMin " & DelayMinutesMin & ")")
    '                            Continue While
    '                        Else
    '                            deleteEntry = True
    '                        End If
    '                    Else
    '                        deleteEntry = True
    '                    End If
    '                End If


    '                If (deleteEntry) Then
    '                    hdl.DeleteEntry(item.QueueID)
    '                    Continue While
    '                End If


    '            Catch ex As Exception
    '                bw.ReportProgress(0, " Exc. " & ex.Message)

    '                Try
    '                    item.Exception = ex.ToString()
    '                    hdl.UpdateEntry(item)
    '                Catch
    '                End Try

    '                Try
    '                    Dim str As String = ""
    '                    str = vbCrLf &
    '                        "Checking ReadyMessages!" & vbCrLf & vbCrLf & _
    '                         "FromProfileID: " & item.FromProfileID & vbCrLf & _
    '                         "ToProfileID: " & item.ToProfileID & vbCrLf & _
    '                         "Type: " & item.Type & vbCrLf & vbCrLf & _
    '                         "Exception : " & ex.ToString()
    '                    clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Exception, while checking ReadyMessages: " & ex.Message, str, False)

    '                    System.Threading.Thread.Sleep(120000)
    '                Catch ex1 As Exception
    '                End Try
    '            End Try

    '            itemCount = itemCount + 1
    '            System.Threading.Thread.Sleep(500)
    '        End While

    '        Dim interval As Integer
    '        Dim r As New Random(System.DateTime.Now.Millisecond)
    '        interval = r.Next(20, 60)
    '        NextDate_MemberActionResponseHandler = DateTime.UtcNow.AddMilliseconds(interval * 1000)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message, ex)
    '    Finally
    '        ctx.Dispose()
    '        hdl.Dispose()
    '    End Try


    'End Sub



#End Region

End Class

#Region "Comments"
'    Private sqlReadyMessages As String = <sql><![CDATA[
'select top(1) m.ReadyMessageID 
'from [EUS_ReadyMessages] m
'inner join [EUS_ReadyMessagesActionsRel] r on r.ReadyMessageID=m.ReadyMessageID
'inner join [EUS_ReadyMessagesActions] ac on ac.ActionsID=r.ActionsID
'where ac.ActionsID in ([ACTIONSID]) 
'and m.Status=2 -- active status
'and	(
'	m.SendPeriodStartMonth is null 	or 
'	(
'		m.SendPeriodStartMonth<=@SendPeriodMonth and 
'		m.SendPeriodEndMonth>=@SendPeriodMonth 
'	)
')
'and	(
'	m.SendPeriodStartDay is null 	or 
'	(
'		m.SendPeriodStartDay<=@SendPeriodDay and 
'		m.SendPeriodEndDay>=@SendPeriodDay 
'	)
')
'and	(
'	m.SendPeriodStartHour is null 	or 
'	(
'		m.SendPeriodStartHour<=@SendPeriodHour and 
'		m.SendPeriodEndHour>=@SendPeriodHour 
'	)
')
'and	(
'	m.SendPeriodStartMinute is null 	or 
'	(
'		m.SendPeriodStartMinute<=@SendPeriodMinute and 
'		m.SendPeriodEndMinute>=@SendPeriodMinute
'	)
')
'and(
'	m.AgeMin is null 	or  
'	m.AgeMin<=@CurrentAge
')
'and(
'	m.AgeMax is null 	or  
'	m.AgeMax>=@CurrentAge
')
'and(
'	m.Country is null 	or  
'	@CurrentCountry is null 	or  
'	m.Country=@CurrentCountry
')
'and(
'	-- profile's lang
'	m.Language is null 	or  
'	@LAGID is null 	or  
'	m.Language=@LAGID
')
'and not exists(
'	select * 
'	from SYS_MemberActionPerformQueue
'	where RecipientProfileID=@ProfileID
'	and ReadyMessageID=m.ReadyMessageID
'	and not DateSent is null
')
'and(
'    -- action delay
'	m.DelayMinutesMin is null 	or  
'	m.DelayMinutesMin<=@DelayMinutes
')
'and(
'    -- action delay
'	m.DelayMinutesMax is null 	or  
'	m.DelayMinutesMax>=@DelayMinutes
')
'order by NEWID()
']]></sql>.Value

'    Private sqlReadyMessages_WithDelay As String = <sql><![CDATA[
'-- to keep current action, we have to check 
'-- if there is any message with big delay

'select 
'    DelayMinutesMin=isnull(min(m.DelayMinutesMin),0)
'    --DelayMinutesMax=max(m.DelayMinutesMax)
'from [EUS_ReadyMessages] m
'inner join [EUS_ReadyMessagesActionsRel] r on r.ReadyMessageID=m.ReadyMessageID
'inner join [EUS_ReadyMessagesActions] ac on ac.ActionsID=r.ActionsID
'where ac.ActionsID in ([ACTIONSID]) 
'and m.Status=2 -- active status
'and	(
'	m.SendPeriodStartMonth is null 	or 
'	(
'		m.SendPeriodStartMonth<=@SendPeriodMonth and 
'		m.SendPeriodEndMonth>=@SendPeriodMonth 
'	)
')
'and	(
'	m.SendPeriodStartDay is null 	or 
'	(
'		m.SendPeriodStartDay<=@SendPeriodDay and 
'		m.SendPeriodEndDay>=@SendPeriodDay 
'	)
')
'and	(
'	m.SendPeriodStartHour is null 	or 
'	(
'		m.SendPeriodStartHour<=@SendPeriodHour and 
'		m.SendPeriodEndHour>=@SendPeriodHour 
'	)
')
'and	(
'	m.SendPeriodStartMinute is null 	or 
'	(
'		m.SendPeriodStartMinute<=@SendPeriodMinute and 
'		m.SendPeriodEndMinute>=@SendPeriodMinute
'	)
')
'and(
'	m.AgeMin is null 	or  
'	m.AgeMin<=@CurrentAge
')
'and(
'	m.AgeMax is null 	or  
'	m.AgeMax>=@CurrentAge
')
'and(
'	m.Country is null 	or  
'	@CurrentCountry is null 	or  
'	m.Country=@CurrentCountry
')
'and(
'	-- profile's lang
'	m.Language is null 	or  
'	@LAGID is null 	or  
'	m.Language=@LAGID
')
'and not exists(
'	select * 
'	from SYS_MemberActionPerformQueue
'	where RecipientProfileID=@ProfileID
'	and ReadyMessageID=m.ReadyMessageID
'	and not DateSent is null
')
'and(
'    m.DelayMinutesMin>@DelayMinutes -- message that have min delay, and not enough time has pass after user action 
')
']]></sql>.Value

#End Region
﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Xml
Imports System.Globalization
Imports System.Linq
Imports System.Data.SqlClient
Imports System.Data.Linq
Imports System.Xml.Linq

Public Class frmTasks

    Dim tmrTaskInterval As Integer
    ' Dim isBusy_PerformImageComparison As Boolean

    Private Sub frmTasks_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GroupControl1.Text = Me.Text
        BarStaticItemStatus.Caption = "Not running"
        StartTimer()
    End Sub


    Private Sub frmTasks_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        OnClose()
    End Sub



    Private Sub frmTasks_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
        OnClose()
    End Sub


    Private Sub OnClose()

        'Try
        '    If (NextDate_SendPaymentVisits_TR > DateTime.MinValue) Then
        '        SupportAppHelper.Set__NextDate_SendPaymentVisits_TR(NextDate_SendPaymentVisits_TR)
        '        NextDate_SendPaymentVisits_TR = DateTime.MinValue
        '    End If
        'Catch
        'End Try

        Try
            If (NextDate_CurrencyXMLCheck > DateTime.MinValue) Then
                SupportAppHelper.Set__NextDate_CurrencyXMLCheck(NextDate_CurrencyXMLCheck)
                NextDate_CurrencyXMLCheck = DateTime.MinValue
            End If
        Catch
        End Try

        Try
            If (NextDate_CheckOnlineWomen > DateTime.MinValue) Then
                SupportAppHelper.Set__NextDate_CheckOnlineWomen(NextDate_CheckOnlineWomen)
                NextDate_CheckOnlineWomen = DateTime.MinValue
            End If
        Catch
        End Try

        Try
            If (NextDate_SendMobileSMSForVirtualProfile > DateTime.MinValue) Then
                SupportAppHelper.Set__NextDate_SendMobileSMSForVirtualProfile(NextDate_SendMobileSMSForVirtualProfile)
                NextDate_SendMobileSMSForVirtualProfile = DateTime.MinValue
            End If
        Catch
        End Try


        Try
            If (bw IsNot Nothing) Then
                bw.CancelAsync()
                bw.Dispose()
                bw = Nothing
            End If
            tmrTask.Enabled = False
        Catch ex As Exception
        End Try
    End Sub

    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork
        'Dim hdl As New Core.DLL.clsSendingEmailsHandler()
        Try
            'Dim emailForSending As Core.DLL.SYS_EmailsQueue = hdl.GetNextEmailToSend()

            'Dim emailCount As Integer
            '  Dim r As New Random(System.DateTime.Now.Millisecond)
            Dim continueDate As DateTime = DateTime.UtcNow.AddMinutes(-1)

            Dim checkCurrencies As Boolean = True
            While True
                While (continueDate > DateTime.UtcNow)
                    System.Threading.Thread.Sleep(200)
                    If bw Is Nothing OrElse bw.CancellationPending = True Then
                        e.Cancel = True
                        Exit Sub
                    End If
                End While

                If bw.CancellationPending = True Then
                    e.Cancel = True
                    Exit Sub
                End If

                Dim interval As Integer = 0
                Dim intervalTmp As Integer = 0

                If (checkCurrencies) Then
                    Try
                        GetEURORates()
                    Catch ex As Exception
                        bw.ReportProgress(0, ex.Message)
                        checkCurrencies = False
                    End Try
                Else
                    bw.ReportProgress(0, "Currencies XML-->check was skipped, see previous errors.")
                End If
                interval = (NextDate_CurrencyXMLCheck - Date.UtcNow).TotalSeconds

                'Try
                '    SendPaymentVisits_TR()
                'Catch ex As Exception
                '    bw.ReportProgress(0, ex.Message)
                'End Try
                'intervalTmp = (NextDate_SendPaymentVisits_TR - Date.UtcNow).TotalSeconds
                'If (interval = 0 OrElse
                '    intervalTmp > 0 AndAlso intervalTmp < interval) Then
                '    ' this event is nearest
                '    interval = intervalTmp
                'End If

                Try
                    CheckOnlineWomen()
                Catch ex As Exception
                    bw.ReportProgress(0, ex.Message)
                End Try
                intervalTmp = (NextDate_CheckOnlineWomen - Date.UtcNow).TotalSeconds
                If (interval = 0 OrElse
                    intervalTmp > 0 AndAlso intervalTmp < interval) Then
                    ' this event is nearest
                    interval = intervalTmp
                End If


                Try
                    SendMobileSMSForVirtualProfile()
                Catch ex As Exception
                    bw.ReportProgress(0, ex.Message)
                End Try
                intervalTmp = (NextDate_SendMobileSMSForVirtualProfile - Date.UtcNow).TotalSeconds
                If (interval = 0 OrElse
                    intervalTmp > 0 AndAlso intervalTmp < interval) Then
                    ' this event is nearest
                    interval = intervalTmp
                End If


                Try
                    ClearTablesData()
                Catch ex As Exception
                    bw.ReportProgress(0, ex.Message)
                End Try
                intervalTmp = (NextDate_ClearTablesData - Date.UtcNow).TotalSeconds
                If (interval = 0 OrElse
                    intervalTmp > 0 AndAlso intervalTmp < interval) Then
                    ' this event is nearest
                    interval = intervalTmp
                End If


                Try
                    MaintainDBIndexes()
                Catch ex As Exception
                    bw.ReportProgress(0, ex.Message)
                End Try
                intervalTmp = (NextDate_MaintainDBIndexes - Date.UtcNow).TotalSeconds
                If (interval = 0 OrElse
                    intervalTmp > 0 AndAlso intervalTmp < interval) Then
                    ' this event is nearest
                    interval = intervalTmp
                End If


                'Dim interval As Integer = r.Next(600, 1200)
                bw.ReportProgress(-1, " Waiting " & interval & " seconds")
                continueDate = DateTime.UtcNow.AddMilliseconds(interval * 1000)

            End While

        Catch ex As Exception
            bw.ReportProgress(0, ex.Message)
        Finally
            'hdl.Dispose()
        End Try
    End Sub

    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        If (e.ProgressPercentage = -1) Then
            Me.BarStaticItemStatus.Caption = Mid(CStr(e.UserState), 1, 200) & "..."
        Else
            AddMessage(CStr(e.UserState))
        End If
    End Sub

    Private Sub AddMessage(message As String)
        'txOutput.Text = "[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf & txOutput.Text
        If (message IsNot Nothing) Then
            Try
                If Me.InvokeRequired Then
                    ' calls itself, but on correct thread
                    Me.BeginInvoke(New Action(Of Object)(AddressOf txtOutput.MaskBox.AppendText), New Object() {"[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf})
                Else
                    txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf)
                End If
            Catch ex As InvalidOperationException
            End Try
        End If
        Try
            If (txtOutput.Lines.Length > 5001) Then
                Dim index As Integer = 0
                Dim linesCount As Integer = txtOutput.Lines.Length
                Dim txtOutputText As String = txtOutput.Text
                For cnt = 0 To linesCount - 5000
                    index = txtOutputText.IndexOf(vbCr, index) + vbCr.Length
                Next
                If (index > 0) Then
                    txtOutput.Text = txtOutput.Text.Remove(0, index).TrimStart({ControlChars.Cr, ControlChars.Lf})
                End If
            End If
        Catch
        End Try
    End Sub


    Private Sub TryAddMessage(progress As Integer, message As String)
        Try
            bw.ReportProgress(progress, message)
        Catch ex As InvalidOperationException
            AddMessage(message)
        Catch ex As Exception
        End Try
    End Sub


    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then

            Me.BarStaticItemStatus.Caption = "Canceled"
            tmrTask.Enabled = False

        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub



    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub

    Private Sub BarButtonItemRun_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRun.ItemClick
        Try
            tmrTask.Enabled = False

            If Not bw.IsBusy = True Then
                BarStaticItemStatus.Caption = "Running"
                bw.RunWorkerAsync()
                BarButtonItemRun.Enabled = False
            Else
                Try
                    bw.CancelAsync()
                    bw.RunWorkerAsync()
                    BarStaticItemStatus.Caption = "Running"
                Catch ex As Exception
                    AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick -> bw.IsBusy" & vbCrLf & ex.Message)
                End Try
            End If

        Catch ex As Exception
            AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick" & vbCrLf & ex.Message)
        Finally
            tmrTask.Enabled = True
            tmrTask.Interval = tmrTaskInterval
        End Try
    End Sub


    Private Sub BarButtonItemCancelJob_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCancelJob.ItemClick
        bw.CancelAsync()
        tmrTask.Enabled = False
        BarButtonItemRun.Enabled = True
    End Sub



    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        If Not bw.IsBusy = True Then
            BarStaticItemStatus.Caption = "Running"
            bw.RunWorkerAsync()
            BarButtonItemRun.Enabled = False
        End If
        tmrTask.Interval = tmrTaskInterval
    End Sub


    Private Sub StartTimer()
        Dim r As New Random(System.DateTime.Now.Millisecond)
        Dim interval As Integer = r.Next(30, 60)

        If (tmrTaskInterval = 0) Then tmrTaskInterval = tmrTask.Interval
        tmrTask.Interval = interval * 1000
        tmrTask.Enabled = True

        tmrTask.Enabled = True
        tmrTask.Start()

        BarStaticItemStatus.Caption = "First run in " & tmrTask.Interval / 1000 & " sec"
    End Sub


#Region "CheckOnlineWomen"

    Dim NextDate_CheckOnlineWomen As DateTime

    Private Sub CheckOnlineWomen()
        Try
            Dim notifyNextCheck As Boolean = False
            Dim dateTo As DateTime
            Dim onlineMinutes As Double

            ' read from config
            If (NextDate_CheckOnlineWomen = DateTime.MinValue) Then
                NextDate_CheckOnlineWomen = SupportAppHelper.Get__NextDate_CheckOnlineWomen()
                notifyNextCheck = True
            End If

            Dim tmp As String = clsConfigValues.GetSYS_ConfigValue("members_online_minutes")
            If (Not Integer.TryParse(tmp, onlineMinutes)) Then onlineMinutes = 20
            If (NextDate_CheckOnlineWomen = DateTime.MinValue) Then
                dateTo = DateTime.UtcNow.AddMinutes(-1)
            Else
                dateTo = NextDate_CheckOnlineWomen.AddMinutes(-1)
            End If

            If (Date.UtcNow > dateTo) Then
                bw.ReportProgress(0, "Online Women-->Checking ...")
                CheckOnlineWomenHelper(onlineMinutes)
                NextDate_CheckOnlineWomen = Date.UtcNow.AddMinutes(onlineMinutes * 2).AddMinutes(1)
            End If

            If (notifyNextCheck OrElse Date.UtcNow > dateTo) Then
                bw.ReportProgress(0, "Online Women-->next check @ " & NextDate_CheckOnlineWomen.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss"))
            End If

        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        End Try
    End Sub


    Private Sub CheckOnlineWomenHelper(onlineMinutes As Double)

        Dim SYS_CountriesGEOList As List(Of SYS_CountriesGEO) = SYS_CountriesGEOHelper.SYS_CountriesGEO_CheckWomenOnline()
        If (SYS_CountriesGEOList.Count = 0) Then Exit Sub

        Dim LastActivityUTCDate As DateTime = Date.UtcNow.AddMinutes(-onlineMinutes)

        Using dsOnline As New DSMembersAdmin()

            Using con As New SqlConnection(My.MySettings.Default.AppDBconnectionString)
                Using taNew As New DSMembersAdminTableAdapters.EUS_ProfilesTableAdapter
                    taNew.Connection = con
                    taNew.FillBy_MembersOnline(dsOnline.EUS_Profiles, True, LastActivityUTCDate, Nothing, False, Nothing, 0, 0, True, False)

                End Using
            End Using
           
           
            Using _CMSDBDataContext As New CMSDBDataContext(My.MySettings.Default.AppDBconnectionString)


                Try

                    For geo = 0 To SYS_CountriesGEOList.Count - 1
                        Dim geoItem As SYS_CountriesGEO = SYS_CountriesGEOList(geo)


                        Dim countWomen As Integer = (From itm In _CMSDBDataContext.EUS_Profiles
                                         Where itm.GenderId = 2 AndAlso
                                                 itm.Status = ProfileStatusEnum.Approved AndAlso
                                                 itm.IsMaster = True AndAlso
                                                 itm.Country = geoItem.Iso
                                         Select itm).Count()

                        If (countWomen <= 0) Then Continue For

                        Dim onlineWomen As Integer = dsOnline.EUS_Profiles.Compute("Count(ProfileID)", "Country='" & geoItem.Iso & "'")

                        Dim percent As Integer = onlineWomen * 100 / countWomen
                        If (percent < geoItem.WomenOnlinePercent) Then
                            Dim targetCount As Integer = (countWomen * geoItem.WomenOnlinePercent) / 100

                            Dim sql As String = <sql><![CDATA[
declare @LastActivityUTCDate datetime = dateadd(mi, -@onlineMinutes, getutcdate())

set rowcount @targetCount

Select ProfileID, LoginName, [LastActivityDateTime], isonline,Country, LastActivityIP
from  EUS_Profiles 
where  GenderId=2 
and Status=4
and IsMaster=1
and Country=@Country
and [LastActivityDateTime]<@LastActivityUTCDate
and exists(select ProfileID 
			from dbo.EUS_ProfilesPrivacySettings
			where (ProfileID=EUS_Profiles.ProfileID or mirrorProfileID=EUS_Profiles.ProfileID)
			and AdminSetting_IsVirtual=1)
order by [LastActivityDateTime] asc
]]></sql>.Value
                            Using con As New Data.SqlClient.SqlConnection(My.Settings.AppDBconnectionString)
                                Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                                    cmd.Parameters.AddWithValue("@onlineMinutes", onlineMinutes)
                                    cmd.Parameters.AddWithValue("@Country", geoItem.Iso)
                                    cmd.Parameters.AddWithValue("@targetCount", targetCount)
                                    Using dt As DataTable = DataHelpers.GetDataTable(cmd)
                                        Dim r As New Random(System.DateTime.Now.Millisecond)
                                        For cnt = 0 To dt.Rows.Count - 1
                                            DataHelpers.EUS_Profile_SetOnlineManually(dt.Rows(cnt)("ProfileID"), dt.Rows(cnt)("LastActivityIP").ToString())
                                            Dim interval As Integer = r.Next(10, 40)
                                            bw.ReportProgress(0, "Online Women-->Set virtual online, waiting " & interval & " sec...")
                                            System.Threading.Thread.Sleep(interval * 1000)
                                        Next
                                        bw.ReportProgress(0, "Online Women-->Country " & geoItem.Iso & " -- set online " & dt.Rows.Count)
                                    End Using
                                End Using
                            End Using
                        End If
                    Next
                Catch ex As Exception

                Finally

                End Try
            End Using
        End Using
    End Sub



#End Region



#Region "GetView_SYS_WebStatistics_PaymentVisits_TR"

    Dim NextDate_SendPaymentVisits_TR As DateTime

    Private Sub SendPaymentVisits_TR()
        Try
            Dim notifyNextCheck As Boolean = False
            Dim dateTo As DateTime
            Dim dateFrom As DateTime

            ' read from config
            If (NextDate_SendPaymentVisits_TR = DateTime.MinValue) Then
                NextDate_SendPaymentVisits_TR = SupportAppHelper.Get__NextDate_SendPaymentVisits_TR()
                notifyNextCheck = True
            End If

            If (NextDate_SendPaymentVisits_TR = DateTime.MinValue) Then
                dateTo = New Date(Date.UtcNow.Year, Date.UtcNow.Month, Date.UtcNow.Day, 15, 0, 0)
                ' dateFrom = dateTo.AddDays(-1)
            Else
                dateTo = NextDate_SendPaymentVisits_TR.AddMinutes(-1)
                dateFrom = dateTo.AddDays(-1)
            End If

            'If (Date.UtcNow > dateTo) Then
            '    bw.ReportProgress(0, "Payment Visits for TR-->Checking ...")
            '    SendPaymentVisits_TRByDate(dateFrom, dateTo)
            '    NextDate_SendPaymentVisits_TR = dateTo.AddDays(1).AddMinutes(1)
            'End If

            If (notifyNextCheck OrElse Date.UtcNow > dateTo) Then
                bw.ReportProgress(0, "Payment Visits for TR-->next check @ " & NextDate_SendPaymentVisits_TR.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss"))
            End If

        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        End Try
    End Sub


    Private Sub SendPaymentVisits_TRByDate(dateFrom As DateTime, dateTo As DateTime)


        Dim sql As String = <sql><![CDATA[
EXEC	[dbo].[GetView_SYS_WebStatistics_PaymentVisits_TR]
		@dateFrom = @dateFrom,
		@dateTo = @dateTo
]]></sql>
        Using con As New Data.SqlClient.SqlConnection(My.Settings.AppDBconnectionString)


            Using cmd As SqlCommand = DataHelpers.GetSqlCommand(con, sql)


                cmd.Parameters.AddWithValue("@dateFrom", dateFrom)
                cmd.Parameters.AddWithValue("@dateTo", dateTo)
                Using ds As DataSet = DataHelpers.GetDataSet(cmd)
                    Dim GTBZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("GTB Standard Time")
                    Dim GTBTimeFrom As DateTime = TimeZoneInfo.ConvertTime(dateFrom, TimeZoneInfo.Utc, GTBZone)
                    Dim GTBTimeTo As DateTime = TimeZoneInfo.ConvertTime(dateTo, TimeZoneInfo.Utc, GTBZone)

                    Dim reportEmailBody As New System.Text.StringBuilder()
                    Dim reportEmailBody_Pattern As String = <body><![CDATA[
--------------------------------------------<br>
Page Visited: [PAGE_VISITED]<br>
Date from: [DATEFROM] (Greece Time)<br>
Date to: [DATETO] (Greece Time)<br>
Profiles (Count [PROFILES-COUNT]):<br>
<br>
[PROFILES]<br>

]]></body>


                    Dim previousPage As String = ""
                    Dim LOGINS As New System.Text.StringBuilder()

                    For tbl = 0 To ds.Tables.Count - 1

                        ' payment visits
                        Dim dt As DataTable = ds.Tables(tbl)
                        Dim _tmpBody As String = ""

                        previousPage = ""



                        For cnt = 0 To dt.Rows.Count - 1

                            Dim dr As DataRow = dt.Rows(cnt)
                            If (previousPage <> dr("Page")) Then

                                If (Not String.IsNullOrEmpty(previousPage)) Then
                                    ' here we have profiles from previous page

                                    LOGINS.Remove(LOGINS.Length - (", " & "<br>").Length, (", " & "<br>").Length)
                                    _tmpBody = _tmpBody.Replace("[PROFILES]", LOGINS.ToString())
                                    LOGINS.Length = 0

                                    reportEmailBody.AppendLine(_tmpBody & "<br>")
                                    _tmpBody = ""
                                End If

                                _tmpBody = reportEmailBody_Pattern
                                _tmpBody = _tmpBody.Replace("[PAGE_VISITED]", dr("Page"))
                                _tmpBody = _tmpBody.Replace("[DATEFROM]", GTBTimeFrom.ToString("dd/MM/yyyy HH:mm"))
                                _tmpBody = _tmpBody.Replace("[DATETO]", GTBTimeTo.ToString("dd/MM/yyyy HH:mm"))
                                _tmpBody = _tmpBody.Replace("[PROFILES-COUNT]", dt.Rows.Count)
                                previousPage = dr("Page")
                            End If

                            LOGINS.Append(dr("LoginName") & ", " & "<br>")
                        Next


                        If (LOGINS.Length > 0) Then
                            LOGINS.Remove(LOGINS.Length - (", " & "<br>").Length, (", " & "<br>").Length)
                            _tmpBody = _tmpBody.Replace("[PROFILES]", LOGINS.ToString())
                            LOGINS.Length = 0

                            reportEmailBody.AppendLine(_tmpBody & "<br>")
                            _tmpBody = ""
                        End If

                    Next


                    If (String.IsNullOrEmpty(reportEmailBody.Length = 0)) Then
                        reportEmailBody.AppendLine("<br>" & "Products and Payment pages was not visited in Turkey" & "<br>")
                    End If

                    Dim MySMTPSettings As New clsMyMail.SMTPSettings()
                    If clsMyMail.MySMTPSettingsSupport.gSMTPServerName Is Nothing OrElse clsMyMail.MySMTPSettingsSupport.gSMTPServerName.Length = 0 Then
                        Dim result As SYS_SMTPServer = DataHelpers.SYS_SMTPServers_GetByNameLINQ("GOOSupport")
                        If result IsNot Nothing Then

                            clsMyMail.MySMTPSettingsSupport.gSMTPOutPort = result.SMTPOutPort
                            clsMyMail.MySMTPSettingsSupport.gSMTPServerName = result.SMTPServerName
                            clsMyMail.MySMTPSettingsSupport.gSMTPLoginName = result.SMTPLoginName
                            clsMyMail.MySMTPSettingsSupport.gSMTPPassword = result.SMTPPassword
                            clsMyMail.MySMTPSettingsSupport.gSMTPenableSsl = result.SmtpUseSsl
                        Else
                            clsMyMail.MySMTPSettingsSupport.gSMTPOutPort = My.Settings.gSMTPOutPort
                            clsMyMail.MySMTPSettingsSupport.gSMTPServerName = My.Settings.gSMTPServerName
                            clsMyMail.MySMTPSettingsSupport.gSMTPLoginName = My.Settings.gSMTPLoginName
                            clsMyMail.MySMTPSettingsSupport.gSMTPPassword = My.Settings.gSMTPPassword
                            clsMyMail.MySMTPSettingsSupport.gSMTPenableSsl = False
                        End If
                    End If

                    Dim subject As String = "Products and Payment visiting for TR"
                    clsMyMail.TrySendMailSupport(My.Settings.gToEmail,
                                        subject,
                                        reportEmailBody.ToString(),
                                        True)

                    AddMessage("sent  to " & My.Settings.gToEmail & ", " & subject)
                End Using
            End Using
        End Using
        '   Dim UTCTime As DateTime = DateTime.UtcNow


    End Sub



#End Region

#Region "EURO RATES"


    Dim previousRatesHash As String
    Dim NextDate_CurrencyXMLCheck As DateTime

    Private Sub GetEURORates()
        'Try
        Dim notifyNextCheck As Boolean = False
        ' read from config
        If (NextDate_CurrencyXMLCheck = DateTime.MinValue) Then
            NextDate_CurrencyXMLCheck = SupportAppHelper.Get__NextDate_CurrencyXMLCheck()
            notifyNextCheck = True
        End If

        If (Date.UtcNow > NextDate_CurrencyXMLCheck) Then

            If (NextDate_CurrencyXMLCheck = Date.MinValue) Then
                NextDate_CurrencyXMLCheck = New Date(Date.UtcNow.Year, Date.UtcNow.Month, Date.UtcNow.Day, 15, 30, 0)
            End If

            NextDate_CurrencyXMLCheck = Date.UtcNow.AddHours(1)
            bw.ReportProgress(0, "Currencies XML-->Checking ...")

            Dim rp As String = ""
            Dim url As String = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"
            Try

                rp = clsWebPageProcessor.GetPageContent(url, 15000)

            Catch ex As Exception
                Dim str As String = ""
                str = "Failed to retrieve Currencies XML from  " & url & vbCrLf & _
                     "Exception : " & ex.ToString() & vbCrLf
                clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Failed to retrieve Currencies XML: " & ex.Message, str, False)

                Throw New System.Exception(ex.Message, ex)
            End Try
            Dim currentHash As String = AppUtils.MD5_GetString(rp)
            If (previousRatesHash Is Nothing OrElse previousRatesHash <> currentHash) Then
                bw.ReportProgress(0, "Currencies XML-->Updating data...")
                GetEURORates_SaveToDB(rp)


                If (previousRatesHash IsNot Nothing) Then
                    NextDate_CurrencyXMLCheck = Date.UtcNow.AddDays(1)
                    bw.ReportProgress(0, "Currencies XML-->changed")
                End If

                previousRatesHash = currentHash
            End If

        End If


        If (Date.UtcNow > NextDate_CurrencyXMLCheck OrElse notifyNextCheck) Then
            bw.ReportProgress(0, "Currencies XML-->next check @ " & NextDate_CurrencyXMLCheck.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss"))
        End If


        'Dim rtesXml = <?xml version="1.0" encoding="UTF-8"?>
        '              <gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01" xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">
        '                  <gesmes:subject>Reference rates</gesmes:subject>
        '                  <gesmes:Sender>
        '                      <gesmes:name>European Central Bank</gesmes:name>
        '                  </gesmes:Sender>
        '                  <Cube>
        '                      <Cube time='2013-10-09'>
        '                          <Cube currency='USD' rate='1.3515'/>
        '                          <Cube currency='JPY' rate='131.55'/>
        '                          <Cube currency='BGN' rate='1.9558'/>
        '                          <Cube currency='CZK' rate='25.598'/>
        '                          <Cube currency='DKK' rate='7.4595'/>
        '                          <Cube currency='GBP' rate='0.84640'/>
        '                          <Cube currency='HUF' rate='296.20'/>
        '                          <Cube currency='LTL' rate='3.4528'/>
        '                          <Cube currency='LVL' rate='0.7027'/>
        '                          <Cube currency='PLN' rate='4.1995'/>
        '                          <Cube currency='RON' rate='4.4658'/>
        '                          <Cube currency='SEK' rate='8.7445'/>
        '                          <Cube currency='CHF' rate='1.2313'/>
        '                          <Cube currency='NOK' rate='8.1100'/>
        '                          <Cube currency='HRK' rate='7.6125'/>
        '                          <Cube currency='RUB' rate='43.7300'/>
        '                          <Cube currency='TRY' rate='2.6857'/>
        '                          <Cube currency='AUD' rate='1.4299'/>
        '                          <Cube currency='BRL' rate='2.9800'/>
        '                          <Cube currency='CAD' rate='1.4029'/>
        '                          <Cube currency='CNY' rate='8.2709'/>
        '                          <Cube currency='HKD' rate='10.4801'/>
        '                          <Cube currency='IDR' rate='15068.43'/>
        '                          <Cube currency='ILS' rate='4.8208'/>
        '                          <Cube currency='INR' rate='83.5980'/>
        '                          <Cube currency='KRW' rate='1453.88'/>
        '                          <Cube currency='MXN' rate='17.8128'/>
        '                          <Cube currency='MYR' rate='4.3222'/>
        '                          <Cube currency='NZD' rate='1.6269'/>
        '                          <Cube currency='PHP' rate='58.346'/>
        '                          <Cube currency='SGD' rate='1.6918'/>
        '                          <Cube currency='THB' rate='42.488'/>
        '                          <Cube currency='ZAR' rate='13.5072'/>
        '                      </Cube>
        '                  </Cube>
        '              </gesmes:Envelope>

        'Dim xmlreader As Xml.XmlReader = Xml.XmlReader.Create(url)
        'Dim ds As New DataSet()
        'ds.ReadXml(xmlreader)


        'Dim rate As String = ""
        'Dim TRYIndex As Integer = rp.IndexOf("<Cube currency='TRY' rate='")
        'If (TRYIndex > -1) Then
        '    Dim from1 As Integer = TRYIndex + ("<Cube currency='TRY' rate='".Length)
        '    Dim to1 As Integer = rp.IndexOf("'/>", TRYIndex)
        '    rate = rp.Substring(from1, to1 - from1)
        'End If

        'Double.TryParse(rate.Replace(".", ","), System.Globalization.NumberStyles.Any, culture, TRYRate)


        'Catch ex As Exception
        '    Throw New System.Exception(ex.Message, ex)
        'End Try
    End Sub


    Private Shared Sub GetEURORates_SaveToDB(ratesxml As String)
        'Try

        'Dim url As String = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"
        'Dim rp As String = clsWebPageProcessor.GetPageContent(url, 15000)
        Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture("el-GR")
        Dim doc As XmlDocument = New XmlDocument()
        doc.LoadXml(ratesxml)
        Dim NodeIter As XmlNodeList = doc.SelectNodes("*")
        If (NodeIter.Count > 0) Then
            NodeIter = NodeIter(0).SelectNodes("*")
            If (NodeIter.Count = 3) Then
                NodeIter = NodeIter(2).SelectNodes("*")
                If (NodeIter.Count = 1) Then
                    NodeIter = NodeIter(0).SelectNodes("*")
                End If
            End If
        End If

        If (NodeIter.Count > 0) Then
            Using con As New Data.SqlClient.SqlConnection(My.Settings.AppDBconnectionString)


                Using command As SqlCommand = DataHelpers.GetSqlCommand(con, "select * from SYS_Currencies")
                    Using adapter As New SqlDataAdapter(command)
                        Using cmdBuilder As SqlCommandBuilder = New SqlCommandBuilder(adapter)
                            Using ds As New DataSet()
                                Try
                                    adapter.Fill(ds)
                                    For cnt As Integer = 0 To NodeIter.Count - 1
                                        Dim currency As String = NodeIter(cnt).Attributes("currency").Value
                                        Dim rate As String = NodeIter(cnt).Attributes("rate").Value
                                        Dim TRYRate As Double = 0
                                        Double.TryParse(rate.Replace(".", ","), System.Globalization.NumberStyles.Any, culture, TRYRate)


                                        Dim dr As DataRow
                                        Dim drs As DataRow() = ds.Tables(0).Select("CurrencyName='" & currency & "'")
                                        If (drs.Length = 0) Then
                                            dr = ds.Tables(0).NewRow()
                                            ds.Tables(0).Rows.Add(dr)
                                            dr("CurrencyID") = cnt + 1
                                            dr("CurrencyName") = currency
                                            dr("Rate") = TRYRate
                                        Else
                                            drs(0)("Rate") = TRYRate
                                        End If

                                    Next
                                    Try
                                        command.Connection.Open()
                                        adapter.Update(ds.Tables(0))
                                    Catch ex As Exception
                                        Throw New System.Exception(ex.Message, ex)
                                    End Try
                                Catch ex As Exception
                                    Throw New System.Exception(ex.Message, ex)
                                End Try
                            End Using
                        End Using
                    End Using
                End Using
            End Using











        End If

        'Catch ex As Exception
        '    Throw New System.Exception(ex.Message, ex)
        'End Try
    End Sub

#End Region


#Region "PHOTOS COMPARER using Google API"



    Private Sub PerformImageComparison_ProgressChanged(sender As Object, e As GoomenaImageCompare.ProgressChangedEventArgs)
        bw.ReportProgress(-1, "Image Comparison --> " & e.CurrentUrl)
    End Sub


    Private Sub PerformImageComparison_Complete(sender As Object, e As GoomenaImageCompare.AlertEventArgs)

        If (customerIDList.Count = 0) Then Return
        Using con As New Data.SqlClient.SqlConnection(My.Settings.AppDBconnectionString)


            Using command As SqlCommand = DataHelpers.GetSqlCommand(con, "")



                Try

                    If (e.uuiListData.Count > 0) Then

                        Using _CMSDBDataContext As New CMSDBDataContext(command.Connection.ConnectionString)


                            Try

                                For cnt = 0 To e.uuiListData.Count - 1
                                    Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = e.uuiListData(cnt)
                                    Dim dr As System.Data.DataRow = customerIDList(itm.InputUrl)

                                    Dim ps As New EUS_CustomerPhotosSimilar()
                                    ps.CustomerID = dr("CustomerID")
                                    ps.CustomerPhotosID = dr("CustomerPhotosID")
                                    ps.DateTimeCreated = Date.UtcNow
                                    ps.PhotoSimilarity = itm.Similarity
                                    ps.PhotoURL = itm.FoundUrl
                                    _CMSDBDataContext.EUS_CustomerPhotosSimilars.InsertOnSubmit(ps)

                                Next

                                _CMSDBDataContext.SubmitChanges()
                            Catch ex As Exception
                                Throw New System.Exception(ex.Message, ex)

                            End Try
                        End Using
                    End If


                    Dim sb As New System.Text.StringBuilder()
                    For cnt = 0 To e.uuiListData.Count - 1
                        Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = e.uuiListData(cnt)

                        If (customerIDList.ContainsKey(itm.InputUrl)) Then
                            Dim dr As System.Data.DataRow = customerIDList(itm.InputUrl)
                            Dim CustomerPhotosID As Integer = dr("CustomerPhotosID")

                            Dim sql As String = "update EUS_CustomerPhotos set PhotoSimilarity=@PhotoSimilarity[COUNT] where CustomerPhotosID=@CustomerPhotosID[COUNT];"
                            sql = sql.Replace("[COUNT]", cnt)
                            sb.AppendLine(sql)
                            command.Parameters.AddWithValue("@PhotoSimilarity" & cnt, 1)
                            command.Parameters.AddWithValue("@CustomerPhotosID" & cnt, CustomerPhotosID)
                            customerIDList.Remove(itm.InputUrl)
                        End If
                    Next

                    'For cnt = 0 To e.uuiListData.Count - 1
                    '    '  Dim itm As GoomenaImageCompare.uuiSimilarUrlItem =  e.uuiListData(cnt)
                    'Next


                    Dim keys As String() = New String(customerIDList.Keys.Count - 1) {}
                    customerIDList.Keys.CopyTo(keys, 0)
                    For cnt = 0 To keys.Length - 1
                        Dim index As Integer = cnt + 10
                        Dim dr As System.Data.DataRow = customerIDList(keys(cnt))
                        Dim CustomerPhotosID As Integer = dr("CustomerPhotosID")

                        Dim sql As String = "update EUS_CustomerPhotos set PhotoSimilarity=@PhotoSimilarity[COUNT] where CustomerPhotosID=@CustomerPhotosID[COUNT];"
                        sql = sql.Replace("[COUNT]", index)
                        sb.AppendLine(sql)
                        command.Parameters.AddWithValue("@PhotoSimilarity" & index, 0)
                        command.Parameters.AddWithValue("@CustomerPhotosID" & index, CustomerPhotosID)
                    Next

                    command.CommandText = sb.ToString()
                    DataHelpers.ExecuteNonQuery(command)

                Catch ex As Exception
                    Throw New System.Exception(ex.Message, ex)
                Finally
                    '   isBusy_PerformImageComparison = False
                End Try
            End Using
        End Using
    End Sub



    Private customerIDList As Dictionary(Of String, System.Data.DataRow)
    Private Sub PerformImageComparison()
        Try
            Dim DirectoryName As String = System.IO.Path.GetDirectoryName(Application.ExecutablePath)
            Dim logFile As String = System.IO.Path.Combine(DirectoryName, "ImageComparison.log")
            If (customerIDList Is Nothing) Then bw.ReportProgress(0, "Image Comparison-->log file " & vbTab & logFile)
            customerIDList = New Dictionary(Of String, System.Data.DataRow)

            Try
                System.IO.File.Delete(logFile)
            Catch
            End Try

            Dim sql As String = "EXEC [GetView_EUS_CustomerPhotos_ImageComparison] @recordsToSelect=10;"
            Dim dt As DataTable = DataHelpers.GetDataTable(sql)
            Dim urlList As New List(Of String)

            bw.ReportProgress(0, "Image Comparison-->photos count:" & dt.Rows.Count)
            For cnt = 0 To dt.Rows.Count - 1
                Dim fn As String = dt.Rows(cnt)("FileName")
                Dim customerid As Integer = dt.Rows(cnt)("CustomerID")
                '   Dim CustomerPhotosID As Long = dt.Rows(cnt)("CustomerPhotosID")
                Dim path As String = ProfileHelper.GetProfileImageURL(customerid, fn, 0, False, False)

                urlList.Add(path)
                customerIDList.Add(path, dt.Rows(cnt))
            Next

            bw.ReportProgress(0, "Image Comparison-->starting search...")

            Dim sd As New GoomenaImageCompare.ImageComparison(10000, urlList, logFile)
            AddHandler sd.CallAlert, AddressOf PerformImageComparison_Complete
            AddHandler sd.ProgressChanged, AddressOf PerformImageComparison_ProgressChanged
            sd.getResults()

            '  isBusy_PerformImageComparison = True

        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        End Try
    End Sub


    'Private Sub GetEURORates_SaveToDB(ratesxml As String)
    '    Try

    '        ''Dim url As String = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"
    '        ''Dim rp As String = clsWebPageProcessor.GetPageContent(url, 15000)
    '        'Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture("el-GR")
    '        'Dim doc As XmlDocument = New XmlDocument()
    '        'doc.LoadXml(ratesxml)
    '        'Dim NodeIter As XmlNodeList = doc.SelectNodes("*")
    '        'If (NodeIter.Count > 0) Then
    '        '    NodeIter = NodeIter(0).SelectNodes("*")
    '        '    If (NodeIter.Count = 3) Then
    '        '        NodeIter = NodeIter(2).SelectNodes("*")
    '        '        If (NodeIter.Count = 1) Then
    '        '            NodeIter = NodeIter(0).SelectNodes("*")
    '        '        End If
    '        '    End If
    '        'End If

    '        'If (NodeIter.Count > 0) Then
    '        '    Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand("select * from SYS_Currencies")
    '        '    Dim adapter As New SqlClient.SqlDataAdapter(command)
    '        '    Dim cmdBuilder As SqlClient.SqlCommandBuilder = New SqlClient.SqlCommandBuilder(adapter)
    '        '    Dim ds As New DataSet()

    '        '    Try
    '        '        adapter.Fill(ds)
    '        '    Catch ex As Exception
    '        '        Throw New System.Exception(ex.Message, ex)
    '        '    Finally
    '        '        command.Connection.Close()
    '        '    End Try


    '        '    For cnt As Integer = 0 To NodeIter.Count - 1
    '        '        Dim currency As String = NodeIter(cnt).Attributes("currency").Value
    '        '        Dim rate As String = NodeIter(cnt).Attributes("rate").Value
    '        '        Dim TRYRate As Double = 0
    '        '        Double.TryParse(rate.Replace(".", ","), System.Globalization.NumberStyles.Any, culture, TRYRate)


    '        '        Dim dr As DataRow
    '        '        Dim drs As DataRow() = ds.Tables(0).Select("CurrencyName='" & currency & "'")
    '        '        If (drs.Length = 0) Then
    '        '            dr = ds.Tables(0).NewRow()
    '        '            ds.Tables(0).Rows.Add(dr)
    '        '            dr("CurrencyID") = cnt + 1
    '        '            dr("CurrencyName") = currency
    '        '            dr("Rate") = TRYRate
    '        '        Else
    '        '            drs(0)("Rate") = TRYRate
    '        '        End If

    '        '    Next

    '        '    Try
    '        '        command.Connection.Open()
    '        '        adapter.Update(ds.Tables(0))
    '        '    Catch ex As Exception
    '        '        Throw New System.Exception(ex.Message, ex)
    '        '    Finally
    '        '        command.Connection.Close()
    '        '        command.Dispose()
    '        '        adapter.Dispose()
    '        '    End Try

    '        'End If

    '    Catch ex As Exception
    '        Throw New System.Exception(ex.Message, ex)
    '    End Try
    'End Sub

#End Region


#Region "SendMobileSMSForVirtualProfile"

    Private NextDate_SendMobileSMSForVirtualProfile As DateTime
    Private OLD_virtual_received_message_notify_sms_period_mins As Integer = 0
    Private ProfilesFewRetry As Integer = 0

    Private Sub SendMobileSMSForVirtualProfile()

        Dim notifyNextCheck As Boolean = False
        Dim virtual_received_message_notify_sms_period_mins As Integer = -1

        If (NextDate_SendMobileSMSForVirtualProfile = DateTime.MinValue) Then
            NextDate_SendMobileSMSForVirtualProfile = SupportAppHelper.Get__NextDate_SendMobileSMSForVirtualProfile()
            notifyNextCheck = True

        End If


        If (NextDate_SendMobileSMSForVirtualProfile = DateTime.MinValue) Then
            NextDate_SendMobileSMSForVirtualProfile = DateTime.UtcNow.AddSeconds(-1)

        ElseIf (OLD_virtual_received_message_notify_sms_period_mins <> 0) Then
            virtual_received_message_notify_sms_period_mins = clsConfigValues.GetSYS_ConfigValueDouble2("virtual_received_message_notify_sms_period_mins")
            If (OLD_virtual_received_message_notify_sms_period_mins <> virtual_received_message_notify_sms_period_mins) Then
                NextDate_SendMobileSMSForVirtualProfile = NextDate_SendMobileSMSForVirtualProfile.AddMinutes(virtual_received_message_notify_sms_period_mins - OLD_virtual_received_message_notify_sms_period_mins)
            End If

        End If

        If NextDate_SendMobileSMSForVirtualProfile <= DateTime.UtcNow Then


            Dim sql As String = <sql><![CDATA[
select 
	prm.ProfileID, 
	prf.LoginName, 
	MessagesReceived=Count(prm.ProfileID),
	OlderMessage= MIN(prm.CreatedDate),
	MINProfileReceivedMessageID= MIN(prm.ProfileReceivedMessageID),
	MAXProfileReceivedMessageID= MAX(prm.ProfileReceivedMessageID)
from dbo.SYS_ProfileReceivedMessage prm
inner join dbo.EUS_Messages msg on msg.EUS_MessageID=prm.MessageID
inner join dbo.EUS_Profiles prf on prf.ProfileID=prm.ProfileID
inner join dbo.EUS_Profiles prfSender on msg.FromProfileID=prfSender.ProfileID
where msg.StatusID=0
and prfSender.Country='TR'
group by prm.ProfileID,prf.LoginName
]]></sql>

            Dim Delete_sql As String = <sql><![CDATA[
delete from dbo.SYS_ProfileReceivedMessage
where ProfileReceivedMessageID in (
    select 
        prm.ProfileReceivedMessageID
    from dbo.SYS_ProfileReceivedMessage prm
    inner join dbo.EUS_Messages msg on msg.EUS_MessageID=prm.MessageID
    inner join dbo.EUS_Profiles prf on prf.ProfileID=prm.ProfileID
    inner join dbo.EUS_Profiles prfSender on msg.FromProfileID=prfSender.ProfileID
    where msg.StatusID=0
    and prfSender.Country='TR'
)
]]></sql>

            Dim Delete_sql_where As String = <sql><![CDATA[
    (
        ProfileReceivedMessageID>=[MINProfileReceivedMessageID]
        and ProfileReceivedMessageID<=[MAXProfileReceivedMessageID]
        and ProfileID=[ProfileID]
    )
    OR
]]></sql>


            Using dt As DataTable = DataHelpers.GetDataTable(sql)


                If (dt.Rows.Count > 0) Then

                    Dim subject As String = "Virtual profiles received messages" & vbCr
                    Dim _phone As String = clsConfigValues.GetSYS_ConfigValue("virtual_received_message_notify_sms_phone")

                    Dim Delete_sql_where_result As String = ""
                    Dim LOGINS_Separator As String = ","
                    Dim LOGINS As New System.Text.StringBuilder()
                    For cnt = 0 To dt.Rows.Count - 1
                        Dim dr As DataRow = dt.Rows(cnt)

                        If ((subject.Length + LOGINS.Length) > 155) Then Exit For
                        LOGINS.Append(dr("LoginName") & LOGINS_Separator)

                        Dim whr As String = Delete_sql_where
                        whr = whr.Replace("[MINProfileReceivedMessageID]", dr("MINProfileReceivedMessageID"))
                        whr = whr.Replace("[MAXProfileReceivedMessageID]", dr("MAXProfileReceivedMessageID"))
                        whr = whr.Replace("[ProfileID]", dr("ProfileID"))
                        Delete_sql_where_result = Delete_sql_where_result & vbCrLf & whr
                    Next

                    If (LOGINS.Length > LOGINS_Separator.Length) Then
                        LOGINS.Remove(LOGINS.Length - LOGINS_Separator.Length, LOGINS_Separator.Length)
                        subject = subject & LOGINS.ToString()
                        If (subject.Length > 159) Then subject = subject.Remove(159, subject - 159)
                    End If

                    If (LOGINS.Length > 20 OrElse (LOGINS.Length > 3 AndAlso ProfilesFewRetry > 4)) Then
                        ProfilesFewRetry = 0

                        If (Not String.IsNullOrEmpty(Delete_sql_where_result)) Then
                            Delete_sql = Delete_sql & "and(" & vbCrLf & _
                            Delete_sql_where_result.Remove(Delete_sql_where_result.LastIndexOf("OR") - 1) & _
                               vbCrLf & ")"

                            DataHelpers.ExecuteNonQuery(Delete_sql)
                        End If

                        clsMobile.SendSMSViaClickatell(_phone, subject, "Soundsoft", "jim2000", "3081457")
                        bw.ReportProgress(0, "sending sms to " & _phone)
                    Else
                        ProfilesFewRetry = ProfilesFewRetry + 1
                        bw.ReportProgress(0, "sms not sent. Profiles are too few! (" & dt.Rows.Count & ")")
                    End If

                End If
            End Using
            notifyNextCheck = True
        End If

        If (virtual_received_message_notify_sms_period_mins = -1) Then
            virtual_received_message_notify_sms_period_mins = clsConfigValues.GetSYS_ConfigValueDouble2("virtual_received_message_notify_sms_period_mins")
        End If
        NextDate_SendMobileSMSForVirtualProfile = DateTime.UtcNow.AddMinutes(virtual_received_message_notify_sms_period_mins)
        OLD_virtual_received_message_notify_sms_period_mins = virtual_received_message_notify_sms_period_mins

        If (notifyNextCheck) Then
            bw.ReportProgress(0, "Send Mobile SMS For Virtual Profile-->next check @ " & NextDate_SendMobileSMSForVirtualProfile.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss"))
        End If
    End Sub



#End Region


#Region "MaintainDBIndexes"
    Private NextDate_MaintainDBIndexes As DateTime
    Private MI_hourCheck As Integer = 4
    Private MI_minsCheck As Integer = 30

    Public Sub MaintainDBIndexes()
        Dim performMaintenance As Boolean = False
        Dim nextDateToCheck As DateTime = DateTime.UtcNow.Date.AddDays(1).AddHours(MI_hourCheck).AddMinutes(MI_minsCheck)

        Try
            Dim maintenance_time As String = clsConfigValues.GetSYS_ConfigValue("support_app_db_indexes_maintenance_time")
            If (Not String.IsNullOrEmpty(maintenance_time)) Then
                Dim parts As String() = maintenance_time.Split(":")
                If (parts.Length = 2) Then
                    Dim hour As String = parts(0)
                    Dim mins As String = parts(1)

                    Integer.TryParse(hour, MI_hourCheck)
                    Integer.TryParse(mins, MI_minsCheck)
                    nextDateToCheck = DateTime.UtcNow.Date.AddDays(1).AddHours(MI_hourCheck).AddMinutes(MI_minsCheck)
                End If
            End If
        Catch ex As Exception
            bw.ReportProgress(0, "Indexes Maintenance--> " & ex.ToString())
        End Try

        ' first time running
        If (NextDate_MaintainDBIndexes = DateTime.MinValue) Then
            'default,  perform maintenance today at morning 5 o'clock 
            NextDate_MaintainDBIndexes = DateTime.UtcNow.Date.AddHours(MI_hourCheck).AddMinutes(MI_minsCheck)

            If (DateTime.UtcNow.Hour > MI_hourCheck + 1) Then
                ' perform maintenance next day at morning 5 o'clock 
                NextDate_MaintainDBIndexes = nextDateToCheck
            End If
            bw.ReportProgress(0, "Indexes Maintenance--> start set @ " & NextDate_MaintainDBIndexes.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss"))
        End If


        Dim currentDate As DateTime = DateTime.UtcNow.Date
        Dim currentDateMin As DateTime = currentDate.AddHours(DateTime.UtcNow.Hour).AddMinutes(-30)
        Dim currentDateMax As DateTime = currentDate.AddHours(DateTime.UtcNow.Hour).AddMinutes(30)

        ' one hour range, to start maintenance
        If (NextDate_MaintainDBIndexes >= currentDateMin AndAlso
            NextDate_MaintainDBIndexes < currentDateMax) Then

            ' start task
            performMaintenance = True

        ElseIf (NextDate_MaintainDBIndexes >= currentDateMin AndAlso
                NextDate_MaintainDBIndexes < currentDate.AddDays(1)) Then
            'maintenance is before the end of current day

            ''do nothing

        ElseIf (NextDate_MaintainDBIndexes < currentDateMin) Then
            'maintenance time has passed, shedule for next day

            NextDate_MaintainDBIndexes = nextDateToCheck

        End If

        If (performMaintenance) Then
            Dim allowTask As Boolean = CBool(CInt(clsConfigValues.GetSYS_ConfigValueDouble2("support_app_allow_db_indexes_maintenance")))
            If (allowTask) Then
                bw.ReportProgress(0, "Indexes Maintenance-->task allowed ")

                Dim task As New clsIndexesMaintenanceHelper()
                AddHandler task.TaskCompeleted, AddressOf clsIndexesMaintenanceHelper_TaskCompeleted

                Dim thr As New System.Threading.Thread(AddressOf task.PerformTask) With {.IsBackground = True}
                'thr.TrySetApartmentState(Threading.ApartmentState.STA)
                thr.Start()
            End If

            NextDate_MaintainDBIndexes = nextDateToCheck

            bw.ReportProgress(0, "Indexes Maintenance-->started ")
            bw.ReportProgress(0, "Indexes Maintenance-->next check @ " & NextDate_MaintainDBIndexes.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss"))
        Else
            bw.ReportProgress(0, "Indexes Maintenance-->skipped, next check @ " & NextDate_MaintainDBIndexes.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss"))
        End If

    End Sub

    Private Sub clsIndexesMaintenanceHelper_TaskCompeleted(sender As Object, e As clsIndexesMaintenanceEventArgs)
        If (e.IsComplete) Then
            bw.ReportProgress(0, "Indexes Maintenance-->complete")

            Dim nexDateToCheck As DateTime = DateTime.UtcNow.Date.AddDays(1).AddHours(MI_hourCheck).AddMinutes(MI_minsCheck)
            NextDate_MaintainDBIndexes = nexDateToCheck

        ElseIf (e.CompleteWithError) Then
            bw.ReportProgress(0, "Indexes Maintenance-->complete with error")
            bw.ReportProgress(0, "Indexes Maintenance--> " & e.ErrorMessage)

            Dim nexDateToCheck As DateTime = DateTime.UtcNow.Date.AddHours(MI_hourCheck).AddMinutes(MI_minsCheck)
            If (DateTime.UtcNow < nexDateToCheck.AddHours(1)) Then
                NextDate_MaintainDBIndexes = nexDateToCheck
            End If
            bw.ReportProgress(0, "Indexes Maintenance-->next check @ " & NextDate_MaintainDBIndexes.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss"))
        End If
    End Sub

#End Region



#Region "ClearTablesData"
    Private NextDate_ClearTablesData As DateTime

    Public Sub ClearTablesData()
        ' Dim performMaintenance As Boolean = False

        ' first time running
        If (NextDate_ClearTablesData = DateTime.MinValue) Then
            NextDate_ClearTablesData = DateTime.UtcNow.AddMinutes(-1)
        End If


        If (NextDate_ClearTablesData <= DateTime.UtcNow) Then
            Dim thr As New System.Threading.Thread(AddressOf PerformClearTablesData) With {.IsBackground = True}
            'thr.TrySetApartmentState(Threading.ApartmentState.STA)
            thr.Start()

            TryAddMessage(0, "Clear Tables-->started")
            NextDate_ClearTablesData = DateTime.UtcNow.AddMinutes(30)
        End If

    End Sub

    Private Sub PerformClearTablesData()
        Try
            Dim command_timeout As Integer = CInt(clsConfigValues.GetSYS_ConfigValueDouble2("support_app_clear_expired_data_command_timeout_sec"))
            If (command_timeout < 1) Then
                command_timeout = 30
            End If

            '   Dim continueTask As Boolean = True
            Dim sql As String = "exec [SYSDB_CLEAR_EXPIRED_DATA] @clear_SYS_EmailsQueue=@clear_SYS_EmailsQueue,@clear_SYS_WebStatistics=@clear_SYS_WebStatistics"
            Using con As New Data.SqlClient.SqlConnection(My.Settings.AppDBconnectionString)

                Using cmd As Data.SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, sql)
                    cmd.Parameters.AddWithValue("@clear_SYS_EmailsQueue", True)
                    cmd.Parameters.AddWithValue("@clear_SYS_WebStatistics", True)
                    cmd.CommandTimeout = command_timeout

                    DataHelpers.ExecuteNonQuery(cmd)
                End Using

            End Using

        Catch ex As Exception
        Finally
            TryAddMessage(0, "Clear Tables-->stopped, next check @ " & NextDate_ClearTablesData.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss"))
        End Try
    End Sub

#End Region


End Class



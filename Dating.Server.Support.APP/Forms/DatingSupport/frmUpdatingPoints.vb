﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Linq
Imports System.Xml
Imports System.Xml.linq

Public Class frmUpdatingPoints

    Dim tmrTaskInterval As Integer


    Private Sub frmUpdatingPoints_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GroupControl1.Text = Me.Text
        'bwDeletingPhotos.RunWorkerAsync()
        BarStaticItemStatus.Caption = "Not running"

        StartTimer()
    End Sub


    Private Sub frmSendingEmails_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

            bw.CancelAsync()
            bw.Dispose()
            bw = Nothing
            tmrTask.Enabled = False
            Me.Dispose()

        Catch ex As Exception
        End Try
    End Sub


    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork

        Using _dc As New CMSDBDataContext(My.Settings.AppDBconnectionString)


            Try

                ' generous account type id
                Dim Gendersid As Integer = (From itm In _dc.EUS_LISTS_Genders
                                               Where itm.ConstantName = "Male"
                                               Select itm.GenderId).First()

                ' all profiles that should be affected
                Dim profileIds = (From itm In _dc.EUS_Profiles
                                Where itm.Status = ProfileStatusEnum.Approved AndAlso _
                                        itm.GenderId = Gendersid AndAlso _
                                        itm.IsMaster = True
                                Select itm.ProfileID, itm.LoginName, itm.GenderId).GetEnumerator()


                While (profileIds.MoveNext())
                    If bw.CancellationPending = True Then
                        e.Cancel = True
                        Exit While
                    Else
                        Try

                            If (ProfileHelper.IsMale(profileIds.Current.GenderId)) Then
                                '                            Dim sql As String = <sql><![CDATA[
                                'declare @creditsofyear int, @unlocksofyear int;

                                'select @creditsofyear=SUM(CREDITS) 
                                'from [EUS_CustomerCredits]
                                'where customerid= @customerid
                                'and DateTimeCreated  between dateadd(year, -1, getutcdate()) and  getutcdate()
                                'and isnull(validfordays ,0)<>0

                                'select @unlocksofyear=COUNT(*)
                                'from EUS_UnlockedConversations
                                'where FromProfileId = @customerid
                                'and DateTimeCreated  between dateadd(year, -1, getutcdate()) and  getutcdate()

                                'if(@creditsofyear > 0 and @unlocksofyear > 0)
                                'begin
                                '    update EUS_Profiles 
                                '    set 
                                '        PointsCredits = @creditsofyear,
                                '        PointsUnlocks = @unlocksofyear
                                '    where profileid= @customerid or MirrorProfileID=@customerid
                                'end
                                '                    ]]></sql>.Value

                                Dim sql As String = String.Format("EXEC UpdateCustomerPoints {0};", profileIds.Current.ProfileID)
                                '   Dim rowsAffected =
                                DataHelpers.ExecuteNonQuery(sql)
                            Else

                            End If
                            bw.ReportProgress(0, "points updated customer """ & profileIds.Current.LoginName & """")

                        Catch ex As Exception
                            bw.ReportProgress(0, " Exc. " & ex.Message)
                        End Try

                    End If

                End While

            Catch ex As Exception
                bw.ReportProgress(0, ex.Message)
      
            End Try
        End Using
    End Sub

    Private Sub AddMessage(message As String)
        'txOutput.Text = "[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf & txOutput.Text
        If (message IsNot Nothing) Then
            txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf)
        End If
        Try
            If (txtOutput.Lines.Length > 5001) Then
                Dim index As Integer = 0
                Dim linesCount As Integer = txtOutput.Lines.Length
                Dim txtOutputText As String = txtOutput.Text
                For cnt = 0 To linesCount - 5000
                    index = txtOutputText.IndexOf(vbCr, index) + vbCr.Length
                Next
                If (index > 0) Then
                    txtOutput.Text = txtOutput.Text.Remove(0, index).TrimStart({ControlChars.Cr, ControlChars.Lf})
                End If
            End If
        Catch
        End Try
    End Sub


    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then
            Me.BarStaticItemStatus.Caption = "Canceled"
            tmrTask.Enabled = False
        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub


    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub

    Private Sub BarButtonItemRun_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRun.ItemClick
        Try
            tmrTask.Enabled = False

            If Not bw.IsBusy = True Then
                BarStaticItemStatus.Caption = "Running"
                bw.RunWorkerAsync()
                BarButtonItemRun.Enabled = False
            Else
                Try
                    bw.CancelAsync()
                    bw.RunWorkerAsync()
                    BarStaticItemStatus.Caption = "Running"
                Catch ex As Exception
                    AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick -> bw.IsBusy" & vbCrLf & ex.Message)
                End Try
            End If

        Catch ex As Exception
            AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick" & vbCrLf & ex.Message)
        Finally
            tmrTask.Enabled = True
            tmrTask.Interval = tmrTaskInterval
        End Try
    End Sub


    Private Sub BarButtonItemCancelJob_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCancelJob.ItemClick
        bw.CancelAsync()
        tmrTask.Enabled = False
        BarButtonItemRun.Enabled = True
    End Sub

    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        AddMessage(CStr(e.UserState))
    End Sub


    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        If Not bw.IsBusy = True Then
            BarStaticItemStatus.Caption = "Running"
            bw.RunWorkerAsync()
        End If
        tmrTask.Interval = tmrTaskInterval
    End Sub


    Private Sub StartTimer()
        Dim r As New Random(System.DateTime.Now.Millisecond)
        Dim interval As Integer = r.Next(30, 60)

        If (tmrTaskInterval = 0) Then tmrTaskInterval = tmrTask.Interval
        tmrTask.Interval = interval * 1000
        tmrTask.Enabled = True

        tmrTask.Enabled = True
        tmrTask.Start()

        BarStaticItemStatus.Caption = "First run in " & tmrTask.Interval / 1000 & " sec"
    End Sub

End Class

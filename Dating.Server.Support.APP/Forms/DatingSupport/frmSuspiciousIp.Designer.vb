﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSuspiciousIp
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSuspiciousIp))
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.tbMain = New DevExpress.XtraTab.XtraTabControl()
        Me.tbLog = New DevExpress.XtraTab.XtraTabPage()
        Me.txtOutput = New DevExpress.XtraEditors.MemoEdit()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.tbBlockedIps = New DevExpress.XtraTab.XtraTabPage()
        Me.grBlockedIps = New DevExpress.XtraGrid.GridControl()
        Me.bdInblock = New System.Windows.Forms.BindingSource()
        Me.DsInBlock = New Dating.Server.Datasets.DLL.dsBlockSuspicious()
        Me.grvBlocked = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBlockedIp = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIp = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDatetimeBlocked = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIsInBlock = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDatetimeUblocked = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCountTimesBlocked = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager()
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItemRun = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemCancelJob = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemClear = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.BarStaticItemStatus = New DevExpress.XtraBars.BarStaticItem()
        Me.BarDockControl3 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl4 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl2 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl1 = New DevExpress.XtraBars.BarDockControl()
        Me.tbShortHistory = New DevExpress.XtraTab.XtraTabPage()
        Me.grHistory = New DevExpress.XtraGrid.GridControl()
        Me.bdHistory = New System.Windows.Forms.BindingSource()
        Me.DsHistory = New Dating.Server.Datasets.DLL.dsBlockSuspicious()
        Me.grvHistory = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBlockedIp1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIp1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDatetimeBlocked1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIsInBlock1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDatetimeUblocked1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCountTimesBlocked1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.BarDockControl7 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl8 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl6 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl5 = New DevExpress.XtraBars.BarDockControl()
        Me.bw = New System.ComponentModel.BackgroundWorker()
        Me.tmrTask = New System.Windows.Forms.Timer()
        Me.brBlockedIps = New DevExpress.XtraBars.BarManager()
        Me.Bar4 = New DevExpress.XtraBars.Bar()
        Me.brAddNewIp = New DevExpress.XtraBars.BarButtonItem()
        Me.brInBlockRefresh = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar5 = New DevExpress.XtraBars.Bar()
        Me.BarManager2 = New DevExpress.XtraBars.BarManager()
        Me.Bar6 = New DevExpress.XtraBars.Bar()
        Me.brRefreshHistory = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar7 = New DevExpress.XtraBars.Bar()
        Me.brDelete = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.tbMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbMain.SuspendLayout()
        Me.tbLog.SuspendLayout()
        CType(Me.txtOutput.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbBlockedIps.SuspendLayout()
        CType(Me.grBlockedIps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bdInblock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsInBlock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvBlocked, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbShortHistory.SuspendLayout()
        CType(Me.grHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bdHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.brBlockedIps, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.tbMain)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(700, 521)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Blocking Ips"
        '
        'tbMain
        '
        Me.tbMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbMain.Location = New System.Drawing.Point(2, 33)
        Me.tbMain.Name = "tbMain"
        Me.tbMain.SelectedTabPage = Me.tbLog
        Me.tbMain.Size = New System.Drawing.Size(696, 486)
        Me.tbMain.TabIndex = 5
        Me.tbMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.tbLog, Me.tbBlockedIps, Me.tbShortHistory})
        '
        'tbLog
        '
        Me.tbLog.Controls.Add(Me.txtOutput)
        Me.tbLog.Controls.Add(Me.barDockControlLeft)
        Me.tbLog.Controls.Add(Me.barDockControlRight)
        Me.tbLog.Controls.Add(Me.barDockControlBottom)
        Me.tbLog.Controls.Add(Me.barDockControlTop)
        Me.tbLog.Name = "tbLog"
        Me.tbLog.Size = New System.Drawing.Size(690, 458)
        Me.tbLog.Text = "Log"
        '
        'txtOutput
        '
        Me.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtOutput.Location = New System.Drawing.Point(0, 22)
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.Size = New System.Drawing.Size(690, 411)
        Me.txtOutput.TabIndex = 0
        '  Me.txtOutput.UseOptimizedRendering = True
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 22)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 411)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(690, 22)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 411)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 433)
        Me.barDockControlBottom.Size = New System.Drawing.Size(690, 25)
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(690, 22)
        '
        'tbBlockedIps
        '
        Me.tbBlockedIps.Controls.Add(Me.grBlockedIps)
        Me.tbBlockedIps.Controls.Add(Me.BarDockControl3)
        Me.tbBlockedIps.Controls.Add(Me.BarDockControl4)
        Me.tbBlockedIps.Controls.Add(Me.BarDockControl2)
        Me.tbBlockedIps.Controls.Add(Me.BarDockControl1)
        Me.tbBlockedIps.Name = "tbBlockedIps"
        Me.tbBlockedIps.Size = New System.Drawing.Size(690, 458)
        Me.tbBlockedIps.Text = "Blocked Ips"
        '
        'grBlockedIps
        '
        Me.grBlockedIps.DataSource = Me.bdInblock
        Me.grBlockedIps.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grBlockedIps.Location = New System.Drawing.Point(0, 24)
        Me.grBlockedIps.MainView = Me.grvBlocked
        Me.grBlockedIps.MenuManager = Me.BarManager1
        Me.grBlockedIps.Name = "grBlockedIps"
        Me.grBlockedIps.Size = New System.Drawing.Size(690, 407)
        Me.grBlockedIps.TabIndex = 0
        Me.grBlockedIps.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvBlocked})
        '
        'bdInblock
        '
        Me.bdInblock.DataMember = "SYS_BlockedIps"
        Me.bdInblock.DataSource = Me.DsInBlock
        '
        'DsInBlock
        '
        Me.DsInBlock.DataSetName = "dsBlockSuspicious"
        Me.DsInBlock.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'grvBlocked
        '
        Me.grvBlocked.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBlockedIp, Me.colIp, Me.colDatetimeBlocked, Me.colIsInBlock, Me.colDatetimeUblocked, Me.colCountTimesBlocked})
        Me.grvBlocked.GridControl = Me.grBlockedIps
        Me.grvBlocked.Name = "grvBlocked"
        '
        'colBlockedIp
        '
        Me.colBlockedIp.Caption = "ID"
        Me.colBlockedIp.FieldName = "BlockedIp"
        Me.colBlockedIp.Name = "colBlockedIp"
        Me.colBlockedIp.OptionsColumn.ReadOnly = True
        Me.colBlockedIp.Visible = True
        Me.colBlockedIp.VisibleIndex = 0
        '
        'colIp
        '
        Me.colIp.FieldName = "Ip"
        Me.colIp.Name = "colIp"
        Me.colIp.Visible = True
        Me.colIp.VisibleIndex = 1
        '
        'colDatetimeBlocked
        '
        Me.colDatetimeBlocked.FieldName = "DatetimeBlocked"
        Me.colDatetimeBlocked.Name = "colDatetimeBlocked"
        Me.colDatetimeBlocked.Visible = True
        Me.colDatetimeBlocked.VisibleIndex = 2
        '
        'colIsInBlock
        '
        Me.colIsInBlock.FieldName = "IsInBlock"
        Me.colIsInBlock.Name = "colIsInBlock"
        '
        'colDatetimeUblocked
        '
        Me.colDatetimeUblocked.Caption = "Datetime to Unblock"
        Me.colDatetimeUblocked.FieldName = "DatetimeUblocked"
        Me.colDatetimeUblocked.Name = "colDatetimeUblocked"
        Me.colDatetimeUblocked.Visible = True
        Me.colDatetimeUblocked.VisibleIndex = 3
        '
        'colCountTimesBlocked
        '
        Me.colCountTimesBlocked.FieldName = "CountTimesBlocked"
        Me.colCountTimesBlocked.Name = "colCountTimesBlocked"
        Me.colCountTimesBlocked.Visible = True
        Me.colCountTimesBlocked.VisibleIndex = 4
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1, Me.Bar2})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me.tbLog
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItemClear, Me.BarStaticItemStatus, Me.BarButtonItemRun, Me.BarButtonItemCancelJob})
        Me.BarManager1.MainMenu = Me.Bar1
        Me.BarManager1.MaxItemId = 4
        Me.BarManager1.StatusBar = Me.Bar2
        '
        'Bar1
        '
        Me.Bar1.BarName = "Custom 3"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.FloatLocation = New System.Drawing.Point(480, 227)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemRun), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemCancelJob, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemClear, True)})
        Me.Bar1.OptionsBar.MultiLine = True
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Custom 3"
        '
        'BarButtonItemRun
        '
        Me.BarButtonItemRun.Caption = "Run"
        Me.BarButtonItemRun.Id = 2
        Me.BarButtonItemRun.Name = "BarButtonItemRun"
        '
        'BarButtonItemCancelJob
        '
        Me.BarButtonItemCancelJob.Caption = "Cancel job"
        Me.BarButtonItemCancelJob.Id = 3
        Me.BarButtonItemCancelJob.Name = "BarButtonItemCancelJob"
        '
        'BarButtonItemClear
        '
        Me.BarButtonItemClear.Caption = "Clear"
        Me.BarButtonItemClear.Id = 0
        Me.BarButtonItemClear.Name = "BarButtonItemClear"
        '
        'Bar2
        '
        Me.Bar2.BarName = "Custom 4"
        Me.Bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarStaticItemStatus)})
        Me.Bar2.OptionsBar.AllowQuickCustomization = False
        Me.Bar2.OptionsBar.DrawDragBorder = False
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Custom 4"
        '
        'BarStaticItemStatus
        '
        Me.BarStaticItemStatus.Caption = "[STATUS]"
        Me.BarStaticItemStatus.Id = 1
        Me.BarStaticItemStatus.Name = "BarStaticItemStatus"
        Me.BarStaticItemStatus.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'BarDockControl3
        '
        Me.BarDockControl3.CausesValidation = False
        Me.BarDockControl3.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl3.Location = New System.Drawing.Point(0, 24)
        Me.BarDockControl3.Size = New System.Drawing.Size(0, 407)
        '
        'BarDockControl4
        '
        Me.BarDockControl4.CausesValidation = False
        Me.BarDockControl4.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl4.Location = New System.Drawing.Point(690, 24)
        Me.BarDockControl4.Size = New System.Drawing.Size(0, 407)
        '
        'BarDockControl2
        '
        Me.BarDockControl2.CausesValidation = False
        Me.BarDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl2.Location = New System.Drawing.Point(0, 431)
        Me.BarDockControl2.Size = New System.Drawing.Size(690, 27)
        '
        'BarDockControl1
        '
        Me.BarDockControl1.CausesValidation = False
        Me.BarDockControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl1.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl1.Size = New System.Drawing.Size(690, 24)
        '
        'tbShortHistory
        '
        Me.tbShortHistory.Controls.Add(Me.grHistory)
        Me.tbShortHistory.Controls.Add(Me.BarDockControl7)
        Me.tbShortHistory.Controls.Add(Me.BarDockControl8)
        Me.tbShortHistory.Controls.Add(Me.BarDockControl6)
        Me.tbShortHistory.Controls.Add(Me.BarDockControl5)
        Me.tbShortHistory.Name = "tbShortHistory"
        Me.tbShortHistory.Size = New System.Drawing.Size(690, 458)
        Me.tbShortHistory.Text = "ShortHistory"
        '
        'grHistory
        '
        Me.grHistory.DataSource = Me.bdHistory
        Me.grHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grHistory.Location = New System.Drawing.Point(0, 24)
        Me.grHistory.MainView = Me.grvHistory
        Me.grHistory.MenuManager = Me.BarManager1
        Me.grHistory.Name = "grHistory"
        Me.grHistory.Size = New System.Drawing.Size(690, 411)
        Me.grHistory.TabIndex = 1
        Me.grHistory.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.grvHistory})
        '
        'bdHistory
        '
        Me.bdHistory.DataMember = "SYS_BlockedIps"
        Me.bdHistory.DataSource = Me.DsHistory
        '
        'DsHistory
        '
        Me.DsHistory.DataSetName = "dsBlockSuspicious"
        Me.DsHistory.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'grvHistory
        '
        Me.grvHistory.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBlockedIp1, Me.colIp1, Me.colDatetimeBlocked1, Me.colIsInBlock1, Me.colDatetimeUblocked1, Me.colCountTimesBlocked1})
        Me.grvHistory.GridControl = Me.grHistory
        Me.grvHistory.Name = "grvHistory"
        '
        'colBlockedIp1
        '
        Me.colBlockedIp1.Caption = "ID"
        Me.colBlockedIp1.FieldName = "BlockedIp"
        Me.colBlockedIp1.Name = "colBlockedIp1"
        Me.colBlockedIp1.OptionsColumn.ReadOnly = True
        Me.colBlockedIp1.Visible = True
        Me.colBlockedIp1.VisibleIndex = 0
        '
        'colIp1
        '
        Me.colIp1.FieldName = "Ip"
        Me.colIp1.Name = "colIp1"
        Me.colIp1.Visible = True
        Me.colIp1.VisibleIndex = 1
        '
        'colDatetimeBlocked1
        '
        Me.colDatetimeBlocked1.FieldName = "DatetimeBlocked"
        Me.colDatetimeBlocked1.Name = "colDatetimeBlocked1"
        Me.colDatetimeBlocked1.Visible = True
        Me.colDatetimeBlocked1.VisibleIndex = 2
        '
        'colIsInBlock1
        '
        Me.colIsInBlock1.FieldName = "IsInBlock"
        Me.colIsInBlock1.Name = "colIsInBlock1"
        '
        'colDatetimeUblocked1
        '
        Me.colDatetimeUblocked1.FieldName = "DatetimeUblocked"
        Me.colDatetimeUblocked1.Name = "colDatetimeUblocked1"
        Me.colDatetimeUblocked1.Visible = True
        Me.colDatetimeUblocked1.VisibleIndex = 3
        '
        'colCountTimesBlocked1
        '
        Me.colCountTimesBlocked1.FieldName = "CountTimesBlocked"
        Me.colCountTimesBlocked1.Name = "colCountTimesBlocked1"
        Me.colCountTimesBlocked1.Visible = True
        Me.colCountTimesBlocked1.VisibleIndex = 4
        '
        'BarDockControl7
        '
        Me.BarDockControl7.CausesValidation = False
        Me.BarDockControl7.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl7.Location = New System.Drawing.Point(0, 24)
        Me.BarDockControl7.Size = New System.Drawing.Size(0, 411)
        '
        'BarDockControl8
        '
        Me.BarDockControl8.CausesValidation = False
        Me.BarDockControl8.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl8.Location = New System.Drawing.Point(690, 24)
        Me.BarDockControl8.Size = New System.Drawing.Size(0, 411)
        '
        'BarDockControl6
        '
        Me.BarDockControl6.CausesValidation = False
        Me.BarDockControl6.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl6.Location = New System.Drawing.Point(0, 435)
        Me.BarDockControl6.Size = New System.Drawing.Size(690, 23)
        '
        'BarDockControl5
        '
        Me.BarDockControl5.CausesValidation = False
        Me.BarDockControl5.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl5.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl5.Size = New System.Drawing.Size(690, 24)
        '
        'bw
        '
        Me.bw.WorkerReportsProgress = True
        Me.bw.WorkerSupportsCancellation = True
        '
        'tmrTask
        '
        Me.tmrTask.Interval = 21600000
        '
        'brBlockedIps
        '
        Me.brBlockedIps.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar4, Me.Bar5})
        Me.brBlockedIps.DockControls.Add(Me.BarDockControl1)
        Me.brBlockedIps.DockControls.Add(Me.BarDockControl2)
        Me.brBlockedIps.DockControls.Add(Me.BarDockControl3)
        Me.brBlockedIps.DockControls.Add(Me.BarDockControl4)
        Me.brBlockedIps.Form = Me.tbBlockedIps
        Me.brBlockedIps.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.brAddNewIp, Me.brInBlockRefresh, Me.brDelete})
        Me.brBlockedIps.MainMenu = Me.Bar4
        Me.brBlockedIps.MaxItemId = 3
        Me.brBlockedIps.StatusBar = Me.Bar5
        '
        'Bar4
        '
        Me.Bar4.BarName = "Main menu"
        Me.Bar4.DockCol = 0
        Me.Bar4.DockRow = 0
        Me.Bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar4.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.brAddNewIp, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.brInBlockRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar4.OptionsBar.MultiLine = True
        Me.Bar4.OptionsBar.UseWholeRow = True
        Me.Bar4.Text = "Main menu"
        '
        'brAddNewIp
        '
        Me.brAddNewIp.Caption = "Add New IP"
        Me.brAddNewIp.Glyph = CType(resources.GetObject("brAddNewIp.Glyph"), System.Drawing.Image)
        Me.brAddNewIp.Id = 0
        Me.brAddNewIp.LargeGlyph = CType(resources.GetObject("brAddNewIp.LargeGlyph"), System.Drawing.Image)
        Me.brAddNewIp.Name = "brAddNewIp"
        '
        'brInBlockRefresh
        '
        Me.brInBlockRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.brInBlockRefresh.Caption = "Refresh List"
        Me.brInBlockRefresh.Glyph = CType(resources.GetObject("brInBlockRefresh.Glyph"), System.Drawing.Image)
        Me.brInBlockRefresh.Id = 1
        Me.brInBlockRefresh.LargeGlyph = CType(resources.GetObject("brInBlockRefresh.LargeGlyph"), System.Drawing.Image)
        Me.brInBlockRefresh.Name = "brInBlockRefresh"
        '
        'Bar5
        '
        Me.Bar5.BarName = "Status bar"
        Me.Bar5.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar5.DockCol = 0
        Me.Bar5.DockRow = 0
        Me.Bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar5.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.brDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar5.OptionsBar.AllowQuickCustomization = False
        Me.Bar5.OptionsBar.DrawDragBorder = False
        Me.Bar5.OptionsBar.UseWholeRow = True
        Me.Bar5.Text = "Status bar"
        '
        'BarManager2
        '
        Me.BarManager2.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar6, Me.Bar7})
        Me.BarManager2.DockControls.Add(Me.BarDockControl5)
        Me.BarManager2.DockControls.Add(Me.BarDockControl6)
        Me.BarManager2.DockControls.Add(Me.BarDockControl7)
        Me.BarManager2.DockControls.Add(Me.BarDockControl8)
        Me.BarManager2.Form = Me.tbShortHistory
        Me.BarManager2.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.brRefreshHistory})
        Me.BarManager2.MainMenu = Me.Bar6
        Me.BarManager2.MaxItemId = 1
        Me.BarManager2.StatusBar = Me.Bar7
        '
        'Bar6
        '
        Me.Bar6.BarName = "Main menu"
        Me.Bar6.DockCol = 0
        Me.Bar6.DockRow = 0
        Me.Bar6.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar6.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.brRefreshHistory, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar6.OptionsBar.MultiLine = True
        Me.Bar6.OptionsBar.UseWholeRow = True
        Me.Bar6.Text = "Main menu"
        '
        'brRefreshHistory
        '
        Me.brRefreshHistory.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.brRefreshHistory.Caption = "Refresh List"
        Me.brRefreshHistory.Glyph = CType(resources.GetObject("brRefreshHistory.Glyph"), System.Drawing.Image)
        Me.brRefreshHistory.Id = 0
        Me.brRefreshHistory.LargeGlyph = CType(resources.GetObject("brRefreshHistory.LargeGlyph"), System.Drawing.Image)
        Me.brRefreshHistory.Name = "brRefreshHistory"
        '
        'Bar7
        '
        Me.Bar7.BarName = "Status bar"
        Me.Bar7.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar7.DockCol = 0
        Me.Bar7.DockRow = 0
        Me.Bar7.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar7.OptionsBar.AllowQuickCustomization = False
        Me.Bar7.OptionsBar.DrawDragBorder = False
        Me.Bar7.OptionsBar.UseWholeRow = True
        Me.Bar7.Text = "Status bar"
        '
        'brDelete
        '
        Me.brDelete.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.brDelete.Caption = "Delete"
        Me.brDelete.Glyph = CType(resources.GetObject("brDelete.Glyph"), System.Drawing.Image)
        Me.brDelete.Id = 2
        Me.brDelete.LargeGlyph = CType(resources.GetObject("brDelete.LargeGlyph"), System.Drawing.Image)
        Me.brDelete.Name = "brDelete"
        '
        'frmSuspiciousIp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(700, 521)
        Me.Controls.Add(Me.GroupControl1)
        Me.Name = "frmSuspiciousIp"
        Me.Text = "Blocking Ips"
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.tbMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbMain.ResumeLayout(False)
        Me.tbLog.ResumeLayout(False)
        CType(Me.txtOutput.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbBlockedIps.ResumeLayout(False)
        CType(Me.grBlockedIps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bdInblock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsInBlock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvBlocked, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbShortHistory.ResumeLayout(False)
        CType(Me.grHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bdHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.brBlockedIps, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtOutput As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarButtonItemClear As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bw As System.ComponentModel.BackgroundWorker
    Friend WithEvents tmrTask As System.Windows.Forms.Timer
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents BarStaticItemStatus As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents BarButtonItemRun As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemCancelJob As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents tbMain As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents tbLog As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents tbBlockedIps As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grBlockedIps As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvBlocked As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BarDockControl3 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl4 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl2 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl1 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents brBlockedIps As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar4 As DevExpress.XtraBars.Bar
    Friend WithEvents Bar5 As DevExpress.XtraBars.Bar
    Friend WithEvents tbShortHistory As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grHistory As DevExpress.XtraGrid.GridControl
    Friend WithEvents grvHistory As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents bdInblock As System.Windows.Forms.BindingSource
    Friend WithEvents DsInBlock As Dating.Server.Datasets.DLL.dsBlockSuspicious
    Friend WithEvents bdHistory As System.Windows.Forms.BindingSource
    Friend WithEvents DsHistory As Dating.Server.Datasets.DLL.dsBlockSuspicious
    Friend WithEvents colBlockedIp As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIp As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDatetimeBlocked As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIsInBlock As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDatetimeUblocked As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCountTimesBlocked As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBlockedIp1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIp1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDatetimeBlocked1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIsInBlock1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDatetimeUblocked1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCountTimesBlocked1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents brAddNewIp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents brInBlockRefresh As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarManager2 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar6 As DevExpress.XtraBars.Bar
    Friend WithEvents Bar7 As DevExpress.XtraBars.Bar
    Friend WithEvents BarDockControl5 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl6 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl7 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl8 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents brRefreshHistory As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents brDelete As DevExpress.XtraBars.BarButtonItem
End Class

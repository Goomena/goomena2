﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Text
Imports System.Linq
Imports System.Data.Linq


Public Class frmSendingEmails

    Dim tmrTaskInterval As Integer
    ' Dim isBusy_PerformImageComparison As Boolean
    Dim ConnectionString As String = My.Settings.AppDBconnectionString

    Private Sub frmSendingEmails_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GroupControl1.Text = Me.Text
        BarStaticItemStatus.Caption = "Not running"

        StartTimer()
    End Sub


    Private Sub frmSendingEmails_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

            bw.CancelAsync()
            bw.Dispose()
            bw = Nothing
            tmrTask.Enabled = False
            Me.Dispose()

        Catch ex As Exception
        End Try
    End Sub


    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork
        Dim hdl As New clsSendingEmailsHandler()
        Try
            clsSendingEmailsHandler.SetRetryingPhotosEmail_To_Support()
            Dim emailForSending As SYS_EmailsQueue = clsSendingEmailsHandler.GetNextEmailToSend()

            Dim emailCount As Integer
            Dim r As New Random(System.DateTime.Now.Millisecond)
            Dim continueDate As DateTime = DateTime.UtcNow.AddMinutes(-1)

            SupportAppHelper.NotifyEmailSent(True)


            While emailForSending IsNot Nothing
                While (continueDate > DateTime.UtcNow)
                    System.Threading.Thread.Sleep(200)
                    If bw.CancellationPending = True Then
                        e.Cancel = True
                        Exit Sub
                    End If
                End While

                If bw.CancellationPending = True Then
                    e.Cancel = True
                    Exit While
                End If


                Dim asyncProcessingStarted As Boolean = False

                Try
                    Dim isActive As Boolean
                    If (emailForSending.CustomerId > 0 AndAlso Not DataHelpers.EUS_Profile_IsProfileActive(emailForSending.CustomerId)) Then
                        emailForSending.CustomerId = DataHelpers.EUS_Profiles_GetMasterProfileID(emailForSending.CustomerId)
                    Else
                        isActive = True
                    End If

                    If (Not isActive AndAlso emailForSending.CustomerId > 0 AndAlso Not DataHelpers.EUS_Profile_IsProfileActive(emailForSending.CustomerId)) Then
                        emailForSending.Exception = "Profile Inactive"
                        bw.ReportProgress(0, " Exc. " & "Profile Inactive")
                    ElseIf (String.IsNullOrEmpty(emailForSending.EmailTo)) Then
                        emailForSending.Exception = "Empty email Address"
                        bw.ReportProgress(0, " Exc. " & "Empty email Address")
                    Else

                        ' here we have an email from site to administration, with new photo 
                        ' this photo will be checked on google search
                        If (emailForSending.EmailAction.StartsWith("New.Photo.Google.Check.ID-")) Then
                            'Dim EmailAction As String = "New.Photo.Google.Check.ID-" & newPhoto.CustomerPhotosID
                            Dim sCustomerPhotosID As String = emailForSending.EmailAction.Replace("New.Photo.Google.Check.ID-", "")
                            Dim lCustomerPhotosID As Long = 0
                            Long.TryParse(sCustomerPhotosID, lCustomerPhotosID)
                            If (lCustomerPhotosID > 0) Then
                                PerformImageComparison(lCustomerPhotosID, emailForSending)
                                asyncProcessingStarted = True
                                'emailForSending.Exception = "asyncProcessingStarted"
                                emailCount = emailCount - 1
                            End If
                        End If


                        If (Not asyncProcessingStarted) Then
                            SendEmailInQueue(emailForSending)
                        End If

                    End If

                Catch ex As Exception
                    asyncProcessingStarted = False
                    emailForSending.Exception = ex.ToString()
                    bw.ReportProgress(0, " Exc. " & ex.Message)
                    Try
                        Dim str As String = ""
                        str = "EmailTo: " & emailForSending.EmailTo & vbCrLf & _
                             "Subject: " & emailForSending.Subject & vbCrLf & _
                             "Email Action: " & emailForSending.EmailAction & vbCrLf & vbCrLf & _
                             "Exception : " & emailForSending.Exception
                        clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Sending Emails Exception: " & ex.Message, str, False)
                    Catch
                    End Try
                End Try
                If (Not asyncProcessingStarted) Then
                    clsSendingEmailsHandler.SetEmailSent(emailForSending)
                End If

                ' create user notifications
                Create_NotificationEmail_Tell_us_who_you_like_Receive_MALE()

                emailForSending = clsSendingEmailsHandler.GetNextEmailToSend()
                SupportAppHelper.NotifyEmailSent(True)
                System.Threading.Thread.Sleep(500)



                emailCount = emailCount + 1
                If (emailCount > 10) Then
                    Dim interval As Integer = r.Next(20, 60)
                    emailCount = 0
                    bw.ReportProgress(0, " Waiting " & interval & " seconds")
                    'System.Threading.Thread.Sleep(interval * 60000)
                    continueDate = DateTime.UtcNow.AddMilliseconds(interval * 1000)
                End If

            End While

            ' create user notifications
            Create_NotificationEmail_Tell_us_who_you_like_Receive_MALE()

        Catch ex As Exception
            bw.ReportProgress(0, ex.Message)
        Finally
            hdl.Dispose()
        End Try
    End Sub

    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        If (e.ProgressPercentage = -1) Then
            Me.BarStaticItemStatus.Caption = Mid(CStr(e.UserState), 1, 200) & "..."
        Else
            AddMessage(CStr(e.UserState))
        End If
    End Sub

    Private Sub AddMessage(message As String)
        'txOutput.Text = "[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf & txOutput.Text
        If (message IsNot Nothing) Then
            Try
                If Me.InvokeRequired Then
                    ' calls itself, but on correct thread
                    Me.BeginInvoke(New Action(Of Object)(AddressOf txtOutput.MaskBox.AppendText), New Object() {"[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf})
                Else
                    txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf)
                End If
            Catch ex As InvalidOperationException
            End Try
        End If
        Try
            If (txtOutput.Lines.Length > 5001) Then
                Dim index As Integer = 0
                Dim linesCount As Integer = txtOutput.Lines.Length
                Dim txtOutputText As String = txtOutput.Text
                For cnt = 0 To linesCount - 5000
                    index = txtOutputText.IndexOf(vbCr, index) + vbCr.Length
                Next
                If (index > 0) Then
                    txtOutput.Text = txtOutput.Text.Remove(0, index).TrimStart({ControlChars.Cr, ControlChars.Lf})
                End If
            End If
        Catch
        End Try
    End Sub

    Private Sub TryAddMessage(progress As Integer, message As String)
        Try
            bw.ReportProgress(progress, message)
        Catch ex As InvalidOperationException
            AddMessage(message)
        Catch ex As Exception
        End Try
    End Sub


    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then
            Me.BarStaticItemStatus.Caption = "Canceled"
            tmrTask.Enabled = False
        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub



    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub

    Private Sub BarButtonItemRun_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRun.ItemClick
        Try
            tmrTask.Enabled = False

            If Not bw.IsBusy = True Then
                BarStaticItemStatus.Caption = "Running"
                bw.RunWorkerAsync()
                BarButtonItemRun.Enabled = False
            Else
                Try
                    bw.CancelAsync()
                    bw.RunWorkerAsync()
                    BarStaticItemStatus.Caption = "Running"
                Catch ex As Exception
                    AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick -> bw.IsBusy" & vbCrLf & ex.Message)
                End Try
            End If

        Catch ex As Exception
            AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick" & vbCrLf & ex.Message)
        Finally
            tmrTask.Enabled = True
            tmrTask.Interval = tmrTaskInterval
        End Try
    End Sub


    Private Sub BarButtonItemCancelJob_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCancelJob.ItemClick
        bw.CancelAsync()
        tmrTask.Enabled = False
        BarButtonItemRun.Enabled = True
    End Sub

    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        If Not bw.IsBusy = True Then
            BarStaticItemStatus.Caption = "Running"
            bw.RunWorkerAsync()
            BarButtonItemRun.Enabled = False
        End If
        tmrTask.Interval = tmrTaskInterval
    End Sub


    Private Sub StartTimer()
        Dim r As New Random(System.DateTime.Now.Millisecond)
        Dim interval As Integer = r.Next(30, 60)

        If (tmrTaskInterval = 0) Then tmrTaskInterval = tmrTask.Interval
        tmrTask.Interval = interval * 1000
        tmrTask.Enabled = True

        tmrTask.Enabled = True
        tmrTask.Start()

        BarStaticItemStatus.Caption = "First run in " & tmrTask.Interval / 1000 & " sec"
    End Sub




    Private Sub PerformImageComparison_ProgressChanged(sender As Object, e As GoomenaImageCompare.ProgressChangedEventArgs)
        TryAddMessage(0, "Image Comparison-->" & e.CurrentUrl)
    End Sub


    Private Sub PerformImageComparison_Complete(sender As Object, e As GoomenaImageCompare.AlertEventArgs)
        Try


            Dim userState As GoogleImageCheckState = e.UserState
            Dim customerIDList As Dictionary(Of String, System.Data.DataRow) = userState.customerIDList
            Dim emailForSending As SYS_EmailsQueue = userState.emailForSending

            TryAddMessage(0, emailForSending.EmailAction & "-->Complete")
            TryAddMessage(0, emailForSending.EmailAction & "-->Similar photos found " & e.uuiListData.Count)
            Using con As New Data.SqlClient.SqlConnection(My.Settings.AppDBconnectionString)


                Using command As System.Data.SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, "")


                    ' If (customerIDList.Count = 0) Then Return


                    Try

                        If (e.uuiListData.Count > 0) Then

                            Dim uniqueInputPhotos As New List(Of String)
                            For cnt = 0 To e.uuiListData.Count - 1
                                Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = e.uuiListData(cnt)
                                If (Not uniqueInputPhotos.Contains(itm.InputUrl)) Then
                                    uniqueInputPhotos.Add(itm.InputUrl)
                                End If
                            Next


                            For cnt = 0 To uniqueInputPhotos.Count - 1

                                Dim urlPhoto As String = uniqueInputPhotos(cnt)
                                If (Not customerIDList.ContainsKey(urlPhoto)) Then Continue For
                                Dim dr As System.Data.DataRow = customerIDList(urlPhoto)
                                Dim loginName As String = DataHelpers.GetEUS_Profile_LoginName_ByProfileID(dr("CustomerID"))

                                Try


                                    Dim Content_Similar As String = <h><![CDATA[
<div>Similar [SIMILARITY]%</div>
<div><img src="[PHOTO-URL]"  style="width:250px" width="250"/></div>
<div><a href="[PHOTO-URL]" target="_blank">[PHOTO-URL]</a></div>
<br/> 
<br/>
]]></h>

                                    Dim htmlTable As New StringBuilder()
                                    '      Dim prevLogin As String = ""

                                    Dim itemsSimilar As List(Of GoomenaImageCompare.uuiSimilarUrlItem) = (From f In e.uuiListData Order By f.Similarity Descending).ToList()
                                    For f1 = 0 To itemsSimilar.Count - 1
                                        Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = itemsSimilar(f1)
                                        If (itm.InputUrl = urlPhoto) Then
                                            Dim simPhoto As String = Content_Similar.Replace("[PHOTO-URL]", itm.FoundUrl)
                                            simPhoto = simPhoto.Replace("[SIMILARITY]", Math.Round(itm.Similarity * 100, 2))
                                            htmlTable.AppendLine(simPhoto)
                                        End If
                                    Next

                                    htmlTable.Insert(0, emailForSending.Body & vbCrLf & "<br><br>" & vbCrLf)
                                    htmlTable.Insert(0, "<b>Similar photos (" & itemsSimilar.Count & ") found!</b><br>" & vbCrLf)
                                    htmlTable.Insert(0, "<html><head>" & "</head><body>")
                                    htmlTable.AppendLine("</body></html>")

                                    emailForSending.Body = htmlTable.ToString()
                                    emailForSending.Subject = emailForSending.Subject & " -- Similar photos (" & itemsSimilar.Count & ")"
                                    TryAddMessage(0, emailForSending.EmailAction & "-->Sending message for " & loginName & ", email action " & emailForSending.EmailAction)
                                    'clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Found similar images", Content, True)
                                Catch
                                End Try
                            Next



                            Using _CMSDBDataContext As New CMSDBDataContext(command.Connection.ConnectionString)


                                Try

                                    For cnt = 0 To e.uuiListData.Count - 1
                                        Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = e.uuiListData(cnt)
                                        If (Not customerIDList.ContainsKey(itm.InputUrl)) Then Continue For
                                        Dim dr As System.Data.DataRow = customerIDList(itm.InputUrl)

                                        Dim ps As New EUS_CustomerPhotosSimilar()
                                        ps.CustomerID = dr("CustomerID")
                                        ps.CustomerPhotosID = dr("CustomerPhotosID")
                                        ps.DateTimeCreated = Date.UtcNow
                                        ps.PhotoSimilarity = itm.Similarity
                                        ps.PhotoURL = itm.FoundUrl
                                        _CMSDBDataContext.EUS_CustomerPhotosSimilars.InsertOnSubmit(ps)

                                    Next

                                    _CMSDBDataContext.SubmitChanges()
                                Catch ex As Exception
                                    TryAddMessage(0, emailForSending.EmailAction & "-->" & ex.ToString())
                                    Throw New System.Exception(ex.Message, ex)
                                End Try
                            End Using
                            TryAddMessage(0, emailForSending.EmailAction & "-->EUS_CustomerPhotosSimilars updated")
                        End If

                        If (customerIDList.Count > 0) Then

                            Dim sb As New System.Text.StringBuilder()
                            For cnt = 0 To e.uuiListData.Count - 1
                                Dim itm As GoomenaImageCompare.uuiSimilarUrlItem = e.uuiListData(cnt)

                                If (customerIDList.ContainsKey(itm.InputUrl)) Then
                                    Dim dr As System.Data.DataRow = customerIDList(itm.InputUrl)
                                    Dim CustomerPhotosID As Integer = dr("CustomerPhotosID")

                                    Dim sql As String = "update EUS_CustomerPhotos set PhotoSimilarity=@PhotoSimilarity[COUNT] where CustomerPhotosID=@CustomerPhotosID[COUNT];"
                                    sql = sql.Replace("[COUNT]", cnt)
                                    sb.AppendLine(sql)
                                    command.Parameters.AddWithValue("@PhotoSimilarity" & cnt, 1)
                                    command.Parameters.AddWithValue("@CustomerPhotosID" & cnt, CustomerPhotosID)
                                    customerIDList.Remove(itm.InputUrl)
                                End If
                            Next


                            Dim keys As String() = New String(customerIDList.Keys.Count - 1) {}
                            customerIDList.Keys.CopyTo(keys, 0)
                            For cnt = 0 To keys.Length - 1
                                Dim dr As System.Data.DataRow = customerIDList(keys(cnt))
                                Dim CustomerPhotosID As Integer = dr("CustomerPhotosID")

                                Dim index As Integer = e.uuiListData.Count + cnt + 10
                                Dim sql As String = "update EUS_CustomerPhotos set PhotoSimilarity=@PhotoSimilarity[COUNT] where CustomerPhotosID=@CustomerPhotosID[COUNT];"
                                sql = sql.Replace("[COUNT]", index)
                                sb.AppendLine(sql)
                                command.Parameters.AddWithValue("@PhotoSimilarity" & index, 0)
                                command.Parameters.AddWithValue("@CustomerPhotosID" & index, CustomerPhotosID)
                            Next

                            command.CommandText = sb.ToString()
                            DataHelpers.ExecuteNonQuery(command)
                            TryAddMessage(0, emailForSending.EmailAction & "-->Database updated")
                        End If

                    Catch ex As Exception
                        emailForSending.Exception = ex.ToString()
                        TryAddMessage(0, " Exc. " & ex.Message)
                        Try
                            Dim str As String = ""
                            str = "EmailTo: " & emailForSending.EmailTo & vbCrLf & _
                                 "Subject: " & emailForSending.Subject & vbCrLf & _
                                 "Email Action: " & emailForSending.EmailAction & vbCrLf & vbCrLf & _
                                 "Exception : " & emailForSending.Exception
                            clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Sending Emails Exception: " & ex.Message, str, False)
                        Catch
                        End Try
                    Finally

                        Using hdl As New clsSendingEmailsHandler()


                            Try
                                SendEmailInQueue(emailForSending)
                                clsSendingEmailsHandler.SetEmailSent(emailForSending)
                            Catch ex As Exception
                            End Try
                        End Using
                    End Try
                End Using
            End Using
        Catch ex As Exception
            TryAddMessage(0, " Exc. " & ex.Message)
            clsMyMail.SendMail2(My.Settings.gToEmail, My.Settings.gToEmail, "Sending Emails Exception: " & ex.Message, ex.ToString(), False)
        Finally
            ' isBusy_PerformImageComparison = False
        End Try
    End Sub


    Private Sub PerformImageComparison(CustomerPhotosID As Long, emailForSending As SYS_EmailsQueue)
        Try
            Dim DirectoryName As String = System.IO.Path.GetDirectoryName(Application.ExecutablePath)
            Dim logFile As String = System.IO.Path.Combine(DirectoryName, "ImageComparison.log")
            bw.ReportProgress(0, "Image Comparison-->log file " & vbTab & logFile)

            Dim userState As New GoogleImageCheckState()

            Try
                System.IO.File.Delete(logFile)
            Catch
            End Try

            'Dim sql As String = "EXEC [GetView_EUS_CustomerPhotos_ImageComparison] @recordsToSelect=10;"
            Dim sql As String = "EXEC [GetView_EUS_CustomerPhotos_ImageComparison] @CustomerPhotosID=" & CustomerPhotosID & ";"
            Dim urlList As New List(Of String)
            Using dt As DataTable = DataHelpers.GetDataTable(sql)




                bw.ReportProgress(0, emailForSending.EmailAction & "-->photos count:" & dt.Rows.Count)
                For cnt = 0 To dt.Rows.Count - 1
                    Dim fn As String = dt.Rows(cnt)("FileName")
                    Dim customerid As Integer = dt.Rows(cnt)("CustomerID")
                    Dim path As String = ProfileHelper.GetProfileImageURL(customerid, fn, 0, False, False)

                    urlList.Add(path)
                    userState.customerIDList.Add(path, dt.Rows(cnt))
                Next
            End Using
            bw.ReportProgress(0, emailForSending.EmailAction & "-->starting search...")

            Dim sd As New GoomenaImageCompare.ImageComparison(10000, urlList, logFile)
            AddHandler sd.CallAlert, AddressOf PerformImageComparison_Complete
            AddHandler sd.ProgressChanged, AddressOf PerformImageComparison_ProgressChanged
            userState.emailForSending = emailForSending
            sd.getResults(userState)

            '  isBusy_PerformImageComparison = True

        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        End Try
    End Sub

    Private Sub SendEmailInQueue(ByRef emailForSending As SYS_EmailsQueue)

        ' send email using specified credentials
        Dim result As SYS_SMTPServer = Nothing
        If (clsNullable.NullTo(emailForSending.SMTPServerCredentialsID, 0) > 0) Then
            result = DataHelpers.SYS_SMTPServers_GetByID(emailForSending.SMTPServerCredentialsID)
        End If


        If (result IsNot Nothing) Then
            Dim MySMTPSettings As New clsMyMail.SMTPSettings()
            MySMTPSettings.gSMTPOutPort = result.SMTPOutPort
            MySMTPSettings.gSMTPServerName = result.SMTPServerName
            MySMTPSettings.gSMTPLoginName = result.SMTPLoginName
            MySMTPSettings.gSMTPPassword = result.SMTPPassword
            MySMTPSettings.gSMTPenableSsl = result.SmtpUseSsl
            clsMyMail.SendMail2(MySMTPSettings, result.SMTPFromEmail, emailForSending.EmailTo,
                                emailForSending.Subject, emailForSending.Body,
                                If(emailForSending.IsHtmlBody.HasValue, emailForSending.IsHtmlBody, True), emailForSending.BCCAddresses)

            emailForSending.DateSent = Date.UtcNow
            emailForSending.Exception = Nothing
            bw.ReportProgress(0, "sent to  " & emailForSending.EmailTo)

        Else


            If (emailForSending.EmailAction.StartsWith("New.Photo.Google.Check.ID-")) Then
                ' send from support@goomena.com
                If clsMyMail.MySMTPSettingsSupport.gSMTPServerName Is Nothing OrElse clsMyMail.MySMTPSettingsSupport.gSMTPServerName.Length = 0 Then
                    result = DataHelpers.SYS_SMTPServers_GetByNameLINQ("GOOMENA")
                    If result IsNot Nothing Then
                        Dim MySMTPSettings As New clsMyMail.SMTPSettings()
                        clsMyMail.MySMTPSettings.gSMTPOutPort = result.SMTPOutPort
                        clsMyMail.MySMTPSettings.gSMTPServerName = result.SMTPServerName
                        clsMyMail.MySMTPSettings.gSMTPLoginName = result.SMTPLoginName
                        clsMyMail.MySMTPSettings.gSMTPPassword = result.SMTPPassword
                        clsMyMail.MySMTPSettings.gSMTPenableSsl = result.SmtpUseSsl
                    Else
                        clsMyMail.MySMTPSettings.gSMTPOutPort = My.Settings.gSMTPOutPort
                        clsMyMail.MySMTPSettings.gSMTPServerName = My.Settings.gSMTPServerName
                        clsMyMail.MySMTPSettings.gSMTPLoginName = My.Settings.gSMTPLoginName
                        clsMyMail.MySMTPSettings.gSMTPPassword = My.Settings.gSMTPPassword
                        clsMyMail.MySMTPSettings.gSMTPenableSsl = False
                    End If
                End If
                clsMyMail.TrySendMail(emailForSending.EmailTo,
                               emailForSending.Subject, emailForSending.Body,
                               If(emailForSending.IsHtmlBody.HasValue, emailForSending.IsHtmlBody, True),
                               emailForSending.BCCAddresses)
          
            Else
                Dim MySMTPSettings As New clsMyMail.SMTPSettings()
                result = DataHelpers.SYS_SMTPServers_GetByNameLINQ("GOOInfo")
                If result IsNot Nothing Then
                    MySMTPSettings.gSMTPOutPort = result.SMTPOutPort
                    MySMTPSettings.gSMTPServerName = result.SMTPServerName
                    MySMTPSettings.gSMTPLoginName = result.SMTPLoginName
                    MySMTPSettings.gSMTPPassword = result.SMTPPassword
                    MySMTPSettings.gSMTPenableSsl = result.SmtpUseSsl
                Else
                    MySMTPSettings.gSMTPOutPort = My.Settings.gSMTPOutPort
                    MySMTPSettings.gSMTPServerName = My.Settings.gSMTPServerName
                    MySMTPSettings.gSMTPLoginName = My.Settings.gSMTPLoginName
                    MySMTPSettings.gSMTPPassword = My.Settings.gSMTPPassword
                    MySMTPSettings.gSMTPenableSsl = False
                End If

                clsMyMail.SendMail2(MySMTPSettings, emailForSending.EmailTo, emailForSending.EmailTo,
                                    emailForSending.Subject, emailForSending.Body,
                                    If(emailForSending.IsHtmlBody.HasValue, emailForSending.IsHtmlBody, True),
                                    emailForSending.BCCAddresses)
            End If

            emailForSending.DateSent = Date.UtcNow
            emailForSending.Exception = Nothing
            TryAddMessage(0, "sent to  " & emailForSending.EmailTo)

        End If

    End Sub


    Public Class GoogleImageCheckState
        Public Property emailForSending As SYS_EmailsQueue
        Public Property customerIDList As Dictionary(Of String, System.Data.DataRow)

        Public Sub New()
            customerIDList = New Dictionary(Of String, System.Data.DataRow)
        End Sub
    End Class


    Private __Create_NotificationEmail_Tell_us_who_you_like_Receive_MALE_Date As DateTime
    Public Sub Create_NotificationEmail_Tell_us_who_you_like_Receive_MALE()

        Dim sbLogger As New StringBuilder()
        Dim sendEmailToSupport As Boolean?
        Using ctx As New CMSDBDataContext(ConnectionString)




            Try

                If (__Create_NotificationEmail_Tell_us_who_you_like_Receive_MALE_Date = DateTime.MinValue) Then
                    ' continue execution
                ElseIf (__Create_NotificationEmail_Tell_us_who_you_like_Receive_MALE_Date > DateTime.UtcNow) Then
                    ' return when time not past
                    Return
                End If

                ' after 10 minutes allow this function to be executed
                __Create_NotificationEmail_Tell_us_who_you_like_Receive_MALE_Date = DateTime.UtcNow.AddMinutes(10)


                Dim ConnectionString As String = My.Settings.AppDBconnectionString
                Dim result As List(Of EUS_Profiles_EUS_ProfilesViewed_GetNotViewedProfilesResult) =
                    ctx.EUS_Profiles_EUS_ProfilesViewed_GetNotViewedProfiles().ToList()

                Dim msg As String = "creating  Tell_us_who_you_like message for profiles, records found " & result.Count
                TryAddMessage(0, msg)
                sbLogger.AppendLine(msg)

                Dim lagsText As New Dictionary(Of String, String)()
                Dim o As New clsSiteLAG()
                Dim view_profile_url As String = clsConfigValues.GetSYS_ConfigValue("view_profile_url")
                '     Dim image_thumb_url As String = clsConfigValues.GetSYS_ConfigValue("image_thumb_url")
                Dim site_name As String = clsConfigValues.GetSYS_ConfigValue("site_name")

                Dim countAllRecs As Integer = result.Count
                While (countAllRecs >= 4)
                    If (sendEmailToSupport Is Nothing) Then sendEmailToSupport = True

                    Dim cnt As Integer = 0
                    Dim itm As EUS_Profiles_EUS_ProfilesViewed_GetNotViewedProfilesResult = result(cnt)

                    Dim countRelatedProfiles As Integer = (From itm2 In result
                                                            Where itm2.ProfileID = itm.ProfileID
                                                            Select itm2).Count()


                    msg = "processing profile " & itm.CurrentLoginName & "-" & itm.ProfileID & ", LAGID " & itm.LAGID & " related not viewed profiles " & countRelatedProfiles
                    TryAddMessage(0, "Tell_us_who_you_like: " & msg)
                    sbLogger.AppendLine(msg)

                    If (Not lagsText.ContainsKey(itm.LAGID)) Then
                        Dim subject1 As String = o.GetCustomStringFromPage(itm.LAGID, "GlobalStrings", "NotificationEmail__Tell_us_who_you_like__Receive_MALE_Subject", ConnectionString)
                        Dim body1 As String = o.GetCustomStringFromPage(itm.LAGID, "GlobalStrings", "NotificationEmail__Tell_us_who_you_like__Receive_MALE", ConnectionString)
                        Dim YearsOldText1 As String = o.GetCustomStringFromPage(itm.LAGID, "control.OfferControl", "YearsOldText", ConnectionString)
                        If (String.IsNullOrEmpty(body1)) Then
                            subject1 = o.GetCustomStringFromPage("US", "GlobalStrings", "NotificationEmail__Tell_us_who_you_like__Receive_MALE_Subject", ConnectionString)
                            body1 = o.GetCustomStringFromPage("US", "GlobalStrings", "NotificationEmail__Tell_us_who_you_like__Receive_MALE", ConnectionString)
                            YearsOldText1 = o.GetCustomStringFromPage("US", "control.OfferControl", "YearsOldText", ConnectionString)
                        End If

                        lagsText.Add(itm.LAGID & "_subject", subject1)
                        lagsText.Add(itm.LAGID, body1)
                        lagsText.Add(itm.LAGID & "_age", YearsOldText1)

                    End If



                    Dim subject As String = lagsText(itm.LAGID & "_subject")
                    Dim body As String = lagsText(itm.LAGID)
                    Dim YearsOldText As String = lagsText(itm.LAGID & "_age")


                    If (ProfileHelper.IsFemale(itm.GenderId)) Then
                        body = body.Replace("[MY-GENDER]", "my-gender-female")
                    Else
                        body = body.Replace("[MY-GENDER]", "my-gender-male")
                    End If
                    body = body.Replace("###RECIPIENTLOGINNAME###", itm.CurrentLoginName)


                    Dim processedItm As EUS_Profiles_EUS_ProfilesViewed_GetNotViewedProfilesResult = (From itm2 In result
                                                                                            Where itm2.ProfileID = itm.ProfileID
                                                                                            Select itm2).FirstOrDefault()

                    Dim rp1 As Integer = 0
                    Dim sbUpdate As New StringBuilder()
                    While (processedItm IsNot Nothing)
                        rp1 = rp1 + 1
                        result.Remove(processedItm)


                        If (rp1 < 5) Then
                            msg = "processing profile " & itm.CurrentLoginName & ", related not viewed profile " & processedItm.TargetLoginName
                            TryAddMessage(0, "Tell_us_who_you_like: " & msg)
                            sbLogger.AppendLine(msg)

                            sbUpdate.AppendLine("update dbo.EUS_ProfilesViewed set ToProfileIDNotifiedDate=getutcdate() where ToProfileID=" & processedItm.ProfileID & " and FromProfileID=" & processedItm.fromProfileID & "")

                            Dim PROFILE_LOGIN As String = processedItm.TargetLoginName
                            Dim PROFILE_URL As String = view_profile_url.Replace("{0}", System.Web.HttpUtility.UrlEncode(PROFILE_LOGIN))
                            Dim PROFILE_DESCR As String = ""
                            Dim PROFILE_PHOTO_URL As String

                            Dim sb As New StringBuilder()
                            sb.Append("<div>")

                            If processedItm.Age IsNot Nothing Then
                                sb.Append(processedItm.Age & " " & YearsOldText & " ")
                            End If

                            sb.Append("<div class=""mem-descr"">")

                            sb.Append(If(Not String.IsNullOrEmpty(processedItm.City), processedItm.City, ""))
                            sb.Append(",  ")
                            sb.Append(If(Not String.IsNullOrEmpty(processedItm.Region), processedItm.Region, ""))
                            sb.Append(",  ")
                            sb.Append(If(Not String.IsNullOrEmpty(processedItm.Country), ProfileHelper.GetCountryName(processedItm.Country), ""))
                            sb.Replace(",  " & ",  ", ",  ")

                            sb.Append("</div>")
                            sb.Append("</div>")
                            PROFILE_DESCR = sb.ToString()
                            sb.Clear()

                            If (processedItm.FileName IsNot Nothing) Then
                                Dim src As String = ProfileHelper.GetProfileImageURL(processedItm.fromProfileID, processedItm.FileName, processedItm.GenderId, True, False, PhotoSize.Thumb)
                                PROFILE_PHOTO_URL = src
                            Else
                                Dim src As String = ProfileHelper.GetProfileImageURL(processedItm.fromProfileID, "", processedItm.GenderId, True, False, PhotoSize.Thumb)
                                PROFILE_PHOTO_URL = (site_name & src.Trim("/"))
                            End If

                            body = body.Replace("[PROFILE-URL" & rp1 & "]", PROFILE_URL)
                            body = body.Replace("[PROFILE-LOGIN" & rp1 & "]", PROFILE_LOGIN)
                            body = body.Replace("[PROFILE-PHOTO-URL" & rp1 & "]", PROFILE_PHOTO_URL)
                            body = body.Replace("[PROFILE-DESCR" & rp1 & "]", PROFILE_DESCR)
                            body = body.Replace("[PROFILE-LOGIN-ENC" & rp1 & "]", System.Web.HttpUtility.UrlEncode(PROFILE_LOGIN))

                            'stis 4 photos exit
                        End If
                        processedItm = (From itm2 In result
                                        Where itm2.ProfileID = itm.ProfileID
                                        Select itm2).FirstOrDefault()
                    End While

                    clsMyMail.SendMailFromQueue(itm.CurrentEmail, subject, body, itm.ProfileID, "Tell_us_who_you_like", True, Nothing, Nothing)
                    DataHelpers.ExecuteNonQuery(sbUpdate.ToString())
                    sbUpdate.Clear()

                    msg = "sending message to profile " & itm.CurrentLoginName & " (" & itm.CurrentEmail & ") and updating EUS_ProfilesViewed "
                    TryAddMessage(0, "Tell_us_who_you_like: " & msg)
                    sbLogger.AppendLine(msg)

                    countAllRecs = result.Count

                    msg = "new records count " & countAllRecs
                    TryAddMessage(0, "Tell_us_who_you_like: " & msg)
                    sbLogger.AppendLine(msg)

                End While
            Catch ex As Exception
                sbLogger.AppendLine(ex.ToString())
            Finally

            End Try
        End Using
        Try
            Dim str As String = sbLogger.ToString()
            If (clsNullable.NullTo(sendEmailToSupport)) Then
                clsMyMail.SendMail2(My.Settings.gSMTPLoginName, "exceptions@zizina.com", "[SUPPORT APP] Tell_us_who_you_like log", str, False)
            End If
        Catch
        Finally
            sbLogger.Clear()
        End Try

    End Sub

 

End Class
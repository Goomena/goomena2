﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Text
Imports System.Linq
Imports System.Data.Linq
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Imports System.Threading.Tasks
Imports System
Imports Microsoft.Web.Administration
Public Class frmSuspiciousIp

    Dim tmrTaskInterval As Integer = 60000
    ' Dim isBusy_PerformImageComparison As Boolean
    Dim ConnectionString As String = My.Settings.AppDBconnectionString
    Private Sub frmSendingForCreditsExpiring_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GroupControl1.Text = Me.Text
        BarStaticItemStatus.Caption = "Not running"

        StartTimer()
    End Sub
    Private Sub frmSendingForCreditsExpiring_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

            bw.CancelAsync()
            bw.Dispose()
            bw = Nothing
            tmrTask.Enabled = False
            Me.Dispose()

        Catch ex As Exception
        End Try
    End Sub
    Private Class UserForHappyBirthday
        Public Property profileId As Integer
        Public Property MirrorprofileId As Integer
        Public Property LoginName As String
        Public Property LagId As String
        Public Property Email As String
        Public Property LastActivityIP As String
        Public Property Referrer As String
        Public Property CustomReferrer As String
    End Class

    Private Class UserForCheckInAdmin
        Public Property profileId As Integer
        Public Property MirrorprofileId As Integer
    End Class
    Private _credits As Integer
    Private Property credits As Integer
        Get
            Return _credits
        End Get
        Set(value As Integer)
            _credits = value
        End Set
    End Property
  
    
    Private Sub refreshblockedIps()
        Dim t As Task = Task.Run(Sub()
                                     Using ds As dsBlockSuspicious = DataHelpers.SYS_FillIsInBlockedIps
                                         Me.Invoke(Sub()
                                                       Me.DsInBlock.SYS_BlockedIps.Clear()
                                                       Me.DsInBlock.SYS_BlockedIps.Merge(ds.SYS_BlockedIps)
                                                   End Sub)
                                     End Using
                                 End Sub, Nothing)
    End Sub
    Private Sub refreshHistory()
        Dim t As Task = Task.Run(Sub()
                                     Using ds As dsBlockSuspicious = DataHelpers.SYS_FillHistoryBlockecIps
                                         Me.Invoke(Sub()
                                                       Me.DsHistory.SYS_BlockedIps.Clear()
                                                       Me.DsHistory.SYS_BlockedIps.Merge(ds.SYS_BlockedIps)
                                                   End Sub)
                                     End Using
                                 End Sub, Nothing)
    End Sub
    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork
        Me.Invoke(Sub()
                      brAddNewIp.Enabled = False
                      brDelete.Enabled = False
                  End Sub)
        Try
            Using _CMSDBDataContext As New CMSDBDataContext(My.MySettings.Default.AppDBconnectionString)
                '    Dim ipSecurityCollection As ConfigurationElementCollection = ipSecuritySection.GetCollection
                bw.ReportProgress(0, "Starting execution ")
                Using ds As dsBlockSuspicious = DataHelpers.SYS_GetSuspiciousIps(DateTime.UtcNow.AddMinutes(-2))
                    If (ds.GetSuspiciousIp.Rows.Count = 0) Then
                        bw.ReportProgress(0, "No suspicious IPS found.")
                        '  Return
                    Else
                        Try

                        
                            bw.ReportProgress(0, "Found " & ds.GetSuspiciousIp.Rows.Count & " suspicious Ips.")
                            Dim serverManager As ServerManager = New ServerManager
                        Dim config As Configuration = serverManager.GetApplicationHostConfiguration
                        Dim ipSecuritySection As ConfigurationSection = config.GetSection("system.webServer/security/ipSecurity")
                        ipSecuritySection.SetAttributeValue("denyAction", "AbortRequest")
                        Dim ipSecurityCollection As ConfigurationElementCollection = ipSecuritySection.GetCollection
                        For Each r As dsBlockSuspicious.GetSuspiciousIpRow In ds.GetSuspiciousIp.Rows
                            Dim found As Boolean = False
                            For i As Integer = ipSecurityCollection.Count - 1 To 0 Step -1
                                'As ConfigurationElement In ipSecurityCollection
                                If r.ip = ipSecurityCollection(i).Item("ipAddress") Then
                                    bw.ReportProgress(0, "Skipping ip :" & r.ip & " beacause it is already in blocked ips.")
                                        found = True
                                        Exit For
                                End If
                            Next
                            If Not found Then
                                    Dim blr As SYS_BlockedIp = (From t As SYS_BlockedIp In _CMSDBDataContext.SYS_BlockedIps
                                                            Where t.Ip = r.ip).FirstOrDefault
                                If blr IsNot Nothing Then
                                    blr.DatetimeBlocked = DateTime.UtcNow
                                    blr.CountTimesBlocked += 1
                                    blr.IsInBlock = True
                                Else
                                    blr = New SYS_BlockedIp
                                    blr.Ip = r.ip
                                    blr.DatetimeBlocked = DateTime.UtcNow
                                        blr.CountTimesBlocked = 1
                                    blr.IsInBlock = True
                                    _CMSDBDataContext.SYS_BlockedIps.InsertOnSubmit(blr)
                                End If
                                If blr.CountTimesBlocked = 1 Then
                                    blr.DatetimeUblocked = DateTime.UtcNow.AddMinutes(3)
                                ElseIf blr.CountTimesBlocked = 2 Then
                                    blr.DatetimeUblocked = DateTime.UtcNow.AddMinutes(20)
                                ElseIf blr.CountTimesBlocked = 3 Then
                                    blr.DatetimeUblocked = DateTime.UtcNow.AddMinutes(60)
                                ElseIf blr.CountTimesBlocked = 4 Then
                                    blr.DatetimeUblocked = DateTime.UtcNow.AddHours(24)
                                ElseIf blr.CountTimesBlocked = 4 Then
                                        blr.DatetimeUblocked = DateTime.UtcNow.AddHours(48)
                                    Else
                                        blr.DatetimeUblocked = DateTime.UtcNow.AddHours(60)
                                    End If
                                _CMSDBDataContext.SubmitChanges()
                                Try
                                    Dim addElement As ConfigurationElement = ipSecurityCollection.CreateElement("add")
                                    addElement("ipAddress") = r.ip
                                    addElement("allowed") = False
                                    ipSecurityCollection.Add(addElement)
                                    bw.ReportProgress(0, "Blocking Ip: " & r.ip)
                                Catch ex As Exception
                                End Try
                            End If
                        Next
                            serverManager.CommitChanges()
                        Catch ex As Exception

                        End Try
                    End If
                    Try
                        Dim serverManager2 As ServerManager = New ServerManager
                        Dim config2 As Configuration = serverManager2.GetApplicationHostConfiguration
                    Dim ipSecuritySection2 As ConfigurationSection = config2.GetSection("system.webServer/security/ipSecurity")
                    ipSecuritySection2.SetAttributeValue("denyAction", "AbortRequest")
                        Dim ipSecurityCollection2 As ConfigurationElementCollection = ipSecuritySection2.GetCollection
                        For i As Integer = ipSecurityCollection2.Count - 1 To 0 Step -1
                            Dim ip As String = ipSecurityCollection2(i).Item("ipAddress")
                            Dim blr As SYS_BlockedIp = (From t As SYS_BlockedIp In _CMSDBDataContext.SYS_BlockedIps
                                                       Where t.Ip = ip).FirstOrDefault
                            If blr IsNot Nothing Then
                                If blr.DatetimeUblocked <> Nothing Then
                                    If blr.DatetimeUblocked <= Date.UtcNow Then
                                        blr.IsInBlock = False
                                        bw.ReportProgress(0, "Unblocking Ip: " & ip)
                                        ipSecurityCollection2.Remove(ipSecurityCollection2(i))
                                    End If
                                Else
                                    blr.IsInBlock = False
                                    bw.ReportProgress(0, "Unblocking Ip: " & ip)
                                    ipSecurityCollection2.Remove(ipSecurityCollection2(i))
                                End If
                            Else
                                bw.ReportProgress(0, "Unblocking Ip: " & ip)
                                ipSecurityCollection2.Remove(ipSecurityCollection2(i))
                            End If
                        Next
                        _CMSDBDataContext.SubmitChanges()
                        serverManager2.CommitChanges()
                    Catch ex As Exception
                    End Try
                    Try
                        For Each blr As SYS_BlockedIp In _CMSDBDataContext.SYS_BlockedIps
                            If blr.CountTimesBlocked = 1 Then
                                If blr.DatetimeUblocked <> Nothing Then
                                    If blr.DatetimeUblocked <= DateTime.UtcNow.AddHours(-12) Then
                                        _CMSDBDataContext.SYS_BlockedIps.DeleteOnSubmit(blr)
                                        bw.ReportProgress(0, "Deleting History IP: " & blr.Ip)
                                    End If
                                End If
                            ElseIf blr.CountTimesBlocked = 2 Then
                                If blr.DatetimeUblocked <> Nothing Then
                                    If blr.DatetimeUblocked <= DateTime.UtcNow.AddHours(-16) Then
                                        _CMSDBDataContext.SYS_BlockedIps.DeleteOnSubmit(blr)
                                        bw.ReportProgress(0, "Deleting History IP: " & blr.Ip)
                                    End If
                                End If
                            ElseIf blr.CountTimesBlocked = 3 Then
                                If blr.DatetimeUblocked <> Nothing Then
                                    If blr.DatetimeUblocked <= DateTime.UtcNow.AddHours(-24) Then
                                        _CMSDBDataContext.SYS_BlockedIps.DeleteOnSubmit(blr)
                                        bw.ReportProgress(0, "Deleting History IP: " & blr.Ip)
                                    End If
                                End If
                            ElseIf blr.CountTimesBlocked = 4 Then
                                If blr.DatetimeUblocked <> Nothing Then
                                    If blr.DatetimeUblocked <= DateTime.UtcNow.AddHours(-48) Then
                                        _CMSDBDataContext.SYS_BlockedIps.DeleteOnSubmit(blr)
                                        bw.ReportProgress(0, "Deleting History IP: " & blr.Ip)
                                    End If
                                End If
                            ElseIf blr.CountTimesBlocked = 4 Then
                                If blr.DatetimeUblocked <> Nothing Then
                                    If blr.DatetimeUblocked <= DateTime.UtcNow.AddHours(-60) Then
                                        _CMSDBDataContext.SYS_BlockedIps.DeleteOnSubmit(blr)
                                        bw.ReportProgress(0, "Deleting History IP: " & blr.Ip)
                                    End If
                                End If
                            Else
                                If blr.DatetimeUblocked <> Nothing Then
                                    If blr.DatetimeUblocked <= DateTime.UtcNow.AddHours(-70) Then
                                        _CMSDBDataContext.SYS_BlockedIps.DeleteOnSubmit(blr)
                                        bw.ReportProgress(0, "Deleting History IP: " & blr.Ip)
                                    End If
                                End If
                            End If
                        Next
                        _CMSDBDataContext.SubmitChanges()
                    Catch ex As Exception
                    End Try
                End Using
            End Using
            refreshblockedIps()
            refreshHistory()
        Catch ex As Exception
            Throw
        Finally
            bw.ReportProgress(0, "End execution ")
            Me.Invoke(Sub()
                          brAddNewIp.Enabled = True
                          brDelete.Enabled = True
                      End Sub)
        End Try


    End Sub

    Private Sub AddMessage(message As String)
        'txOutput.Text = "[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf & txOutput.Text
        If (message IsNot Nothing) Then
            Try
                If Me.InvokeRequired Then
                    ' calls itself, but on correct thread
                    Me.BeginInvoke(New Action(Of Object)(AddressOf txtOutput.MaskBox.AppendText), New Object() {"[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf})
                Else
                    txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf)
                End If
            Catch ex As InvalidOperationException
            End Try
        End If
        Try
            If (txtOutput.Lines.Length > 5001) Then
                Dim index As Integer = 0
                Dim linesCount As Integer = txtOutput.Lines.Length
                Dim txtOutputText As String = txtOutput.Text
                For cnt = 0 To linesCount - 5000
                    index = txtOutputText.IndexOf(vbCr, index) + vbCr.Length
                Next
                If (index > 0) Then
                    txtOutput.Text = txtOutput.Text.Remove(0, index).TrimStart({ControlChars.Cr, ControlChars.Lf})
                End If
            End If
        Catch
        End Try
    End Sub


    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        If (e.ProgressPercentage = -1) Then
            Me.BarStaticItemStatus.Caption = Mid(CStr(e.UserState), 1, 200) & "..."
        Else
            AddMessage(CStr(e.UserState))
        End If
    End Sub


    Private Sub TryAddMessage(progress As Integer, message As String)
        Try
            bw.ReportProgress(progress, message)
        Catch ex As InvalidOperationException
            AddMessage(message)
        Catch ex As Exception
        End Try
    End Sub


    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then
            Me.BarStaticItemStatus.Caption = "Canceled"
            tmrTask.Enabled = False
        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub



    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub

    Private Sub BarButtonItemRun_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRun.ItemClick
        Try
            tmrTask.Enabled = False

            If Not bw.IsBusy = True Then
                BarStaticItemStatus.Caption = "Running"
                bw.RunWorkerAsync()
                BarButtonItemRun.Enabled = False
            Else
                Try
                    bw.CancelAsync()
                    bw.RunWorkerAsync()
                    BarStaticItemStatus.Caption = "Running"
                Catch ex As Exception
                    AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick -> bw.IsBusy" & vbCrLf & ex.Message)
                End Try
            End If

        Catch ex As Exception
            AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick" & vbCrLf & ex.Message)
        Finally
            tmrTask.Enabled = True
            tmrTask.Interval = tmrTaskInterval
        End Try
    End Sub


    Private Sub BarButtonItemCancelJob_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCancelJob.ItemClick
        bw.CancelAsync()
        tmrTask.Enabled = False
        BarButtonItemRun.Enabled = True
    End Sub

    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        If Not bw.IsBusy = True Then
            BarStaticItemStatus.Caption = "Running"
            bw.RunWorkerAsync()
            BarButtonItemRun.Enabled = False
        End If
        tmrTask.Interval = tmrTaskInterval
    End Sub


    Private Sub StartTimer()
        Dim r As New Random(System.DateTime.Now.Millisecond)
        Dim interval As Integer = r.Next(3, 6)

        If (tmrTaskInterval = 0) Then tmrTaskInterval = tmrTask.Interval
        tmrTask.Interval = interval * 1000
        tmrTask.Enabled = True
        tmrTask.Start()

        BarStaticItemStatus.Caption = "First run in " & tmrTask.Interval / 1000 & " sec"
    End Sub


    Private Sub brRefreshHistory_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles brRefreshHistory.ItemClick
        refreshHistory()
    End Sub

    Private Sub brInBlockRefresh_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles brInBlockRefresh.ItemClick
        refreshHistory()
    End Sub

    Private Sub PerformImageComparison_ProgressChanged(sender As Object, e As GoomenaImageCompare.ProgressChangedEventArgs)
        TryAddMessage(0, "Image Comparison-->" & e.CurrentUrl)
    End Sub




  
    Private Sub brAddNewIp_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles brAddNewIp.ItemClick
        Using frm As New frmAddIp
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                refreshblockedIps()
            End If
        End Using
    End Sub

    Private Sub brDelete_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles brDelete.ItemClick
        If grvBlocked.FocusedRowHandle > -1 Then
            Dim ip As String = grvBlocked.GetFocusedRowCellValue(colIp)
            If MsgBox("Are you sure you want to unblock ip:" & ip & "?", MsgBoxStyle.Information + MsgBoxStyle.YesNoCancel, "Confirm") = MsgBoxResult.Yes Then
                Dim b As Boolean = False
                Dim t1 As task = Task.Run(Sub()
                                              Try
                                                  Using _CMSDBDataContext As New CMSDBDataContext(My.MySettings.Default.AppDBconnectionString)
                                                      Dim sys As SYS_BlockedIp = (From t As SYS_BlockedIp In _CMSDBDataContext.SYS_BlockedIps
                                                             Where t.Ip = ip).FirstOrDefault
                                                      If sys IsNot Nothing Then
                                                          _CMSDBDataContext.SYS_BlockedIps.DeleteOnSubmit(sys)
                                                      End If
                                                      Try
                                                          Dim serverManager As ServerManager = New ServerManager
                                                          Dim config As Configuration = serverManager.GetApplicationHostConfiguration
                                                          Dim ipSecuritySection As ConfigurationSection = config.GetSection("system.webServer/security/ipSecurity")
                                                          ipSecuritySection.SetAttributeValue("denyAction", "AbortRequest")
                                                          Dim ipSecurityCollection As ConfigurationElementCollection = ipSecuritySection.GetCollection
                                                          For i As Integer = ipSecurityCollection.Count - 1 To 0 Step -1
                                                              Dim ip1 As String = ipSecurityCollection(i).Item("ipAddress")
                                                              If ip1 = ip Then
                                                                  ipSecurityCollection.Remove(ipSecurityCollection(i))
                                                                  Exit For
                                                              End If
                                                          Next
                                                          _CMSDBDataContext.SubmitChanges()
                                                          serverManager.CommitChanges()
                                                          b = True
                                                      Catch ex As Exception
                                                          Me.Invoke(Sub()
                                                                        MsgBox(ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Error")
                                                                    End Sub
                                                              )
                                                      End Try


                                                  End Using
                                              Catch ex As Exception
                                                  Me.Invoke(Sub()
                                                                MsgBox(ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Error")
                                                            End Sub)
                                              End Try


                                          End Sub, Nothing)
                t1.Wait()
                If b Then
                    refreshblockedIps()
                End If
            End If
        End If
    End Sub

    Private Sub grBlockedIps_Click(sender As Object, e As EventArgs) Handles grBlockedIps.Click

    End Sub

    Private Sub grvBlocked_FocusedRowChanged(sender As Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles grvBlocked.FocusedRowChanged
        If grvBlocked.FocusedRowHandle > -1 Then
            brDelete.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            brDelete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If
    End Sub

    Private Sub grvBlocked_RowCountChanged(sender As Object, e As EventArgs) Handles grvBlocked.RowCountChanged
        If grvBlocked.RowCount > 0 Then
            brDelete.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
        Else
            brDelete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If
    End Sub
End Class
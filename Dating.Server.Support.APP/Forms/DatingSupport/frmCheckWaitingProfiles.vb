﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Data.SqlClient
Imports System.Text

Public Class frmCheckWaitingProfiles

    Dim tmrTaskInterval As Integer
    Dim timeFrmt As String = "yyyy-MM-dd HH:mm"
    Dim timeSpanFrmt As String = "hh\:mm\:ss"
    Dim emailRecipient As String = "maria@goomena.com"
    Dim MySMTPSettings As clsMyMail.SMTPSettings
    Dim SYS_SMTPServer1 As SYS_SMTPServer
    Dim mobileLastDateSent As DateTime = DateTime.MinValue

    Private Sub frmCheckWaitingProfiles_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GroupControl1.Text = Me.Text
        BarStaticItemStatus.Caption = "Not running"

        StartTimer()
    End Sub


    Private Sub frmCheckWaitingProfiles_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

            bw.CancelAsync()
            bw.Dispose()
            bw = Nothing
            tmrTask.Enabled = False
            Me.Dispose()

        Catch ex As Exception
        End Try
    End Sub



    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork
        Try

            If bw.CancellationPending = True Then
                e.Cancel = True
            Else
                PerformCheck()
            End If


        Catch ex As Exception
            bw.ReportProgress(0, ex.Message)
        Finally

        End Try
    End Sub


    Private Sub PerformCheck()

        Try
            '  Dim ds As New DSSYSData()
            Using ds = DataHelpers.SYS_UsersLogins_GetAll()



                If (ds.SYS_UsersLogins.Rows.Count = 0) Then
                    bw.ReportProgress(0, "No supporters online. Exiting. ")
                    Return
                End If

                Dim supporterOnline As New StringBuilder()
                For cnt = 0 To ds.SYS_UsersLogins.Rows.Count - 1
                    Dim dr As DataRow = ds.SYS_UsersLogins.Rows(cnt)
                    supporterOnline.Append(dr("LoginName"))

                    If (Not dr.IsNull("ComputerName") AndAlso
                        dr("ComputerName").ToString().Trim() <> "") Then
                        supporterOnline.Append(" (ComputerName-" & dr("ComputerName") & ")")
                    End If

                    supporterOnline.Append(", ")
                Next

                supporterOnline.Remove(supporterOnline.Length - 2, 1)
                Dim config As New clsConfigValues()
                If (config.auto_approve_new_profiles <> "0") Then
                    bw.ReportProgress(0, "Automatic approve mode enabled.")
                    'Return
                Else
                    bw.ReportProgress(0, "Manual approve mode.")
                End If

                Dim UTCTime As DateTime = DateTime.UtcNow
                Dim GTBZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("GTB Standard Time")
                Dim GTBTime As DateTime = TimeZoneInfo.ConvertTime(UTCTime, TimeZoneInfo.Utc, GTBZone)
                Dim timeDiff As TimeSpan = GTBTime - UTCTime


                Dim reportEmailBody As String = <body><![CDATA[
[MODE]
Supporter(s) online: [SUPPORTER]
UTC Time: [UTC_TIME]
Greece Time: [GREECE_TIME]
Time difference: [TIME_DIFF]

]]></body>

                Dim reportEmailBodyNewProfiles As String = <body><![CDATA[
--------------------------------------------
New Profiles: [NEW_COUNT]
Login names: [LOGINS]
Older profile registered: [LOGIN], [REG_DATE] (Greece time)
MAX Awaiting time : [AWAITING_TIME]

]]></body>

                Dim reportEmailBodyUpdatingProfiles As String = <body><![CDATA[
--------------------------------------------
Updating Profiles: [UPD_COUNT]
Login names: [LOGINS]
Older profile update time: [LOGIN], [UPD_DATE] (Greece time) 
MAX Awaiting time : [AWAITING_TIME]

]]></body>

               
              
              
                Try
                    Using dsNew As New DSMembersAdmin()
                        Using con As New SqlConnection(My.MySettings.Default.AppDBconnectionString)
                            Using taNew As New DSMembersAdminTableAdapters.EUS_ProfilesTableAdapter
                                taNew.Connection = con
                                taNew.FillBy_Status(dsNew.EUS_Profiles, ProfileStatusEnum.NewProfile)
                                If (dsNew.EUS_Profiles.Rows.Count > 0) Then

                                    Dim LOGINS As New System.Text.StringBuilder()

                                    Dim mindate As DateTime = DateTime.MaxValue
                                    Dim minDrs As DSMembersAdmin.EUS_ProfilesRow
                                    Dim LOGIN As String = ""

                                    Dim drs As DSMembersAdmin.EUS_ProfilesRow() = dsNew.EUS_Profiles.Select()
                                    For cnt = 0 To drs.Length - 1
                                        If (mindate > drs(cnt).DateTimeToRegister) Then
                                            minDrs = drs(cnt)
                                            mindate = drs(cnt).DateTimeToRegister
                                            LOGIN = minDrs.LoginName
                                        End If
                                        LOGINS.Append(drs(cnt).LoginName & ", ")
                                    Next
                                    LOGINS.Remove(LOGINS.Length - 2, 1)



                                    reportEmailBodyNewProfiles = reportEmailBodyNewProfiles.Replace("[NEW_COUNT]", dsNew.EUS_Profiles.Rows.Count)
                                    reportEmailBodyNewProfiles = reportEmailBodyNewProfiles.Replace("[LOGINS]", LOGINS.ToString())
                                    reportEmailBodyNewProfiles = reportEmailBodyNewProfiles.Replace("[LOGIN]", LOGIN)
                                    reportEmailBodyNewProfiles = reportEmailBodyNewProfiles.Replace("[REG_DATE]", (mindate + timeDiff).ToString(timeFrmt))
                                    reportEmailBodyNewProfiles = reportEmailBodyNewProfiles.Replace("[AWAITING_TIME]", (DateTime.UtcNow - mindate).ToString(timeSpanFrmt))

                                End If


                            End Using
                        End Using
                        Using dsUpdating As New DSMembersAdmin()
                            Using con As New SqlConnection(My.MySettings.Default.AppDBconnectionString)
                                Using taUpdating As New DSMembersAdminTableAdapters.EUS_ProfilesTableAdapter
                                    Try

                                        taUpdating.Connection = con
                                        taUpdating.FillBy_Status(dsUpdating.EUS_Profiles, ProfileStatusEnum.Updating)
                                        If (dsUpdating.EUS_Profiles.Rows.Count > 0) Then

                                            Dim LOGINS As New System.Text.StringBuilder()

                                            Dim mindate As DateTime = DateTime.MaxValue
                                            Dim minDrs As DSMembersAdmin.EUS_ProfilesRow
                                            Dim LOGIN As String = ""

                                            Dim drs As DSMembersAdmin.EUS_ProfilesRow() = dsUpdating.EUS_Profiles.Select()
                                            For cnt = 0 To drs.Length - 1
                                                If (mindate > drs(cnt).LastUpdateProfileDateTime) Then
                                                    minDrs = drs(cnt)
                                                    mindate = drs(cnt).LastUpdateProfileDateTime
                                                    LOGIN = minDrs.LoginName
                                                End If
                                                LOGINS.Append(drs(cnt).LoginName & ", ")
                                            Next
                                            LOGINS.Remove(LOGINS.Length - 2, 1)


                                            reportEmailBodyUpdatingProfiles = reportEmailBodyUpdatingProfiles.Replace("[UPD_COUNT]", dsUpdating.EUS_Profiles.Rows.Count)
                                            reportEmailBodyUpdatingProfiles = reportEmailBodyUpdatingProfiles.Replace("[LOGINS]", LOGINS.ToString())
                                            reportEmailBodyUpdatingProfiles = reportEmailBodyUpdatingProfiles.Replace("[LOGIN]", LOGIN)
                                            reportEmailBodyUpdatingProfiles = reportEmailBodyUpdatingProfiles.Replace("[UPD_DATE]", (mindate + timeDiff).ToString(timeFrmt))
                                            reportEmailBodyUpdatingProfiles = reportEmailBodyUpdatingProfiles.Replace("[AWAITING_TIME]", (DateTime.UtcNow - mindate).ToString(timeSpanFrmt))

                                        End If
                                    Catch ex As Exception
                                        bw.ReportProgress(0, ex.Message)
                                    End Try
                                End Using
                            End Using
                            If (dsNew.EUS_Profiles.Rows.Count > 0 OrElse dsUpdating.EUS_Profiles.Rows.Count > 0) Then

                                If (config.auto_approve_new_profiles = "1") Then
                                    reportEmailBody = reportEmailBody.Replace("[MODE]", "Automatic Mode")
                                Else
                                    reportEmailBody = reportEmailBody.Replace("[MODE]", "Manual Mode")
                                End If
                                reportEmailBody = reportEmailBody.Replace("[SUPPORTER]", supporterOnline.ToString())
                                reportEmailBody = reportEmailBody.Replace("[UTC_TIME]", UTCTime.ToString(timeFrmt))
                                reportEmailBody = reportEmailBody.Replace("[GREECE_TIME]", GTBTime.ToString(timeFrmt))
                                reportEmailBody = reportEmailBody.Replace("[TIME_DIFF]", timeDiff.ToString(timeSpanFrmt))


                                If (dsNew.EUS_Profiles.Rows.Count > 0) Then
                                    reportEmailBody = reportEmailBody & reportEmailBodyNewProfiles
                                End If

                                If (dsUpdating.EUS_Profiles.Rows.Count > 0) Then
                                    reportEmailBody = reportEmailBody & reportEmailBodyUpdatingProfiles
                                End If



                                If (clsMyMail.MySMTPSettingsSupport.gSMTPServerName Is Nothing OrElse clsMyMail.MySMTPSettingsSupport.gSMTPServerName.Length = 0) Then
                                    Dim SYS_SMTPServe As SYS_SMTPServer = DataHelpers.SYS_SMTPServers_GetByNameLINQ("GOOSupport")
                                    If SYS_SMTPServe IsNot Nothing Then
                                        clsMyMail.MySMTPSettingsSupport.gSMTPOutPort = SYS_SMTPServe.SMTPOutPort
                                        clsMyMail.MySMTPSettingsSupport.gSMTPServerName = SYS_SMTPServe.SMTPServerName
                                        clsMyMail.MySMTPSettingsSupport.gSMTPLoginName = SYS_SMTPServe.SMTPLoginName
                                        clsMyMail.MySMTPSettingsSupport.gSMTPPassword = SYS_SMTPServe.SMTPPassword
                                        clsMyMail.MySMTPSettingsSupport.gSMTPenableSsl = SYS_SMTPServe.SmtpUseSsl
                                    End If
                                   
                                End If


                                Dim subject As String = "Goomena awaiting profiles report New-{0}, Updating-{1}"
                                subject = String.Format(subject, dsNew.EUS_Profiles.Rows.Count, dsUpdating.EUS_Profiles.Rows.Count)

                                clsMyMail.TrySendMailSupport(emailRecipient, subject, reportEmailBody, False)
                              
                                bw.ReportProgress(0, "sent  to " & emailRecipient & ", " & subject)


                                If (mobileLastDateSent = DateTime.MinValue OrElse ((UTCTime - mobileLastDateSent).Hours >= 3)) Then

                                    Dim phonesList As New List(Of String)
                                    If (Not String.IsNullOrEmpty(config.admin_controler_phone1)) Then phonesList.Add(config.admin_controler_phone1)
                                    If (Not String.IsNullOrEmpty(config.admin_controler_phone2)) Then phonesList.Add(config.admin_controler_phone2)
                                    If (Not String.IsNullOrEmpty(config.admin_controler_phone3)) Then phonesList.Add(config.admin_controler_phone3)
                                    If (Not String.IsNullOrEmpty(config.admin_controler_phone4)) Then phonesList.Add(config.admin_controler_phone4)
                                    If (Not String.IsNullOrEmpty(config.admin_controler_phone5)) Then phonesList.Add(config.admin_controler_phone5)

                                    If (phonesList.Count > 0) Then

                                        For Each itm In phonesList
                                            clsMobile.SendSMSViaClickatell(itm, subject, "Soundsoft", "jim2000", "3081457")
                                            bw.ReportProgress(0, "sending sms to " & itm)
                                        Next

                                        mobileLastDateSent = UTCTime
                                    End If

                                End If

                            End If
                        End Using
                    End Using
                Catch ex As Exception
                    bw.ReportProgress(0, ex.Message)
                End Try


               
            End Using
        Catch ex As Exception
            bw.ReportProgress(0, ex.Message)
        End Try

    End Sub



    Private Sub AddMessage(message As String)
        If (message IsNot Nothing) Then
            txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf)
        End If
        Try
            If (txtOutput.Lines.Length > 5001) Then
                Dim index As Integer = 0
                Dim linesCount As Integer = txtOutput.Lines.Length
                Dim txtOutputText As String = txtOutput.Text
                For cnt = 0 To linesCount - 5000
                    index = txtOutputText.IndexOf(vbCr, index) + vbCr.Length
                Next
                If (index > 0) Then
                    txtOutput.Text = txtOutput.Text.Remove(0, index).TrimStart({ControlChars.Cr, ControlChars.Lf})
                End If
            End If
        Catch
        End Try
    End Sub


    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then
            Me.BarStaticItemStatus.Caption = "Canceled"
            tmrTask.Enabled = False
        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub



    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub

    Private Sub BarButtonItemRun_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRun.ItemClick
        Try
            tmrTask.Enabled = False

            If Not bw.IsBusy = True Then
                BarStaticItemStatus.Caption = "Running"
                bw.RunWorkerAsync()
                BarButtonItemRun.Enabled = False
            Else
                Try
                    bw.CancelAsync()
                    bw.RunWorkerAsync()
                    BarStaticItemStatus.Caption = "Running"
                Catch ex As Exception
                    AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick -> bw.IsBusy" & vbCrLf & ex.Message)
                End Try
            End If

        Catch ex As Exception
            AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick" & vbCrLf & ex.Message)
        Finally
            tmrTask.Enabled = True
            tmrTask.Interval = tmrTaskInterval
        End Try

    End Sub


    Private Sub BarButtonItemCancelJob_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCancelJob.ItemClick
        bw.CancelAsync()
        tmrTask.Enabled = False
        BarButtonItemRun.Enabled = True
    End Sub

    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        AddMessage(CStr(e.UserState))
    End Sub



    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        If Not bw.IsBusy = True Then
            BarStaticItemStatus.Caption = "Running"
            bw.RunWorkerAsync()
            BarButtonItemRun.Enabled = False
        End If
        tmrTask.Interval = tmrTaskInterval
        'tmrTask.Interval = 30000
    End Sub


    Private Sub StartTimer()
        Dim r As New Random(System.DateTime.Now.Millisecond)
        Dim interval As Integer = r.Next(30, 60)

        If (tmrTaskInterval = 0) Then tmrTaskInterval = tmrTask.Interval
        tmrTask.Interval = interval * 1000
        tmrTask.Enabled = True

        tmrTask.Enabled = True
        tmrTask.Start()

        BarStaticItemStatus.Caption = "First run in " & tmrTask.Interval / 1000 & " sec"
    End Sub


End Class
﻿Imports System.ComponentModel
Imports DevExpress.Skins
Imports DevExpress.LookAndFeel
Imports DevExpress.UserSkins
Imports DevExpress.XtraEditors


Public Class frmMain
    Sub New()
        'InitSkins()
        InitializeComponent()
    End Sub

    'Sub InitSkins()
    '    DevExpress.Skins.SkinManager.EnableFormSkins()
    '    DevExpress.UserSkins.BonusSkins.Register()
    '    UserLookAndFeel.Default.SetSkinStyle("DevExpress Style")
    'End Sub

    Private Sub frmMain_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            Using sqlConn As New SqlClient.SqlConnection(My.Settings.AppDBconnectionString)


                'Me.Text = m_SiteURL & " Goomena CPanel Administrator - Ver: " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & " WS:" & gAdminWS.Url & " - APP:" & RegistryAPP & " Site Name:" & m_SiteName
                Me.Text = " Goomena CPanel Administrator Support - Ver: " & _
                    My.Application.Info.Version.ToString() & _
                    " Connecting to SQL Server : " & sqlConn.DataSource & " Database=" & sqlConn.Database

            End Using
#If DEBUG Then
            '  My.Settings.AppDBconnectionString = My.Settings.AppDBconnectionString__LOCAL
#End If
            Core.DLL.DataHelpers.SetConnectionString(My.Settings.AppDBconnectionString)

            LoadPredifenedSubForms()
            ReadSMTPSettings()
#If DEBUG Then
            BarButtonItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Always
#End If
        Catch ex As Exception
            ErrorMsgBox("frmMain->frmMain_Load")
        End Try
    End Sub



    Public Sub LoadPredifenedSubForms()
        Try
            '' If (1 = 2) Then

            Dim frm As New frmDeletingPhotos()
            frm.Text = NavBarItemDeletingPhotos.Caption
            LoadSubForm(frm)

            Dim frm2 As New frmSendingEmails()
            frm2.Text = NavBarItemSendingEmails.Caption
            LoadSubForm(frm2)

            Dim frm3 As New frmCheckWaitingProfiles()
            frm3.Text = nbiCheckWaitingProfiles.Caption
            LoadSubForm(frm3)

            Dim frm4 As New frmHandleMemberActions()
            frm4.Text = NavBarItemHandleMemberActions.Caption
            LoadSubForm(frm4)

            Dim frm5 As New frmTasks()
            frm5.Text = NavBarItemVariousTasks.Caption
            LoadSubForm(frm5)

            Dim frm6 As New frmSuspiciousIp
            frm6.Text = nvbBlockedIps.Caption
            LoadSubForm(frm6)
          

            Dim frm7 As New frmReferrals
            frm7.Text = nvbOnlines.Caption
            LoadSubForm(frm7)
        Catch ex As Exception
            ErrorMsgBox("frmMain->LoadPredifenedSubForms")
        End Try
    End Sub



    Public Sub LoadSubForm(ByVal frm As XtraForm)
        On Error GoTo er
        Dim i As Integer
        For i = 0 To xtraTabbedMdiManager1.Pages.Count - 1
            If CheckPageName(xtraTabbedMdiManager1.Pages(i).Text, frm.Text) Then
                xtraTabbedMdiManager1.SelectedPage = xtraTabbedMdiManager1.Pages(i)
                Return
            End If
        Next i
        LoadSubFormEx(frm)
        Exit Sub
er:     ErrorMsgBox("frmMain->LoadSubForm")
    End Sub


    Public Sub LoadSubFormEx(ByVal frm As XtraForm)
        Try
            frm.MdiParent = Me
            frm.Show()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->LoadSubFormEx")
        End Try
    End Sub

    Shared Function CheckPageName(PageText As String, frmText As String) As Boolean
        Dim isMatch As Boolean

        Try
            If (PageText = frmText) Then
                isMatch = True
            Else

                Dim frmTextNew As String = frmText

                If (frmText.IndexOf("[") > -1 AndAlso frmText.IndexOf("]") > -1) Then
                    frmTextNew = frmText.Remove(frmText.IndexOf("[") + 1, frmText.IndexOf("]") - (frmText.IndexOf("[") + 1))
                End If

                If (frmText = frmTextNew) Then ' not changed, try parenthesis
                    If (frmText.IndexOf("(") > -1 AndAlso frmText.IndexOf(")") > -1) Then
                        frmTextNew = frmText.Remove(frmText.IndexOf("(") + 1, frmText.IndexOf(")") - (frmText.IndexOf("(") + 1))
                    End If
                End If


                Dim PageTextNew As String = PageText
                If (PageText.IndexOf("[") > -1 AndAlso PageText.IndexOf("]") > -1) Then
                    PageTextNew = PageText.Remove(PageText.IndexOf("[") + 1, PageText.IndexOf("]") - (PageText.IndexOf("[") + 1))
                End If

                If (PageText = PageTextNew) Then ' not changed, try parenthesis
                    If (PageText.IndexOf("(") > -1 AndAlso PageText.IndexOf(")") > -1) Then
                        PageTextNew = PageText.Remove(PageText.IndexOf("(") + 1, PageText.IndexOf(")") - (PageText.IndexOf("(") + 1))
                    End If
                End If


                If (frmTextNew = PageTextNew) Then
                    isMatch = True
                End If

            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->LoadSubFormEx")
        End Try

        Return isMatch
    End Function

    Private Sub NavBarItemDeletingPhotos_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemDeletingPhotos.LinkClicked
        Try
            Dim frm As New frmDeletingPhotos()
            frm.Text = NavBarItemDeletingPhotos.Caption
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemDeletingPhotos_LinkClicked")
        End Try

    End Sub

    Private Sub NavBarItemPoints_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemPoints.LinkClicked
        Try
            Dim frm3 As New frmUpdatingPoints()
            frm3.Text = NavBarItemPoints.Caption
            LoadSubForm(frm3)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemPoints_LinkClicked")
        End Try
    End Sub

    Private Sub NavBarItemSendingEmails_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemSendingEmails.LinkClicked
        Try
            Dim frm2 As New frmSendingEmails()
            frm2.Text = NavBarItemSendingEmails.Caption
            LoadSubForm(frm2)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemSendingEmails_LinkClicked")
        End Try
    End Sub

    Private Sub NavBarItemSheduledTasks_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemVariousTasks.LinkClicked
        Try
            Dim frm2 As New frmTasks()
            frm2.Text = NavBarItemVariousTasks.Caption
            LoadSubForm(frm2)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemVariousTasks")
        End Try
    End Sub

    Private Sub nbiCheckWaitingProfiles_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nbiCheckWaitingProfiles.LinkClicked
        Try
            Dim frm2 As New frmCheckWaitingProfiles()
            frm2.Text = nbiCheckWaitingProfiles.Caption
            LoadSubForm(frm2)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->nbiCheckWaitingProfiles")
        End Try
    End Sub


    Private Sub NavBarItemHandleMemberActions_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemHandleMemberActions.LinkClicked
        Try
            Dim frm2 As New frmHandleMemberActions()
            frm2.Text = NavBarItemHandleMemberActions.Caption
            LoadSubForm(frm2)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemHandleMemberActions")
        End Try
    End Sub


    Private Sub BarButtonItem1_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        Try
            Dim i As Integer
            For i = xtraTabbedMdiManager1.Pages.Count - 1 To 0 Step -1
                xtraTabbedMdiManager1.Pages(i).MdiChild.Close()
            Next i

            Dim frm2 As New frmTasks()
            frm2.MaintainDBIndexes()
            frm2.ShowDialog()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemHandleMemberActions")
        End Try
        '  frmSendingEmails.Create_NotificationEmail_Tell_us_who_you_like_Receive_MALE()
    End Sub

    Private Sub nvbBlockedIps_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvbBlockedIps.LinkClicked
        Try
            Dim frm2 As New frmSuspiciousIp
            frm2.Text = nvbBlockedIps.Caption
            LoadSubForm(frm2)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemHandleMemberActions")
        End Try
    End Sub

    Private Sub nvbOnlines_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nvbOnlines.LinkClicked
        Try
            Dim frm2 As New frmReferrals
            frm2.Text = nvbOnlines.Caption
            LoadSubForm(frm2)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemHandleMemberActions")
        End Try
    End Sub
End Class

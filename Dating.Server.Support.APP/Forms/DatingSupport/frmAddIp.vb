﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Text
Imports System.Linq
Imports System.Data.Linq
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient
Imports System.Threading.Tasks
Imports System
Imports Microsoft.Web.Administration

Public Class frmAddIp

    Private Function validateForm() As Boolean
        Dim b As Boolean = False
        If txtIp.Text = "" Then
            txtIp.ErrorText = "Ip cannot be empty."
            Return False
        Else
            Dim tmp As Boolean = True

            Try


                Using _CMSDBDataContext As New CMSDBDataContext(My.MySettings.Default.AppDBconnectionString)
                    Dim sys As SYS_BlockedIp = (From t2 As SYS_BlockedIp In _CMSDBDataContext.SYS_BlockedIps
                                                            Where t2.Ip = txtIp.Text).FirstOrDefault
                    If sys IsNot Nothing Then

                        txtIp.ErrorText = "This ip already exists"
                        Return False


                    Else

                        txtIp.ErrorText = Nothing


                    End If
                End Using
            Catch ex As Exception

            End Try

        End If
        If dtUntil.DateTime < DateTime.UtcNow Then
            dtUntil.ErrorText = "Until must big after UtcNow"
            Return False
        Else
            dtUntil.ErrorText = Nothing
        End If
        Return True
    End Function


    Private Sub frmAddIp_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub cmdOk_Click(sender As Object, e As EventArgs) Handles cmdOk.Click
        If validateForm() Then
            Dim b As Boolean = False
            Dim t As Task = Task.Run(Sub()
                                         Try
                                             Using _CMSDBDataContext As New CMSDBDataContext(My.MySettings.Default.AppDBconnectionString)
                                                 Dim sys As New SYS_BlockedIp
                                                 sys.Ip = txtIp.Text
                                                 sys.DatetimeBlocked = DateTime.UtcNow
                                                 sys.DatetimeUblocked = dtUntil.DateTime
                                                 sys.IsInBlock = True
                                                 sys.CountTimesBlocked = 1
                                                 Try
                                                     Dim serverManager As ServerManager = New ServerManager
                                                     Dim config As Configuration = serverManager.GetApplicationHostConfiguration
                                                     Dim ipSecuritySection As ConfigurationSection = config.GetSection("system.webServer/security/ipSecurity")
                                                     ipSecuritySection.SetAttributeValue("denyAction", "AbortRequest")
                                                     Dim ipSecurityCollection As ConfigurationElementCollection = ipSecuritySection.GetCollection
                                                     Dim addElement As ConfigurationElement = ipSecurityCollection.CreateElement("add")
                                                     addElement("ipAddress") = txtIp.Text
                                                     addElement("allowed") = False
                                                     ipSecurityCollection.Add(addElement)
                                                     serverManager.CommitChanges()
                                                     _CMSDBDataContext.SYS_BlockedIps.InsertOnSubmit(sys)
                                                     _CMSDBDataContext.SubmitChanges()
                                                     b = True
                                                 Catch ex As Exception
                                                     Me.Invoke(Sub()
                                                                   MsgBox(ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Error")
                                                               End Sub
                                                         )
                                                 End Try


                                             End Using
                                         Catch ex As Exception
                                             Me.Invoke(Sub()
                                                           MsgBox(ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Error")
                                                       End Sub
                                                       )
                                         End Try
                                     End Sub)
            t.Wait()
            If b = True Then
                Me.DialogResult = Windows.Forms.DialogResult.OK
                Me.Close()
            End If
        End If
    End Sub

    Private Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub
End Class
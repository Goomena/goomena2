﻿Imports Dating.Server.Datasets.DLL
Imports Dating.Server.Core.DLL
Imports System.Text
Imports System.Linq
Imports System.Data.Linq


Public Class frmReferrals

    Dim tmrTaskInterval As Integer
    ' Dim isBusy_PerformImageComparison As Boolean
    Dim ConnectionString As String = My.Settings.AppDBconnectionString

    Private Sub frmSendingEmails_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GroupControl1.Text = Me.Text
        BarStaticItemStatus.Caption = "Not running"

        StartTimer()
    End Sub


    Private Sub frmSendingEmails_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try

            bw.CancelAsync()
            bw.Dispose()
            bw = Nothing
            tmrTask.Enabled = False
            Me.Dispose()

        Catch ex As Exception
        End Try
    End Sub

    Private CountOnlineNow As String = <sql><![CDATA[
SELECT count(*)
  FROM [dbo].[EUS_Profiles]
  where IsMaster=1 and ReferrerParentId>0 and 
  GenderId=2 and Status in (2,4) 
  and IsOnline=1 and LastActivityDateTime>=@date
]]></sql>
    Private GetProfilesToAddOnline As String = <sql><![CDATA[
SELECT top(@rows) [ProfileID]
  FROM [dbo].[EUS_Profiles]
  where IsMaster=1 and ReferrerParentId>0 and 
  GenderId=2 and Status in (2,4) 
  and IsOnline=0 and LastActivityDateTime<=@date
 order by NEWID()
]]></sql>
    Private GetMenOnline As String = <sql><![CDATA[
SELECT top(@rows) [ProfileID]
  FROM [dbo].[EUS_Profiles]
  where IsMaster=1 and 
  GenderId=1 and Status in (2,4) 
  and IsOnline=1 and LastActivityDateTime>=@date and ProfileID not in (SELECT [Eus_ProfileId]    
  FROM [dbo].[v_MarksViewed]
  where [Lastdate]>@dt and [DayComplete]=1)
order by LastActivityDateTime desc
]]></sql>
    Private CountViewedSend As String = <sql><![CDATA[
SELECT count([EUS_ProfilesViewedID])
        FROM [dbo].[EUS_ProfilesViewed]
  where [ToProfileID]=@ProfileId and [DateTimeToCreate]>=@dt
]]></sql>
    Private FemaletoSendView As String = <sql><![CDATA[
SELECT top(1) [ProfileID]
  FROM [dbo].[EUS_Profiles]
  where IsMaster=1 and 
  GenderId=2 and Status in (2,4) 
  and IsOnline=1 and LastActivityDateTime>=@date and ProfileID not in (SELECT FromProfileID         FROM [dbo].[EUS_ProfilesViewed]
  where [ToProfileID]=@ProfileId ) and ProfileID not in (select [FromProfileID] from [dbo].[EUS_Messages] where ToProfileID =@ProfileId)
  and ProfileID not in (select ToProfileID from [dbo].[EUS_Messages] where [FromProfileID] =@ProfileId) AND  not exists(select *  
				from EUS_ProfilesBlocked bl  
				where (
						bl.FromProfileID =[EUS_Profiles].ProfileID and bl.ToProfileID = @ProfileId
					) or  (
						bl.FromProfileID =@ProfileId and bl.ToProfileID = [EUS_Profiles].ProfileID
					))
order by LastActivityDateTime desc
]]></sql>
    Shared Generator As System.Random = New System.Random()
    Public Function GetRandom(ByVal Min As Integer, ByVal Max As Integer) As Integer
        ' by making Generator static, we preserve the same instance '
        ' (i.e., do not create new instances with the same seed over and over) '
        ' between calls '
        Return Generator.Next(Min, Max)
    End Function
    Private ttm As New Threading.Timer(AddressOf doCheck, Nothing, Threading.Timeout.Infinite, Threading.Timeout.Infinite)
    Private Sub doCheck(ByVal o As Object)
        If tmrTask.Enabled Then
            Try
                Using dbml As New CMSDBDataContext(My.Settings.AppDBconnectionString)
                    Dim r As vi_Profile() = (From itm As vi_Profile In dbml.vi_Profiles
                                            Where itm.LoggedIn = False AndAlso itm.LogIn <= DateTime.UtcNow Select itm).ToArray
                    If r IsNot Nothing Then
                        For Each vi In r
                            Try
                                Dim pr As EUS_Profile = (From itm In dbml.EUS_Profiles
                                                        Where itm.ProfileID = vi.Eus_ProfileId
                                                        Select itm).FirstOrDefault
                                If pr IsNot Nothing Then
                                    pr.IsOnline = True
                                    pr.LastActivityDateTime = DateTime.UtcNow
                                    vi.LoggedIn = True
                                    dbml.SubmitChanges()
                                    AddMessage("Login of Profile: " & pr.ProfileID & " - " & pr.LoginName)
                                End If
                            Catch ex As Exception
                                AddMessage(ex.Message)
                            End Try
                        Next
                    End If
                    r = (From itm As vi_Profile In dbml.vi_Profiles
                                       Where itm.LoggedIn = True AndAlso itm.LogOut <= DateTime.UtcNow Select itm).ToArray
                    If r IsNot Nothing Then
                        For Each vi In r
                            Try
                                Dim pr As EUS_Profile = (From itm In dbml.EUS_Profiles
                                                        Where itm.ProfileID = vi.Eus_ProfileId
                                                        Select itm).FirstOrDefault
                                If pr IsNot Nothing Then
                                    pr.IsOnline = False
                                    '  pr.LastActivityDateTime = DateTime.UtcNow
                                    dbml.vi_Profiles.DeleteOnSubmit(vi)
                                    dbml.SubmitChanges()
                                    AddMessage("Logout of Profile: " & pr.ProfileID & " - " & pr.LoginName)
                                End If
                            Catch ex As Exception
                                AddMessage(ex.Message)
                            End Try
                        Next
                    End If
                    r = (From itm As vi_Profile In dbml.vi_Profiles
                                     Where itm.LoggedIn = True AndAlso itm.LogIn >= DateTime.UtcNow.AddMinutes(-GetRandom(15, 20)) Select itm).ToArray
                    If r IsNot Nothing Then
                        For Each vi In r
                            Try
                                Dim pr As EUS_Profile = (From itm In dbml.EUS_Profiles
                                                        Where itm.ProfileID = vi.Eus_ProfileId
                                                        Select itm).FirstOrDefault
                                If pr IsNot Nothing Then
                                    pr.LastActivityDateTime = DateTime.UtcNow
                                    dbml.SubmitChanges()
                                End If
                            Catch ex As Exception
                                AddMessage(ex.Message)
                            End Try
                        Next
                    End If

                End Using
            Catch ex As Exception
                AddMessage(ex.Message)
            End Try
        End If
    End Sub
    Private Sub bw_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bw.DoWork
        AddMessage("Starting Check....")
        Dim hdl As New clsSendingEmailsHandler()
        Try

            Using con As New Data.SqlClient.SqlConnection(My.Settings.AppDBconnectionString)
                Try

                    Dim o As Integer = 0
                    Using cmd As Data.SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, CountOnlineNow)
                        cmd.Parameters.AddWithValue("@date", Now.AddMinutes(-30).ToUniversalTime)
                        o = CInt(DataHelpers.ExecuteScalar(cmd))
                        AddMessage("Total Logged in Profiles: " & o)
                    End Using
                    Dim ctm As DateTime = DateTime.UtcNow
                    Dim ctmf As DateTime = New DateTime(ctm.Year, ctm.Month, ctm.Day, 3, 0, 0)
                    Dim ctmt As DateTime = New DateTime(ctm.Year, ctm.Month, ctm.Day, 5, 30, 0)
                    Dim maxProfilesToAddOnline As Integer = 0
                    If ctm > ctmf AndAlso ctm < ctmt Then
                        If o < 2 Then
                            maxProfilesToAddOnline = GetRandom(2, 5)
                        End If
                    ElseIf ctm > ctmf AndAlso ctm < ctmt.AddHours(2) Then
                        If o < 4 Then
                            maxProfilesToAddOnline = GetRandom(4, 9)
                        End If
                    ElseIf ctm > ctmf AndAlso ctm < ctmt.AddHours(12) Then
                        If o < 15 Then
                            maxProfilesToAddOnline = GetRandom(15, 25)
                        End If
                    ElseIf ctm > ctmf.AddHours(-3) AndAlso ctm < ctmf Then
                        If o < 10 Then
                            maxProfilesToAddOnline = GetRandom(10, 15)
                        End If
                    Else
                        If o < 20 Then
                            maxProfilesToAddOnline = GetRandom(20, 30)
                        End If
                    End If
                    If maxProfilesToAddOnline > 0 Then
                        AddMessage("Needs to add " & maxProfilesToAddOnline & " online profiles")
                        Using cmd As Data.SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, GetProfilesToAddOnline)
                            cmd.Parameters.AddWithValue("@date", Now.AddHours(-3).ToUniversalTime)
                            cmd.Parameters.AddWithValue("@rows", maxProfilesToAddOnline)
                            Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
                            For Each r As DataRow In dt.Rows
                                Dim newvi As New vi_Profile
                                newvi.Eus_ProfileId = r.Item("ProfileID")
                                newvi.LogIn = DateTime.UtcNow.AddMinutes(GetRandom(0, 30))
                                newvi.LogOut = newvi.LogIn.AddMinutes(GetRandom(30, 180))
                                newvi.LoggedIn = False

                                Using dbml As New CMSDBDataContext(My.Settings.AppDBconnectionString)
                                    dbml.vi_Profiles.InsertOnSubmit(newvi)
                                    dbml.SubmitChanges()
                                End Using
                            Next
                        End Using
                    Else
                        AddMessage("No need to add Profiles Online")
                    End If
                Catch ex As Exception

                End Try

        Try


            Using cmd As Data.SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, GetMenOnline)
                        cmd.Parameters.AddWithValue("@date", Now.AddMinutes(-30).ToUniversalTime)
                cmd.Parameters.AddWithValue("@rows", 15)
                Dim d As DateTime = Now.AddDays(-1)
                d = New Date(d.Year, d.Month, d.Day, 23, 59, 59).ToUniversalTime
                cmd.Parameters.AddWithValue("@dt", d)
                Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
                Using dbml As New CMSDBDataContext(My.Settings.AppDBconnectionString)
                    For Each r As DataRow In dt.Rows
                        Try


                            Dim mrk As v_MarksViewed = (From itm In dbml.v_MarksVieweds
                                                       Where itm.Eus_ProfileId = CInt(r.Item("ProfileID"))).FirstOrDefault
                            Dim dt2 As DataTable = Nothing
                            If mrk IsNot Nothing Then
                                If mrk.Lastdate > d Then
                                    Using cmd2 As Data.SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, CountViewedSend)
                                        cmd2.Parameters.AddWithValue("@dt", d)
                                        cmd2.Parameters.AddWithValue("@ProfileId", CInt(r.Item("ProfileID")))
                                                Dim o As Integer = CInt(DataHelpers.ExecuteScalar(cmd2))
                                        If o >= mrk.dayCount Then
                                            mrk.DayComplete = True
                                        Else
                                            Using cmd3 As Data.SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, FemaletoSendView)
                                                        cmd3.Parameters.AddWithValue("@date", Now.AddMinutes(30).ToUniversalTime)
                                                cmd3.Parameters.AddWithValue("@ProfileId", CInt(r.Item("ProfileID")))
                                                dt2 = DataHelpers.GetDataTable(cmd)
                                            End Using
                                        End If
                                    End Using
                                End If
                            Else
                                mrk = New v_MarksViewed
                                mrk.Eus_ProfileId = CInt(r.Item("ProfileID"))
                                mrk.dayCount = GetRandom(0, 6)
                                mrk.Lastdate = DateTime.UtcNow
                                dbml.v_MarksVieweds.InsertOnSubmit(mrk)
                                Using cmd3 As Data.SqlClient.SqlCommand = DataHelpers.GetSqlCommand(con, FemaletoSendView)
                                            cmd3.Parameters.AddWithValue("@date", Now.AddMinutes(30).ToUniversalTime)
                                    cmd3.Parameters.AddWithValue("@ProfileId", CInt(r.Item("ProfileID")))
                                    dt2 = DataHelpers.GetDataTable(cmd)
                                End Using
                                If dt2 IsNot Nothing AndAlso dt2.Rows.Count > 0 Then
                                    Dim prid As Integer = CInt(dt2.Rows(0).Item("ProfileID"))
                                    clsUserDoes.MarkAsViewed(CInt(r.Item("ProfileID")), prid, True)
                                    mrk.Lastdate = DateTime.UtcNow
                                    dbml.SubmitChanges()
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                    Next
                End Using
            End Using
        Catch ex As Exception

        End Try

            End Using
        Catch ex As Exception
            bw.ReportProgress(0, ex.Message)
        Finally
            hdl.Dispose()
        End Try
    End Sub

    Private Sub bw_ProgressChanged(sender As System.Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bw.ProgressChanged
        If (e.ProgressPercentage = -1) Then
            Me.BarStaticItemStatus.Caption = Mid(CStr(e.UserState), 1, 200) & "..."
        Else
            AddMessage(CStr(e.UserState))
        End If
    End Sub

    Private Sub AddMessage(message As String)
        'txOutput.Text = "[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf & txOutput.Text
        If (message IsNot Nothing) Then
            Try
                If Me.InvokeRequired Then
                    ' calls itself, but on correct thread
                    Me.BeginInvoke(New Action(Of Object)(AddressOf txtOutput.MaskBox.AppendText), New Object() {"[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf})
                Else
                    txtOutput.MaskBox.AppendText("[" & Date.Now.ToLocalTime() & "] " & message & vbCrLf)
                End If
            Catch ex As InvalidOperationException
            End Try
        End If
        Try
            If (txtOutput.Lines.Length > 5001) Then
                Dim index As Integer = 0
                Dim linesCount As Integer = txtOutput.Lines.Length
                Dim txtOutputText As String = txtOutput.Text
                For cnt = 0 To linesCount - 5000
                    index = txtOutputText.IndexOf(vbCr, index) + vbCr.Length
                Next
                If (index > 0) Then
                    txtOutput.Text = txtOutput.Text.Remove(0, index).TrimStart({ControlChars.Cr, ControlChars.Lf})
                End If
            End If
        Catch
        End Try
    End Sub

    Private Sub TryAddMessage(progress As Integer, message As String)
        Try
            bw.ReportProgress(progress, message)
        Catch ex As InvalidOperationException
            AddMessage(message)
        Catch ex As Exception
        End Try
    End Sub


    Private Sub bw_RunWorkerCompleted(sender As System.Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bw.RunWorkerCompleted
        If e.Cancelled = True Then
            Me.BarStaticItemStatus.Caption = "Canceled"
            tmrTask.Enabled = False
        ElseIf e.Error IsNot Nothing Then
            Me.BarStaticItemStatus.Caption = "Error: " & e.Error.Message
        Else
            Me.BarStaticItemStatus.Caption = "Not running"
        End If
    End Sub



    Private Sub BarButtonItemClear_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemClear.ItemClick
        txtOutput.Text = ""
    End Sub

    Private Sub BarButtonItemRun_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemRun.ItemClick
        Try
            tmrTask.Enabled = False

            If Not bw.IsBusy = True Then
                BarStaticItemStatus.Caption = "Running"
                bw.RunWorkerAsync()
                BarButtonItemRun.Enabled = False
            Else
                Try
                    bw.CancelAsync()
                    bw.RunWorkerAsync()
                    BarStaticItemStatus.Caption = "Running"
                Catch ex As Exception
                    AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick -> bw.IsBusy" & vbCrLf & ex.Message)
                End Try
            End If

        Catch ex As Exception
            AddMessage(Me.Name & " -> BarButtonItemRun_ItemClick" & vbCrLf & ex.Message)
        Finally
            tmrTask.Enabled = True
            tmrTask.Interval = tmrTaskInterval
        End Try
    End Sub


    Private Sub BarButtonItemCancelJob_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemCancelJob.ItemClick
        bw.CancelAsync()
        tmrTask.Enabled = False
        BarButtonItemRun.Enabled = True
    End Sub

    Private Sub tmrTask_Tick(sender As System.Object, e As System.EventArgs) Handles tmrTask.Tick
        If Not bw.IsBusy = True Then
            BarStaticItemStatus.Caption = "Running"
            bw.RunWorkerAsync()
            BarButtonItemRun.Enabled = False
        End If
        If tmrTask.Interval <> tmrTaskInterval Then
            tmrTask.Interval = tmrTaskInterval
        End If
    End Sub


    Private Sub StartTimer()
        Dim r As New Random(System.DateTime.Now.Millisecond)
        Dim interval As Integer = r.Next(30, 60)

        If (tmrTaskInterval = 0) Then tmrTaskInterval = tmrTask.Interval
        tmrTask.Interval = interval * 1000
        tmrTask.Enabled = True

        tmrTask.Enabled = True
        tmrTask.Start()
        ttm.Change(0, 60000)
        BarStaticItemStatus.Caption = "First run in " & tmrTask.Interval / 1000 & " sec"
    End Sub






End Class
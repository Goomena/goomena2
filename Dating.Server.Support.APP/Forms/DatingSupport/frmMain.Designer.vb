﻿Imports DevExpress.Skins
Imports DevExpress.LookAndFeel
Imports DevExpress.UserSkins
Imports DevExpress.XtraEditors


<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.navbarImageListLarge = New System.Windows.Forms.ImageList(Me.components)
        Me.navbarImageList = New System.Windows.Forms.ImageList(Me.components)
        Me.barManager = New DevExpress.XtraBars.BarManager(Me.components)
        Me.bar2 = New DevExpress.XtraBars.Bar()
        Me.mFile = New DevExpress.XtraBars.BarSubItem()
        Me.iClose = New DevExpress.XtraBars.BarButtonItem()
        Me.iExit = New DevExpress.XtraBars.BarButtonItem()
        Me.mHelp = New DevExpress.XtraBars.BarSubItem()
        Me.iAbout = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.barButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.iNew = New DevExpress.XtraBars.BarButtonItem()
        Me.navBarControl = New DevExpress.XtraNavBar.NavBarControl()
        Me.NavBarGroupActions = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavBarItemDeletingPhotos = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItemPoints = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItemSendingEmails = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiSendingForCreditsExpiring = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItemVariousTasks = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbiCheckWaitingProfiles = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItemHandleMemberActions = New DevExpress.XtraNavBar.NavBarItem()
        Me.nbrHappyBirthday = New DevExpress.XtraNavBar.NavBarItem()
        Me.nvbBlockedIps = New DevExpress.XtraNavBar.NavBarItem()
        Me.xtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.nvbOnlines = New DevExpress.XtraNavBar.NavBarItem()
        CType(Me.barManager, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.navBarControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'navbarImageListLarge
        '
        Me.navbarImageListLarge.ImageStream = CType(resources.GetObject("navbarImageListLarge.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.navbarImageListLarge.TransparentColor = System.Drawing.Color.Transparent
        Me.navbarImageListLarge.Images.SetKeyName(0, "Mail_32x32.png")
        Me.navbarImageListLarge.Images.SetKeyName(1, "Organizer_32x32.png")
        '
        'navbarImageList
        '
        Me.navbarImageList.ImageStream = CType(resources.GetObject("navbarImageList.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.navbarImageList.TransparentColor = System.Drawing.Color.Transparent
        Me.navbarImageList.Images.SetKeyName(0, "Inbox_16x16.png")
        Me.navbarImageList.Images.SetKeyName(1, "Outbox_16x16.png")
        Me.navbarImageList.Images.SetKeyName(2, "Drafts_16x16.png")
        Me.navbarImageList.Images.SetKeyName(3, "Trash_16x16.png")
        Me.navbarImageList.Images.SetKeyName(4, "Calendar_16x16.png")
        Me.navbarImageList.Images.SetKeyName(5, "Tasks_16x16.png")
        '
        'barManager
        '
        Me.barManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bar2})
        Me.barManager.DockControls.Add(Me.barDockControlTop)
        Me.barManager.DockControls.Add(Me.barDockControlBottom)
        Me.barManager.DockControls.Add(Me.barDockControlLeft)
        Me.barManager.DockControls.Add(Me.barDockControlRight)
        Me.barManager.Form = Me
        Me.barManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mFile, Me.barButtonItem2, Me.iClose, Me.iNew, Me.iExit, Me.mHelp, Me.iAbout, Me.BarButtonItem1})
        Me.barManager.MainMenu = Me.bar2
        Me.barManager.MaxItemId = 13
        '
        'bar2
        '
        Me.bar2.BarName = "Main menu"
        Me.bar2.DockCol = 0
        Me.bar2.DockRow = 0
        Me.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mFile), New DevExpress.XtraBars.LinkPersistInfo(Me.mHelp), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem1)})
        Me.bar2.OptionsBar.MultiLine = True
        Me.bar2.OptionsBar.UseWholeRow = True
        Me.bar2.Text = "Main menu"
        '
        'mFile
        '
        Me.mFile.Caption = "&File"
        Me.mFile.Id = 0
        Me.mFile.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.iClose), New DevExpress.XtraBars.LinkPersistInfo(Me.iExit)})
        Me.mFile.Name = "mFile"
        '
        'iClose
        '
        Me.iClose.Caption = "&Close"
        Me.iClose.Id = 5
        Me.iClose.Name = "iClose"
        '
        'iExit
        '
        Me.iExit.Caption = "E&xit"
        Me.iExit.Id = 9
        Me.iExit.Name = "iExit"
        '
        'mHelp
        '
        Me.mHelp.Caption = "&Help"
        Me.mHelp.Id = 10
        Me.mHelp.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.iAbout)})
        Me.mHelp.Name = "mHelp"
        '
        'iAbout
        '
        Me.iAbout.Caption = "&About"
        Me.iAbout.Id = 11
        Me.iAbout.Name = "iAbout"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "test"
        Me.BarButtonItem1.Id = 12
        Me.BarButtonItem1.Name = "BarButtonItem1"
        Me.BarButtonItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(950, 22)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 550)
        Me.barDockControlBottom.Size = New System.Drawing.Size(950, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 22)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 528)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(950, 22)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 528)
        '
        'barButtonItem2
        '
        Me.barButtonItem2.Caption = "Open"
        Me.barButtonItem2.Id = 2
        Me.barButtonItem2.Name = "barButtonItem2"
        '
        'iNew
        '
        Me.iNew.Caption = "&New"
        Me.iNew.Id = 6
        Me.iNew.Name = "iNew"
        '
        'navBarControl
        '
        Me.navBarControl.ActiveGroup = Me.NavBarGroupActions
        Me.navBarControl.Dock = System.Windows.Forms.DockStyle.Left
        Me.navBarControl.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroupActions})
        Me.navBarControl.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.NavBarItemDeletingPhotos, Me.NavBarItemPoints, Me.NavBarItemSendingEmails, Me.NavBarItemVariousTasks, Me.nbiCheckWaitingProfiles, Me.NavBarItemHandleMemberActions, Me.nbiSendingForCreditsExpiring, Me.nbrHappyBirthday, Me.nvbBlockedIps, Me.nvbOnlines})
        Me.navBarControl.LargeImages = Me.navbarImageListLarge
        Me.navBarControl.Location = New System.Drawing.Point(0, 22)
        Me.navBarControl.Name = "navBarControl"
        Me.navBarControl.OptionsNavPane.ExpandedWidth = 210
        Me.navBarControl.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane
        Me.navBarControl.Size = New System.Drawing.Size(210, 528)
        Me.navBarControl.SmallImages = Me.navbarImageList
        Me.navBarControl.StoreDefaultPaintStyleName = True
        Me.navBarControl.TabIndex = 2
        Me.navBarControl.Text = "navBarControl1"
        '
        'NavBarGroupActions
        '
        Me.NavBarGroupActions.Caption = "Actions"
        Me.NavBarGroupActions.Expanded = True
        Me.NavBarGroupActions.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemDeletingPhotos), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemPoints), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemSendingEmails), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiSendingForCreditsExpiring), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemVariousTasks), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbiCheckWaitingProfiles), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemHandleMemberActions), New DevExpress.XtraNavBar.NavBarItemLink(Me.nbrHappyBirthday), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvbBlockedIps), New DevExpress.XtraNavBar.NavBarItemLink(Me.nvbOnlines)})
        Me.NavBarGroupActions.Name = "NavBarGroupActions"
        '
        'NavBarItemDeletingPhotos
        '
        Me.NavBarItemDeletingPhotos.Caption = "Deleting Photos"
        Me.NavBarItemDeletingPhotos.Name = "NavBarItemDeletingPhotos"
        '
        'NavBarItemPoints
        '
        Me.NavBarItemPoints.Caption = "Points"
        Me.NavBarItemPoints.Name = "NavBarItemPoints"
        '
        'NavBarItemSendingEmails
        '
        Me.NavBarItemSendingEmails.Caption = "Sending Emails"
        Me.NavBarItemSendingEmails.Name = "NavBarItemSendingEmails"
        '
        'nbiSendingForCreditsExpiring
        '
        Me.nbiSendingForCreditsExpiring.Caption = "Sending Email - Expiring Credits"
        Me.nbiSendingForCreditsExpiring.Name = "nbiSendingForCreditsExpiring"
        '
        'NavBarItemVariousTasks
        '
        Me.NavBarItemVariousTasks.Caption = "Various Tasks"
        Me.NavBarItemVariousTasks.Name = "NavBarItemVariousTasks"
        '
        'nbiCheckWaitingProfiles
        '
        Me.nbiCheckWaitingProfiles.Caption = "Check Waiting Profiles"
        Me.nbiCheckWaitingProfiles.Name = "nbiCheckWaitingProfiles"
        '
        'NavBarItemHandleMemberActions
        '
        Me.NavBarItemHandleMemberActions.Caption = "Handle Member Actions"
        Me.NavBarItemHandleMemberActions.Name = "NavBarItemHandleMemberActions"
        '
        'nbrHappyBirthday
        '
        Me.nbrHappyBirthday.Caption = "Sending Happy Birthday"
        Me.nbrHappyBirthday.Name = "nbrHappyBirthday"
        '
        'nvbBlockedIps
        '
        Me.nvbBlockedIps.Caption = "Suspicious IPs"
        Me.nvbBlockedIps.Name = "nvbBlockedIps"
        '
        'xtraTabbedMdiManager1
        '
        Me.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageAndTabControlHeader
        Me.xtraTabbedMdiManager1.MdiParent = Me
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(210, 22)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(3, 528)
        Me.Splitter1.TabIndex = 6
        Me.Splitter1.TabStop = False
        '
        'nvbOnlines
        '
        Me.nvbOnlines.Caption = "OnlineFemales"
        Me.nvbOnlines.Name = "nvbOnlines"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(950, 550)
        Me.Controls.Add(Me.Splitter1)
        Me.Controls.Add(Me.navBarControl)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "frmMain"
        Me.Text = "frmMain"
        CType(Me.barManager, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.navBarControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents barManager As DevExpress.XtraBars.BarManager
    Private WithEvents bar2 As DevExpress.XtraBars.Bar
    Private WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Private WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Private WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Private WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Private WithEvents mFile As DevExpress.XtraBars.BarSubItem
    Private WithEvents barButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iClose As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iNew As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iExit As DevExpress.XtraBars.BarButtonItem
    Private WithEvents mHelp As DevExpress.XtraBars.BarSubItem
    Private WithEvents iAbout As DevExpress.XtraBars.BarButtonItem
    Private WithEvents navbarImageList As System.Windows.Forms.ImageList
    Private WithEvents navbarImageListLarge As System.Windows.Forms.ImageList
    Private WithEvents navBarControl As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroupActions As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarItemDeletingPhotos As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItemPoints As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents xtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents NavBarItemSendingEmails As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents NavBarItemVariousTasks As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiCheckWaitingProfiles As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItemHandleMemberActions As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbiSendingForCreditsExpiring As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents nbrHappyBirthday As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvbBlockedIps As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nvbOnlines As DevExpress.XtraNavBar.NavBarItem

End Class

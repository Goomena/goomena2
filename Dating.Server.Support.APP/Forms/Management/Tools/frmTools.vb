﻿Public Class frmTools 

    Private Sub btnCheck_Click(sender As System.Object, e As System.EventArgs) Handles btnCheck.Click

        If (txtStringToSearch.Text.Trim() <> "") Then

        End If
    End Sub

    Private Sub btnReplace_Click(sender As System.Object, e As System.EventArgs) Handles btnReplace.Click

    End Sub

    Private Sub btnGEOGRReplaceS_Click(sender As System.Object, e As System.EventArgs) Handles btnGEOGRReplaceS.Click

        Try
            ShowWaitWin("Working please wait...")

            Dim dsgeo As New AdminWS.DSGEO()
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetSYS_GEO_GR")
            CheckWebServiceCallResult(gAdminWS.GetSYS_GEO_GR(gAdminWSHeaders, dsgeo))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetSYS_GEO_GR")

            Dim cnt As Integer = 0
            For cnt = 0 To dsgeo.SYS_GEO_GR.Rows.Count - 1
                Dim strCnt As Integer

                Dim row As AdminWS.DSGEO.SYS_GEO_GRRow = dsgeo.SYS_GEO_GR.Rows(cnt)
                If (row.language <> "EL") Then
                    Continue For
                End If


                ' fix city name
                Dim cityStrs() As String = row.city.Split(" "c)

                For strCnt = 0 To cityStrs.Length - 1
                    Dim str As String = cityStrs(strCnt)
                    If (str.EndsWith("σ")) Then
                        str = str.Remove(str.Length - 1)
                        str = str & "ς"
                        cityStrs(strCnt) = str
                    End If
                Next
                Dim newCity As String = String.Join(" ", cityStrs)
                If (newCity <> row.city) Then
                    txtOut.Text = "city: " & newCity & vbCrLf & txtOut.Text
                    row.city = newCity
                End If


                ' fix region1 name
                Dim region1Strs() As String = row.region1.Split(" "c)

                For strCnt = 0 To region1Strs.Length - 1
                    Dim str As String = region1Strs(strCnt)
                    If (str.EndsWith("σ")) Then
                        str = str.Remove(str.Length - 1)
                        str = str & "ς"
                        region1Strs(strCnt) = str
                    End If
                Next

                Dim newRegion1 As String = String.Join(" ", region1Strs)
                If (newRegion1 <> row.region1) Then
                    txtOut.Text = "region1: " & newRegion1 & vbCrLf & txtOut.Text
                    row.region1 = newRegion1
                End If




                ' fix region2 name
                Dim region2Strs() As String = row.region2.Split(" "c)

                For strCnt = 0 To region2Strs.Length - 1
                    Dim str As String = region2Strs(strCnt)
                    If (str.EndsWith("σ")) Then
                        str = str.Remove(str.Length - 1)
                        str = str & "ς"
                        region2Strs(strCnt) = str
                    End If
                Next

                Dim newRegion2 As String = String.Join(" ", region2Strs)
                If (newRegion2 <> row.region2) Then
                    txtOut.Text = "region2: " & newRegion2 & vbCrLf & txtOut.Text
                    row.region2 = newRegion2
                End If


                Application.DoEvents()
            Next


            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateSYS_GEO_GR")
            CheckWebServiceCallResult(gAdminWS.UpdateSYS_GEO_GR(gAdminWSHeaders, dsgeo))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateSYS_GEO_GR")

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        Finally
            HideWaitWin()
        End Try
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTools
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.gcReplaceDbString = New DevExpress.XtraEditors.GroupControl()
        Me.lblRowsAffected = New DevExpress.XtraEditors.LabelControl()
        Me.btnCheck = New DevExpress.XtraEditors.SimpleButton()
        Me.btnReplace = New DevExpress.XtraEditors.SimpleButton()
        Me.txtNewString = New DevExpress.XtraEditors.TextEdit()
        Me.txtStringToSearch = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.UniCMSDB = New Dating.Client.Admin.APP.AdminWS.UniCMSDB()
        Me.SYS_SitePagesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SYS_MessagesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.gcGEOGRReplaceS = New DevExpress.XtraEditors.GroupControl()
        Me.txtOut = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.btnGEOGRReplaceS = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.gcReplaceDbString, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcReplaceDbString.SuspendLayout()
        CType(Me.txtNewString.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStringToSearch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UniCMSDB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SYS_SitePagesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SYS_MessagesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcGEOGRReplaceS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcGEOGRReplaceS.SuspendLayout()
        CType(Me.txtOut.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcReplaceDbString
        '
        Me.gcReplaceDbString.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gcReplaceDbString.Controls.Add(Me.lblRowsAffected)
        Me.gcReplaceDbString.Controls.Add(Me.btnCheck)
        Me.gcReplaceDbString.Controls.Add(Me.btnReplace)
        Me.gcReplaceDbString.Controls.Add(Me.txtNewString)
        Me.gcReplaceDbString.Controls.Add(Me.txtStringToSearch)
        Me.gcReplaceDbString.Controls.Add(Me.LabelControl2)
        Me.gcReplaceDbString.Controls.Add(Me.LabelControl1)
        Me.gcReplaceDbString.Enabled = False
        Me.gcReplaceDbString.Location = New System.Drawing.Point(12, 12)
        Me.gcReplaceDbString.Name = "gcReplaceDbString"
        Me.gcReplaceDbString.Size = New System.Drawing.Size(568, 138)
        Me.gcReplaceDbString.TabIndex = 0
        Me.gcReplaceDbString.Text = "Replace DBString"
        '
        'lblRowsAffected
        '
        Me.lblRowsAffected.Location = New System.Drawing.Point(19, 104)
        Me.lblRowsAffected.Name = "lblRowsAffected"
        Me.lblRowsAffected.Size = New System.Drawing.Size(86, 13)
        Me.lblRowsAffected.TabIndex = 6
        Me.lblRowsAffected.Text = "[lblRowsAffected]"
        '
        'btnCheck
        '
        Me.btnCheck.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCheck.Location = New System.Drawing.Point(403, 104)
        Me.btnCheck.Name = "btnCheck"
        Me.btnCheck.Size = New System.Drawing.Size(75, 23)
        Me.btnCheck.TabIndex = 5
        Me.btnCheck.Text = "Check"
        '
        'btnReplace
        '
        Me.btnReplace.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReplace.Location = New System.Drawing.Point(484, 104)
        Me.btnReplace.Name = "btnReplace"
        Me.btnReplace.Size = New System.Drawing.Size(75, 23)
        Me.btnReplace.TabIndex = 4
        Me.btnReplace.Text = "Replace"
        '
        'txtNewString
        '
        Me.txtNewString.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNewString.Location = New System.Drawing.Point(91, 67)
        Me.txtNewString.Name = "txtNewString"
        Me.txtNewString.Size = New System.Drawing.Size(468, 20)
        Me.txtNewString.TabIndex = 3
        '
        'txtStringToSearch
        '
        Me.txtStringToSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStringToSearch.Location = New System.Drawing.Point(91, 37)
        Me.txtStringToSearch.Name = "txtStringToSearch"
        Me.txtStringToSearch.Size = New System.Drawing.Size(468, 20)
        Me.txtStringToSearch.TabIndex = 2
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(30, 70)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(55, 13)
        Me.LabelControl2.TabIndex = 1
        Me.LabelControl2.Text = "New string:"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(5, 40)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(80, 13)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "String to search:"
        '
        'UniCMSDB
        '
        Me.UniCMSDB.DataSetName = "UniCMSDB"
        Me.UniCMSDB.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SYS_SitePagesBindingSource
        '
        Me.SYS_SitePagesBindingSource.DataMember = "SYS_SitePages"
        Me.SYS_SitePagesBindingSource.DataSource = Me.UniCMSDB
        '
        'SYS_MessagesBindingSource
        '
        Me.SYS_MessagesBindingSource.DataMember = "SYS_Messages"
        Me.SYS_MessagesBindingSource.DataSource = Me.UniCMSDB
        '
        'gcGEOGRReplaceS
        '
        Me.gcGEOGRReplaceS.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gcGEOGRReplaceS.Controls.Add(Me.txtOut)
        Me.gcGEOGRReplaceS.Controls.Add(Me.LabelControl3)
        Me.gcGEOGRReplaceS.Controls.Add(Me.btnGEOGRReplaceS)
        Me.gcGEOGRReplaceS.Location = New System.Drawing.Point(12, 185)
        Me.gcGEOGRReplaceS.Name = "gcGEOGRReplaceS"
        Me.gcGEOGRReplaceS.Size = New System.Drawing.Size(568, 187)
        Me.gcGEOGRReplaceS.TabIndex = 1
        Me.gcGEOGRReplaceS.Text = "GEO GR Replace s"
        '
        'txtOut
        '
        Me.txtOut.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOut.Location = New System.Drawing.Point(6, 26)
        Me.txtOut.Name = "txtOut"
        Me.txtOut.Size = New System.Drawing.Size(553, 121)
        Me.txtOut.TabIndex = 7
        '
        'LabelControl3
        '
        Me.LabelControl3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl3.Location = New System.Drawing.Point(19, 157)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(86, 13)
        Me.LabelControl3.TabIndex = 6
        Me.LabelControl3.Text = "[lblRowsAffected]"
        Me.LabelControl3.Visible = False
        '
        'btnGEOGRReplaceS
        '
        Me.btnGEOGRReplaceS.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnGEOGRReplaceS.Location = New System.Drawing.Point(424, 157)
        Me.btnGEOGRReplaceS.Name = "btnGEOGRReplaceS"
        Me.btnGEOGRReplaceS.Size = New System.Drawing.Size(135, 23)
        Me.btnGEOGRReplaceS.TabIndex = 4
        Me.btnGEOGRReplaceS.Text = "Replace Greek final s"
        '
        'frmTools
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(592, 394)
        Me.Controls.Add(Me.gcGEOGRReplaceS)
        Me.Controls.Add(Me.gcReplaceDbString)
        Me.Name = "frmTools"
        Me.Text = "frmTools"
        CType(Me.gcReplaceDbString, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcReplaceDbString.ResumeLayout(False)
        Me.gcReplaceDbString.PerformLayout()
        CType(Me.txtNewString.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStringToSearch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UniCMSDB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SYS_SitePagesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SYS_MessagesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcGEOGRReplaceS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcGEOGRReplaceS.ResumeLayout(False)
        Me.gcGEOGRReplaceS.PerformLayout()
        CType(Me.txtOut.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gcReplaceDbString As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtStringToSearch As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNewString As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblRowsAffected As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnCheck As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnReplace As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents UniCMSDB As Dating.Client.Admin.APP.AdminWS.UniCMSDB
    Friend WithEvents SYS_SitePagesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SYS_MessagesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents gcGEOGRReplaceS As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtOut As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnGEOGRReplaceS As DevExpress.XtraEditors.SimpleButton
End Class

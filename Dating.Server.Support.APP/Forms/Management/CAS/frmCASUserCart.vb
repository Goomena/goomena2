﻿Public Class frmCASUserCart

    Dim UL As New clsUserLog
    Public IsSaved As Boolean
    Public TerminalID As Integer = 0

    Private Sub Save()
        Try
            Me.Validate()
            '   Me.TerminalBindingSource.EndEdit()
            '   Me.TableAdapterManager.UpdateAll(Me.LotteryDBDataSet)
            IsSaved = True
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub frmBOTerminalItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            IsSaved = False
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Public Sub ADD()
        Try
            'Me.TerminalTableAdapter.FillByTerminalID(Me.LotteryDBDataSet.Terminal, 0)
            'Me.TerminalBindingSource.AddNew()
            'Me.TerminalBindingSource.Current("IsActive") = True
            'Me.TerminalBindingSource.Current("DTCreate") = Now
            'Me.TerminalBindingSource.Current("TerminalAppSerialNumber") = Library.Public.Common.GetRandom(1000, 9999) & "-" & Library.Public.Common.GetRandom(1000, 9999) & "-" & Library.Public.Common.GetRandom(1000, 9999) & "-" & Library.Public.Common.GetRandom(1000, 9999)
            'Me.TerminalBindingSource.Current("TerminalAppVersionOnActivate") = 0
            'Me.TerminalBindingSource.Current("ActivicationPUK") = Library.Public.Common.GetRandom(1000, 9999) & "-" & Library.Public.Common.GetRandom(1000, 9999) & "-" & Library.Public.Common.GetRandom(1000, 9999) & "-" & Library.Public.Common.GetRandom(1000, 9999)
            'Me.TerminalBindingSource.Current("MaxActivications") = 15
            'Me.TerminalBindingSource.Current("ActivationsCount") = 0
            'Me.TerminalBindingSource.Current("LastActivationDateTime") = Now
            'Me.TerminalBindingSource.Current("TerminalUniqueTokenID") = ""
            'Me.TerminalBindingSource.Current("TerminalHardwareID") = ""
            'Me.TerminalBindingSource.Current("UseStaticIP") = False
            'Me.TerminalBindingSource.Current("StaticIP") = ""
            'UL.BeginWork(201, "New Entry Terminal", "")

            Me.ShowDialog()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Public Sub EDIT(ByVal TerminalID As Integer)
        Try
            'UL.BeginWork(202, "Edit cart Terminal ID:" & TerminalID, "")
            'Me.TerminalTableAdapter.FillByTerminalID(Me.LotteryDBDataSet.Terminal, TerminalID)
            'Me.ShowDialog()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Public Sub DELETE(ByVal TerminalID As Integer)
        Try
            'Me.TerminalTableAdapter.FillByTerminalID(Me.LotteryDBDataSet.Terminal, TerminalID)
            'If Library.Public.SQLFunctions.CheckRelatedRecords("AgencyTransaction", "TerminalID", TerminalID, G.MasterConnNET.m_ConnectionString) Then
            '    MsgBox("Found Transactions on this Agency, Cannot Delete Before deleted Transaction ")
            '    Exit Sub
            'End If
            'UL.BeginWork(201, "Delete cart Terminal ID:" & TerminalID, "")
            'Me.TerminalTableAdapter.FillByTerminalID(Me.LotteryDBDataSet.Terminal, TerminalID)
            'Dim sz As String = "Deleted " & Me.TerminalBindingSource.Current("Title")
            'Me.TerminalBindingSource.RemoveCurrent()
            'Save()
            'UL.EndWork(1, TerminalID, sz, 0, 0, 0)
            'MsgBox(sz)
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UL.EndWork(False, TerminalID, "Terminal Cart", 0, 0, 0)
        Close()
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        'TerminalID = Me.TerminalBindingSource.Current("TerminalID")
        'UL.EndWork(1, TerminalID, "Terminal Cart", 0, 0, 0)
        'Save()
        'Hide()
    End Sub

    Private Sub lbTerminalCart_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles lbTerminalCart.Paint

    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCASCPanelMain
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCASCPanelMain))
        Me.CmdUsers = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdRoles = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdLoginsHistory = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdWorksHistory = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdClose = New DevExpress.XtraEditors.SimpleButton()
        Me.SuspendLayout()
        '
        'CmdUsers
        '
        Me.CmdUsers.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CmdUsers.Appearance.Options.UseFont = True
        Me.CmdUsers.Location = New System.Drawing.Point(12, 12)
        Me.CmdUsers.Name = "CmdUsers"
        Me.CmdUsers.Size = New System.Drawing.Size(397, 45)
        Me.CmdUsers.TabIndex = 1
        Me.CmdUsers.Text = "Users"
        '
        'CmdRoles
        '
        Me.CmdRoles.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CmdRoles.Appearance.Options.UseFont = True
        Me.CmdRoles.Location = New System.Drawing.Point(13, 63)
        Me.CmdRoles.Name = "CmdRoles"
        Me.CmdRoles.Size = New System.Drawing.Size(397, 45)
        Me.CmdRoles.TabIndex = 2
        Me.CmdRoles.Text = "Roles"
        '
        'CmdLoginsHistory
        '
        Me.CmdLoginsHistory.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CmdLoginsHistory.Appearance.Options.UseFont = True
        Me.CmdLoginsHistory.Location = New System.Drawing.Point(13, 114)
        Me.CmdLoginsHistory.Name = "CmdLoginsHistory"
        Me.CmdLoginsHistory.Size = New System.Drawing.Size(397, 45)
        Me.CmdLoginsHistory.TabIndex = 3
        Me.CmdLoginsHistory.Text = "Logins History"
        '
        'CmdWorksHistory
        '
        Me.CmdWorksHistory.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CmdWorksHistory.Appearance.Options.UseFont = True
        Me.CmdWorksHistory.Location = New System.Drawing.Point(13, 165)
        Me.CmdWorksHistory.Name = "CmdWorksHistory"
        Me.CmdWorksHistory.Size = New System.Drawing.Size(397, 45)
        Me.CmdWorksHistory.TabIndex = 4
        Me.CmdWorksHistory.Text = "Works History"
        '
        'CmdClose
        '
        Me.CmdClose.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.CmdClose.Appearance.Options.UseFont = True
        Me.CmdClose.Location = New System.Drawing.Point(13, 216)
        Me.CmdClose.Name = "CmdClose"
        Me.CmdClose.Size = New System.Drawing.Size(397, 45)
        Me.CmdClose.TabIndex = 5
        Me.CmdClose.Text = "Close"
        '
        'frmCASCPanelMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(422, 275)
        Me.Controls.Add(Me.CmdClose)
        Me.Controls.Add(Me.CmdWorksHistory)
        Me.Controls.Add(Me.CmdLoginsHistory)
        Me.Controls.Add(Me.CmdRoles)
        Me.Controls.Add(Me.CmdUsers)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCASCPanelMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmUsersOfThisCPanelMain"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CmdUsers As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdRoles As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdLoginsHistory As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdWorksHistory As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdClose As DevExpress.XtraEditors.SimpleButton
End Class

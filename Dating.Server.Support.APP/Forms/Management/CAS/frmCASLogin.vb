Imports Dating.Server.Core.DLL

Public Class frmCASLogin
    Inherits DevExpress.XtraEditors.XtraForm
    Public LoginOK As Boolean = False
    Dim CloseForm As Boolean

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        'Add any initialization after the InitializeComponent() call

    End Sub

    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            If (Not System.IO.File.Exists("Config.xml")) Then
                System.IO.File.AppendAllText("Config.xml", "<?xml version=""1.0"" standalone=""yes""?>" & vbCrLf & "<LocalConfigData xmlns=""http://tempuri.org/LocalConfigData.xsd""></LocalConfigData>")
            End If

            LocalConfigData.ReadXml("Config.xml")
            Dim i As Integer
            For i = 0 To SitesBindingSource.Count - 1
                Dim SiteName As String = SitesBindingSource.Current("SiteName")
                If Len(SiteName) Then
                    cbSiteName.Properties.Items.Add(SiteName)
                End If
                SitesBindingSource.MoveNext()
            Next
            SitesBindingSource.MoveFirst()
            If ConfigBindingSource.Count = 0 Then
                ConfigBindingSource.AddNew()
                ConfigBindingSource.EndEdit()

            End If
            cbSiteName.EditValue = ConfigBindingSource.Current("LastSiteName")

            Library.Public.Common.CenterScreen(Me)
            LoginOK = False
            cbSiteName.Focus()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdCancel.Click
        LoginOK = False
        CloseForm = True
        Close()
    End Sub


    Private Sub frmLogin_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        '  LoginOK = False
        If Not CloseForm Then
            e.Cancel = True
        End If
    End Sub

    Private Sub CmdOK_Click(sender As System.Object, e As System.EventArgs) Handles CmdOK.Click
        Try
            LoginOK = False



            'Dim ws As String = cbSiteName.Text
            'ws = ws.TrimEnd("/"c, "\"c)

            'If (Not ws.Contains("localhost")) Then
            '    If (Not ws.Contains("/adminws")) Then
            '        ws = ws & "/adminws"
            '    End If
            'End If

            'If (Not ws.Contains("/admin.asmx")) Then
            '    ws = ws & "/admin.asmx"
            'End If

            'If (Not ws.StartsWith("http://")) Then
            '    ws = "http://" & ws
            'End If

            'gAdminWS.Url = ws
            '   gWorkingSiteName = cbSiteName.Text.Replace("http://", String.Empty).Replace("https://", String.Empty).Replace("/", String.Empty).Replace(":", String.Empty)

            Globals.PerformLogin(txLoginName.Text, txPassword.Text)
            If gLoginRec.HasErrors Or Not gLoginRec.IsValid Then
                GoTo GoClose
            End If

            LoginOK = True
            '   m_UserRole = "Manager"

            If txLoginName.Text.ToLower = "tools" And txPassword.Text.ToLower = "tools321" Then
                LoginOK = True
                ' m_UserRole = "Admin"
            End If

            GoTo GoClose

            'If txLoginName.Text.ToLower = "qqq" Then
            '    Dim Reg As String = My.Computer.Registry.GetValue(RegBase, "IsDev", "none")
            '    If Reg.ToLower = "jim" Then
            '        LoginOK = True
            '        m_UserRole = "Admin"
            '    Else
            '        Dim pass As String = InputBox("Give Dev Pass", "Developer Password")
            '        If pass = "Jim1968" Then
            '            My.Computer.Registry.SetValue(RegBase, "IsDev", "jim")

            '        End If

            '    End If
            'End If

            'If txLoginName.Text.ToLower = "jim" And txPassword.Text.ToLower = "1968" Then
            '    LoginOK = True
            '    m_UserRole = "Admin"
            'End If
            'If txLoginName.Text.ToLower = "sofi" And txPassword.Text.ToLower = "barby" Then
            '    LoginOK = True
            '    m_UserRole = "Admin"
            'End If

            'If txLoginName.Text.ToLower = "savas" And txPassword.Text.ToLower = "65000" Then
            '    LoginOK = True
            '    m_UserRole = "savas"
            'End If
            'If txLoginName.Text.ToLower = "panos" And txPassword.Text.ToLower = "panos2011" Then
            '    LoginOK = True
            '    m_UserRole = "panos"
            'End If
            'If txLoginName.Text.ToLower = "cms" And txPassword.Text.ToLower = "cms2011" Then
            '    LoginOK = True
            '    m_UserRole = "cms"
            'End If


            '            If Len(cbSiteName.EditValue) Then
            '                Dim o = ConfigBindingSource.Current("LastSiteName")

            '                ConfigBindingSource.Current("LastSiteName") = cbSiteName.EditValue
            '                If ConfigBindingSource.Count = 0 Then
            '                    ConfigBindingSource.AddNew()
            '                    ConfigBindingSource.EndEdit()
            '                    SaveConfig()
            '                End If
            '            End If


GoClose:
            '            '!!gWorkingSiteName = cbSiteName.Text
            '            gWorkingSiteName = cbSiteName.Text.Replace("http://", String.Empty).Replace("https://", String.Empty).Replace("/", String.Empty).Replace(":", String.Empty)
            If LoginOK Then
                CloseForm = True
                Hide()
            End If

            txLoginName.Focus()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try

    End Sub


    Private Sub CmdSaveSite_Click(sender As System.Object, e As System.EventArgs) Handles CmdSaveSite.Click
        Try
            If Len(cbSiteName.EditValue) Then
                Try
                    SitesBindingSource.AddNew()
                    SitesBindingSource.Current("SiteName") = cbSiteName.EditValue
                    SitesBindingSource.EndEdit()
                    LocalConfigData.WriteXml("Config.xml")
                    cbSiteName.Properties.Items.Add(cbSiteName.EditValue)
                Catch ex As Exception
                    SitesBindingSource.Remove(SitesBindingSource.Current)
                End Try
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdSaveLogin_Click(sender As System.Object, e As System.EventArgs) Handles CmdSaveLogin.Click
        Try
            If Len(txLoginName.EditValue) Then
                If ConfigBindingSource.Count = 0 Then
                    ConfigBindingSource.AddNew()
                End If
                ConfigBindingSource.Current("SavedLoginName") = txLoginName.EditValue
                ConfigBindingSource.EndEdit()
                LocalConfigData.WriteXml("Config.xml")
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdSavePass_Click(sender As System.Object, e As System.EventArgs) Handles CmdSavePass.Click
        Try
            If Len(txPassword.EditValue) Then
                If ConfigBindingSource.Count = 0 Then
                    ConfigBindingSource.AddNew()
                End If
                ConfigBindingSource.Current("SavedPassWord") = txPassword.EditValue
                ConfigBindingSource.EndEdit()
                LocalConfigData.WriteXml("Config.xml")
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub
End Class


'        Try
'            LoginOK = False

'            If txLoginName.Text.ToLower = "qqq" Then
'Dim Reg As String = My.Computer.Registry.GetValue(RegBase, "IsDev", "none")
'                If Reg.ToLower = "jim" Then
'                    LoginOK = True
'                    m_UserRole = "Admin"
'                Else
'Dim pass As String = InputBox("Give Dev Pass", "Developer Password")
'                    If pass = "Jim1968" Then
'                        My.Computer.Registry.SetValue(RegBase, "IsDev", "jim")

'                    End If

'                End If
'            End If

'            If txLoginName.Text.ToLower = "jim" And txPassword.Text.ToLower = "1968" Then
'                LoginOK = True
'                m_UserRole = "Admin"
'            End If
'            If txLoginName.Text.ToLower = "sofi" And txPassword.Text.ToLower = "barby" Then
'                LoginOK = True
'                m_UserRole = "Admin"
'            End If

'            If txLoginName.Text.ToLower = "savas" And txPassword.Text.ToLower = "65000" Then
'                LoginOK = True
'                m_UserRole = "savas"
'            End If
'            If txLoginName.Text.ToLower = "panos" And txPassword.Text.ToLower = "panos2011" Then
'                LoginOK = True
'                m_UserRole = "panos"
'            End If
'            If txLoginName.Text.ToLower = "cms" And txPassword.Text.ToLower = "cms2011" Then
'                LoginOK = True
'                m_UserRole = "cms"
'            End If


'            If LoginOK Then
'                CloseForm = True
'                Hide()
'            End If

'            txLoginName.Focus()
'        Catch ex As Exception
'            ErrorMsgBox(ex, "")
'        End Try


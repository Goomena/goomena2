<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCASLogin
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCASLogin))
        Me.GroupControlSwitchUser = New DevExpress.XtraEditors.GroupControl()
        Me.CmdOK = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.txPassword = New DevExpress.XtraEditors.TextEdit()
        Me.ConfigBindingSource = New System.Windows.Forms.BindingSource()
        Me.LocalConfigData = New Dating.Server.Support.APP.LocalConfigData()
        Me.txLoginName = New DevExpress.XtraEditors.TextEdit()
        Me.lbPassword = New DevExpress.XtraEditors.LabelControl()
        Me.lbLoginName = New DevExpress.XtraEditors.LabelControl()
        Me.CmdSavePass = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdSaveLogin = New DevExpress.XtraEditors.SimpleButton()
        Me.SitesBindingSource = New System.Windows.Forms.BindingSource()
        Me.CmdSaveSite = New DevExpress.XtraEditors.SimpleButton()
        Me.cbSiteName = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.lblWebSite = New DevExpress.XtraEditors.LabelControl()
        CType(Me.GroupControlSwitchUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConfigBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LocalConfigData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txLoginName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SitesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbSiteName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControlSwitchUser
        '
        Me.GroupControlSwitchUser.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.GroupControlSwitchUser.AppearanceCaption.Options.UseFont = True
        Me.GroupControlSwitchUser.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControlSwitchUser.Location = New System.Drawing.Point(0, 0)
        Me.GroupControlSwitchUser.Name = "GroupControlSwitchUser"
        Me.GroupControlSwitchUser.Size = New System.Drawing.Size(587, 36)
        Me.GroupControlSwitchUser.TabIndex = 15
        Me.GroupControlSwitchUser.Text = " Login User"
        '
        'CmdOK
        '
        Me.CmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdOK.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.CmdOK.Appearance.Options.UseFont = True
        Me.CmdOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.CmdOK.Location = New System.Drawing.Point(323, 100)
        Me.CmdOK.Name = "CmdOK"
        Me.CmdOK.Size = New System.Drawing.Size(123, 29)
        Me.CmdOK.TabIndex = 203
        Me.CmdOK.Text = "OK"
        '
        'CmdCancel
        '
        Me.CmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.CmdCancel.Appearance.Options.UseFont = True
        Me.CmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.CmdCancel.Location = New System.Drawing.Point(452, 100)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.Size = New System.Drawing.Size(123, 29)
        Me.CmdCancel.TabIndex = 3
        Me.CmdCancel.Text = "Cancel"
        '
        'txPassword
        '
        Me.txPassword.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txPassword.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.ConfigBindingSource, "SavedPassWord", True))
        Me.txPassword.Location = New System.Drawing.Point(132, 68)
        Me.txPassword.Name = "txPassword"
        Me.txPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txPassword.Size = New System.Drawing.Size(314, 20)
        Me.txPassword.TabIndex = 202
        '
        'ConfigBindingSource
        '
        Me.ConfigBindingSource.DataMember = "Config"
        Me.ConfigBindingSource.DataSource = Me.LocalConfigData
        '
        'LocalConfigData
        '
        Me.LocalConfigData.DataSetName = "LocalConfigData"
        Me.LocalConfigData.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'txLoginName
        '
        Me.txLoginName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txLoginName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.ConfigBindingSource, "SavedLoginName", True))
        Me.txLoginName.Location = New System.Drawing.Point(132, 42)
        Me.txLoginName.Name = "txLoginName"
        Me.txLoginName.Size = New System.Drawing.Size(314, 20)
        Me.txLoginName.TabIndex = 201
        '
        'lbPassword
        '
        Me.lbPassword.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbPassword.Location = New System.Drawing.Point(58, 70)
        Me.lbPassword.Name = "lbPassword"
        Me.lbPassword.Size = New System.Drawing.Size(60, 13)
        Me.lbPassword.TabIndex = 197
        Me.lbPassword.Text = "Password :"
        '
        'lbLoginName
        '
        Me.lbLoginName.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbLoginName.Location = New System.Drawing.Point(47, 44)
        Me.lbLoginName.Name = "lbLoginName"
        Me.lbLoginName.Size = New System.Drawing.Size(71, 13)
        Me.lbLoginName.TabIndex = 196
        Me.lbLoginName.Text = "Login name :"
        '
        'CmdSavePass
        '
        Me.CmdSavePass.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdSavePass.Location = New System.Drawing.Point(452, 67)
        Me.CmdSavePass.Name = "CmdSavePass"
        Me.CmdSavePass.Size = New System.Drawing.Size(123, 20)
        Me.CmdSavePass.TabIndex = 203
        Me.CmdSavePass.Text = "Save Pass"
        '
        'CmdSaveLogin
        '
        Me.CmdSaveLogin.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdSaveLogin.Location = New System.Drawing.Point(452, 41)
        Me.CmdSaveLogin.Name = "CmdSaveLogin"
        Me.CmdSaveLogin.Size = New System.Drawing.Size(123, 20)
        Me.CmdSaveLogin.TabIndex = 202
        Me.CmdSaveLogin.Text = "Save Login"
        '
        'SitesBindingSource
        '
        Me.SitesBindingSource.DataMember = "Sites"
        Me.SitesBindingSource.DataSource = Me.LocalConfigData
        '
        'CmdSaveSite
        '
        Me.CmdSaveSite.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdSaveSite.Location = New System.Drawing.Point(138, 106)
        Me.CmdSaveSite.Name = "CmdSaveSite"
        Me.CmdSaveSite.Size = New System.Drawing.Size(123, 20)
        Me.CmdSaveSite.TabIndex = 201
        Me.CmdSaveSite.Text = "Save Site"
        Me.CmdSaveSite.Visible = False
        '
        'cbSiteName
        '
        Me.cbSiteName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbSiteName.Location = New System.Drawing.Point(73, 106)
        Me.cbSiteName.Name = "cbSiteName"
        Me.cbSiteName.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbSiteName.Size = New System.Drawing.Size(59, 20)
        Me.cbSiteName.TabIndex = 200
        Me.cbSiteName.Visible = False
        '
        'lblWebSite
        '
        Me.lblWebSite.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblWebSite.Location = New System.Drawing.Point(12, 109)
        Me.lblWebSite.Name = "lblWebSite"
        Me.lblWebSite.Size = New System.Drawing.Size(55, 13)
        Me.lblWebSite.TabIndex = 199
        Me.lblWebSite.Text = "Web site :"
        Me.lblWebSite.Visible = False
        '
        'frmCASLogin
        '
        Me.AcceptButton = Me.CmdOK
        Me.CancelButton = Me.CmdCancel
        Me.ClientSize = New System.Drawing.Size(587, 141)
        Me.Controls.Add(Me.CmdSavePass)
        Me.Controls.Add(Me.CmdSaveLogin)
        Me.Controls.Add(Me.CmdSaveSite)
        Me.Controls.Add(Me.cbSiteName)
        Me.Controls.Add(Me.lblWebSite)
        Me.Controls.Add(Me.txPassword)
        Me.Controls.Add(Me.txLoginName)
        Me.Controls.Add(Me.lbPassword)
        Me.Controls.Add(Me.lbLoginName)
        Me.Controls.Add(Me.CmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.GroupControlSwitchUser)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCASLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login User"
        CType(Me.GroupControlSwitchUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConfigBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LocalConfigData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txLoginName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SitesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbSiteName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupControlSwitchUser As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CmdOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txLoginName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbPassword As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLoginName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CmdSavePass As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdSaveLogin As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LocalConfigData As Dating.Server.Support.APP.LocalConfigData
    Friend WithEvents ConfigBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SitesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CmdSaveSite As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbSiteName As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents lblWebSite As DevExpress.XtraEditors.LabelControl
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPhotos
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gcApprovalSettings = New DevExpress.XtraEditors.GroupControl()
        Me.ucLayout3D1 = New Dating.Client.Admin.APP.ucLayout3D()
        CType(Me.gcApprovalSettings, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcApprovalSettings.SuspendLayout()
        Me.SuspendLayout()
        '
        'gcApprovalSettings
        '
        Me.gcApprovalSettings.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold)
        Me.gcApprovalSettings.AppearanceCaption.Options.UseFont = True
        Me.gcApprovalSettings.Controls.Add(Me.ucLayout3D1)
        Me.gcApprovalSettings.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcApprovalSettings.Location = New System.Drawing.Point(0, 0)
        Me.gcApprovalSettings.Name = "gcApprovalSettings"
        Me.gcApprovalSettings.Size = New System.Drawing.Size(977, 575)
        Me.gcApprovalSettings.TabIndex = 6
        Me.gcApprovalSettings.Text = "Approval Settings"
        '
        'ucLayout3D1
        '
        Me.ucLayout3D1.DefaultView = Dating.Server.Core.DLL.PhotosFilterEnum.None
        Me.ucLayout3D1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ucLayout3D1.Location = New System.Drawing.Point(2, 33)
        Me.ucLayout3D1.Name = "ucLayout3D1"
        Me.ucLayout3D1.Size = New System.Drawing.Size(973, 540)
        Me.ucLayout3D1.TabIndex = 0
        '
        'frmPhotos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(977, 575)
        Me.Controls.Add(Me.gcApprovalSettings)
        Me.Name = "frmPhotos"
        Me.Text = "Photos"
        CType(Me.gcApprovalSettings, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcApprovalSettings.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gcApprovalSettings As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ucLayout3D1 As Dating.Client.Admin.APP.ucLayout3D
End Class

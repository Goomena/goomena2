﻿Public Class frmMessages

    Public Property ShowSendingReceiving As Boolean
        Get
            Return UcMessages1.ShowSendingReceiving

        End Get
        Set(value As Boolean)
            UcMessages1.ShowSendingReceiving = value
        End Set
    End Property


    Public Property ProfileID As Integer
        Get
            Return UcMessages1.ProfileID

        End Get
        Set(value As Integer)
            UcMessages1.ProfileID = value
        End Set
    End Property

    Private Sub frmMessages_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            UcMessages1.Init()
            UcMessages1.RefreshGrid()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMessages->Load")
        End Try
    End Sub

    Public Sub RefreshGrid()
        UcMessages1.RefreshGrid()
    End Sub

End Class
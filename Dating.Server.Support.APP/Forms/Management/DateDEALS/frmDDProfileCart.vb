﻿Imports DevExpress.XtraEditors.Controls
Imports Dating.Client.Admin.APP.AdminWS.DSMembers
Imports System.Net.Mail
Imports Dating.Server.Core.DLL

Public Class frmDDProfileCart

    Const __TESTING__ = False
    Const __TESTING__PROFILEID = 69

    'Dim UL As New clsUserLog
    Public IsSaved As Boolean
    Public ProfileID As Integer

    Private FormAction As FormActionEnum
    Private MasterProfileID As Integer
    Private MirrorProfileID As Integer
    Private MasterRowIndex As Integer
    Private MirrorRowIndex As Integer



    Private Sub frmDDProfileCart_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            '''''''''''''''''''''''''''''''''
            ' REMOVE !!!!!!!!!!!!!!!!
            '''''''''''''''''''''''''''''''''
            If __TESTING__ Then

                Dim DataRecordLogin As New AdminWS.clsDataRecordLogin()
                DataRecordLogin.LoginName = "a"
                DataRecordLogin.Password = "a"

                gAdminWS.Url = "http://localhost:108/admin.asmx"
                gLoginRec = gAdminWS.Login(DataRecordLogin)
                If gLoginRec.HasErrors Then
                    MsgBox(gLoginRec.Message)
                End If
                m_UserRole = "Admin"


                ProfileID = __TESTING__PROFILEID
                FormAction = FormActionEnum.EDIT
            End If

            'AddHandler Me.ucGridPhotos.GridView.KeyDown, AddressOf GridView_KeyDown
            'AddHandler Me.ucGridPhotos.GridView.DoubleClick, AddressOf GridView_DoubleClick
            'AddHandler Me.ucGridPhotos.GridView.RowUpdated, AddressOf GridView_RowUpdated

            IsSaved = False
            Me._LoadProfileData()
            'Me._LoadPhotosData()


            ' remove edit, add & delete buttons
            Me.ucGridTransactions.Bar1.Manager.Items.RemoveAt(1)
            Me.ucGridTransactions.Bar1.Manager.Items.RemoveAt(0)
            Me.ucGridTransactions.Bar1.Manager.Items.RemoveAt(0)

            ucLayout3D1.Init()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub



#Region "Profile"


    Public Sub ADD()
        Try
            Me.FormAction = FormActionEnum.ADD
            Me.ProfileID = 0
            Me.ShowDialog()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Public Sub EDIT(ByVal ProfileID As Integer)
        Try
            Me.FormAction = FormActionEnum.EDIT
            Me.ProfileID = ProfileID
            Me.ShowDialog()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Public Sub DELETE(ByVal TerminalID As Integer, Permanently As Boolean)
        Try
            Me.FormAction = FormActionEnum.DELETE
            Me.ProfileID = TerminalID

            'Me.EUS_ProfilesTableAdapter.FillByProfileID(Me.DateDealsDataSet.EUS_Profiles, ProfileID)
            'If Library.Public.SQLFunctions.CheckRelatedRecords("AgencyTransaction", "ProfileID", ProfileID, G.MasterConnNET.m_ConnectionString) Then
            '    MsgBox("Found Transactions on this Agency, Cannot Delete Before deleted Transaction ")
            '    Exit Sub
            'End If
            '            UL.BeginWork(201, "Delete cart ProfileID ID:" & ProfileID, "")
            'Me.EUS_ProfilesTableAdapter.FillByProfileID(Me.DateDealsDataSet.EUS_Profiles, ProfileID)
            PergormReLogin()
            CheckWebServiceCallResult(gAdminWS.GetEUS_Profiles_ByProfileOrMirrorID(gLoginRec.Token, Me.DSMembers, ProfileID))
            Me.EUS_ProfilesBindingSource.DataSource = Me.DSMembers

            Dim sz As String = ""
            If (Permanently) Then
                sz = "Deleted Permanently " & Me.EUS_ProfilesBindingSource.Current("LoginName")
                Me.EUS_ProfilesBindingSource.RemoveCurrent()
            Else
                sz = "Deleted " & Me.EUS_ProfilesBindingSource.Current("LoginName")
                For Each dr As EUS_ProfilesRow In Me.DSMembers.EUS_Profiles.Rows
                    dr.Status = ProfileStatusEnum.Deleted
                Next
            End If
            _Save()
            '            UL.EndWork(1, ProfileID, sz, 0, 0, 0)
            MsgBox(sz)
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub



    Private Sub _LoadProfileData()

        PergormReLogin()
        CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS(gLoginRec.Token, Me.DSLists))
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_Income, "US", "IncomeId", icbAnnualIncome, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_BodyType, "US", "BodyTypeId", icbBodyTypeID, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_ChildrenNumber, "US", "ChildrenNumberId", icbChildrenID, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_Drinking, "US", "DrinkingId", icbDrinkingHabit, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_Education, "US", "EducationId", icbEducation, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_Ethnicity, "US", "EthnicityId", icbEthnicity, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_EyeColor, "US", "EyeColorId", icbEyeColorID, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_HairColor, "US", "HairColorId", icbHairColorID, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_Height, "US", "HeightId", icbHeightID, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_NetWorth, "US", "NetWorthId", icbNetWorth, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_RelationshipStatus, "US", "RelationshipStatusId", icbRelationshipStatusID, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_Religion, "US", "ReligionId", icbReligion, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_Smoking, "US", "SmokingId", icbSmokingHabit, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_Gender, "US", "GenderId", icbGenderId, True)
        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_AccountType, "US", "AccountTypeId", icbAccountTypeId, True)

        ' Hide all buttons that indicate change of field
        _HideChangeNotificationButtons(Me)

        If (FormAction = FormActionEnum.ADD) Then

            Try
                Me.EUS_ProfilesBindingSource.AddNew()

                Me.EUS_ProfilesBindingSource.Current("IsMaster") = 0
                Me.EUS_ProfilesBindingSource.Current("MirrorProfileID") = 0
                Me.EUS_ProfilesBindingSource.Current("Status") = 0
                Me.EUS_ProfilesBindingSource.Current("LoginName") = ""
                Me.EUS_ProfilesBindingSource.Current("Password") = ""
                Me.EUS_ProfilesBindingSource.Current("FirstName") = ""
                Me.EUS_ProfilesBindingSource.Current("LastName") = ""
                Me.EUS_ProfilesBindingSource.Current("GenderId") = ""
                Me.EUS_ProfilesBindingSource.Current("Country") = ""
                Me.EUS_ProfilesBindingSource.Current("Region") = ""
                Me.EUS_ProfilesBindingSource.Current("City") = ""
                Me.EUS_ProfilesBindingSource.Current("Zip") = ""
                Me.EUS_ProfilesBindingSource.Current("CityArea") = ""
                Me.EUS_ProfilesBindingSource.Current("Address") = ""
                Me.EUS_ProfilesBindingSource.Current("Telephone") = ""
                Me.EUS_ProfilesBindingSource.Current("eMail") = ""
                Me.EUS_ProfilesBindingSource.Current("Cellular") = ""
                Me.EUS_ProfilesBindingSource.Current("AreYouWillingToTravel") = 0
                Me.EUS_ProfilesBindingSource.Current("AboutMe_Heading") = ""
                Me.EUS_ProfilesBindingSource.Current("AboutMe_DescribeYourself") = ""
                Me.EUS_ProfilesBindingSource.Current("AboutMe_DescribeAnIdealFirstDate") = ""
                Me.EUS_ProfilesBindingSource.Current("OtherDetails_EducationID") = 0
                Me.EUS_ProfilesBindingSource.Current("OtherDetails_AnnualIncomeID") = 0
                Me.EUS_ProfilesBindingSource.Current("OtherDetails_NetWorthID") = 0
                Me.EUS_ProfilesBindingSource.Current("OtherDetails_Occupation") = ""
                Me.EUS_ProfilesBindingSource.Current("PersonalInfo_HeightID") = 0
                Me.EUS_ProfilesBindingSource.Current("PersonalInfo_BodyTypeID") = 0
                Me.EUS_ProfilesBindingSource.Current("PersonalInfo_EyeColorID") = 0
                Me.EUS_ProfilesBindingSource.Current("PersonalInfo_HairColorID") = 0
                Me.EUS_ProfilesBindingSource.Current("PersonalInfo_ChildrenID") = 0
                Me.EUS_ProfilesBindingSource.Current("PersonalInfo_EthnicityID") = 0
                Me.EUS_ProfilesBindingSource.Current("PersonalInfo_ReligionID") = 0
                Me.EUS_ProfilesBindingSource.Current("PersonalInfo_SmokingHabitID") = 0
                Me.EUS_ProfilesBindingSource.Current("PersonalInfo_DrinkingHabitID") = 0
                Me.EUS_ProfilesBindingSource.Current("LookingFor_ToMeetMaleID") = 0
                Me.EUS_ProfilesBindingSource.Current("LookingFor_ToMeetFemaleID") = 0
                Me.EUS_ProfilesBindingSource.Current("LookingFor_RelationshipStatusID") = 0
                Me.EUS_ProfilesBindingSource.Current("LookingFor_TypeOfDating_ShortTermRelationship") = 0
                Me.EUS_ProfilesBindingSource.Current("LookingFor_TypeOfDating_Friendship") = 0
                Me.EUS_ProfilesBindingSource.Current("LookingFor_TypeOfDating_LongTermRelationship") = 0
                Me.EUS_ProfilesBindingSource.Current("LookingFor_TypeOfDating_MutuallyBeneficialArrangements") = 0
                Me.EUS_ProfilesBindingSource.Current("LookingFor_TypeOfDating_MarriedDating") = 0
                Me.EUS_ProfilesBindingSource.Current("LookingFor_TypeOfDating_AdultDating_Casual") = 0
                Me.EUS_ProfilesBindingSource.Current("PrivacySettings_HideMeFromSearchResults") = 0
                Me.EUS_ProfilesBindingSource.Current("PrivacySettings_HideMeFromMembersIHaveBlocked") = 0
                Me.EUS_ProfilesBindingSource.Current("PrivacySettings_NotShowInOtherUsersFavoritedList") = 0
                Me.EUS_ProfilesBindingSource.Current("PrivacySettings_NotShowInOtherUsersViewedList") = 0
                Me.EUS_ProfilesBindingSource.Current("PrivacySettings_NotShowMyGifts") = 0
                Me.EUS_ProfilesBindingSource.Current("PrivacySettings_HideMyLastLoginDateTime") = 0
                Me.EUS_ProfilesBindingSource.Current("DateTimeToRegister") = Now
                Me.EUS_ProfilesBindingSource.Current("RegisterIP") = ""
                Me.EUS_ProfilesBindingSource.Current("RegisterGEOInfos") = ""
                Me.EUS_ProfilesBindingSource.Current("RegisterUserAgent") = ""
                Me.EUS_ProfilesBindingSource.Current("LastLoginDateTime") = Now
                Me.EUS_ProfilesBindingSource.Current("LastLoginIP") = ""
                Me.EUS_ProfilesBindingSource.Current("LastLoginGEOInfos") = ""
                Me.EUS_ProfilesBindingSource.Current("LastUpdateProfileDateTime") = Now
                Me.EUS_ProfilesBindingSource.Current("LastUpdateProfileIP") = ""
                Me.EUS_ProfilesBindingSource.Current("LastUpdateProfileGEOInfo") = ""
                Me.EUS_ProfilesBindingSource.Current("LAGID") = "US"
                Me.EUS_ProfilesBindingSource.Current("ThemeName") = "Red"
                Me.EUS_ProfilesBindingSource.Current("Referrer") = ""
                Me.EUS_ProfilesBindingSource.Current("CustomReferrer") = ""

                'UL.BeginWork(201, "New Entry Profile", "")
                lbProfileCart.Text = "New Entry Profile"
            Catch ex As Exception
                ErrorMsgBox(ex, "")
            End Try

        ElseIf (FormAction = FormActionEnum.EDIT) Then

            Dim masterRow As EUS_ProfilesRow = Nothing
            Dim mirrorRow As EUS_ProfilesRow = Nothing

            Try
                'UL.BeginWork(202, "Edit cart ProfileID:" & ProfileID, "")
                PergormReLogin()
                CheckWebServiceCallResult(gAdminWS.GetEUS_Profiles_ByProfileOrMirrorID(gLoginRec.Token, Me.DSMembers, ProfileID))

                Dim cnt As Integer = 0
                For cnt = 0 To Me.DSMembers.EUS_Profiles.Rows.Count - 1
                    Dim dr As DataRow = Me.DSMembers.EUS_Profiles.Rows(cnt)
                    If (dr("Status") = ProfileStatusEnum.NewProfile) Then
                        MasterProfileID = dr("ProfileID")
                        MasterRowIndex = cnt
                        MirrorProfileID = dr("ProfileID")
                        MirrorRowIndex = cnt
                        mirrorRow = Me.DSMembers.EUS_Profiles(MirrorRowIndex)
                    Else

                        If (dr("IsMaster") = True) Then
                            MasterProfileID = dr("ProfileID")
                            MasterRowIndex = cnt
                            masterRow = Me.DSMembers.EUS_Profiles(MasterRowIndex)
                        ElseIf (dr("IsMaster") = False) Then
                            MirrorProfileID = dr("ProfileID")
                            MirrorRowIndex = cnt
                            mirrorRow = Me.DSMembers.EUS_Profiles(MirrorRowIndex)
                        End If

                    End If
                Next

                Me.EUS_ProfilesBindingSource.DataSource = Me.DSMembers
                Me.EUS_ProfilesBindingSource.Position = MirrorRowIndex

                If (Me.EUS_ProfilesBindingSource.Current Is Nothing) Then
                    Me.EUS_ProfilesBindingSource.Position = MasterRowIndex
                End If


                ' search for changed fields and show related indication buttons
                If (MasterRowIndex <> MirrorRowIndex AndAlso Me.DSMembers.EUS_Profiles.Rows.Count = 2) Then
                    _MarkDifferentFields(masterRow, mirrorRow)
                End If


                ' profile gynaikas
                If (mirrorRow.GenderId = 2) Then
                    lbAnnualIncomeID.Visible = False
                    icbAnnualIncome.Visible = False
                    lbNetWorthID.Visible = False
                    icbNetWorth.Visible = False
                Else
                    lbAnnualIncomeID.Visible = True
                    icbAnnualIncome.Visible = True
                End If


            Catch ex As Exception
                ErrorMsgBox(ex, "")
            End Try


            Try

                PergormReLogin()
                CheckWebServiceCallResult(gAdminWS.GetEUS_CustomerTransaction(gLoginRec.Token, Me.DSMembers, ProfileID))
                Me.EUS_CustomerTransactionBindingSource.DataSource = Me.DSMembers.EUS_CustomerTransaction

                Dim hiddenColumnsNames As New List(Of String)
                hiddenColumnsNames.Add("CustomerTransactionID")
                hiddenColumnsNames.Add("CustomerID")
                Me.ucGridTransactions.LoadControl(Me.DSMembers.EUS_CustomerTransaction, hiddenColumnsNames)
            Catch ex As Exception
                ErrorMsgBox(ex, "")
            End Try


            If (masterRow IsNot Nothing And mirrorRow IsNot Nothing) Then
                Me.cmdApprove.Enabled = True
                If (masterRow.Status = ProfileStatusEnum.Approved AndAlso mirrorRow.Status = ProfileStatusEnum.Approved) Then
                    Me.cmdApprove.Enabled = False
                End If

                'lbProfileCart.Text = masterRow.LoginName
            End If


            If (mirrorRow IsNot Nothing) Then
                Me.cmdReject.Enabled = True
                If (mirrorRow.Status = ProfileStatusEnum.Rejected) Then
                    Me.cmdReject.Enabled = False
                End If

                'lbProfileCart.Text = mirrorRow.LoginName
            End If

            If (masterRow IsNot Nothing) Then

                Me.Text = "Profile : " & masterRow.LoginName
                lbProfileCart.Text = masterRow.LoginName & ", " & _
                    "Age: " & ProfileHelper.GetCurrentAge(masterRow.Birthday) & ", " & _
                    masterRow.City & ", " & _
                    ModMain.GetCountryName(masterRow.Country, Me.DSLists.SYS_CountriesGEO)

                lbProfileCart.Text = lbProfileCart.Text.Replace(", ,", ",")
            ElseIf (mirrorRow IsNot Nothing) Then

                Me.Text = "Profile : " & mirrorRow.LoginName
                lbProfileCart.Text = mirrorRow.LoginName & ", " & _
                    "Age: " & ProfileHelper.GetCurrentAge(mirrorRow.Birthday) & ", " & _
                    mirrorRow.City & ", " & _
                    ModMain.GetCountryName(mirrorRow.Country, Me.DSLists.SYS_CountriesGEO)

                lbProfileCart.Text = lbProfileCart.Text.Replace(", ,", ",")
            End If

            If (cbStatus.Properties.Items.Count = 0) Then
                ClsCombos.FillComboUsingIntegerBasedEnum(GetType(ProfileStatusEnum), cbStatus, True)
                Dim ic As ImageComboBoxItem = cbStatus.Properties.Items.GetItem(mirrorRow.Status)
                cbStatus.SelectedItem = ic
            End If

        ElseIf (FormAction = FormActionEnum.DELETE) Then

        End If


    End Sub


    Private Sub _MarkDifferentFields(masterRow As EUS_ProfilesRow, mirrorRow As EUS_ProfilesRow)
        For Each dc As DataColumn In Me.DSMembers.EUS_Profiles.Columns

            Try
                Select Case dc.ColumnName

                    Case "LoginName"
                        If (masterRow.IsLoginNameNull()) Then masterRow.LoginName = String.Empty
                        If (mirrorRow.IsLoginNameNull()) Then mirrorRow.LoginName = String.Empty

                        If (masterRow.LoginName <> mirrorRow.LoginName) Then

                            btnLoginNameChanged.Visible = True
                            btnLoginName2Changed.Visible = True

                        End If

                    Case "Country"
                        If (masterRow.IsCountryNull()) Then masterRow.Country = String.Empty
                        If (mirrorRow.IsCountryNull()) Then mirrorRow.Country = String.Empty

                        If (masterRow.Country <> mirrorRow.Country) Then

                            btnGEOTwoLetterCountryChanged.Visible = True
                            btnCountry2Changed.Visible = True

                        End If

                    Case "ThemeName"
                        If (masterRow.IsThemeNameNull()) Then masterRow.ThemeName = String.Empty
                        If (mirrorRow.IsThemeNameNull()) Then mirrorRow.ThemeName = String.Empty

                        If (masterRow.ThemeName <> mirrorRow.ThemeName) Then

                            btnThemeChanged.Visible = True

                        End If

                    Case "FirstName"
                        If (masterRow.IsFirstNameNull()) Then masterRow.FirstName = String.Empty
                        If (mirrorRow.IsFirstNameNull()) Then mirrorRow.FirstName = String.Empty

                        If (masterRow.FirstName <> mirrorRow.FirstName) Then

                            btnFirstNameChanged.Visible = True

                        End If

                    Case "LastName"
                        If (masterRow.IsLastNameNull()) Then masterRow.LastName = String.Empty
                        If (mirrorRow.IsLastNameNull()) Then mirrorRow.LastName = String.Empty

                        If (masterRow.LastName <> mirrorRow.LastName) Then

                            btnLastNameChanged.Visible = True

                        End If

                    Case "Region", "_Region"
                        If (masterRow.Is_RegionNull()) Then masterRow._Region = String.Empty
                        If (mirrorRow.Is_RegionNull()) Then mirrorRow._Region = String.Empty

                        If (masterRow._Region <> mirrorRow._Region) Then
                            btnRegionChanged.Visible = True
                        End If

                    Case "Zip"
                        If (masterRow.IsZipNull()) Then masterRow.Zip = String.Empty
                        If (mirrorRow.IsZipNull()) Then mirrorRow.Zip = String.Empty

                        If (masterRow.Zip <> mirrorRow.Zip) Then

                            btnZipChanged.Visible = True

                        End If

                    Case "City"
                        If (masterRow.IsCityNull()) Then masterRow.City = String.Empty
                        If (mirrorRow.IsCityNull()) Then mirrorRow.City = String.Empty

                        If (masterRow.City <> mirrorRow.City) Then

                            btnCityChanged.Visible = True

                        End If

                    Case "CityArea"
                        If (masterRow.IsCityAreaNull()) Then masterRow.CityArea = String.Empty
                        If (mirrorRow.IsCityAreaNull()) Then mirrorRow.CityArea = String.Empty

                        If (masterRow.CityArea <> mirrorRow.CityArea) Then

                            btnCityAreaChanged.Visible = True

                        End If

                    Case "Address"
                        If (masterRow.IsAddressNull()) Then masterRow.Address = String.Empty
                        If (mirrorRow.IsAddressNull()) Then mirrorRow.Address = String.Empty

                        If (masterRow.Address <> mirrorRow.Address) Then

                            btnAddressChanged.Visible = True

                        End If

                    Case "Telephone"
                        If (masterRow.IsTelephoneNull()) Then masterRow.Telephone = String.Empty
                        If (mirrorRow.IsTelephoneNull()) Then mirrorRow.Telephone = String.Empty

                        If (masterRow.Telephone <> mirrorRow.Telephone) Then

                            btnTelephoneChanged.Visible = True

                        End If

                    Case "eMail"
                        If (masterRow.IseMailNull()) Then masterRow.eMail = String.Empty
                        If (mirrorRow.IseMailNull()) Then mirrorRow.eMail = String.Empty

                        If (masterRow.eMail <> mirrorRow.eMail) Then

                            btnEmailChanged.Visible = True

                        End If

                    Case "Cellular"
                        If (masterRow.IsCellularNull()) Then masterRow.Cellular = String.Empty
                        If (mirrorRow.IsCellularNull()) Then mirrorRow.Cellular = String.Empty

                        If (masterRow.Cellular <> mirrorRow.Cellular) Then

                            btnCellularChanged.Visible = True

                        End If

                    Case "AboutMe_Heading"
                        If (masterRow.IsAboutMe_HeadingNull()) Then masterRow.AboutMe_Heading = String.Empty
                        If (mirrorRow.IsAboutMe_HeadingNull()) Then mirrorRow.AboutMe_Heading = String.Empty

                        If (masterRow.AboutMe_Heading <> mirrorRow.AboutMe_Heading) Then

                            btnAboutMe_HeadingChanged.Visible = True

                        End If

                    Case "AboutMe_DescribeYourself"
                        If (masterRow.IsAboutMe_DescribeYourselfNull()) Then masterRow.AboutMe_DescribeYourself = String.Empty
                        If (mirrorRow.IsAboutMe_DescribeYourselfNull()) Then mirrorRow.AboutMe_DescribeYourself = String.Empty

                        If (masterRow.AboutMe_DescribeYourself <> mirrorRow.AboutMe_DescribeYourself) Then

                            btnAboutMe_DescribeYourselfChanged.Visible = True

                        End If

                    Case "AboutMe_DescribeAnIdealFirstDate"
                        If (masterRow.IsAboutMe_DescribeAnIdealFirstDateNull()) Then masterRow.AboutMe_DescribeAnIdealFirstDate = String.Empty
                        If (mirrorRow.IsAboutMe_DescribeAnIdealFirstDateNull()) Then mirrorRow.AboutMe_DescribeAnIdealFirstDate = String.Empty

                        If (masterRow.AboutMe_DescribeAnIdealFirstDate <> mirrorRow.AboutMe_DescribeAnIdealFirstDate) Then

                            btnAboutMe_DescribeAnIdealFirstDateChanged.Visible = True

                        End If

                    Case "OtherDetails_Occupation"
                        If (masterRow.IsOtherDetails_OccupationNull()) Then masterRow.OtherDetails_Occupation = String.Empty
                        If (mirrorRow.IsOtherDetails_OccupationNull()) Then mirrorRow.OtherDetails_Occupation = String.Empty

                        If (masterRow.OtherDetails_Occupation <> mirrorRow.OtherDetails_Occupation) Then

                            btnOtherDetails_OccupationChanged.Visible = True

                        End If

                End Select
            Catch ex As Exception
                ErrorMsgBox(ex, "")
            End Try

        Next
    End Sub


    Private Sub _HideChangeNotificationButtons(ctl As Control)
        If (TypeOf ctl Is DevExpress.XtraEditors.SimpleButton) Then
            If (DirectCast(ctl, DevExpress.XtraEditors.SimpleButton).Name.EndsWith("Changed")) Then
                DirectCast(ctl, DevExpress.XtraEditors.SimpleButton).Visible = False
            End If
        Else
            For Each ctl1 As Control In ctl.Controls
                _HideChangeNotificationButtons(ctl1)
            Next
        End If
    End Sub

    Private Sub _Save()
        Try
            Me.Validate()
            Me.EUS_ProfilesBindingSource.EndEdit()
            Me.DSMembers = Me.EUS_ProfilesBindingSource.DataSource
            PergormReLogin()
            CheckWebServiceCallResult(gAdminWS.UpdateEUS_Profiles(gLoginRec.Token, Me.DSMembers))
            IsSaved = True
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub _SendEmailMessage(toAddress As String, subject As String, content As String)

        Dim client As New SmtpClient()
        Dim mail As New MailMessage()

        Dim FromEmail As String = My.MySettings.Default.FromEmail
        Dim FromEmailPassword As String = My.MySettings.Default.FromEmailPassword
        Dim SMTPPort As Integer = My.MySettings.Default.SMTPPort
        Dim SMTPHost As String = My.MySettings.Default.SMTPHost

        client.Credentials = New Net.NetworkCredential(FromEmail, FromEmailPassword)
        client.Port = SMTPPort
        client.Host = SMTPHost
        'client.EnableSsl = True

        mail = New MailMessage()
        mail.From = New MailAddress(FromEmail)
        mail.To.Add(toAddress)
        mail.Subject = subject
        mail.Body = content
        client.Send(mail)

        'Dim cmail As New Library.Public.clsMyMail()
        'cmail.SendMail("webmaster@dating-deals.com", toAddress, subject, content)
    End Sub

    Private Sub _DisplayDiff(colName As String)
        Try

            Me.DSMembers = Me.EUS_ProfilesBindingSource.DataSource

            Dim newRow As DataRow = Me.DSMembers.EUS_Profiles.Rows(MirrorRowIndex)
            Dim oldRow As DataRow = Me.DSMembers.EUS_Profiles.Rows(MasterRowIndex)

            Dim newValue As String = ""
            Dim oldValue As String = ""

            If (Not newRow.IsNull(colName)) Then newValue = newRow.Item(colName)
            If (Not oldRow.IsNull(colName)) Then oldValue = oldRow.Item(colName)

            Dim frm As New frmDDProfileChangedFields()
            frm.LoadControl(newValue, oldValue)
            frm.ShowDialog()
            If (frm.IsNewValueApproved) Then
                newRow.Item(colName) = frm.GetUpdatedNewValue()
            End If
            frm.Dispose()

            Me.EUS_ProfilesBindingSource.DataSource = Me.DSMembers
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub _ApproveNewProfile()
        Try
            Me.DSMembers = EUS_ProfilesBindingSource.DataSource

            Dim masterRow As EUS_ProfilesRow = Me.DSMembers.EUS_Profiles.Rows(MirrorRowIndex)
            masterRow.MirrorProfileID = 0
            masterRow.IsMaster = True
            masterRow.Status = ProfileStatusEnum.Approved


            Dim mirrorRow As EUS_ProfilesRow = Me.DSMembers.EUS_Profiles.NewEUS_ProfilesRow()
            mirrorRow.ItemArray = masterRow.ItemArray
            mirrorRow.IsMaster = False
            mirrorRow.ProfileID = -1
            mirrorRow.MirrorProfileID = masterRow.ProfileID
            Me.DSMembers.EUS_Profiles.AddEUS_ProfilesRow(mirrorRow)

            Try

                PergormReLogin()
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_Profiles(gLoginRec.Token, Me.DSMembers))

            Catch ex As Exception
                ErrorMsgBox(ex, "")
            End Try



            Dim newProfileId As Integer = 0
            For Each dr As DataRow In Me.DSMembers.EUS_Profiles.Rows
                If (dr("MirrorProfileID").ToString() = masterRow.ProfileID.ToString()) Then
                    newProfileId = dr("ProfileID")
                    Exit For
                End If
            Next

            For Each dr As DataRow In Me.DSMembers.EUS_Profiles.Rows
                If (dr("ProfileID").ToString() = masterRow.ProfileID.ToString()) Then
                    dr("MirrorProfileID") = newProfileId
                    Exit For
                End If
            Next

            PergormReLogin()
            CheckWebServiceCallResult(gAdminWS.UpdateEUS_Profiles(gLoginRec.Token, Me.DSMembers))

            ' refresh dataset
            PergormReLogin()
            CheckWebServiceCallResult(gAdminWS.GetEUS_Profiles_ByProfileOrMirrorID(gLoginRec.Token, Me.DSMembers, ProfileID))
            Me.EUS_ProfilesBindingSource.DataSource = Me.DSMembers

            _SendEmailMessage(masterRow.eMail, "Your subscription to " & My.MySettings.Default.SiteNameForCustomers & " approved.", "Your subscription to " & My.MySettings.Default.SiteNameForCustomers & " approved.")

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _ApproveExistingProfile()
        Try
            Dim isvalid = True
            If (MasterRowIndex = MirrorRowIndex) Then
                isvalid = False
                MsgBox("Invalid profile data: " & vbCrLf & vbTab & "the mirror and master is same table record." & vbCrLf & vbTab & "Cannot approve.")
            End If

            If (isvalid) Then

                Me.DSMembers = EUS_ProfilesBindingSource.DataSource

                ''''''''''''''''''''''''''''''''''''''''''''
                ' copy data from mirror record to master, overwrite old user data with new
                ''''''''''''''''''''''''''''''''''''''''''''

                ' copy data that is not read only
                Dim cnt As Integer = 0
                For cnt = 0 To DSMembers.EUS_Profiles.Columns.Count - 1
                    If (Not DSMembers.EUS_Profiles.Columns(cnt).ReadOnly) Then
                        DirectCast(Me.EUS_ProfilesBindingSource.Item(MasterRowIndex), DataRowView).Row(cnt) = DirectCast(Me.EUS_ProfilesBindingSource.Item(MirrorRowIndex), DataRowView).Row(cnt)
                    End If
                Next

                ' set specific columns data
                Dim mirrorRow As EUS_ProfilesRow = Me.DSMembers.EUS_Profiles.Rows(MirrorRowIndex)
                Dim masterRow As EUS_ProfilesRow = Me.DSMembers.EUS_Profiles.Rows(MasterRowIndex)

                mirrorRow.Status = ProfileStatusEnum.Approved
                masterRow.Status = ProfileStatusEnum.Approved

                mirrorRow.IsMaster = False
                masterRow.IsMaster = True

                mirrorRow.MirrorProfileID = masterRow.ProfileID
                masterRow.MirrorProfileID = mirrorRow.ProfileID

                ' update db
                PergormReLogin()
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_Profiles(gLoginRec.Token, Me.DSMembers))

                ' send email to user
                _SendEmailMessage(mirrorRow.eMail, "Your changes to " & My.MySettings.Default.SiteNameForCustomers & " profile are accepted.", "Your changes to " & My.MySettings.Default.SiteNameForCustomers & " profile are accepted.")

            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _RejectNewProfile(reasonStr As String, reasonTextStr As String)
        Try

            Me.DSMembers = EUS_ProfilesBindingSource.DataSource

            Dim mirrorRow As EUS_ProfilesRow = Me.DSMembers.EUS_Profiles.Rows(MirrorRowIndex)
            mirrorRow.Status = ProfileStatusEnum.Rejected
            PergormReLogin()
            CheckWebServiceCallResult(gAdminWS.UpdateEUS_Profiles(gLoginRec.Token, Me.DSMembers))

            Dim text =
                "Your subscription to " & My.MySettings.Default.SiteNameForCustomers & " rejected." & vbCrLf & vbCrLf & _
                reasonStr & vbCrLf & vbCrLf & _
                reasonTextStr

            _SendEmailMessage(mirrorRow.eMail, "Your subscription to " & My.MySettings.Default.SiteNameForCustomers & " rejected.", text)

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _RejectExistingProfile(reasonStr As String, reasonTextStr As String)
        Try
            Me.DSMembers = EUS_ProfilesBindingSource.DataSource


            ''''''''''''''''''''''''''''''''''''''''''''
            ' copy data from master record to mirror, overwrite user changes
            ''''''''''''''''''''''''''''''''''''''''''''

            ' copy data that is not read only
            Dim cnt As Integer = 0
            For cnt = 0 To DSMembers.EUS_Profiles.Columns.Count - 1
                If (Not DSMembers.EUS_Profiles.Columns(cnt).ReadOnly) Then
                    DirectCast(Me.EUS_ProfilesBindingSource.Item(MirrorRowIndex), DataRowView).Row(cnt) = DirectCast(Me.EUS_ProfilesBindingSource.Item(MasterRowIndex), DataRowView).Row(cnt)
                End If
            Next

            ' set specific columns data
            Dim mirrorRow As EUS_ProfilesRow = Me.DSMembers.EUS_Profiles.Rows(MirrorRowIndex)
            Dim masterRow As EUS_ProfilesRow = Me.DSMembers.EUS_Profiles.Rows(MasterRowIndex)

            mirrorRow.IsMaster = False
            masterRow.IsMaster = True

            mirrorRow.MirrorProfileID = masterRow.ProfileID
            masterRow.MirrorProfileID = mirrorRow.ProfileID


            ' update db
            PergormReLogin()
            CheckWebServiceCallResult(gAdminWS.UpdateEUS_Profiles(gLoginRec.Token, Me.DSMembers))

            ' send email to user
            Dim text =
                "Your changes to " & My.MySettings.Default.SiteNameForCustomers & " profile are rejected." & vbCrLf & vbCrLf & _
                reasonStr & vbCrLf & vbCrLf & _
                reasonTextStr

            _SendEmailMessage(mirrorRow.eMail, "Your changes to " & My.MySettings.Default.SiteNameForCustomers & " profile are rejected.", text)
        Catch ex As Exception
            Throw
        End Try
    End Sub



    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel1.Click
        '        UL.EndWork(False, ProfileID, "ProfileID Cart", 0, 0, 0)
        Close()
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave1.Click
        Me.ProfileID = Me.EUS_ProfilesBindingSource.Current("ProfileID")
        '        UL.EndWork(1, ProfileID, "ProfileID Cart", 0, 0, 0)
        _Save()
        Hide()
    End Sub

    Private Sub cmdApprove_Click(sender As System.Object, e As System.EventArgs) Handles cmdApprove.Click
        Try

            Dim approveMsg As String = "You are about to APPROVE current profile." & vbCrLf & "Are you sure you want to continue?"
            Dim result = MessageBox.Show(approveMsg, Me.Text & " - Approve", MessageBoxButtons.OKCancel)

            If (result = Windows.Forms.DialogResult.OK OrElse result = Windows.Forms.DialogResult.Yes) Then


                'Me.DSMembers.EUS_Profiles.Rows(0)("Status") 
                If (DirectCast(cbStatus.SelectedItem, ImageComboBoxItem).Value = ProfileStatusEnum.NewProfile) Then
                    ' save and reload before approving
                    _Save()


                    PergormReLogin()
                    CheckWebServiceCallResult(gAdminWS.GetEUS_Profiles_ByProfileOrMirrorID(gLoginRec.Token, Me.DSMembers, ProfileID))
                    Me.EUS_ProfilesBindingSource.DataSource = Me.DSMembers
                    _ApproveNewProfile()


                    PergormReLogin()
                    CheckWebServiceCallResult(gAdminWS.NotifyMembersBeForNewMember(gLoginRec.Token, ProfileID))

                ElseIf (DirectCast(cbStatus.SelectedItem, ImageComboBoxItem).Value = ProfileStatusEnum.Updating) Then
                    ' save and reload before approving
                    _Save()

                    PergormReLogin()
                    CheckWebServiceCallResult(gAdminWS.GetEUS_Profiles_ByProfileOrMirrorID(gLoginRec.Token, Me.DSMembers, ProfileID))
                    Me.EUS_ProfilesBindingSource.DataSource = Me.DSMembers
                    _ApproveExistingProfile()
                End If

                Me.Close()
            End If

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub cmdReject_Click(sender As System.Object, e As System.EventArgs) Handles cmdReject.Click
        Try

            frmDDProfileRejecting.ShowDialog()
            If (frmDDProfileRejecting.Done) Then
                Dim reasonStr As String = frmDDProfileRejecting.GetReason()
                Dim reasonTextStr As String = frmDDProfileRejecting.GetReasonText()

                If (DirectCast(cbStatus.SelectedItem, ImageComboBoxItem).Value = ProfileStatusEnum.NewProfile) Then
                    _RejectNewProfile(reasonStr, reasonTextStr)
                ElseIf (DirectCast(cbStatus.SelectedItem, ImageComboBoxItem).Value = ProfileStatusEnum.Updating) Then
                    _RejectExistingProfile(reasonStr, reasonTextStr)
                End If
                Me.Close()
            End If

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub btnLoginNameChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnLoginNameChanged.Click
        _DisplayDiff("LoginName")
    End Sub

    Private Sub btnGEOTwoLetterCountryChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnGEOTwoLetterCountryChanged.Click
        _DisplayDiff("Country")
    End Sub

    Private Sub btnThemeChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnThemeChanged.Click
        _DisplayDiff("ThemeName")
    End Sub

    Private Sub btnLoginName2Changed_Click(sender As System.Object, e As System.EventArgs) Handles btnLoginName2Changed.Click
        _DisplayDiff("LoginName")
    End Sub

    Private Sub btnFirstNameChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnFirstNameChanged.Click
        _DisplayDiff("FirstName")
    End Sub

    Private Sub btnLastNameChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnLastNameChanged.Click
        _DisplayDiff("LastName")
    End Sub

    Private Sub btnGenderChanged_Click(sender As System.Object, e As System.EventArgs)
        _DisplayDiff("GenderId")
    End Sub

    Private Sub btnCountry2Changed_Click(sender As System.Object, e As System.EventArgs) Handles btnCountry2Changed.Click
        _DisplayDiff("Country")
    End Sub

    Private Sub btnRegionChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnRegionChanged.Click
        _DisplayDiff("Region")
    End Sub

    Private Sub btnCityChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnCityChanged.Click
        _DisplayDiff("City")
    End Sub

    Private Sub btnCityAreaChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnCityAreaChanged.Click
        _DisplayDiff("CityArea")
    End Sub

    Private Sub btnAddressChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnAddressChanged.Click
        _DisplayDiff("Address")
    End Sub

    Private Sub btnTelephoneChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnTelephoneChanged.Click
        _DisplayDiff("Telephone")
    End Sub

    Private Sub btnEmailChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnEmailChanged.Click
        _DisplayDiff("eMail")
    End Sub

    Private Sub btnZipChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnZipChanged.Click
        _DisplayDiff("Zip")
    End Sub

    Private Sub btnAboutMe_HeadingChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnAboutMe_HeadingChanged.Click
        _DisplayDiff("AboutMe_Heading")
    End Sub

    Private Sub btnAboutMe_DescribeYourselfChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnAboutMe_DescribeYourselfChanged.Click
        _DisplayDiff("AboutMe_DescribeYourself")
    End Sub

    Private Sub btnAboutMe_DescribeAnIdealFirstDateChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnAboutMe_DescribeAnIdealFirstDateChanged.Click
        _DisplayDiff("AboutMe_DescribeAnIdealFirstDate")
    End Sub

    Private Sub btnOtherDetails_OccupationChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnOtherDetails_OccupationChanged.Click
        _DisplayDiff("OtherDetails_Occupation")
    End Sub

    Private Sub btnCellularChanged_Click(sender As System.Object, e As System.EventArgs) Handles btnCellularChanged.Click
        _DisplayDiff("Cellular")
    End Sub

    Private Sub cbStatus_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbStatus.SelectedIndexChanged

    End Sub

    Private Sub btnNextChange_Click(sender As System.Object, e As System.EventArgs) Handles cmdNextChange.Click

    End Sub

    Private Sub btnPreviousChange_Click(sender As System.Object, e As System.EventArgs) Handles cmdPreviousChange.Click

    End Sub


#End Region



    Private Sub TABS_SelectedPageChanging(sender As System.Object, e As DevExpress.XtraTab.TabPageChangingEventArgs) Handles TABS.SelectedPageChanging

        If (e.Page.Equals(TABPhotosCarousel)) Then
            pcMainCommands.Visible = False
            ucLayout3D1.ProfileID = Me.MasterProfileID
            'ElseIf (e.Page.Equals(TABPhotos)) Then
            '    pcMainCommands.Visible = False
        Else
            pcMainCommands.Visible = True
        End If


        If (e.Page.Equals(TABCredits)) Then
            PergormReLogin()
            CheckWebServiceCallResult(gAdminWS.GetCustomerAvailableCredits(gLoginRec.Token, Me.MasterProfileID, Me.DSMembers))
            CustomerAvailableCreditsBindingSource.DataSource = Me.DSMembers.CustomerAvailableCredits

            CheckWebServiceCallResult(gAdminWS.GetCustomerCreditsHistory(gLoginRec.Token, Me.MasterProfileID, Me.DSMembers))
            'CustomerCreditsHistoryBindingSource.DataSource = Me.DSMembers.CustomerCreditsHistory
            Dim hiddenColumnsNames As New List(Of String)
            hiddenColumnsNames.Add("CustomerCreditsId")
            hiddenColumnsNames.Add("CustomerId")
            UcGridCreditsHistory.LoadControl(Me.DSMembers.CustomerCreditsHistory, hiddenColumnsNames)

        End If
    End Sub

End Class


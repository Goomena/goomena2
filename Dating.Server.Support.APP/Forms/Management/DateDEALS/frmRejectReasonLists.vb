﻿Imports DevExpress.XtraVerticalGrid
Imports Library.Public

Public Class frmRejectReasonLists


    Private Sub frmDDManagerLists_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.gpMain.Text = Me.Text
        'RepositoryItemImageComboBoxLAG.OwnerEdit.SelectedText = "US"
        LoadGrid()
    End Sub




    Private Sub LoadGrid()
        Try
            ShowWaitWin("Working please wait...")
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetEUS_LISTS")
            CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_RejectingReasons(gAdminWSHeaders, Me.DSLists))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetEUS_LISTS")

            Dim FocusedRowHandle As Integer = gv1.FocusedRowHandle
            Me.EUSLISTSRejectingReasonsBindingSource.DataSource = Me.DSLists.EUS_LISTS_RejectingReasons

            If (FocusedRowHandle > -1 AndAlso FocusedRowHandle < Me.DSLists.EUS_LISTS_RejectingReasons.Rows.Count) Then
                gv1.FocusedRowHandle = FocusedRowHandle
            End If
            HideWaitWin()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmRejectReasonLists->RetriveData")
        End Try
    End Sub


    Private Sub Save()
        Try
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateEUS_LISTS")
            ShowWaitWin("Working please wait...")
            CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_RejectingReasons(gAdminWSHeaders, Me.DSLists))
            HideWaitWin()
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateEUS_LISTS")
        Catch ex As Exception
            ErrorMsgBox(ex, "frmRejectReasonLists->RetriveData")
        End Try
    End Sub



    Private Sub ADD()
        Try
            Dim frm As New frmRejectReasonItem()
            frm.ADD(-1, cbLAG.EditValue)
            frm.ShowDialog()
            If (frm.IsOK) Then
                LoadGrid()
            End If
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub EDIT()
        Try
            Dim RejectingReasonsId As Integer = DirectCast(DirectCast(gv1.GetFocusedRow(), System.Data.DataRowView).Row, AdminWS.DSLists.EUS_LISTS_RejectingReasonsRow).RejectingReasonsId
            Dim frm As New frmRejectReasonItem()
            frm.EDIT(RejectingReasonsId, cbLAG.EditValue)
            frm.ShowDialog()
            If (frm.IsOK) Then
                LoadGrid()
            End If
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub DELETE()
        Try
            If MsgBox("Are you sure delete selected record?", MsgBoxStyle.Exclamation + vbYesNo) = vbYes Then
                gv1.DeleteSelectedRows()
                Save()
                LoadGrid()
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub gv1_DoubleClick(sender As System.Object, e As System.EventArgs) Handles gv1.DoubleClick
        Me.EDIT()
    End Sub

    Private Sub BarAddNew_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarAddNew.ItemClick
        Me.ADD()
    End Sub

    Private Sub BarEdit_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarEdit.ItemClick
        Me.EDIT()
    End Sub

    Private Sub BarDelete_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarDelete.ItemClick
        Me.DELETE()
    End Sub

    Private Sub BarRefresh_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarRefresh.ItemClick
        Me.LoadGrid()
    End Sub

    Private Sub cbLAG_EditValueChanging(sender As System.Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles cbLAG.EditValueChanging
        If (e.OldValue <> "US") Then
            Dim col As DevExpress.XtraGrid.Columns.GridColumn = gv1.Columns(e.OldValue)
            col.Visible = False
            Dim col2 As DevExpress.XtraGrid.Columns.GridColumn = gv1.Columns("Text" & e.OldValue)
            col2.Visible = False
        End If
        If (e.NewValue <> "US") Then
            Dim col As DevExpress.XtraGrid.Columns.GridColumn = gv1.Columns(e.NewValue)
            col.Visible = True
            col.VisibleIndex = 2
            Dim col2 As DevExpress.XtraGrid.Columns.GridColumn = gv1.Columns("Text" & e.NewValue)
            col2.Visible = True
            col2.VisibleIndex = 3
        End If
    End Sub

End Class
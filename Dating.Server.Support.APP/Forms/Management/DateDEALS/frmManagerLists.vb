﻿Imports DevExpress.XtraVerticalGrid
Imports Library.Public

Public Class frmManagerLists

    Public Property WhatListToLoad As String


    Private Sub frmDDManagerLists_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        AddHandler Me.ucGridLists.GridView.KeyDown, AddressOf GridView_KeyDown
        'AddHandler Me.ucGridLists.GridView.DoubleClick, AddressOf GridView_DoubleClick
        AddHandler Me.ucGridLists.GridView.RowUpdated, AddressOf GridView_RowUpdated

        Me.gpMain.Text = Me.Text

        ' remove the edit button
        Me.ucGridLists.Bar1.Manager.Items.RemoveAt(1)

        If (Me.WhatListToLoad = "EUS_LISTS_Gender") Then
            ' remove add & delete buttons
            Me.ucGridLists.Bar1.Manager.Items.RemoveAt(0)
            Me.ucGridLists.Bar1.Manager.Items.RemoveAt(0)
        End If

        If (Me.WhatListToLoad = "EUS_LISTS_AccountType") Then
            ' remove add & delete buttons
            Me.ucGridLists.Bar1.Manager.Items.RemoveAt(0)
            Me.ucGridLists.Bar1.Manager.Items.RemoveAt(0)
        End If

        If (Me.WhatListToLoad = "EUS_LISTS_PhotosDisplayLevel") Then
            ' remove add & delete buttons
            Me.ucGridLists.Bar1.Manager.Items.RemoveAt(0)
            Me.ucGridLists.Bar1.Manager.Items.RemoveAt(0)
        End If

        If (Me.WhatListToLoad = "EUS_LISTS_TypeOfDating") Then
            ' remove add & delete buttons
            Me.ucGridLists.Bar1.Manager.Items.RemoveAt(0)
            Me.ucGridLists.Bar1.Manager.Items.RemoveAt(0)
        End If

        RefreshGrid()
    End Sub



    Private Sub RefreshGrid()
        ShowWaitWin("Working please wait...")
        Me.RetriveData()
        Me.LoadGrid()
        HideWaitWin()
    End Sub

    Private Sub RetriveData()
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetEUS_LISTS")
        Select Case (Me.WhatListToLoad)

            Case "EUS_LISTS_BodyType"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_BodyType(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_ChildrenNumber"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_ChildrenNumber(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Drinking"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_Drinking(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Education"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_Education(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Ethnicity"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_Ethnicity(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_EyeColor"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_EyeColor(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_HairColor"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_HairColor(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Height"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_Height(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Income"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_Income(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_NetWorth"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_NetWorth(gAdminWSHeaders, Me.DSLists))

                'Case "EUS_LISTS_RejectingReasons"
                '    CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_RejectingReasons(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_RelationshipStatus"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_RelationshipStatus(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Religion"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_Religion(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Smoking"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_Smoking(gAdminWSHeaders, Me.DSLists))

            Case "SYS_CountriesGEO"
                CheckWebServiceCallResult(gAdminWS.GetSYS_CountriesGEO(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_AccountType"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_AccountType(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Gender"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_Gender(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_PhotosDisplayLevel"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_PhotosDisplayLevel(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_TypeOfDating"
                CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_TypeOfDating(gAdminWSHeaders, Me.DSLists))

        End Select
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetEUS_LISTS")
    End Sub

    Private Sub LoadGrid()
        Dim FocusedRowHandle As Integer = Me.ucGridLists.GridView.FocusedRowHandle

        Select Case (Me.WhatListToLoad)
            'gAdminWS.get(gAdminWSHeaders, Me.DSMembers)
            Case "EUS_LISTS_BodyType"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_BodyType)

            Case "EUS_LISTS_ChildrenNumber"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_ChildrenNumber)

            Case "EUS_LISTS_Drinking"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_Drinking)

            Case "EUS_LISTS_Education"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_Education)

            Case "EUS_LISTS_Ethnicity"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_Ethnicity)

            Case "EUS_LISTS_EyeColor"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_EyeColor)

            Case "EUS_LISTS_HairColor"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_HairColor)

            Case "EUS_LISTS_Height"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_Height)

            Case "EUS_LISTS_Income"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_Income)

            Case "EUS_LISTS_NetWorth"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_NetWorth)

                'Case "EUS_LISTS_RejectingReasons"
                '    Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_RejectingReasons)

            Case "EUS_LISTS_RelationshipStatus"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_RelationshipStatus)

            Case "EUS_LISTS_Religion"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_Religion)

            Case "EUS_LISTS_Smoking"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_Smoking)

            Case "SYS_CountriesGEO"
                Me.ucGridLists.LoadControl(Me.DSLists.SYS_CountriesGEO)

            Case "EUS_LISTS_AccountType"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_AccountType)

            Case "EUS_LISTS_Gender"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_Gender)

            Case "EUS_LISTS_PhotosDisplayLevel"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_PhotosDisplayLevel)

            Case "EUS_LISTS_TypeOfDating"
                Me.ucGridLists.LoadControl(Me.DSLists.EUS_LISTS_TypeOfDating)


        End Select
        Me.ucGridLists.GridView.FocusedRowHandle = FocusedRowHandle
    End Sub

    Private Sub Save()

        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateEUS_LISTS")
        ShowWaitWin("Working please wait...")
        Select Case (Me.WhatListToLoad)
            'gAdminWS.get(gAdminWSHeaders, Me.DSMembers)
            Case "EUS_LISTS_BodyType"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_BodyType(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_ChildrenNumber"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_ChildrenNumber(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Drinking"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_Drinking(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Education"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_Education(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Ethnicity"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_Ethnicity(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_EyeColor"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_EyeColor(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_HairColor"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_HairColor(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Height"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_Height(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Income"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_Income(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_NetWorth"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_NetWorth(gAdminWSHeaders, Me.DSLists))

                'Case "EUS_LISTS_RejectingReasons"
                '    CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_RejectingReasons(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_RelationshipStatus"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_RelationshipStatus(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Religion"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_Religion(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Smoking"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_Smoking(gAdminWSHeaders, Me.DSLists))

            Case "SYS_CountriesGEO"
                CheckWebServiceCallResult(gAdminWS.UpdateSYS_CountriesGEO(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_AccountType"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_AccountType(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_Gender"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_Gender(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_PhotosDisplayLevel"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_PhotosDisplayLevel(gAdminWSHeaders, Me.DSLists))

            Case "EUS_LISTS_TypeOfDating"
                CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_TypeOfDating(gAdminWSHeaders, Me.DSLists))

        End Select
        HideWaitWin()
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateEUS_LISTS")
    End Sub



    Private Sub ADD()
        Try
            Me.ucGridLists.GridView.AddNewRow()
            'Dim FocusedRowHandle As Integer = GridView1.FocusedRowHandle
            'frmTerminalsCart.ADD()
            'If frmTerminalsCart.IsSaved Then
            '    RefreshGrid()
            '    Me.TerminalGridBindingSource.Position = Me.TerminalGridBindingSource.Find("TerminalID", frmTerminalsCart.TerminalID)
            'End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub EDIT()
        Try
            'Dim TerminalID As Integer
            'TerminalID = GridView1.GetFocusedRowCellValue("TerminalID")
            'Dim FocusedRowHandle As Integer = GridView1.FocusedRowHandle
            'frmTerminalsCart.EDIT(TerminalID)
            'If frmTerminalsCart.IsSaved Then
            '    RefreshGrid()
            '    Me.TerminalGridBindingSource.Position = Me.TerminalGridBindingSource.Find("TerminalID", frmTerminalsCart.TerminalID)
            'End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub DELETE()
        Try
            If MsgBox("Are you sure delete selected record?", MsgBoxStyle.Exclamation + vbYesNo) = vbYes Then
                Me.ucGridLists.GridView.DeleteSelectedRows()
                Save()
                LoadGrid()
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


#Region "ucGrid Control - GridView event handlers"
    Private Sub GridView_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyCode = System.Windows.Forms.Keys.Delete Then
            DELETE()
        ElseIf e.KeyCode = System.Windows.Forms.Keys.Enter Then
            Me.ucGridLists.GridView.CloseEditor()
            Me.ucGridLists.GridView.UpdateCurrentRow()
        End If
    End Sub

    'Private Sub GridView_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    EDIT()
    'End Sub

    Private Sub GridView_RowUpdated(ByVal sender As System.Object, ByVal e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs)
        System.Diagnostics.Debug.Print("GridView_RowUpdated")
        Save()
        LoadGrid()
        'RefreshGrid()
    End Sub
#End Region




#Region "ucGrid Control - toolbar event handlers"
    Private Sub ucGridLists_ToolbarAddNewClicked(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ucGridLists.ToolbarAddNewClicked
        ADD()
    End Sub

    Private Sub ucGridLists_ToolbarDeleteClicked(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ucGridLists.ToolbarDeleteClicked
        DELETE()
    End Sub

    Private Sub ucGridLists_ToolbarEditClicked(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ucGridLists.ToolbarEditClicked
        EDIT()
    End Sub

    Private Sub ucGridLists_ToolbarRefreshClicked(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles ucGridLists.ToolbarRefreshClicked
        RefreshGrid()
    End Sub
#End Region

End Class
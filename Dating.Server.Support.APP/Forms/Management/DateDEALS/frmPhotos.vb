﻿Imports Dating.Server.Core.DLL

Public Class frmPhotos


    Public Property DefaultView As PhotosFilterEnum
        Get
            Return ucLayout3D1.DefaultView

        End Get
        Set(value As PhotosFilterEnum)
            ucLayout3D1.DefaultView = value
        End Set
    End Property


    Public Property ProfileID As Integer
        Get
            Return ucLayout3D1.ProfileID

        End Get
        Set(value As Integer)
            ucLayout3D1.ProfileID = value
        End Set
    End Property



    Private Sub frmPhotos_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If (DefaultView = PhotosFilterEnum.AllPhotos) Then
            gcApprovalSettings.Text = "All Photos"
        ElseIf (DefaultView = PhotosFilterEnum.ApprovedPhotos) Then
            gcApprovalSettings.Text = "Approved Photos"
        ElseIf (DefaultView = PhotosFilterEnum.RejectedPhotos) Then
            gcApprovalSettings.Text = "Rejected Photos"
        ElseIf (DefaultView = PhotosFilterEnum.DeletedPhotos) Then
            gcApprovalSettings.Text = "Deleted Photos"
        ElseIf (DefaultView = PhotosFilterEnum.AutoApprovedPhotos) Then
            gcApprovalSettings.Text = "Auto Approved Photos"
        Else
            gcApprovalSettings.Text = "New Photos"
        End If
        ucLayout3D1.ShowEditProfileButton = True
        ucLayout3D1.Init()

        DefaultView = PhotosFilterEnum.None
    End Sub
End Class
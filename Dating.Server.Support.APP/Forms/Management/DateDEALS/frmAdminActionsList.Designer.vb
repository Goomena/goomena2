﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdminActionsList
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdminActionsList))
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.BarAddNew = New DevExpress.XtraBars.BarButtonItem()
        Me.BarOpen = New DevExpress.XtraBars.BarButtonItem()
        Me.BarDelete = New DevExpress.XtraBars.BarButtonItem()
        Me.BarRefresh = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar3 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItemEditProfile = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar4 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItemToday = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemYesterday = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemTwoDaysAgo = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemLastSevenDays = New DevExpress.XtraBars.BarButtonItem()
        Me.BarEditItemDateFrom = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.BarEditItemDateTo = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.BarEditItemSpinYear = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemSpinEditYear = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
        Me.BarEditItemSpinMonth = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemSpinEditMonth = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
        Me.BarButtonItemSpinGo = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.RepositoryItemImageComboBoxLAG = New DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTextEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemSpinEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
        Me.gpMain = New DevExpress.XtraEditors.GroupControl()
        Me.gridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.AdminActionsViewBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsSecurity = New Dating.Client.Admin.APP.AdminWS.dsSecurity()
        Me.gv1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colActionDescription = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAdminActionsId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDate_Time = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colProfileIdAffected = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSystemUserId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colProfileLoginName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUserLoginName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItemEditSendingProfile = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSpinEditYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSpinEditMonth, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemImageComboBoxLAG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gpMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpMain.SuspendLayout()
        CType(Me.gridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdminActionsViewBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsSecurity, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2, Me.Bar3, Me.Bar4})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageList1
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarAddNew, Me.BarOpen, Me.BarDelete, Me.BarRefresh, Me.BarButtonItemEditProfile, Me.BarEditItemDateFrom, Me.BarEditItemDateTo, Me.BarButtonItemToday, Me.BarButtonItemYesterday, Me.BarButtonItemTwoDaysAgo, Me.BarButtonItemLastSevenDays, Me.BarEditItemSpinMonth, Me.BarEditItemSpinYear, Me.BarButtonItemSpinGo})
        Me.BarManager1.MaxItemId = 22
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemImageComboBoxLAG, Me.RepositoryItemTextEdit1, Me.RepositoryItemDateEdit1, Me.RepositoryItemTextEdit2, Me.RepositoryItemTextEdit3, Me.RepositoryItemDateEdit2, Me.RepositoryItemSpinEdit1, Me.RepositoryItemSpinEditMonth, Me.RepositoryItemSpinEditYear})
        Me.BarManager1.StatusBar = Me.Bar3
        '
        'Bar2
        '
        Me.Bar2.BarAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Bar2.BarAppearance.Normal.Options.UseFont = True
        Me.Bar2.BarName = "Custom 2"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.FloatLocation = New System.Drawing.Point(189, 287)
        Me.Bar2.FloatSize = New System.Drawing.Size(322, 39)
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarAddNew), New DevExpress.XtraBars.LinkPersistInfo(Me.BarOpen), New DevExpress.XtraBars.LinkPersistInfo(Me.BarDelete), New DevExpress.XtraBars.LinkPersistInfo(Me.BarRefresh)})
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Custom 2"
        '
        'BarAddNew
        '
        Me.BarAddNew.Caption = "Add New"
        Me.BarAddNew.Id = 0
        Me.BarAddNew.ImageIndex = 100
        Me.BarAddNew.Name = "BarAddNew"
        Me.BarAddNew.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        Me.BarAddNew.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarOpen
        '
        Me.BarOpen.Caption = "Open"
        Me.BarOpen.Id = 1
        Me.BarOpen.ImageIndex = 103
        Me.BarOpen.Name = "BarOpen"
        Me.BarOpen.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarDelete
        '
        Me.BarDelete.Caption = "Delete"
        Me.BarDelete.Id = 2
        Me.BarDelete.ImageIndex = 101
        Me.BarDelete.Name = "BarDelete"
        Me.BarDelete.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        Me.BarDelete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarRefresh
        '
        Me.BarRefresh.Caption = "Refresh"
        Me.BarRefresh.Id = 3
        Me.BarRefresh.ImageIndex = 109
        Me.BarRefresh.Name = "BarRefresh"
        Me.BarRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'Bar3
        '
        Me.Bar3.BarName = "Custom 3"
        Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar3.DockCol = 0
        Me.Bar3.DockRow = 0
        Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar3.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemEditProfile)})
        Me.Bar3.OptionsBar.AllowQuickCustomization = False
        Me.Bar3.OptionsBar.DrawDragBorder = False
        Me.Bar3.OptionsBar.UseWholeRow = True
        Me.Bar3.Text = "Custom 3"
        '
        'BarButtonItemEditProfile
        '
        Me.BarButtonItemEditProfile.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BarButtonItemEditProfile.Caption = "Edit Profile"
        Me.BarButtonItemEditProfile.Id = 6
        Me.BarButtonItemEditProfile.Name = "BarButtonItemEditProfile"
        '
        'Bar4
        '
        Me.Bar4.BarName = "Custom 4"
        Me.Bar4.DockCol = 0
        Me.Bar4.DockRow = 1
        Me.Bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar4.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemToday, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemYesterday), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemTwoDaysAgo), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemLastSevenDays), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BarEditItemDateFrom, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BarEditItemDateTo, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BarEditItemSpinYear, "", True, True, True, 0, Nothing, DevExpress.XtraBars.BarItemPaintStyle.Standard), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BarEditItemSpinMonth, DevExpress.XtraBars.BarItemPaintStyle.Standard), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BarButtonItemSpinGo, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)})
        Me.Bar4.OptionsBar.UseWholeRow = True
        Me.Bar4.Text = "Custom 4"
        '
        'BarButtonItemToday
        '
        Me.BarButtonItemToday.Caption = "Today"
        Me.BarButtonItemToday.Id = 11
        Me.BarButtonItemToday.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BarButtonItemToday.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonItemToday.Name = "BarButtonItemToday"
        '
        'BarButtonItemYesterday
        '
        Me.BarButtonItemYesterday.Caption = "Yesterday"
        Me.BarButtonItemYesterday.Id = 12
        Me.BarButtonItemYesterday.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BarButtonItemYesterday.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonItemYesterday.Name = "BarButtonItemYesterday"
        '
        'BarButtonItemTwoDaysAgo
        '
        Me.BarButtonItemTwoDaysAgo.Caption = "2 Days Ago"
        Me.BarButtonItemTwoDaysAgo.Id = 13
        Me.BarButtonItemTwoDaysAgo.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BarButtonItemTwoDaysAgo.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonItemTwoDaysAgo.Name = "BarButtonItemTwoDaysAgo"
        '
        'BarButtonItemLastSevenDays
        '
        Me.BarButtonItemLastSevenDays.Caption = "Last 7 Days"
        Me.BarButtonItemLastSevenDays.Id = 14
        Me.BarButtonItemLastSevenDays.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BarButtonItemLastSevenDays.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonItemLastSevenDays.Name = "BarButtonItemLastSevenDays"
        '
        'BarEditItemDateFrom
        '
        Me.BarEditItemDateFrom.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BarEditItemDateFrom.Caption = "From"
        Me.BarEditItemDateFrom.Edit = Me.RepositoryItemDateEdit1
        Me.BarEditItemDateFrom.Id = 7
        Me.BarEditItemDateFrom.Name = "BarEditItemDateFrom"
        Me.BarEditItemDateFrom.Width = 113
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.RepositoryItemDateEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEdit1.EditFormat.FormatString = "dd/MM/yyyy"
        Me.RepositoryItemDateEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        Me.RepositoryItemDateEdit1.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        '
        'BarEditItemDateTo
        '
        Me.BarEditItemDateTo.Caption = "To"
        Me.BarEditItemDateTo.Edit = Me.RepositoryItemDateEdit2
        Me.BarEditItemDateTo.Id = 10
        Me.BarEditItemDateTo.Name = "BarEditItemDateTo"
        Me.BarEditItemDateTo.Width = 111
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.RepositoryItemDateEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEdit2.EditFormat.FormatString = "dd/MM/yyyy"
        Me.RepositoryItemDateEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        Me.RepositoryItemDateEdit2.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        '
        'BarEditItemSpinYear
        '
        Me.BarEditItemSpinYear.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BarEditItemSpinYear.Caption = "Year"
        Me.BarEditItemSpinYear.Edit = Me.RepositoryItemSpinEditYear
        Me.BarEditItemSpinYear.Id = 17
        Me.BarEditItemSpinYear.Name = "BarEditItemSpinYear"
        Me.BarEditItemSpinYear.Width = 76
        '
        'RepositoryItemSpinEditYear
        '
        Me.RepositoryItemSpinEditYear.AutoHeight = False
        Me.RepositoryItemSpinEditYear.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemSpinEditYear.Name = "RepositoryItemSpinEditYear"
        '
        'BarEditItemSpinMonth
        '
        Me.BarEditItemSpinMonth.Caption = "Month"
        Me.BarEditItemSpinMonth.Edit = Me.RepositoryItemSpinEditMonth
        Me.BarEditItemSpinMonth.Id = 16
        Me.BarEditItemSpinMonth.Name = "BarEditItemSpinMonth"
        Me.BarEditItemSpinMonth.Width = 76
        '
        'RepositoryItemSpinEditMonth
        '
        Me.RepositoryItemSpinEditMonth.AutoHeight = False
        Me.RepositoryItemSpinEditMonth.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemSpinEditMonth.Name = "RepositoryItemSpinEditMonth"
        '
        'BarButtonItemSpinGo
        '
        Me.BarButtonItemSpinGo.Caption = "Go"
        Me.BarButtonItemSpinGo.Id = 18
        Me.BarButtonItemSpinGo.ImageIndex = 173
        Me.BarButtonItemSpinGo.Name = "BarButtonItemSpinGo"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(927, 69)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 471)
        Me.barDockControlBottom.Size = New System.Drawing.Size(927, 22)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 69)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 402)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(927, 69)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 402)
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "About.ico")
        Me.ImageList1.Images.SetKeyName(1, "Accounts.ico")
        Me.ImageList1.Images.SetKeyName(2, "Address Book.ico")
        Me.ImageList1.Images.SetKeyName(3, "Air Brush.ico")
        Me.ImageList1.Images.SetKeyName(4, "Align-Centre-2.ico")
        Me.ImageList1.Images.SetKeyName(5, "Align-Centre.ico")
        Me.ImageList1.Images.SetKeyName(6, "Align-Full-2.ico")
        Me.ImageList1.Images.SetKeyName(7, "Align-Full.ico")
        Me.ImageList1.Images.SetKeyName(8, "Align-Left-2.ico")
        Me.ImageList1.Images.SetKeyName(9, "Align-Left.ico")
        Me.ImageList1.Images.SetKeyName(10, "Align-Right-2.ico")
        Me.ImageList1.Images.SetKeyName(11, "Align-Right.ico")
        Me.ImageList1.Images.SetKeyName(12, "Arrow-Left Right.ico")
        Me.ImageList1.Images.SetKeyName(13, "Arrow-Up Down.ico")
        Me.ImageList1.Images.SetKeyName(14, "Attachment.ico")
        Me.ImageList1.Images.SetKeyName(15, "Auto Correct-Options.ico")
        Me.ImageList1.Images.SetKeyName(16, "Auto-Type.ico")
        Me.ImageList1.Images.SetKeyName(17, "Back.ico")
        Me.ImageList1.Images.SetKeyName(18, "Bar Code.ico")
        Me.ImageList1.Images.SetKeyName(19, "Bold.ico")
        Me.ImageList1.Images.SetKeyName(20, "Book-Open.ico")
        Me.ImageList1.Images.SetKeyName(21, "Books.ico")
        Me.ImageList1.Images.SetKeyName(22, "Book-Search.ico")
        Me.ImageList1.Images.SetKeyName(23, "Borders and Shading.ico")
        Me.ImageList1.Images.SetKeyName(24, "Box-Closed-2.ico")
        Me.ImageList1.Images.SetKeyName(25, "Box-Closed.ico")
        Me.ImageList1.Images.SetKeyName(26, "Box-Open-2.ico")
        Me.ImageList1.Images.SetKeyName(27, "Box-Open.ico")
        Me.ImageList1.Images.SetKeyName(28, "Bullets-2.ico")
        Me.ImageList1.Images.SetKeyName(29, "Bullets.ico")
        Me.ImageList1.Images.SetKeyName(30, "Calculator.ico")
        Me.ImageList1.Images.SetKeyName(31, "Calendar.ico")
        Me.ImageList1.Images.SetKeyName(32, "Calendar-Go To Date.ico")
        Me.ImageList1.Images.SetKeyName(33, "Calendar-Search.ico")
        Me.ImageList1.Images.SetKeyName(34, "Calendar-Select Day.ico")
        Me.ImageList1.Images.SetKeyName(35, "Calendar-Select Month.ico")
        Me.ImageList1.Images.SetKeyName(36, "Calendar-Select Week.ico")
        Me.ImageList1.Images.SetKeyName(37, "Calendar-Select Work Week.ico")
        Me.ImageList1.Images.SetKeyName(38, "Camera.ico")
        Me.ImageList1.Images.SetKeyName(39, "Camera-Photo.ico")
        Me.ImageList1.Images.SetKeyName(40, "Card File.ico")
        Me.ImageList1.Images.SetKeyName(41, "Card.ico")
        Me.ImageList1.Images.SetKeyName(42, "Card-Add.ico")
        Me.ImageList1.Images.SetKeyName(43, "Card-Delete.ico")
        Me.ImageList1.Images.SetKeyName(44, "Card-Edit.ico")
        Me.ImageList1.Images.SetKeyName(45, "Card-New.ico")
        Me.ImageList1.Images.SetKeyName(46, "Card-Next.ico")
        Me.ImageList1.Images.SetKeyName(47, "Card-Previous.ico")
        Me.ImageList1.Images.SetKeyName(48, "Card-Remove.ico")
        Me.ImageList1.Images.SetKeyName(49, "Card-Search.ico")
        Me.ImageList1.Images.SetKeyName(50, "CD-Burn.ico")
        Me.ImageList1.Images.SetKeyName(51, "CD-In Case.ico")
        Me.ImageList1.Images.SetKeyName(52, "CD-ROM.ico")
        Me.ImageList1.Images.SetKeyName(53, "Center Across Cells.ico")
        Me.ImageList1.Images.SetKeyName(54, "Certificate.ico")
        Me.ImageList1.Images.SetKeyName(55, "Chart.ico")
        Me.ImageList1.Images.SetKeyName(56, "Chart-Bar.ico")
        Me.ImageList1.Images.SetKeyName(57, "Chart-Line.ico")
        Me.ImageList1.Images.SetKeyName(58, "Chart-Pie.ico")
        Me.ImageList1.Images.SetKeyName(59, "Chart-Point.ico")
        Me.ImageList1.Images.SetKeyName(60, "Clean.ico")
        Me.ImageList1.Images.SetKeyName(61, "Clear.ico")
        Me.ImageList1.Images.SetKeyName(62, "Clipboard.ico")
        Me.ImageList1.Images.SetKeyName(63, "Clock.ico")
        Me.ImageList1.Images.SetKeyName(64, "Close.ico")
        Me.ImageList1.Images.SetKeyName(65, "Colour Picker.ico")
        Me.ImageList1.Images.SetKeyName(66, "Columns-2.ico")
        Me.ImageList1.Images.SetKeyName(67, "Columns.ico")
        Me.ImageList1.Images.SetKeyName(68, "Command Prompt.ico")
        Me.ImageList1.Images.SetKeyName(69, "Compress.ico")
        Me.ImageList1.Images.SetKeyName(70, "Computer.ico")
        Me.ImageList1.Images.SetKeyName(71, "Connect.ico")
        Me.ImageList1.Images.SetKeyName(72, "Contact.ico")
        Me.ImageList1.Images.SetKeyName(73, "Contact-Add.ico")
        Me.ImageList1.Images.SetKeyName(74, "Contact-Delete.ico")
        Me.ImageList1.Images.SetKeyName(75, "Contact-Edit.ico")
        Me.ImageList1.Images.SetKeyName(76, "Contact-New.ico")
        Me.ImageList1.Images.SetKeyName(77, "Contact-Remove.ico")
        Me.ImageList1.Images.SetKeyName(78, "Contact-Search.ico")
        Me.ImageList1.Images.SetKeyName(79, "Copy.ico")
        Me.ImageList1.Images.SetKeyName(80, "Cross.ico")
        Me.ImageList1.Images.SetKeyName(81, "Currency.ico")
        Me.ImageList1.Images.SetKeyName(82, "Currency-Notes.ico")
        Me.ImageList1.Images.SetKeyName(83, "Cut.ico")
        Me.ImageList1.Images.SetKeyName(84, "Database.ico")
        Me.ImageList1.Images.SetKeyName(85, "Database-Add.ico")
        Me.ImageList1.Images.SetKeyName(86, "Database-Close.ico")
        Me.ImageList1.Images.SetKeyName(87, "Database-Delete.ico")
        Me.ImageList1.Images.SetKeyName(88, "Database-Edit.ico")
        Me.ImageList1.Images.SetKeyName(89, "Database-Filter.ico")
        Me.ImageList1.Images.SetKeyName(90, "Database-New.ico")
        Me.ImageList1.Images.SetKeyName(91, "Database-Open.ico")
        Me.ImageList1.Images.SetKeyName(92, "Database-Properties.ico")
        Me.ImageList1.Images.SetKeyName(93, "Database-Remove.ico")
        Me.ImageList1.Images.SetKeyName(94, "Database-Save.ico")
        Me.ImageList1.Images.SetKeyName(95, "Database-Schema.ico")
        Me.ImageList1.Images.SetKeyName(96, "Database-Search.ico")
        Me.ImageList1.Images.SetKeyName(97, "Database-Security.ico")
        Me.ImageList1.Images.SetKeyName(98, "Database-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(99, "Date-Time.ico")
        Me.ImageList1.Images.SetKeyName(100, "db-Add.ico")
        Me.ImageList1.Images.SetKeyName(101, "db-Cancel.ico")
        Me.ImageList1.Images.SetKeyName(102, "db-Delete.ico")
        Me.ImageList1.Images.SetKeyName(103, "db-Edit.ico")
        Me.ImageList1.Images.SetKeyName(104, "db-First.ico")
        Me.ImageList1.Images.SetKeyName(105, "db-Last.ico")
        Me.ImageList1.Images.SetKeyName(106, "db-Next.ico")
        Me.ImageList1.Images.SetKeyName(107, "db-Post.ico")
        Me.ImageList1.Images.SetKeyName(108, "db-Previous.ico")
        Me.ImageList1.Images.SetKeyName(109, "db-Refresh.ico")
        Me.ImageList1.Images.SetKeyName(110, "Decimals-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(111, "Decimals-Increase.ico")
        Me.ImageList1.Images.SetKeyName(112, "Decompress.ico")
        Me.ImageList1.Images.SetKeyName(113, "Delete.ico")
        Me.ImageList1.Images.SetKeyName(114, "Delete-Blue.ico")
        Me.ImageList1.Images.SetKeyName(115, "Design.ico")
        Me.ImageList1.Images.SetKeyName(116, "Desktop.ico")
        Me.ImageList1.Images.SetKeyName(117, "Dictionary.ico")
        Me.ImageList1.Images.SetKeyName(118, "Disconnect.ico")
        Me.ImageList1.Images.SetKeyName(119, "Discuss.ico")
        Me.ImageList1.Images.SetKeyName(120, "Doc-Access.ico")
        Me.ImageList1.Images.SetKeyName(121, "Doc-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(122, "Doc-Excel.ico")
        Me.ImageList1.Images.SetKeyName(123, "Doc-HTML.ico")
        Me.ImageList1.Images.SetKeyName(124, "Doc-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(125, "Doc-RTF.ico")
        Me.ImageList1.Images.SetKeyName(126, "Document-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(127, "Document-Protect.ico")
        Me.ImageList1.Images.SetKeyName(128, "Doc-Word.ico")
        Me.ImageList1.Images.SetKeyName(129, "Doc-XML.ico")
        Me.ImageList1.Images.SetKeyName(130, "Drawing.ico")
        Me.ImageList1.Images.SetKeyName(131, "Edit.ico")
        Me.ImageList1.Images.SetKeyName(132, "Equaliser.ico")
        Me.ImageList1.Images.SetKeyName(133, "Execute.ico")
        Me.ImageList1.Images.SetKeyName(134, "Exit.ico")
        Me.ImageList1.Images.SetKeyName(135, "Export.ico")
        Me.ImageList1.Images.SetKeyName(136, "Export-Access.ico")
        Me.ImageList1.Images.SetKeyName(137, "Export-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(138, "Export-CSV.ico")
        Me.ImageList1.Images.SetKeyName(139, "Export-Excel.ico")
        Me.ImageList1.Images.SetKeyName(140, "Export-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(141, "Export-HTML.ico")
        Me.ImageList1.Images.SetKeyName(142, "Export-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(143, "Export-RTF.ico")
        Me.ImageList1.Images.SetKeyName(144, "Export-Text.ico")
        Me.ImageList1.Images.SetKeyName(145, "Export-Word.ico")
        Me.ImageList1.Images.SetKeyName(146, "Export-XML.ico")
        Me.ImageList1.Images.SetKeyName(147, "Favorites.ico")
        Me.ImageList1.Images.SetKeyName(148, "Fill.ico")
        Me.ImageList1.Images.SetKeyName(149, "Fill-Down.ico")
        Me.ImageList1.Images.SetKeyName(150, "Fill-Left.ico")
        Me.ImageList1.Images.SetKeyName(151, "Fill-Right.ico")
        Me.ImageList1.Images.SetKeyName(152, "Fill-Up.ico")
        Me.ImageList1.Images.SetKeyName(153, "Filter.ico")
        Me.ImageList1.Images.SetKeyName(154, "Flag.ico")
        Me.ImageList1.Images.SetKeyName(155, "Flag-Delete.ico")
        Me.ImageList1.Images.SetKeyName(156, "Flag-Next.ico")
        Me.ImageList1.Images.SetKeyName(157, "Flag-Previous.ico")
        Me.ImageList1.Images.SetKeyName(158, "Flow Chart.ico")
        Me.ImageList1.Images.SetKeyName(159, "Folder List.ico")
        Me.ImageList1.Images.SetKeyName(160, "Folder.ico")
        Me.ImageList1.Images.SetKeyName(161, "Folder-Closed.ico")
        Me.ImageList1.Images.SetKeyName(162, "Folder-Delete.ico")
        Me.ImageList1.Images.SetKeyName(163, "Folder-New.ico")
        Me.ImageList1.Images.SetKeyName(164, "Folder-Options.ico")
        Me.ImageList1.Images.SetKeyName(165, "Folder-Search.ico")
        Me.ImageList1.Images.SetKeyName(166, "Footer.ico")
        Me.ImageList1.Images.SetKeyName(167, "Format Painter.ico")
        Me.ImageList1.Images.SetKeyName(168, "Format-Font Size.ico")
        Me.ImageList1.Images.SetKeyName(169, "Format-Font.ico")
        Me.ImageList1.Images.SetKeyName(170, "Format-Paragraph.ico")
        Me.ImageList1.Images.SetKeyName(171, "Format-Percentage.ico")
        Me.ImageList1.Images.SetKeyName(172, "Formatting-Remove.ico")
        Me.ImageList1.Images.SetKeyName(173, "Forward.ico")
        Me.ImageList1.Images.SetKeyName(174, "Full Screen.ico")
        Me.ImageList1.Images.SetKeyName(175, "Full Screen-New.ico")
        Me.ImageList1.Images.SetKeyName(176, "Function.ico")
        Me.ImageList1.Images.SetKeyName(177, "Go To Line.ico")
        Me.ImageList1.Images.SetKeyName(178, "Grid-2.ico")
        Me.ImageList1.Images.SetKeyName(179, "Grid.ico")
        Me.ImageList1.Images.SetKeyName(180, "Grid-Auto Format-2.ico")
        Me.ImageList1.Images.SetKeyName(181, "Grid-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(182, "Grid-Column Width.ico")
        Me.ImageList1.Images.SetKeyName(183, "Grid-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(184, "Grid-Delete Cell.ico")
        Me.ImageList1.Images.SetKeyName(185, "Grid-Delete Column.ico")
        Me.ImageList1.Images.SetKeyName(186, "Grid-Delete Row.ico")
        Me.ImageList1.Images.SetKeyName(187, "Grid-Delete.ico")
        Me.ImageList1.Images.SetKeyName(188, "Grid-Insert Cell.ico")
        Me.ImageList1.Images.SetKeyName(189, "Grid-Insert Column Left.ico")
        Me.ImageList1.Images.SetKeyName(190, "Grid-Insert Column Right.ico")
        Me.ImageList1.Images.SetKeyName(191, "Grid-Insert Column.ico")
        Me.ImageList1.Images.SetKeyName(192, "Grid-Insert Row Above.ico")
        Me.ImageList1.Images.SetKeyName(193, "Grid-Insert Row Below.ico")
        Me.ImageList1.Images.SetKeyName(194, "Grid-Insert Row.ico")
        Me.ImageList1.Images.SetKeyName(195, "Grid-Merge Cells.ico")
        Me.ImageList1.Images.SetKeyName(196, "Grid-New-2.ico")
        Me.ImageList1.Images.SetKeyName(197, "Grid-New.ico")
        Me.ImageList1.Images.SetKeyName(198, "Grid-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(199, "Grid-Options.ico")
        Me.ImageList1.Images.SetKeyName(200, "Grid-Row Height.ico")
        Me.ImageList1.Images.SetKeyName(201, "Grid-Select All-2.ico")
        Me.ImageList1.Images.SetKeyName(202, "Grid-Select All.ico")
        Me.ImageList1.Images.SetKeyName(203, "Grid-Select Cell-2.ico")
        Me.ImageList1.Images.SetKeyName(204, "Grid-Select Cell.ico")
        Me.ImageList1.Images.SetKeyName(205, "Grid-Select Column-2.ico")
        Me.ImageList1.Images.SetKeyName(206, "Grid-Select Column.ico")
        Me.ImageList1.Images.SetKeyName(207, "Grid-Select Row-2.ico")
        Me.ImageList1.Images.SetKeyName(208, "Grid-Select Row.ico")
        Me.ImageList1.Images.SetKeyName(209, "Grid-Split Cells.ico")
        Me.ImageList1.Images.SetKeyName(210, "Grid-Wizard-2.ico")
        Me.ImageList1.Images.SetKeyName(211, "Grid-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(212, "Hand Held.ico")
        Me.ImageList1.Images.SetKeyName(213, "Hand Shake.ico")
        Me.ImageList1.Images.SetKeyName(214, "Header and Footer.ico")
        Me.ImageList1.Images.SetKeyName(215, "Header and Footer-Toggle.ico")
        Me.ImageList1.Images.SetKeyName(216, "Header.ico")
        Me.ImageList1.Images.SetKeyName(217, "Header-Next.ico")
        Me.ImageList1.Images.SetKeyName(218, "Header-Previous.ico")
        Me.ImageList1.Images.SetKeyName(219, "Help.ico")
        Me.ImageList1.Images.SetKeyName(220, "Highlighter.ico")
        Me.ImageList1.Images.SetKeyName(221, "History.ico")
        Me.ImageList1.Images.SetKeyName(222, "Home.ico")
        Me.ImageList1.Images.SetKeyName(223, "Image.ico")
        Me.ImageList1.Images.SetKeyName(224, "Image-Brightness.ico")
        Me.ImageList1.Images.SetKeyName(225, "Image-Colour.ico")
        Me.ImageList1.Images.SetKeyName(226, "Image-Contrast.ico")
        Me.ImageList1.Images.SetKeyName(227, "Image-Crop.ico")
        Me.ImageList1.Images.SetKeyName(228, "Image-Flip.ico")
        Me.ImageList1.Images.SetKeyName(229, "Image-Mirror.ico")
        Me.ImageList1.Images.SetKeyName(230, "Image-Paint.ico")
        Me.ImageList1.Images.SetKeyName(231, "Image-Resize.ico")
        Me.ImageList1.Images.SetKeyName(232, "Image-Rotate.ico")
        Me.ImageList1.Images.SetKeyName(233, "Image-Select.ico")
        Me.ImageList1.Images.SetKeyName(234, "Import.ico")
        Me.ImageList1.Images.SetKeyName(235, "Import-Access.ico")
        Me.ImageList1.Images.SetKeyName(236, "Import-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(237, "Import-CSV.ico")
        Me.ImageList1.Images.SetKeyName(238, "Import-Excel.ico")
        Me.ImageList1.Images.SetKeyName(239, "Import-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(240, "Import-HTML.ico")
        Me.ImageList1.Images.SetKeyName(241, "Import-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(242, "Import-RTF.ico")
        Me.ImageList1.Images.SetKeyName(243, "Import-Text.ico")
        Me.ImageList1.Images.SetKeyName(244, "Import-Word.ico")
        Me.ImageList1.Images.SetKeyName(245, "Import-XML.ico")
        Me.ImageList1.Images.SetKeyName(246, "Indent-Decrease-2.ico")
        Me.ImageList1.Images.SetKeyName(247, "Indent-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(248, "Indent-Increase-2.ico")
        Me.ImageList1.Images.SetKeyName(249, "Indent-Increase.ico")
        Me.ImageList1.Images.SetKeyName(250, "Insert.ico")
        Me.ImageList1.Images.SetKeyName(251, "Insert-File.ico")
        Me.ImageList1.Images.SetKeyName(252, "Insert-Image.ico")
        Me.ImageList1.Images.SetKeyName(253, "Insert-Object.ico")
        Me.ImageList1.Images.SetKeyName(254, "Invoice.ico")
        Me.ImageList1.Images.SetKeyName(255, "Italic.ico")
        Me.ImageList1.Images.SetKeyName(256, "Key.ico")
        Me.ImageList1.Images.SetKeyName(257, "Landscape.ico")
        Me.ImageList1.Images.SetKeyName(258, "Launch.ico")
        Me.ImageList1.Images.SetKeyName(259, "Letter.ico")
        Me.ImageList1.Images.SetKeyName(260, "Line Spacing-2.ico")
        Me.ImageList1.Images.SetKeyName(261, "Line Spacing.ico")
        Me.ImageList1.Images.SetKeyName(262, "Link.ico")
        Me.ImageList1.Images.SetKeyName(263, "Lock.ico")
        Me.ImageList1.Images.SetKeyName(264, "Mail.ico")
        Me.ImageList1.Images.SetKeyName(265, "Mail-Forward.ico")
        Me.ImageList1.Images.SetKeyName(266, "Mail-In Box-2.ico")
        Me.ImageList1.Images.SetKeyName(267, "Mail-In Box.ico")
        Me.ImageList1.Images.SetKeyName(268, "Mail-In-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(269, "Mail-New.ico")
        Me.ImageList1.Images.SetKeyName(270, "Mail-Out Box-2.ico")
        Me.ImageList1.Images.SetKeyName(271, "Mail-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(272, "Mail-Reply To All.ico")
        Me.ImageList1.Images.SetKeyName(273, "Mail-Reply.ico")
        Me.ImageList1.Images.SetKeyName(274, "Mail-Search.ico")
        Me.ImageList1.Images.SetKeyName(275, "Mail-Send and Receive.ico")
        Me.ImageList1.Images.SetKeyName(276, "Media.ico")
        Me.ImageList1.Images.SetKeyName(277, "Microphone.ico")
        Me.ImageList1.Images.SetKeyName(278, "Midi Keyboard.ico")
        Me.ImageList1.Images.SetKeyName(279, "Minus.ico")
        Me.ImageList1.Images.SetKeyName(280, "mm-Eject.ico")
        Me.ImageList1.Images.SetKeyName(281, "mm-Fast Forward.ico")
        Me.ImageList1.Images.SetKeyName(282, "mm-First.ico")
        Me.ImageList1.Images.SetKeyName(283, "mm-Last.ico")
        Me.ImageList1.Images.SetKeyName(284, "mm-Pause.ico")
        Me.ImageList1.Images.SetKeyName(285, "mm-Play.ico")
        Me.ImageList1.Images.SetKeyName(286, "mm-Rewind.ico")
        Me.ImageList1.Images.SetKeyName(287, "mm-Stop.ico")
        Me.ImageList1.Images.SetKeyName(288, "Monitor.ico")
        Me.ImageList1.Images.SetKeyName(289, "Movie.ico")
        Me.ImageList1.Images.SetKeyName(290, "Musical Note-2.ico")
        Me.ImageList1.Images.SetKeyName(291, "Musical Note.ico")
        Me.ImageList1.Images.SetKeyName(292, "New.ico")
        Me.ImageList1.Images.SetKeyName(293, "Note.ico")
        Me.ImageList1.Images.SetKeyName(294, "Note-Add.ico")
        Me.ImageList1.Images.SetKeyName(295, "Note-Delete.ico")
        Me.ImageList1.Images.SetKeyName(296, "Note-Edit.ico")
        Me.ImageList1.Images.SetKeyName(297, "Note-New.ico")
        Me.ImageList1.Images.SetKeyName(298, "Note-Next.ico")
        Me.ImageList1.Images.SetKeyName(299, "Notepad.ico")
        Me.ImageList1.Images.SetKeyName(300, "Note-Previous.ico")
        Me.ImageList1.Images.SetKeyName(301, "Note-Remove.ico")
        Me.ImageList1.Images.SetKeyName(302, "Notes.ico")
        Me.ImageList1.Images.SetKeyName(303, "Note-Search.ico")
        Me.ImageList1.Images.SetKeyName(304, "Number of Pages.ico")
        Me.ImageList1.Images.SetKeyName(305, "Numbering-2.ico")
        Me.ImageList1.Images.SetKeyName(306, "Numbering.ico")
        Me.ImageList1.Images.SetKeyName(307, "Object-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(308, "Object-Select.ico")
        Me.ImageList1.Images.SetKeyName(309, "Object-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(310, "Open.ico")
        Me.ImageList1.Images.SetKeyName(311, "Outline-2.ico")
        Me.ImageList1.Images.SetKeyName(312, "Outline.ico")
        Me.ImageList1.Images.SetKeyName(313, "Page Number.ico")
        Me.ImageList1.Images.SetKeyName(314, "Page-Break.ico")
        Me.ImageList1.Images.SetKeyName(315, "Page-Invert.ico")
        Me.ImageList1.Images.SetKeyName(316, "Page-Next.ico")
        Me.ImageList1.Images.SetKeyName(317, "Page-Previous.ico")
        Me.ImageList1.Images.SetKeyName(318, "Page-Setup.ico")
        Me.ImageList1.Images.SetKeyName(319, "Paintbrush.ico")
        Me.ImageList1.Images.SetKeyName(320, "Parcel.ico")
        Me.ImageList1.Images.SetKeyName(321, "Paste.ico")
        Me.ImageList1.Images.SetKeyName(322, "Permission.ico")
        Me.ImageList1.Images.SetKeyName(323, "Phone.ico")
        Me.ImageList1.Images.SetKeyName(324, "Planner.ico")
        Me.ImageList1.Images.SetKeyName(325, "Plus.ico")
        Me.ImageList1.Images.SetKeyName(326, "Portrait.ico")
        Me.ImageList1.Images.SetKeyName(327, "Preview.ico")
        Me.ImageList1.Images.SetKeyName(328, "Print-2.ico")
        Me.ImageList1.Images.SetKeyName(329, "Print.ico")
        Me.ImageList1.Images.SetKeyName(330, "Print-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(331, "Print-Options.ico")
        Me.ImageList1.Images.SetKeyName(332, "Procedure.ico")
        Me.ImageList1.Images.SetKeyName(333, "Process.ico")
        Me.ImageList1.Images.SetKeyName(334, "Project.ico")
        Me.ImageList1.Images.SetKeyName(335, "Project-Gant Chart.ico")
        Me.ImageList1.Images.SetKeyName(336, "Properties.ico")
        Me.ImageList1.Images.SetKeyName(337, "Push Pin.ico")
        Me.ImageList1.Images.SetKeyName(338, "Recycle Bin.ico")
        Me.ImageList1.Images.SetKeyName(339, "Redo.ico")
        Me.ImageList1.Images.SetKeyName(340, "Refresh.ico")
        Me.ImageList1.Images.SetKeyName(341, "Remove.ico")
        Me.ImageList1.Images.SetKeyName(342, "Rename.ico")
        Me.ImageList1.Images.SetKeyName(343, "Report-2.ico")
        Me.ImageList1.Images.SetKeyName(344, "Report.ico")
        Me.ImageList1.Images.SetKeyName(345, "Report-Bound.ico")
        Me.ImageList1.Images.SetKeyName(346, "Research.ico")
        Me.ImageList1.Images.SetKeyName(347, "Ruler.ico")
        Me.ImageList1.Images.SetKeyName(348, "Ruler-Formatting.ico")
        Me.ImageList1.Images.SetKeyName(349, "Save All.ico")
        Me.ImageList1.Images.SetKeyName(350, "Save As HTML.ico")
        Me.ImageList1.Images.SetKeyName(351, "Save As.ico")
        Me.ImageList1.Images.SetKeyName(352, "Save Draft.ico")
        Me.ImageList1.Images.SetKeyName(353, "Save.ico")
        Me.ImageList1.Images.SetKeyName(354, "Script.ico")
        Me.ImageList1.Images.SetKeyName(355, "Search.ico")
        Me.ImageList1.Images.SetKeyName(356, "Search-Next.ico")
        Me.ImageList1.Images.SetKeyName(357, "Search-Previous.ico")
        Me.ImageList1.Images.SetKeyName(358, "Search-Replace.ico")
        Me.ImageList1.Images.SetKeyName(359, "Select All.ico")
        Me.ImageList1.Images.SetKeyName(360, "Slide Show.ico")
        Me.ImageList1.Images.SetKeyName(361, "Slide.ico")
        Me.ImageList1.Images.SetKeyName(362, "Software-Analyse.ico")
        Me.ImageList1.Images.SetKeyName(363, "Sort-Ascending.ico")
        Me.ImageList1.Images.SetKeyName(364, "Sort-Descending.ico")
        Me.ImageList1.Images.SetKeyName(365, "Sound-Delete.ico")
        Me.ImageList1.Images.SetKeyName(366, "Sound-Fade In.ico")
        Me.ImageList1.Images.SetKeyName(367, "Sound-Fade Out.ico")
        Me.ImageList1.Images.SetKeyName(368, "Sound-Join.ico")
        Me.ImageList1.Images.SetKeyName(369, "Sound-Mark End.ico")
        Me.ImageList1.Images.SetKeyName(370, "Sound-Mark Start.ico")
        Me.ImageList1.Images.SetKeyName(371, "Sound-Split.ico")
        Me.ImageList1.Images.SetKeyName(372, "Spell Check.ico")
        Me.ImageList1.Images.SetKeyName(373, "Stop.ico")
        Me.ImageList1.Images.SetKeyName(374, "Strikeout.ico")
        Me.ImageList1.Images.SetKeyName(375, "Subscript.ico")
        Me.ImageList1.Images.SetKeyName(376, "Sum.ico")
        Me.ImageList1.Images.SetKeyName(377, "Summary.ico")
        Me.ImageList1.Images.SetKeyName(378, "Superscript.ico")
        Me.ImageList1.Images.SetKeyName(379, "Support.ico")
        Me.ImageList1.Images.SetKeyName(380, "Tables and Borders.ico")
        Me.ImageList1.Images.SetKeyName(381, "Task.ico")
        Me.ImageList1.Images.SetKeyName(382, "Task-Search.ico")
        Me.ImageList1.Images.SetKeyName(383, "Thesaurus.ico")
        Me.ImageList1.Images.SetKeyName(384, "Tick.ico")
        Me.ImageList1.Images.SetKeyName(385, "Time Line.ico")
        Me.ImageList1.Images.SetKeyName(386, "Tip.ico")
        Me.ImageList1.Images.SetKeyName(387, "To Do List.ico")
        Me.ImageList1.Images.SetKeyName(388, "To Lowercase.ico")
        Me.ImageList1.Images.SetKeyName(389, "To Uppercase.ico")
        Me.ImageList1.Images.SetKeyName(390, "Tools.ico")
        Me.ImageList1.Images.SetKeyName(391, "Tree View.ico")
        Me.ImageList1.Images.SetKeyName(392, "Tree View-Add.ico")
        Me.ImageList1.Images.SetKeyName(393, "Tree View-Delete.ico")
        Me.ImageList1.Images.SetKeyName(394, "Tree View-Edit.ico")
        Me.ImageList1.Images.SetKeyName(395, "Tree View-New.ico")
        Me.ImageList1.Images.SetKeyName(396, "Tree View-Remove.ico")
        Me.ImageList1.Images.SetKeyName(397, "Tree View-Search.ico")
        Me.ImageList1.Images.SetKeyName(398, "Underline.ico")
        Me.ImageList1.Images.SetKeyName(399, "Undo.ico")
        Me.ImageList1.Images.SetKeyName(400, "Unlock.ico")
        Me.ImageList1.Images.SetKeyName(401, "User-2.ico")
        Me.ImageList1.Images.SetKeyName(402, "User.ico")
        Me.ImageList1.Images.SetKeyName(403, "User-Add-2.ico")
        Me.ImageList1.Images.SetKeyName(404, "User-Add.ico")
        Me.ImageList1.Images.SetKeyName(405, "User-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(406, "User-Delete.ico")
        Me.ImageList1.Images.SetKeyName(407, "User-Edit-2.ico")
        Me.ImageList1.Images.SetKeyName(408, "User-Edit.ico")
        Me.ImageList1.Images.SetKeyName(409, "User-New-2.ico")
        Me.ImageList1.Images.SetKeyName(410, "User-New.ico")
        Me.ImageList1.Images.SetKeyName(411, "User-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(412, "User-Options.ico")
        Me.ImageList1.Images.SetKeyName(413, "User-Password-2.ico")
        Me.ImageList1.Images.SetKeyName(414, "User-Password.ico")
        Me.ImageList1.Images.SetKeyName(415, "User-Remove-2.ico")
        Me.ImageList1.Images.SetKeyName(416, "User-Remove.ico")
        Me.ImageList1.Images.SetKeyName(417, "Users-2.ico")
        Me.ImageList1.Images.SetKeyName(418, "Users.ico")
        Me.ImageList1.Images.SetKeyName(419, "User-Search-2.ico")
        Me.ImageList1.Images.SetKeyName(420, "User-Search.ico")
        Me.ImageList1.Images.SetKeyName(421, "User-Security-2.ico")
        Me.ImageList1.Images.SetKeyName(422, "User-Security.ico")
        Me.ImageList1.Images.SetKeyName(423, "View-Details.ico")
        Me.ImageList1.Images.SetKeyName(424, "View-Large Icons.ico")
        Me.ImageList1.Images.SetKeyName(425, "View-List.ico")
        Me.ImageList1.Images.SetKeyName(426, "View-One Page.ico")
        Me.ImageList1.Images.SetKeyName(427, "View-Page Width.ico")
        Me.ImageList1.Images.SetKeyName(428, "Views.ico")
        Me.ImageList1.Images.SetKeyName(429, "View-Small Icons.ico")
        Me.ImageList1.Images.SetKeyName(430, "Volume.ico")
        Me.ImageList1.Images.SetKeyName(431, "Volume-Mute.ico")
        Me.ImageList1.Images.SetKeyName(432, "Warning.ico")
        Me.ImageList1.Images.SetKeyName(433, "Waste Bin.ico")
        Me.ImageList1.Images.SetKeyName(434, "Window.ico")
        Me.ImageList1.Images.SetKeyName(435, "Window-Add.ico")
        Me.ImageList1.Images.SetKeyName(436, "Window-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(437, "Window-Cascade.ico")
        Me.ImageList1.Images.SetKeyName(438, "Window-Close.ico")
        Me.ImageList1.Images.SetKeyName(439, "Window-Delete.ico")
        Me.ImageList1.Images.SetKeyName(440, "Window-Edit.ico")
        Me.ImageList1.Images.SetKeyName(441, "Window-Maximise.ico")
        Me.ImageList1.Images.SetKeyName(442, "Window-Minimise.ico")
        Me.ImageList1.Images.SetKeyName(443, "Window-New.ico")
        Me.ImageList1.Images.SetKeyName(444, "Window-Next.ico")
        Me.ImageList1.Images.SetKeyName(445, "Window-Options.ico")
        Me.ImageList1.Images.SetKeyName(446, "Window-Preview.ico")
        Me.ImageList1.Images.SetKeyName(447, "Window-Previous.ico")
        Me.ImageList1.Images.SetKeyName(448, "Window-Properties.ico")
        Me.ImageList1.Images.SetKeyName(449, "Window-Remove.ico")
        Me.ImageList1.Images.SetKeyName(450, "Window-Search.ico")
        Me.ImageList1.Images.SetKeyName(451, "Window-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(452, "Window-Split.ico")
        Me.ImageList1.Images.SetKeyName(453, "Window-Tile Horizontal.ico")
        Me.ImageList1.Images.SetKeyName(454, "Window-Tile Vertical.ico")
        Me.ImageList1.Images.SetKeyName(455, "Wizard.ico")
        Me.ImageList1.Images.SetKeyName(456, "Word Count.ico")
        Me.ImageList1.Images.SetKeyName(457, "Word Wrap-2.ico")
        Me.ImageList1.Images.SetKeyName(458, "Word Wrap.ico")
        Me.ImageList1.Images.SetKeyName(459, "Work Sheet.ico")
        Me.ImageList1.Images.SetKeyName(460, "Work Sheet-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(461, "Work Sheet-Delete.ico")
        Me.ImageList1.Images.SetKeyName(462, "Work Sheet-New.ico")
        Me.ImageList1.Images.SetKeyName(463, "Work Sheet-Options.ico")
        Me.ImageList1.Images.SetKeyName(464, "Work Sheet-Protect.ico")
        Me.ImageList1.Images.SetKeyName(465, "Work Sheet-Rename.ico")
        Me.ImageList1.Images.SetKeyName(466, "Zoom-In.ico")
        Me.ImageList1.Images.SetKeyName(467, "Zoom-Out.ico")
        '
        'RepositoryItemImageComboBoxLAG
        '
        Me.RepositoryItemImageComboBoxLAG.AutoHeight = False
        Me.RepositoryItemImageComboBoxLAG.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemImageComboBoxLAG.Items.AddRange(New DevExpress.XtraEditors.Controls.ImageComboBoxItem() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("US", "US", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("GR", "GR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("HU", "HU", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("DE", "DE", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("AL", "AL", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("SR", "SR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("RU", "RU", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("RO", "RO", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("BG", "BG", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("TR", "TR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("FR", "FR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("ES", "ES", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("IT", "IT", -1)})
        Me.RepositoryItemImageComboBoxLAG.Name = "RepositoryItemImageComboBoxLAG"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'RepositoryItemTextEdit3
        '
        Me.RepositoryItemTextEdit3.AutoHeight = False
        Me.RepositoryItemTextEdit3.Name = "RepositoryItemTextEdit3"
        '
        'RepositoryItemSpinEdit1
        '
        Me.RepositoryItemSpinEdit1.AutoHeight = False
        Me.RepositoryItemSpinEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.RepositoryItemSpinEdit1.Name = "RepositoryItemSpinEdit1"
        '
        'gpMain
        '
        Me.gpMain.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.gpMain.AppearanceCaption.Options.UseFont = True
        Me.gpMain.CaptionImageLocation = DevExpress.Utils.GroupElementLocation.BeforeText
        Me.gpMain.Controls.Add(Me.gridControl1)
        Me.gpMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gpMain.Location = New System.Drawing.Point(0, 69)
        Me.gpMain.Name = "gpMain"
        Me.gpMain.Size = New System.Drawing.Size(927, 402)
        Me.gpMain.TabIndex = 5
        Me.gpMain.Text = " Terminals Manager"
        '
        'gridControl1
        '
        Me.gridControl1.DataSource = Me.AdminActionsViewBindingSource
        Me.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gridControl1.Location = New System.Drawing.Point(2, 33)
        Me.gridControl1.MainView = Me.gv1
        Me.gridControl1.Name = "gridControl1"
        Me.gridControl1.Size = New System.Drawing.Size(923, 367)
        Me.gridControl1.TabIndex = 5
        Me.gridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.gv1})
        '
        'AdminActionsViewBindingSource
        '
        Me.AdminActionsViewBindingSource.DataMember = "AdminActionsView"
        Me.AdminActionsViewBindingSource.DataSource = Me.DsSecurity
        '
        'DsSecurity
        '
        Me.DsSecurity.DataSetName = "dsSecurity"
        Me.DsSecurity.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'gv1
        '
        Me.gv1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colActionDescription, Me.colAdminActionsId, Me.colDate_Time, Me.colProfileIdAffected, Me.colSystemUserId, Me.colProfileLoginName, Me.colUserLoginName})
        Me.gv1.GridControl = Me.gridControl1
        Me.gv1.Name = "gv1"
        Me.gv1.OptionsBehavior.Editable = False
        Me.gv1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDate_Time, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colActionDescription
        '
        Me.colActionDescription.FieldName = "ActionDescription"
        Me.colActionDescription.Name = "colActionDescription"
        Me.colActionDescription.Visible = True
        Me.colActionDescription.VisibleIndex = 2
        '
        'colAdminActionsId
        '
        Me.colAdminActionsId.FieldName = "AdminActionsId"
        Me.colAdminActionsId.Name = "colAdminActionsId"
        '
        'colDate_Time
        '
        Me.colDate_Time.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"
        Me.colDate_Time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colDate_Time.FieldName = "Date_Time"
        Me.colDate_Time.Name = "colDate_Time"
        Me.colDate_Time.Visible = True
        Me.colDate_Time.VisibleIndex = 0
        '
        'colProfileIdAffected
        '
        Me.colProfileIdAffected.FieldName = "ProfileIdAffected"
        Me.colProfileIdAffected.Name = "colProfileIdAffected"
        '
        'colSystemUserId
        '
        Me.colSystemUserId.FieldName = "SystemUserId"
        Me.colSystemUserId.Name = "colSystemUserId"
        Me.colSystemUserId.Visible = True
        Me.colSystemUserId.VisibleIndex = 3
        '
        'colProfileLoginName
        '
        Me.colProfileLoginName.FieldName = "ProfileLoginName"
        Me.colProfileLoginName.Name = "colProfileLoginName"
        Me.colProfileLoginName.Visible = True
        Me.colProfileLoginName.VisibleIndex = 1
        '
        'colUserLoginName
        '
        Me.colUserLoginName.FieldName = "UserLoginName"
        Me.colUserLoginName.Name = "colUserLoginName"
        Me.colUserLoginName.Visible = True
        Me.colUserLoginName.VisibleIndex = 4
        '
        'Bar1
        '
        Me.Bar1.BarName = "Status bar"
        Me.Bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Status bar"
        '
        'BarButtonItemEditSendingProfile
        '
        Me.BarButtonItemEditSendingProfile.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.BarButtonItemEditSendingProfile.Caption = "Edit Sending Profile"
        Me.BarButtonItemEditSendingProfile.Id = 0
        Me.BarButtonItemEditSendingProfile.Name = "BarButtonItemEditSendingProfile"
        '
        'frmAdminActionsList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(927, 493)
        Me.Controls.Add(Me.gpMain)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Name = "frmAdminActionsList"
        Me.Text = "frmSysMessages"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSpinEditYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSpinEditMonth, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemImageComboBoxLAG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gpMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpMain.ResumeLayout(False)
        CType(Me.gridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdminActionsViewBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsSecurity, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents BarAddNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarOpen As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarDelete As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarRefresh As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents RepositoryItemImageComboBoxLAG As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents gpMain As DevExpress.XtraEditors.GroupControl
    Friend WithEvents gridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents gv1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
    Friend WithEvents BarButtonItemEditProfile As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemEditSendingProfile As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Bar4 As DevExpress.XtraBars.Bar
    Friend WithEvents BarEditItemDateFrom As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents BarEditItemDateTo As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents BarButtonItemToday As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemYesterday As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemTwoDaysAgo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemLastSevenDays As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarEditItemSpinMonth As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemSpinEditMonth As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents BarEditItemSpinYear As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemSpinEditYear As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemSpinEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents BarButtonItemSpinGo As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents DsSecurity As Dating.Client.Admin.APP.AdminWS.dsSecurity
    Friend WithEvents colActionDescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAdminActionsId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDate_Time As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colProfileIdAffected As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSystemUserId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AdminActionsViewBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents colProfileLoginName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUserLoginName As DevExpress.XtraGrid.Columns.GridColumn
End Class

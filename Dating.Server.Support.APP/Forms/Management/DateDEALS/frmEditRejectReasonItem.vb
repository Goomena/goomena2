﻿Imports System.ComponentModel

Public Class frmEditRejectReasonItem

    Public MessageIDEdit As Integer
    Public IsOK As Boolean = False

    Private Sub frmSitePageCustomItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IsOK = False
    End Sub

    Private Sub UpdateToHTML(ByVal LAGID As String)
        Try
            'Select Case LAGID
            '    Case "US"
            '        UchtmlEditor1.HTMLBody = Library.Public.Common.CheckNULL(SYS_MessagesBindingSource.Current("US"), "")
            '        UchtmlEditor1.MessageID = Library.Public.Common.CheckNULL(SYS_MessagesBindingSource.Current("MessageID"), "")
            '        UchtmlEditor1.PageID = Library.Public.Common.CheckNULL(SYS_MessagesBindingSource.Current("SitePageID"), "")
            '        UchtmlEditor1.FileName = Library.Public.Common.CheckNULL(SYS_MessagesBindingSource.Current("MessageKey"), "")
            '    Case "GR"
            '        UchtmlEditor1.HTMLBody = Library.Public.Common.CheckNULL(SYS_MessagesBindingSource.Current("GR"), "")
            '    Case "HU"
            '        UchtmlEditor1.HTMLBody = Library.Public.Common.CheckNULL(SYS_MessagesBindingSource.Current("HU"), "")
            '    Case "ES"
            '        UchtmlEditor1.HTMLBody = Library.Public.Common.CheckNULL(SYS_MessagesBindingSource.Current("ES"), "")
            'End Select

            UchtmlEditor1.HTMLBody = Library.Public.Common.CheckNULL(EUS_LISTS_RejectingReasonsBindingSource.Current(LAGID & "Text"), "")
            UchtmlEditor1.MessageID = Library.Public.Common.CheckNULL(EUS_LISTS_RejectingReasonsBindingSource.Current("MessageID"), "")
            'UchtmlEditor1.PageID = Library.Public.Common.CheckNULL(EUS_LISTS_RejectingReasonsBindingSource.Current("SitePageID"), "")
            'UchtmlEditor1.FileName = Globals.GetFileName(Library.Public.Common.CheckNULL(EUS_LISTS_RejectingReasonsBindingSource.Current("MessageKey"), ""))

            ' select by default tabdesign
            'UchtmlEditor1.TABDesign.Focus()

            UchtmlEditor1.RefreshEditor()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub UpdateFromHTML(ByVal LAGID As String)
        Try

            UchtmlEditor1.RefreshEditorToDB()
            Select Case cbLAG.EditValue
                Case "US"
                    EUS_LISTS_RejectingReasonsBindingSource.Current("US") = UchtmlEditor1.HTMLBody
                Case "GR"
                    EUS_LISTS_RejectingReasonsBindingSource.Current("GR") = UchtmlEditor1.HTMLBody
                Case "HU"
                    EUS_LISTS_RejectingReasonsBindingSource.Current("HU") = UchtmlEditor1.HTMLBody
                Case "ES"
                    EUS_LISTS_RejectingReasonsBindingSource.Current("ES") = UchtmlEditor1.HTMLBody
            End Select
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub cbLAG_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles cbLAG.EditValueChanging
        Try
            UpdateFromHTML(e.OldValue)
            UpdateToHTML(e.NewValue)
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub Save()
        Try
            UpdateFromHTML(cbLAG.EditValue)
            Me.Validate()
            Me.EUS_LISTS_RejectingReasonsBindingSource.EndEdit()
            ' Me.TableAdapterManager.UpdateAll(Me.UniCMSDB)

            Dim WebErr As New AdminWS.clsDataRecordErrorsReturn
            WebErr = CheckWebServiceCallResult(gAdminWS.UpdateSYS_Messages(gAdminWSHeaders, Me.DSLists))
            If WebErr.HasErrors Then
                Exit Sub
            End If
            RefreshGrid()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub Load(ByVal MessageID As Integer)
        Try
            MessageIDEdit = MessageID
            RefreshGrid()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub RefreshGrid()
        Try


            Dim WebErr As New AdminWS.clsDataRecordErrorsReturn
            If (MessageIDEdit > 0) Then
                WebErr = CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_RejectingReasons(gAdminWSHeaders, MessageIDEdit, Me.DSLists))
            Else
                WebErr = CheckWebServiceCallResult(gAdminWS.GetSYS_Messages(gAdminWSHeaders, Me.DSLists))
            End If

            If WebErr.HasErrors Then
                Exit Sub
            End If

            EUS_LISTS_RejectingReasonsBindingSource.DataSource = Me.DSLists.EUS_LISTS_RejectingReasons

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Public Sub ADD(ByVal SitePageID As Integer, ByVal ActiveLAG As String)
        Try
            Dim MessageID As Integer = 0
            cbLAG.EditValue = ActiveLAG
            Load(MessageID)
            Me.EUS_LISTS_RejectingReasonsBindingSource.AddNew()
            EUS_LISTS_RejectingReasonsBindingSource.Current("SitePageID") = SitePageID
            EUS_LISTS_RejectingReasonsBindingSource.Current("MessageKey") = ""
            EUS_LISTS_RejectingReasonsBindingSource.Current("US") = ""
            UpdateToHTML(cbLAG.EditValue)

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Public Sub EDIT(ByVal MessageID As Integer, ByVal SitePageID As Integer, ByVal ActiveLAG As String)
        Try
            cbLAG.EditValue = ActiveLAG
            Load(MessageID)
            '  Me.SYS_MessagesBindingSource.AddNew()
            '            SYS_MessagesBindingSource.Current("SitePageID") = SitePageID
            UpdateToHTML(cbLAG.EditValue)
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Public Sub DELETE(ByVal MessageID As Integer)
        Try
            Load(MessageID)
            Dim sz As String = "Deleted " & Me.EUS_LISTS_RejectingReasonsBindingSource.Current("MessageKey")
            Me.EUS_LISTS_RejectingReasonsBindingSource.RemoveCurrent()
            Me.Validate()
            Me.EUS_LISTS_RejectingReasonsBindingSource.EndEdit()
            Save()
            MsgBox(sz)
            IsOK = True
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdOK.Click
        Try
            Save()
            IsOK = True
            Close()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdCancel.Click
        Try
            IsOK = False
            Close()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub CmdSAVE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdSAVE.Click
        Try
            Save()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try

    End Sub

End Class
﻿Imports DevExpress.XtraEditors.Controls

Public Class frmDDProfileRejecting
    Public Done As Boolean = False

    Private Sub frmDDRejectingProfile_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            PergormReLogin()
            CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_RejectingReasons(gLoginRec.Token, Me.DSLists))
            ClsCombos.FillComboUsingDatatable(Me.DSLists.EUS_LISTS_RejectingReasons, "US", "RejectingReasonsId", icbRejectingReasonID, True)
            icbRejectingReasonID.SelectedIndex = 0
            LoadText()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub icbRejectingReasonID_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles icbRejectingReasonID.SelectedIndexChanged
        LoadText()
    End Sub


    Private Sub LoadText()

        For Each dr As DataRow In Me.DSLists.EUS_LISTS_RejectingReasons.Rows
            If (DirectCast(icbRejectingReasonID.SelectedItem, ImageComboBoxItem).Value = dr("RejectingReasonsId")) Then
                txRejectingReasonText.Text = dr("Text" & "US").ToString()
                Exit For
            End If
        Next

    End Sub


    Private Sub btnDone_Click(sender As System.Object, e As System.EventArgs) Handles btnDone.Click
        'If (Me.icbRejectingReasonID.SelectedIndex > 0) Then
        Done = True
        'End If
        Me.Hide()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Done = False
        Me.Close()
    End Sub

    Public Function GetReason() As String
        'If (Me.icbRejectingReasonID.SelectedIndex > 0) Then
        Return DirectCast(Me.icbRejectingReasonID.SelectedItem, ImageComboBoxItem).Description
        'End If
        'Return Nothing
    End Function


    Public Function GetReasonText() As String
        'If (Me.icbRejectingReasonID.SelectedIndex > 0) Then
        Return txRejectingReasonText.Text
        'End If
        'Return Nothing
    End Function
End Class
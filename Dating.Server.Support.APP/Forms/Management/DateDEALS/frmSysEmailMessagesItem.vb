﻿Imports System.ComponentModel

Public Class frmSysEmailMessagesItem

    Public MessageIDEdit As Integer
    Public IsOK As Boolean = False

    Private Sub frmSitePageCustomItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IsOK = False
    End Sub

    Private Sub UpdateToHTML(ByVal LAGID As String)
        Try

            UchtmlEditor1.HTMLBody = Library.Public.Common.CheckNULL(SYS_EmailMessagesBindingSource.Current("MessageText" & LAGID), "")
            UchtmlEditor1.MessageID = Library.Public.Common.CheckNULL(SYS_EmailMessagesBindingSource.Current("MessageID"), "")
            UchtmlEditor1.PageID = Library.Public.Common.CheckNULL(SYS_EmailMessagesBindingSource.Current("MessageID"), "")
            UchtmlEditor1.FileName = Globals.GetFileName(Library.Public.Common.CheckNULL(SYS_EmailMessagesBindingSource.Current("SubjectUS"), ""))

            ' select by default tabdesign
            'UchtmlEditor1.TABDesign.Focus()

            UchtmlEditor1.RefreshEditor()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub UpdateFromHTML(ByVal LAGID As String)
        Try

            UchtmlEditor1.RefreshEditorToDB()
            SYS_EmailMessagesBindingSource.Current("MessageText" & LAGID) = UchtmlEditor1.HTMLBody
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub cbLAG_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles cbLAG.EditValueChanging
        Try

            UpdateFromHTML(e.OldValue)
            UpdateToHTML(e.NewValue)

            SYS_EmailMessagesBindingSource.Current("Subject" & e.OldValue) = txMessageSubject.Text
            txMessageSubject.Text = SYS_EmailMessagesBindingSource.Current("Subject" & e.NewValue).ToString()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub Save()
        Try

            If (Me.SYS_EmailMessagesBindingSource.Current("SiteID") Is Nothing OrElse Me.SYS_EmailMessagesBindingSource.Current("SiteID") Is System.DBNull.Value) Then
                Me.SYS_EmailMessagesBindingSource.Current("SiteID") = -1
            End If

            UpdateFromHTML(cbLAG.EditValue)
            Me.Validate()
            Me.SYS_EmailMessagesBindingSource.EndEdit()

            Dim WebErr As New AdminWS.clsDataRecordErrorsReturn
            WebErr = CheckWebServiceCallResult(gAdminWS.UpdateSYS_EmailMessages(gAdminWSHeaders, Me.UniCMSDB))
            If WebErr.HasErrors Then
                Exit Sub
            End If
            LoadDataSource()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub Load(ByVal MessageID As Integer)
        Try
            MessageIDEdit = MessageID
            LoadDataSource()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub LoadDataSource()
        Try
            Dim WebErr As New AdminWS.clsDataRecordErrorsReturn
            If (MessageIDEdit > 0) Then
                WebErr = CheckWebServiceCallResult(gAdminWS.GetSYS_EmailMessagesByMessageID(gAdminWSHeaders, Me.UniCMSDB, MessageIDEdit))
            Else
                WebErr = CheckWebServiceCallResult(gAdminWS.GetSYS_EmailMessages(gAdminWSHeaders, Me.UniCMSDB))
            End If

            If WebErr.HasErrors Then
                Exit Sub
            End If


            SYS_EmailMessagesBindingSource.DataSource = Me.UniCMSDB.SYS_EmailMessages

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub



    Public Sub ADD(ByVal MessageID As Integer, ByVal ActiveLAG As String)
        Try
            txMessageAction.Enabled = True
            Load(MessageID)

            SYS_EmailMessagesBindingSource.AddNew()
            SYS_EmailMessagesBindingSource.Current("MessageID") = MessageID
            SYS_EmailMessagesBindingSource.Current("SubjectUS") = ""
            SYS_EmailMessagesBindingSource.Current("MessageTextUS") = ""
            'SYS_EmailMessagesBindingSource.Current("USText") = ""

            UpdateToHTML(cbLAG.EditValue)
            cbLAG.EditValue = ActiveLAG
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Public Sub EDIT(ByVal MessageID As Integer, ByVal ActiveLAG As String)
        Try
            Load(MessageID)
            UpdateToHTML(cbLAG.EditValue)
            txMessageSubject.Text = SYS_EmailMessagesBindingSource.Current("Subject" & cbLAG.EditValue).ToString()
            txMessageAction.Text = SYS_EmailMessagesBindingSource.Current("MessageAction").ToString()
            txtPaymentMethod.Text = SYS_EmailMessagesBindingSource.Current("PaymentMethod").ToString()
            cbLAG.EditValue = ActiveLAG
            'UpdateToHTML(ActiveLAG)
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Public Sub DELETE(ByVal MessageID As Integer)
        Try
            Load(MessageID)
            Dim sz As String = "Deleted " & Me.SYS_EmailMessagesBindingSource.Current("SubjectUS")
            Me.SYS_EmailMessagesBindingSource.RemoveCurrent()
            Me.Validate()
            Me.SYS_EmailMessagesBindingSource.EndEdit()
            Save()
            MsgBox(sz)
            IsOK = True
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdOK.Click
        Try
            Save()
            IsOK = True
            Close()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdCancel.Click
        Try
            IsOK = False
            Close()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub CmdSAVE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdSAVE.Click
        Try
            Save()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try

    End Sub

End Class
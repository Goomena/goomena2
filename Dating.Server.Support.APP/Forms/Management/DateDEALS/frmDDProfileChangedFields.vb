﻿Public Class frmDDProfileChangedFields 

    Public IsNewValueApproved As Boolean = False

    Public Sub LoadControl(newValue As String, oldValue As String)
        Me.memoNewValue.Text = newValue
        Me.memoOldValue.Text = oldValue
    End Sub

    Public Function GetUpdatedNewValue() As String
        Return Me.memoNewValue.Text
    End Function

    Private Sub cmdSave1_Click(sender As System.Object, e As System.EventArgs) Handles cmdSave1.Click
        IsNewValueApproved = True
        Me.Close()
    End Sub

    Private Sub cmdCancel1_Click(sender As System.Object, e As System.EventArgs) Handles cmdCancel1.Click
        Me.Close()
    End Sub


End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDDManagerProfiles
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDDManagerProfiles))
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BarAddNEW = New DevExpress.XtraBars.BarButtonItem()
        Me.BarEDIT = New DevExpress.XtraBars.BarButtonItem()
        Me.BarDELETE = New DevExpress.XtraBars.BarButtonItem()
        Me.BarRefresh = New DevExpress.XtraBars.BarButtonItem()
        Me.BarProfilesFilter = New DevExpress.XtraBars.BarSubItem()
        Me.bciShowMasterRecords = New DevExpress.XtraBars.BarCheckItem()
        Me.bciShowNonMasterRecords = New DevExpress.XtraBars.BarCheckItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.gpMain = New DevExpress.XtraEditors.GroupControl()
        Me.GridControlProfiles = New DevExpress.XtraGrid.GridControl()
        Me.EUS_ProfilesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DSMembers = New Dating.Client.Admin.APP.AdminWS.DSMembers()
        Me.GridViewProfiles = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colStatus = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLoginName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGenderId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemImageComboBox_GenderId = New DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox()
        Me.colCountry = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colRegion = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCity = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colZip = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coleMail = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCellular = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAboutMe_Heading = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAboutMe_DescribeYourself = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAboutMe_DescribeAnIdealFirstDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateTimeToRegister = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastLoginDateTime = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastUpdateProfileDateTime = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBirthday = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNotificationsSettings_WhenNewMembersNearMe = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.BarCheckItem2 = New DevExpress.XtraBars.BarCheckItem()
        Me.RepositoryItemImageComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox()
        Me.BarCheckItem1 = New DevExpress.XtraBars.BarCheckItem()
        Me.DSLists = New Dating.Client.Admin.APP.AdminWS.DSLists()
        Me.EUS_LISTS_GenderBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gpMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpMain.SuspendLayout()
        CType(Me.GridControlProfiles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_ProfilesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSMembers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewProfiles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemImageComboBox_GenderId, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemImageComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_LISTS_GenderBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me.gpMain
        Me.BarManager1.Images = Me.ImageList1
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarRefresh, Me.BarEDIT, Me.BarAddNEW, Me.BarDELETE, Me.BarProfilesFilter, Me.BarCheckItem2, Me.bciShowMasterRecords, Me.bciShowNonMasterRecords})
        Me.BarManager1.MaxItemId = 15
        Me.BarManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemImageComboBox1})
        '
        'Bar1
        '
        Me.Bar1.BarAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.Bar1.BarAppearance.Normal.Options.UseFont = True
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarAddNEW), New DevExpress.XtraBars.LinkPersistInfo(Me.BarEDIT), New DevExpress.XtraBars.LinkPersistInfo(Me.BarDELETE), New DevExpress.XtraBars.LinkPersistInfo(Me.BarRefresh), New DevExpress.XtraBars.LinkPersistInfo(Me.BarProfilesFilter)})
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'BarAddNEW
        '
        Me.BarAddNEW.Caption = "Add New"
        Me.BarAddNEW.Id = 5
        Me.BarAddNEW.ImageIndex = 100
        Me.BarAddNEW.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.BarAddNEW.ItemAppearance.Normal.Options.UseFont = True
        Me.BarAddNEW.Name = "BarAddNEW"
        Me.BarAddNEW.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarEDIT
        '
        Me.BarEDIT.Caption = "Edit"
        Me.BarEDIT.Id = 4
        Me.BarEDIT.ImageIndex = 103
        Me.BarEDIT.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.BarEDIT.ItemAppearance.Normal.Options.UseFont = True
        Me.BarEDIT.Name = "BarEDIT"
        Me.BarEDIT.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarDELETE
        '
        Me.BarDELETE.Caption = "Delete"
        Me.BarDELETE.Id = 6
        Me.BarDELETE.ImageIndex = 101
        Me.BarDELETE.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.BarDELETE.ItemAppearance.Normal.Options.UseFont = True
        Me.BarDELETE.Name = "BarDELETE"
        Me.BarDELETE.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarRefresh
        '
        Me.BarRefresh.Caption = "Refresh"
        Me.BarRefresh.Id = 3
        Me.BarRefresh.ImageIndex = 109
        Me.BarRefresh.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.BarRefresh.ItemAppearance.Normal.Options.UseFont = True
        Me.BarRefresh.Name = "BarRefresh"
        Me.BarRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarProfilesFilter
        '
        Me.BarProfilesFilter.Caption = "[Select]"
        Me.BarProfilesFilter.Id = 11
        Me.BarProfilesFilter.ImageIndex = 28
        Me.BarProfilesFilter.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BarProfilesFilter.ItemAppearance.Normal.Options.UseFont = True
        Me.BarProfilesFilter.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.bciShowMasterRecords), New DevExpress.XtraBars.LinkPersistInfo(Me.bciShowNonMasterRecords)})
        Me.BarProfilesFilter.Name = "BarProfilesFilter"
        Me.BarProfilesFilter.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'bciShowMasterRecords
        '
        Me.bciShowMasterRecords.Caption = "Show Master Records"
        Me.bciShowMasterRecords.Id = 13
        Me.bciShowMasterRecords.Name = "bciShowMasterRecords"
        '
        'bciShowNonMasterRecords
        '
        Me.bciShowNonMasterRecords.Caption = "Show Non Master Profiles"
        Me.bciShowNonMasterRecords.Id = 14
        Me.bciShowNonMasterRecords.Name = "bciShowNonMasterRecords"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(2, 33)
        Me.barDockControlTop.Size = New System.Drawing.Size(890, 39)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(2, 449)
        Me.barDockControlBottom.Size = New System.Drawing.Size(890, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(2, 72)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 377)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(892, 72)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 377)
        '
        'gpMain
        '
        Me.gpMain.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.gpMain.AppearanceCaption.Options.UseFont = True
        Me.gpMain.CaptionImageLocation = DevExpress.Utils.GroupElementLocation.BeforeText
        Me.gpMain.Controls.Add(Me.GridControlProfiles)
        Me.gpMain.Controls.Add(Me.barDockControlLeft)
        Me.gpMain.Controls.Add(Me.barDockControlRight)
        Me.gpMain.Controls.Add(Me.barDockControlBottom)
        Me.gpMain.Controls.Add(Me.barDockControlTop)
        Me.gpMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gpMain.Location = New System.Drawing.Point(0, 0)
        Me.gpMain.Name = "gpMain"
        Me.gpMain.Size = New System.Drawing.Size(894, 451)
        Me.gpMain.TabIndex = 4
        Me.gpMain.Text = " Date Deals Profiles"
        '
        'GridControlProfiles
        '
        Me.GridControlProfiles.DataSource = Me.EUS_ProfilesBindingSource
        Me.GridControlProfiles.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlProfiles.Location = New System.Drawing.Point(2, 72)
        Me.GridControlProfiles.MainView = Me.GridViewProfiles
        Me.GridControlProfiles.MenuManager = Me.BarManager1
        Me.GridControlProfiles.Name = "GridControlProfiles"
        Me.GridControlProfiles.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemImageComboBox_GenderId})
        Me.GridControlProfiles.Size = New System.Drawing.Size(890, 377)
        Me.GridControlProfiles.TabIndex = 5
        Me.GridControlProfiles.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewProfiles})
        '
        'EUS_ProfilesBindingSource
        '
        Me.EUS_ProfilesBindingSource.DataMember = "EUS_Profiles"
        Me.EUS_ProfilesBindingSource.DataSource = Me.DSMembers
        Me.EUS_ProfilesBindingSource.Sort = "DateTimeToRegister desc, LoginName asc"
        '
        'DSMembers
        '
        Me.DSMembers.DataSetName = "DSMembers"
        Me.DSMembers.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridViewProfiles
        '
        Me.GridViewProfiles.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colStatus, Me.colLoginName, Me.colGenderId, Me.colCountry, Me.colRegion, Me.colCity, Me.colZip, Me.coleMail, Me.colCellular, Me.colAboutMe_Heading, Me.colAboutMe_DescribeYourself, Me.colAboutMe_DescribeAnIdealFirstDate, Me.colDateTimeToRegister, Me.colLastLoginDateTime, Me.colLastUpdateProfileDateTime, Me.colBirthday, Me.colNotificationsSettings_WhenNewMembersNearMe})
        Me.GridViewProfiles.GridControl = Me.GridControlProfiles
        Me.GridViewProfiles.Name = "GridViewProfiles"
        Me.GridViewProfiles.OptionsBehavior.AllowIncrementalSearch = True
        Me.GridViewProfiles.OptionsBehavior.Editable = False
        Me.GridViewProfiles.OptionsBehavior.ReadOnly = True
        Me.GridViewProfiles.OptionsView.ColumnAutoWidth = False
        Me.GridViewProfiles.OptionsView.EnableAppearanceEvenRow = True
        Me.GridViewProfiles.OptionsView.EnableAppearanceOddRow = True
        Me.GridViewProfiles.OptionsView.ShowAutoFilterRow = True
        Me.GridViewProfiles.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDateTimeToRegister, DevExpress.Data.ColumnSortOrder.Descending)})
        '
        'colStatus
        '
        Me.colStatus.FieldName = "Status"
        Me.colStatus.Name = "colStatus"
        Me.colStatus.Visible = True
        Me.colStatus.VisibleIndex = 16
        Me.colStatus.Width = 56
        '
        'colLoginName
        '
        Me.colLoginName.FieldName = "LoginName"
        Me.colLoginName.Name = "colLoginName"
        Me.colLoginName.Visible = True
        Me.colLoginName.VisibleIndex = 2
        Me.colLoginName.Width = 143
        '
        'colGenderId
        '
        Me.colGenderId.ColumnEdit = Me.RepositoryItemImageComboBox_GenderId
        Me.colGenderId.FieldName = "GenderId"
        Me.colGenderId.Name = "colGenderId"
        Me.colGenderId.Visible = True
        Me.colGenderId.VisibleIndex = 4
        '
        'RepositoryItemImageComboBox_GenderId
        '
        Me.RepositoryItemImageComboBox_GenderId.AutoHeight = False
        Me.RepositoryItemImageComboBox_GenderId.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemImageComboBox_GenderId.Name = "RepositoryItemImageComboBox_GenderId"
        '
        'colCountry
        '
        Me.colCountry.FieldName = "Country"
        Me.colCountry.Name = "colCountry"
        Me.colCountry.Visible = True
        Me.colCountry.VisibleIndex = 5
        '
        'colRegion
        '
        Me.colRegion.FieldName = "Region"
        Me.colRegion.Name = "colRegion"
        Me.colRegion.Visible = True
        Me.colRegion.VisibleIndex = 6
        '
        'colCity
        '
        Me.colCity.FieldName = "City"
        Me.colCity.Name = "colCity"
        Me.colCity.Visible = True
        Me.colCity.VisibleIndex = 7
        '
        'colZip
        '
        Me.colZip.FieldName = "Zip"
        Me.colZip.Name = "colZip"
        Me.colZip.Visible = True
        Me.colZip.VisibleIndex = 8
        '
        'coleMail
        '
        Me.coleMail.FieldName = "eMail"
        Me.coleMail.Name = "coleMail"
        Me.coleMail.Visible = True
        Me.coleMail.VisibleIndex = 3
        Me.coleMail.Width = 147
        '
        'colCellular
        '
        Me.colCellular.FieldName = "Cellular"
        Me.colCellular.Name = "colCellular"
        Me.colCellular.Visible = True
        Me.colCellular.VisibleIndex = 9
        Me.colCellular.Width = 132
        '
        'colAboutMe_Heading
        '
        Me.colAboutMe_Heading.Caption = "Heading"
        Me.colAboutMe_Heading.FieldName = "AboutMe_Heading"
        Me.colAboutMe_Heading.Name = "colAboutMe_Heading"
        Me.colAboutMe_Heading.Visible = True
        Me.colAboutMe_Heading.VisibleIndex = 10
        Me.colAboutMe_Heading.Width = 171
        '
        'colAboutMe_DescribeYourself
        '
        Me.colAboutMe_DescribeYourself.Caption = "DescribeYourself"
        Me.colAboutMe_DescribeYourself.FieldName = "AboutMe_DescribeYourself"
        Me.colAboutMe_DescribeYourself.Name = "colAboutMe_DescribeYourself"
        Me.colAboutMe_DescribeYourself.Visible = True
        Me.colAboutMe_DescribeYourself.VisibleIndex = 11
        Me.colAboutMe_DescribeYourself.Width = 183
        '
        'colAboutMe_DescribeAnIdealFirstDate
        '
        Me.colAboutMe_DescribeAnIdealFirstDate.Caption = "DescribeAnIdealFirstDate"
        Me.colAboutMe_DescribeAnIdealFirstDate.FieldName = "AboutMe_DescribeAnIdealFirstDate"
        Me.colAboutMe_DescribeAnIdealFirstDate.Name = "colAboutMe_DescribeAnIdealFirstDate"
        Me.colAboutMe_DescribeAnIdealFirstDate.Visible = True
        Me.colAboutMe_DescribeAnIdealFirstDate.VisibleIndex = 12
        Me.colAboutMe_DescribeAnIdealFirstDate.Width = 144
        '
        'colDateTimeToRegister
        '
        Me.colDateTimeToRegister.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"
        Me.colDateTimeToRegister.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colDateTimeToRegister.FieldName = "DateTimeToRegister"
        Me.colDateTimeToRegister.Name = "colDateTimeToRegister"
        Me.colDateTimeToRegister.Visible = True
        Me.colDateTimeToRegister.VisibleIndex = 0
        Me.colDateTimeToRegister.Width = 133
        '
        'colLastLoginDateTime
        '
        Me.colLastLoginDateTime.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"
        Me.colLastLoginDateTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colLastLoginDateTime.FieldName = "LastLoginDateTime"
        Me.colLastLoginDateTime.Name = "colLastLoginDateTime"
        Me.colLastLoginDateTime.Visible = True
        Me.colLastLoginDateTime.VisibleIndex = 13
        Me.colLastLoginDateTime.Width = 142
        '
        'colLastUpdateProfileDateTime
        '
        Me.colLastUpdateProfileDateTime.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"
        Me.colLastUpdateProfileDateTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colLastUpdateProfileDateTime.FieldName = "LastUpdateProfileDateTime"
        Me.colLastUpdateProfileDateTime.Name = "colLastUpdateProfileDateTime"
        Me.colLastUpdateProfileDateTime.Visible = True
        Me.colLastUpdateProfileDateTime.VisibleIndex = 1
        Me.colLastUpdateProfileDateTime.Width = 139
        '
        'colBirthday
        '
        Me.colBirthday.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.colBirthday.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colBirthday.FieldName = "Birthday"
        Me.colBirthday.Name = "colBirthday"
        Me.colBirthday.Visible = True
        Me.colBirthday.VisibleIndex = 14
        Me.colBirthday.Width = 117
        '
        'colNotificationsSettings_WhenNewMembersNearMe
        '
        Me.colNotificationsSettings_WhenNewMembersNearMe.Caption = "Autonotification Enabled"
        Me.colNotificationsSettings_WhenNewMembersNearMe.FieldName = "NotificationsSettings_WhenNewMembersNearMe"
        Me.colNotificationsSettings_WhenNewMembersNearMe.Name = "colNotificationsSettings_WhenNewMembersNearMe"
        Me.colNotificationsSettings_WhenNewMembersNearMe.Visible = True
        Me.colNotificationsSettings_WhenNewMembersNearMe.VisibleIndex = 15
        Me.colNotificationsSettings_WhenNewMembersNearMe.Width = 127
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "About.ico")
        Me.ImageList1.Images.SetKeyName(1, "Accounts.ico")
        Me.ImageList1.Images.SetKeyName(2, "Address Book.ico")
        Me.ImageList1.Images.SetKeyName(3, "Air Brush.ico")
        Me.ImageList1.Images.SetKeyName(4, "Align-Centre-2.ico")
        Me.ImageList1.Images.SetKeyName(5, "Align-Centre.ico")
        Me.ImageList1.Images.SetKeyName(6, "Align-Full-2.ico")
        Me.ImageList1.Images.SetKeyName(7, "Align-Full.ico")
        Me.ImageList1.Images.SetKeyName(8, "Align-Left-2.ico")
        Me.ImageList1.Images.SetKeyName(9, "Align-Left.ico")
        Me.ImageList1.Images.SetKeyName(10, "Align-Right-2.ico")
        Me.ImageList1.Images.SetKeyName(11, "Align-Right.ico")
        Me.ImageList1.Images.SetKeyName(12, "Arrow-Left Right.ico")
        Me.ImageList1.Images.SetKeyName(13, "Arrow-Up Down.ico")
        Me.ImageList1.Images.SetKeyName(14, "Attachment.ico")
        Me.ImageList1.Images.SetKeyName(15, "Auto Correct-Options.ico")
        Me.ImageList1.Images.SetKeyName(16, "Auto-Type.ico")
        Me.ImageList1.Images.SetKeyName(17, "Back.ico")
        Me.ImageList1.Images.SetKeyName(18, "Bar Code.ico")
        Me.ImageList1.Images.SetKeyName(19, "Bold.ico")
        Me.ImageList1.Images.SetKeyName(20, "Book-Open.ico")
        Me.ImageList1.Images.SetKeyName(21, "Books.ico")
        Me.ImageList1.Images.SetKeyName(22, "Book-Search.ico")
        Me.ImageList1.Images.SetKeyName(23, "Borders and Shading.ico")
        Me.ImageList1.Images.SetKeyName(24, "Box-Closed-2.ico")
        Me.ImageList1.Images.SetKeyName(25, "Box-Closed.ico")
        Me.ImageList1.Images.SetKeyName(26, "Box-Open-2.ico")
        Me.ImageList1.Images.SetKeyName(27, "Box-Open.ico")
        Me.ImageList1.Images.SetKeyName(28, "Bullets-2.ico")
        Me.ImageList1.Images.SetKeyName(29, "Bullets.ico")
        Me.ImageList1.Images.SetKeyName(30, "Calculator.ico")
        Me.ImageList1.Images.SetKeyName(31, "Calendar.ico")
        Me.ImageList1.Images.SetKeyName(32, "Calendar-Go To Date.ico")
        Me.ImageList1.Images.SetKeyName(33, "Calendar-Search.ico")
        Me.ImageList1.Images.SetKeyName(34, "Calendar-Select Day.ico")
        Me.ImageList1.Images.SetKeyName(35, "Calendar-Select Month.ico")
        Me.ImageList1.Images.SetKeyName(36, "Calendar-Select Week.ico")
        Me.ImageList1.Images.SetKeyName(37, "Calendar-Select Work Week.ico")
        Me.ImageList1.Images.SetKeyName(38, "Camera.ico")
        Me.ImageList1.Images.SetKeyName(39, "Camera-Photo.ico")
        Me.ImageList1.Images.SetKeyName(40, "Card File.ico")
        Me.ImageList1.Images.SetKeyName(41, "Card.ico")
        Me.ImageList1.Images.SetKeyName(42, "Card-Add.ico")
        Me.ImageList1.Images.SetKeyName(43, "Card-Delete.ico")
        Me.ImageList1.Images.SetKeyName(44, "Card-Edit.ico")
        Me.ImageList1.Images.SetKeyName(45, "Card-New.ico")
        Me.ImageList1.Images.SetKeyName(46, "Card-Next.ico")
        Me.ImageList1.Images.SetKeyName(47, "Card-Previous.ico")
        Me.ImageList1.Images.SetKeyName(48, "Card-Remove.ico")
        Me.ImageList1.Images.SetKeyName(49, "Card-Search.ico")
        Me.ImageList1.Images.SetKeyName(50, "CD-Burn.ico")
        Me.ImageList1.Images.SetKeyName(51, "CD-In Case.ico")
        Me.ImageList1.Images.SetKeyName(52, "CD-ROM.ico")
        Me.ImageList1.Images.SetKeyName(53, "Center Across Cells.ico")
        Me.ImageList1.Images.SetKeyName(54, "Certificate.ico")
        Me.ImageList1.Images.SetKeyName(55, "Chart.ico")
        Me.ImageList1.Images.SetKeyName(56, "Chart-Bar.ico")
        Me.ImageList1.Images.SetKeyName(57, "Chart-Line.ico")
        Me.ImageList1.Images.SetKeyName(58, "Chart-Pie.ico")
        Me.ImageList1.Images.SetKeyName(59, "Chart-Point.ico")
        Me.ImageList1.Images.SetKeyName(60, "Clean.ico")
        Me.ImageList1.Images.SetKeyName(61, "Clear.ico")
        Me.ImageList1.Images.SetKeyName(62, "Clipboard.ico")
        Me.ImageList1.Images.SetKeyName(63, "Clock.ico")
        Me.ImageList1.Images.SetKeyName(64, "Close.ico")
        Me.ImageList1.Images.SetKeyName(65, "Colour Picker.ico")
        Me.ImageList1.Images.SetKeyName(66, "Columns-2.ico")
        Me.ImageList1.Images.SetKeyName(67, "Columns.ico")
        Me.ImageList1.Images.SetKeyName(68, "Command Prompt.ico")
        Me.ImageList1.Images.SetKeyName(69, "Compress.ico")
        Me.ImageList1.Images.SetKeyName(70, "Computer.ico")
        Me.ImageList1.Images.SetKeyName(71, "Connect.ico")
        Me.ImageList1.Images.SetKeyName(72, "Contact.ico")
        Me.ImageList1.Images.SetKeyName(73, "Contact-Add.ico")
        Me.ImageList1.Images.SetKeyName(74, "Contact-Delete.ico")
        Me.ImageList1.Images.SetKeyName(75, "Contact-Edit.ico")
        Me.ImageList1.Images.SetKeyName(76, "Contact-New.ico")
        Me.ImageList1.Images.SetKeyName(77, "Contact-Remove.ico")
        Me.ImageList1.Images.SetKeyName(78, "Contact-Search.ico")
        Me.ImageList1.Images.SetKeyName(79, "Copy.ico")
        Me.ImageList1.Images.SetKeyName(80, "Cross.ico")
        Me.ImageList1.Images.SetKeyName(81, "Currency.ico")
        Me.ImageList1.Images.SetKeyName(82, "Currency-Notes.ico")
        Me.ImageList1.Images.SetKeyName(83, "Cut.ico")
        Me.ImageList1.Images.SetKeyName(84, "Database.ico")
        Me.ImageList1.Images.SetKeyName(85, "Database-Add.ico")
        Me.ImageList1.Images.SetKeyName(86, "Database-Close.ico")
        Me.ImageList1.Images.SetKeyName(87, "Database-Delete.ico")
        Me.ImageList1.Images.SetKeyName(88, "Database-Edit.ico")
        Me.ImageList1.Images.SetKeyName(89, "Database-Filter.ico")
        Me.ImageList1.Images.SetKeyName(90, "Database-New.ico")
        Me.ImageList1.Images.SetKeyName(91, "Database-Open.ico")
        Me.ImageList1.Images.SetKeyName(92, "Database-Properties.ico")
        Me.ImageList1.Images.SetKeyName(93, "Database-Remove.ico")
        Me.ImageList1.Images.SetKeyName(94, "Database-Save.ico")
        Me.ImageList1.Images.SetKeyName(95, "Database-Schema.ico")
        Me.ImageList1.Images.SetKeyName(96, "Database-Search.ico")
        Me.ImageList1.Images.SetKeyName(97, "Database-Security.ico")
        Me.ImageList1.Images.SetKeyName(98, "Database-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(99, "Date-Time.ico")
        Me.ImageList1.Images.SetKeyName(100, "db-Add.ico")
        Me.ImageList1.Images.SetKeyName(101, "db-Cancel.ico")
        Me.ImageList1.Images.SetKeyName(102, "db-Delete.ico")
        Me.ImageList1.Images.SetKeyName(103, "db-Edit.ico")
        Me.ImageList1.Images.SetKeyName(104, "db-First.ico")
        Me.ImageList1.Images.SetKeyName(105, "db-Last.ico")
        Me.ImageList1.Images.SetKeyName(106, "db-Next.ico")
        Me.ImageList1.Images.SetKeyName(107, "db-Post.ico")
        Me.ImageList1.Images.SetKeyName(108, "db-Previous.ico")
        Me.ImageList1.Images.SetKeyName(109, "db-Refresh.ico")
        Me.ImageList1.Images.SetKeyName(110, "Decimals-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(111, "Decimals-Increase.ico")
        Me.ImageList1.Images.SetKeyName(112, "Decompress.ico")
        Me.ImageList1.Images.SetKeyName(113, "Delete.ico")
        Me.ImageList1.Images.SetKeyName(114, "Delete-Blue.ico")
        Me.ImageList1.Images.SetKeyName(115, "Design.ico")
        Me.ImageList1.Images.SetKeyName(116, "Desktop.ico")
        Me.ImageList1.Images.SetKeyName(117, "Dictionary.ico")
        Me.ImageList1.Images.SetKeyName(118, "Disconnect.ico")
        Me.ImageList1.Images.SetKeyName(119, "Discuss.ico")
        Me.ImageList1.Images.SetKeyName(120, "Doc-Access.ico")
        Me.ImageList1.Images.SetKeyName(121, "Doc-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(122, "Doc-Excel.ico")
        Me.ImageList1.Images.SetKeyName(123, "Doc-HTML.ico")
        Me.ImageList1.Images.SetKeyName(124, "Doc-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(125, "Doc-RTF.ico")
        Me.ImageList1.Images.SetKeyName(126, "Document-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(127, "Document-Protect.ico")
        Me.ImageList1.Images.SetKeyName(128, "Doc-Word.ico")
        Me.ImageList1.Images.SetKeyName(129, "Doc-XML.ico")
        Me.ImageList1.Images.SetKeyName(130, "Drawing.ico")
        Me.ImageList1.Images.SetKeyName(131, "Edit.ico")
        Me.ImageList1.Images.SetKeyName(132, "Equaliser.ico")
        Me.ImageList1.Images.SetKeyName(133, "Execute.ico")
        Me.ImageList1.Images.SetKeyName(134, "Exit.ico")
        Me.ImageList1.Images.SetKeyName(135, "Export.ico")
        Me.ImageList1.Images.SetKeyName(136, "Export-Access.ico")
        Me.ImageList1.Images.SetKeyName(137, "Export-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(138, "Export-CSV.ico")
        Me.ImageList1.Images.SetKeyName(139, "Export-Excel.ico")
        Me.ImageList1.Images.SetKeyName(140, "Export-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(141, "Export-HTML.ico")
        Me.ImageList1.Images.SetKeyName(142, "Export-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(143, "Export-RTF.ico")
        Me.ImageList1.Images.SetKeyName(144, "Export-Text.ico")
        Me.ImageList1.Images.SetKeyName(145, "Export-Word.ico")
        Me.ImageList1.Images.SetKeyName(146, "Export-XML.ico")
        Me.ImageList1.Images.SetKeyName(147, "Favorites.ico")
        Me.ImageList1.Images.SetKeyName(148, "Fill.ico")
        Me.ImageList1.Images.SetKeyName(149, "Fill-Down.ico")
        Me.ImageList1.Images.SetKeyName(150, "Fill-Left.ico")
        Me.ImageList1.Images.SetKeyName(151, "Fill-Right.ico")
        Me.ImageList1.Images.SetKeyName(152, "Fill-Up.ico")
        Me.ImageList1.Images.SetKeyName(153, "Filter.ico")
        Me.ImageList1.Images.SetKeyName(154, "Flag.ico")
        Me.ImageList1.Images.SetKeyName(155, "Flag-Delete.ico")
        Me.ImageList1.Images.SetKeyName(156, "Flag-Next.ico")
        Me.ImageList1.Images.SetKeyName(157, "Flag-Previous.ico")
        Me.ImageList1.Images.SetKeyName(158, "Flow Chart.ico")
        Me.ImageList1.Images.SetKeyName(159, "Folder List.ico")
        Me.ImageList1.Images.SetKeyName(160, "Folder.ico")
        Me.ImageList1.Images.SetKeyName(161, "Folder-Closed.ico")
        Me.ImageList1.Images.SetKeyName(162, "Folder-Delete.ico")
        Me.ImageList1.Images.SetKeyName(163, "Folder-New.ico")
        Me.ImageList1.Images.SetKeyName(164, "Folder-Options.ico")
        Me.ImageList1.Images.SetKeyName(165, "Folder-Search.ico")
        Me.ImageList1.Images.SetKeyName(166, "Footer.ico")
        Me.ImageList1.Images.SetKeyName(167, "Format Painter.ico")
        Me.ImageList1.Images.SetKeyName(168, "Format-Font Size.ico")
        Me.ImageList1.Images.SetKeyName(169, "Format-Font.ico")
        Me.ImageList1.Images.SetKeyName(170, "Format-Paragraph.ico")
        Me.ImageList1.Images.SetKeyName(171, "Format-Percentage.ico")
        Me.ImageList1.Images.SetKeyName(172, "Formatting-Remove.ico")
        Me.ImageList1.Images.SetKeyName(173, "Forward.ico")
        Me.ImageList1.Images.SetKeyName(174, "Full Screen.ico")
        Me.ImageList1.Images.SetKeyName(175, "Full Screen-New.ico")
        Me.ImageList1.Images.SetKeyName(176, "Function.ico")
        Me.ImageList1.Images.SetKeyName(177, "Go To Line.ico")
        Me.ImageList1.Images.SetKeyName(178, "Grid-2.ico")
        Me.ImageList1.Images.SetKeyName(179, "Grid.ico")
        Me.ImageList1.Images.SetKeyName(180, "Grid-Auto Format-2.ico")
        Me.ImageList1.Images.SetKeyName(181, "Grid-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(182, "Grid-Column Width.ico")
        Me.ImageList1.Images.SetKeyName(183, "Grid-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(184, "Grid-Delete Cell.ico")
        Me.ImageList1.Images.SetKeyName(185, "Grid-Delete Column.ico")
        Me.ImageList1.Images.SetKeyName(186, "Grid-Delete Row.ico")
        Me.ImageList1.Images.SetKeyName(187, "Grid-Delete.ico")
        Me.ImageList1.Images.SetKeyName(188, "Grid-Insert Cell.ico")
        Me.ImageList1.Images.SetKeyName(189, "Grid-Insert Column Left.ico")
        Me.ImageList1.Images.SetKeyName(190, "Grid-Insert Column Right.ico")
        Me.ImageList1.Images.SetKeyName(191, "Grid-Insert Column.ico")
        Me.ImageList1.Images.SetKeyName(192, "Grid-Insert Row Above.ico")
        Me.ImageList1.Images.SetKeyName(193, "Grid-Insert Row Below.ico")
        Me.ImageList1.Images.SetKeyName(194, "Grid-Insert Row.ico")
        Me.ImageList1.Images.SetKeyName(195, "Grid-Merge Cells.ico")
        Me.ImageList1.Images.SetKeyName(196, "Grid-New-2.ico")
        Me.ImageList1.Images.SetKeyName(197, "Grid-New.ico")
        Me.ImageList1.Images.SetKeyName(198, "Grid-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(199, "Grid-Options.ico")
        Me.ImageList1.Images.SetKeyName(200, "Grid-Row Height.ico")
        Me.ImageList1.Images.SetKeyName(201, "Grid-Select All-2.ico")
        Me.ImageList1.Images.SetKeyName(202, "Grid-Select All.ico")
        Me.ImageList1.Images.SetKeyName(203, "Grid-Select Cell-2.ico")
        Me.ImageList1.Images.SetKeyName(204, "Grid-Select Cell.ico")
        Me.ImageList1.Images.SetKeyName(205, "Grid-Select Column-2.ico")
        Me.ImageList1.Images.SetKeyName(206, "Grid-Select Column.ico")
        Me.ImageList1.Images.SetKeyName(207, "Grid-Select Row-2.ico")
        Me.ImageList1.Images.SetKeyName(208, "Grid-Select Row.ico")
        Me.ImageList1.Images.SetKeyName(209, "Grid-Split Cells.ico")
        Me.ImageList1.Images.SetKeyName(210, "Grid-Wizard-2.ico")
        Me.ImageList1.Images.SetKeyName(211, "Grid-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(212, "Hand Held.ico")
        Me.ImageList1.Images.SetKeyName(213, "Hand Shake.ico")
        Me.ImageList1.Images.SetKeyName(214, "Header and Footer.ico")
        Me.ImageList1.Images.SetKeyName(215, "Header and Footer-Toggle.ico")
        Me.ImageList1.Images.SetKeyName(216, "Header.ico")
        Me.ImageList1.Images.SetKeyName(217, "Header-Next.ico")
        Me.ImageList1.Images.SetKeyName(218, "Header-Previous.ico")
        Me.ImageList1.Images.SetKeyName(219, "Help.ico")
        Me.ImageList1.Images.SetKeyName(220, "Highlighter.ico")
        Me.ImageList1.Images.SetKeyName(221, "History.ico")
        Me.ImageList1.Images.SetKeyName(222, "Home.ico")
        Me.ImageList1.Images.SetKeyName(223, "Image.ico")
        Me.ImageList1.Images.SetKeyName(224, "Image-Brightness.ico")
        Me.ImageList1.Images.SetKeyName(225, "Image-Colour.ico")
        Me.ImageList1.Images.SetKeyName(226, "Image-Contrast.ico")
        Me.ImageList1.Images.SetKeyName(227, "Image-Crop.ico")
        Me.ImageList1.Images.SetKeyName(228, "Image-Flip.ico")
        Me.ImageList1.Images.SetKeyName(229, "Image-Mirror.ico")
        Me.ImageList1.Images.SetKeyName(230, "Image-Paint.ico")
        Me.ImageList1.Images.SetKeyName(231, "Image-Resize.ico")
        Me.ImageList1.Images.SetKeyName(232, "Image-Rotate.ico")
        Me.ImageList1.Images.SetKeyName(233, "Image-Select.ico")
        Me.ImageList1.Images.SetKeyName(234, "Import.ico")
        Me.ImageList1.Images.SetKeyName(235, "Import-Access.ico")
        Me.ImageList1.Images.SetKeyName(236, "Import-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(237, "Import-CSV.ico")
        Me.ImageList1.Images.SetKeyName(238, "Import-Excel.ico")
        Me.ImageList1.Images.SetKeyName(239, "Import-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(240, "Import-HTML.ico")
        Me.ImageList1.Images.SetKeyName(241, "Import-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(242, "Import-RTF.ico")
        Me.ImageList1.Images.SetKeyName(243, "Import-Text.ico")
        Me.ImageList1.Images.SetKeyName(244, "Import-Word.ico")
        Me.ImageList1.Images.SetKeyName(245, "Import-XML.ico")
        Me.ImageList1.Images.SetKeyName(246, "Indent-Decrease-2.ico")
        Me.ImageList1.Images.SetKeyName(247, "Indent-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(248, "Indent-Increase-2.ico")
        Me.ImageList1.Images.SetKeyName(249, "Indent-Increase.ico")
        Me.ImageList1.Images.SetKeyName(250, "Insert.ico")
        Me.ImageList1.Images.SetKeyName(251, "Insert-File.ico")
        Me.ImageList1.Images.SetKeyName(252, "Insert-Image.ico")
        Me.ImageList1.Images.SetKeyName(253, "Insert-Object.ico")
        Me.ImageList1.Images.SetKeyName(254, "Invoice.ico")
        Me.ImageList1.Images.SetKeyName(255, "Italic.ico")
        Me.ImageList1.Images.SetKeyName(256, "Key.ico")
        Me.ImageList1.Images.SetKeyName(257, "Landscape.ico")
        Me.ImageList1.Images.SetKeyName(258, "Launch.ico")
        Me.ImageList1.Images.SetKeyName(259, "Letter.ico")
        Me.ImageList1.Images.SetKeyName(260, "Line Spacing-2.ico")
        Me.ImageList1.Images.SetKeyName(261, "Line Spacing.ico")
        Me.ImageList1.Images.SetKeyName(262, "Link.ico")
        Me.ImageList1.Images.SetKeyName(263, "Lock.ico")
        Me.ImageList1.Images.SetKeyName(264, "Mail.ico")
        Me.ImageList1.Images.SetKeyName(265, "Mail-Forward.ico")
        Me.ImageList1.Images.SetKeyName(266, "Mail-In Box-2.ico")
        Me.ImageList1.Images.SetKeyName(267, "Mail-In Box.ico")
        Me.ImageList1.Images.SetKeyName(268, "Mail-In-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(269, "Mail-New.ico")
        Me.ImageList1.Images.SetKeyName(270, "Mail-Out Box-2.ico")
        Me.ImageList1.Images.SetKeyName(271, "Mail-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(272, "Mail-Reply To All.ico")
        Me.ImageList1.Images.SetKeyName(273, "Mail-Reply.ico")
        Me.ImageList1.Images.SetKeyName(274, "Mail-Search.ico")
        Me.ImageList1.Images.SetKeyName(275, "Mail-Send and Receive.ico")
        Me.ImageList1.Images.SetKeyName(276, "Media.ico")
        Me.ImageList1.Images.SetKeyName(277, "Microphone.ico")
        Me.ImageList1.Images.SetKeyName(278, "Midi Keyboard.ico")
        Me.ImageList1.Images.SetKeyName(279, "Minus.ico")
        Me.ImageList1.Images.SetKeyName(280, "mm-Eject.ico")
        Me.ImageList1.Images.SetKeyName(281, "mm-Fast Forward.ico")
        Me.ImageList1.Images.SetKeyName(282, "mm-First.ico")
        Me.ImageList1.Images.SetKeyName(283, "mm-Last.ico")
        Me.ImageList1.Images.SetKeyName(284, "mm-Pause.ico")
        Me.ImageList1.Images.SetKeyName(285, "mm-Play.ico")
        Me.ImageList1.Images.SetKeyName(286, "mm-Rewind.ico")
        Me.ImageList1.Images.SetKeyName(287, "mm-Stop.ico")
        Me.ImageList1.Images.SetKeyName(288, "Monitor.ico")
        Me.ImageList1.Images.SetKeyName(289, "Movie.ico")
        Me.ImageList1.Images.SetKeyName(290, "Musical Note-2.ico")
        Me.ImageList1.Images.SetKeyName(291, "Musical Note.ico")
        Me.ImageList1.Images.SetKeyName(292, "New.ico")
        Me.ImageList1.Images.SetKeyName(293, "Note.ico")
        Me.ImageList1.Images.SetKeyName(294, "Note-Add.ico")
        Me.ImageList1.Images.SetKeyName(295, "Note-Delete.ico")
        Me.ImageList1.Images.SetKeyName(296, "Note-Edit.ico")
        Me.ImageList1.Images.SetKeyName(297, "Note-New.ico")
        Me.ImageList1.Images.SetKeyName(298, "Note-Next.ico")
        Me.ImageList1.Images.SetKeyName(299, "Notepad.ico")
        Me.ImageList1.Images.SetKeyName(300, "Note-Previous.ico")
        Me.ImageList1.Images.SetKeyName(301, "Note-Remove.ico")
        Me.ImageList1.Images.SetKeyName(302, "Notes.ico")
        Me.ImageList1.Images.SetKeyName(303, "Note-Search.ico")
        Me.ImageList1.Images.SetKeyName(304, "Number of Pages.ico")
        Me.ImageList1.Images.SetKeyName(305, "Numbering-2.ico")
        Me.ImageList1.Images.SetKeyName(306, "Numbering.ico")
        Me.ImageList1.Images.SetKeyName(307, "Object-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(308, "Object-Select.ico")
        Me.ImageList1.Images.SetKeyName(309, "Object-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(310, "Open.ico")
        Me.ImageList1.Images.SetKeyName(311, "Outline-2.ico")
        Me.ImageList1.Images.SetKeyName(312, "Outline.ico")
        Me.ImageList1.Images.SetKeyName(313, "Page Number.ico")
        Me.ImageList1.Images.SetKeyName(314, "Page-Break.ico")
        Me.ImageList1.Images.SetKeyName(315, "Page-Invert.ico")
        Me.ImageList1.Images.SetKeyName(316, "Page-Next.ico")
        Me.ImageList1.Images.SetKeyName(317, "Page-Previous.ico")
        Me.ImageList1.Images.SetKeyName(318, "Page-Setup.ico")
        Me.ImageList1.Images.SetKeyName(319, "Paintbrush.ico")
        Me.ImageList1.Images.SetKeyName(320, "Parcel.ico")
        Me.ImageList1.Images.SetKeyName(321, "Paste.ico")
        Me.ImageList1.Images.SetKeyName(322, "Permission.ico")
        Me.ImageList1.Images.SetKeyName(323, "Phone.ico")
        Me.ImageList1.Images.SetKeyName(324, "Planner.ico")
        Me.ImageList1.Images.SetKeyName(325, "Plus.ico")
        Me.ImageList1.Images.SetKeyName(326, "Portrait.ico")
        Me.ImageList1.Images.SetKeyName(327, "Preview.ico")
        Me.ImageList1.Images.SetKeyName(328, "Print-2.ico")
        Me.ImageList1.Images.SetKeyName(329, "Print.ico")
        Me.ImageList1.Images.SetKeyName(330, "Print-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(331, "Print-Options.ico")
        Me.ImageList1.Images.SetKeyName(332, "Procedure.ico")
        Me.ImageList1.Images.SetKeyName(333, "Process.ico")
        Me.ImageList1.Images.SetKeyName(334, "Project.ico")
        Me.ImageList1.Images.SetKeyName(335, "Project-Gant Chart.ico")
        Me.ImageList1.Images.SetKeyName(336, "Properties.ico")
        Me.ImageList1.Images.SetKeyName(337, "Push Pin.ico")
        Me.ImageList1.Images.SetKeyName(338, "Recycle Bin.ico")
        Me.ImageList1.Images.SetKeyName(339, "Redo.ico")
        Me.ImageList1.Images.SetKeyName(340, "Refresh.ico")
        Me.ImageList1.Images.SetKeyName(341, "Remove.ico")
        Me.ImageList1.Images.SetKeyName(342, "Rename.ico")
        Me.ImageList1.Images.SetKeyName(343, "Report-2.ico")
        Me.ImageList1.Images.SetKeyName(344, "Report.ico")
        Me.ImageList1.Images.SetKeyName(345, "Report-Bound.ico")
        Me.ImageList1.Images.SetKeyName(346, "Research.ico")
        Me.ImageList1.Images.SetKeyName(347, "Ruler.ico")
        Me.ImageList1.Images.SetKeyName(348, "Ruler-Formatting.ico")
        Me.ImageList1.Images.SetKeyName(349, "Save All.ico")
        Me.ImageList1.Images.SetKeyName(350, "Save As HTML.ico")
        Me.ImageList1.Images.SetKeyName(351, "Save As.ico")
        Me.ImageList1.Images.SetKeyName(352, "Save Draft.ico")
        Me.ImageList1.Images.SetKeyName(353, "Save.ico")
        Me.ImageList1.Images.SetKeyName(354, "Script.ico")
        Me.ImageList1.Images.SetKeyName(355, "Search.ico")
        Me.ImageList1.Images.SetKeyName(356, "Search-Next.ico")
        Me.ImageList1.Images.SetKeyName(357, "Search-Previous.ico")
        Me.ImageList1.Images.SetKeyName(358, "Search-Replace.ico")
        Me.ImageList1.Images.SetKeyName(359, "Select All.ico")
        Me.ImageList1.Images.SetKeyName(360, "Slide Show.ico")
        Me.ImageList1.Images.SetKeyName(361, "Slide.ico")
        Me.ImageList1.Images.SetKeyName(362, "Software-Analyse.ico")
        Me.ImageList1.Images.SetKeyName(363, "Sort-Ascending.ico")
        Me.ImageList1.Images.SetKeyName(364, "Sort-Descending.ico")
        Me.ImageList1.Images.SetKeyName(365, "Sound-Delete.ico")
        Me.ImageList1.Images.SetKeyName(366, "Sound-Fade In.ico")
        Me.ImageList1.Images.SetKeyName(367, "Sound-Fade Out.ico")
        Me.ImageList1.Images.SetKeyName(368, "Sound-Join.ico")
        Me.ImageList1.Images.SetKeyName(369, "Sound-Mark End.ico")
        Me.ImageList1.Images.SetKeyName(370, "Sound-Mark Start.ico")
        Me.ImageList1.Images.SetKeyName(371, "Sound-Split.ico")
        Me.ImageList1.Images.SetKeyName(372, "Spell Check.ico")
        Me.ImageList1.Images.SetKeyName(373, "Stop.ico")
        Me.ImageList1.Images.SetKeyName(374, "Strikeout.ico")
        Me.ImageList1.Images.SetKeyName(375, "Subscript.ico")
        Me.ImageList1.Images.SetKeyName(376, "Sum.ico")
        Me.ImageList1.Images.SetKeyName(377, "Summary.ico")
        Me.ImageList1.Images.SetKeyName(378, "Superscript.ico")
        Me.ImageList1.Images.SetKeyName(379, "Support.ico")
        Me.ImageList1.Images.SetKeyName(380, "Tables and Borders.ico")
        Me.ImageList1.Images.SetKeyName(381, "Task.ico")
        Me.ImageList1.Images.SetKeyName(382, "Task-Search.ico")
        Me.ImageList1.Images.SetKeyName(383, "Thesaurus.ico")
        Me.ImageList1.Images.SetKeyName(384, "Tick.ico")
        Me.ImageList1.Images.SetKeyName(385, "Time Line.ico")
        Me.ImageList1.Images.SetKeyName(386, "Tip.ico")
        Me.ImageList1.Images.SetKeyName(387, "To Do List.ico")
        Me.ImageList1.Images.SetKeyName(388, "To Lowercase.ico")
        Me.ImageList1.Images.SetKeyName(389, "To Uppercase.ico")
        Me.ImageList1.Images.SetKeyName(390, "Tools.ico")
        Me.ImageList1.Images.SetKeyName(391, "Tree View.ico")
        Me.ImageList1.Images.SetKeyName(392, "Tree View-Add.ico")
        Me.ImageList1.Images.SetKeyName(393, "Tree View-Delete.ico")
        Me.ImageList1.Images.SetKeyName(394, "Tree View-Edit.ico")
        Me.ImageList1.Images.SetKeyName(395, "Tree View-New.ico")
        Me.ImageList1.Images.SetKeyName(396, "Tree View-Remove.ico")
        Me.ImageList1.Images.SetKeyName(397, "Tree View-Search.ico")
        Me.ImageList1.Images.SetKeyName(398, "Underline.ico")
        Me.ImageList1.Images.SetKeyName(399, "Undo.ico")
        Me.ImageList1.Images.SetKeyName(400, "Unlock.ico")
        Me.ImageList1.Images.SetKeyName(401, "User-2.ico")
        Me.ImageList1.Images.SetKeyName(402, "User.ico")
        Me.ImageList1.Images.SetKeyName(403, "User-Add-2.ico")
        Me.ImageList1.Images.SetKeyName(404, "User-Add.ico")
        Me.ImageList1.Images.SetKeyName(405, "User-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(406, "User-Delete.ico")
        Me.ImageList1.Images.SetKeyName(407, "User-Edit-2.ico")
        Me.ImageList1.Images.SetKeyName(408, "User-Edit.ico")
        Me.ImageList1.Images.SetKeyName(409, "User-New-2.ico")
        Me.ImageList1.Images.SetKeyName(410, "User-New.ico")
        Me.ImageList1.Images.SetKeyName(411, "User-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(412, "User-Options.ico")
        Me.ImageList1.Images.SetKeyName(413, "User-Password-2.ico")
        Me.ImageList1.Images.SetKeyName(414, "User-Password.ico")
        Me.ImageList1.Images.SetKeyName(415, "User-Remove-2.ico")
        Me.ImageList1.Images.SetKeyName(416, "User-Remove.ico")
        Me.ImageList1.Images.SetKeyName(417, "Users-2.ico")
        Me.ImageList1.Images.SetKeyName(418, "Users.ico")
        Me.ImageList1.Images.SetKeyName(419, "User-Search-2.ico")
        Me.ImageList1.Images.SetKeyName(420, "User-Search.ico")
        Me.ImageList1.Images.SetKeyName(421, "User-Security-2.ico")
        Me.ImageList1.Images.SetKeyName(422, "User-Security.ico")
        Me.ImageList1.Images.SetKeyName(423, "View-Details.ico")
        Me.ImageList1.Images.SetKeyName(424, "View-Large Icons.ico")
        Me.ImageList1.Images.SetKeyName(425, "View-List.ico")
        Me.ImageList1.Images.SetKeyName(426, "View-One Page.ico")
        Me.ImageList1.Images.SetKeyName(427, "View-Page Width.ico")
        Me.ImageList1.Images.SetKeyName(428, "Views.ico")
        Me.ImageList1.Images.SetKeyName(429, "View-Small Icons.ico")
        Me.ImageList1.Images.SetKeyName(430, "Volume.ico")
        Me.ImageList1.Images.SetKeyName(431, "Volume-Mute.ico")
        Me.ImageList1.Images.SetKeyName(432, "Warning.ico")
        Me.ImageList1.Images.SetKeyName(433, "Waste Bin.ico")
        Me.ImageList1.Images.SetKeyName(434, "Window.ico")
        Me.ImageList1.Images.SetKeyName(435, "Window-Add.ico")
        Me.ImageList1.Images.SetKeyName(436, "Window-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(437, "Window-Cascade.ico")
        Me.ImageList1.Images.SetKeyName(438, "Window-Close.ico")
        Me.ImageList1.Images.SetKeyName(439, "Window-Delete.ico")
        Me.ImageList1.Images.SetKeyName(440, "Window-Edit.ico")
        Me.ImageList1.Images.SetKeyName(441, "Window-Maximise.ico")
        Me.ImageList1.Images.SetKeyName(442, "Window-Minimise.ico")
        Me.ImageList1.Images.SetKeyName(443, "Window-New.ico")
        Me.ImageList1.Images.SetKeyName(444, "Window-Next.ico")
        Me.ImageList1.Images.SetKeyName(445, "Window-Options.ico")
        Me.ImageList1.Images.SetKeyName(446, "Window-Preview.ico")
        Me.ImageList1.Images.SetKeyName(447, "Window-Previous.ico")
        Me.ImageList1.Images.SetKeyName(448, "Window-Properties.ico")
        Me.ImageList1.Images.SetKeyName(449, "Window-Remove.ico")
        Me.ImageList1.Images.SetKeyName(450, "Window-Search.ico")
        Me.ImageList1.Images.SetKeyName(451, "Window-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(452, "Window-Split.ico")
        Me.ImageList1.Images.SetKeyName(453, "Window-Tile Horizontal.ico")
        Me.ImageList1.Images.SetKeyName(454, "Window-Tile Vertical.ico")
        Me.ImageList1.Images.SetKeyName(455, "Wizard.ico")
        Me.ImageList1.Images.SetKeyName(456, "Word Count.ico")
        Me.ImageList1.Images.SetKeyName(457, "Word Wrap-2.ico")
        Me.ImageList1.Images.SetKeyName(458, "Word Wrap.ico")
        Me.ImageList1.Images.SetKeyName(459, "Work Sheet.ico")
        Me.ImageList1.Images.SetKeyName(460, "Work Sheet-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(461, "Work Sheet-Delete.ico")
        Me.ImageList1.Images.SetKeyName(462, "Work Sheet-New.ico")
        Me.ImageList1.Images.SetKeyName(463, "Work Sheet-Options.ico")
        Me.ImageList1.Images.SetKeyName(464, "Work Sheet-Protect.ico")
        Me.ImageList1.Images.SetKeyName(465, "Work Sheet-Rename.ico")
        Me.ImageList1.Images.SetKeyName(466, "Zoom-In.ico")
        Me.ImageList1.Images.SetKeyName(467, "Zoom-Out.ico")
        '
        'BarCheckItem2
        '
        Me.BarCheckItem2.Caption = "BarCheckItem2"
        Me.BarCheckItem2.Id = 12
        Me.BarCheckItem2.Name = "BarCheckItem2"
        '
        'RepositoryItemImageComboBox1
        '
        Me.RepositoryItemImageComboBox1.AutoHeight = False
        Me.RepositoryItemImageComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemImageComboBox1.Name = "RepositoryItemImageComboBox1"
        '
        'BarCheckItem1
        '
        Me.BarCheckItem1.Caption = "BarCheckItem1"
        Me.BarCheckItem1.Id = 10
        Me.BarCheckItem1.Name = "BarCheckItem1"
        '
        'DSLists
        '
        Me.DSLists.DataSetName = "DSLists"
        Me.DSLists.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EUS_LISTS_GenderBindingSource
        '
        Me.EUS_LISTS_GenderBindingSource.DataMember = "EUS_LISTS_Gender"
        Me.EUS_LISTS_GenderBindingSource.DataSource = Me.DSLists
        '
        'frmDDManagerProfiles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(894, 451)
        Me.Controls.Add(Me.gpMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmDDManagerProfiles"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " Date Deals Profiles"
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gpMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpMain.ResumeLayout(False)
        CType(Me.GridControlProfiles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_ProfilesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSMembers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewProfiles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemImageComboBox_GenderId, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemImageComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_LISTS_GenderBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents gpMain As DevExpress.XtraEditors.GroupControl
    Friend WithEvents BarRefresh As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GridControlProfiles As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewProfiles As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BarAddNEW As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarEDIT As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarDELETE As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents colStatus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLoginName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGenderId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCountry As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colRegion As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCity As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colZip As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coleMail As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCellular As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAboutMe_Heading As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAboutMe_DescribeYourself As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAboutMe_DescribeAnIdealFirstDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateTimeToRegister As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastLoginDateTime As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastUpdateProfileDateTime As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents EUS_ProfilesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DSMembers As Dating.Client.Admin.APP.AdminWS.DSMembers
    Friend WithEvents RepositoryItemImageComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
    Friend WithEvents BarProfilesFilter As DevExpress.XtraBars.BarSubItem
    Friend WithEvents bciShowMasterRecords As DevExpress.XtraBars.BarCheckItem
    Friend WithEvents bciShowNonMasterRecords As DevExpress.XtraBars.BarCheckItem
    Friend WithEvents BarCheckItem2 As DevExpress.XtraBars.BarCheckItem
    Friend WithEvents BarCheckItem1 As DevExpress.XtraBars.BarCheckItem
    Friend WithEvents RepositoryItemImageComboBox_GenderId As DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox
    Friend WithEvents colBirthday As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNotificationsSettings_WhenNewMembersNearMe As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DSLists As Dating.Client.Admin.APP.AdminWS.DSLists
    Friend WithEvents EUS_LISTS_GenderBindingSource As System.Windows.Forms.BindingSource
End Class

﻿Public Class frmLikes 

    Public Property ShowSendingReceiving As Boolean
        Get
            Return UcLikes1.ShowSendingReceiving

        End Get
        Set(value As Boolean)
            UcLikes1.ShowSendingReceiving = value
        End Set
    End Property


    Public Property ProfileID As Integer
        Get
            Return UcLikes1.ProfileID

        End Get
        Set(value As Integer)
            UcLikes1.ProfileID = value
        End Set
    End Property

    Private Sub frmLikes_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            UcLikes1.Init()
            UcLikes1.RefreshGrid()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmLikes->Load")
        End Try
    End Sub

    Public Sub RefreshGrid()
        UcLikes1.RefreshGrid()
    End Sub
End Class
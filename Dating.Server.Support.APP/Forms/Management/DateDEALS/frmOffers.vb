﻿Public Class frmOffers

    Public Property ShowSendingReceiving As Boolean
        Get
            Return UcOffers1.ShowSendingReceiving

        End Get
        Set(value As Boolean)
            UcOffers1.ShowSendingReceiving = value
        End Set
    End Property


    Public Property ProfileID As Integer
        Get
            Return UcOffers1.ProfileID

        End Get
        Set(value As Integer)
            UcOffers1.ProfileID = value
        End Set
    End Property

    Private Sub frmOffers_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            UcOffers1.Init()
            UcOffers1.RefreshGrid()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmOffers->Load")
        End Try
    End Sub

    Public Sub RefreshGrid()
        UcOffers1.RefreshGrid()
    End Sub

End Class
﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Data
Imports System.IO
Imports System.Diagnostics
Imports DevExpress.XtraEditors
Imports DevExpress.Skins
Imports DevExpress.XtraBars.Ribbon
Imports DevExpress.XtraBars.Ribbon.Gallery
Imports DevExpress.Utils.Drawing
Imports DevExpress.Utils
Imports DevExpress.XtraBars
Imports DevExpress.XtraNavBar
Imports DevExpress.UserSkins
Imports System.Web
Imports System.Web.HttpUtility
Imports System.Web.Mail
Imports System.Net
Imports System.Data.SqlClient
Imports Dating.Server.Core.DLL


Public Class frmDDMain


    Public m_SiteIDOnLocal As Integer
    Dim m_SiteName As String
    Dim m_SiteURL As String
    Dim tmrMainCountersInterval As Integer


    Private Sub frmMainCP_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        On Error GoTo ER

        If (m_UserRole = "Admin") Then
            NavBarGroupTools.Visible = True
        Else
            NavBarGroupTools.Visible = False
        End If

        Dim exetype As String
        Me.Text = m_SiteURL & " Goomena CPanel Administrator - Ver: " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & " WS:" & gAdminWS.Url & " - APP:" & RegistryAPP & " Site Name:" & m_SiteName
        '" Connecting to SQL Server : " & G.DB_ServerName & " Database=" & G.DB_DataBase & " Login=" & G.DB_Login & 
        Dim fw As New frmWaitProccess
        fw.Text = "Starting Application..."
        fw.ProgressBar.Properties.Maximum = 3
        fw.CmdCancel.Visible = False
        fw.Show()
        Application.DoEvents()
        Me.MaximizeBox = True
        NavBarMarketing.ActiveGroup = NavBarMarketing.Groups(0)
        NavBarMarketing.ActiveGroup = NavBarMarketing.Groups(0)
        NavBarMarketing.ActiveGroup.SelectedLinkIndex = -1




        fw.Close()
        Exit Sub
ER:     ErrorMsgBox("frmMain->frmMain_Load")
    End Sub

    Private Sub frmDDMain_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        GetAdminCounters()
    End Sub

    Public Sub LoadSubForm(ByVal frm As XtraForm)
        On Error GoTo er
        Dim i As Integer
        For i = 0 To xtraTabbedMdiManager1.Pages.Count - 1
            If xtraTabbedMdiManager1.Pages(i).Text = frm.Text Then
                xtraTabbedMdiManager1.SelectedPage = xtraTabbedMdiManager1.Pages(i)
                If (TypeOf xtraTabbedMdiManager1.Pages(i).MdiChild Is frmDDManagerProfiles) Then
                    DirectCast(xtraTabbedMdiManager1.Pages(i).MdiChild, frmDDManagerProfiles).RefreshGrid()
                End If
                Return
            End If
        Next i
        LoadSubFormEx(frm)
        Exit Sub
er:     ErrorMsgBox("frmMain->LoadSubForm")
    End Sub

    Public Function FocusSubForm(FormTitle As String) As Boolean
        On Error GoTo er
        Dim i As Integer
        For i = 0 To xtraTabbedMdiManager1.Pages.Count - 1
            If xtraTabbedMdiManager1.Pages(i).Text = FormTitle Then
                xtraTabbedMdiManager1.SelectedPage = xtraTabbedMdiManager1.Pages(i)
                If (TypeOf xtraTabbedMdiManager1.Pages(i).MdiChild Is frmDDManagerProfiles) Then
                    DirectCast(xtraTabbedMdiManager1.Pages(i).MdiChild, frmDDManagerProfiles).RefreshGrid()
                End If
                Return True
            End If
        Next i
        Return False
        Exit Function
er:     ErrorMsgBox("frmMain->FocusSubForm")
    End Function

    Public Sub LoadSubFormEx(ByVal frm As XtraForm)
        Try
            frm.MdiParent = Me
            frm.Show()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->LoadSubFormEx")
        End Try
    End Sub

    Private Sub iAbout_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles iAbout.ItemClick
        frmAbout.Show()
    End Sub

    Private Sub iExit_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles iExit.ItemClick
        gExitNow = True
        Close()
    End Sub

    Private Sub iSwitchUser_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles iSwitchUser.ItemClick
        frmCASLogin.ShowDialog()
    End Sub

    Private Sub NavBarControl1_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarMarketing.LinkClicked
        Try
            '  MsgBox(e.Link.ItemName & "ii=" & e.Link.Item.Tag)
        Catch ex As Exception

        End Try
    End Sub


    Public Sub EDIT(ByVal SitesID As Integer, SiteName As String, SiteURL As String)
        Try
            m_SiteIDOnLocal = SitesID
            m_SiteName = SiteName
            m_SiteURL = SiteURL
            Me.ShowDialog()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


#Region "Config LISTs"


    Private Sub navSYS_CountriesGEO_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navSYS_CountriesGEO.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "SYS_CountriesGEO"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navSYS_CountriesGEO")
        End Try
    End Sub

    Private Sub navEUS_LISTS_BodyType_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_BodyType.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_BodyType"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_BodyType")
        End Try
    End Sub

    Private Sub navEUS_LISTS_ChildrenNumber_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_ChildrenNumber.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_ChildrenNumber"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_ChildrenNumber")
        End Try
    End Sub

    Private Sub navEUS_LISTS_Drinking_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_Drinking.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_Drinking"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_Drinking")
        End Try
    End Sub

    Private Sub navEUS_LISTS_Education_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_Education.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_Education"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_Education")
        End Try
    End Sub

    Private Sub navEUS_LISTS_Ethnicity_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_Ethnicity.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_Ethnicity"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_Ethnicity")
        End Try
    End Sub

    Private Sub navEUS_LISTS_EyeColor_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_EyeColor.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_EyeColor"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_EyeColor")
        End Try
    End Sub

    Private Sub navEUS_LISTS_HairColor_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_HairColor.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_HairColor"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_HairColor")
        End Try
    End Sub

    Private Sub navEUS_LISTS_Height_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_Height.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_Height"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_Height")
        End Try
    End Sub

    Private Sub navEUS_LISTS_Income_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_Income.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_Income"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_Income")
        End Try
    End Sub

    Private Sub navEUS_LISTS_NetWorth_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_NetWorth.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_NetWorth"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_NetWorth")
        End Try
    End Sub

    Private Sub navEUS_LISTS_RejectingReasons_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_RejectingReasons.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_RejectingReasons"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_RejectingReasons")
        End Try
    End Sub

    Private Sub navEUS_LISTS_RelationshipStatus_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_RelationshipStatus.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_RelationshipStatus"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_RelationshipStatus")
        End Try
    End Sub

    Private Sub navEUS_LISTS_Smoking_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_Smoking.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_Smoking"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_Smoking")
        End Try
    End Sub

    Private Sub navEUS_LISTS_Religion_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_Religion.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_Religion"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_Religion")
        End Try
    End Sub


    Private Sub navEUS_LISTS_AccountType_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_AccountType.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_AccountType"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_AccountType")
        End Try
    End Sub


    Private Sub navEUS_LISTS_Gender_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_Gender.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_Gender"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_Gender")
        End Try
    End Sub


    Private Sub navEUS_LISTS_PhotosDisplayLevel_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_PhotosDisplayLevel.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_PhotosDisplayLevel"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_PhotosDisplayLevel")
        End Try
    End Sub


    Private Sub navEUS_LISTS_TypeOfDating_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navEUS_LISTS_TypeOfDating.LinkClicked
        Try
            Dim frm As New frmDDManagerLists()
            frm.Text = e.Link.Caption
            frm.WhatListToLoad = "EUS_LISTS_TypeOfDating"
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navEUS_LISTS_TypeOfDating")
        End Try
    End Sub
#End Region

    Private Sub NavBarItemTools_UpdateGEOGR_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemTools_UpdateGEOGR.LinkClicked
        Try
            Dim frm As New frmTools()
            frm.Text = "Tools" 'e.Link.Caption
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemTools_UpdateGEOGR")
        End Try
    End Sub

    Private Sub NavBarItemTools_ReplaceDBString_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemTools_ReplaceDBString.LinkClicked
        Try
            Dim frm As New frmTools()
            frm.Text = "Tools" 'e.Link.Caption
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemTools_ReplaceDBString")
        End Try
    End Sub

    Private Sub NavBarItemConfig_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemAutoApprove.LinkClicked
        Try
            Dim frm As New frmConfig()
            frm.Text = e.Link.Caption
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemConfig")
        End Try
    End Sub

#Region "Profiles+Photos group"
    Private Sub navProfiles_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navProfiles.LinkClicked
        Try
            Dim frm As New frmDDManagerProfiles()
            frm.Text = e.Link.Caption
            LoadSubForm(frm)
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navProfiles")
        End Try
    End Sub

    Private Sub NavBarItemAutoApprovedProfiles_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemAutoApprovedProfiles.LinkClicked
        Try
            Dim frm As New frmDDManagerProfiles()
            frm.Text = e.Link.Caption
            frm.WhatProfilesToLoad = ProfileStatusEnum.AutoApprovedProfile
            LoadSubForm(frm)

            GetAdminCounters()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemAutoApprovedProfiles")
        End Try

    End Sub


    Private Sub navNewProfiles_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navNewProfiles.LinkClicked
        Try
            Dim frm As New frmDDManagerProfiles()
            frm.Text = e.Link.Caption
            frm.WhatProfilesToLoad = ProfileStatusEnum.NewProfile
            LoadSubForm(frm)

            GetAdminCounters()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navNewProfiles")
        End Try
    End Sub

    Private Sub navUpdatingProfiles_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navUpdatingProfiles.LinkClicked
        Try
            Dim frm As New frmDDManagerProfiles()
            frm.Text = e.Link.Caption
            frm.WhatProfilesToLoad = ProfileStatusEnum.Updating
            LoadSubForm(frm)

            GetAdminCounters()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navUpdatingProfiles")
        End Try
    End Sub

    Private Sub navApprovedProfiles_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navApprovedProfiles.LinkClicked
        Try
            Dim frm As New frmDDManagerProfiles()
            frm.Text = e.Link.Caption
            frm.WhatProfilesToLoad = ProfileStatusEnum.Approved
            LoadSubForm(frm)

            GetAdminCounters()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navApprovedProfiles")
        End Try
    End Sub

    Private Sub navRejectedProfiles_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navRejectedNewProfiles.LinkClicked
        Try
            Dim frm As New frmDDManagerProfiles()
            frm.Text = e.Link.Caption
            frm.WhatProfilesToLoad = ProfileStatusEnum.Rejected
            LoadSubForm(frm)

            GetAdminCounters()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navRejectedProfiles")
        End Try
    End Sub

    Private Sub navDeletedProfiles_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles navDeletedProfiles.LinkClicked
        Try
            Dim frm As New frmDDManagerProfiles()
            frm.Text = e.Link.Caption
            frm.WhatProfilesToLoad = ProfileStatusEnum.Deleted
            LoadSubForm(frm)

            GetAdminCounters()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->navDeletedProfiles")
        End Try
    End Sub

    Private Sub NavDeletedByUser_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavDeletedByUser.LinkClicked
        Try
            Dim frm As New frmDDManagerProfiles()
            frm.Text = e.Link.Caption
            frm.WhatProfilesToLoad = ProfileStatusEnum.DeletedByUser
            LoadSubForm(frm)

            GetAdminCounters()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavDeletedByUser")
        End Try
    End Sub

    Private Sub NavBarItemNewPhotos_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemNewPhotos.LinkClicked
        Try
            Dim frm As New frmPhotos()
            frm.ProfileID = -9876
            frm.DefaultView = PhotosFilterEnum.NewPhotos
            frm.Text = e.Link.Caption
            LoadSubForm(frm)

            GetAdminCounters()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemNewPhotos")
        End Try
    End Sub

    Private Sub NavBarItemApprovedPhotos_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemApprovedPhotos.LinkClicked
        Try
            Dim frm As New frmPhotos()
            frm.ProfileID = -9876
            frm.DefaultView = PhotosFilterEnum.ApprovedPhotos
            frm.Text = e.Link.Caption
            LoadSubForm(frm)

            GetAdminCounters()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemApprovedPhotos")
        End Try
    End Sub

    Private Sub NavBarItemDeletedPhotos_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemDeletedPhotos.LinkClicked
        Try
            Dim frm As New frmPhotos()
            frm.ProfileID = -9876
            frm.DefaultView = PhotosFilterEnum.DeletedPhotos
            frm.Text = e.Link.Caption
            LoadSubForm(frm)

            GetAdminCounters()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemDeletedPhotos")
        End Try
    End Sub

    Private Sub NavBarItemAutoApprovedPhotos_LinkClicked(sender As System.Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItemAutoApprovedPhotos.LinkClicked
        Try
            Dim frm As New frmPhotos()
            frm.ProfileID = -9876
            frm.DefaultView = PhotosFilterEnum.AutoApprovedPhotos
            frm.Text = e.Link.Caption
            LoadSubForm(frm)

            GetAdminCounters()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmMain->NavBarItemAutoApprovedPhotos")
        End Try
    End Sub

#End Region

    Private Sub NavBarGroupProfiles_ItemChanged(sender As System.Object, e As System.EventArgs) Handles NavBarGroupProfiles.ItemChanged

    End Sub

#Region "Admin counters on profile+Photos"

    Dim ButtonsNames As New Dictionary(Of String, String)
    Private Sub SetAdminCountersToUI()
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New MethodInvoker(AddressOf SetAdminCountersToUI))
            Else
                Try

                    If (ButtonsNames.Count = 0) Then
                        ButtonsNames.Add(NavBarItemAutoApprovedProfiles.Name, NavBarItemAutoApprovedProfiles.Caption)
                        ButtonsNames.Add(NavBarItemAutoApprovedPhotos.Name, NavBarItemAutoApprovedPhotos.Caption)
                        ButtonsNames.Add(navNewProfiles.Name, navNewProfiles.Caption)
                        ButtonsNames.Add(navUpdatingProfiles.Name, navUpdatingProfiles.Caption)
                        ButtonsNames.Add(navApprovedProfiles.Name, navApprovedProfiles.Caption)
                        ButtonsNames.Add(navRejectedNewProfiles.Name, navRejectedNewProfiles.Caption)
                        ButtonsNames.Add(navDeletedProfiles.Name, navDeletedProfiles.Caption)
                        ButtonsNames.Add(NavDeletedByUser.Name, NavDeletedByUser.Caption)
                        ButtonsNames.Add(NavBarItemNewPhotos.Name, NavBarItemNewPhotos.Caption)
                        ButtonsNames.Add(NavBarItemApprovedPhotos.Name, NavBarItemApprovedPhotos.Caption)
                        ButtonsNames.Add(NavBarItemDeletedPhotos.Name, NavBarItemDeletedPhotos.Caption)
                    End If

                    Dim members As New AdminWS.DSMembers()

                    PergormReLogin()
                    CheckWebServiceCallResult(gAdminWS.GetAdminCounters(gLoginRec.Token, members))

                    If (members.GetAdminCounters.Rows.Count = 2) Then
                        Dim currentrow As AdminWS.DSMembers.GetAdminCountersRow
                        Dim row1 As AdminWS.DSMembers.GetAdminCountersRow = members.GetAdminCounters.Rows(0)
                        Dim row2 As AdminWS.DSMembers.GetAdminCountersRow = members.GetAdminCounters.Rows(1)

                        If (row1.DateTimeCreated > row2.DateTimeCreated) Then
                            currentrow = row1
                        Else
                            currentrow = row2
                        End If

                        For Each dc As DataColumn In members.GetAdminCounters.Columns
                            SetButtons(currentrow, dc.ColumnName, row1(dc.ColumnName) <> row2(dc.ColumnName))
                        Next

                    ElseIf (members.GetAdminCounters.Rows.Count = 1) Then

                        For Each dc As DataColumn In members.GetAdminCounters.Columns
                            SetButtons(members.GetAdminCounters.Rows(0), dc.ColumnName, True)
                        Next

                    End If

                Catch ex As Exception
                    ErrorMsgBox(ex, "frmMain->NavBarItemAutoApprovedPhotos")
                End Try


            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub SetButtons(ByRef currentrow As AdminWS.DSMembers.GetAdminCountersRow, columnName As String, Optional isChanged As Boolean = False)
        Select Case columnName
            Case "AutoApprovedProfiles"
                If (currentrow(columnName) > 0) Then
                    If (isChanged) Then NavBarItemAutoApprovedProfiles.Appearance.Font = New Font(NavBarItemAutoApprovedProfiles.Appearance.Font.FontFamily, NavBarItemAutoApprovedProfiles.Appearance.Font.Size, FontStyle.Bold)
                    NavBarItemAutoApprovedProfiles.Caption = ButtonsNames(NavBarItemAutoApprovedProfiles.Name) & " [" & currentrow(columnName) & "]"
                Else
                    NavBarItemAutoApprovedProfiles.Appearance.Font = New Font(NavBarItemAutoApprovedProfiles.Appearance.Font.FontFamily, NavBarItemAutoApprovedProfiles.Appearance.Font.Size, FontStyle.Regular)
                    NavBarItemAutoApprovedProfiles.Caption = ButtonsNames(NavBarItemAutoApprovedProfiles.Name)
                End If

            Case "AutoApprovedPhotos"
                If (currentrow(columnName) > 0) Then
                    If (isChanged) Then NavBarItemAutoApprovedPhotos.Appearance.Font = New Font(NavBarItemAutoApprovedPhotos.Appearance.Font.FontFamily, NavBarItemAutoApprovedPhotos.Appearance.Font.Size, FontStyle.Bold)
                    NavBarItemAutoApprovedPhotos.Caption = ButtonsNames(NavBarItemAutoApprovedPhotos.Name) & " [" & currentrow(columnName) & "]"
                Else
                    NavBarItemAutoApprovedPhotos.Appearance.Font = New Font(NavBarItemAutoApprovedPhotos.Appearance.Font.FontFamily, NavBarItemAutoApprovedPhotos.Appearance.Font.Size, FontStyle.Regular)
                    NavBarItemAutoApprovedPhotos.Caption = ButtonsNames(NavBarItemAutoApprovedPhotos.Name)
                End If

            Case "NewProfiles"
                If (currentrow(columnName) > 0) Then
                    If (isChanged) Then navNewProfiles.Appearance.Font = New Font(navNewProfiles.Appearance.Font.FontFamily, navNewProfiles.Appearance.Font.Size, FontStyle.Bold)
                    navNewProfiles.Caption = ButtonsNames(navNewProfiles.Name) & " [" & currentrow(columnName) & "]"
                Else
                    navNewProfiles.Appearance.Font = New Font(navNewProfiles.Appearance.Font.FontFamily, navNewProfiles.Appearance.Font.Size, FontStyle.Regular)
                    navNewProfiles.Caption = ButtonsNames(navNewProfiles.Name)
                End If

            Case "UpdatingProfiles"
                If (currentrow(columnName) > 0) Then
                    If (isChanged) Then navUpdatingProfiles.Appearance.Font = New Font(navUpdatingProfiles.Appearance.Font.FontFamily, navUpdatingProfiles.Appearance.Font.Size, FontStyle.Bold)
                    navUpdatingProfiles.Caption = ButtonsNames(navUpdatingProfiles.Name) & " [" & currentrow(columnName) & "]"
                Else
                    navUpdatingProfiles.Appearance.Font = New Font(navUpdatingProfiles.Appearance.Font.FontFamily, navUpdatingProfiles.Appearance.Font.Size, FontStyle.Regular)
                    navUpdatingProfiles.Caption = ButtonsNames(navUpdatingProfiles.Name)
                End If

            Case "ApprovedProfiles"
                If (currentrow(columnName) > 0) Then
                    'navApprovedProfiles.Appearance.Font = New Font(navApprovedProfiles.Appearance.Font.FontFamily, navApprovedProfiles.Appearance.Font.Size, FontStyle.Bold)
                    navApprovedProfiles.Caption = ButtonsNames(navApprovedProfiles.Name) & " (" & currentrow(columnName) & ")"
                Else
                    'navApprovedProfiles.Appearance.Font = New Font(navApprovedProfiles.Appearance.Font.FontFamily, navApprovedProfiles.Appearance.Font.Size, FontStyle.Regular)
                    navApprovedProfiles.Caption = ButtonsNames(navApprovedProfiles.Name)
                End If

            Case "RejectedNewProfiles"
                If (currentrow(columnName) > 0) Then
                    'navRejectedNewProfiles.Appearance.Font = New Font(navRejectedNewProfiles.Appearance.Font.FontFamily, navRejectedNewProfiles.Appearance.Font.Size, FontStyle.Bold)
                    navRejectedNewProfiles.Caption = ButtonsNames(navRejectedNewProfiles.Name) & " (" & currentrow(columnName) & ")"
                Else
                    'navRejectedNewProfiles.Appearance.Font = New Font(navRejectedNewProfiles.Appearance.Font.FontFamily, navRejectedNewProfiles.Appearance.Font.Size, FontStyle.Regular)
                    navRejectedNewProfiles.Caption = ButtonsNames(navRejectedNewProfiles.Name)
                End If

            Case "DeletedProfiles"
                If (currentrow(columnName) > 0) Then
                    'navDeletedProfiles.Appearance.Font = New Font(navDeletedProfiles.Appearance.Font.FontFamily, navDeletedProfiles.Appearance.Font.Size, FontStyle.Bold)
                    navDeletedProfiles.Caption = ButtonsNames(navDeletedProfiles.Name) & " (" & currentrow(columnName) & ")"
                Else
                    'navDeletedProfiles.Appearance.Font = New Font(navDeletedProfiles.Appearance.Font.FontFamily, navDeletedProfiles.Appearance.Font.Size, FontStyle.Regular)
                    navDeletedProfiles.Caption = ButtonsNames(navDeletedProfiles.Name)
                End If

            Case "DeletedByUserProfiles"
                If (currentrow(columnName) > 0) Then
                    'NavDeletedByUser.Appearance.Font = New Font(NavDeletedByUser.Appearance.Font.FontFamily, NavDeletedByUser.Appearance.Font.Size, FontStyle.Bold)
                    NavDeletedByUser.Caption = ButtonsNames(NavDeletedByUser.Name) & " (" & currentrow(columnName) & ")"
                Else
                    'NavDeletedByUser.Appearance.Font = New Font(NavDeletedByUser.Appearance.Font.FontFamily, NavDeletedByUser.Appearance.Font.Size, FontStyle.Regular)
                    NavDeletedByUser.Caption = ButtonsNames(NavDeletedByUser.Name)
                End If

            Case "NewPhotos"
                If (currentrow(columnName) > 0) Then
                    If (isChanged) Then NavBarItemNewPhotos.Appearance.Font = New Font(NavBarItemNewPhotos.Appearance.Font.FontFamily, NavBarItemNewPhotos.Appearance.Font.Size, FontStyle.Bold)
                    NavBarItemNewPhotos.Caption = ButtonsNames(NavBarItemNewPhotos.Name) & " [" & currentrow(columnName) & "]"
                Else
                    NavBarItemNewPhotos.Appearance.Font = New Font(NavBarItemNewPhotos.Appearance.Font.FontFamily, NavBarItemNewPhotos.Appearance.Font.Size, FontStyle.Regular)
                    NavBarItemNewPhotos.Caption = ButtonsNames(NavBarItemNewPhotos.Name)
                End If

            Case "ApprovedPhotos"
                If (currentrow(columnName) > 0) Then
                    'NavBarItemApprovedPhotos.Appearance.Font = New Font(NavBarItemApprovedPhotos.Appearance.Font.FontFamily, NavBarItemApprovedPhotos.Appearance.Font.Size, FontStyle.Bold)
                    NavBarItemApprovedPhotos.Caption = ButtonsNames(NavBarItemApprovedPhotos.Name) & " (" & currentrow(columnName) & ")"
                Else
                    'NavBarItemApprovedPhotos.Appearance.Font = New Font(NavBarItemApprovedPhotos.Appearance.Font.FontFamily, NavBarItemApprovedPhotos.Appearance.Font.Size, FontStyle.Regular)
                    NavBarItemApprovedPhotos.Caption = ButtonsNames(NavBarItemApprovedPhotos.Name)
                End If

            Case "DeletePhotos"
                If (currentrow(columnName) > 0) Then
                    'NavBarItemDeletedPhotos.Appearance.Font = New Font(NavBarItemDeletedPhotos.Appearance.Font.FontFamily, NavBarItemDeletedPhotos.Appearance.Font.Size, FontStyle.Bold)
                    NavBarItemDeletedPhotos.Caption = ButtonsNames(NavBarItemDeletedPhotos.Name) & " (" & currentrow(columnName) & ")"
                Else
                    'NavBarItemDeletedPhotos.Appearance.Font = New Font(NavBarItemDeletedPhotos.Appearance.Font.FontFamily, NavBarItemDeletedPhotos.Appearance.Font.Size, FontStyle.Regular)
                    NavBarItemDeletedPhotos.Caption = ButtonsNames(NavBarItemDeletedPhotos.Name)
                End If

        End Select
    End Sub




    Private Sub tmrMainCounters_Tick(sender As System.Object, e As System.EventArgs) Handles tmrMainCounters.Tick
        SetAdminCountersToUI()
        tmrMainCounters.Interval = tmrMainCountersInterval
    End Sub

    Private Sub GetAdminCounters()
        If (tmrMainCountersInterval = 0) Then tmrMainCountersInterval = tmrMainCounters.Interval
        tmrMainCounters.Interval = 1000
        tmrMainCounters.Enabled = True
    End Sub

#End Region

End Class
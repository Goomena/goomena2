﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDDProfilePhoto
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDDProfilePhoto))
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.picPhoto = New DevExpress.XtraEditors.PictureEdit()
        Me.txtID = New DevExpress.XtraEditors.TextEdit()
        Me.EUS_CustomerPhotosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DSMembers = New Dating.Client.Admin.APP.AdminWS.DSMembers()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.txtProfileID = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.txtPhotoFileName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.txtCheckedContextID = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.dtUploaded = New DevExpress.XtraEditors.DateEdit()
        Me.icbDisplayLevelId = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.chkIsDefault = New DevExpress.XtraEditors.CheckEdit()
        Me.cmdSave1 = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdCancel1 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.lblWidthHeightInfo = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.lblTypeInfo = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.lblSizeInfo = New DevExpress.XtraEditors.LabelControl()
        Me.gcPhoto = New DevExpress.XtraEditors.GroupControl()
        Me.chkHasDeclined = New DevExpress.XtraEditors.CheckEdit()
        Me.chkHasAproved = New DevExpress.XtraEditors.CheckEdit()
        Me.txtPhotoURL = New DevExpress.XtraEditors.MemoEdit()
        Me.DSLists = New Dating.Client.Admin.APP.AdminWS.DSLists()
        Me.EUS_LISTS_PhotosDisplayLevelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.picPhoto.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_CustomerPhotosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSMembers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtProfileID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPhotoFileName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCheckedContextID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtUploaded.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtUploaded.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbDisplayLevelId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIsDefault.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcPhoto.SuspendLayout()
        CType(Me.chkHasDeclined.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkHasAproved.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPhotoURL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_LISTS_PhotosDisplayLevelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "About.ico")
        Me.ImageList1.Images.SetKeyName(1, "Accounts.ico")
        Me.ImageList1.Images.SetKeyName(2, "Address Book.ico")
        Me.ImageList1.Images.SetKeyName(3, "Air Brush.ico")
        Me.ImageList1.Images.SetKeyName(4, "Align-Centre-2.ico")
        Me.ImageList1.Images.SetKeyName(5, "Align-Centre.ico")
        Me.ImageList1.Images.SetKeyName(6, "Align-Full-2.ico")
        Me.ImageList1.Images.SetKeyName(7, "Align-Full.ico")
        Me.ImageList1.Images.SetKeyName(8, "Align-Left-2.ico")
        Me.ImageList1.Images.SetKeyName(9, "Align-Left.ico")
        Me.ImageList1.Images.SetKeyName(10, "Align-Right-2.ico")
        Me.ImageList1.Images.SetKeyName(11, "Align-Right.ico")
        Me.ImageList1.Images.SetKeyName(12, "Arrow-Left Right.ico")
        Me.ImageList1.Images.SetKeyName(13, "Arrow-Up Down.ico")
        Me.ImageList1.Images.SetKeyName(14, "Attachment.ico")
        Me.ImageList1.Images.SetKeyName(15, "Auto Correct-Options.ico")
        Me.ImageList1.Images.SetKeyName(16, "Auto-Type.ico")
        Me.ImageList1.Images.SetKeyName(17, "Back.ico")
        Me.ImageList1.Images.SetKeyName(18, "Bar Code.ico")
        Me.ImageList1.Images.SetKeyName(19, "Bold.ico")
        Me.ImageList1.Images.SetKeyName(20, "Book-Open.ico")
        Me.ImageList1.Images.SetKeyName(21, "Books.ico")
        Me.ImageList1.Images.SetKeyName(22, "Book-Search.ico")
        Me.ImageList1.Images.SetKeyName(23, "Borders and Shading.ico")
        Me.ImageList1.Images.SetKeyName(24, "Box-Closed-2.ico")
        Me.ImageList1.Images.SetKeyName(25, "Box-Closed.ico")
        Me.ImageList1.Images.SetKeyName(26, "Box-Open-2.ico")
        Me.ImageList1.Images.SetKeyName(27, "Box-Open.ico")
        Me.ImageList1.Images.SetKeyName(28, "Bullets-2.ico")
        Me.ImageList1.Images.SetKeyName(29, "Bullets.ico")
        Me.ImageList1.Images.SetKeyName(30, "Calculator.ico")
        Me.ImageList1.Images.SetKeyName(31, "Calendar.ico")
        Me.ImageList1.Images.SetKeyName(32, "Calendar-Go To Date.ico")
        Me.ImageList1.Images.SetKeyName(33, "Calendar-Search.ico")
        Me.ImageList1.Images.SetKeyName(34, "Calendar-Select Day.ico")
        Me.ImageList1.Images.SetKeyName(35, "Calendar-Select Month.ico")
        Me.ImageList1.Images.SetKeyName(36, "Calendar-Select Week.ico")
        Me.ImageList1.Images.SetKeyName(37, "Calendar-Select Work Week.ico")
        Me.ImageList1.Images.SetKeyName(38, "Camera.ico")
        Me.ImageList1.Images.SetKeyName(39, "Camera-Photo.ico")
        Me.ImageList1.Images.SetKeyName(40, "Card File.ico")
        Me.ImageList1.Images.SetKeyName(41, "Card.ico")
        Me.ImageList1.Images.SetKeyName(42, "Card-Add.ico")
        Me.ImageList1.Images.SetKeyName(43, "Card-Delete.ico")
        Me.ImageList1.Images.SetKeyName(44, "Card-Edit.ico")
        Me.ImageList1.Images.SetKeyName(45, "Card-New.ico")
        Me.ImageList1.Images.SetKeyName(46, "Card-Next.ico")
        Me.ImageList1.Images.SetKeyName(47, "Card-Previous.ico")
        Me.ImageList1.Images.SetKeyName(48, "Card-Remove.ico")
        Me.ImageList1.Images.SetKeyName(49, "Card-Search.ico")
        Me.ImageList1.Images.SetKeyName(50, "CD-Burn.ico")
        Me.ImageList1.Images.SetKeyName(51, "CD-In Case.ico")
        Me.ImageList1.Images.SetKeyName(52, "CD-ROM.ico")
        Me.ImageList1.Images.SetKeyName(53, "Center Across Cells.ico")
        Me.ImageList1.Images.SetKeyName(54, "Certificate.ico")
        Me.ImageList1.Images.SetKeyName(55, "Chart.ico")
        Me.ImageList1.Images.SetKeyName(56, "Chart-Bar.ico")
        Me.ImageList1.Images.SetKeyName(57, "Chart-Line.ico")
        Me.ImageList1.Images.SetKeyName(58, "Chart-Pie.ico")
        Me.ImageList1.Images.SetKeyName(59, "Chart-Point.ico")
        Me.ImageList1.Images.SetKeyName(60, "Clean.ico")
        Me.ImageList1.Images.SetKeyName(61, "Clear.ico")
        Me.ImageList1.Images.SetKeyName(62, "Clipboard.ico")
        Me.ImageList1.Images.SetKeyName(63, "Clock.ico")
        Me.ImageList1.Images.SetKeyName(64, "Close.ico")
        Me.ImageList1.Images.SetKeyName(65, "Colour Picker.ico")
        Me.ImageList1.Images.SetKeyName(66, "Columns-2.ico")
        Me.ImageList1.Images.SetKeyName(67, "Columns.ico")
        Me.ImageList1.Images.SetKeyName(68, "Command Prompt.ico")
        Me.ImageList1.Images.SetKeyName(69, "Compress.ico")
        Me.ImageList1.Images.SetKeyName(70, "Computer.ico")
        Me.ImageList1.Images.SetKeyName(71, "Connect.ico")
        Me.ImageList1.Images.SetKeyName(72, "Contact.ico")
        Me.ImageList1.Images.SetKeyName(73, "Contact-Add.ico")
        Me.ImageList1.Images.SetKeyName(74, "Contact-Delete.ico")
        Me.ImageList1.Images.SetKeyName(75, "Contact-Edit.ico")
        Me.ImageList1.Images.SetKeyName(76, "Contact-New.ico")
        Me.ImageList1.Images.SetKeyName(77, "Contact-Remove.ico")
        Me.ImageList1.Images.SetKeyName(78, "Contact-Search.ico")
        Me.ImageList1.Images.SetKeyName(79, "Copy.ico")
        Me.ImageList1.Images.SetKeyName(80, "Cross.ico")
        Me.ImageList1.Images.SetKeyName(81, "Currency.ico")
        Me.ImageList1.Images.SetKeyName(82, "Currency-Notes.ico")
        Me.ImageList1.Images.SetKeyName(83, "Cut.ico")
        Me.ImageList1.Images.SetKeyName(84, "Database.ico")
        Me.ImageList1.Images.SetKeyName(85, "Database-Add.ico")
        Me.ImageList1.Images.SetKeyName(86, "Database-Close.ico")
        Me.ImageList1.Images.SetKeyName(87, "Database-Delete.ico")
        Me.ImageList1.Images.SetKeyName(88, "Database-Edit.ico")
        Me.ImageList1.Images.SetKeyName(89, "Database-Filter.ico")
        Me.ImageList1.Images.SetKeyName(90, "Database-New.ico")
        Me.ImageList1.Images.SetKeyName(91, "Database-Open.ico")
        Me.ImageList1.Images.SetKeyName(92, "Database-Properties.ico")
        Me.ImageList1.Images.SetKeyName(93, "Database-Remove.ico")
        Me.ImageList1.Images.SetKeyName(94, "Database-Save.ico")
        Me.ImageList1.Images.SetKeyName(95, "Database-Schema.ico")
        Me.ImageList1.Images.SetKeyName(96, "Database-Search.ico")
        Me.ImageList1.Images.SetKeyName(97, "Database-Security.ico")
        Me.ImageList1.Images.SetKeyName(98, "Database-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(99, "Date-Time.ico")
        Me.ImageList1.Images.SetKeyName(100, "db-Add.ico")
        Me.ImageList1.Images.SetKeyName(101, "db-Cancel.ico")
        Me.ImageList1.Images.SetKeyName(102, "db-Delete.ico")
        Me.ImageList1.Images.SetKeyName(103, "db-Edit.ico")
        Me.ImageList1.Images.SetKeyName(104, "db-First.ico")
        Me.ImageList1.Images.SetKeyName(105, "db-Last.ico")
        Me.ImageList1.Images.SetKeyName(106, "db-Next.ico")
        Me.ImageList1.Images.SetKeyName(107, "db-Post.ico")
        Me.ImageList1.Images.SetKeyName(108, "db-Previous.ico")
        Me.ImageList1.Images.SetKeyName(109, "db-Refresh.ico")
        Me.ImageList1.Images.SetKeyName(110, "Decimals-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(111, "Decimals-Increase.ico")
        Me.ImageList1.Images.SetKeyName(112, "Decompress.ico")
        Me.ImageList1.Images.SetKeyName(113, "Delete.ico")
        Me.ImageList1.Images.SetKeyName(114, "Delete-Blue.ico")
        Me.ImageList1.Images.SetKeyName(115, "Design.ico")
        Me.ImageList1.Images.SetKeyName(116, "Desktop.ico")
        Me.ImageList1.Images.SetKeyName(117, "Dictionary.ico")
        Me.ImageList1.Images.SetKeyName(118, "Disconnect.ico")
        Me.ImageList1.Images.SetKeyName(119, "Discuss.ico")
        Me.ImageList1.Images.SetKeyName(120, "Doc-Access.ico")
        Me.ImageList1.Images.SetKeyName(121, "Doc-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(122, "Doc-Excel.ico")
        Me.ImageList1.Images.SetKeyName(123, "Doc-HTML.ico")
        Me.ImageList1.Images.SetKeyName(124, "Doc-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(125, "Doc-RTF.ico")
        Me.ImageList1.Images.SetKeyName(126, "Document-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(127, "Document-Protect.ico")
        Me.ImageList1.Images.SetKeyName(128, "Doc-Word.ico")
        Me.ImageList1.Images.SetKeyName(129, "Doc-XML.ico")
        Me.ImageList1.Images.SetKeyName(130, "Drawing.ico")
        Me.ImageList1.Images.SetKeyName(131, "Edit.ico")
        Me.ImageList1.Images.SetKeyName(132, "Equaliser.ico")
        Me.ImageList1.Images.SetKeyName(133, "Execute.ico")
        Me.ImageList1.Images.SetKeyName(134, "Exit.ico")
        Me.ImageList1.Images.SetKeyName(135, "Export.ico")
        Me.ImageList1.Images.SetKeyName(136, "Export-Access.ico")
        Me.ImageList1.Images.SetKeyName(137, "Export-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(138, "Export-CSV.ico")
        Me.ImageList1.Images.SetKeyName(139, "Export-Excel.ico")
        Me.ImageList1.Images.SetKeyName(140, "Export-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(141, "Export-HTML.ico")
        Me.ImageList1.Images.SetKeyName(142, "Export-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(143, "Export-RTF.ico")
        Me.ImageList1.Images.SetKeyName(144, "Export-Text.ico")
        Me.ImageList1.Images.SetKeyName(145, "Export-Word.ico")
        Me.ImageList1.Images.SetKeyName(146, "Export-XML.ico")
        Me.ImageList1.Images.SetKeyName(147, "Favorites.ico")
        Me.ImageList1.Images.SetKeyName(148, "Fill.ico")
        Me.ImageList1.Images.SetKeyName(149, "Fill-Down.ico")
        Me.ImageList1.Images.SetKeyName(150, "Fill-Left.ico")
        Me.ImageList1.Images.SetKeyName(151, "Fill-Right.ico")
        Me.ImageList1.Images.SetKeyName(152, "Fill-Up.ico")
        Me.ImageList1.Images.SetKeyName(153, "Filter.ico")
        Me.ImageList1.Images.SetKeyName(154, "Flag.ico")
        Me.ImageList1.Images.SetKeyName(155, "Flag-Delete.ico")
        Me.ImageList1.Images.SetKeyName(156, "Flag-Next.ico")
        Me.ImageList1.Images.SetKeyName(157, "Flag-Previous.ico")
        Me.ImageList1.Images.SetKeyName(158, "Flow Chart.ico")
        Me.ImageList1.Images.SetKeyName(159, "Folder List.ico")
        Me.ImageList1.Images.SetKeyName(160, "Folder.ico")
        Me.ImageList1.Images.SetKeyName(161, "Folder-Closed.ico")
        Me.ImageList1.Images.SetKeyName(162, "Folder-Delete.ico")
        Me.ImageList1.Images.SetKeyName(163, "Folder-New.ico")
        Me.ImageList1.Images.SetKeyName(164, "Folder-Options.ico")
        Me.ImageList1.Images.SetKeyName(165, "Folder-Search.ico")
        Me.ImageList1.Images.SetKeyName(166, "Footer.ico")
        Me.ImageList1.Images.SetKeyName(167, "Format Painter.ico")
        Me.ImageList1.Images.SetKeyName(168, "Format-Font Size.ico")
        Me.ImageList1.Images.SetKeyName(169, "Format-Font.ico")
        Me.ImageList1.Images.SetKeyName(170, "Format-Paragraph.ico")
        Me.ImageList1.Images.SetKeyName(171, "Format-Percentage.ico")
        Me.ImageList1.Images.SetKeyName(172, "Formatting-Remove.ico")
        Me.ImageList1.Images.SetKeyName(173, "Forward.ico")
        Me.ImageList1.Images.SetKeyName(174, "Full Screen.ico")
        Me.ImageList1.Images.SetKeyName(175, "Full Screen-New.ico")
        Me.ImageList1.Images.SetKeyName(176, "Function.ico")
        Me.ImageList1.Images.SetKeyName(177, "Go To Line.ico")
        Me.ImageList1.Images.SetKeyName(178, "Grid-2.ico")
        Me.ImageList1.Images.SetKeyName(179, "Grid.ico")
        Me.ImageList1.Images.SetKeyName(180, "Grid-Auto Format-2.ico")
        Me.ImageList1.Images.SetKeyName(181, "Grid-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(182, "Grid-Column Width.ico")
        Me.ImageList1.Images.SetKeyName(183, "Grid-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(184, "Grid-Delete Cell.ico")
        Me.ImageList1.Images.SetKeyName(185, "Grid-Delete Column.ico")
        Me.ImageList1.Images.SetKeyName(186, "Grid-Delete Row.ico")
        Me.ImageList1.Images.SetKeyName(187, "Grid-Delete.ico")
        Me.ImageList1.Images.SetKeyName(188, "Grid-Insert Cell.ico")
        Me.ImageList1.Images.SetKeyName(189, "Grid-Insert Column Left.ico")
        Me.ImageList1.Images.SetKeyName(190, "Grid-Insert Column Right.ico")
        Me.ImageList1.Images.SetKeyName(191, "Grid-Insert Column.ico")
        Me.ImageList1.Images.SetKeyName(192, "Grid-Insert Row Above.ico")
        Me.ImageList1.Images.SetKeyName(193, "Grid-Insert Row Below.ico")
        Me.ImageList1.Images.SetKeyName(194, "Grid-Insert Row.ico")
        Me.ImageList1.Images.SetKeyName(195, "Grid-Merge Cells.ico")
        Me.ImageList1.Images.SetKeyName(196, "Grid-New-2.ico")
        Me.ImageList1.Images.SetKeyName(197, "Grid-New.ico")
        Me.ImageList1.Images.SetKeyName(198, "Grid-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(199, "Grid-Options.ico")
        Me.ImageList1.Images.SetKeyName(200, "Grid-Row Height.ico")
        Me.ImageList1.Images.SetKeyName(201, "Grid-Select All-2.ico")
        Me.ImageList1.Images.SetKeyName(202, "Grid-Select All.ico")
        Me.ImageList1.Images.SetKeyName(203, "Grid-Select Cell-2.ico")
        Me.ImageList1.Images.SetKeyName(204, "Grid-Select Cell.ico")
        Me.ImageList1.Images.SetKeyName(205, "Grid-Select Column-2.ico")
        Me.ImageList1.Images.SetKeyName(206, "Grid-Select Column.ico")
        Me.ImageList1.Images.SetKeyName(207, "Grid-Select Row-2.ico")
        Me.ImageList1.Images.SetKeyName(208, "Grid-Select Row.ico")
        Me.ImageList1.Images.SetKeyName(209, "Grid-Split Cells.ico")
        Me.ImageList1.Images.SetKeyName(210, "Grid-Wizard-2.ico")
        Me.ImageList1.Images.SetKeyName(211, "Grid-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(212, "Hand Held.ico")
        Me.ImageList1.Images.SetKeyName(213, "Hand Shake.ico")
        Me.ImageList1.Images.SetKeyName(214, "Header and Footer.ico")
        Me.ImageList1.Images.SetKeyName(215, "Header and Footer-Toggle.ico")
        Me.ImageList1.Images.SetKeyName(216, "Header.ico")
        Me.ImageList1.Images.SetKeyName(217, "Header-Next.ico")
        Me.ImageList1.Images.SetKeyName(218, "Header-Previous.ico")
        Me.ImageList1.Images.SetKeyName(219, "Help.ico")
        Me.ImageList1.Images.SetKeyName(220, "Highlighter.ico")
        Me.ImageList1.Images.SetKeyName(221, "History.ico")
        Me.ImageList1.Images.SetKeyName(222, "Home.ico")
        Me.ImageList1.Images.SetKeyName(223, "Image.ico")
        Me.ImageList1.Images.SetKeyName(224, "Image-Brightness.ico")
        Me.ImageList1.Images.SetKeyName(225, "Image-Colour.ico")
        Me.ImageList1.Images.SetKeyName(226, "Image-Contrast.ico")
        Me.ImageList1.Images.SetKeyName(227, "Image-Crop.ico")
        Me.ImageList1.Images.SetKeyName(228, "Image-Flip.ico")
        Me.ImageList1.Images.SetKeyName(229, "Image-Mirror.ico")
        Me.ImageList1.Images.SetKeyName(230, "Image-Paint.ico")
        Me.ImageList1.Images.SetKeyName(231, "Image-Resize.ico")
        Me.ImageList1.Images.SetKeyName(232, "Image-Rotate.ico")
        Me.ImageList1.Images.SetKeyName(233, "Image-Select.ico")
        Me.ImageList1.Images.SetKeyName(234, "Import.ico")
        Me.ImageList1.Images.SetKeyName(235, "Import-Access.ico")
        Me.ImageList1.Images.SetKeyName(236, "Import-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(237, "Import-CSV.ico")
        Me.ImageList1.Images.SetKeyName(238, "Import-Excel.ico")
        Me.ImageList1.Images.SetKeyName(239, "Import-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(240, "Import-HTML.ico")
        Me.ImageList1.Images.SetKeyName(241, "Import-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(242, "Import-RTF.ico")
        Me.ImageList1.Images.SetKeyName(243, "Import-Text.ico")
        Me.ImageList1.Images.SetKeyName(244, "Import-Word.ico")
        Me.ImageList1.Images.SetKeyName(245, "Import-XML.ico")
        Me.ImageList1.Images.SetKeyName(246, "Indent-Decrease-2.ico")
        Me.ImageList1.Images.SetKeyName(247, "Indent-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(248, "Indent-Increase-2.ico")
        Me.ImageList1.Images.SetKeyName(249, "Indent-Increase.ico")
        Me.ImageList1.Images.SetKeyName(250, "Insert.ico")
        Me.ImageList1.Images.SetKeyName(251, "Insert-File.ico")
        Me.ImageList1.Images.SetKeyName(252, "Insert-Image.ico")
        Me.ImageList1.Images.SetKeyName(253, "Insert-Object.ico")
        Me.ImageList1.Images.SetKeyName(254, "Invoice.ico")
        Me.ImageList1.Images.SetKeyName(255, "Italic.ico")
        Me.ImageList1.Images.SetKeyName(256, "Key.ico")
        Me.ImageList1.Images.SetKeyName(257, "Landscape.ico")
        Me.ImageList1.Images.SetKeyName(258, "Launch.ico")
        Me.ImageList1.Images.SetKeyName(259, "Letter.ico")
        Me.ImageList1.Images.SetKeyName(260, "Line Spacing-2.ico")
        Me.ImageList1.Images.SetKeyName(261, "Line Spacing.ico")
        Me.ImageList1.Images.SetKeyName(262, "Link.ico")
        Me.ImageList1.Images.SetKeyName(263, "Lock.ico")
        Me.ImageList1.Images.SetKeyName(264, "Mail.ico")
        Me.ImageList1.Images.SetKeyName(265, "Mail-Forward.ico")
        Me.ImageList1.Images.SetKeyName(266, "Mail-In Box-2.ico")
        Me.ImageList1.Images.SetKeyName(267, "Mail-In Box.ico")
        Me.ImageList1.Images.SetKeyName(268, "Mail-In-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(269, "Mail-New.ico")
        Me.ImageList1.Images.SetKeyName(270, "Mail-Out Box-2.ico")
        Me.ImageList1.Images.SetKeyName(271, "Mail-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(272, "Mail-Reply To All.ico")
        Me.ImageList1.Images.SetKeyName(273, "Mail-Reply.ico")
        Me.ImageList1.Images.SetKeyName(274, "Mail-Search.ico")
        Me.ImageList1.Images.SetKeyName(275, "Mail-Send and Receive.ico")
        Me.ImageList1.Images.SetKeyName(276, "Media.ico")
        Me.ImageList1.Images.SetKeyName(277, "Microphone.ico")
        Me.ImageList1.Images.SetKeyName(278, "Midi Keyboard.ico")
        Me.ImageList1.Images.SetKeyName(279, "Minus.ico")
        Me.ImageList1.Images.SetKeyName(280, "mm-Eject.ico")
        Me.ImageList1.Images.SetKeyName(281, "mm-Fast Forward.ico")
        Me.ImageList1.Images.SetKeyName(282, "mm-First.ico")
        Me.ImageList1.Images.SetKeyName(283, "mm-Last.ico")
        Me.ImageList1.Images.SetKeyName(284, "mm-Pause.ico")
        Me.ImageList1.Images.SetKeyName(285, "mm-Play.ico")
        Me.ImageList1.Images.SetKeyName(286, "mm-Rewind.ico")
        Me.ImageList1.Images.SetKeyName(287, "mm-Stop.ico")
        Me.ImageList1.Images.SetKeyName(288, "Monitor.ico")
        Me.ImageList1.Images.SetKeyName(289, "Movie.ico")
        Me.ImageList1.Images.SetKeyName(290, "Musical Note-2.ico")
        Me.ImageList1.Images.SetKeyName(291, "Musical Note.ico")
        Me.ImageList1.Images.SetKeyName(292, "New.ico")
        Me.ImageList1.Images.SetKeyName(293, "Note.ico")
        Me.ImageList1.Images.SetKeyName(294, "Note-Add.ico")
        Me.ImageList1.Images.SetKeyName(295, "Note-Delete.ico")
        Me.ImageList1.Images.SetKeyName(296, "Note-Edit.ico")
        Me.ImageList1.Images.SetKeyName(297, "Note-New.ico")
        Me.ImageList1.Images.SetKeyName(298, "Note-Next.ico")
        Me.ImageList1.Images.SetKeyName(299, "Notepad.ico")
        Me.ImageList1.Images.SetKeyName(300, "Note-Previous.ico")
        Me.ImageList1.Images.SetKeyName(301, "Note-Remove.ico")
        Me.ImageList1.Images.SetKeyName(302, "Notes.ico")
        Me.ImageList1.Images.SetKeyName(303, "Note-Search.ico")
        Me.ImageList1.Images.SetKeyName(304, "Number of Pages.ico")
        Me.ImageList1.Images.SetKeyName(305, "Numbering-2.ico")
        Me.ImageList1.Images.SetKeyName(306, "Numbering.ico")
        Me.ImageList1.Images.SetKeyName(307, "Object-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(308, "Object-Select.ico")
        Me.ImageList1.Images.SetKeyName(309, "Object-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(310, "Open.ico")
        Me.ImageList1.Images.SetKeyName(311, "Outline-2.ico")
        Me.ImageList1.Images.SetKeyName(312, "Outline.ico")
        Me.ImageList1.Images.SetKeyName(313, "Page Number.ico")
        Me.ImageList1.Images.SetKeyName(314, "Page-Break.ico")
        Me.ImageList1.Images.SetKeyName(315, "Page-Invert.ico")
        Me.ImageList1.Images.SetKeyName(316, "Page-Next.ico")
        Me.ImageList1.Images.SetKeyName(317, "Page-Previous.ico")
        Me.ImageList1.Images.SetKeyName(318, "Page-Setup.ico")
        Me.ImageList1.Images.SetKeyName(319, "Paintbrush.ico")
        Me.ImageList1.Images.SetKeyName(320, "Parcel.ico")
        Me.ImageList1.Images.SetKeyName(321, "Paste.ico")
        Me.ImageList1.Images.SetKeyName(322, "Permission.ico")
        Me.ImageList1.Images.SetKeyName(323, "Phone.ico")
        Me.ImageList1.Images.SetKeyName(324, "Planner.ico")
        Me.ImageList1.Images.SetKeyName(325, "Plus.ico")
        Me.ImageList1.Images.SetKeyName(326, "Portrait.ico")
        Me.ImageList1.Images.SetKeyName(327, "Preview.ico")
        Me.ImageList1.Images.SetKeyName(328, "Print-2.ico")
        Me.ImageList1.Images.SetKeyName(329, "Print.ico")
        Me.ImageList1.Images.SetKeyName(330, "Print-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(331, "Print-Options.ico")
        Me.ImageList1.Images.SetKeyName(332, "Procedure.ico")
        Me.ImageList1.Images.SetKeyName(333, "Process.ico")
        Me.ImageList1.Images.SetKeyName(334, "Project.ico")
        Me.ImageList1.Images.SetKeyName(335, "Project-Gant Chart.ico")
        Me.ImageList1.Images.SetKeyName(336, "Properties.ico")
        Me.ImageList1.Images.SetKeyName(337, "Push Pin.ico")
        Me.ImageList1.Images.SetKeyName(338, "Recycle Bin.ico")
        Me.ImageList1.Images.SetKeyName(339, "Redo.ico")
        Me.ImageList1.Images.SetKeyName(340, "Refresh.ico")
        Me.ImageList1.Images.SetKeyName(341, "Remove.ico")
        Me.ImageList1.Images.SetKeyName(342, "Rename.ico")
        Me.ImageList1.Images.SetKeyName(343, "Report-2.ico")
        Me.ImageList1.Images.SetKeyName(344, "Report.ico")
        Me.ImageList1.Images.SetKeyName(345, "Report-Bound.ico")
        Me.ImageList1.Images.SetKeyName(346, "Research.ico")
        Me.ImageList1.Images.SetKeyName(347, "Ruler.ico")
        Me.ImageList1.Images.SetKeyName(348, "Ruler-Formatting.ico")
        Me.ImageList1.Images.SetKeyName(349, "Save All.ico")
        Me.ImageList1.Images.SetKeyName(350, "Save As HTML.ico")
        Me.ImageList1.Images.SetKeyName(351, "Save As.ico")
        Me.ImageList1.Images.SetKeyName(352, "Save Draft.ico")
        Me.ImageList1.Images.SetKeyName(353, "Save.ico")
        Me.ImageList1.Images.SetKeyName(354, "Script.ico")
        Me.ImageList1.Images.SetKeyName(355, "Search.ico")
        Me.ImageList1.Images.SetKeyName(356, "Search-Next.ico")
        Me.ImageList1.Images.SetKeyName(357, "Search-Previous.ico")
        Me.ImageList1.Images.SetKeyName(358, "Search-Replace.ico")
        Me.ImageList1.Images.SetKeyName(359, "Select All.ico")
        Me.ImageList1.Images.SetKeyName(360, "Slide Show.ico")
        Me.ImageList1.Images.SetKeyName(361, "Slide.ico")
        Me.ImageList1.Images.SetKeyName(362, "Software-Analyse.ico")
        Me.ImageList1.Images.SetKeyName(363, "Sort-Ascending.ico")
        Me.ImageList1.Images.SetKeyName(364, "Sort-Descending.ico")
        Me.ImageList1.Images.SetKeyName(365, "Sound-Delete.ico")
        Me.ImageList1.Images.SetKeyName(366, "Sound-Fade In.ico")
        Me.ImageList1.Images.SetKeyName(367, "Sound-Fade Out.ico")
        Me.ImageList1.Images.SetKeyName(368, "Sound-Join.ico")
        Me.ImageList1.Images.SetKeyName(369, "Sound-Mark End.ico")
        Me.ImageList1.Images.SetKeyName(370, "Sound-Mark Start.ico")
        Me.ImageList1.Images.SetKeyName(371, "Sound-Split.ico")
        Me.ImageList1.Images.SetKeyName(372, "Spell Check.ico")
        Me.ImageList1.Images.SetKeyName(373, "Stop.ico")
        Me.ImageList1.Images.SetKeyName(374, "Strikeout.ico")
        Me.ImageList1.Images.SetKeyName(375, "Subscript.ico")
        Me.ImageList1.Images.SetKeyName(376, "Sum.ico")
        Me.ImageList1.Images.SetKeyName(377, "Summary.ico")
        Me.ImageList1.Images.SetKeyName(378, "Superscript.ico")
        Me.ImageList1.Images.SetKeyName(379, "Support.ico")
        Me.ImageList1.Images.SetKeyName(380, "Tables and Borders.ico")
        Me.ImageList1.Images.SetKeyName(381, "Task.ico")
        Me.ImageList1.Images.SetKeyName(382, "Task-Search.ico")
        Me.ImageList1.Images.SetKeyName(383, "Thesaurus.ico")
        Me.ImageList1.Images.SetKeyName(384, "Tick.ico")
        Me.ImageList1.Images.SetKeyName(385, "Time Line.ico")
        Me.ImageList1.Images.SetKeyName(386, "Tip.ico")
        Me.ImageList1.Images.SetKeyName(387, "To Do List.ico")
        Me.ImageList1.Images.SetKeyName(388, "To Lowercase.ico")
        Me.ImageList1.Images.SetKeyName(389, "To Uppercase.ico")
        Me.ImageList1.Images.SetKeyName(390, "Tools.ico")
        Me.ImageList1.Images.SetKeyName(391, "Tree View.ico")
        Me.ImageList1.Images.SetKeyName(392, "Tree View-Add.ico")
        Me.ImageList1.Images.SetKeyName(393, "Tree View-Delete.ico")
        Me.ImageList1.Images.SetKeyName(394, "Tree View-Edit.ico")
        Me.ImageList1.Images.SetKeyName(395, "Tree View-New.ico")
        Me.ImageList1.Images.SetKeyName(396, "Tree View-Remove.ico")
        Me.ImageList1.Images.SetKeyName(397, "Tree View-Search.ico")
        Me.ImageList1.Images.SetKeyName(398, "Underline.ico")
        Me.ImageList1.Images.SetKeyName(399, "Undo.ico")
        Me.ImageList1.Images.SetKeyName(400, "Unlock.ico")
        Me.ImageList1.Images.SetKeyName(401, "User-2.ico")
        Me.ImageList1.Images.SetKeyName(402, "User.ico")
        Me.ImageList1.Images.SetKeyName(403, "User-Add-2.ico")
        Me.ImageList1.Images.SetKeyName(404, "User-Add.ico")
        Me.ImageList1.Images.SetKeyName(405, "User-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(406, "User-Delete.ico")
        Me.ImageList1.Images.SetKeyName(407, "User-Edit-2.ico")
        Me.ImageList1.Images.SetKeyName(408, "User-Edit.ico")
        Me.ImageList1.Images.SetKeyName(409, "User-New-2.ico")
        Me.ImageList1.Images.SetKeyName(410, "User-New.ico")
        Me.ImageList1.Images.SetKeyName(411, "User-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(412, "User-Options.ico")
        Me.ImageList1.Images.SetKeyName(413, "User-Password-2.ico")
        Me.ImageList1.Images.SetKeyName(414, "User-Password.ico")
        Me.ImageList1.Images.SetKeyName(415, "User-Remove-2.ico")
        Me.ImageList1.Images.SetKeyName(416, "User-Remove.ico")
        Me.ImageList1.Images.SetKeyName(417, "Users-2.ico")
        Me.ImageList1.Images.SetKeyName(418, "Users.ico")
        Me.ImageList1.Images.SetKeyName(419, "User-Search-2.ico")
        Me.ImageList1.Images.SetKeyName(420, "User-Search.ico")
        Me.ImageList1.Images.SetKeyName(421, "User-Security-2.ico")
        Me.ImageList1.Images.SetKeyName(422, "User-Security.ico")
        Me.ImageList1.Images.SetKeyName(423, "View-Details.ico")
        Me.ImageList1.Images.SetKeyName(424, "View-Large Icons.ico")
        Me.ImageList1.Images.SetKeyName(425, "View-List.ico")
        Me.ImageList1.Images.SetKeyName(426, "View-One Page.ico")
        Me.ImageList1.Images.SetKeyName(427, "View-Page Width.ico")
        Me.ImageList1.Images.SetKeyName(428, "Views.ico")
        Me.ImageList1.Images.SetKeyName(429, "View-Small Icons.ico")
        Me.ImageList1.Images.SetKeyName(430, "Volume.ico")
        Me.ImageList1.Images.SetKeyName(431, "Volume-Mute.ico")
        Me.ImageList1.Images.SetKeyName(432, "Warning.ico")
        Me.ImageList1.Images.SetKeyName(433, "Waste Bin.ico")
        Me.ImageList1.Images.SetKeyName(434, "Window.ico")
        Me.ImageList1.Images.SetKeyName(435, "Window-Add.ico")
        Me.ImageList1.Images.SetKeyName(436, "Window-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(437, "Window-Cascade.ico")
        Me.ImageList1.Images.SetKeyName(438, "Window-Close.ico")
        Me.ImageList1.Images.SetKeyName(439, "Window-Delete.ico")
        Me.ImageList1.Images.SetKeyName(440, "Window-Edit.ico")
        Me.ImageList1.Images.SetKeyName(441, "Window-Maximise.ico")
        Me.ImageList1.Images.SetKeyName(442, "Window-Minimise.ico")
        Me.ImageList1.Images.SetKeyName(443, "Window-New.ico")
        Me.ImageList1.Images.SetKeyName(444, "Window-Next.ico")
        Me.ImageList1.Images.SetKeyName(445, "Window-Options.ico")
        Me.ImageList1.Images.SetKeyName(446, "Window-Preview.ico")
        Me.ImageList1.Images.SetKeyName(447, "Window-Previous.ico")
        Me.ImageList1.Images.SetKeyName(448, "Window-Properties.ico")
        Me.ImageList1.Images.SetKeyName(449, "Window-Remove.ico")
        Me.ImageList1.Images.SetKeyName(450, "Window-Search.ico")
        Me.ImageList1.Images.SetKeyName(451, "Window-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(452, "Window-Split.ico")
        Me.ImageList1.Images.SetKeyName(453, "Window-Tile Horizontal.ico")
        Me.ImageList1.Images.SetKeyName(454, "Window-Tile Vertical.ico")
        Me.ImageList1.Images.SetKeyName(455, "Wizard.ico")
        Me.ImageList1.Images.SetKeyName(456, "Word Count.ico")
        Me.ImageList1.Images.SetKeyName(457, "Word Wrap-2.ico")
        Me.ImageList1.Images.SetKeyName(458, "Word Wrap.ico")
        Me.ImageList1.Images.SetKeyName(459, "Work Sheet.ico")
        Me.ImageList1.Images.SetKeyName(460, "Work Sheet-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(461, "Work Sheet-Delete.ico")
        Me.ImageList1.Images.SetKeyName(462, "Work Sheet-New.ico")
        Me.ImageList1.Images.SetKeyName(463, "Work Sheet-Options.ico")
        Me.ImageList1.Images.SetKeyName(464, "Work Sheet-Protect.ico")
        Me.ImageList1.Images.SetKeyName(465, "Work Sheet-Rename.ico")
        Me.ImageList1.Images.SetKeyName(466, "Zoom-In.ico")
        Me.ImageList1.Images.SetKeyName(467, "Zoom-Out.ico")
        '
        'picPhoto
        '
        Me.picPhoto.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.picPhoto.Location = New System.Drawing.Point(13, 38)
        Me.picPhoto.Name = "picPhoto"
        Me.picPhoto.Properties.LookAndFeel.SkinName = "Black"
        Me.picPhoto.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Zoom
        Me.picPhoto.Size = New System.Drawing.Size(370, 396)
        Me.picPhoto.TabIndex = 0
        '
        'txtID
        '
        Me.txtID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_CustomerPhotosBindingSource, "CustomerPhotosID", True))
        Me.txtID.Enabled = False
        Me.txtID.Location = New System.Drawing.Point(520, 37)
        Me.txtID.Name = "txtID"
        Me.txtID.Properties.LookAndFeel.SkinName = "Black"
        Me.txtID.Size = New System.Drawing.Size(108, 20)
        Me.txtID.TabIndex = 215
        '
        'EUS_CustomerPhotosBindingSource
        '
        Me.EUS_CustomerPhotosBindingSource.DataMember = "EUS_CustomerPhotos"
        Me.EUS_CustomerPhotosBindingSource.DataSource = Me.DSMembers
        '
        'DSMembers
        '
        Me.DSMembers.DataSetName = "DSMembers"
        Me.DSMembers.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LabelControl4
        '
        Me.LabelControl4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl4.Location = New System.Drawing.Point(493, 44)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl4.TabIndex = 216
        Me.LabelControl4.Text = "ID :"
        '
        'txtProfileID
        '
        Me.txtProfileID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtProfileID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_CustomerPhotosBindingSource, "CustomerID", True))
        Me.txtProfileID.Enabled = False
        Me.txtProfileID.Location = New System.Drawing.Point(520, 63)
        Me.txtProfileID.Name = "txtProfileID"
        Me.txtProfileID.Properties.LookAndFeel.SkinName = "Black"
        Me.txtProfileID.Size = New System.Drawing.Size(108, 20)
        Me.txtProfileID.TabIndex = 217
        '
        'LabelControl1
        '
        Me.LabelControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(443, 70)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl1.TabIndex = 218
        Me.LabelControl1.Text = "Profile ID :"
        '
        'LabelControl2
        '
        Me.LabelControl2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(419, 96)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(95, 13)
        Me.LabelControl2.TabIndex = 220
        Me.LabelControl2.Text = "Date Uploaded :"
        '
        'txtPhotoFileName
        '
        Me.txtPhotoFileName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPhotoFileName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_CustomerPhotosBindingSource, "FileName", True))
        Me.txtPhotoFileName.Location = New System.Drawing.Point(520, 115)
        Me.txtPhotoFileName.Name = "txtPhotoFileName"
        Me.txtPhotoFileName.Properties.LookAndFeel.SkinName = "Black"
        Me.txtPhotoFileName.Size = New System.Drawing.Size(247, 20)
        Me.txtPhotoFileName.TabIndex = 221
        '
        'LabelControl3
        '
        Me.LabelControl3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(443, 122)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(71, 13)
        Me.LabelControl3.TabIndex = 222
        Me.LabelControl3.Text = "File Name :"
        '
        'LabelControl5
        '
        Me.LabelControl5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl5.Location = New System.Drawing.Point(432, 148)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl5.TabIndex = 224
        Me.LabelControl5.Text = "Display Level :"
        '
        'LabelControl6
        '
        Me.LabelControl6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl6.Location = New System.Drawing.Point(432, 174)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl6.TabIndex = 226
        Me.LabelControl6.Text = "Is Aproved :"
        '
        'LabelControl7
        '
        Me.LabelControl7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl7.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl7.Location = New System.Drawing.Point(545, 174)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(82, 13)
        Me.LabelControl7.TabIndex = 228
        Me.LabelControl7.Text = "Is Declined :"
        '
        'txtCheckedContextID
        '
        Me.txtCheckedContextID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCheckedContextID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_CustomerPhotosBindingSource, "CheckedContextID", True))
        Me.txtCheckedContextID.Location = New System.Drawing.Point(520, 221)
        Me.txtCheckedContextID.Name = "txtCheckedContextID"
        Me.txtCheckedContextID.Properties.LookAndFeel.SkinName = "Black"
        Me.txtCheckedContextID.Size = New System.Drawing.Size(108, 20)
        Me.txtCheckedContextID.TabIndex = 229
        '
        'LabelControl8
        '
        Me.LabelControl8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl8.Location = New System.Drawing.Point(391, 228)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(123, 13)
        Me.LabelControl8.TabIndex = 230
        Me.LabelControl8.Text = "Checked Context ID :"
        '
        'LabelControl9
        '
        Me.LabelControl9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl9.Location = New System.Drawing.Point(391, 199)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(123, 13)
        Me.LabelControl9.TabIndex = 232
        Me.LabelControl9.Text = "Is Default :"
        '
        'dtUploaded
        '
        Me.dtUploaded.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtUploaded.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_CustomerPhotosBindingSource, "DateTimeToUploading", True))
        Me.dtUploaded.EditValue = Nothing
        Me.dtUploaded.Location = New System.Drawing.Point(520, 89)
        Me.dtUploaded.Name = "dtUploaded"
        Me.dtUploaded.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtUploaded.Properties.LookAndFeel.SkinName = "Black"
        Me.dtUploaded.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dtUploaded.Size = New System.Drawing.Size(247, 20)
        Me.dtUploaded.TabIndex = 278
        '
        'icbDisplayLevelId
        '
        Me.icbDisplayLevelId.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.icbDisplayLevelId.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_CustomerPhotosBindingSource, "DisplayLevel", True))
        Me.icbDisplayLevelId.Location = New System.Drawing.Point(520, 141)
        Me.icbDisplayLevelId.Name = "icbDisplayLevelId"
        Me.icbDisplayLevelId.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.icbDisplayLevelId.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.icbDisplayLevelId.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbDisplayLevelId.Properties.LookAndFeel.SkinName = "Black"
        Me.icbDisplayLevelId.Size = New System.Drawing.Size(247, 20)
        Me.icbDisplayLevelId.TabIndex = 405
        '
        'chkIsDefault
        '
        Me.chkIsDefault.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkIsDefault.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_CustomerPhotosBindingSource, "IsDefault", True))
        Me.chkIsDefault.Location = New System.Drawing.Point(520, 196)
        Me.chkIsDefault.Name = "chkIsDefault"
        Me.chkIsDefault.Properties.Caption = ""
        Me.chkIsDefault.Properties.LookAndFeel.SkinName = "Black"
        Me.chkIsDefault.Size = New System.Drawing.Size(19, 19)
        Me.chkIsDefault.TabIndex = 408
        '
        'cmdSave1
        '
        Me.cmdSave1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSave1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.cmdSave1.Appearance.Options.UseFont = True
        Me.cmdSave1.ImageIndex = 107
        Me.cmdSave1.ImageList = Me.ImageList1
        Me.cmdSave1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.cmdSave1.Location = New System.Drawing.Point(577, 401)
        Me.cmdSave1.Name = "cmdSave1"
        Me.cmdSave1.Size = New System.Drawing.Size(92, 34)
        Me.cmdSave1.TabIndex = 245
        Me.cmdSave1.Text = "Save"
        '
        'cmdCancel1
        '
        Me.cmdCancel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.cmdCancel1.Appearance.Options.UseFont = True
        Me.cmdCancel1.ImageIndex = 399
        Me.cmdCancel1.ImageList = Me.ImageList1
        Me.cmdCancel1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.cmdCancel1.Location = New System.Drawing.Point(675, 400)
        Me.cmdCancel1.Name = "cmdCancel1"
        Me.cmdCancel1.Size = New System.Drawing.Size(92, 34)
        Me.cmdCancel1.TabIndex = 246
        Me.cmdCancel1.Text = "Cancel"
        '
        'LabelControl10
        '
        Me.LabelControl10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl10.Location = New System.Drawing.Point(391, 266)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(123, 13)
        Me.LabelControl10.TabIndex = 409
        Me.LabelControl10.Text = "Dimentions (W,H):"
        '
        'lblWidthHeightInfo
        '
        Me.lblWidthHeightInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWidthHeightInfo.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblWidthHeightInfo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblWidthHeightInfo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblWidthHeightInfo.Location = New System.Drawing.Point(522, 266)
        Me.lblWidthHeightInfo.Name = "lblWidthHeightInfo"
        Me.lblWidthHeightInfo.Size = New System.Drawing.Size(105, 13)
        Me.lblWidthHeightInfo.TabIndex = 410
        Me.lblWidthHeightInfo.Text = "[W,H]"
        '
        'LabelControl12
        '
        Me.LabelControl12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl12.Location = New System.Drawing.Point(392, 304)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(123, 13)
        Me.LabelControl12.TabIndex = 411
        Me.LabelControl12.Text = "Type :"
        '
        'lblTypeInfo
        '
        Me.lblTypeInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTypeInfo.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblTypeInfo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblTypeInfo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblTypeInfo.Location = New System.Drawing.Point(523, 304)
        Me.lblTypeInfo.Name = "lblTypeInfo"
        Me.lblTypeInfo.Size = New System.Drawing.Size(105, 13)
        Me.lblTypeInfo.TabIndex = 412
        Me.lblTypeInfo.Text = "[EXT]"
        '
        'LabelControl14
        '
        Me.LabelControl14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl14.Location = New System.Drawing.Point(475, 285)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(40, 13)
        Me.LabelControl14.TabIndex = 413
        Me.LabelControl14.Text = "Size :"
        '
        'lblSizeInfo
        '
        Me.lblSizeInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSizeInfo.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblSizeInfo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblSizeInfo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblSizeInfo.Location = New System.Drawing.Point(523, 285)
        Me.lblSizeInfo.Name = "lblSizeInfo"
        Me.lblSizeInfo.Size = New System.Drawing.Size(105, 13)
        Me.lblSizeInfo.TabIndex = 414
        Me.lblSizeInfo.Text = "[B]"
        '
        'gcPhoto
        '
        Me.gcPhoto.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold)
        Me.gcPhoto.AppearanceCaption.Options.UseFont = True
        Me.gcPhoto.Controls.Add(Me.lblSizeInfo)
        Me.gcPhoto.Controls.Add(Me.LabelControl14)
        Me.gcPhoto.Controls.Add(Me.lblTypeInfo)
        Me.gcPhoto.Controls.Add(Me.LabelControl12)
        Me.gcPhoto.Controls.Add(Me.lblWidthHeightInfo)
        Me.gcPhoto.Controls.Add(Me.LabelControl10)
        Me.gcPhoto.Controls.Add(Me.cmdCancel1)
        Me.gcPhoto.Controls.Add(Me.cmdSave1)
        Me.gcPhoto.Controls.Add(Me.chkIsDefault)
        Me.gcPhoto.Controls.Add(Me.chkHasDeclined)
        Me.gcPhoto.Controls.Add(Me.chkHasAproved)
        Me.gcPhoto.Controls.Add(Me.icbDisplayLevelId)
        Me.gcPhoto.Controls.Add(Me.dtUploaded)
        Me.gcPhoto.Controls.Add(Me.LabelControl9)
        Me.gcPhoto.Controls.Add(Me.LabelControl8)
        Me.gcPhoto.Controls.Add(Me.txtCheckedContextID)
        Me.gcPhoto.Controls.Add(Me.LabelControl7)
        Me.gcPhoto.Controls.Add(Me.LabelControl6)
        Me.gcPhoto.Controls.Add(Me.LabelControl5)
        Me.gcPhoto.Controls.Add(Me.LabelControl3)
        Me.gcPhoto.Controls.Add(Me.txtPhotoFileName)
        Me.gcPhoto.Controls.Add(Me.LabelControl2)
        Me.gcPhoto.Controls.Add(Me.LabelControl1)
        Me.gcPhoto.Controls.Add(Me.txtProfileID)
        Me.gcPhoto.Controls.Add(Me.LabelControl4)
        Me.gcPhoto.Controls.Add(Me.txtID)
        Me.gcPhoto.Controls.Add(Me.picPhoto)
        Me.gcPhoto.Controls.Add(Me.txtPhotoURL)
        Me.gcPhoto.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcPhoto.Location = New System.Drawing.Point(0, 0)
        Me.gcPhoto.Name = "gcPhoto"
        Me.gcPhoto.Size = New System.Drawing.Size(779, 446)
        Me.gcPhoto.TabIndex = 0
        Me.gcPhoto.Text = "Member's Photo"
        '
        'chkHasDeclined
        '
        Me.chkHasDeclined.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkHasDeclined.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_CustomerPhotosBindingSource, "HasDeclined", True))
        Me.chkHasDeclined.Location = New System.Drawing.Point(633, 171)
        Me.chkHasDeclined.Name = "chkHasDeclined"
        Me.chkHasDeclined.Properties.Caption = ""
        Me.chkHasDeclined.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.chkHasDeclined.Properties.LookAndFeel.SkinName = "Black"
        Me.chkHasDeclined.Properties.RadioGroupIndex = 1
        Me.chkHasDeclined.Size = New System.Drawing.Size(19, 19)
        Me.chkHasDeclined.TabIndex = 407
        Me.chkHasDeclined.TabStop = False
        '
        'chkHasAproved
        '
        Me.chkHasAproved.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkHasAproved.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_CustomerPhotosBindingSource, "HasAproved", True))
        Me.chkHasAproved.Location = New System.Drawing.Point(520, 171)
        Me.chkHasAproved.Name = "chkHasAproved"
        Me.chkHasAproved.Properties.Caption = ""
        Me.chkHasAproved.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.chkHasAproved.Properties.LookAndFeel.SkinName = "Black"
        Me.chkHasAproved.Properties.RadioGroupIndex = 1
        Me.chkHasAproved.Size = New System.Drawing.Size(19, 19)
        Me.chkHasAproved.TabIndex = 406
        Me.chkHasAproved.TabStop = False
        '
        'txtPhotoURL
        '
        Me.txtPhotoURL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPhotoURL.Location = New System.Drawing.Point(392, 331)
        Me.txtPhotoURL.Name = "txtPhotoURL"
        Me.txtPhotoURL.Properties.LookAndFeel.SkinName = "Black"
        Me.txtPhotoURL.Properties.ReadOnly = True
        Me.txtPhotoURL.Properties.UseParentBackground = True
        Me.txtPhotoURL.Size = New System.Drawing.Size(375, 56)
        Me.txtPhotoURL.TabIndex = 416
        '
        'DSLists
        '
        Me.DSLists.DataSetName = "DSLists"
        Me.DSLists.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EUS_LISTS_PhotosDisplayLevelBindingSource
        '
        Me.EUS_LISTS_PhotosDisplayLevelBindingSource.DataMember = "EUS_LISTS_PhotosDisplayLevel"
        Me.EUS_LISTS_PhotosDisplayLevelBindingSource.DataSource = Me.DSLists
        '
        'frmDDProfilePhoto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(779, 446)
        Me.Controls.Add(Me.gcPhoto)
        Me.Name = "frmDDProfilePhoto"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Terminal Cart"
        CType(Me.picPhoto.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_CustomerPhotosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSMembers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtProfileID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPhotoFileName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCheckedContextID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtUploaded.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtUploaded.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbDisplayLevelId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIsDefault.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcPhoto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcPhoto.ResumeLayout(False)
        CType(Me.chkHasDeclined.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkHasAproved.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPhotoURL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_LISTS_PhotosDisplayLevelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents picPhoto As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents txtID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtProfileID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtPhotoFileName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtCheckedContextID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dtUploaded As DevExpress.XtraEditors.DateEdit
    Friend WithEvents icbDisplayLevelId As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents chkIsDefault As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents cmdSave1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdCancel1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblWidthHeightInfo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblTypeInfo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblSizeInfo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcPhoto As DevExpress.XtraEditors.GroupControl
    Friend WithEvents chkHasDeclined As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkHasAproved As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txtPhotoURL As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents EUS_CustomerPhotosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DSMembers As Dating.Client.Admin.APP.AdminWS.DSMembers
    Friend WithEvents DSLists As Dating.Client.Admin.APP.AdminWS.DSLists
    Friend WithEvents EUS_LISTS_PhotosDisplayLevelBindingSource As System.Windows.Forms.BindingSource
End Class

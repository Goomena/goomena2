﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRejectReasonItem
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txMainTitle = New DevExpress.XtraEditors.TextEdit()
        Me.EUS_LISTS_RejectingReasonsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DSLists = New Dating.Client.Admin.APP.AdminWS.DSLists()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.UchtmlEditor1 = New Dating.Client.Admin.APP.UCHTMLEditor()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CmdOK = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.cbLAG = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.CmdSAVE = New DevExpress.XtraEditors.SimpleButton()
        Me.txtOtherLagTitle = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.txMainTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_LISTS_RejectingReasonsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbLAG.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtOtherLagTitle.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txMainTitle
        '
        Me.txMainTitle.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_LISTS_RejectingReasonsBindingSource, "US", True))
        Me.txMainTitle.Location = New System.Drawing.Point(121, 13)
        Me.txMainTitle.Name = "txMainTitle"
        Me.txMainTitle.Size = New System.Drawing.Size(469, 20)
        Me.txMainTitle.TabIndex = 0
        '
        'EUS_LISTS_RejectingReasonsBindingSource
        '
        Me.EUS_LISTS_RejectingReasonsBindingSource.DataMember = "EUS_LISTS_RejectingReasons"
        Me.EUS_LISTS_RejectingReasonsBindingSource.DataSource = Me.DSLists
        '
        'DSLists
        '
        Me.DSLists.DataSetName = "DSLists"
        Me.DSLists.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(24, 11)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(91, 22)
        Me.LabelControl1.TabIndex = 6
        Me.LabelControl1.Text = "US Title :"
        '
        'UchtmlEditor1
        '
        Me.UchtmlEditor1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UchtmlEditor1.Location = New System.Drawing.Point(2, 21)
        Me.UchtmlEditor1.Name = "UchtmlEditor1"
        Me.UchtmlEditor1.Size = New System.Drawing.Size(656, 341)
        Me.UchtmlEditor1.TabIndex = 7
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.UchtmlEditor1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 65)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(660, 364)
        Me.GroupControl1.TabIndex = 8
        Me.GroupControl1.Text = "Body Message"
        '
        'CmdOK
        '
        Me.CmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdOK.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.CmdOK.Appearance.Options.UseFont = True
        Me.CmdOK.Location = New System.Drawing.Point(355, 435)
        Me.CmdOK.Name = "CmdOK"
        Me.CmdOK.Size = New System.Drawing.Size(168, 39)
        Me.CmdOK.TabIndex = 9
        Me.CmdOK.Text = "SAVE && CLOSE"
        '
        'CmdCancel
        '
        Me.CmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.CmdCancel.Appearance.Options.UseFont = True
        Me.CmdCancel.Location = New System.Drawing.Point(529, 435)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.Size = New System.Drawing.Size(128, 39)
        Me.CmdCancel.TabIndex = 10
        Me.CmdCancel.Text = "Cancel"
        '
        'cbLAG
        '
        Me.cbLAG.EditValue = "US"
        Me.cbLAG.Location = New System.Drawing.Point(600, 13)
        Me.cbLAG.Name = "cbLAG"
        Me.cbLAG.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.cbLAG.Properties.Appearance.Options.UseFont = True
        Me.cbLAG.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLAG.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.ImageComboBoxItem() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("US", "US", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("GR", "GR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("HU", "HU", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("DE", "DE", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("AL", "AL", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("SR", "SR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("RU", "RU", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("RO", "RO", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("BG", "BG", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("TR", "TR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("FR", "FR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("ES", "ES", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("IT", "IT", -1)})
        Me.cbLAG.Size = New System.Drawing.Size(72, 20)
        Me.cbLAG.TabIndex = 12
        '
        'CmdSAVE
        '
        Me.CmdSAVE.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdSAVE.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.CmdSAVE.Appearance.Options.UseFont = True
        Me.CmdSAVE.Location = New System.Drawing.Point(181, 435)
        Me.CmdSAVE.Name = "CmdSAVE"
        Me.CmdSAVE.Size = New System.Drawing.Size(168, 39)
        Me.CmdSAVE.TabIndex = 13
        Me.CmdSAVE.Text = "SAVE"
        '
        'txtOtherLagTitle
        '
        Me.txtOtherLagTitle.Location = New System.Drawing.Point(121, 39)
        Me.txtOtherLagTitle.Name = "txtOtherLagTitle"
        Me.txtOtherLagTitle.Size = New System.Drawing.Size(469, 20)
        Me.txtOtherLagTitle.TabIndex = 14
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(11, 37)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(104, 22)
        Me.LabelControl3.TabIndex = 15
        Me.LabelControl3.Text = "Other Lag Title :"
        '
        'frmEditRejectReasonItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(677, 486)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.txtOtherLagTitle)
        Me.Controls.Add(Me.CmdSAVE)
        Me.Controls.Add(Me.cbLAG)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdOK)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.txMainTitle)
        Me.Name = "frmEditRejectReasonItem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Page Custom Item"
        CType(Me.txMainTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_LISTS_RejectingReasonsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.cbLAG.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtOtherLagTitle.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txMainTitle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents UchtmlEditor1 As UCHTMLEditor
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CmdOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbLAG As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents CmdSAVE As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DSLists As Dating.Client.Admin.APP.AdminWS.DSLists
    Friend WithEvents EUS_LISTS_RejectingReasonsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents txtOtherLagTitle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
End Class

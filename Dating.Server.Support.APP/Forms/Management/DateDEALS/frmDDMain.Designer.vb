﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDDMain
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDDMain))
        Me.DxErrorProvider1 = New DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(Me.components)
        Me.xtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.NavBarMarketing = New DevExpress.XtraNavBar.NavBarControl()
        Me.NavBarGroupProfiles = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavBarItemAutoApprovedProfiles = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItemAutoApprovedPhotos = New DevExpress.XtraNavBar.NavBarItem()
        Me.navProfiles = New DevExpress.XtraNavBar.NavBarItem()
        Me.navNewProfiles = New DevExpress.XtraNavBar.NavBarItem()
        Me.navUpdatingProfiles = New DevExpress.XtraNavBar.NavBarItem()
        Me.navApprovedProfiles = New DevExpress.XtraNavBar.NavBarItem()
        Me.navRejectedNewProfiles = New DevExpress.XtraNavBar.NavBarItem()
        Me.navDeletedProfiles = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavDeletedByUser = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItemNewPhotos = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItemApprovedPhotos = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItemDeletedPhotos = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarGroupMarketingTools = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavEmailOfferSettings = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarEmailMessages = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarGroupFiles = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavBarGroupLists = New DevExpress.XtraNavBar.NavBarGroup()
        Me.navSYS_CountriesGEO = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_BodyType = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_ChildrenNumber = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_Drinking = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_Education = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_Ethnicity = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_EyeColor = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_HairColor = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_Height = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_Income = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_NetWorth = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_RejectingReasons = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_RelationshipStatus = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_Religion = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_Smoking = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_AccountType = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_Gender = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_PhotosDisplayLevel = New DevExpress.XtraNavBar.NavBarItem()
        Me.navEUS_LISTS_TypeOfDating = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarGroupConfig2 = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavBarItemAutoApprove = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarGroupTools = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavBarItemTools_UpdateGEOGR = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItemTools_ReplaceDBString = New DevExpress.XtraNavBar.NavBarItem()
        Me.ImageCollectionNaviSmall = New DevExpress.Utils.ImageCollection(Me.components)
        Me.NavAgencysService = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavMenus = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavPrograms = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavUsers = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavLoginAccessHistory = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavWorksFromUsers = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavRoles = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavInstalledPrograms = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavLicenses = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavFeedbackActivity = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavCustomers = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavSMSAccounts = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavSMSTranstactions = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavProviders = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavSalesSites = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavProducts = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavTransactions = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavPages = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBasic = New DevExpress.XtraNavBar.NavBarItem()
        Me.SplitterControl1 = New DevExpress.XtraEditors.SplitterControl()
        Me.barManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.bar2 = New DevExpress.XtraBars.Bar()
        Me.siFile = New DevExpress.XtraBars.BarSubItem()
        Me.iSwitchUser = New DevExpress.XtraBars.BarButtonItem()
        Me.iPrint = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iExit = New DevExpress.XtraBars.BarButtonItem()
        Me.siView = New DevExpress.XtraBars.BarSubItem()
        Me.iToolBars = New DevExpress.XtraBars.BarToolbarsListItem()
        Me.siTools = New DevExpress.XtraBars.BarSubItem()
        Me.iOptions = New DevExpress.XtraBars.BarButtonItem()
        Me.siHelp = New DevExpress.XtraBars.BarSubItem()
        Me.iAbout = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSendSMSTest = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonZeveraBringe = New DevExpress.XtraBars.BarButtonItem()
        Me.bar4 = New DevExpress.XtraBars.Bar()
        Me.iText = New DevExpress.XtraBars.BarStaticItem()
        Me.eProgress = New DevExpress.XtraBars.BarEditItem()
        Me.repositoryItemProgressBar1 = New DevExpress.XtraEditors.Repository.RepositoryItemProgressBar()
        Me.barDockControl1 = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControl2 = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControl3 = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControl4 = New DevExpress.XtraBars.BarDockControl()
        Me.iBack = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iForward = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iStop = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iRefresh = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iHome = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iSearch = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iFavorites = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iMedia = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iHistory = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iMail = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iEdit = New DevExpress.XtraBars.BarLargeButtonItem()
        Me.iGo = New DevExpress.XtraBars.BarButtonItem()
        Me.eAddress = New DevExpress.XtraBars.BarEditItem()
        Me.repositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.iSave = New DevExpress.XtraBars.BarButtonItem()
        Me.ipsWXP = New DevExpress.XtraBars.BarButtonItem()
        Me.ipsOXP = New DevExpress.XtraBars.BarButtonItem()
        Me.ipsO2K = New DevExpress.XtraBars.BarButtonItem()
        Me.iPaintStyle = New DevExpress.XtraBars.BarSubItem()
        Me.ipsDefault = New DevExpress.XtraBars.BarButtonItem()
        Me.ipsO3 = New DevExpress.XtraBars.BarButtonItem()
        Me.SitesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.tmrMainCounters = New System.Windows.Forms.Timer(Me.components)
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.xtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarMarketing, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ImageCollectionNaviSmall, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.barManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repositoryItemProgressBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.repositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SitesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DxErrorProvider1
        '
        Me.DxErrorProvider1.ContainerControl = Me
        '
        'xtraTabbedMdiManager1
        '
        Me.xtraTabbedMdiManager1.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageAndTabControlHeader
        Me.xtraTabbedMdiManager1.MdiParent = Me
        '
        'NavBarMarketing
        '
        Me.NavBarMarketing.ActiveGroup = Me.NavBarGroupProfiles
        Me.NavBarMarketing.AllowSelectedLink = True
        Me.NavBarMarketing.ContentButtonHint = Nothing
        Me.NavBarMarketing.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarMarketing.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroupProfiles, Me.NavBarGroupMarketingTools, Me.NavBarGroupFiles, Me.NavBarGroupLists, Me.NavBarGroupConfig2, Me.NavBarGroupTools})
        Me.NavBarMarketing.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.navSYS_CountriesGEO, Me.navEUS_LISTS_BodyType, Me.navEUS_LISTS_ChildrenNumber, Me.navEUS_LISTS_Drinking, Me.navEUS_LISTS_Education, Me.navEUS_LISTS_Ethnicity, Me.navEUS_LISTS_EyeColor, Me.navEUS_LISTS_HairColor, Me.navEUS_LISTS_Height, Me.navEUS_LISTS_Income, Me.navEUS_LISTS_NetWorth, Me.navEUS_LISTS_RejectingReasons, Me.navEUS_LISTS_RelationshipStatus, Me.navEUS_LISTS_Religion, Me.navEUS_LISTS_Smoking, Me.navApprovedProfiles, Me.navRejectedNewProfiles, Me.navEUS_LISTS_AccountType, Me.navEUS_LISTS_Gender, Me.navDeletedProfiles, Me.navEUS_LISTS_PhotosDisplayLevel, Me.navEUS_LISTS_TypeOfDating, Me.NavDeletedByUser, Me.NavBarItemTools_UpdateGEOGR, Me.NavBarItemTools_ReplaceDBString, Me.NavBarItemAutoApprove, Me.NavBarItemNewPhotos, Me.NavBarItemApprovedPhotos, Me.NavBarItemDeletedPhotos, Me.NavBarItemAutoApprovedProfiles, Me.NavBarItemAutoApprovedPhotos})
        Me.NavBarMarketing.Location = New System.Drawing.Point(0, 22)
        Me.NavBarMarketing.Name = "NavBarMarketing"
        Me.NavBarMarketing.OptionsNavPane.ExpandedWidth = 204
        Me.NavBarMarketing.OptionsNavPane.ShowOverflowPanel = False
        Me.NavBarMarketing.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane
        Me.NavBarMarketing.Size = New System.Drawing.Size(204, 643)
        Me.NavBarMarketing.SkinExplorerBarViewScrollStyle = DevExpress.XtraNavBar.SkinExplorerBarViewScrollStyle.Buttons
        Me.NavBarMarketing.SmallImages = Me.ImageCollectionNaviSmall
        Me.NavBarMarketing.TabIndex = 8
        Me.NavBarMarketing.Text = "Marketing Tools"
        Me.NavBarMarketing.View = New DevExpress.XtraNavBar.ViewInfo.SkinNavigationPaneViewInfoRegistrator()
        '
        'NavBarGroupProfiles
        '
        Me.NavBarGroupProfiles.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.NavBarGroupProfiles.Appearance.Options.UseFont = True
        Me.NavBarGroupProfiles.Caption = "Profiles..."
        Me.NavBarGroupProfiles.Expanded = True
        Me.NavBarGroupProfiles.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemAutoApprovedProfiles), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemAutoApprovedPhotos), New DevExpress.XtraNavBar.NavBarItemLink(Me.navProfiles), New DevExpress.XtraNavBar.NavBarItemLink(Me.navNewProfiles), New DevExpress.XtraNavBar.NavBarItemLink(Me.navUpdatingProfiles), New DevExpress.XtraNavBar.NavBarItemLink(Me.navApprovedProfiles), New DevExpress.XtraNavBar.NavBarItemLink(Me.navRejectedNewProfiles), New DevExpress.XtraNavBar.NavBarItemLink(Me.navDeletedProfiles), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavDeletedByUser), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemNewPhotos), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemApprovedPhotos), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemDeletedPhotos)})
        Me.NavBarGroupProfiles.Name = "NavBarGroupProfiles"
        Me.NavBarGroupProfiles.SelectedLinkIndex = 1
        Me.NavBarGroupProfiles.TopVisibleLinkIndex = 1
        '
        'NavBarItemAutoApprovedProfiles
        '
        Me.NavBarItemAutoApprovedProfiles.Caption = "Auto Approved Profiles"
        Me.NavBarItemAutoApprovedProfiles.Name = "NavBarItemAutoApprovedProfiles"
        '
        'NavBarItemAutoApprovedPhotos
        '
        Me.NavBarItemAutoApprovedPhotos.Caption = "Auto Approved Photos"
        Me.NavBarItemAutoApprovedPhotos.Name = "NavBarItemAutoApprovedPhotos"
        '
        'navProfiles
        '
        Me.navProfiles.Caption = "Profiles"
        Me.navProfiles.Name = "navProfiles"
        Me.navProfiles.Visible = False
        '
        'navNewProfiles
        '
        Me.navNewProfiles.Caption = "New Profiles"
        Me.navNewProfiles.Name = "navNewProfiles"
        '
        'navUpdatingProfiles
        '
        Me.navUpdatingProfiles.Caption = "Updating Profiles"
        Me.navUpdatingProfiles.Name = "navUpdatingProfiles"
        '
        'navApprovedProfiles
        '
        Me.navApprovedProfiles.Caption = "Approved Profiles"
        Me.navApprovedProfiles.Name = "navApprovedProfiles"
        '
        'navRejectedNewProfiles
        '
        Me.navRejectedNewProfiles.Caption = "Rejected New Profiles"
        Me.navRejectedNewProfiles.Name = "navRejectedNewProfiles"
        '
        'navDeletedProfiles
        '
        Me.navDeletedProfiles.Caption = "Deleted Profiles"
        Me.navDeletedProfiles.Name = "navDeletedProfiles"
        '
        'NavDeletedByUser
        '
        Me.NavDeletedByUser.Caption = "Deleted By User"
        Me.NavDeletedByUser.Name = "NavDeletedByUser"
        '
        'NavBarItemNewPhotos
        '
        Me.NavBarItemNewPhotos.Caption = "New Photos"
        Me.NavBarItemNewPhotos.Name = "NavBarItemNewPhotos"
        '
        'NavBarItemApprovedPhotos
        '
        Me.NavBarItemApprovedPhotos.Caption = "Approved Photos"
        Me.NavBarItemApprovedPhotos.Name = "NavBarItemApprovedPhotos"
        '
        'NavBarItemDeletedPhotos
        '
        Me.NavBarItemDeletedPhotos.Caption = "Deleted Photos"
        Me.NavBarItemDeletedPhotos.Name = "NavBarItemDeletedPhotos"
        '
        'NavBarGroupMarketingTools
        '
        Me.NavBarGroupMarketingTools.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.NavBarGroupMarketingTools.Appearance.Options.UseFont = True
        Me.NavBarGroupMarketingTools.Caption = "Marketing Tools..."
        Me.NavBarGroupMarketingTools.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.NavEmailOfferSettings), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarEmailMessages)})
        Me.NavBarGroupMarketingTools.Name = "NavBarGroupMarketingTools"
        '
        'NavEmailOfferSettings
        '
        Me.NavEmailOfferSettings.Caption = "Email Offers Settings"
        Me.NavEmailOfferSettings.Name = "NavEmailOfferSettings"
        '
        'NavBarEmailMessages
        '
        Me.NavBarEmailMessages.Caption = "Email Messages"
        Me.NavBarEmailMessages.Name = "NavBarEmailMessages"
        '
        'NavBarGroupFiles
        '
        Me.NavBarGroupFiles.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.NavBarGroupFiles.Appearance.Options.UseFont = True
        Me.NavBarGroupFiles.Caption = "Files..."
        Me.NavBarGroupFiles.Name = "NavBarGroupFiles"
        '
        'NavBarGroupLists
        '
        Me.NavBarGroupLists.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.NavBarGroupLists.Appearance.Options.UseFont = True
        Me.NavBarGroupLists.Caption = "Lists..."
        Me.NavBarGroupLists.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.navSYS_CountriesGEO), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_BodyType), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_ChildrenNumber), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_Drinking), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_Education), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_Ethnicity), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_EyeColor), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_HairColor), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_Height), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_Income), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_NetWorth), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_RejectingReasons), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_RelationshipStatus), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_Religion), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_Smoking), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_AccountType), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_Gender), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_PhotosDisplayLevel), New DevExpress.XtraNavBar.NavBarItemLink(Me.navEUS_LISTS_TypeOfDating)})
        Me.NavBarGroupLists.Name = "NavBarGroupLists"
        Me.NavBarGroupLists.TopVisibleLinkIndex = 8
        '
        'navSYS_CountriesGEO
        '
        Me.navSYS_CountriesGEO.Caption = "CountriesGEO List"
        Me.navSYS_CountriesGEO.Name = "navSYS_CountriesGEO"
        '
        'navEUS_LISTS_BodyType
        '
        Me.navEUS_LISTS_BodyType.Caption = "Body Type List"
        Me.navEUS_LISTS_BodyType.Name = "navEUS_LISTS_BodyType"
        '
        'navEUS_LISTS_ChildrenNumber
        '
        Me.navEUS_LISTS_ChildrenNumber.Caption = "Children Number List"
        Me.navEUS_LISTS_ChildrenNumber.Name = "navEUS_LISTS_ChildrenNumber"
        '
        'navEUS_LISTS_Drinking
        '
        Me.navEUS_LISTS_Drinking.Caption = "Drinking List"
        Me.navEUS_LISTS_Drinking.Name = "navEUS_LISTS_Drinking"
        '
        'navEUS_LISTS_Education
        '
        Me.navEUS_LISTS_Education.Caption = "Education List"
        Me.navEUS_LISTS_Education.Name = "navEUS_LISTS_Education"
        '
        'navEUS_LISTS_Ethnicity
        '
        Me.navEUS_LISTS_Ethnicity.Caption = "Ethnicity List"
        Me.navEUS_LISTS_Ethnicity.Name = "navEUS_LISTS_Ethnicity"
        '
        'navEUS_LISTS_EyeColor
        '
        Me.navEUS_LISTS_EyeColor.Caption = "Eye Color List"
        Me.navEUS_LISTS_EyeColor.Name = "navEUS_LISTS_EyeColor"
        '
        'navEUS_LISTS_HairColor
        '
        Me.navEUS_LISTS_HairColor.Caption = "Hair Color List"
        Me.navEUS_LISTS_HairColor.Name = "navEUS_LISTS_HairColor"
        '
        'navEUS_LISTS_Height
        '
        Me.navEUS_LISTS_Height.Caption = "Height List"
        Me.navEUS_LISTS_Height.Name = "navEUS_LISTS_Height"
        '
        'navEUS_LISTS_Income
        '
        Me.navEUS_LISTS_Income.Caption = "Annual Income List"
        Me.navEUS_LISTS_Income.Name = "navEUS_LISTS_Income"
        '
        'navEUS_LISTS_NetWorth
        '
        Me.navEUS_LISTS_NetWorth.Caption = "Net Worth List"
        Me.navEUS_LISTS_NetWorth.Name = "navEUS_LISTS_NetWorth"
        '
        'navEUS_LISTS_RejectingReasons
        '
        Me.navEUS_LISTS_RejectingReasons.Caption = "Rejecting Reasons List"
        Me.navEUS_LISTS_RejectingReasons.Name = "navEUS_LISTS_RejectingReasons"
        '
        'navEUS_LISTS_RelationshipStatus
        '
        Me.navEUS_LISTS_RelationshipStatus.Caption = "Relationship Status List"
        Me.navEUS_LISTS_RelationshipStatus.Name = "navEUS_LISTS_RelationshipStatus"
        '
        'navEUS_LISTS_Religion
        '
        Me.navEUS_LISTS_Religion.Caption = "Religion List"
        Me.navEUS_LISTS_Religion.Name = "navEUS_LISTS_Religion"
        '
        'navEUS_LISTS_Smoking
        '
        Me.navEUS_LISTS_Smoking.Caption = "Smoking List"
        Me.navEUS_LISTS_Smoking.Name = "navEUS_LISTS_Smoking"
        '
        'navEUS_LISTS_AccountType
        '
        Me.navEUS_LISTS_AccountType.Caption = "Account Type List"
        Me.navEUS_LISTS_AccountType.Name = "navEUS_LISTS_AccountType"
        '
        'navEUS_LISTS_Gender
        '
        Me.navEUS_LISTS_Gender.Caption = "Gender List"
        Me.navEUS_LISTS_Gender.Name = "navEUS_LISTS_Gender"
        '
        'navEUS_LISTS_PhotosDisplayLevel
        '
        Me.navEUS_LISTS_PhotosDisplayLevel.Caption = "Photos Display Level List"
        Me.navEUS_LISTS_PhotosDisplayLevel.Name = "navEUS_LISTS_PhotosDisplayLevel"
        '
        'navEUS_LISTS_TypeOfDating
        '
        Me.navEUS_LISTS_TypeOfDating.Caption = "Type Of Dating List"
        Me.navEUS_LISTS_TypeOfDating.Name = "navEUS_LISTS_TypeOfDating"
        '
        'NavBarGroupConfig2
        '
        Me.NavBarGroupConfig2.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.NavBarGroupConfig2.Appearance.Options.UseFont = True
        Me.NavBarGroupConfig2.Caption = "Config..."
        Me.NavBarGroupConfig2.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemAutoApprove)})
        Me.NavBarGroupConfig2.Name = "NavBarGroupConfig2"
        '
        'NavBarItemAutoApprove
        '
        Me.NavBarItemAutoApprove.Caption = "Auto approve"
        Me.NavBarItemAutoApprove.Name = "NavBarItemAutoApprove"
        '
        'NavBarGroupTools
        '
        Me.NavBarGroupTools.Caption = "Tools"
        Me.NavBarGroupTools.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemTools_UpdateGEOGR), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItemTools_ReplaceDBString)})
        Me.NavBarGroupTools.Name = "NavBarGroupTools"
        '
        'NavBarItemTools_UpdateGEOGR
        '
        Me.NavBarItemTools_UpdateGEOGR.Caption = "Update GEO GR"
        Me.NavBarItemTools_UpdateGEOGR.Name = "NavBarItemTools_UpdateGEOGR"
        '
        'NavBarItemTools_ReplaceDBString
        '
        Me.NavBarItemTools_ReplaceDBString.Caption = "Replace DBString"
        Me.NavBarItemTools_ReplaceDBString.Name = "NavBarItemTools_ReplaceDBString"
        '
        'ImageCollectionNaviSmall
        '
        Me.ImageCollectionNaviSmall.ImageSize = New System.Drawing.Size(28, 28)
        Me.ImageCollectionNaviSmall.ImageStream = CType(resources.GetObject("ImageCollectionNaviSmall.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.ImageCollectionNaviSmall.Images.SetKeyName(0, "About.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(1, "Accounts.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(2, "Address Book.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(3, "Air Brush.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(4, "Align-Centre.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(5, "Align-Centre-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(6, "Align-Full.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(7, "Align-Full-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(8, "Align-Left.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(9, "Align-Left-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(10, "Align-Right.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(11, "Align-Right-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(12, "Arrow-Left Right.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(13, "Arrow-Up Down.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(14, "Attachment.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(15, "Auto Correct-Options.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(16, "Auto-Type.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(17, "Back.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(18, "Bar Code.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(19, "Bold.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(20, "Book-Open.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(21, "Books.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(22, "Book-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(23, "Borders and Shading.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(24, "Box-Closed.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(25, "Box-Closed-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(26, "Box-Open.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(27, "Box-Open-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(28, "Bullets.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(29, "Bullets-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(30, "Calculator.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(31, "Calendar.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(32, "Calendar-Go To Date.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(33, "Calendar-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(34, "Calendar-Select Day.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(35, "Calendar-Select Month.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(36, "Calendar-Select Week.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(37, "Calendar-Select Work Week.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(38, "Camera.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(39, "Camera-Photo.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(40, "Card File.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(41, "Card.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(42, "Card-Add.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(43, "Card-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(44, "Card-Edit.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(45, "Card-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(46, "Card-Next.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(47, "Card-Previous.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(48, "Card-Remove.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(49, "Card-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(50, "CD-Burn.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(51, "CD-In Case.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(52, "CD-ROM.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(53, "Center Across Cells.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(54, "Certificate.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(55, "Chart.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(56, "Chart-Bar.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(57, "Chart-Line.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(58, "Chart-Pie.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(59, "Chart-Point.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(60, "Clean.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(61, "Clear.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(62, "Clipboard.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(63, "Clock.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(64, "Close.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(65, "Colour Picker.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(66, "Columns.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(67, "Columns-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(68, "Command Prompt.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(69, "Compress.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(70, "Computer.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(71, "Connect.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(72, "Contact.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(73, "Contact-Add.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(74, "Contact-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(75, "Contact-Edit.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(76, "Contact-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(77, "Contact-Remove.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(78, "Contact-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(79, "Copy.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(80, "Cross.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(81, "Currency.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(82, "Currency-Notes.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(83, "Cut.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(84, "Database.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(85, "Database-Add.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(86, "Database-Close.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(87, "Database-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(88, "Database-Edit.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(89, "Database-Filter.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(90, "Database-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(91, "Database-Open.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(92, "Database-Properties.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(93, "Database-Remove.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(94, "Database-Save.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(95, "Database-Schema.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(96, "Database-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(97, "Database-Security.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(98, "Database-Wizard.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(99, "Date-Time.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(100, "db-Add.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(101, "db-Cancel.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(102, "db-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(103, "db-Edit.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(104, "db-First.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(105, "db-Last.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(106, "db-Next.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(107, "db-Post.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(108, "db-Previous.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(109, "db-Refresh.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(110, "Decimals-Decrease.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(111, "Decimals-Increase.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(112, "Decompress.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(113, "Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(114, "Delete-Blue.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(115, "Design.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(116, "Desktop.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(117, "Dictionary.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(118, "Disconnect.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(119, "Discuss.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(120, "Doc-Access.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(121, "Doc-Acrobat.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(122, "Doc-Excel.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(123, "Doc-HTML.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(124, "Doc-Outlook.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(125, "Doc-RTF.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(126, "Document-Auto Format.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(127, "Document-Protect.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(128, "Doc-Word.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(129, "Doc-XML.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(130, "Drawing.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(131, "Edit.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(132, "Equaliser.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(133, "Execute.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(134, "Exit.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(135, "Export.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(136, "Export-Access.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(137, "Export-Acrobat.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(138, "Export-CSV.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(139, "Export-Excel.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(140, "Export-Fixed Length.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(141, "Export-HTML.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(142, "Export-Outlook.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(143, "Export-RTF.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(144, "Export-Text.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(145, "Export-Word.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(146, "Export-XML.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(147, "Favorites.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(148, "Fill.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(149, "Fill-Down.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(150, "Fill-Left.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(151, "Fill-Right.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(152, "Fill-Up.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(153, "Filter.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(154, "Flag.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(155, "Flag-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(156, "Flag-Next.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(157, "Flag-Previous.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(158, "Flow Chart.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(159, "Folder List.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(160, "Folder.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(161, "Folder-Closed.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(162, "Folder-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(163, "Folder-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(164, "Folder-Options.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(165, "Folder-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(166, "Footer.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(167, "Format Painter.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(168, "Format-Font Size.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(169, "Format-Font.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(170, "Format-Paragraph.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(171, "Format-Percentage.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(172, "Formatting-Remove.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(173, "Forward.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(174, "Full Screen.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(175, "Full Screen-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(176, "Function.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(177, "Go To Line.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(178, "Grid.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(179, "Grid-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(180, "Grid-Auto Format.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(181, "Grid-Auto Format-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(182, "Grid-Column Width.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(183, "Grid-Delete Cell.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(184, "Grid-Delete Column.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(185, "Grid-Delete Row.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(186, "Grid-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(187, "Grid-Delete-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(188, "Grid-Insert Cell.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(189, "Grid-Insert Column Left.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(190, "Grid-Insert Column Right.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(191, "Grid-Insert Column.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(192, "Grid-Insert Row Above.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(193, "Grid-Insert Row Below.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(194, "Grid-Insert Row.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(195, "Grid-Merge Cells.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(196, "Grid-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(197, "Grid-New-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(198, "Grid-Options.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(199, "Grid-Options-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(200, "Grid-Row Height.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(201, "Grid-Select All.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(202, "Grid-Select All-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(203, "Grid-Select Cell.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(204, "Grid-Select Cell-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(205, "Grid-Select Column.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(206, "Grid-Select Column-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(207, "Grid-Select Row.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(208, "Grid-Select Row-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(209, "Grid-Split Cells.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(210, "Grid-Wizard.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(211, "Grid-Wizard-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(212, "Hand Held.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(213, "Hand Shake.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(214, "Header and Footer.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(215, "Header and Footer-Toggle.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(216, "Header.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(217, "Header-Next.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(218, "Header-Previous.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(219, "Help.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(220, "Highlighter.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(221, "History.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(222, "Home.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(223, "Image.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(224, "Image-Brightness.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(225, "Image-Colour.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(226, "Image-Contrast.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(227, "Image-Crop.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(228, "Image-Flip.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(229, "Image-Mirror.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(230, "Image-Paint.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(231, "Image-Resize.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(232, "Image-Rotate.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(233, "Image-Select.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(234, "Import.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(235, "Import-Access.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(236, "Import-Acrobat.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(237, "Import-CSV.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(238, "Import-Excel.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(239, "Import-Fixed Length.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(240, "Import-HTML.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(241, "Import-Outlook.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(242, "Import-RTF.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(243, "Import-Text.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(244, "Import-Word.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(245, "Import-XML.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(246, "Indent-Decrease.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(247, "Indent-Decrease-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(248, "Indent-Increase.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(249, "Indent-Increase-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(250, "Insert.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(251, "Insert-File.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(252, "Insert-Image.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(253, "Insert-Object.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(254, "Invoice.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(255, "Italic.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(256, "Key.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(257, "Landscape.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(258, "Launch.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(259, "Letter.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(260, "Line Spacing.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(261, "Line Spacing-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(262, "Link.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(263, "Lock.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(264, "Mail.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(265, "Mail-Forward.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(266, "Mail-In Box.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(267, "Mail-In Box-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(268, "Mail-In-Out Box.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(269, "Mail-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(270, "Mail-Out Box.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(271, "Mail-Out Box-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(272, "Mail-Reply To All.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(273, "Mail-Reply.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(274, "Mail-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(275, "Mail-Send And Receive.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(276, "Media.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(277, "Microphone.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(278, "Midi Keyboard.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(279, "Minus.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(280, "mm-Eject.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(281, "mm-Fast Forward.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(282, "mm-First.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(283, "mm-Last.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(284, "mm-Pause.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(285, "mm-Play.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(286, "mm-Rewind.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(287, "mm-Stop.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(288, "Monitor.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(289, "Movie.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(290, "Musical Note.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(291, "Musical Note-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(292, "New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(293, "Note.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(294, "Note-Add.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(295, "Note-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(296, "Note-Edit.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(297, "Note-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(298, "Note-Next.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(299, "Notepad.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(300, "Note-Previous.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(301, "Note-Remove.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(302, "Notes.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(303, "Note-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(304, "Number Of Pages.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(305, "Numbering.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(306, "Numbering-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(307, "Object-Bring To Front.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(308, "Object-Select.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(309, "Object-Send To Back.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(310, "Open.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(311, "Outline.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(312, "Outline-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(313, "Page Number.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(314, "Page-Break.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(315, "Page-Invert.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(316, "Page-Next.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(317, "Page-Previous.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(318, "Page-Setup.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(319, "Paintbrush.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(320, "Parcel.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(321, "Paste.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(322, "Permission.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(323, "Phone.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(324, "Planner.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(325, "Plus.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(326, "Portrait.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(327, "Preview.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(328, "Print.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(329, "Print-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(330, "Print-Options.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(331, "Print-Options-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(332, "Procedure.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(333, "Process.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(334, "Project.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(335, "Project-Gant Chart.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(336, "Properties.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(337, "Push Pin.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(338, "Recycle Bin.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(339, "Redo.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(340, "Refresh.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(341, "Remove.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(342, "Rename.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(343, "Report.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(344, "Report-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(345, "Report-Bound.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(346, "Research.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(347, "Ruler.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(348, "Ruler-Formatting.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(349, "Save All.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(350, "Save As HTML.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(351, "Save As.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(352, "Save Draft.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(353, "Save.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(354, "Script.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(355, "Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(356, "Search-Next.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(357, "Search-Previous.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(358, "Search-Replace.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(359, "Select All.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(360, "Slide Show.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(361, "Slide.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(362, "Software-Analyse.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(363, "Sort-Ascending.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(364, "Sort-Descending.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(365, "Sound-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(366, "Sound-Fade In.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(367, "Sound-Fade Out.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(368, "Sound-Join.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(369, "Sound-Mark End.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(370, "Sound-Mark Start.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(371, "Sound-Split.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(372, "Spell Check.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(373, "Stop.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(374, "Strikeout.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(375, "Subscript.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(376, "Sum.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(377, "Summary.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(378, "Superscript.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(379, "Support.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(380, "Tables and Borders.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(381, "Task.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(382, "Task-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(383, "Thesaurus.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(384, "Tick.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(385, "Time Line.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(386, "Tip.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(387, "To Do List.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(388, "To Lowercase.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(389, "To Uppercase.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(390, "Tools.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(391, "Tree View.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(392, "Tree View-Add.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(393, "Tree View-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(394, "Tree View-Edit.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(395, "Tree View-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(396, "Tree View-Remove.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(397, "Tree View-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(398, "Underline.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(399, "Undo.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(400, "Unlock.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(401, "User.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(402, "User-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(403, "User-Add.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(404, "User-Add-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(405, "User-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(406, "User-Delete-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(407, "User-Edit.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(408, "User-Edit-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(409, "User-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(410, "User-New-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(411, "User-Options.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(412, "User-Options-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(413, "User-Password.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(414, "User-Password-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(415, "User-Remove.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(416, "User-Remove-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(417, "Users.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(418, "Users-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(419, "User-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(420, "User-Search-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(421, "User-Security.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(422, "User-Security-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(423, "View-Details.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(424, "View-Large Icons.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(425, "View-List.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(426, "View-One Page.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(427, "View-Page Width.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(428, "Views.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(429, "View-Small Icons.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(430, "Volume.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(431, "Volume-Mute.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(432, "Warning.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(433, "Waste Bin.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(434, "Window.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(435, "Window-Add.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(436, "Window-Bring To Front.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(437, "Window-Cascade.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(438, "Window-Close.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(439, "Window-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(440, "Window-Edit.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(441, "Window-Maximise.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(442, "Window-Minimise.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(443, "Window-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(444, "Window-Next.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(445, "Window-Options.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(446, "Window-Preview.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(447, "Window-Previous.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(448, "Window-Properties.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(449, "Window-Remove.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(450, "Window-Search.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(451, "Window-Send To Back.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(452, "Window-Split.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(453, "Window-Tile Horizontal.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(454, "Window-Tile Vertical.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(455, "Wizard.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(456, "Word Count.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(457, "Word Wrap.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(458, "Word Wrap-2.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(459, "Work Sheet.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(460, "Work Sheet-Auto Format.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(461, "Work Sheet-Delete.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(462, "Work Sheet-New.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(463, "Work Sheet-Options.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(464, "Work Sheet-Protect.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(465, "Work Sheet-Rename.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(466, "Zoom-In.ico")
        Me.ImageCollectionNaviSmall.Images.SetKeyName(467, "Zoom-Out.ico")
        '
        'NavAgencysService
        '
        Me.NavAgencysService.Caption = "Agencys Service"
        Me.NavAgencysService.Name = "NavAgencysService"
        Me.NavAgencysService.SmallImage = CType(resources.GetObject("NavAgencysService.SmallImage"), System.Drawing.Image)
        Me.NavAgencysService.Tag = "StartUP"
        '
        'NavMenus
        '
        Me.NavMenus.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavMenus.Appearance.Options.UseFont = True
        Me.NavMenus.Caption = "Menus"
        Me.NavMenus.Name = "NavMenus"
        Me.NavMenus.SmallImage = CType(resources.GetObject("NavMenus.SmallImage"), System.Drawing.Image)
        Me.NavMenus.SmallImageIndex = 1
        '
        'NavPrograms
        '
        Me.NavPrograms.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavPrograms.Appearance.Options.UseFont = True
        Me.NavPrograms.Caption = "Programs"
        Me.NavPrograms.Name = "NavPrograms"
        Me.NavPrograms.SmallImageIndex = 386
        '
        'NavUsers
        '
        Me.NavUsers.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavUsers.Appearance.Options.UseFont = True
        Me.NavUsers.Caption = "Users"
        Me.NavUsers.Name = "NavUsers"
        Me.NavUsers.SmallImageIndex = 422
        '
        'NavLoginAccessHistory
        '
        Me.NavLoginAccessHistory.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavLoginAccessHistory.Appearance.Options.UseFont = True
        Me.NavLoginAccessHistory.Caption = "Login Access History"
        Me.NavLoginAccessHistory.Name = "NavLoginAccessHistory"
        Me.NavLoginAccessHistory.SmallImageIndex = 412
        '
        'NavWorksFromUsers
        '
        Me.NavWorksFromUsers.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavWorksFromUsers.Appearance.Options.UseFont = True
        Me.NavWorksFromUsers.Caption = "Works From Users"
        Me.NavWorksFromUsers.Name = "NavWorksFromUsers"
        Me.NavWorksFromUsers.SmallImageIndex = 417
        '
        'NavRoles
        '
        Me.NavRoles.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavRoles.Appearance.Options.UseFont = True
        Me.NavRoles.Caption = "Roles"
        Me.NavRoles.Name = "NavRoles"
        Me.NavRoles.SmallImageIndex = 418
        '
        'NavInstalledPrograms
        '
        Me.NavInstalledPrograms.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavInstalledPrograms.Appearance.Options.UseFont = True
        Me.NavInstalledPrograms.Caption = "Installed Programs"
        Me.NavInstalledPrograms.Name = "NavInstalledPrograms"
        Me.NavInstalledPrograms.SmallImageIndex = 27
        '
        'NavLicenses
        '
        Me.NavLicenses.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavLicenses.Appearance.Options.UseFont = True
        Me.NavLicenses.Caption = "Licenses"
        Me.NavLicenses.Name = "NavLicenses"
        Me.NavLicenses.SmallImageIndex = 116
        '
        'NavFeedbackActivity
        '
        Me.NavFeedbackActivity.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavFeedbackActivity.Appearance.Options.UseFont = True
        Me.NavFeedbackActivity.Caption = "Feedback Activity"
        Me.NavFeedbackActivity.Name = "NavFeedbackActivity"
        Me.NavFeedbackActivity.SmallImageIndex = 156
        '
        'NavCustomers
        '
        Me.NavCustomers.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavCustomers.Appearance.Options.UseFont = True
        Me.NavCustomers.Caption = "Customers"
        Me.NavCustomers.Name = "NavCustomers"
        Me.NavCustomers.SmallImageIndex = 417
        '
        'NavSMSAccounts
        '
        Me.NavSMSAccounts.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavSMSAccounts.Appearance.Options.UseFont = True
        Me.NavSMSAccounts.Caption = "SMS Accounts"
        Me.NavSMSAccounts.Name = "NavSMSAccounts"
        Me.NavSMSAccounts.SmallImageIndex = 267
        '
        'NavSMSTranstactions
        '
        Me.NavSMSTranstactions.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavSMSTranstactions.Appearance.Options.UseFont = True
        Me.NavSMSTranstactions.Caption = "SMS Transtactions"
        Me.NavSMSTranstactions.Name = "NavSMSTranstactions"
        Me.NavSMSTranstactions.SmallImageIndex = 22
        '
        'NavProviders
        '
        Me.NavProviders.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavProviders.Appearance.Options.UseFont = True
        Me.NavProviders.Caption = "Providers"
        Me.NavProviders.Name = "NavProviders"
        Me.NavProviders.SmallImageIndex = 82
        '
        'NavSalesSites
        '
        Me.NavSalesSites.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavSalesSites.Appearance.Options.UseFont = True
        Me.NavSalesSites.Caption = "my Sites"
        Me.NavSalesSites.Name = "NavSalesSites"
        Me.NavSalesSites.SmallImageIndex = 46
        '
        'NavProducts
        '
        Me.NavProducts.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavProducts.Appearance.Options.UseFont = True
        Me.NavProducts.Caption = "Products"
        Me.NavProducts.Name = "NavProducts"
        Me.NavProducts.SmallImageIndex = 18
        '
        'NavTransactions
        '
        Me.NavTransactions.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.NavTransactions.Appearance.Options.UseFont = True
        Me.NavTransactions.Caption = "Transactions"
        Me.NavTransactions.Name = "NavTransactions"
        Me.NavTransactions.SmallImageIndex = 199
        '
        'NavPages
        '
        Me.NavPages.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.NavPages.Appearance.Options.UseFont = True
        Me.NavPages.Caption = "Pages"
        Me.NavPages.Name = "NavPages"
        Me.NavPages.SmallImage = CType(resources.GetObject("NavPages.SmallImage"), System.Drawing.Image)
        '
        'NavBasic
        '
        Me.NavBasic.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.NavBasic.Appearance.Options.UseFont = True
        Me.NavBasic.Caption = "Basic"
        Me.NavBasic.Name = "NavBasic"
        Me.NavBasic.SmallImage = CType(resources.GetObject("NavBasic.SmallImage"), System.Drawing.Image)
        '
        'SplitterControl1
        '
        Me.SplitterControl1.Location = New System.Drawing.Point(204, 22)
        Me.SplitterControl1.Name = "SplitterControl1"
        Me.SplitterControl1.Size = New System.Drawing.Size(5, 643)
        Me.SplitterControl1.TabIndex = 11
        Me.SplitterControl1.TabStop = False
        '
        'barManager1
        '
        Me.barManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.bar2, Me.bar4})
        Me.barManager1.Categories.AddRange(New DevExpress.XtraBars.BarManagerCategory() {New DevExpress.XtraBars.BarManagerCategory("Built-in Menus", New System.Guid("4712321c-b9cd-461f-b453-4a7791063abb")), New DevExpress.XtraBars.BarManagerCategory("Standard", New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")), New DevExpress.XtraBars.BarManagerCategory("Address", New System.Guid("fb82a187-cdf0-4f39-a566-c00dbaba593d")), New DevExpress.XtraBars.BarManagerCategory("StatusBar", New System.Guid("2ca54f89-3af6-4cbb-93d8-4a4a9387f283")), New DevExpress.XtraBars.BarManagerCategory("Items", New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")), New DevExpress.XtraBars.BarManagerCategory("Favorites", New System.Guid("e1ba440c-33dc-4df6-b712-79cdc4dcd983"))})
        Me.barManager1.DockControls.Add(Me.barDockControl1)
        Me.barManager1.DockControls.Add(Me.barDockControl2)
        Me.barManager1.DockControls.Add(Me.barDockControl3)
        Me.barManager1.DockControls.Add(Me.barDockControl4)
        Me.barManager1.Form = Me
        Me.barManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.siFile, Me.siView, Me.siTools, Me.siHelp, Me.iBack, Me.iForward, Me.iStop, Me.iRefresh, Me.iHome, Me.iSearch, Me.iFavorites, Me.iMedia, Me.iHistory, Me.iMail, Me.iPrint, Me.iEdit, Me.iGo, Me.eAddress, Me.iText, Me.eProgress, Me.iToolBars, Me.iAbout, Me.iExit, Me.iOptions, Me.iSwitchUser, Me.iSave, Me.ipsWXP, Me.ipsOXP, Me.ipsO2K, Me.iPaintStyle, Me.ipsO3, Me.ipsDefault, Me.BarSendSMSTest, Me.BarButtonZeveraBringe})
        Me.barManager1.MainMenu = Me.bar2
        Me.barManager1.MaxItemId = 42
        Me.barManager1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.repositoryItemComboBox1, Me.repositoryItemProgressBar1})
        Me.barManager1.StatusBar = Me.bar4
        '
        'bar2
        '
        Me.bar2.BarItemHorzIndent = 6
        Me.bar2.BarName = "MainMenu"
        Me.bar2.DockCol = 0
        Me.bar2.DockRow = 0
        Me.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.bar2.FloatLocation = New System.Drawing.Point(39, 106)
        Me.bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.siFile), New DevExpress.XtraBars.LinkPersistInfo(Me.siView), New DevExpress.XtraBars.LinkPersistInfo(Me.siTools), New DevExpress.XtraBars.LinkPersistInfo(Me.siHelp)})
        Me.bar2.OptionsBar.AllowQuickCustomization = False
        Me.bar2.OptionsBar.DrawDragBorder = False
        Me.bar2.OptionsBar.MultiLine = True
        Me.bar2.OptionsBar.RotateWhenVertical = False
        Me.bar2.OptionsBar.UseWholeRow = True
        Me.bar2.Text = "MainMenu"
        '
        'siFile
        '
        Me.siFile.Caption = "&File"
        Me.siFile.CategoryGuid = New System.Guid("4712321c-b9cd-461f-b453-4a7791063abb")
        Me.siFile.Id = 0
        Me.siFile.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.iSwitchUser), New DevExpress.XtraBars.LinkPersistInfo(Me.iPrint, True), New DevExpress.XtraBars.LinkPersistInfo(Me.iExit, True)})
        Me.siFile.Name = "siFile"
        '
        'iSwitchUser
        '
        Me.iSwitchUser.Caption = "Switch &User..."
        Me.iSwitchUser.CategoryGuid = New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")
        Me.iSwitchUser.Id = 29
        Me.iSwitchUser.ImageIndex = 4
        Me.iSwitchUser.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O))
        Me.iSwitchUser.Name = "iSwitchUser"
        '
        'iPrint
        '
        Me.iPrint.Caption = "&Print..."
        Me.iPrint.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iPrint.Hint = "Print"
        Me.iPrint.Id = 18
        Me.iPrint.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P))
        Me.iPrint.LargeImageIndex = 8
        Me.iPrint.Name = "iPrint"
        Me.iPrint.ShowCaptionOnBar = False
        '
        'iExit
        '
        Me.iExit.Caption = "E&xit"
        Me.iExit.CategoryGuid = New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")
        Me.iExit.Id = 27
        Me.iExit.Name = "iExit"
        '
        'siView
        '
        Me.siView.Caption = "&View"
        Me.siView.CategoryGuid = New System.Guid("4712321c-b9cd-461f-b453-4a7791063abb")
        Me.siView.Id = 2
        Me.siView.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.iToolBars)})
        Me.siView.Name = "siView"
        '
        'iToolBars
        '
        Me.iToolBars.Caption = "ToolBarsList"
        Me.iToolBars.CategoryGuid = New System.Guid("4712321c-b9cd-461f-b453-4a7791063abb")
        Me.iToolBars.Id = 25
        Me.iToolBars.Name = "iToolBars"
        '
        'siTools
        '
        Me.siTools.Caption = "O&ptions"
        Me.siTools.CategoryGuid = New System.Guid("4712321c-b9cd-461f-b453-4a7791063abb")
        Me.siTools.Id = 3
        Me.siTools.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.iOptions)})
        Me.siTools.Name = "siTools"
        '
        'iOptions
        '
        Me.iOptions.Caption = "Options..."
        Me.iOptions.CategoryGuid = New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")
        Me.iOptions.Id = 28
        Me.iOptions.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.iOptions.ItemAppearance.Normal.Options.UseFont = True
        Me.iOptions.Name = "iOptions"
        '
        'siHelp
        '
        Me.siHelp.Caption = "&Help"
        Me.siHelp.CategoryGuid = New System.Guid("4712321c-b9cd-461f-b453-4a7791063abb")
        Me.siHelp.Id = 4
        Me.siHelp.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.iAbout), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSendSMSTest), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonZeveraBringe)})
        Me.siHelp.Name = "siHelp"
        '
        'iAbout
        '
        Me.iAbout.Caption = "&About"
        Me.iAbout.CategoryGuid = New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")
        Me.iAbout.Id = 26
        Me.iAbout.Name = "iAbout"
        '
        'BarSendSMSTest
        '
        Me.BarSendSMSTest.Caption = "Send SMS Test"
        Me.BarSendSMSTest.Id = 38
        Me.BarSendSMSTest.Name = "BarSendSMSTest"
        '
        'BarButtonZeveraBringe
        '
        Me.BarButtonZeveraBringe.Caption = "Tool Zevera Bringe SQL Database"
        Me.BarButtonZeveraBringe.Id = 39
        Me.BarButtonZeveraBringe.Name = "BarButtonZeveraBringe"
        '
        'bar4
        '
        Me.bar4.BarName = "Status Bar"
        Me.bar4.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.bar4.DockCol = 0
        Me.bar4.DockRow = 0
        Me.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.bar4.FloatLocation = New System.Drawing.Point(30, 434)
        Me.bar4.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.iText), New DevExpress.XtraBars.LinkPersistInfo(Me.eProgress)})
        Me.bar4.OptionsBar.AllowQuickCustomization = False
        Me.bar4.OptionsBar.DrawDragBorder = False
        Me.bar4.OptionsBar.DrawSizeGrip = True
        Me.bar4.OptionsBar.RotateWhenVertical = False
        Me.bar4.OptionsBar.UseWholeRow = True
        Me.bar4.Text = "Status Bar"
        '
        'iText
        '
        Me.iText.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring
        Me.iText.CategoryGuid = New System.Guid("2ca54f89-3af6-4cbb-93d8-4a4a9387f283")
        Me.iText.Id = 22
        Me.iText.Name = "iText"
        Me.iText.RightIndent = 3
        Me.iText.TextAlignment = System.Drawing.StringAlignment.Near
        Me.iText.Width = 32
        '
        'eProgress
        '
        Me.eProgress.CanOpenEdit = False
        Me.eProgress.CategoryGuid = New System.Guid("2ca54f89-3af6-4cbb-93d8-4a4a9387f283")
        Me.eProgress.Edit = Me.repositoryItemProgressBar1
        Me.eProgress.EditHeight = 10
        Me.eProgress.Id = 24
        Me.eProgress.Name = "eProgress"
        Me.eProgress.Width = 70
        '
        'repositoryItemProgressBar1
        '
        Me.repositoryItemProgressBar1.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.repositoryItemProgressBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1"
        '
        'barDockControl1
        '
        Me.barDockControl1.CausesValidation = False
        Me.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControl1.Location = New System.Drawing.Point(0, 0)
        Me.barDockControl1.Size = New System.Drawing.Size(987, 22)
        '
        'barDockControl2
        '
        Me.barDockControl2.CausesValidation = False
        Me.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControl2.Location = New System.Drawing.Point(0, 665)
        Me.barDockControl2.Size = New System.Drawing.Size(987, 23)
        '
        'barDockControl3
        '
        Me.barDockControl3.CausesValidation = False
        Me.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControl3.Location = New System.Drawing.Point(0, 22)
        Me.barDockControl3.Size = New System.Drawing.Size(0, 643)
        '
        'barDockControl4
        '
        Me.barDockControl4.CausesValidation = False
        Me.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControl4.Location = New System.Drawing.Point(987, 22)
        Me.barDockControl4.Size = New System.Drawing.Size(0, 643)
        '
        'iBack
        '
        Me.iBack.Caption = "Back"
        Me.iBack.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right
        Me.iBack.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iBack.Enabled = False
        Me.iBack.Hint = "Back"
        Me.iBack.Id = 6
        Me.iBack.LargeImageIndex = 0
        Me.iBack.LargeImageIndexDisabled = 12
        Me.iBack.Name = "iBack"
        '
        'iForward
        '
        Me.iForward.Caption = "Forward"
        Me.iForward.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iForward.Enabled = False
        Me.iForward.Hint = "Forward"
        Me.iForward.Id = 8
        Me.iForward.LargeImageIndex = 3
        Me.iForward.LargeImageIndexDisabled = 13
        Me.iForward.Name = "iForward"
        Me.iForward.ShowCaptionOnBar = False
        '
        'iStop
        '
        Me.iStop.Caption = "Stop"
        Me.iStop.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iStop.Hint = "Stop"
        Me.iStop.Id = 9
        Me.iStop.LargeImageIndex = 11
        Me.iStop.Name = "iStop"
        Me.iStop.ShowCaptionOnBar = False
        '
        'iRefresh
        '
        Me.iRefresh.Caption = "Refresh"
        Me.iRefresh.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iRefresh.Hint = "Refresh"
        Me.iRefresh.Id = 10
        Me.iRefresh.LargeImageIndex = 9
        Me.iRefresh.Name = "iRefresh"
        Me.iRefresh.ShowCaptionOnBar = False
        '
        'iHome
        '
        Me.iHome.Caption = "Home"
        Me.iHome.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iHome.Hint = "Home"
        Me.iHome.Id = 11
        Me.iHome.LargeImageIndex = 5
        Me.iHome.Name = "iHome"
        Me.iHome.ShowCaptionOnBar = False
        '
        'iSearch
        '
        Me.iSearch.Caption = "Search"
        Me.iSearch.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right
        Me.iSearch.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iSearch.Hint = "Search"
        Me.iSearch.Id = 12
        Me.iSearch.LargeImageIndex = 10
        Me.iSearch.Name = "iSearch"
        '
        'iFavorites
        '
        Me.iFavorites.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check
        Me.iFavorites.Caption = "Favorites"
        Me.iFavorites.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right
        Me.iFavorites.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iFavorites.Down = True
        Me.iFavorites.Hint = "Favorites"
        Me.iFavorites.Id = 13
        Me.iFavorites.LargeImageIndex = 2
        Me.iFavorites.Name = "iFavorites"
        '
        'iMedia
        '
        Me.iMedia.Caption = "Media"
        Me.iMedia.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right
        Me.iMedia.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iMedia.Hint = "Media"
        Me.iMedia.Id = 15
        Me.iMedia.LargeImageIndex = 7
        Me.iMedia.Name = "iMedia"
        '
        'iHistory
        '
        Me.iHistory.Caption = "History"
        Me.iHistory.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iHistory.Enabled = False
        Me.iHistory.Hint = "History"
        Me.iHistory.Id = 16
        Me.iHistory.LargeImageIndex = 4
        Me.iHistory.LargeImageIndexDisabled = 14
        Me.iHistory.Name = "iHistory"
        Me.iHistory.ShowCaptionOnBar = False
        '
        'iMail
        '
        Me.iMail.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown
        Me.iMail.Caption = "Mail"
        Me.iMail.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iMail.Enabled = False
        Me.iMail.Hint = "Mail"
        Me.iMail.Id = 17
        Me.iMail.LargeImageIndex = 6
        Me.iMail.LargeImageIndexDisabled = 15
        Me.iMail.Name = "iMail"
        Me.iMail.ShowCaptionOnBar = False
        '
        'iEdit
        '
        Me.iEdit.Caption = "Edit"
        Me.iEdit.CategoryGuid = New System.Guid("8e707040-b093-4d7e-8f27-277ae2456d3b")
        Me.iEdit.Hint = "Open Notepad"
        Me.iEdit.Id = 19
        Me.iEdit.LargeImageIndex = 1
        Me.iEdit.Name = "iEdit"
        Me.iEdit.ShowCaptionOnBar = False
        '
        'iGo
        '
        Me.iGo.Caption = "Go"
        Me.iGo.CategoryGuid = New System.Guid("fb82a187-cdf0-4f39-a566-c00dbaba593d")
        Me.iGo.Glyph = CType(resources.GetObject("iGo.Glyph"), System.Drawing.Image)
        Me.iGo.Hint = "Go to ..."
        Me.iGo.Id = 20
        Me.iGo.Name = "iGo"
        Me.iGo.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'eAddress
        '
        Me.eAddress.AutoFillWidth = True
        Me.eAddress.Caption = "Address"
        Me.eAddress.CategoryGuid = New System.Guid("fb82a187-cdf0-4f39-a566-c00dbaba593d")
        Me.eAddress.Edit = Me.repositoryItemComboBox1
        Me.eAddress.Id = 21
        Me.eAddress.IEBehavior = True
        Me.eAddress.Name = "eAddress"
        Me.eAddress.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        Me.eAddress.Width = 400
        '
        'repositoryItemComboBox1
        '
        Me.repositoryItemComboBox1.AllowFocused = False
        Me.repositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.repositoryItemComboBox1.CycleOnDblClick = False
        Me.repositoryItemComboBox1.Name = "repositoryItemComboBox1"
        '
        'iSave
        '
        Me.iSave.Caption = "&Save"
        Me.iSave.CategoryGuid = New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")
        Me.iSave.Enabled = False
        Me.iSave.Id = 30
        Me.iSave.ImageIndex = 1
        Me.iSave.ItemShortcut = New DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S))
        Me.iSave.Name = "iSave"
        '
        'ipsWXP
        '
        Me.ipsWXP.Caption = "Windows XP"
        Me.ipsWXP.CategoryGuid = New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")
        Me.ipsWXP.Description = "WindowsXP"
        Me.ipsWXP.Id = 32
        Me.ipsWXP.ImageIndex = 4
        Me.ipsWXP.Name = "ipsWXP"
        '
        'ipsOXP
        '
        Me.ipsOXP.Caption = "Office XP"
        Me.ipsOXP.CategoryGuid = New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")
        Me.ipsOXP.Description = "OfficeXP"
        Me.ipsOXP.Id = 33
        Me.ipsOXP.ImageIndex = 2
        Me.ipsOXP.Name = "ipsOXP"
        '
        'ipsO2K
        '
        Me.ipsO2K.Caption = "Office 2000"
        Me.ipsO2K.CategoryGuid = New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")
        Me.ipsO2K.Description = "Office2000"
        Me.ipsO2K.Id = 34
        Me.ipsO2K.ImageIndex = 3
        Me.ipsO2K.Name = "ipsO2K"
        '
        'iPaintStyle
        '
        Me.iPaintStyle.Caption = "Paint Style"
        Me.iPaintStyle.CategoryGuid = New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")
        Me.iPaintStyle.Id = 35
        Me.iPaintStyle.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.ipsDefault), New DevExpress.XtraBars.LinkPersistInfo(Me.ipsWXP), New DevExpress.XtraBars.LinkPersistInfo(Me.ipsOXP), New DevExpress.XtraBars.LinkPersistInfo(Me.ipsO2K), New DevExpress.XtraBars.LinkPersistInfo(Me.ipsO3)})
        Me.iPaintStyle.Name = "iPaintStyle"
        Me.iPaintStyle.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'ipsDefault
        '
        Me.ipsDefault.Caption = "Default"
        Me.ipsDefault.CategoryGuid = New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")
        Me.ipsDefault.Description = "Default"
        Me.ipsDefault.Id = 37
        Me.ipsDefault.Name = "ipsDefault"
        '
        'ipsO3
        '
        Me.ipsO3.Caption = "Office 2003"
        Me.ipsO3.CategoryGuid = New System.Guid("b086ef9d-c758-46ba-a35f-058eada7ad13")
        Me.ipsO3.Description = "Office2003"
        Me.ipsO3.Id = 36
        Me.ipsO3.ImageIndex = 5
        Me.ipsO3.Name = "ipsO3"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "About.ico")
        Me.ImageList1.Images.SetKeyName(1, "Accounts.ico")
        Me.ImageList1.Images.SetKeyName(2, "Address Book.ico")
        Me.ImageList1.Images.SetKeyName(3, "Air Brush.ico")
        Me.ImageList1.Images.SetKeyName(4, "Align-Centre-2.ico")
        Me.ImageList1.Images.SetKeyName(5, "Align-Centre.ico")
        Me.ImageList1.Images.SetKeyName(6, "Align-Full-2.ico")
        Me.ImageList1.Images.SetKeyName(7, "Align-Full.ico")
        Me.ImageList1.Images.SetKeyName(8, "Align-Left-2.ico")
        Me.ImageList1.Images.SetKeyName(9, "Align-Left.ico")
        Me.ImageList1.Images.SetKeyName(10, "Align-Right-2.ico")
        Me.ImageList1.Images.SetKeyName(11, "Align-Right.ico")
        Me.ImageList1.Images.SetKeyName(12, "Arrow-Left Right.ico")
        Me.ImageList1.Images.SetKeyName(13, "Arrow-Up Down.ico")
        Me.ImageList1.Images.SetKeyName(14, "Attachment.ico")
        Me.ImageList1.Images.SetKeyName(15, "Auto Correct-Options.ico")
        Me.ImageList1.Images.SetKeyName(16, "Auto-Type.ico")
        Me.ImageList1.Images.SetKeyName(17, "Back.ico")
        Me.ImageList1.Images.SetKeyName(18, "Bar Code.ico")
        Me.ImageList1.Images.SetKeyName(19, "Bold.ico")
        Me.ImageList1.Images.SetKeyName(20, "Book-Open.ico")
        Me.ImageList1.Images.SetKeyName(21, "Books.ico")
        Me.ImageList1.Images.SetKeyName(22, "Book-Search.ico")
        Me.ImageList1.Images.SetKeyName(23, "Borders and Shading.ico")
        Me.ImageList1.Images.SetKeyName(24, "Box-Closed-2.ico")
        Me.ImageList1.Images.SetKeyName(25, "Box-Closed.ico")
        Me.ImageList1.Images.SetKeyName(26, "Box-Open-2.ico")
        Me.ImageList1.Images.SetKeyName(27, "Box-Open.ico")
        Me.ImageList1.Images.SetKeyName(28, "Bullets-2.ico")
        Me.ImageList1.Images.SetKeyName(29, "Bullets.ico")
        Me.ImageList1.Images.SetKeyName(30, "Calculator.ico")
        Me.ImageList1.Images.SetKeyName(31, "Calendar.ico")
        Me.ImageList1.Images.SetKeyName(32, "Calendar-Go To Date.ico")
        Me.ImageList1.Images.SetKeyName(33, "Calendar-Search.ico")
        Me.ImageList1.Images.SetKeyName(34, "Calendar-Select Day.ico")
        Me.ImageList1.Images.SetKeyName(35, "Calendar-Select Month.ico")
        Me.ImageList1.Images.SetKeyName(36, "Calendar-Select Week.ico")
        Me.ImageList1.Images.SetKeyName(37, "Calendar-Select Work Week.ico")
        Me.ImageList1.Images.SetKeyName(38, "Camera.ico")
        Me.ImageList1.Images.SetKeyName(39, "Camera-Photo.ico")
        Me.ImageList1.Images.SetKeyName(40, "Card File.ico")
        Me.ImageList1.Images.SetKeyName(41, "Card.ico")
        Me.ImageList1.Images.SetKeyName(42, "Card-Add.ico")
        Me.ImageList1.Images.SetKeyName(43, "Card-Delete.ico")
        Me.ImageList1.Images.SetKeyName(44, "Card-Edit.ico")
        Me.ImageList1.Images.SetKeyName(45, "Card-New.ico")
        Me.ImageList1.Images.SetKeyName(46, "Card-Next.ico")
        Me.ImageList1.Images.SetKeyName(47, "Card-Previous.ico")
        Me.ImageList1.Images.SetKeyName(48, "Card-Remove.ico")
        Me.ImageList1.Images.SetKeyName(49, "Card-Search.ico")
        Me.ImageList1.Images.SetKeyName(50, "CD-Burn.ico")
        Me.ImageList1.Images.SetKeyName(51, "CD-In Case.ico")
        Me.ImageList1.Images.SetKeyName(52, "CD-ROM.ico")
        Me.ImageList1.Images.SetKeyName(53, "Center Across Cells.ico")
        Me.ImageList1.Images.SetKeyName(54, "Certificate.ico")
        Me.ImageList1.Images.SetKeyName(55, "Chart.ico")
        Me.ImageList1.Images.SetKeyName(56, "Chart-Bar.ico")
        Me.ImageList1.Images.SetKeyName(57, "Chart-Line.ico")
        Me.ImageList1.Images.SetKeyName(58, "Chart-Pie.ico")
        Me.ImageList1.Images.SetKeyName(59, "Chart-Point.ico")
        Me.ImageList1.Images.SetKeyName(60, "Clean.ico")
        Me.ImageList1.Images.SetKeyName(61, "Clear.ico")
        Me.ImageList1.Images.SetKeyName(62, "Clipboard.ico")
        Me.ImageList1.Images.SetKeyName(63, "Clock.ico")
        Me.ImageList1.Images.SetKeyName(64, "Close.ico")
        Me.ImageList1.Images.SetKeyName(65, "Colour Picker.ico")
        Me.ImageList1.Images.SetKeyName(66, "Columns-2.ico")
        Me.ImageList1.Images.SetKeyName(67, "Columns.ico")
        Me.ImageList1.Images.SetKeyName(68, "Command Prompt.ico")
        Me.ImageList1.Images.SetKeyName(69, "Compress.ico")
        Me.ImageList1.Images.SetKeyName(70, "Computer.ico")
        Me.ImageList1.Images.SetKeyName(71, "Connect.ico")
        Me.ImageList1.Images.SetKeyName(72, "Contact.ico")
        Me.ImageList1.Images.SetKeyName(73, "Contact-Add.ico")
        Me.ImageList1.Images.SetKeyName(74, "Contact-Delete.ico")
        Me.ImageList1.Images.SetKeyName(75, "Contact-Edit.ico")
        Me.ImageList1.Images.SetKeyName(76, "Contact-New.ico")
        Me.ImageList1.Images.SetKeyName(77, "Contact-Remove.ico")
        Me.ImageList1.Images.SetKeyName(78, "Contact-Search.ico")
        Me.ImageList1.Images.SetKeyName(79, "Copy.ico")
        Me.ImageList1.Images.SetKeyName(80, "Cross.ico")
        Me.ImageList1.Images.SetKeyName(81, "Currency.ico")
        Me.ImageList1.Images.SetKeyName(82, "Currency-Notes.ico")
        Me.ImageList1.Images.SetKeyName(83, "Cut.ico")
        Me.ImageList1.Images.SetKeyName(84, "Database.ico")
        Me.ImageList1.Images.SetKeyName(85, "Database-Add.ico")
        Me.ImageList1.Images.SetKeyName(86, "Database-Close.ico")
        Me.ImageList1.Images.SetKeyName(87, "Database-Delete.ico")
        Me.ImageList1.Images.SetKeyName(88, "Database-Edit.ico")
        Me.ImageList1.Images.SetKeyName(89, "Database-Filter.ico")
        Me.ImageList1.Images.SetKeyName(90, "Database-New.ico")
        Me.ImageList1.Images.SetKeyName(91, "Database-Open.ico")
        Me.ImageList1.Images.SetKeyName(92, "Database-Properties.ico")
        Me.ImageList1.Images.SetKeyName(93, "Database-Remove.ico")
        Me.ImageList1.Images.SetKeyName(94, "Database-Save.ico")
        Me.ImageList1.Images.SetKeyName(95, "Database-Schema.ico")
        Me.ImageList1.Images.SetKeyName(96, "Database-Search.ico")
        Me.ImageList1.Images.SetKeyName(97, "Database-Security.ico")
        Me.ImageList1.Images.SetKeyName(98, "Database-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(99, "Date-Time.ico")
        Me.ImageList1.Images.SetKeyName(100, "db-Add.ico")
        Me.ImageList1.Images.SetKeyName(101, "db-Cancel.ico")
        Me.ImageList1.Images.SetKeyName(102, "db-Delete.ico")
        Me.ImageList1.Images.SetKeyName(103, "db-Edit.ico")
        Me.ImageList1.Images.SetKeyName(104, "db-First.ico")
        Me.ImageList1.Images.SetKeyName(105, "db-Last.ico")
        Me.ImageList1.Images.SetKeyName(106, "db-Next.ico")
        Me.ImageList1.Images.SetKeyName(107, "db-Post.ico")
        Me.ImageList1.Images.SetKeyName(108, "db-Previous.ico")
        Me.ImageList1.Images.SetKeyName(109, "db-Refresh.ico")
        Me.ImageList1.Images.SetKeyName(110, "Decimals-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(111, "Decimals-Increase.ico")
        Me.ImageList1.Images.SetKeyName(112, "Decompress.ico")
        Me.ImageList1.Images.SetKeyName(113, "Delete.ico")
        Me.ImageList1.Images.SetKeyName(114, "Delete-Blue.ico")
        Me.ImageList1.Images.SetKeyName(115, "Design.ico")
        Me.ImageList1.Images.SetKeyName(116, "Desktop.ico")
        Me.ImageList1.Images.SetKeyName(117, "Dictionary.ico")
        Me.ImageList1.Images.SetKeyName(118, "Disconnect.ico")
        Me.ImageList1.Images.SetKeyName(119, "Discuss.ico")
        Me.ImageList1.Images.SetKeyName(120, "Doc-Access.ico")
        Me.ImageList1.Images.SetKeyName(121, "Doc-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(122, "Doc-Excel.ico")
        Me.ImageList1.Images.SetKeyName(123, "Doc-HTML.ico")
        Me.ImageList1.Images.SetKeyName(124, "Doc-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(125, "Doc-RTF.ico")
        Me.ImageList1.Images.SetKeyName(126, "Document-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(127, "Document-Protect.ico")
        Me.ImageList1.Images.SetKeyName(128, "Doc-Word.ico")
        Me.ImageList1.Images.SetKeyName(129, "Doc-XML.ico")
        Me.ImageList1.Images.SetKeyName(130, "Drawing.ico")
        Me.ImageList1.Images.SetKeyName(131, "Edit.ico")
        Me.ImageList1.Images.SetKeyName(132, "Equaliser.ico")
        Me.ImageList1.Images.SetKeyName(133, "Execute.ico")
        Me.ImageList1.Images.SetKeyName(134, "Exit.ico")
        Me.ImageList1.Images.SetKeyName(135, "Export.ico")
        Me.ImageList1.Images.SetKeyName(136, "Export-Access.ico")
        Me.ImageList1.Images.SetKeyName(137, "Export-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(138, "Export-CSV.ico")
        Me.ImageList1.Images.SetKeyName(139, "Export-Excel.ico")
        Me.ImageList1.Images.SetKeyName(140, "Export-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(141, "Export-HTML.ico")
        Me.ImageList1.Images.SetKeyName(142, "Export-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(143, "Export-RTF.ico")
        Me.ImageList1.Images.SetKeyName(144, "Export-Text.ico")
        Me.ImageList1.Images.SetKeyName(145, "Export-Word.ico")
        Me.ImageList1.Images.SetKeyName(146, "Export-XML.ico")
        Me.ImageList1.Images.SetKeyName(147, "Favorites.ico")
        Me.ImageList1.Images.SetKeyName(148, "Fill.ico")
        Me.ImageList1.Images.SetKeyName(149, "Fill-Down.ico")
        Me.ImageList1.Images.SetKeyName(150, "Fill-Left.ico")
        Me.ImageList1.Images.SetKeyName(151, "Fill-Right.ico")
        Me.ImageList1.Images.SetKeyName(152, "Fill-Up.ico")
        Me.ImageList1.Images.SetKeyName(153, "Filter.ico")
        Me.ImageList1.Images.SetKeyName(154, "Flag.ico")
        Me.ImageList1.Images.SetKeyName(155, "Flag-Delete.ico")
        Me.ImageList1.Images.SetKeyName(156, "Flag-Next.ico")
        Me.ImageList1.Images.SetKeyName(157, "Flag-Previous.ico")
        Me.ImageList1.Images.SetKeyName(158, "Flow Chart.ico")
        Me.ImageList1.Images.SetKeyName(159, "Folder List.ico")
        Me.ImageList1.Images.SetKeyName(160, "Folder.ico")
        Me.ImageList1.Images.SetKeyName(161, "Folder-Closed.ico")
        Me.ImageList1.Images.SetKeyName(162, "Folder-Delete.ico")
        Me.ImageList1.Images.SetKeyName(163, "Folder-New.ico")
        Me.ImageList1.Images.SetKeyName(164, "Folder-Options.ico")
        Me.ImageList1.Images.SetKeyName(165, "Folder-Search.ico")
        Me.ImageList1.Images.SetKeyName(166, "Footer.ico")
        Me.ImageList1.Images.SetKeyName(167, "Format Painter.ico")
        Me.ImageList1.Images.SetKeyName(168, "Format-Font Size.ico")
        Me.ImageList1.Images.SetKeyName(169, "Format-Font.ico")
        Me.ImageList1.Images.SetKeyName(170, "Format-Paragraph.ico")
        Me.ImageList1.Images.SetKeyName(171, "Format-Percentage.ico")
        Me.ImageList1.Images.SetKeyName(172, "Formatting-Remove.ico")
        Me.ImageList1.Images.SetKeyName(173, "Forward.ico")
        Me.ImageList1.Images.SetKeyName(174, "Full Screen.ico")
        Me.ImageList1.Images.SetKeyName(175, "Full Screen-New.ico")
        Me.ImageList1.Images.SetKeyName(176, "Function.ico")
        Me.ImageList1.Images.SetKeyName(177, "Go To Line.ico")
        Me.ImageList1.Images.SetKeyName(178, "Grid-2.ico")
        Me.ImageList1.Images.SetKeyName(179, "Grid.ico")
        Me.ImageList1.Images.SetKeyName(180, "Grid-Auto Format-2.ico")
        Me.ImageList1.Images.SetKeyName(181, "Grid-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(182, "Grid-Column Width.ico")
        Me.ImageList1.Images.SetKeyName(183, "Grid-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(184, "Grid-Delete Cell.ico")
        Me.ImageList1.Images.SetKeyName(185, "Grid-Delete Column.ico")
        Me.ImageList1.Images.SetKeyName(186, "Grid-Delete Row.ico")
        Me.ImageList1.Images.SetKeyName(187, "Grid-Delete.ico")
        Me.ImageList1.Images.SetKeyName(188, "Grid-Insert Cell.ico")
        Me.ImageList1.Images.SetKeyName(189, "Grid-Insert Column Left.ico")
        Me.ImageList1.Images.SetKeyName(190, "Grid-Insert Column Right.ico")
        Me.ImageList1.Images.SetKeyName(191, "Grid-Insert Column.ico")
        Me.ImageList1.Images.SetKeyName(192, "Grid-Insert Row Above.ico")
        Me.ImageList1.Images.SetKeyName(193, "Grid-Insert Row Below.ico")
        Me.ImageList1.Images.SetKeyName(194, "Grid-Insert Row.ico")
        Me.ImageList1.Images.SetKeyName(195, "Grid-Merge Cells.ico")
        Me.ImageList1.Images.SetKeyName(196, "Grid-New-2.ico")
        Me.ImageList1.Images.SetKeyName(197, "Grid-New.ico")
        Me.ImageList1.Images.SetKeyName(198, "Grid-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(199, "Grid-Options.ico")
        Me.ImageList1.Images.SetKeyName(200, "Grid-Row Height.ico")
        Me.ImageList1.Images.SetKeyName(201, "Grid-Select All-2.ico")
        Me.ImageList1.Images.SetKeyName(202, "Grid-Select All.ico")
        Me.ImageList1.Images.SetKeyName(203, "Grid-Select Cell-2.ico")
        Me.ImageList1.Images.SetKeyName(204, "Grid-Select Cell.ico")
        Me.ImageList1.Images.SetKeyName(205, "Grid-Select Column-2.ico")
        Me.ImageList1.Images.SetKeyName(206, "Grid-Select Column.ico")
        Me.ImageList1.Images.SetKeyName(207, "Grid-Select Row-2.ico")
        Me.ImageList1.Images.SetKeyName(208, "Grid-Select Row.ico")
        Me.ImageList1.Images.SetKeyName(209, "Grid-Split Cells.ico")
        Me.ImageList1.Images.SetKeyName(210, "Grid-Wizard-2.ico")
        Me.ImageList1.Images.SetKeyName(211, "Grid-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(212, "Hand Held.ico")
        Me.ImageList1.Images.SetKeyName(213, "Hand Shake.ico")
        Me.ImageList1.Images.SetKeyName(214, "Header and Footer.ico")
        Me.ImageList1.Images.SetKeyName(215, "Header and Footer-Toggle.ico")
        Me.ImageList1.Images.SetKeyName(216, "Header.ico")
        Me.ImageList1.Images.SetKeyName(217, "Header-Next.ico")
        Me.ImageList1.Images.SetKeyName(218, "Header-Previous.ico")
        Me.ImageList1.Images.SetKeyName(219, "Help.ico")
        Me.ImageList1.Images.SetKeyName(220, "Highlighter.ico")
        Me.ImageList1.Images.SetKeyName(221, "History.ico")
        Me.ImageList1.Images.SetKeyName(222, "Home.ico")
        Me.ImageList1.Images.SetKeyName(223, "Image.ico")
        Me.ImageList1.Images.SetKeyName(224, "Image-Brightness.ico")
        Me.ImageList1.Images.SetKeyName(225, "Image-Colour.ico")
        Me.ImageList1.Images.SetKeyName(226, "Image-Contrast.ico")
        Me.ImageList1.Images.SetKeyName(227, "Image-Crop.ico")
        Me.ImageList1.Images.SetKeyName(228, "Image-Flip.ico")
        Me.ImageList1.Images.SetKeyName(229, "Image-Mirror.ico")
        Me.ImageList1.Images.SetKeyName(230, "Image-Paint.ico")
        Me.ImageList1.Images.SetKeyName(231, "Image-Resize.ico")
        Me.ImageList1.Images.SetKeyName(232, "Image-Rotate.ico")
        Me.ImageList1.Images.SetKeyName(233, "Image-Select.ico")
        Me.ImageList1.Images.SetKeyName(234, "Import.ico")
        Me.ImageList1.Images.SetKeyName(235, "Import-Access.ico")
        Me.ImageList1.Images.SetKeyName(236, "Import-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(237, "Import-CSV.ico")
        Me.ImageList1.Images.SetKeyName(238, "Import-Excel.ico")
        Me.ImageList1.Images.SetKeyName(239, "Import-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(240, "Import-HTML.ico")
        Me.ImageList1.Images.SetKeyName(241, "Import-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(242, "Import-RTF.ico")
        Me.ImageList1.Images.SetKeyName(243, "Import-Text.ico")
        Me.ImageList1.Images.SetKeyName(244, "Import-Word.ico")
        Me.ImageList1.Images.SetKeyName(245, "Import-XML.ico")
        Me.ImageList1.Images.SetKeyName(246, "Indent-Decrease-2.ico")
        Me.ImageList1.Images.SetKeyName(247, "Indent-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(248, "Indent-Increase-2.ico")
        Me.ImageList1.Images.SetKeyName(249, "Indent-Increase.ico")
        Me.ImageList1.Images.SetKeyName(250, "Insert.ico")
        Me.ImageList1.Images.SetKeyName(251, "Insert-File.ico")
        Me.ImageList1.Images.SetKeyName(252, "Insert-Image.ico")
        Me.ImageList1.Images.SetKeyName(253, "Insert-Object.ico")
        Me.ImageList1.Images.SetKeyName(254, "Invoice.ico")
        Me.ImageList1.Images.SetKeyName(255, "Italic.ico")
        Me.ImageList1.Images.SetKeyName(256, "Key.ico")
        Me.ImageList1.Images.SetKeyName(257, "Landscape.ico")
        Me.ImageList1.Images.SetKeyName(258, "Launch.ico")
        Me.ImageList1.Images.SetKeyName(259, "Letter.ico")
        Me.ImageList1.Images.SetKeyName(260, "Line Spacing-2.ico")
        Me.ImageList1.Images.SetKeyName(261, "Line Spacing.ico")
        Me.ImageList1.Images.SetKeyName(262, "Link.ico")
        Me.ImageList1.Images.SetKeyName(263, "Lock.ico")
        Me.ImageList1.Images.SetKeyName(264, "Mail.ico")
        Me.ImageList1.Images.SetKeyName(265, "Mail-Forward.ico")
        Me.ImageList1.Images.SetKeyName(266, "Mail-In Box-2.ico")
        Me.ImageList1.Images.SetKeyName(267, "Mail-In Box.ico")
        Me.ImageList1.Images.SetKeyName(268, "Mail-In-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(269, "Mail-New.ico")
        Me.ImageList1.Images.SetKeyName(270, "Mail-Out Box-2.ico")
        Me.ImageList1.Images.SetKeyName(271, "Mail-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(272, "Mail-Reply To All.ico")
        Me.ImageList1.Images.SetKeyName(273, "Mail-Reply.ico")
        Me.ImageList1.Images.SetKeyName(274, "Mail-Search.ico")
        Me.ImageList1.Images.SetKeyName(275, "Mail-Send and Receive.ico")
        Me.ImageList1.Images.SetKeyName(276, "Media.ico")
        Me.ImageList1.Images.SetKeyName(277, "Microphone.ico")
        Me.ImageList1.Images.SetKeyName(278, "Midi Keyboard.ico")
        Me.ImageList1.Images.SetKeyName(279, "Minus.ico")
        Me.ImageList1.Images.SetKeyName(280, "mm-Eject.ico")
        Me.ImageList1.Images.SetKeyName(281, "mm-Fast Forward.ico")
        Me.ImageList1.Images.SetKeyName(282, "mm-First.ico")
        Me.ImageList1.Images.SetKeyName(283, "mm-Last.ico")
        Me.ImageList1.Images.SetKeyName(284, "mm-Pause.ico")
        Me.ImageList1.Images.SetKeyName(285, "mm-Play.ico")
        Me.ImageList1.Images.SetKeyName(286, "mm-Rewind.ico")
        Me.ImageList1.Images.SetKeyName(287, "mm-Stop.ico")
        Me.ImageList1.Images.SetKeyName(288, "Monitor.ico")
        Me.ImageList1.Images.SetKeyName(289, "Movie.ico")
        Me.ImageList1.Images.SetKeyName(290, "Musical Note-2.ico")
        Me.ImageList1.Images.SetKeyName(291, "Musical Note.ico")
        Me.ImageList1.Images.SetKeyName(292, "New.ico")
        Me.ImageList1.Images.SetKeyName(293, "Note.ico")
        Me.ImageList1.Images.SetKeyName(294, "Note-Add.ico")
        Me.ImageList1.Images.SetKeyName(295, "Note-Delete.ico")
        Me.ImageList1.Images.SetKeyName(296, "Note-Edit.ico")
        Me.ImageList1.Images.SetKeyName(297, "Note-New.ico")
        Me.ImageList1.Images.SetKeyName(298, "Note-Next.ico")
        Me.ImageList1.Images.SetKeyName(299, "Notepad.ico")
        Me.ImageList1.Images.SetKeyName(300, "Note-Previous.ico")
        Me.ImageList1.Images.SetKeyName(301, "Note-Remove.ico")
        Me.ImageList1.Images.SetKeyName(302, "Notes.ico")
        Me.ImageList1.Images.SetKeyName(303, "Note-Search.ico")
        Me.ImageList1.Images.SetKeyName(304, "Number of Pages.ico")
        Me.ImageList1.Images.SetKeyName(305, "Numbering-2.ico")
        Me.ImageList1.Images.SetKeyName(306, "Numbering.ico")
        Me.ImageList1.Images.SetKeyName(307, "Object-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(308, "Object-Select.ico")
        Me.ImageList1.Images.SetKeyName(309, "Object-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(310, "Open.ico")
        Me.ImageList1.Images.SetKeyName(311, "Outline-2.ico")
        Me.ImageList1.Images.SetKeyName(312, "Outline.ico")
        Me.ImageList1.Images.SetKeyName(313, "Page Number.ico")
        Me.ImageList1.Images.SetKeyName(314, "Page-Break.ico")
        Me.ImageList1.Images.SetKeyName(315, "Page-Invert.ico")
        Me.ImageList1.Images.SetKeyName(316, "Page-Next.ico")
        Me.ImageList1.Images.SetKeyName(317, "Page-Previous.ico")
        Me.ImageList1.Images.SetKeyName(318, "Page-Setup.ico")
        Me.ImageList1.Images.SetKeyName(319, "Paintbrush.ico")
        Me.ImageList1.Images.SetKeyName(320, "Parcel.ico")
        Me.ImageList1.Images.SetKeyName(321, "Paste.ico")
        Me.ImageList1.Images.SetKeyName(322, "Permission.ico")
        Me.ImageList1.Images.SetKeyName(323, "Phone.ico")
        Me.ImageList1.Images.SetKeyName(324, "Planner.ico")
        Me.ImageList1.Images.SetKeyName(325, "Plus.ico")
        Me.ImageList1.Images.SetKeyName(326, "Portrait.ico")
        Me.ImageList1.Images.SetKeyName(327, "Preview.ico")
        Me.ImageList1.Images.SetKeyName(328, "Print-2.ico")
        Me.ImageList1.Images.SetKeyName(329, "Print.ico")
        Me.ImageList1.Images.SetKeyName(330, "Print-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(331, "Print-Options.ico")
        Me.ImageList1.Images.SetKeyName(332, "Procedure.ico")
        Me.ImageList1.Images.SetKeyName(333, "Process.ico")
        Me.ImageList1.Images.SetKeyName(334, "Project.ico")
        Me.ImageList1.Images.SetKeyName(335, "Project-Gant Chart.ico")
        Me.ImageList1.Images.SetKeyName(336, "Properties.ico")
        Me.ImageList1.Images.SetKeyName(337, "Push Pin.ico")
        Me.ImageList1.Images.SetKeyName(338, "Recycle Bin.ico")
        Me.ImageList1.Images.SetKeyName(339, "Redo.ico")
        Me.ImageList1.Images.SetKeyName(340, "Refresh.ico")
        Me.ImageList1.Images.SetKeyName(341, "Remove.ico")
        Me.ImageList1.Images.SetKeyName(342, "Rename.ico")
        Me.ImageList1.Images.SetKeyName(343, "Report-2.ico")
        Me.ImageList1.Images.SetKeyName(344, "Report.ico")
        Me.ImageList1.Images.SetKeyName(345, "Report-Bound.ico")
        Me.ImageList1.Images.SetKeyName(346, "Research.ico")
        Me.ImageList1.Images.SetKeyName(347, "Ruler.ico")
        Me.ImageList1.Images.SetKeyName(348, "Ruler-Formatting.ico")
        Me.ImageList1.Images.SetKeyName(349, "Save All.ico")
        Me.ImageList1.Images.SetKeyName(350, "Save As HTML.ico")
        Me.ImageList1.Images.SetKeyName(351, "Save As.ico")
        Me.ImageList1.Images.SetKeyName(352, "Save Draft.ico")
        Me.ImageList1.Images.SetKeyName(353, "Save.ico")
        Me.ImageList1.Images.SetKeyName(354, "Script.ico")
        Me.ImageList1.Images.SetKeyName(355, "Search.ico")
        Me.ImageList1.Images.SetKeyName(356, "Search-Next.ico")
        Me.ImageList1.Images.SetKeyName(357, "Search-Previous.ico")
        Me.ImageList1.Images.SetKeyName(358, "Search-Replace.ico")
        Me.ImageList1.Images.SetKeyName(359, "Select All.ico")
        Me.ImageList1.Images.SetKeyName(360, "Slide Show.ico")
        Me.ImageList1.Images.SetKeyName(361, "Slide.ico")
        Me.ImageList1.Images.SetKeyName(362, "Software-Analyse.ico")
        Me.ImageList1.Images.SetKeyName(363, "Sort-Ascending.ico")
        Me.ImageList1.Images.SetKeyName(364, "Sort-Descending.ico")
        Me.ImageList1.Images.SetKeyName(365, "Sound-Delete.ico")
        Me.ImageList1.Images.SetKeyName(366, "Sound-Fade In.ico")
        Me.ImageList1.Images.SetKeyName(367, "Sound-Fade Out.ico")
        Me.ImageList1.Images.SetKeyName(368, "Sound-Join.ico")
        Me.ImageList1.Images.SetKeyName(369, "Sound-Mark End.ico")
        Me.ImageList1.Images.SetKeyName(370, "Sound-Mark Start.ico")
        Me.ImageList1.Images.SetKeyName(371, "Sound-Split.ico")
        Me.ImageList1.Images.SetKeyName(372, "Spell Check.ico")
        Me.ImageList1.Images.SetKeyName(373, "Stop.ico")
        Me.ImageList1.Images.SetKeyName(374, "Strikeout.ico")
        Me.ImageList1.Images.SetKeyName(375, "Subscript.ico")
        Me.ImageList1.Images.SetKeyName(376, "Sum.ico")
        Me.ImageList1.Images.SetKeyName(377, "Summary.ico")
        Me.ImageList1.Images.SetKeyName(378, "Superscript.ico")
        Me.ImageList1.Images.SetKeyName(379, "Support.ico")
        Me.ImageList1.Images.SetKeyName(380, "Tables and Borders.ico")
        Me.ImageList1.Images.SetKeyName(381, "Task.ico")
        Me.ImageList1.Images.SetKeyName(382, "Task-Search.ico")
        Me.ImageList1.Images.SetKeyName(383, "Thesaurus.ico")
        Me.ImageList1.Images.SetKeyName(384, "Tick.ico")
        Me.ImageList1.Images.SetKeyName(385, "Time Line.ico")
        Me.ImageList1.Images.SetKeyName(386, "Tip.ico")
        Me.ImageList1.Images.SetKeyName(387, "To Do List.ico")
        Me.ImageList1.Images.SetKeyName(388, "To Lowercase.ico")
        Me.ImageList1.Images.SetKeyName(389, "To Uppercase.ico")
        Me.ImageList1.Images.SetKeyName(390, "Tools.ico")
        Me.ImageList1.Images.SetKeyName(391, "Tree View.ico")
        Me.ImageList1.Images.SetKeyName(392, "Tree View-Add.ico")
        Me.ImageList1.Images.SetKeyName(393, "Tree View-Delete.ico")
        Me.ImageList1.Images.SetKeyName(394, "Tree View-Edit.ico")
        Me.ImageList1.Images.SetKeyName(395, "Tree View-New.ico")
        Me.ImageList1.Images.SetKeyName(396, "Tree View-Remove.ico")
        Me.ImageList1.Images.SetKeyName(397, "Tree View-Search.ico")
        Me.ImageList1.Images.SetKeyName(398, "Underline.ico")
        Me.ImageList1.Images.SetKeyName(399, "Undo.ico")
        Me.ImageList1.Images.SetKeyName(400, "Unlock.ico")
        Me.ImageList1.Images.SetKeyName(401, "User-2.ico")
        Me.ImageList1.Images.SetKeyName(402, "User.ico")
        Me.ImageList1.Images.SetKeyName(403, "User-Add-2.ico")
        Me.ImageList1.Images.SetKeyName(404, "User-Add.ico")
        Me.ImageList1.Images.SetKeyName(405, "User-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(406, "User-Delete.ico")
        Me.ImageList1.Images.SetKeyName(407, "User-Edit-2.ico")
        Me.ImageList1.Images.SetKeyName(408, "User-Edit.ico")
        Me.ImageList1.Images.SetKeyName(409, "User-New-2.ico")
        Me.ImageList1.Images.SetKeyName(410, "User-New.ico")
        Me.ImageList1.Images.SetKeyName(411, "User-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(412, "User-Options.ico")
        Me.ImageList1.Images.SetKeyName(413, "User-Password-2.ico")
        Me.ImageList1.Images.SetKeyName(414, "User-Password.ico")
        Me.ImageList1.Images.SetKeyName(415, "User-Remove-2.ico")
        Me.ImageList1.Images.SetKeyName(416, "User-Remove.ico")
        Me.ImageList1.Images.SetKeyName(417, "Users-2.ico")
        Me.ImageList1.Images.SetKeyName(418, "Users.ico")
        Me.ImageList1.Images.SetKeyName(419, "User-Search-2.ico")
        Me.ImageList1.Images.SetKeyName(420, "User-Search.ico")
        Me.ImageList1.Images.SetKeyName(421, "User-Security-2.ico")
        Me.ImageList1.Images.SetKeyName(422, "User-Security.ico")
        Me.ImageList1.Images.SetKeyName(423, "View-Details.ico")
        Me.ImageList1.Images.SetKeyName(424, "View-Large Icons.ico")
        Me.ImageList1.Images.SetKeyName(425, "View-List.ico")
        Me.ImageList1.Images.SetKeyName(426, "View-One Page.ico")
        Me.ImageList1.Images.SetKeyName(427, "View-Page Width.ico")
        Me.ImageList1.Images.SetKeyName(428, "Views.ico")
        Me.ImageList1.Images.SetKeyName(429, "View-Small Icons.ico")
        Me.ImageList1.Images.SetKeyName(430, "Volume.ico")
        Me.ImageList1.Images.SetKeyName(431, "Volume-Mute.ico")
        Me.ImageList1.Images.SetKeyName(432, "Warning.ico")
        Me.ImageList1.Images.SetKeyName(433, "Waste Bin.ico")
        Me.ImageList1.Images.SetKeyName(434, "Window.ico")
        Me.ImageList1.Images.SetKeyName(435, "Window-Add.ico")
        Me.ImageList1.Images.SetKeyName(436, "Window-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(437, "Window-Cascade.ico")
        Me.ImageList1.Images.SetKeyName(438, "Window-Close.ico")
        Me.ImageList1.Images.SetKeyName(439, "Window-Delete.ico")
        Me.ImageList1.Images.SetKeyName(440, "Window-Edit.ico")
        Me.ImageList1.Images.SetKeyName(441, "Window-Maximise.ico")
        Me.ImageList1.Images.SetKeyName(442, "Window-Minimise.ico")
        Me.ImageList1.Images.SetKeyName(443, "Window-New.ico")
        Me.ImageList1.Images.SetKeyName(444, "Window-Next.ico")
        Me.ImageList1.Images.SetKeyName(445, "Window-Options.ico")
        Me.ImageList1.Images.SetKeyName(446, "Window-Preview.ico")
        Me.ImageList1.Images.SetKeyName(447, "Window-Previous.ico")
        Me.ImageList1.Images.SetKeyName(448, "Window-Properties.ico")
        Me.ImageList1.Images.SetKeyName(449, "Window-Remove.ico")
        Me.ImageList1.Images.SetKeyName(450, "Window-Search.ico")
        Me.ImageList1.Images.SetKeyName(451, "Window-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(452, "Window-Split.ico")
        Me.ImageList1.Images.SetKeyName(453, "Window-Tile Horizontal.ico")
        Me.ImageList1.Images.SetKeyName(454, "Window-Tile Vertical.ico")
        Me.ImageList1.Images.SetKeyName(455, "Wizard.ico")
        Me.ImageList1.Images.SetKeyName(456, "Word Count.ico")
        Me.ImageList1.Images.SetKeyName(457, "Word Wrap-2.ico")
        Me.ImageList1.Images.SetKeyName(458, "Word Wrap.ico")
        Me.ImageList1.Images.SetKeyName(459, "Work Sheet.ico")
        Me.ImageList1.Images.SetKeyName(460, "Work Sheet-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(461, "Work Sheet-Delete.ico")
        Me.ImageList1.Images.SetKeyName(462, "Work Sheet-New.ico")
        Me.ImageList1.Images.SetKeyName(463, "Work Sheet-Options.ico")
        Me.ImageList1.Images.SetKeyName(464, "Work Sheet-Protect.ico")
        Me.ImageList1.Images.SetKeyName(465, "Work Sheet-Rename.ico")
        Me.ImageList1.Images.SetKeyName(466, "Zoom-In.ico")
        Me.ImageList1.Images.SetKeyName(467, "Zoom-Out.ico")
        '
        'tmrMainCounters
        '
        Me.tmrMainCounters.Interval = 300000
        '
        'frmDDMain
        '
        Me.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Appearance.Options.UseFont = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(987, 688)
        Me.Controls.Add(Me.SplitterControl1)
        Me.Controls.Add(Me.NavBarMarketing)
        Me.Controls.Add(Me.barDockControl3)
        Me.Controls.Add(Me.barDockControl4)
        Me.Controls.Add(Me.barDockControl2)
        Me.Controls.Add(Me.barDockControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "frmDDMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DateDEALS Control Panel Administrator"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.xtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarMarketing, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ImageCollectionNaviSmall, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.barManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repositoryItemProgressBar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.repositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SitesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DxErrorProvider1 As DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider
    Private WithEvents xtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents NavBarMarketing As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavAgencysService As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents SplitterControl1 As DevExpress.XtraEditors.SplitterControl
    Private WithEvents barDockControl3 As DevExpress.XtraBars.BarDockControl
    Private WithEvents barDockControl4 As DevExpress.XtraBars.BarDockControl
    Private WithEvents barDockControl2 As DevExpress.XtraBars.BarDockControl
    Private WithEvents barDockControl1 As DevExpress.XtraBars.BarDockControl
    Private WithEvents barManager1 As DevExpress.XtraBars.BarManager
    Private WithEvents iBack As DevExpress.XtraBars.BarLargeButtonItem
    Private WithEvents iForward As DevExpress.XtraBars.BarLargeButtonItem
    Private WithEvents iStop As DevExpress.XtraBars.BarLargeButtonItem
    Private WithEvents iRefresh As DevExpress.XtraBars.BarLargeButtonItem
    Private WithEvents iHome As DevExpress.XtraBars.BarLargeButtonItem
    Private WithEvents iSearch As DevExpress.XtraBars.BarLargeButtonItem
    Private WithEvents iFavorites As DevExpress.XtraBars.BarLargeButtonItem
    Private WithEvents iHistory As DevExpress.XtraBars.BarLargeButtonItem
    Private WithEvents iMail As DevExpress.XtraBars.BarLargeButtonItem
    Private WithEvents iPrint As DevExpress.XtraBars.BarLargeButtonItem
    Private WithEvents iEdit As DevExpress.XtraBars.BarLargeButtonItem
    Private WithEvents bar2 As DevExpress.XtraBars.Bar
    Private WithEvents siFile As DevExpress.XtraBars.BarSubItem
    Private WithEvents iSwitchUser As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iSave As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iExit As DevExpress.XtraBars.BarButtonItem
    Private WithEvents siView As DevExpress.XtraBars.BarSubItem
    Private WithEvents iToolBars As DevExpress.XtraBars.BarToolbarsListItem
    Private WithEvents siTools As DevExpress.XtraBars.BarSubItem
    Private WithEvents iOptions As DevExpress.XtraBars.BarButtonItem
    Private WithEvents siHelp As DevExpress.XtraBars.BarSubItem
    Private WithEvents iAbout As DevExpress.XtraBars.BarButtonItem
    Private WithEvents eAddress As DevExpress.XtraBars.BarEditItem
    Private WithEvents repositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Private WithEvents iGo As DevExpress.XtraBars.BarButtonItem
    Private WithEvents bar4 As DevExpress.XtraBars.Bar
    Private WithEvents iText As DevExpress.XtraBars.BarStaticItem
    Private WithEvents eProgress As DevExpress.XtraBars.BarEditItem
    Private WithEvents repositoryItemProgressBar1 As DevExpress.XtraEditors.Repository.RepositoryItemProgressBar
    Private WithEvents iPaintStyle As DevExpress.XtraBars.BarSubItem
    Private WithEvents ipsDefault As DevExpress.XtraBars.BarButtonItem
    Private WithEvents ipsWXP As DevExpress.XtraBars.BarButtonItem
    Private WithEvents ipsOXP As DevExpress.XtraBars.BarButtonItem
    Private WithEvents ipsO2K As DevExpress.XtraBars.BarButtonItem
    Private WithEvents ipsO3 As DevExpress.XtraBars.BarButtonItem
    Private WithEvents iMedia As DevExpress.XtraBars.BarLargeButtonItem
    Friend WithEvents NavMenus As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavPrograms As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavRoles As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavUsers As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavLoginAccessHistory As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavWorksFromUsers As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents ImageCollectionNaviSmall As DevExpress.Utils.ImageCollection
    Friend WithEvents NavInstalledPrograms As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavLicenses As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavFeedbackActivity As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavCustomers As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavSMSAccounts As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavSMSTranstactions As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents BarSendSMSTest As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonZeveraBringe As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents NavProviders As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavSalesSites As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavProducts As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavTransactions As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavPages As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBasic As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents SitesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents NavBarGroupMarketingTools As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavEmailOfferSettings As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarEmailMessages As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarGroupProfiles As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents navProfiles As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navNewProfiles As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navUpdatingProfiles As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarGroupFiles As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarGroupLists As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents navSYS_CountriesGEO As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_BodyType As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_ChildrenNumber As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_Drinking As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_Education As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_Ethnicity As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_EyeColor As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_HairColor As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_Height As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_Income As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_NetWorth As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_RejectingReasons As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_RelationshipStatus As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_Religion As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_Smoking As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navApprovedProfiles As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navRejectedNewProfiles As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_AccountType As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_Gender As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navDeletedProfiles As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_PhotosDisplayLevel As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents navEUS_LISTS_TypeOfDating As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavDeletedByUser As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarGroupTools As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarItemTools_UpdateGEOGR As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItemTools_ReplaceDBString As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarGroupConfig2 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarItemAutoApprove As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItemNewPhotos As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItemApprovedPhotos As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItemDeletedPhotos As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItemAutoApprovedProfiles As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItemAutoApprovedPhotos As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents tmrMainCounters As System.Windows.Forms.Timer
End Class

﻿Imports DevExpress.XtraEditors.Controls

Public Class frmProfileRejecting
    Public Done As Boolean = False

    Private Sub frmDDRejectingProfile_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            ShowWaitWin("Working please wait...")
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetEUS_LISTS_RejectingReasons")
            CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_RejectingReasons(gAdminWSHeaders, Me.DSLists))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetEUS_LISTS_RejectingReasons")

            ClsCombos.FillComboUsingDatatable(Me.DSLists.EUS_LISTS_RejectingReasons, "US", "RejectingReasonsId", icbRejectingReasonID, True)
            icbRejectingReasonID.SelectedIndex = 0
            LoadText()

            UchtmlEditor1.TABDesign.Select()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        Finally
            HideWaitWin()
        End Try
    End Sub

    Private Sub icbRejectingReasonID_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles icbRejectingReasonID.SelectedIndexChanged
        LoadText()
    End Sub


    Private Sub LoadText()

        For Each dr As DataRow In Me.DSLists.EUS_LISTS_RejectingReasons.Rows
            If (DirectCast(icbRejectingReasonID.SelectedItem, ImageComboBoxItem).Value = dr("RejectingReasonsId")) Then
                UchtmlEditor1.HTMLBody = dr("Text" & "US").ToString()
                UchtmlEditor1.RefreshEditor()
                Exit For
            End If
        Next

    End Sub


    Private Sub btnDone_Click(sender As System.Object, e As System.EventArgs) Handles btnDone.Click
        'If (Me.icbRejectingReasonID.SelectedIndex > 0) Then
        Done = True
        'End If
        Me.Hide()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Done = False
        Me.Close()
    End Sub

    Public Function GetReason() As String
        'If (Me.icbRejectingReasonID.SelectedIndex > 0) Then
        Return DirectCast(Me.icbRejectingReasonID.SelectedItem, ImageComboBoxItem).Description
        'End If
        'Return Nothing
    End Function


    Public Function GetReasonText() As String
        'If (Me.icbRejectingReasonID.SelectedIndex > 0) Then
        UchtmlEditor1.RefreshEditorToDB()
        Return UchtmlEditor1.HTMLBody
        'End If
        'Return Nothing
    End Function



End Class
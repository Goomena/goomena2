﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSysEmailMessagesItem
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txMessageAction = New DevExpress.XtraEditors.TextEdit()
        Me.SYS_EmailMessagesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.UniCMSDB = New Dating.Client.Admin.APP.AdminWS.UniCMSDB()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.UchtmlEditor1 = New Dating.Client.Admin.APP.UCHTMLEditor()
        Me.CmdOK = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.cbLAG = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.CmdSAVE = New DevExpress.XtraEditors.SimpleButton()
        Me.txMessageSubject = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.txtPaymentMethod = New DevExpress.XtraEditors.TextEdit()
        CType(Me.txMessageAction.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SYS_EmailMessagesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.UniCMSDB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.cbLAG.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txMessageSubject.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txMessageAction
        '
        Me.txMessageAction.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.SYS_EmailMessagesBindingSource, "MessageAction", True))
        Me.txMessageAction.Enabled = False
        Me.txMessageAction.Location = New System.Drawing.Point(188, 13)
        Me.txMessageAction.Name = "txMessageAction"
        Me.txMessageAction.Size = New System.Drawing.Size(545, 20)
        Me.txMessageAction.TabIndex = 0
        '
        'SYS_EmailMessagesBindingSource
        '
        Me.SYS_EmailMessagesBindingSource.DataMember = "SYS_EmailMessages"
        Me.SYS_EmailMessagesBindingSource.DataSource = Me.UniCMSDB
        '
        'UniCMSDB
        '
        Me.UniCMSDB.DataSetName = "UniCMSDB"
        Me.UniCMSDB.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(170, 22)
        Me.LabelControl1.TabIndex = 6
        Me.LabelControl1.Text = "Message Action:"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.UchtmlEditor1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 66)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(822, 379)
        Me.GroupControl1.TabIndex = 8
        Me.GroupControl1.Text = "Body Message"
        '
        'UchtmlEditor1
        '
        Me.UchtmlEditor1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UchtmlEditor1.Location = New System.Drawing.Point(2, 21)
        Me.UchtmlEditor1.Name = "UchtmlEditor1"
        Me.UchtmlEditor1.Size = New System.Drawing.Size(818, 356)
        Me.UchtmlEditor1.TabIndex = 0
        '
        'CmdOK
        '
        Me.CmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdOK.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.CmdOK.Appearance.Options.UseFont = True
        Me.CmdOK.Location = New System.Drawing.Point(517, 451)
        Me.CmdOK.Name = "CmdOK"
        Me.CmdOK.Size = New System.Drawing.Size(168, 39)
        Me.CmdOK.TabIndex = 9
        Me.CmdOK.Text = "SAVE && CLOSE"
        '
        'CmdCancel
        '
        Me.CmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.CmdCancel.Appearance.Options.UseFont = True
        Me.CmdCancel.Location = New System.Drawing.Point(691, 451)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.Size = New System.Drawing.Size(128, 39)
        Me.CmdCancel.TabIndex = 10
        Me.CmdCancel.Text = "Cancel"
        '
        'cbLAG
        '
        Me.cbLAG.EditValue = "US"
        Me.cbLAG.Location = New System.Drawing.Point(755, 14)
        Me.cbLAG.Name = "cbLAG"
        Me.cbLAG.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.cbLAG.Properties.Appearance.Options.UseFont = True
        Me.cbLAG.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLAG.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.ImageComboBoxItem() {New DevExpress.XtraEditors.Controls.ImageComboBoxItem("US", "US", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("GR", "GR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("HU", "HU", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("DE", "DE", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("AL", "AL", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("SR", "SR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("RU", "RU", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("RO", "RO", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("BG", "BG", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("TR", "TR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("FR", "FR", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("ES", "ES", -1), New DevExpress.XtraEditors.Controls.ImageComboBoxItem("IT", "IT", -1)})
        Me.cbLAG.Size = New System.Drawing.Size(72, 20)
        Me.cbLAG.TabIndex = 12
        '
        'CmdSAVE
        '
        Me.CmdSAVE.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdSAVE.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.CmdSAVE.Appearance.Options.UseFont = True
        Me.CmdSAVE.Location = New System.Drawing.Point(343, 451)
        Me.CmdSAVE.Name = "CmdSAVE"
        Me.CmdSAVE.Size = New System.Drawing.Size(168, 39)
        Me.CmdSAVE.TabIndex = 13
        Me.CmdSAVE.Text = "SAVE"
        '
        'txMessageSubject
        '
        Me.txMessageSubject.Location = New System.Drawing.Point(188, 39)
        Me.txMessageSubject.Name = "txMessageSubject"
        Me.txMessageSubject.Size = New System.Drawing.Size(277, 20)
        Me.txMessageSubject.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(91, 40)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(91, 22)
        Me.LabelControl2.TabIndex = 6
        Me.LabelControl2.Text = "Subject:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(471, 37)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(91, 22)
        Me.LabelControl3.TabIndex = 15
        Me.LabelControl3.Text = "Payment:"
        '
        'txtPaymentMethod
        '
        Me.txtPaymentMethod.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.SYS_EmailMessagesBindingSource, "PaymentMethod", True))
        Me.txtPaymentMethod.Location = New System.Drawing.Point(568, 39)
        Me.txtPaymentMethod.Name = "txtPaymentMethod"
        Me.txtPaymentMethod.Size = New System.Drawing.Size(165, 20)
        Me.txtPaymentMethod.TabIndex = 14
        '
        'frmSysEmailMessagesItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(839, 502)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.txtPaymentMethod)
        Me.Controls.Add(Me.CmdSAVE)
        Me.Controls.Add(Me.cbLAG)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.CmdOK)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.txMessageSubject)
        Me.Controls.Add(Me.txMessageAction)
        Me.Name = "frmSysEmailMessagesItem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Page Custom Item"
        CType(Me.txMessageAction.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SYS_EmailMessagesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.UniCMSDB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.cbLAG.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txMessageSubject.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txMessageAction As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CmdOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbLAG As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents CmdSAVE As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents UniCMSDB As Dating.Client.Admin.APP.AdminWS.UniCMSDB
    Friend WithEvents txMessageSubject As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtPaymentMethod As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SYS_EmailMessagesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents UchtmlEditor1 As Dating.Client.Admin.APP.UCHTMLEditor
End Class

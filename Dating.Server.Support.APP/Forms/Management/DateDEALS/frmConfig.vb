﻿
Public Class frmConfig

    Private Sub frmConfig_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            _Load()
            'Me.SYS_ConfigBindingSource.DataSource = Me.UniCMSDB.SYS_Config


            'ClsCombos.FillComboUsingDatatable(Me.DSLists.EUS_LISTS_RejectingReasons, "US", "RejectingReasonsId", icbRejectingReasonID, True)
            'icbRejectingReasonID.SelectedIndex = 0
            'LoadText()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub



    Private Sub cmdSave1_Click(sender As System.Object, e As System.EventArgs) Handles cmdSave1.Click
        _Save()
    End Sub

    Private Sub cmdCancel1_Click(sender As System.Object, e As System.EventArgs) Handles cmdCancel1.Click
        _Load()
    End Sub

    Private Sub _Load()
        ShowWaitWin("Working please wait...")

        Try

            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetSYS_Config")
            CheckWebServiceCallResult(gAdminWS.GetSYS_Config(gAdminWSHeaders, Me.UniCMSDB))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetSYS_Config")


            Dim cnt As Integer
            For cnt = 0 To Me.UniCMSDB.SYS_Config.Rows.Count - 1
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.Rows(cnt)
                If (row.ConfigName = "auto_approve_new_profiles" AndAlso row.ConfigValue = "1") Then
                    chkAutoApproveNewProfiles.Checked = True
                End If
                If (row.ConfigName = "auto_approve_updating_profiles" AndAlso row.ConfigValue = "1") Then
                    chkAutoApproveUpdatingProfiles.Checked = True
                End If
                If (row.ConfigName = "auto_approve_photos" AndAlso row.ConfigValue = "1") Then
                    chkAutoApprovePhotos.Checked = True
                End If
            Next

            SetUI_OnChanged(False)

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try

        HideWaitWin()

    End Sub


    Private Sub _Save()
        Try
            ShowWaitWin("Working please wait...")

            'Dim row As SYS_ConfigRow = From itm In Me.UniCMSDB.SYS_Config.Rows
            Dim auto_approve_new_profilesSet As Boolean = False
            Dim auto_approve_updating_profilesSet As Boolean = False
            Dim auto_approve_photosSet As Boolean = False

            Dim cnt As Integer
            For cnt = 0 To Me.UniCMSDB.SYS_Config.Rows.Count - 1
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.Rows(cnt)

                If (row.ConfigName = "auto_approve_new_profiles") Then
                    If (chkAutoApproveNewProfiles.Checked) Then
                        row.ConfigValue = "1"
                    Else
                        row.ConfigValue = "0"
                    End If
                    auto_approve_new_profilesSet = True
                End If

                If (row.ConfigName = "auto_approve_updating_profiles") Then
                    If (chkAutoApproveUpdatingProfiles.Checked) Then
                        row.ConfigValue = "1"
                    Else
                        row.ConfigValue = "0"
                    End If
                    auto_approve_updating_profilesSet = True
                End If

                If (row.ConfigName = "auto_approve_photos") Then
                    If (chkAutoApprovePhotos.Checked) Then
                        row.ConfigValue = "1"
                    Else
                        row.ConfigValue = "0"
                    End If
                    auto_approve_photosSet = True
                End If

            Next

            If (Not auto_approve_new_profilesSet) Then
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "auto_approve_new_profiles"
                If (chkAutoApproveNewProfiles.Checked) Then
                    row.ConfigValue = "1"
                Else
                    row.ConfigValue = "0"
                End If
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

            If (Not auto_approve_updating_profilesSet) Then
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "auto_approve_updating_profiles"
                If (chkAutoApproveNewProfiles.Checked) Then
                    row.ConfigValue = "1"
                Else
                    row.ConfigValue = "0"
                End If
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If

            If (Not auto_approve_photosSet) Then
                Dim row As AdminWS.UniCMSDB.SYS_ConfigRow = Me.UniCMSDB.SYS_Config.NewSYS_ConfigRow()
                row.ConfigName = "auto_approve_photos"
                If (chkAutoApproveNewProfiles.Checked) Then
                    row.ConfigValue = "1"
                Else
                    row.ConfigValue = "0"
                End If
                Me.UniCMSDB.SYS_Config.Rows.Add(row)
            End If


            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateSYS_Config")
            CheckWebServiceCallResult(gAdminWS.UpdateSYS_Config(gAdminWSHeaders, Me.UniCMSDB))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateSYS_Config")

            SetUI_OnChanged(False)
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        Finally
            HideWaitWin()
        End Try

    End Sub

    Private Sub chkAutoApproveNewProfiles_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkAutoApproveNewProfiles.CheckedChanged
        SetUI_OnChanged(True)
    End Sub


    Private Sub chkAutoApproveUpdatingProfiles_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkAutoApproveUpdatingProfiles.CheckedChanged
        SetUI_OnChanged(True)
    End Sub

    Private Sub chkAutoApprovePhotos_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkAutoApprovePhotos.CheckedChanged
        SetUI_OnChanged(True)
    End Sub

    Sub SetUI_OnChanged(enable As Boolean)
        cmdSave1.Enabled = enable
        cmdCancel1.Enabled = enable
    End Sub
End Class
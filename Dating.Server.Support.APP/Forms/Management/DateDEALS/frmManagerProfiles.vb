﻿Imports DevExpress.XtraVerticalGrid
Imports Library.Public
Imports Dating.Server.Core.DLL

Public Class frmManagerProfiles

    '    SELECT  User.UserId
    'FROM    ZipCode AS MyZipCode
    '        INNER JOIN ZipDistance ON MyZipCode.ZipCode = ZipDistance.MyZipCode
    '        INNER JOIN ZipCode AS TheirZipCode ON ZipDistance.OtherZipCode = TheirZipCode.ZipCode
    '        INNER JOIN User AS User ON TheirZipCode.ZipCode = User.ZipCode
    'WHERE   ( MyZipCode.ZipCode = 75044 )
    '        AND ( ZipDistance.Distance < 50 )

    'http://www.postcode-distance.com/ajax_results?c=gr&z=41100&r=150&u=2&s=
    'http://www.postcode-distance.com/ajax_results?c=COUNTRY&z=ZIPCODE&r=AREA IN KM&u=1=MILES,2=KM&s=


    Public Property WhatProfilesToLoad As ProfileStatusEnum


    Private Sub frmTerminalsManager_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.BarEDIT.Enabled = False
        Me.BarDELETE.Enabled = False


        If (Me.WhatProfilesToLoad = ProfileStatusEnum.Deleted) Then
            BarDELETE.Caption = "Delete Permanently"
            bciShowNonMasterRecords.Checked = True

        ElseIf (Me.WhatProfilesToLoad = ProfileStatusEnum.Approved) Then
            bciShowNonMasterRecords.Checked = True

        ElseIf (Me.WhatProfilesToLoad = ProfileStatusEnum.NewProfile) Then
            bciShowNonMasterRecords.Checked = True

        ElseIf (Me.WhatProfilesToLoad = ProfileStatusEnum.Rejected) Then
            bciShowNonMasterRecords.Checked = True

        ElseIf (Me.WhatProfilesToLoad = ProfileStatusEnum.Updating) Then
            bciShowNonMasterRecords.Checked = True

        Else
            bciShowMasterRecords.Checked = True
        End If


        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetEUS_LISTS_Gender")
        ShowWaitWin("Working please wait...")
        CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_Gender(gAdminWSHeaders, Me.DSLists))
        HideWaitWin()
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetEUS_LISTS_Gender")

        ClsCombos.FillComboUsingDatatable(Me.DSLists.EUS_LISTS_Gender, "US", "GenderId", RepositoryItemImageComboBox_GenderId, True)

        ' called automatically when filter checkbox is checked
        '_RefreshGrid()
    End Sub


    Private Sub _RefreshGrid()

        Try
            ShowWaitWin("Working please wait...")

            Dim FocusedRowHandle As Integer = GridViewProfiles.FocusedRowHandle

            'Select Case (Me.WhatProfilesToLoad)
            '    Case ProfileStatusEnum.None
            '        gAdminWS.GetEUS_Profiles(gAdminWSHeaders, Me.DSMembers)
            '    Case ProfileStatusEnum.Approved
            '        gAdminWS.GetEUS_Profiles_ByStatus(gAdminWSHeaders, Me.DSMembers, ProfileStatusEnum.Approved)
            '    Case ProfileStatusEnum.NewProfile
            '        gAdminWS.GetEUS_Profiles_ByStatus(gAdminWSHeaders, Me.DSMembers, ProfileStatusEnum.NewProfile)
            '    Case ProfileStatusEnum.Rejected
            '        gAdminWS.GetEUS_Profiles_ByStatus(gAdminWSHeaders, Me.DSMembers, ProfileStatusEnum.Rejected)
            '    Case ProfileStatusEnum.Updating
            '        gAdminWS.GetEUS_Profiles_ByStatus(gAdminWSHeaders, Me.DSMembers, ProfileStatusEnum.Updating)
            'End Select

            Dim parms As New AdminWS.ProfilesRetrieveParams()
            parms.UseParams = True
            parms.MasterRecords = bciShowMasterRecords.Checked
            parms.NonMasterRecords = bciShowNonMasterRecords.Checked
            parms.Status = Me.WhatProfilesToLoad

            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetEUS_ProfilesWithParams")
            CheckWebServiceCallResult(gAdminWS.GetEUS_ProfilesWithParams(gAdminWSHeaders, Me.DSMembers, parms))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetEUS_ProfilesWithParams ")

            Me.EUS_ProfilesBindingSource.DataSource = Me.DSMembers
            GridViewProfiles.FocusedRowHandle = FocusedRowHandle
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        Finally
            HideWaitWin()
        End Try
    End Sub

    Public Sub RefreshGrid()
        _RefreshGrid()
    End Sub

    Private Sub ADD()
        Try
            Dim FocusedRowHandle As Integer = GridViewProfiles.FocusedRowHandle
            Dim frm As New frmProfileCart
            frm.ADD()
            If frm.IsSaved Then
                _RefreshGrid()
                Me.EUS_ProfilesBindingSource.Position = Me.EUS_ProfilesBindingSource.Find("ProfileID", frm.ProfileID)
            End If
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub EDIT()
        Try
            Dim ProfileID As Integer
            ProfileID = GridViewProfiles.GetFocusedRowCellValue("ProfileID")
            'Dim MirrorProfileID = GridViewProfiles.GetFocusedRowCellValue("MirrorProfileID")
            'Dim IsMaster As Boolean = GridViewProfiles.GetFocusedRowCellValue("IsMaster")
            'If (Not IsMaster) Then
            '    ProfileID = MirrorProfileID
            'End If

            Dim FocusedRowHandle As Integer = GridViewProfiles.FocusedRowHandle
            Dim frm As New frmProfileCart
            frm.EDIT(ProfileID)
            If frm.IsSaved Then
                _RefreshGrid()
                Me.EUS_ProfilesBindingSource.Position = Me.EUS_ProfilesBindingSource.Find("ProfileID", frm.ProfileID)
            End If
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub DELETE()
        Try
            If MsgBox("You are about to DELETE selected record." & vbCrLf & "Continue?", MsgBoxStyle.Exclamation + vbYesNo) = vbYes Then
                Dim ProfileID As Integer
                ProfileID = GridViewProfiles.GetFocusedRowCellValue("ProfileID")
                Dim FocusedRowHandle As Integer = GridViewProfiles.FocusedRowHandle

                Dim deletePermanently As Boolean = False
                If (Me.WhatProfilesToLoad = ProfileStatusEnum.Deleted) Then
                    deletePermanently = True
                End If

                frmProfileCart.DELETE(ProfileID, deletePermanently)
                If frmProfileCart.IsSaved Then
                    _RefreshGrid()
                    GridViewProfiles.FocusedRowHandle = FocusedRowHandle - 1
                End If

            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarDELETE_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarDELETE.ItemClick
        DELETE()
    End Sub

    Private Sub GridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridViewProfiles.KeyDown
        If e.KeyCode = System.Windows.Forms.Keys.Delete Then
            DELETE()
        End If
    End Sub

    Private Sub BarRefresh_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarRefresh.ItemClick
        _RefreshGrid()
    End Sub

    Private Sub GridView1_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GridViewProfiles.DoubleClick
        EDIT()
    End Sub
    Private Sub BarAddNEW_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarAddNEW.ItemClick
        MsgBox("Feature not implemented.")
        'ADD()
    End Sub
    Private Sub BarEDIT_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarEDIT.ItemClick
        EDIT()
    End Sub


    Private Sub GridControl1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GridControlProfiles.Click

    End Sub



    Private Sub GridView1_FocusedRowChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles GridViewProfiles.FocusedRowChanged

        If (GridViewProfiles.FocusedRowHandle > -1) Then
            Me.BarEDIT.Enabled = True
            Me.BarDELETE.Enabled = True
        End If

    End Sub

    Private Sub bciShowMasterRecords_CheckedChanged(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bciShowMasterRecords.CheckedChanged
        _SetFilterCheckListTitle()
        _RefreshGrid()
    End Sub

    Private Sub bciShowNonMasterRecords_CheckedChanged(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bciShowNonMasterRecords.CheckedChanged
        _SetFilterCheckListTitle()
        _RefreshGrid()
    End Sub

    Private Sub _SetFilterCheckListTitle()
        If (bciShowMasterRecords.Checked) AndAlso (bciShowNonMasterRecords.Checked) Then
            BarProfilesFilter.Caption = "Master and Non Master Records"

        ElseIf (bciShowMasterRecords.Checked) AndAlso (Not bciShowNonMasterRecords.Checked) Then
            BarProfilesFilter.Caption = "Master Records"

        ElseIf (Not bciShowMasterRecords.Checked) AndAlso (bciShowNonMasterRecords.Checked) Then
            BarProfilesFilter.Caption = "Non Master Records"

        ElseIf (Not bciShowMasterRecords.Checked) AndAlso (Not bciShowNonMasterRecords.Checked) Then
            BarProfilesFilter.Caption = "[Select record status]"

        End If
    End Sub



End Class
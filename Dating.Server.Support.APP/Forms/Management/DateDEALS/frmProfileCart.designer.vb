﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProfileCart
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmProfileCart))
        Me.lbProfileCart = New DevExpress.XtraEditors.GroupControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.TABS = New DevExpress.XtraTab.XtraTabControl()
        Me.ImageList1 = New System.Windows.Forms.ImageList()
        Me.TABGeneral = New DevExpress.XtraTab.XtraTabPage()
        Me.PictureBoxDefaultPhoto = New System.Windows.Forms.PictureBox()
        Me.gcPoints = New DevExpress.XtraEditors.GroupControl()
        Me.lblPointsVerification = New DevExpress.XtraEditors.LabelControl()
        Me.tbcPointsVerification = New DevExpress.XtraEditors.TrackBarControl()
        Me.EUS_ProfilesBindingSource = New System.Windows.Forms.BindingSource()
        Me.DSMembers = New Dating.Client.Admin.APP.AdminWS.DSMembers()
        Me.lblPointsUnlocksValue = New DevExpress.XtraEditors.LabelControl()
        Me.lblPointsUnlocks = New DevExpress.XtraEditors.LabelControl()
        Me.lblPointsCreditsValue = New DevExpress.XtraEditors.LabelControl()
        Me.lblPointsCredits = New DevExpress.XtraEditors.LabelControl()
        Me.lblPointsBeauty = New DevExpress.XtraEditors.LabelControl()
        Me.tbcPointsBeauty = New DevExpress.XtraEditors.TrackBarControl()
        Me.btnThemeChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLoginNameChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.btnGEOTwoLetterCountryChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.txTheme = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.txGEOTwoLetterCountry = New DevExpress.XtraEditors.TextEdit()
        Me.txCustomReferrer = New DevExpress.XtraEditors.TextEdit()
        Me.txReferrer = New DevExpress.XtraEditors.TextEdit()
        Me.lbGEOTwoLetterCountry = New DevExpress.XtraEditors.LabelControl()
        Me.lbCustomReferrer = New DevExpress.XtraEditors.LabelControl()
        Me.lbReferrer = New DevExpress.XtraEditors.LabelControl()
        Me.txIPLastLogin = New DevExpress.XtraEditors.TextEdit()
        Me.lbIPLastLogin = New DevExpress.XtraEditors.LabelControl()
        Me.dtLastLogin = New DevExpress.XtraEditors.DateEdit()
        Me.lbIPRegistration = New DevExpress.XtraEditors.LabelControl()
        Me.dtRegistration = New DevExpress.XtraEditors.DateEdit()
        Me.txPassword = New DevExpress.XtraEditors.TextEdit()
        Me.txLoginName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl66 = New DevExpress.XtraEditors.LabelControl()
        Me.txIPRegistration = New DevExpress.XtraEditors.TextEdit()
        Me.lbRegisterDateTime = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.lbLoginName = New DevExpress.XtraEditors.LabelControl()
        Me.lbDateTimeLastLogin = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.lbPassword = New DevExpress.XtraEditors.LabelControl()
        Me.TABAccount = New DevExpress.XtraTab.XtraTabPage()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.TabPageAccount = New DevExpress.XtraTab.XtraTabPage()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.btnTWNameChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.txRegion = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.icbAccountTypeId = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.txtTWName = New DevExpress.XtraEditors.TextEdit()
        Me.lbStatus = New DevExpress.XtraEditors.LabelControl()
        Me.lblTWName = New DevExpress.XtraEditors.LabelControl()
        Me.lbPassword2 = New DevExpress.XtraEditors.LabelControl()
        Me.btnFBNameChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.lbMirrorProfileID = New DevExpress.XtraEditors.LabelControl()
        Me.txtFBName = New DevExpress.XtraEditors.TextEdit()
        Me.lbIsMaster = New DevExpress.XtraEditors.LabelControl()
        Me.lblFBName = New DevExpress.XtraEditors.LabelControl()
        Me.lbLoginName2 = New DevExpress.XtraEditors.LabelControl()
        Me.icbGenderId = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.txMirrorProfileID = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.lbFirstName = New DevExpress.XtraEditors.LabelControl()
        Me.btnCityAreaChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.lbLastName = New DevExpress.XtraEditors.LabelControl()
        Me.txCityArea = New DevExpress.XtraEditors.TextEdit()
        Me.txFirstName = New DevExpress.XtraEditors.TextEdit()
        Me.lblCityArea = New DevExpress.XtraEditors.LabelControl()
        Me.txLastName = New DevExpress.XtraEditors.TextEdit()
        Me.btnZipChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.ckIsMaster = New DevExpress.XtraEditors.CheckEdit()
        Me.btnCellularChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.txLoginName2 = New DevExpress.XtraEditors.TextEdit()
        Me.btnEmailChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.txPassword2 = New DevExpress.XtraEditors.TextEdit()
        Me.btnTelephoneChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.lbGender = New DevExpress.XtraEditors.LabelControl()
        Me.btnAddressChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.lbCountry = New DevExpress.XtraEditors.LabelControl()
        Me.btnCityChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.lbRegion = New DevExpress.XtraEditors.LabelControl()
        Me.btnRegionChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.lbZip = New DevExpress.XtraEditors.LabelControl()
        Me.btnCountry2Changed = New DevExpress.XtraEditors.SimpleButton()
        Me.txCountry = New DevExpress.XtraEditors.TextEdit()
        Me.btnLastNameChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.txZip = New DevExpress.XtraEditors.TextEdit()
        Me.btnFirstNameChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.lbCity = New DevExpress.XtraEditors.LabelControl()
        Me.btnLoginName2Changed = New DevExpress.XtraEditors.SimpleButton()
        Me.lbTelephone = New DevExpress.XtraEditors.LabelControl()
        Me.cbStatus = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.lbeMail = New DevExpress.XtraEditors.LabelControl()
        Me.ckAreYouWillingToTravel = New DevExpress.XtraEditors.CheckEdit()
        Me.txCity = New DevExpress.XtraEditors.TextEdit()
        Me.txCellular = New DevExpress.XtraEditors.TextEdit()
        Me.txTelephone = New DevExpress.XtraEditors.TextEdit()
        Me.lbAreYouWillingToTravel = New DevExpress.XtraEditors.LabelControl()
        Me.txeMail = New DevExpress.XtraEditors.TextEdit()
        Me.lbCellular = New DevExpress.XtraEditors.LabelControl()
        Me.lbAddress = New DevExpress.XtraEditors.LabelControl()
        Me.txAddress = New DevExpress.XtraEditors.TextEdit()
        Me.TabPageAboutMe = New DevExpress.XtraTab.XtraTabPage()
        Me.MemoEdit3 = New DevExpress.XtraEditors.MemoEdit()
        Me.MemoEdit2 = New DevExpress.XtraEditors.MemoEdit()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.btnOtherDetails_OccupationChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAboutMe_HeadingChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.icbNetWorth = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.icbAnnualIncome = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.txOccupation = New DevExpress.XtraEditors.TextEdit()
        Me.btnAboutMe_DescribeYourselfChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.lbOccupation = New DevExpress.XtraEditors.LabelControl()
        Me.lbNetWorthID = New DevExpress.XtraEditors.LabelControl()
        Me.btnAboutMe_DescribeAnIdealFirstDateChanged = New DevExpress.XtraEditors.SimpleButton()
        Me.lbAnnualIncomeID = New DevExpress.XtraEditors.LabelControl()
        Me.lbDescribeYourself = New DevExpress.XtraEditors.LabelControl()
        Me.lbEducationID = New DevExpress.XtraEditors.LabelControl()
        Me.lbHeading = New DevExpress.XtraEditors.LabelControl()
        Me.icbEducation = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.lbDescribeAnIdealFirstDate = New DevExpress.XtraEditors.LabelControl()
        Me.TabPagePersonal = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.icbDrinkingHabit = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.icbEthnicity = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.icbSmokingHabit = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.lbBodyTypeID = New DevExpress.XtraEditors.LabelControl()
        Me.icbReligion = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.lbHairColorID = New DevExpress.XtraEditors.LabelControl()
        Me.lbHeightID = New DevExpress.XtraEditors.LabelControl()
        Me.icbChildrenID = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.lbEyeColorID = New DevExpress.XtraEditors.LabelControl()
        Me.icbHairColorID = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.lbChildrenID = New DevExpress.XtraEditors.LabelControl()
        Me.icbEyeColorID = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.icbBodyTypeID = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.icbHeightID = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TabPageLookingFor = New DevExpress.XtraTab.XtraTabPage()
        Me.icbRelationshipStatusID = New DevExpress.XtraEditors.ImageComboBoxEdit()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.chkMarriedDating = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.chkAdultDating_Casual = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.chkMutuallyBeneficialArrangements = New DevExpress.XtraEditors.CheckEdit()
        Me.chkToMeetMaleID = New DevExpress.XtraEditors.CheckEdit()
        Me.chkLongTermRelationship = New DevExpress.XtraEditors.CheckEdit()
        Me.chkToMeetFemaleID = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.chkFriendship = New DevExpress.XtraEditors.CheckEdit()
        Me.chkShortTermRelationship = New DevExpress.XtraEditors.CheckEdit()
        Me.TabPageSettings = New DevExpress.XtraTab.XtraTabPage()
        Me.chkNotificationsSettings_WhenNewMembersNearMe = New DevExpress.XtraEditors.CheckEdit()
        Me.lblNotificationsSettings_WhenNewMembersNearMe = New DevExpress.XtraEditors.LabelControl()
        Me.chkNotificationsSettings_WhenNewOfferReceived = New DevExpress.XtraEditors.CheckEdit()
        Me.lblNotificationsSettings_WhenNewOfferReceived = New DevExpress.XtraEditors.LabelControl()
        Me.chkNotificationsSettings_WhenLikeReceived = New DevExpress.XtraEditors.CheckEdit()
        Me.lblNotificationsSettings_WhenLikeReceived = New DevExpress.XtraEditors.LabelControl()
        Me.chkNotificationsSettings_WhenNewMessageReceived = New DevExpress.XtraEditors.CheckEdit()
        Me.lblNotificationsSettings_WhenNewMessageReceived = New DevExpress.XtraEditors.LabelControl()
        Me.chkHideMyLastLoginDateTime = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.chkNotShowMyGifts = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.chkNotShowInOtherUsersViewedList = New DevExpress.XtraEditors.CheckEdit()
        Me.chkHideMeFromSearchResults = New DevExpress.XtraEditors.CheckEdit()
        Me.chkNotShowInOtherUsersFavoritedList = New DevExpress.XtraEditors.CheckEdit()
        Me.chkHideMeFromMembersIHaveBlocked = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.TABPhotosCarousel = New DevExpress.XtraTab.XtraTabPage()
        Me.ucLayout3D1 = New Dating.Client.Admin.APP.ucLayout3D()
        Me.TABLogins = New DevExpress.XtraTab.XtraTabPage()
        Me.gcLoginHistory = New DevExpress.XtraEditors.GroupControl()
        Me.UcGridLoginHistory = New Dating.Client.Admin.APP.ucGrid()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TABCredits = New DevExpress.XtraTab.XtraTabPage()
        Me.gcCreditsHistory = New DevExpress.XtraEditors.GroupControl()
        Me.UcGridCreditsHistory = New Dating.Client.Admin.APP.ucGrid()
        Me.txtAllCreditsExpDate = New DevExpress.XtraEditors.TextEdit()
        Me.CustomerAvailableCreditsBindingSource = New System.Windows.Forms.BindingSource()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.txtAvailableCredits = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.TABTransactions = New DevExpress.XtraTab.XtraTabPage()
        Me.ucGridTransactions = New Dating.Client.Admin.APP.ucGrid()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.TabPageLikes = New DevExpress.XtraTab.XtraTabPage()
        Me.UcLikes1 = New Dating.Client.Admin.APP.UCLikes()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TabPageMessages = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.UcMessages1 = New Dating.Client.Admin.APP.UCMessages()
        Me.TabPageOffers = New DevExpress.XtraTab.XtraTabPage()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.UcOffers1 = New Dating.Client.Admin.APP.UCOffers()
        Me.pcMainCommands = New DevExpress.XtraEditors.PanelControl()
        Me.cmdCancel1 = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdSave1 = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdReject = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdApprove = New DevExpress.XtraEditors.SimpleButton()
        Me.EUS_CustomerPhotosBindingSource = New System.Windows.Forms.BindingSource()
        Me.DSLists = New Dating.Client.Admin.APP.AdminWS.DSLists()
        Me.EUS_LISTS_EducationBindingSource = New System.Windows.Forms.BindingSource()
        Me.EUS_LISTS_IncomeBindingSource = New System.Windows.Forms.BindingSource()
        Me.EUS_LISTS_NetWorthBindingSource = New System.Windows.Forms.BindingSource()
        Me.EUS_CustomerTransactionBindingSource = New System.Windows.Forms.BindingSource()
        Me.CustomerCreditsHistoryBindingSource = New System.Windows.Forms.BindingSource()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager()
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItemSendEmail = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemLoginToProfile = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.CustomerCreditsHistoryAdminBindingSource = New System.Windows.Forms.BindingSource()
        CType(Me.lbProfileCart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.lbProfileCart.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.TABS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TABS.SuspendLayout()
        Me.TABGeneral.SuspendLayout()
        CType(Me.PictureBoxDefaultPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcPoints, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcPoints.SuspendLayout()
        CType(Me.tbcPointsVerification, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcPointsVerification.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_ProfilesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSMembers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcPointsBeauty, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbcPointsBeauty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txTheme.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txGEOTwoLetterCountry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCustomReferrer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txReferrer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txIPLastLogin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtLastLogin.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtLastLogin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtRegistration.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtRegistration.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txLoginName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txIPRegistration.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TABAccount.SuspendLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.TabPageAccount.SuspendLayout()
        CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txRegion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbAccountTypeId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTWName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtFBName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbGenderId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txMirrorProfileID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCityArea.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txFirstName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txLastName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckIsMaster.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txLoginName2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txPassword2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCountry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txZip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckAreYouWillingToTravel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txCellular.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txTelephone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txeMail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageAboutMe.SuspendLayout()
        CType(Me.MemoEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbNetWorth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbAnnualIncome.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txOccupation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbEducation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPagePersonal.SuspendLayout()
        CType(Me.icbDrinkingHabit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbEthnicity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbSmokingHabit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbReligion.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbChildrenID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbHairColorID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbEyeColorID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbBodyTypeID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.icbHeightID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageLookingFor.SuspendLayout()
        CType(Me.icbRelationshipStatusID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMarriedDating.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAdultDating_Casual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkMutuallyBeneficialArrangements.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkToMeetMaleID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkLongTermRelationship.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkToMeetFemaleID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkFriendship.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkShortTermRelationship.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageSettings.SuspendLayout()
        CType(Me.chkNotificationsSettings_WhenNewMembersNearMe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNotificationsSettings_WhenNewOfferReceived.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNotificationsSettings_WhenLikeReceived.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNotificationsSettings_WhenNewMessageReceived.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkHideMyLastLoginDateTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNotShowMyGifts.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNotShowInOtherUsersViewedList.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkHideMeFromSearchResults.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkNotShowInOtherUsersFavoritedList.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkHideMeFromMembersIHaveBlocked.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TABPhotosCarousel.SuspendLayout()
        Me.TABLogins.SuspendLayout()
        CType(Me.gcLoginHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcLoginHistory.SuspendLayout()
        Me.TABCredits.SuspendLayout()
        CType(Me.gcCreditsHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcCreditsHistory.SuspendLayout()
        CType(Me.txtAllCreditsExpDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerAvailableCreditsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtAvailableCredits.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TABTransactions.SuspendLayout()
        Me.TabPageLikes.SuspendLayout()
        Me.TabPageMessages.SuspendLayout()
        Me.TabPageOffers.SuspendLayout()
        CType(Me.pcMainCommands, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pcMainCommands.SuspendLayout()
        CType(Me.EUS_CustomerPhotosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_LISTS_EducationBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_LISTS_IncomeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_LISTS_NetWorthBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_CustomerTransactionBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerCreditsHistoryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CustomerCreditsHistoryAdminBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbProfileCart
        '
        Me.lbProfileCart.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lbProfileCart.AppearanceCaption.Options.UseFont = True
        Me.lbProfileCart.Controls.Add(Me.PanelControl1)
        Me.lbProfileCart.Controls.Add(Me.pcMainCommands)
        Me.lbProfileCart.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbProfileCart.Location = New System.Drawing.Point(0, 39)
        Me.lbProfileCart.Name = "lbProfileCart"
        Me.lbProfileCart.Size = New System.Drawing.Size(1063, 651)
        Me.lbProfileCart.TabIndex = 233
        Me.lbProfileCart.Text = "myusername, Age:24, Athens, Greece"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.TABS)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(2, 33)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1059, 571)
        Me.PanelControl1.TabIndex = 251
        '
        'TABS
        '
        Me.TABS.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TABS.Images = Me.ImageList1
        Me.TABS.Location = New System.Drawing.Point(2, 2)
        Me.TABS.Name = "TABS"
        Me.TABS.SelectedTabPage = Me.TABGeneral
        Me.TABS.Size = New System.Drawing.Size(1055, 567)
        Me.TABS.TabIndex = 240
        Me.TABS.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.TABGeneral, Me.TABAccount, Me.TABPhotosCarousel, Me.TABLogins, Me.TABCredits, Me.TABTransactions, Me.TabPageLikes, Me.TabPageMessages, Me.TabPageOffers})
        Me.TABS.Tag = ""
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "About.ico")
        Me.ImageList1.Images.SetKeyName(1, "Accounts.ico")
        Me.ImageList1.Images.SetKeyName(2, "Address Book.ico")
        Me.ImageList1.Images.SetKeyName(3, "Air Brush.ico")
        Me.ImageList1.Images.SetKeyName(4, "Align-Centre-2.ico")
        Me.ImageList1.Images.SetKeyName(5, "Align-Centre.ico")
        Me.ImageList1.Images.SetKeyName(6, "Align-Full-2.ico")
        Me.ImageList1.Images.SetKeyName(7, "Align-Full.ico")
        Me.ImageList1.Images.SetKeyName(8, "Align-Left-2.ico")
        Me.ImageList1.Images.SetKeyName(9, "Align-Left.ico")
        Me.ImageList1.Images.SetKeyName(10, "Align-Right-2.ico")
        Me.ImageList1.Images.SetKeyName(11, "Align-Right.ico")
        Me.ImageList1.Images.SetKeyName(12, "Arrow-Left Right.ico")
        Me.ImageList1.Images.SetKeyName(13, "Arrow-Up Down.ico")
        Me.ImageList1.Images.SetKeyName(14, "Attachment.ico")
        Me.ImageList1.Images.SetKeyName(15, "Auto Correct-Options.ico")
        Me.ImageList1.Images.SetKeyName(16, "Auto-Type.ico")
        Me.ImageList1.Images.SetKeyName(17, "Back.ico")
        Me.ImageList1.Images.SetKeyName(18, "Bar Code.ico")
        Me.ImageList1.Images.SetKeyName(19, "Bold.ico")
        Me.ImageList1.Images.SetKeyName(20, "Book-Open.ico")
        Me.ImageList1.Images.SetKeyName(21, "Books.ico")
        Me.ImageList1.Images.SetKeyName(22, "Book-Search.ico")
        Me.ImageList1.Images.SetKeyName(23, "Borders and Shading.ico")
        Me.ImageList1.Images.SetKeyName(24, "Box-Closed-2.ico")
        Me.ImageList1.Images.SetKeyName(25, "Box-Closed.ico")
        Me.ImageList1.Images.SetKeyName(26, "Box-Open-2.ico")
        Me.ImageList1.Images.SetKeyName(27, "Box-Open.ico")
        Me.ImageList1.Images.SetKeyName(28, "Bullets-2.ico")
        Me.ImageList1.Images.SetKeyName(29, "Bullets.ico")
        Me.ImageList1.Images.SetKeyName(30, "Calculator.ico")
        Me.ImageList1.Images.SetKeyName(31, "Calendar.ico")
        Me.ImageList1.Images.SetKeyName(32, "Calendar-Go To Date.ico")
        Me.ImageList1.Images.SetKeyName(33, "Calendar-Search.ico")
        Me.ImageList1.Images.SetKeyName(34, "Calendar-Select Day.ico")
        Me.ImageList1.Images.SetKeyName(35, "Calendar-Select Month.ico")
        Me.ImageList1.Images.SetKeyName(36, "Calendar-Select Week.ico")
        Me.ImageList1.Images.SetKeyName(37, "Calendar-Select Work Week.ico")
        Me.ImageList1.Images.SetKeyName(38, "Camera.ico")
        Me.ImageList1.Images.SetKeyName(39, "Camera-Photo.ico")
        Me.ImageList1.Images.SetKeyName(40, "Card File.ico")
        Me.ImageList1.Images.SetKeyName(41, "Card.ico")
        Me.ImageList1.Images.SetKeyName(42, "Card-Add.ico")
        Me.ImageList1.Images.SetKeyName(43, "Card-Delete.ico")
        Me.ImageList1.Images.SetKeyName(44, "Card-Edit.ico")
        Me.ImageList1.Images.SetKeyName(45, "Card-New.ico")
        Me.ImageList1.Images.SetKeyName(46, "Card-Next.ico")
        Me.ImageList1.Images.SetKeyName(47, "Card-Previous.ico")
        Me.ImageList1.Images.SetKeyName(48, "Card-Remove.ico")
        Me.ImageList1.Images.SetKeyName(49, "Card-Search.ico")
        Me.ImageList1.Images.SetKeyName(50, "CD-Burn.ico")
        Me.ImageList1.Images.SetKeyName(51, "CD-In Case.ico")
        Me.ImageList1.Images.SetKeyName(52, "CD-ROM.ico")
        Me.ImageList1.Images.SetKeyName(53, "Center Across Cells.ico")
        Me.ImageList1.Images.SetKeyName(54, "Certificate.ico")
        Me.ImageList1.Images.SetKeyName(55, "Chart.ico")
        Me.ImageList1.Images.SetKeyName(56, "Chart-Bar.ico")
        Me.ImageList1.Images.SetKeyName(57, "Chart-Line.ico")
        Me.ImageList1.Images.SetKeyName(58, "Chart-Pie.ico")
        Me.ImageList1.Images.SetKeyName(59, "Chart-Point.ico")
        Me.ImageList1.Images.SetKeyName(60, "Clean.ico")
        Me.ImageList1.Images.SetKeyName(61, "Clear.ico")
        Me.ImageList1.Images.SetKeyName(62, "Clipboard.ico")
        Me.ImageList1.Images.SetKeyName(63, "Clock.ico")
        Me.ImageList1.Images.SetKeyName(64, "Close.ico")
        Me.ImageList1.Images.SetKeyName(65, "Colour Picker.ico")
        Me.ImageList1.Images.SetKeyName(66, "Columns-2.ico")
        Me.ImageList1.Images.SetKeyName(67, "Columns.ico")
        Me.ImageList1.Images.SetKeyName(68, "Command Prompt.ico")
        Me.ImageList1.Images.SetKeyName(69, "Compress.ico")
        Me.ImageList1.Images.SetKeyName(70, "Computer.ico")
        Me.ImageList1.Images.SetKeyName(71, "Connect.ico")
        Me.ImageList1.Images.SetKeyName(72, "Contact.ico")
        Me.ImageList1.Images.SetKeyName(73, "Contact-Add.ico")
        Me.ImageList1.Images.SetKeyName(74, "Contact-Delete.ico")
        Me.ImageList1.Images.SetKeyName(75, "Contact-Edit.ico")
        Me.ImageList1.Images.SetKeyName(76, "Contact-New.ico")
        Me.ImageList1.Images.SetKeyName(77, "Contact-Remove.ico")
        Me.ImageList1.Images.SetKeyName(78, "Contact-Search.ico")
        Me.ImageList1.Images.SetKeyName(79, "Copy.ico")
        Me.ImageList1.Images.SetKeyName(80, "Cross.ico")
        Me.ImageList1.Images.SetKeyName(81, "Currency.ico")
        Me.ImageList1.Images.SetKeyName(82, "Currency-Notes.ico")
        Me.ImageList1.Images.SetKeyName(83, "Cut.ico")
        Me.ImageList1.Images.SetKeyName(84, "Database.ico")
        Me.ImageList1.Images.SetKeyName(85, "Database-Add.ico")
        Me.ImageList1.Images.SetKeyName(86, "Database-Close.ico")
        Me.ImageList1.Images.SetKeyName(87, "Database-Delete.ico")
        Me.ImageList1.Images.SetKeyName(88, "Database-Edit.ico")
        Me.ImageList1.Images.SetKeyName(89, "Database-Filter.ico")
        Me.ImageList1.Images.SetKeyName(90, "Database-New.ico")
        Me.ImageList1.Images.SetKeyName(91, "Database-Open.ico")
        Me.ImageList1.Images.SetKeyName(92, "Database-Properties.ico")
        Me.ImageList1.Images.SetKeyName(93, "Database-Remove.ico")
        Me.ImageList1.Images.SetKeyName(94, "Database-Save.ico")
        Me.ImageList1.Images.SetKeyName(95, "Database-Schema.ico")
        Me.ImageList1.Images.SetKeyName(96, "Database-Search.ico")
        Me.ImageList1.Images.SetKeyName(97, "Database-Security.ico")
        Me.ImageList1.Images.SetKeyName(98, "Database-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(99, "Date-Time.ico")
        Me.ImageList1.Images.SetKeyName(100, "db-Add.ico")
        Me.ImageList1.Images.SetKeyName(101, "db-Cancel.ico")
        Me.ImageList1.Images.SetKeyName(102, "db-Delete.ico")
        Me.ImageList1.Images.SetKeyName(103, "db-Edit.ico")
        Me.ImageList1.Images.SetKeyName(104, "db-First.ico")
        Me.ImageList1.Images.SetKeyName(105, "db-Last.ico")
        Me.ImageList1.Images.SetKeyName(106, "db-Next.ico")
        Me.ImageList1.Images.SetKeyName(107, "db-Post.ico")
        Me.ImageList1.Images.SetKeyName(108, "db-Previous.ico")
        Me.ImageList1.Images.SetKeyName(109, "db-Refresh.ico")
        Me.ImageList1.Images.SetKeyName(110, "Decimals-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(111, "Decimals-Increase.ico")
        Me.ImageList1.Images.SetKeyName(112, "Decompress.ico")
        Me.ImageList1.Images.SetKeyName(113, "Delete.ico")
        Me.ImageList1.Images.SetKeyName(114, "Delete-Blue.ico")
        Me.ImageList1.Images.SetKeyName(115, "Design.ico")
        Me.ImageList1.Images.SetKeyName(116, "Desktop.ico")
        Me.ImageList1.Images.SetKeyName(117, "Dictionary.ico")
        Me.ImageList1.Images.SetKeyName(118, "Disconnect.ico")
        Me.ImageList1.Images.SetKeyName(119, "Discuss.ico")
        Me.ImageList1.Images.SetKeyName(120, "Doc-Access.ico")
        Me.ImageList1.Images.SetKeyName(121, "Doc-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(122, "Doc-Excel.ico")
        Me.ImageList1.Images.SetKeyName(123, "Doc-HTML.ico")
        Me.ImageList1.Images.SetKeyName(124, "Doc-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(125, "Doc-RTF.ico")
        Me.ImageList1.Images.SetKeyName(126, "Document-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(127, "Document-Protect.ico")
        Me.ImageList1.Images.SetKeyName(128, "Doc-Word.ico")
        Me.ImageList1.Images.SetKeyName(129, "Doc-XML.ico")
        Me.ImageList1.Images.SetKeyName(130, "Drawing.ico")
        Me.ImageList1.Images.SetKeyName(131, "Edit.ico")
        Me.ImageList1.Images.SetKeyName(132, "Equaliser.ico")
        Me.ImageList1.Images.SetKeyName(133, "Execute.ico")
        Me.ImageList1.Images.SetKeyName(134, "Exit.ico")
        Me.ImageList1.Images.SetKeyName(135, "Export.ico")
        Me.ImageList1.Images.SetKeyName(136, "Export-Access.ico")
        Me.ImageList1.Images.SetKeyName(137, "Export-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(138, "Export-CSV.ico")
        Me.ImageList1.Images.SetKeyName(139, "Export-Excel.ico")
        Me.ImageList1.Images.SetKeyName(140, "Export-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(141, "Export-HTML.ico")
        Me.ImageList1.Images.SetKeyName(142, "Export-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(143, "Export-RTF.ico")
        Me.ImageList1.Images.SetKeyName(144, "Export-Text.ico")
        Me.ImageList1.Images.SetKeyName(145, "Export-Word.ico")
        Me.ImageList1.Images.SetKeyName(146, "Export-XML.ico")
        Me.ImageList1.Images.SetKeyName(147, "Favorites.ico")
        Me.ImageList1.Images.SetKeyName(148, "Fill.ico")
        Me.ImageList1.Images.SetKeyName(149, "Fill-Down.ico")
        Me.ImageList1.Images.SetKeyName(150, "Fill-Left.ico")
        Me.ImageList1.Images.SetKeyName(151, "Fill-Right.ico")
        Me.ImageList1.Images.SetKeyName(152, "Fill-Up.ico")
        Me.ImageList1.Images.SetKeyName(153, "Filter.ico")
        Me.ImageList1.Images.SetKeyName(154, "Flag.ico")
        Me.ImageList1.Images.SetKeyName(155, "Flag-Delete.ico")
        Me.ImageList1.Images.SetKeyName(156, "Flag-Next.ico")
        Me.ImageList1.Images.SetKeyName(157, "Flag-Previous.ico")
        Me.ImageList1.Images.SetKeyName(158, "Flow Chart.ico")
        Me.ImageList1.Images.SetKeyName(159, "Folder List.ico")
        Me.ImageList1.Images.SetKeyName(160, "Folder.ico")
        Me.ImageList1.Images.SetKeyName(161, "Folder-Closed.ico")
        Me.ImageList1.Images.SetKeyName(162, "Folder-Delete.ico")
        Me.ImageList1.Images.SetKeyName(163, "Folder-New.ico")
        Me.ImageList1.Images.SetKeyName(164, "Folder-Options.ico")
        Me.ImageList1.Images.SetKeyName(165, "Folder-Search.ico")
        Me.ImageList1.Images.SetKeyName(166, "Footer.ico")
        Me.ImageList1.Images.SetKeyName(167, "Format Painter.ico")
        Me.ImageList1.Images.SetKeyName(168, "Format-Font Size.ico")
        Me.ImageList1.Images.SetKeyName(169, "Format-Font.ico")
        Me.ImageList1.Images.SetKeyName(170, "Format-Paragraph.ico")
        Me.ImageList1.Images.SetKeyName(171, "Format-Percentage.ico")
        Me.ImageList1.Images.SetKeyName(172, "Formatting-Remove.ico")
        Me.ImageList1.Images.SetKeyName(173, "Forward.ico")
        Me.ImageList1.Images.SetKeyName(174, "Full Screen.ico")
        Me.ImageList1.Images.SetKeyName(175, "Full Screen-New.ico")
        Me.ImageList1.Images.SetKeyName(176, "Function.ico")
        Me.ImageList1.Images.SetKeyName(177, "Go To Line.ico")
        Me.ImageList1.Images.SetKeyName(178, "Grid-2.ico")
        Me.ImageList1.Images.SetKeyName(179, "Grid.ico")
        Me.ImageList1.Images.SetKeyName(180, "Grid-Auto Format-2.ico")
        Me.ImageList1.Images.SetKeyName(181, "Grid-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(182, "Grid-Column Width.ico")
        Me.ImageList1.Images.SetKeyName(183, "Grid-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(184, "Grid-Delete Cell.ico")
        Me.ImageList1.Images.SetKeyName(185, "Grid-Delete Column.ico")
        Me.ImageList1.Images.SetKeyName(186, "Grid-Delete Row.ico")
        Me.ImageList1.Images.SetKeyName(187, "Grid-Delete.ico")
        Me.ImageList1.Images.SetKeyName(188, "Grid-Insert Cell.ico")
        Me.ImageList1.Images.SetKeyName(189, "Grid-Insert Column Left.ico")
        Me.ImageList1.Images.SetKeyName(190, "Grid-Insert Column Right.ico")
        Me.ImageList1.Images.SetKeyName(191, "Grid-Insert Column.ico")
        Me.ImageList1.Images.SetKeyName(192, "Grid-Insert Row Above.ico")
        Me.ImageList1.Images.SetKeyName(193, "Grid-Insert Row Below.ico")
        Me.ImageList1.Images.SetKeyName(194, "Grid-Insert Row.ico")
        Me.ImageList1.Images.SetKeyName(195, "Grid-Merge Cells.ico")
        Me.ImageList1.Images.SetKeyName(196, "Grid-New-2.ico")
        Me.ImageList1.Images.SetKeyName(197, "Grid-New.ico")
        Me.ImageList1.Images.SetKeyName(198, "Grid-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(199, "Grid-Options.ico")
        Me.ImageList1.Images.SetKeyName(200, "Grid-Row Height.ico")
        Me.ImageList1.Images.SetKeyName(201, "Grid-Select All-2.ico")
        Me.ImageList1.Images.SetKeyName(202, "Grid-Select All.ico")
        Me.ImageList1.Images.SetKeyName(203, "Grid-Select Cell-2.ico")
        Me.ImageList1.Images.SetKeyName(204, "Grid-Select Cell.ico")
        Me.ImageList1.Images.SetKeyName(205, "Grid-Select Column-2.ico")
        Me.ImageList1.Images.SetKeyName(206, "Grid-Select Column.ico")
        Me.ImageList1.Images.SetKeyName(207, "Grid-Select Row-2.ico")
        Me.ImageList1.Images.SetKeyName(208, "Grid-Select Row.ico")
        Me.ImageList1.Images.SetKeyName(209, "Grid-Split Cells.ico")
        Me.ImageList1.Images.SetKeyName(210, "Grid-Wizard-2.ico")
        Me.ImageList1.Images.SetKeyName(211, "Grid-Wizard.ico")
        Me.ImageList1.Images.SetKeyName(212, "Hand Held.ico")
        Me.ImageList1.Images.SetKeyName(213, "Hand Shake.ico")
        Me.ImageList1.Images.SetKeyName(214, "Header and Footer.ico")
        Me.ImageList1.Images.SetKeyName(215, "Header and Footer-Toggle.ico")
        Me.ImageList1.Images.SetKeyName(216, "Header.ico")
        Me.ImageList1.Images.SetKeyName(217, "Header-Next.ico")
        Me.ImageList1.Images.SetKeyName(218, "Header-Previous.ico")
        Me.ImageList1.Images.SetKeyName(219, "Help.ico")
        Me.ImageList1.Images.SetKeyName(220, "Highlighter.ico")
        Me.ImageList1.Images.SetKeyName(221, "History.ico")
        Me.ImageList1.Images.SetKeyName(222, "Home.ico")
        Me.ImageList1.Images.SetKeyName(223, "Image.ico")
        Me.ImageList1.Images.SetKeyName(224, "Image-Brightness.ico")
        Me.ImageList1.Images.SetKeyName(225, "Image-Colour.ico")
        Me.ImageList1.Images.SetKeyName(226, "Image-Contrast.ico")
        Me.ImageList1.Images.SetKeyName(227, "Image-Crop.ico")
        Me.ImageList1.Images.SetKeyName(228, "Image-Flip.ico")
        Me.ImageList1.Images.SetKeyName(229, "Image-Mirror.ico")
        Me.ImageList1.Images.SetKeyName(230, "Image-Paint.ico")
        Me.ImageList1.Images.SetKeyName(231, "Image-Resize.ico")
        Me.ImageList1.Images.SetKeyName(232, "Image-Rotate.ico")
        Me.ImageList1.Images.SetKeyName(233, "Image-Select.ico")
        Me.ImageList1.Images.SetKeyName(234, "Import.ico")
        Me.ImageList1.Images.SetKeyName(235, "Import-Access.ico")
        Me.ImageList1.Images.SetKeyName(236, "Import-Acrobat.ico")
        Me.ImageList1.Images.SetKeyName(237, "Import-CSV.ico")
        Me.ImageList1.Images.SetKeyName(238, "Import-Excel.ico")
        Me.ImageList1.Images.SetKeyName(239, "Import-Fixed Length.ico")
        Me.ImageList1.Images.SetKeyName(240, "Import-HTML.ico")
        Me.ImageList1.Images.SetKeyName(241, "Import-Outlook.ico")
        Me.ImageList1.Images.SetKeyName(242, "Import-RTF.ico")
        Me.ImageList1.Images.SetKeyName(243, "Import-Text.ico")
        Me.ImageList1.Images.SetKeyName(244, "Import-Word.ico")
        Me.ImageList1.Images.SetKeyName(245, "Import-XML.ico")
        Me.ImageList1.Images.SetKeyName(246, "Indent-Decrease-2.ico")
        Me.ImageList1.Images.SetKeyName(247, "Indent-Decrease.ico")
        Me.ImageList1.Images.SetKeyName(248, "Indent-Increase-2.ico")
        Me.ImageList1.Images.SetKeyName(249, "Indent-Increase.ico")
        Me.ImageList1.Images.SetKeyName(250, "Insert.ico")
        Me.ImageList1.Images.SetKeyName(251, "Insert-File.ico")
        Me.ImageList1.Images.SetKeyName(252, "Insert-Image.ico")
        Me.ImageList1.Images.SetKeyName(253, "Insert-Object.ico")
        Me.ImageList1.Images.SetKeyName(254, "Invoice.ico")
        Me.ImageList1.Images.SetKeyName(255, "Italic.ico")
        Me.ImageList1.Images.SetKeyName(256, "Key.ico")
        Me.ImageList1.Images.SetKeyName(257, "Landscape.ico")
        Me.ImageList1.Images.SetKeyName(258, "Launch.ico")
        Me.ImageList1.Images.SetKeyName(259, "Letter.ico")
        Me.ImageList1.Images.SetKeyName(260, "Line Spacing-2.ico")
        Me.ImageList1.Images.SetKeyName(261, "Line Spacing.ico")
        Me.ImageList1.Images.SetKeyName(262, "Link.ico")
        Me.ImageList1.Images.SetKeyName(263, "Lock.ico")
        Me.ImageList1.Images.SetKeyName(264, "Mail.ico")
        Me.ImageList1.Images.SetKeyName(265, "Mail-Forward.ico")
        Me.ImageList1.Images.SetKeyName(266, "Mail-In Box-2.ico")
        Me.ImageList1.Images.SetKeyName(267, "Mail-In Box.ico")
        Me.ImageList1.Images.SetKeyName(268, "Mail-In-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(269, "Mail-New.ico")
        Me.ImageList1.Images.SetKeyName(270, "Mail-Out Box-2.ico")
        Me.ImageList1.Images.SetKeyName(271, "Mail-Out Box.ico")
        Me.ImageList1.Images.SetKeyName(272, "Mail-Reply To All.ico")
        Me.ImageList1.Images.SetKeyName(273, "Mail-Reply.ico")
        Me.ImageList1.Images.SetKeyName(274, "Mail-Search.ico")
        Me.ImageList1.Images.SetKeyName(275, "Mail-Send and Receive.ico")
        Me.ImageList1.Images.SetKeyName(276, "Media.ico")
        Me.ImageList1.Images.SetKeyName(277, "Microphone.ico")
        Me.ImageList1.Images.SetKeyName(278, "Midi Keyboard.ico")
        Me.ImageList1.Images.SetKeyName(279, "Minus.ico")
        Me.ImageList1.Images.SetKeyName(280, "mm-Eject.ico")
        Me.ImageList1.Images.SetKeyName(281, "mm-Fast Forward.ico")
        Me.ImageList1.Images.SetKeyName(282, "mm-First.ico")
        Me.ImageList1.Images.SetKeyName(283, "mm-Last.ico")
        Me.ImageList1.Images.SetKeyName(284, "mm-Pause.ico")
        Me.ImageList1.Images.SetKeyName(285, "mm-Play.ico")
        Me.ImageList1.Images.SetKeyName(286, "mm-Rewind.ico")
        Me.ImageList1.Images.SetKeyName(287, "mm-Stop.ico")
        Me.ImageList1.Images.SetKeyName(288, "Monitor.ico")
        Me.ImageList1.Images.SetKeyName(289, "Movie.ico")
        Me.ImageList1.Images.SetKeyName(290, "Musical Note-2.ico")
        Me.ImageList1.Images.SetKeyName(291, "Musical Note.ico")
        Me.ImageList1.Images.SetKeyName(292, "New.ico")
        Me.ImageList1.Images.SetKeyName(293, "Note.ico")
        Me.ImageList1.Images.SetKeyName(294, "Note-Add.ico")
        Me.ImageList1.Images.SetKeyName(295, "Note-Delete.ico")
        Me.ImageList1.Images.SetKeyName(296, "Note-Edit.ico")
        Me.ImageList1.Images.SetKeyName(297, "Note-New.ico")
        Me.ImageList1.Images.SetKeyName(298, "Note-Next.ico")
        Me.ImageList1.Images.SetKeyName(299, "Notepad.ico")
        Me.ImageList1.Images.SetKeyName(300, "Note-Previous.ico")
        Me.ImageList1.Images.SetKeyName(301, "Note-Remove.ico")
        Me.ImageList1.Images.SetKeyName(302, "Notes.ico")
        Me.ImageList1.Images.SetKeyName(303, "Note-Search.ico")
        Me.ImageList1.Images.SetKeyName(304, "Number of Pages.ico")
        Me.ImageList1.Images.SetKeyName(305, "Numbering-2.ico")
        Me.ImageList1.Images.SetKeyName(306, "Numbering.ico")
        Me.ImageList1.Images.SetKeyName(307, "Object-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(308, "Object-Select.ico")
        Me.ImageList1.Images.SetKeyName(309, "Object-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(310, "Open.ico")
        Me.ImageList1.Images.SetKeyName(311, "Outline-2.ico")
        Me.ImageList1.Images.SetKeyName(312, "Outline.ico")
        Me.ImageList1.Images.SetKeyName(313, "Page Number.ico")
        Me.ImageList1.Images.SetKeyName(314, "Page-Break.ico")
        Me.ImageList1.Images.SetKeyName(315, "Page-Invert.ico")
        Me.ImageList1.Images.SetKeyName(316, "Page-Next.ico")
        Me.ImageList1.Images.SetKeyName(317, "Page-Previous.ico")
        Me.ImageList1.Images.SetKeyName(318, "Page-Setup.ico")
        Me.ImageList1.Images.SetKeyName(319, "Paintbrush.ico")
        Me.ImageList1.Images.SetKeyName(320, "Parcel.ico")
        Me.ImageList1.Images.SetKeyName(321, "Paste.ico")
        Me.ImageList1.Images.SetKeyName(322, "Permission.ico")
        Me.ImageList1.Images.SetKeyName(323, "Phone.ico")
        Me.ImageList1.Images.SetKeyName(324, "Planner.ico")
        Me.ImageList1.Images.SetKeyName(325, "Plus.ico")
        Me.ImageList1.Images.SetKeyName(326, "Portrait.ico")
        Me.ImageList1.Images.SetKeyName(327, "Preview.ico")
        Me.ImageList1.Images.SetKeyName(328, "Print-2.ico")
        Me.ImageList1.Images.SetKeyName(329, "Print.ico")
        Me.ImageList1.Images.SetKeyName(330, "Print-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(331, "Print-Options.ico")
        Me.ImageList1.Images.SetKeyName(332, "Procedure.ico")
        Me.ImageList1.Images.SetKeyName(333, "Process.ico")
        Me.ImageList1.Images.SetKeyName(334, "Project.ico")
        Me.ImageList1.Images.SetKeyName(335, "Project-Gant Chart.ico")
        Me.ImageList1.Images.SetKeyName(336, "Properties.ico")
        Me.ImageList1.Images.SetKeyName(337, "Push Pin.ico")
        Me.ImageList1.Images.SetKeyName(338, "Recycle Bin.ico")
        Me.ImageList1.Images.SetKeyName(339, "Redo.ico")
        Me.ImageList1.Images.SetKeyName(340, "Refresh.ico")
        Me.ImageList1.Images.SetKeyName(341, "Remove.ico")
        Me.ImageList1.Images.SetKeyName(342, "Rename.ico")
        Me.ImageList1.Images.SetKeyName(343, "Report-2.ico")
        Me.ImageList1.Images.SetKeyName(344, "Report.ico")
        Me.ImageList1.Images.SetKeyName(345, "Report-Bound.ico")
        Me.ImageList1.Images.SetKeyName(346, "Research.ico")
        Me.ImageList1.Images.SetKeyName(347, "Ruler.ico")
        Me.ImageList1.Images.SetKeyName(348, "Ruler-Formatting.ico")
        Me.ImageList1.Images.SetKeyName(349, "Save All.ico")
        Me.ImageList1.Images.SetKeyName(350, "Save As HTML.ico")
        Me.ImageList1.Images.SetKeyName(351, "Save As.ico")
        Me.ImageList1.Images.SetKeyName(352, "Save Draft.ico")
        Me.ImageList1.Images.SetKeyName(353, "Save.ico")
        Me.ImageList1.Images.SetKeyName(354, "Script.ico")
        Me.ImageList1.Images.SetKeyName(355, "Search.ico")
        Me.ImageList1.Images.SetKeyName(356, "Search-Next.ico")
        Me.ImageList1.Images.SetKeyName(357, "Search-Previous.ico")
        Me.ImageList1.Images.SetKeyName(358, "Search-Replace.ico")
        Me.ImageList1.Images.SetKeyName(359, "Select All.ico")
        Me.ImageList1.Images.SetKeyName(360, "Slide Show.ico")
        Me.ImageList1.Images.SetKeyName(361, "Slide.ico")
        Me.ImageList1.Images.SetKeyName(362, "Software-Analyse.ico")
        Me.ImageList1.Images.SetKeyName(363, "Sort-Ascending.ico")
        Me.ImageList1.Images.SetKeyName(364, "Sort-Descending.ico")
        Me.ImageList1.Images.SetKeyName(365, "Sound-Delete.ico")
        Me.ImageList1.Images.SetKeyName(366, "Sound-Fade In.ico")
        Me.ImageList1.Images.SetKeyName(367, "Sound-Fade Out.ico")
        Me.ImageList1.Images.SetKeyName(368, "Sound-Join.ico")
        Me.ImageList1.Images.SetKeyName(369, "Sound-Mark End.ico")
        Me.ImageList1.Images.SetKeyName(370, "Sound-Mark Start.ico")
        Me.ImageList1.Images.SetKeyName(371, "Sound-Split.ico")
        Me.ImageList1.Images.SetKeyName(372, "Spell Check.ico")
        Me.ImageList1.Images.SetKeyName(373, "Stop.ico")
        Me.ImageList1.Images.SetKeyName(374, "Strikeout.ico")
        Me.ImageList1.Images.SetKeyName(375, "Subscript.ico")
        Me.ImageList1.Images.SetKeyName(376, "Sum.ico")
        Me.ImageList1.Images.SetKeyName(377, "Summary.ico")
        Me.ImageList1.Images.SetKeyName(378, "Superscript.ico")
        Me.ImageList1.Images.SetKeyName(379, "Support.ico")
        Me.ImageList1.Images.SetKeyName(380, "Tables and Borders.ico")
        Me.ImageList1.Images.SetKeyName(381, "Task.ico")
        Me.ImageList1.Images.SetKeyName(382, "Task-Search.ico")
        Me.ImageList1.Images.SetKeyName(383, "Thesaurus.ico")
        Me.ImageList1.Images.SetKeyName(384, "Tick.ico")
        Me.ImageList1.Images.SetKeyName(385, "Time Line.ico")
        Me.ImageList1.Images.SetKeyName(386, "Tip.ico")
        Me.ImageList1.Images.SetKeyName(387, "To Do List.ico")
        Me.ImageList1.Images.SetKeyName(388, "To Lowercase.ico")
        Me.ImageList1.Images.SetKeyName(389, "To Uppercase.ico")
        Me.ImageList1.Images.SetKeyName(390, "Tools.ico")
        Me.ImageList1.Images.SetKeyName(391, "Tree View.ico")
        Me.ImageList1.Images.SetKeyName(392, "Tree View-Add.ico")
        Me.ImageList1.Images.SetKeyName(393, "Tree View-Delete.ico")
        Me.ImageList1.Images.SetKeyName(394, "Tree View-Edit.ico")
        Me.ImageList1.Images.SetKeyName(395, "Tree View-New.ico")
        Me.ImageList1.Images.SetKeyName(396, "Tree View-Remove.ico")
        Me.ImageList1.Images.SetKeyName(397, "Tree View-Search.ico")
        Me.ImageList1.Images.SetKeyName(398, "Underline.ico")
        Me.ImageList1.Images.SetKeyName(399, "Undo.ico")
        Me.ImageList1.Images.SetKeyName(400, "Unlock.ico")
        Me.ImageList1.Images.SetKeyName(401, "User-2.ico")
        Me.ImageList1.Images.SetKeyName(402, "User.ico")
        Me.ImageList1.Images.SetKeyName(403, "User-Add-2.ico")
        Me.ImageList1.Images.SetKeyName(404, "User-Add.ico")
        Me.ImageList1.Images.SetKeyName(405, "User-Delete-2.ico")
        Me.ImageList1.Images.SetKeyName(406, "User-Delete.ico")
        Me.ImageList1.Images.SetKeyName(407, "User-Edit-2.ico")
        Me.ImageList1.Images.SetKeyName(408, "User-Edit.ico")
        Me.ImageList1.Images.SetKeyName(409, "User-New-2.ico")
        Me.ImageList1.Images.SetKeyName(410, "User-New.ico")
        Me.ImageList1.Images.SetKeyName(411, "User-Options-2.ico")
        Me.ImageList1.Images.SetKeyName(412, "User-Options.ico")
        Me.ImageList1.Images.SetKeyName(413, "User-Password-2.ico")
        Me.ImageList1.Images.SetKeyName(414, "User-Password.ico")
        Me.ImageList1.Images.SetKeyName(415, "User-Remove-2.ico")
        Me.ImageList1.Images.SetKeyName(416, "User-Remove.ico")
        Me.ImageList1.Images.SetKeyName(417, "Users-2.ico")
        Me.ImageList1.Images.SetKeyName(418, "Users.ico")
        Me.ImageList1.Images.SetKeyName(419, "User-Search-2.ico")
        Me.ImageList1.Images.SetKeyName(420, "User-Search.ico")
        Me.ImageList1.Images.SetKeyName(421, "User-Security-2.ico")
        Me.ImageList1.Images.SetKeyName(422, "User-Security.ico")
        Me.ImageList1.Images.SetKeyName(423, "View-Details.ico")
        Me.ImageList1.Images.SetKeyName(424, "View-Large Icons.ico")
        Me.ImageList1.Images.SetKeyName(425, "View-List.ico")
        Me.ImageList1.Images.SetKeyName(426, "View-One Page.ico")
        Me.ImageList1.Images.SetKeyName(427, "View-Page Width.ico")
        Me.ImageList1.Images.SetKeyName(428, "Views.ico")
        Me.ImageList1.Images.SetKeyName(429, "View-Small Icons.ico")
        Me.ImageList1.Images.SetKeyName(430, "Volume.ico")
        Me.ImageList1.Images.SetKeyName(431, "Volume-Mute.ico")
        Me.ImageList1.Images.SetKeyName(432, "Warning.ico")
        Me.ImageList1.Images.SetKeyName(433, "Waste Bin.ico")
        Me.ImageList1.Images.SetKeyName(434, "Window.ico")
        Me.ImageList1.Images.SetKeyName(435, "Window-Add.ico")
        Me.ImageList1.Images.SetKeyName(436, "Window-Bring To Front.ico")
        Me.ImageList1.Images.SetKeyName(437, "Window-Cascade.ico")
        Me.ImageList1.Images.SetKeyName(438, "Window-Close.ico")
        Me.ImageList1.Images.SetKeyName(439, "Window-Delete.ico")
        Me.ImageList1.Images.SetKeyName(440, "Window-Edit.ico")
        Me.ImageList1.Images.SetKeyName(441, "Window-Maximise.ico")
        Me.ImageList1.Images.SetKeyName(442, "Window-Minimise.ico")
        Me.ImageList1.Images.SetKeyName(443, "Window-New.ico")
        Me.ImageList1.Images.SetKeyName(444, "Window-Next.ico")
        Me.ImageList1.Images.SetKeyName(445, "Window-Options.ico")
        Me.ImageList1.Images.SetKeyName(446, "Window-Preview.ico")
        Me.ImageList1.Images.SetKeyName(447, "Window-Previous.ico")
        Me.ImageList1.Images.SetKeyName(448, "Window-Properties.ico")
        Me.ImageList1.Images.SetKeyName(449, "Window-Remove.ico")
        Me.ImageList1.Images.SetKeyName(450, "Window-Search.ico")
        Me.ImageList1.Images.SetKeyName(451, "Window-Send To Back.ico")
        Me.ImageList1.Images.SetKeyName(452, "Window-Split.ico")
        Me.ImageList1.Images.SetKeyName(453, "Window-Tile Horizontal.ico")
        Me.ImageList1.Images.SetKeyName(454, "Window-Tile Vertical.ico")
        Me.ImageList1.Images.SetKeyName(455, "Wizard.ico")
        Me.ImageList1.Images.SetKeyName(456, "Word Count.ico")
        Me.ImageList1.Images.SetKeyName(457, "Word Wrap-2.ico")
        Me.ImageList1.Images.SetKeyName(458, "Word Wrap.ico")
        Me.ImageList1.Images.SetKeyName(459, "Work Sheet.ico")
        Me.ImageList1.Images.SetKeyName(460, "Work Sheet-Auto Format.ico")
        Me.ImageList1.Images.SetKeyName(461, "Work Sheet-Delete.ico")
        Me.ImageList1.Images.SetKeyName(462, "Work Sheet-New.ico")
        Me.ImageList1.Images.SetKeyName(463, "Work Sheet-Options.ico")
        Me.ImageList1.Images.SetKeyName(464, "Work Sheet-Protect.ico")
        Me.ImageList1.Images.SetKeyName(465, "Work Sheet-Rename.ico")
        Me.ImageList1.Images.SetKeyName(466, "Zoom-In.ico")
        Me.ImageList1.Images.SetKeyName(467, "Zoom-Out.ico")
        '
        'TABGeneral
        '
        Me.TABGeneral.AutoScroll = True
        Me.TABGeneral.Controls.Add(Me.PictureBoxDefaultPhoto)
        Me.TABGeneral.Controls.Add(Me.gcPoints)
        Me.TABGeneral.Controls.Add(Me.btnThemeChanged)
        Me.TABGeneral.Controls.Add(Me.btnLoginNameChanged)
        Me.TABGeneral.Controls.Add(Me.btnGEOTwoLetterCountryChanged)
        Me.TABGeneral.Controls.Add(Me.txTheme)
        Me.TABGeneral.Controls.Add(Me.LabelControl19)
        Me.TABGeneral.Controls.Add(Me.txGEOTwoLetterCountry)
        Me.TABGeneral.Controls.Add(Me.txCustomReferrer)
        Me.TABGeneral.Controls.Add(Me.txReferrer)
        Me.TABGeneral.Controls.Add(Me.lbGEOTwoLetterCountry)
        Me.TABGeneral.Controls.Add(Me.lbCustomReferrer)
        Me.TABGeneral.Controls.Add(Me.lbReferrer)
        Me.TABGeneral.Controls.Add(Me.txIPLastLogin)
        Me.TABGeneral.Controls.Add(Me.lbIPLastLogin)
        Me.TABGeneral.Controls.Add(Me.dtLastLogin)
        Me.TABGeneral.Controls.Add(Me.lbIPRegistration)
        Me.TABGeneral.Controls.Add(Me.dtRegistration)
        Me.TABGeneral.Controls.Add(Me.txPassword)
        Me.TABGeneral.Controls.Add(Me.txLoginName)
        Me.TABGeneral.Controls.Add(Me.LabelControl66)
        Me.TABGeneral.Controls.Add(Me.txIPRegistration)
        Me.TABGeneral.Controls.Add(Me.lbRegisterDateTime)
        Me.TABGeneral.Controls.Add(Me.LabelControl4)
        Me.TABGeneral.Controls.Add(Me.lbLoginName)
        Me.TABGeneral.Controls.Add(Me.lbDateTimeLastLogin)
        Me.TABGeneral.Controls.Add(Me.TextEdit3)
        Me.TABGeneral.Controls.Add(Me.lbPassword)
        Me.TABGeneral.ImageIndex = 337
        Me.TABGeneral.Name = "TABGeneral"
        Me.TABGeneral.Size = New System.Drawing.Size(1049, 528)
        Me.TABGeneral.Text = "General"
        '
        'PictureBoxDefaultPhoto
        '
        Me.PictureBoxDefaultPhoto.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBoxDefaultPhoto.InitialImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.guy
        Me.PictureBoxDefaultPhoto.Location = New System.Drawing.Point(788, 38)
        Me.PictureBoxDefaultPhoto.Name = "PictureBoxDefaultPhoto"
        Me.PictureBoxDefaultPhoto.Size = New System.Drawing.Size(255, 256)
        Me.PictureBoxDefaultPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBoxDefaultPhoto.TabIndex = 396
        Me.PictureBoxDefaultPhoto.TabStop = False
        '
        'gcPoints
        '
        Me.gcPoints.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gcPoints.Controls.Add(Me.lblPointsVerification)
        Me.gcPoints.Controls.Add(Me.tbcPointsVerification)
        Me.gcPoints.Controls.Add(Me.lblPointsUnlocksValue)
        Me.gcPoints.Controls.Add(Me.lblPointsUnlocks)
        Me.gcPoints.Controls.Add(Me.lblPointsCreditsValue)
        Me.gcPoints.Controls.Add(Me.lblPointsCredits)
        Me.gcPoints.Controls.Add(Me.lblPointsBeauty)
        Me.gcPoints.Controls.Add(Me.tbcPointsBeauty)
        Me.gcPoints.Location = New System.Drawing.Point(7, 340)
        Me.gcPoints.Name = "gcPoints"
        Me.gcPoints.Size = New System.Drawing.Size(1036, 198)
        Me.gcPoints.TabIndex = 395
        Me.gcPoints.Text = "Member Points"
        '
        'lblPointsVerification
        '
        Me.lblPointsVerification.Location = New System.Drawing.Point(33, 43)
        Me.lblPointsVerification.Name = "lblPointsVerification"
        Me.lblPointsVerification.Size = New System.Drawing.Size(53, 13)
        Me.lblPointsVerification.TabIndex = 7
        Me.lblPointsVerification.Text = "Verification"
        '
        'tbcPointsVerification
        '
        Me.tbcPointsVerification.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PointsVerification", True))
        Me.tbcPointsVerification.EditValue = 1
        Me.tbcPointsVerification.Location = New System.Drawing.Point(92, 34)
        Me.tbcPointsVerification.Name = "tbcPointsVerification"
        Me.tbcPointsVerification.Properties.Minimum = 1
        Me.tbcPointsVerification.Properties.ShowValueToolTip = True
        Me.tbcPointsVerification.Size = New System.Drawing.Size(136, 42)
        Me.tbcPointsVerification.TabIndex = 6
        Me.tbcPointsVerification.Value = 1
        '
        'EUS_ProfilesBindingSource
        '
        Me.EUS_ProfilesBindingSource.DataMember = "EUS_Profiles"
        Me.EUS_ProfilesBindingSource.DataSource = Me.DSMembers
        '
        'DSMembers
        '
        Me.DSMembers.DataSetName = "DSMembers"
        Me.DSMembers.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'lblPointsUnlocksValue
        '
        Me.lblPointsUnlocksValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EUS_ProfilesBindingSource, "PointsUnlocks", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "1", "N0"))
        Me.lblPointsUnlocksValue.Location = New System.Drawing.Point(421, 43)
        Me.lblPointsUnlocksValue.Name = "lblPointsUnlocksValue"
        Me.lblPointsUnlocksValue.Size = New System.Drawing.Size(39, 13)
        Me.lblPointsUnlocksValue.TabIndex = 5
        Me.lblPointsUnlocksValue.Text = "[VALUE]"
        '
        'lblPointsUnlocks
        '
        Me.lblPointsUnlocks.Location = New System.Drawing.Point(381, 43)
        Me.lblPointsUnlocks.Name = "lblPointsUnlocks"
        Me.lblPointsUnlocks.Size = New System.Drawing.Size(36, 13)
        Me.lblPointsUnlocks.TabIndex = 4
        Me.lblPointsUnlocks.Text = "Unlocks"
        '
        'lblPointsCreditsValue
        '
        Me.lblPointsCreditsValue.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.EUS_ProfilesBindingSource, "PointsCredits", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "1", "N0"))
        Me.lblPointsCreditsValue.Location = New System.Drawing.Point(308, 43)
        Me.lblPointsCreditsValue.Name = "lblPointsCreditsValue"
        Me.lblPointsCreditsValue.Size = New System.Drawing.Size(39, 13)
        Me.lblPointsCreditsValue.TabIndex = 3
        Me.lblPointsCreditsValue.Text = "[VALUE]"
        '
        'lblPointsCredits
        '
        Me.lblPointsCredits.Location = New System.Drawing.Point(268, 43)
        Me.lblPointsCredits.Name = "lblPointsCredits"
        Me.lblPointsCredits.Size = New System.Drawing.Size(34, 13)
        Me.lblPointsCredits.TabIndex = 2
        Me.lblPointsCredits.Text = "Credits"
        '
        'lblPointsBeauty
        '
        Me.lblPointsBeauty.Location = New System.Drawing.Point(52, 92)
        Me.lblPointsBeauty.Name = "lblPointsBeauty"
        Me.lblPointsBeauty.Size = New System.Drawing.Size(34, 13)
        Me.lblPointsBeauty.TabIndex = 1
        Me.lblPointsBeauty.Text = "Beauty"
        '
        'tbcPointsBeauty
        '
        Me.tbcPointsBeauty.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PointsBeauty", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation, "1", "N0"))
        Me.tbcPointsBeauty.EditValue = 1
        Me.tbcPointsBeauty.Location = New System.Drawing.Point(92, 83)
        Me.tbcPointsBeauty.Name = "tbcPointsBeauty"
        Me.tbcPointsBeauty.Properties.Minimum = 1
        Me.tbcPointsBeauty.Properties.ShowValueToolTip = True
        Me.tbcPointsBeauty.Size = New System.Drawing.Size(136, 42)
        Me.tbcPointsBeauty.TabIndex = 0
        Me.tbcPointsBeauty.Value = 1
        '
        'btnThemeChanged
        '
        Me.btnThemeChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnThemeChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnThemeChanged.ImageIndex = 0
        Me.btnThemeChanged.ImageList = Me.ImageList1
        Me.btnThemeChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnThemeChanged.Location = New System.Drawing.Point(737, 227)
        Me.btnThemeChanged.Name = "btnThemeChanged"
        Me.btnThemeChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnThemeChanged.TabIndex = 394
        '
        'btnLoginNameChanged
        '
        Me.btnLoginNameChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnLoginNameChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnLoginNameChanged.ImageIndex = 0
        Me.btnLoginNameChanged.ImageList = Me.ImageList1
        Me.btnLoginNameChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnLoginNameChanged.Location = New System.Drawing.Point(737, 69)
        Me.btnLoginNameChanged.Name = "btnLoginNameChanged"
        Me.btnLoginNameChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnLoginNameChanged.TabIndex = 393
        '
        'btnGEOTwoLetterCountryChanged
        '
        Me.btnGEOTwoLetterCountryChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnGEOTwoLetterCountryChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnGEOTwoLetterCountryChanged.ImageList = Me.ImageList1
        Me.btnGEOTwoLetterCountryChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnGEOTwoLetterCountryChanged.Location = New System.Drawing.Point(360, 228)
        Me.btnGEOTwoLetterCountryChanged.Name = "btnGEOTwoLetterCountryChanged"
        Me.btnGEOTwoLetterCountryChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnGEOTwoLetterCountryChanged.TabIndex = 392
        '
        'txTheme
        '
        Me.txTheme.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "ThemeName", True))
        Me.txTheme.Location = New System.Drawing.Point(615, 228)
        Me.txTheme.Name = "txTheme"
        Me.txTheme.Properties.LookAndFeel.SkinName = "Black"
        Me.txTheme.Size = New System.Drawing.Size(116, 20)
        Me.txTheme.TabIndex = 294
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl19.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl19.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl19.Location = New System.Drawing.Point(392, 233)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(217, 13)
        Me.LabelControl19.TabIndex = 293
        Me.LabelControl19.Text = "Theme Name :"
        '
        'txGEOTwoLetterCountry
        '
        Me.txGEOTwoLetterCountry.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Country", True))
        Me.txGEOTwoLetterCountry.Location = New System.Drawing.Point(312, 228)
        Me.txGEOTwoLetterCountry.Name = "txGEOTwoLetterCountry"
        Me.txGEOTwoLetterCountry.Properties.LookAndFeel.SkinName = "Black"
        Me.txGEOTwoLetterCountry.Size = New System.Drawing.Size(42, 20)
        Me.txGEOTwoLetterCountry.TabIndex = 291
        '
        'txCustomReferrer
        '
        Me.txCustomReferrer.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "CustomReferrer", True))
        Me.txCustomReferrer.Location = New System.Drawing.Point(312, 202)
        Me.txCustomReferrer.Name = "txCustomReferrer"
        Me.txCustomReferrer.Properties.LookAndFeel.SkinName = "Black"
        Me.txCustomReferrer.Size = New System.Drawing.Size(419, 20)
        Me.txCustomReferrer.TabIndex = 290
        '
        'txReferrer
        '
        Me.txReferrer.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Referrer", True))
        Me.txReferrer.Location = New System.Drawing.Point(312, 176)
        Me.txReferrer.Name = "txReferrer"
        Me.txReferrer.Properties.LookAndFeel.SkinName = "Black"
        Me.txReferrer.Size = New System.Drawing.Size(419, 20)
        Me.txReferrer.TabIndex = 289
        '
        'lbGEOTwoLetterCountry
        '
        Me.lbGEOTwoLetterCountry.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbGEOTwoLetterCountry.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbGEOTwoLetterCountry.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbGEOTwoLetterCountry.Location = New System.Drawing.Point(12, 232)
        Me.lbGEOTwoLetterCountry.Name = "lbGEOTwoLetterCountry"
        Me.lbGEOTwoLetterCountry.Size = New System.Drawing.Size(294, 13)
        Me.lbGEOTwoLetterCountry.TabIndex = 287
        Me.lbGEOTwoLetterCountry.Text = "GEO Two Letter Country :"
        '
        'lbCustomReferrer
        '
        Me.lbCustomReferrer.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbCustomReferrer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbCustomReferrer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbCustomReferrer.Location = New System.Drawing.Point(12, 205)
        Me.lbCustomReferrer.Name = "lbCustomReferrer"
        Me.lbCustomReferrer.Size = New System.Drawing.Size(294, 13)
        Me.lbCustomReferrer.TabIndex = 286
        Me.lbCustomReferrer.Text = "Custom referrer :"
        '
        'lbReferrer
        '
        Me.lbReferrer.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbReferrer.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbReferrer.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbReferrer.Location = New System.Drawing.Point(12, 179)
        Me.lbReferrer.Name = "lbReferrer"
        Me.lbReferrer.Size = New System.Drawing.Size(294, 13)
        Me.lbReferrer.TabIndex = 285
        Me.lbReferrer.Text = "Referrer :"
        '
        'txIPLastLogin
        '
        Me.txIPLastLogin.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LastLoginIP", True))
        Me.txIPLastLogin.Location = New System.Drawing.Point(615, 149)
        Me.txIPLastLogin.Name = "txIPLastLogin"
        Me.txIPLastLogin.Properties.LookAndFeel.SkinName = "Black"
        Me.txIPLastLogin.Size = New System.Drawing.Size(116, 20)
        Me.txIPLastLogin.TabIndex = 281
        '
        'lbIPLastLogin
        '
        Me.lbIPLastLogin.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbIPLastLogin.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbIPLastLogin.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbIPLastLogin.Location = New System.Drawing.Point(565, 153)
        Me.lbIPLastLogin.Name = "lbIPLastLogin"
        Me.lbIPLastLogin.Size = New System.Drawing.Size(44, 13)
        Me.lbIPLastLogin.TabIndex = 280
        Me.lbIPLastLogin.Text = "IP :"
        '
        'dtLastLogin
        '
        Me.dtLastLogin.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LastLoginDateTime", True))
        Me.dtLastLogin.EditValue = Nothing
        Me.dtLastLogin.Location = New System.Drawing.Point(312, 148)
        Me.dtLastLogin.Name = "dtLastLogin"
        Me.dtLastLogin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtLastLogin.Properties.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"
        Me.dtLastLogin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dtLastLogin.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dtLastLogin.Size = New System.Drawing.Size(247, 20)
        Me.dtLastLogin.TabIndex = 279
        '
        'lbIPRegistration
        '
        Me.lbIPRegistration.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbIPRegistration.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbIPRegistration.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbIPRegistration.Location = New System.Drawing.Point(565, 127)
        Me.lbIPRegistration.Name = "lbIPRegistration"
        Me.lbIPRegistration.Size = New System.Drawing.Size(44, 13)
        Me.lbIPRegistration.TabIndex = 278
        Me.lbIPRegistration.Text = "IP :"
        '
        'dtRegistration
        '
        Me.dtRegistration.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "DateTimeToRegister", True))
        Me.dtRegistration.EditValue = Nothing
        Me.dtRegistration.Location = New System.Drawing.Point(312, 122)
        Me.dtRegistration.Name = "dtRegistration"
        Me.dtRegistration.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dtRegistration.Properties.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"
        Me.dtRegistration.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.dtRegistration.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.dtRegistration.Size = New System.Drawing.Size(247, 20)
        Me.dtRegistration.TabIndex = 277
        '
        'txPassword
        '
        Me.txPassword.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Password", True))
        Me.txPassword.Location = New System.Drawing.Point(312, 96)
        Me.txPassword.Name = "txPassword"
        Me.txPassword.Properties.LookAndFeel.SkinName = "Black"
        Me.txPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txPassword.Properties.UseSystemPasswordChar = True
        Me.txPassword.Size = New System.Drawing.Size(247, 20)
        Me.txPassword.TabIndex = 276
        '
        'txLoginName
        '
        Me.txLoginName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LoginName", True))
        Me.txLoginName.Location = New System.Drawing.Point(312, 70)
        Me.txLoginName.Name = "txLoginName"
        Me.txLoginName.Properties.LookAndFeel.SkinName = "Black"
        Me.txLoginName.Size = New System.Drawing.Size(419, 20)
        Me.txLoginName.TabIndex = 275
        '
        'LabelControl66
        '
        Me.LabelControl66.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl66.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl66.AutoEllipsis = True
        Me.LabelControl66.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl66.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl66.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl66.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl66.Name = "LabelControl66"
        Me.LabelControl66.Size = New System.Drawing.Size(1049, 32)
        Me.LabelControl66.TabIndex = 274
        Me.LabelControl66.Text = "  Εδώ βλέπουμε συνοπτικές πληροφορίες που έχουν επεξεργαστεί είναι οι πιο σημαντι" & _
    "κές πληροφορίες για την μελέτη τις συμπεριφοράς του χρηστή μας."
        '
        'txIPRegistration
        '
        Me.txIPRegistration.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "RegisterIP", True))
        Me.txIPRegistration.Location = New System.Drawing.Point(615, 122)
        Me.txIPRegistration.Name = "txIPRegistration"
        Me.txIPRegistration.Properties.LookAndFeel.SkinName = "Black"
        Me.txIPRegistration.Size = New System.Drawing.Size(116, 20)
        Me.txIPRegistration.TabIndex = 268
        '
        'lbRegisterDateTime
        '
        Me.lbRegisterDateTime.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbRegisterDateTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbRegisterDateTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbRegisterDateTime.Location = New System.Drawing.Point(136, 125)
        Me.lbRegisterDateTime.Name = "lbRegisterDateTime"
        Me.lbRegisterDateTime.Size = New System.Drawing.Size(170, 13)
        Me.lbRegisterDateTime.TabIndex = 267
        Me.lbRegisterDateTime.Text = "Date && Time of registration :"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl4.Location = New System.Drawing.Point(285, 48)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(21, 13)
        Me.LabelControl4.TabIndex = 214
        Me.LabelControl4.Text = "ID :"
        '
        'lbLoginName
        '
        Me.lbLoginName.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbLoginName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbLoginName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbLoginName.Location = New System.Drawing.Point(95, 73)
        Me.lbLoginName.Name = "lbLoginName"
        Me.lbLoginName.Size = New System.Drawing.Size(211, 13)
        Me.lbLoginName.TabIndex = 206
        Me.lbLoginName.Text = "Login Name :"
        '
        'lbDateTimeLastLogin
        '
        Me.lbDateTimeLastLogin.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbDateTimeLastLogin.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbDateTimeLastLogin.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbDateTimeLastLogin.Location = New System.Drawing.Point(57, 151)
        Me.lbDateTimeLastLogin.Name = "lbDateTimeLastLogin"
        Me.lbDateTimeLastLogin.Size = New System.Drawing.Size(249, 13)
        Me.lbDateTimeLastLogin.TabIndex = 261
        Me.lbDateTimeLastLogin.Text = "Date && Time of last login :"
        '
        'TextEdit3
        '
        Me.TextEdit3.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "ProfileID", True))
        Me.TextEdit3.Enabled = False
        Me.TextEdit3.Location = New System.Drawing.Point(312, 45)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.LookAndFeel.SkinName = "Black"
        Me.TextEdit3.Size = New System.Drawing.Size(108, 20)
        Me.TextEdit3.TabIndex = 0
        '
        'lbPassword
        '
        Me.lbPassword.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbPassword.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbPassword.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbPassword.Location = New System.Drawing.Point(95, 99)
        Me.lbPassword.Name = "lbPassword"
        Me.lbPassword.Size = New System.Drawing.Size(211, 13)
        Me.lbPassword.TabIndex = 205
        Me.lbPassword.Text = "Password :"
        '
        'TABAccount
        '
        Me.TABAccount.Controls.Add(Me.XtraTabControl1)
        Me.TABAccount.ImageIndex = 401
        Me.TABAccount.Name = "TABAccount"
        Me.TABAccount.Size = New System.Drawing.Size(1049, 528)
        Me.TABAccount.Text = "Account"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left
        Me.XtraTabControl1.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Horizontal
        Me.XtraTabControl1.Images = Me.ImageList1
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.TabPageAccount
        Me.XtraTabControl1.Size = New System.Drawing.Size(1049, 528)
        Me.XtraTabControl1.TabIndex = 251
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.TabPageAccount, Me.TabPageAboutMe, Me.TabPagePersonal, Me.TabPageLookingFor, Me.TabPageSettings})
        '
        'TabPageAccount
        '
        Me.TabPageAccount.Controls.Add(Me.DateEdit1)
        Me.TabPageAccount.Controls.Add(Me.LabelControl33)
        Me.TabPageAccount.Controls.Add(Me.btnTWNameChanged)
        Me.TabPageAccount.Controls.Add(Me.txRegion)
        Me.TabPageAccount.Controls.Add(Me.LabelControl30)
        Me.TabPageAccount.Controls.Add(Me.icbAccountTypeId)
        Me.TabPageAccount.Controls.Add(Me.txtTWName)
        Me.TabPageAccount.Controls.Add(Me.lbStatus)
        Me.TabPageAccount.Controls.Add(Me.lblTWName)
        Me.TabPageAccount.Controls.Add(Me.lbPassword2)
        Me.TabPageAccount.Controls.Add(Me.btnFBNameChanged)
        Me.TabPageAccount.Controls.Add(Me.lbMirrorProfileID)
        Me.TabPageAccount.Controls.Add(Me.txtFBName)
        Me.TabPageAccount.Controls.Add(Me.lbIsMaster)
        Me.TabPageAccount.Controls.Add(Me.lblFBName)
        Me.TabPageAccount.Controls.Add(Me.lbLoginName2)
        Me.TabPageAccount.Controls.Add(Me.icbGenderId)
        Me.TabPageAccount.Controls.Add(Me.txMirrorProfileID)
        Me.TabPageAccount.Controls.Add(Me.LabelControl28)
        Me.TabPageAccount.Controls.Add(Me.lbFirstName)
        Me.TabPageAccount.Controls.Add(Me.btnCityAreaChanged)
        Me.TabPageAccount.Controls.Add(Me.lbLastName)
        Me.TabPageAccount.Controls.Add(Me.txCityArea)
        Me.TabPageAccount.Controls.Add(Me.txFirstName)
        Me.TabPageAccount.Controls.Add(Me.lblCityArea)
        Me.TabPageAccount.Controls.Add(Me.txLastName)
        Me.TabPageAccount.Controls.Add(Me.btnZipChanged)
        Me.TabPageAccount.Controls.Add(Me.ckIsMaster)
        Me.TabPageAccount.Controls.Add(Me.btnCellularChanged)
        Me.TabPageAccount.Controls.Add(Me.txLoginName2)
        Me.TabPageAccount.Controls.Add(Me.btnEmailChanged)
        Me.TabPageAccount.Controls.Add(Me.txPassword2)
        Me.TabPageAccount.Controls.Add(Me.btnTelephoneChanged)
        Me.TabPageAccount.Controls.Add(Me.lbGender)
        Me.TabPageAccount.Controls.Add(Me.btnAddressChanged)
        Me.TabPageAccount.Controls.Add(Me.lbCountry)
        Me.TabPageAccount.Controls.Add(Me.btnCityChanged)
        Me.TabPageAccount.Controls.Add(Me.lbRegion)
        Me.TabPageAccount.Controls.Add(Me.btnRegionChanged)
        Me.TabPageAccount.Controls.Add(Me.lbZip)
        Me.TabPageAccount.Controls.Add(Me.btnCountry2Changed)
        Me.TabPageAccount.Controls.Add(Me.txCountry)
        Me.TabPageAccount.Controls.Add(Me.btnLastNameChanged)
        Me.TabPageAccount.Controls.Add(Me.txZip)
        Me.TabPageAccount.Controls.Add(Me.btnFirstNameChanged)
        Me.TabPageAccount.Controls.Add(Me.lbCity)
        Me.TabPageAccount.Controls.Add(Me.btnLoginName2Changed)
        Me.TabPageAccount.Controls.Add(Me.lbTelephone)
        Me.TabPageAccount.Controls.Add(Me.cbStatus)
        Me.TabPageAccount.Controls.Add(Me.lbeMail)
        Me.TabPageAccount.Controls.Add(Me.ckAreYouWillingToTravel)
        Me.TabPageAccount.Controls.Add(Me.txCity)
        Me.TabPageAccount.Controls.Add(Me.txCellular)
        Me.TabPageAccount.Controls.Add(Me.txTelephone)
        Me.TabPageAccount.Controls.Add(Me.lbAreYouWillingToTravel)
        Me.TabPageAccount.Controls.Add(Me.txeMail)
        Me.TabPageAccount.Controls.Add(Me.lbCellular)
        Me.TabPageAccount.Controls.Add(Me.lbAddress)
        Me.TabPageAccount.Controls.Add(Me.txAddress)
        Me.TabPageAccount.ImageIndex = 402
        Me.TabPageAccount.Name = "TabPageAccount"
        Me.TabPageAccount.Size = New System.Drawing.Size(947, 522)
        Me.TabPageAccount.Text = "Account"
        '
        'DateEdit1
        '
        Me.DateEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Birthday", True))
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(772, 96)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Pink
        Me.DateEdit1.Properties.Appearance.Options.UseBackColor = True
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.DisplayFormat.FormatString = "dd/MM/yyyy"
        Me.DateEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton()})
        Me.DateEdit1.Size = New System.Drawing.Size(162, 20)
        Me.DateEdit1.TabIndex = 412
        '
        'LabelControl33
        '
        Me.LabelControl33.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl33.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl33.AutoEllipsis = True
        Me.LabelControl33.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl33.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl33.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl33.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(947, 32)
        Me.LabelControl33.TabIndex = 275
        Me.LabelControl33.Text = "  Εδώ βλέπουμε συνοπτικές πληροφορίες που έχουν επεξεργαστεί είναι οι πιο σημαντι" & _
    "κές πληροφορίες για την μελέτη τις συμπεριφοράς του χρηστή μας."
        '
        'btnTWNameChanged
        '
        Me.btnTWNameChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnTWNameChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnTWNameChanged.ImageIndex = 0
        Me.btnTWNameChanged.ImageList = Me.ImageList1
        Me.btnTWNameChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnTWNameChanged.Location = New System.Drawing.Point(581, 510)
        Me.btnTWNameChanged.Name = "btnTWNameChanged"
        Me.btnTWNameChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnTWNameChanged.TabIndex = 410
        Me.btnTWNameChanged.Visible = False
        '
        'txRegion
        '
        Me.txRegion.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Region", True))
        Me.txRegion.Location = New System.Drawing.Point(328, 279)
        Me.txRegion.Name = "txRegion"
        Me.txRegion.Properties.LookAndFeel.SkinName = "Black"
        Me.txRegion.Size = New System.Drawing.Size(247, 20)
        Me.txRegion.TabIndex = 317
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl30.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl30.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl30.Location = New System.Drawing.Point(622, 99)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(144, 13)
        Me.LabelControl30.TabIndex = 411
        Me.LabelControl30.Text = "Birthdate :"
        '
        'icbAccountTypeId
        '
        Me.icbAccountTypeId.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "AccountTypeId", True))
        Me.icbAccountTypeId.Location = New System.Drawing.Point(328, 227)
        Me.icbAccountTypeId.Name = "icbAccountTypeId"
        Me.icbAccountTypeId.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbAccountTypeId.Properties.LookAndFeel.SkinName = "Black"
        Me.icbAccountTypeId.Size = New System.Drawing.Size(247, 20)
        Me.icbAccountTypeId.TabIndex = 315
        '
        'txtTWName
        '
        Me.txtTWName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "TwitterName", True))
        Me.txtTWName.Location = New System.Drawing.Point(328, 511)
        Me.txtTWName.Name = "txtTWName"
        Me.txtTWName.Properties.LookAndFeel.SkinName = "Black"
        Me.txtTWName.Size = New System.Drawing.Size(247, 20)
        Me.txtTWName.TabIndex = 409
        '
        'lbStatus
        '
        Me.lbStatus.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbStatus.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbStatus.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbStatus.Location = New System.Drawing.Point(274, 72)
        Me.lbStatus.Name = "lbStatus"
        Me.lbStatus.Size = New System.Drawing.Size(48, 13)
        Me.lbStatus.TabIndex = 292
        Me.lbStatus.Text = "Status :"
        '
        'lblTWName
        '
        Me.lblTWName.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblTWName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblTWName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblTWName.Location = New System.Drawing.Point(216, 514)
        Me.lblTWName.Name = "lblTWName"
        Me.lblTWName.Size = New System.Drawing.Size(106, 13)
        Me.lblTWName.TabIndex = 408
        Me.lblTWName.Text = "Twitter Name :"
        '
        'lbPassword2
        '
        Me.lbPassword2.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbPassword2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbPassword2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbPassword2.Location = New System.Drawing.Point(73, 119)
        Me.lbPassword2.Name = "lbPassword2"
        Me.lbPassword2.Size = New System.Drawing.Size(249, 23)
        Me.lbPassword2.TabIndex = 295
        Me.lbPassword2.Text = "Password :"
        '
        'btnFBNameChanged
        '
        Me.btnFBNameChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnFBNameChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnFBNameChanged.ImageIndex = 0
        Me.btnFBNameChanged.ImageList = Me.ImageList1
        Me.btnFBNameChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnFBNameChanged.Location = New System.Drawing.Point(581, 484)
        Me.btnFBNameChanged.Name = "btnFBNameChanged"
        Me.btnFBNameChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnFBNameChanged.TabIndex = 407
        Me.btnFBNameChanged.Visible = False
        '
        'lbMirrorProfileID
        '
        Me.lbMirrorProfileID.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbMirrorProfileID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbMirrorProfileID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbMirrorProfileID.Location = New System.Drawing.Point(224, 46)
        Me.lbMirrorProfileID.Name = "lbMirrorProfileID"
        Me.lbMirrorProfileID.Size = New System.Drawing.Size(98, 13)
        Me.lbMirrorProfileID.TabIndex = 293
        Me.lbMirrorProfileID.Text = "Mirror Profile ID :"
        '
        'txtFBName
        '
        Me.txtFBName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "FacebookName", True))
        Me.txtFBName.Location = New System.Drawing.Point(328, 485)
        Me.txtFBName.Name = "txtFBName"
        Me.txtFBName.Properties.LookAndFeel.SkinName = "Black"
        Me.txtFBName.Size = New System.Drawing.Size(247, 20)
        Me.txtFBName.TabIndex = 406
        '
        'lbIsMaster
        '
        Me.lbIsMaster.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbIsMaster.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbIsMaster.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbIsMaster.Location = New System.Drawing.Point(28, 35)
        Me.lbIsMaster.Name = "lbIsMaster"
        Me.lbIsMaster.Size = New System.Drawing.Size(60, 13)
        Me.lbIsMaster.TabIndex = 294
        Me.lbIsMaster.Text = "Is Master :"
        Me.lbIsMaster.Visible = False
        '
        'lblFBName
        '
        Me.lblFBName.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblFBName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblFBName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblFBName.Location = New System.Drawing.Point(216, 488)
        Me.lblFBName.Name = "lblFBName"
        Me.lblFBName.Size = New System.Drawing.Size(106, 13)
        Me.lblFBName.TabIndex = 405
        Me.lblFBName.Text = "Facebook Name :"
        '
        'lbLoginName2
        '
        Me.lbLoginName2.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbLoginName2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbLoginName2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbLoginName2.Location = New System.Drawing.Point(152, 99)
        Me.lbLoginName2.Name = "lbLoginName2"
        Me.lbLoginName2.Size = New System.Drawing.Size(170, 13)
        Me.lbLoginName2.TabIndex = 296
        Me.lbLoginName2.Text = "Login Name :"
        '
        'icbGenderId
        '
        Me.icbGenderId.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "GenderId", True))
        Me.icbGenderId.Location = New System.Drawing.Point(328, 201)
        Me.icbGenderId.Name = "icbGenderId"
        Me.icbGenderId.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.icbGenderId.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.icbGenderId.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbGenderId.Size = New System.Drawing.Size(247, 20)
        Me.icbGenderId.TabIndex = 404
        '
        'txMirrorProfileID
        '
        Me.txMirrorProfileID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "MirrorProfileID", True))
        Me.txMirrorProfileID.Location = New System.Drawing.Point(328, 43)
        Me.txMirrorProfileID.Name = "txMirrorProfileID"
        Me.txMirrorProfileID.Properties.LookAndFeel.SkinName = "Black"
        Me.txMirrorProfileID.Size = New System.Drawing.Size(247, 20)
        Me.txMirrorProfileID.TabIndex = 297
        '
        'LabelControl28
        '
        Me.LabelControl28.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl28.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl28.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl28.Location = New System.Drawing.Point(274, 204)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(48, 13)
        Me.LabelControl28.TabIndex = 403
        Me.LabelControl28.Text = "Gender :"
        '
        'lbFirstName
        '
        Me.lbFirstName.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbFirstName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbFirstName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbFirstName.Location = New System.Drawing.Point(28, 152)
        Me.lbFirstName.Name = "lbFirstName"
        Me.lbFirstName.Size = New System.Drawing.Size(294, 13)
        Me.lbFirstName.TabIndex = 304
        Me.lbFirstName.Text = "First Name :"
        '
        'btnCityAreaChanged
        '
        Me.btnCityAreaChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnCityAreaChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnCityAreaChanged.ImageIndex = 0
        Me.btnCityAreaChanged.ImageList = Me.ImageList1
        Me.btnCityAreaChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnCityAreaChanged.Location = New System.Drawing.Point(581, 330)
        Me.btnCityAreaChanged.Name = "btnCityAreaChanged"
        Me.btnCityAreaChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnCityAreaChanged.TabIndex = 402
        '
        'lbLastName
        '
        Me.lbLastName.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbLastName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbLastName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbLastName.Location = New System.Drawing.Point(28, 179)
        Me.lbLastName.Name = "lbLastName"
        Me.lbLastName.Size = New System.Drawing.Size(294, 13)
        Me.lbLastName.TabIndex = 305
        Me.lbLastName.Text = "Last Name :"
        '
        'txCityArea
        '
        Me.txCityArea.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "CityArea", True))
        Me.txCityArea.Location = New System.Drawing.Point(328, 331)
        Me.txCityArea.Name = "txCityArea"
        Me.txCityArea.Properties.LookAndFeel.SkinName = "Black"
        Me.txCityArea.Size = New System.Drawing.Size(247, 20)
        Me.txCityArea.TabIndex = 401
        '
        'txFirstName
        '
        Me.txFirstName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "FirstName", True))
        Me.txFirstName.Location = New System.Drawing.Point(328, 149)
        Me.txFirstName.Name = "txFirstName"
        Me.txFirstName.Properties.Appearance.BackColor = System.Drawing.Color.LightGoldenrodYellow
        Me.txFirstName.Properties.Appearance.Options.UseBackColor = True
        Me.txFirstName.Properties.LookAndFeel.SkinName = "Black"
        Me.txFirstName.Size = New System.Drawing.Size(247, 20)
        Me.txFirstName.TabIndex = 306
        '
        'lblCityArea
        '
        Me.lblCityArea.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblCityArea.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblCityArea.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblCityArea.Location = New System.Drawing.Point(262, 334)
        Me.lblCityArea.Name = "lblCityArea"
        Me.lblCityArea.Size = New System.Drawing.Size(60, 13)
        Me.lblCityArea.TabIndex = 400
        Me.lblCityArea.Text = "City Area :"
        '
        'txLastName
        '
        Me.txLastName.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LastName", True))
        Me.txLastName.Location = New System.Drawing.Point(328, 175)
        Me.txLastName.Name = "txLastName"
        Me.txLastName.Properties.LookAndFeel.SkinName = "Black"
        Me.txLastName.Size = New System.Drawing.Size(247, 20)
        Me.txLastName.TabIndex = 307
        '
        'btnZipChanged
        '
        Me.btnZipChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnZipChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnZipChanged.ImageIndex = 0
        Me.btnZipChanged.ImageList = Me.ImageList1
        Me.btnZipChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnZipChanged.Location = New System.Drawing.Point(748, 278)
        Me.btnZipChanged.Name = "btnZipChanged"
        Me.btnZipChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnZipChanged.TabIndex = 399
        '
        'ckIsMaster
        '
        Me.ckIsMaster.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "IsMaster", True))
        Me.ckIsMaster.Location = New System.Drawing.Point(94, 32)
        Me.ckIsMaster.Name = "ckIsMaster"
        Me.ckIsMaster.Properties.Caption = ""
        Me.ckIsMaster.Size = New System.Drawing.Size(19, 19)
        Me.ckIsMaster.TabIndex = 308
        Me.ckIsMaster.Visible = False
        '
        'btnCellularChanged
        '
        Me.btnCellularChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnCellularChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnCellularChanged.ImageIndex = 0
        Me.btnCellularChanged.ImageList = Me.ImageList1
        Me.btnCellularChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnCellularChanged.Location = New System.Drawing.Point(581, 434)
        Me.btnCellularChanged.Name = "btnCellularChanged"
        Me.btnCellularChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnCellularChanged.TabIndex = 398
        '
        'txLoginName2
        '
        Me.txLoginName2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LoginName", True))
        Me.txLoginName2.Location = New System.Drawing.Point(328, 95)
        Me.txLoginName2.Name = "txLoginName2"
        Me.txLoginName2.Properties.Appearance.BackColor = System.Drawing.Color.Honeydew
        Me.txLoginName2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txLoginName2.Properties.Appearance.Options.UseBackColor = True
        Me.txLoginName2.Properties.Appearance.Options.UseFont = True
        Me.txLoginName2.Properties.LookAndFeel.SkinName = "Black"
        Me.txLoginName2.Size = New System.Drawing.Size(247, 20)
        Me.txLoginName2.TabIndex = 309
        '
        'btnEmailChanged
        '
        Me.btnEmailChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnEmailChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnEmailChanged.ImageIndex = 0
        Me.btnEmailChanged.ImageList = Me.ImageList1
        Me.btnEmailChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnEmailChanged.Location = New System.Drawing.Point(581, 408)
        Me.btnEmailChanged.Name = "btnEmailChanged"
        Me.btnEmailChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnEmailChanged.TabIndex = 397
        '
        'txPassword2
        '
        Me.txPassword2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Password", True))
        Me.txPassword2.Location = New System.Drawing.Point(328, 122)
        Me.txPassword2.Name = "txPassword2"
        Me.txPassword2.Properties.LookAndFeel.SkinName = "Black"
        Me.txPassword2.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txPassword2.Properties.UseSystemPasswordChar = True
        Me.txPassword2.Size = New System.Drawing.Size(247, 20)
        Me.txPassword2.TabIndex = 310
        '
        'btnTelephoneChanged
        '
        Me.btnTelephoneChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnTelephoneChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnTelephoneChanged.ImageIndex = 0
        Me.btnTelephoneChanged.ImageList = Me.ImageList1
        Me.btnTelephoneChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnTelephoneChanged.Location = New System.Drawing.Point(581, 382)
        Me.btnTelephoneChanged.Name = "btnTelephoneChanged"
        Me.btnTelephoneChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnTelephoneChanged.TabIndex = 396
        '
        'lbGender
        '
        Me.lbGender.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbGender.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbGender.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbGender.Location = New System.Drawing.Point(28, 230)
        Me.lbGender.Name = "lbGender"
        Me.lbGender.Size = New System.Drawing.Size(294, 13)
        Me.lbGender.TabIndex = 311
        Me.lbGender.Text = "Account Type :"
        '
        'btnAddressChanged
        '
        Me.btnAddressChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnAddressChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnAddressChanged.ImageIndex = 0
        Me.btnAddressChanged.ImageList = Me.ImageList1
        Me.btnAddressChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnAddressChanged.Location = New System.Drawing.Point(581, 356)
        Me.btnAddressChanged.Name = "btnAddressChanged"
        Me.btnAddressChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnAddressChanged.TabIndex = 395
        '
        'lbCountry
        '
        Me.lbCountry.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbCountry.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbCountry.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbCountry.Location = New System.Drawing.Point(28, 256)
        Me.lbCountry.Name = "lbCountry"
        Me.lbCountry.Size = New System.Drawing.Size(294, 13)
        Me.lbCountry.TabIndex = 312
        Me.lbCountry.Text = "Country :"
        '
        'btnCityChanged
        '
        Me.btnCityChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnCityChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnCityChanged.ImageIndex = 0
        Me.btnCityChanged.ImageList = Me.ImageList1
        Me.btnCityChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnCityChanged.Location = New System.Drawing.Point(581, 304)
        Me.btnCityChanged.Name = "btnCityChanged"
        Me.btnCityChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnCityChanged.TabIndex = 394
        '
        'lbRegion
        '
        Me.lbRegion.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbRegion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbRegion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbRegion.Location = New System.Drawing.Point(28, 282)
        Me.lbRegion.Name = "lbRegion"
        Me.lbRegion.Size = New System.Drawing.Size(294, 13)
        Me.lbRegion.TabIndex = 313
        Me.lbRegion.Text = "Region :"
        '
        'btnRegionChanged
        '
        Me.btnRegionChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnRegionChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnRegionChanged.ImageIndex = 0
        Me.btnRegionChanged.ImageList = Me.ImageList1
        Me.btnRegionChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnRegionChanged.Location = New System.Drawing.Point(581, 278)
        Me.btnRegionChanged.Name = "btnRegionChanged"
        Me.btnRegionChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnRegionChanged.TabIndex = 393
        '
        'lbZip
        '
        Me.lbZip.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbZip.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbZip.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbZip.Location = New System.Drawing.Point(635, 283)
        Me.lbZip.Name = "lbZip"
        Me.lbZip.Size = New System.Drawing.Size(30, 13)
        Me.lbZip.TabIndex = 314
        Me.lbZip.Text = "Zip :"
        '
        'btnCountry2Changed
        '
        Me.btnCountry2Changed.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnCountry2Changed.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnCountry2Changed.ImageIndex = 0
        Me.btnCountry2Changed.ImageList = Me.ImageList1
        Me.btnCountry2Changed.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnCountry2Changed.Location = New System.Drawing.Point(581, 252)
        Me.btnCountry2Changed.Name = "btnCountry2Changed"
        Me.btnCountry2Changed.Size = New System.Drawing.Size(24, 21)
        Me.btnCountry2Changed.TabIndex = 392
        '
        'txCountry
        '
        Me.txCountry.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Country", True))
        Me.txCountry.Location = New System.Drawing.Point(328, 253)
        Me.txCountry.Name = "txCountry"
        Me.txCountry.Properties.Appearance.BackColor = System.Drawing.Color.GhostWhite
        Me.txCountry.Properties.Appearance.Options.UseBackColor = True
        Me.txCountry.Properties.LookAndFeel.SkinName = "Black"
        Me.txCountry.Size = New System.Drawing.Size(247, 20)
        Me.txCountry.TabIndex = 316
        '
        'btnLastNameChanged
        '
        Me.btnLastNameChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnLastNameChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnLastNameChanged.ImageIndex = 0
        Me.btnLastNameChanged.ImageList = Me.ImageList1
        Me.btnLastNameChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnLastNameChanged.Location = New System.Drawing.Point(581, 175)
        Me.btnLastNameChanged.Name = "btnLastNameChanged"
        Me.btnLastNameChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnLastNameChanged.TabIndex = 390
        '
        'txZip
        '
        Me.txZip.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Zip", True))
        Me.txZip.Location = New System.Drawing.Point(671, 279)
        Me.txZip.Name = "txZip"
        Me.txZip.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.txZip.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txZip.Properties.Appearance.Options.UseBackColor = True
        Me.txZip.Properties.Appearance.Options.UseFont = True
        Me.txZip.Properties.LookAndFeel.SkinName = "Black"
        Me.txZip.Size = New System.Drawing.Size(71, 20)
        Me.txZip.TabIndex = 318
        '
        'btnFirstNameChanged
        '
        Me.btnFirstNameChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnFirstNameChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnFirstNameChanged.ImageIndex = 0
        Me.btnFirstNameChanged.ImageList = Me.ImageList1
        Me.btnFirstNameChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnFirstNameChanged.Location = New System.Drawing.Point(581, 148)
        Me.btnFirstNameChanged.Name = "btnFirstNameChanged"
        Me.btnFirstNameChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnFirstNameChanged.TabIndex = 389
        '
        'lbCity
        '
        Me.lbCity.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbCity.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbCity.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbCity.Location = New System.Drawing.Point(262, 308)
        Me.lbCity.Name = "lbCity"
        Me.lbCity.Size = New System.Drawing.Size(60, 13)
        Me.lbCity.TabIndex = 319
        Me.lbCity.Text = "City :"
        '
        'btnLoginName2Changed
        '
        Me.btnLoginName2Changed.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnLoginName2Changed.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnLoginName2Changed.ImageIndex = 0
        Me.btnLoginName2Changed.ImageList = Me.ImageList1
        Me.btnLoginName2Changed.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnLoginName2Changed.Location = New System.Drawing.Point(581, 95)
        Me.btnLoginName2Changed.Name = "btnLoginName2Changed"
        Me.btnLoginName2Changed.Size = New System.Drawing.Size(24, 21)
        Me.btnLoginName2Changed.TabIndex = 388
        '
        'lbTelephone
        '
        Me.lbTelephone.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbTelephone.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbTelephone.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbTelephone.Location = New System.Drawing.Point(28, 386)
        Me.lbTelephone.Name = "lbTelephone"
        Me.lbTelephone.Size = New System.Drawing.Size(294, 13)
        Me.lbTelephone.TabIndex = 320
        Me.lbTelephone.Text = "Telephone :"
        '
        'cbStatus
        '
        Me.cbStatus.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Status", True))
        Me.cbStatus.Location = New System.Drawing.Point(328, 69)
        Me.cbStatus.Name = "cbStatus"
        Me.cbStatus.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.cbStatus.Properties.AppearanceDisabled.Options.UseBackColor = True
        Me.cbStatus.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbStatus.Properties.ReadOnly = True
        Me.cbStatus.Size = New System.Drawing.Size(247, 20)
        Me.cbStatus.TabIndex = 387
        '
        'lbeMail
        '
        Me.lbeMail.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbeMail.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbeMail.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbeMail.Location = New System.Drawing.Point(28, 412)
        Me.lbeMail.Name = "lbeMail"
        Me.lbeMail.Size = New System.Drawing.Size(294, 13)
        Me.lbeMail.TabIndex = 321
        Me.lbeMail.Text = "eMail :"
        '
        'ckAreYouWillingToTravel
        '
        Me.ckAreYouWillingToTravel.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "AreYouWillingToTravel", True))
        Me.ckAreYouWillingToTravel.Location = New System.Drawing.Point(326, 461)
        Me.ckAreYouWillingToTravel.Name = "ckAreYouWillingToTravel"
        Me.ckAreYouWillingToTravel.Properties.Caption = ""
        Me.ckAreYouWillingToTravel.Size = New System.Drawing.Size(75, 19)
        Me.ckAreYouWillingToTravel.TabIndex = 330
        '
        'txCity
        '
        Me.txCity.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "City", True))
        Me.txCity.Location = New System.Drawing.Point(328, 305)
        Me.txCity.Name = "txCity"
        Me.txCity.Properties.LookAndFeel.SkinName = "Black"
        Me.txCity.Size = New System.Drawing.Size(247, 20)
        Me.txCity.TabIndex = 322
        '
        'txCellular
        '
        Me.txCellular.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Cellular", True))
        Me.txCellular.Location = New System.Drawing.Point(328, 435)
        Me.txCellular.Name = "txCellular"
        Me.txCellular.Properties.LookAndFeel.SkinName = "Black"
        Me.txCellular.Size = New System.Drawing.Size(247, 20)
        Me.txCellular.TabIndex = 329
        '
        'txTelephone
        '
        Me.txTelephone.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Telephone", True))
        Me.txTelephone.Location = New System.Drawing.Point(328, 383)
        Me.txTelephone.Name = "txTelephone"
        Me.txTelephone.Properties.LookAndFeel.SkinName = "Black"
        Me.txTelephone.Size = New System.Drawing.Size(247, 20)
        Me.txTelephone.TabIndex = 323
        '
        'lbAreYouWillingToTravel
        '
        Me.lbAreYouWillingToTravel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbAreYouWillingToTravel.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbAreYouWillingToTravel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbAreYouWillingToTravel.Location = New System.Drawing.Point(28, 462)
        Me.lbAreYouWillingToTravel.Name = "lbAreYouWillingToTravel"
        Me.lbAreYouWillingToTravel.Size = New System.Drawing.Size(294, 13)
        Me.lbAreYouWillingToTravel.TabIndex = 328
        Me.lbAreYouWillingToTravel.Text = "Are You Willing To Travel :"
        '
        'txeMail
        '
        Me.txeMail.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "eMail", True))
        Me.txeMail.Location = New System.Drawing.Point(328, 409)
        Me.txeMail.Name = "txeMail"
        Me.txeMail.Properties.LookAndFeel.SkinName = "Black"
        Me.txeMail.Size = New System.Drawing.Size(247, 20)
        Me.txeMail.TabIndex = 324
        '
        'lbCellular
        '
        Me.lbCellular.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbCellular.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbCellular.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbCellular.Location = New System.Drawing.Point(28, 438)
        Me.lbCellular.Name = "lbCellular"
        Me.lbCellular.Size = New System.Drawing.Size(294, 13)
        Me.lbCellular.TabIndex = 327
        Me.lbCellular.Text = "Cellular :"
        '
        'lbAddress
        '
        Me.lbAddress.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbAddress.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbAddress.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbAddress.Location = New System.Drawing.Point(28, 360)
        Me.lbAddress.Name = "lbAddress"
        Me.lbAddress.Size = New System.Drawing.Size(294, 13)
        Me.lbAddress.TabIndex = 325
        Me.lbAddress.Text = "Address :"
        '
        'txAddress
        '
        Me.txAddress.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "Address", True))
        Me.txAddress.Location = New System.Drawing.Point(328, 357)
        Me.txAddress.Name = "txAddress"
        Me.txAddress.Properties.LookAndFeel.SkinName = "Black"
        Me.txAddress.Size = New System.Drawing.Size(247, 20)
        Me.txAddress.TabIndex = 326
        '
        'TabPageAboutMe
        '
        Me.TabPageAboutMe.Controls.Add(Me.MemoEdit3)
        Me.TabPageAboutMe.Controls.Add(Me.MemoEdit2)
        Me.TabPageAboutMe.Controls.Add(Me.MemoEdit1)
        Me.TabPageAboutMe.Controls.Add(Me.btnOtherDetails_OccupationChanged)
        Me.TabPageAboutMe.Controls.Add(Me.btnAboutMe_HeadingChanged)
        Me.TabPageAboutMe.Controls.Add(Me.icbNetWorth)
        Me.TabPageAboutMe.Controls.Add(Me.icbAnnualIncome)
        Me.TabPageAboutMe.Controls.Add(Me.LabelControl34)
        Me.TabPageAboutMe.Controls.Add(Me.txOccupation)
        Me.TabPageAboutMe.Controls.Add(Me.btnAboutMe_DescribeYourselfChanged)
        Me.TabPageAboutMe.Controls.Add(Me.lbOccupation)
        Me.TabPageAboutMe.Controls.Add(Me.lbNetWorthID)
        Me.TabPageAboutMe.Controls.Add(Me.btnAboutMe_DescribeAnIdealFirstDateChanged)
        Me.TabPageAboutMe.Controls.Add(Me.lbAnnualIncomeID)
        Me.TabPageAboutMe.Controls.Add(Me.lbDescribeYourself)
        Me.TabPageAboutMe.Controls.Add(Me.lbEducationID)
        Me.TabPageAboutMe.Controls.Add(Me.lbHeading)
        Me.TabPageAboutMe.Controls.Add(Me.icbEducation)
        Me.TabPageAboutMe.Controls.Add(Me.lbDescribeAnIdealFirstDate)
        Me.TabPageAboutMe.ImageIndex = 408
        Me.TabPageAboutMe.Name = "TabPageAboutMe"
        Me.TabPageAboutMe.Size = New System.Drawing.Size(947, 527)
        Me.TabPageAboutMe.Text = "About me"
        '
        'MemoEdit3
        '
        Me.MemoEdit3.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "AboutMe_DescribeAnIdealFirstDate", True))
        Me.MemoEdit3.Location = New System.Drawing.Point(358, 267)
        Me.MemoEdit3.Name = "MemoEdit3"
        Me.MemoEdit3.Size = New System.Drawing.Size(525, 91)
        Me.MemoEdit3.TabIndex = 396
        '
        'MemoEdit2
        '
        Me.MemoEdit2.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "AboutMe_DescribeYourself", True))
        Me.MemoEdit2.Location = New System.Drawing.Point(358, 170)
        Me.MemoEdit2.Name = "MemoEdit2"
        Me.MemoEdit2.Size = New System.Drawing.Size(525, 91)
        Me.MemoEdit2.TabIndex = 395
        '
        'MemoEdit1
        '
        Me.MemoEdit1.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "AboutMe_Heading", True))
        Me.MemoEdit1.Location = New System.Drawing.Point(358, 121)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Size = New System.Drawing.Size(525, 43)
        Me.MemoEdit1.TabIndex = 392
        '
        'btnOtherDetails_OccupationChanged
        '
        Me.btnOtherDetails_OccupationChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnOtherDetails_OccupationChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnOtherDetails_OccupationChanged.ImageIndex = 0
        Me.btnOtherDetails_OccupationChanged.ImageList = Me.ImageList1
        Me.btnOtherDetails_OccupationChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnOtherDetails_OccupationChanged.Location = New System.Drawing.Point(889, 94)
        Me.btnOtherDetails_OccupationChanged.Name = "btnOtherDetails_OccupationChanged"
        Me.btnOtherDetails_OccupationChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnOtherDetails_OccupationChanged.TabIndex = 389
        '
        'btnAboutMe_HeadingChanged
        '
        Me.btnAboutMe_HeadingChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnAboutMe_HeadingChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnAboutMe_HeadingChanged.ImageIndex = 0
        Me.btnAboutMe_HeadingChanged.ImageList = Me.ImageList1
        Me.btnAboutMe_HeadingChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnAboutMe_HeadingChanged.Location = New System.Drawing.Point(889, 133)
        Me.btnAboutMe_HeadingChanged.Name = "btnAboutMe_HeadingChanged"
        Me.btnAboutMe_HeadingChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnAboutMe_HeadingChanged.TabIndex = 391
        '
        'icbNetWorth
        '
        Me.icbNetWorth.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "OtherDetails_NetWorthID", True))
        Me.icbNetWorth.Location = New System.Drawing.Point(808, 539)
        Me.icbNetWorth.Name = "icbNetWorth"
        Me.icbNetWorth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbNetWorth.Size = New System.Drawing.Size(133, 20)
        Me.icbNetWorth.TabIndex = 369
        Me.icbNetWorth.Visible = False
        '
        'icbAnnualIncome
        '
        Me.icbAnnualIncome.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "OtherDetails_AnnualIncomeID", True))
        Me.icbAnnualIncome.Location = New System.Drawing.Point(358, 69)
        Me.icbAnnualIncome.Name = "icbAnnualIncome"
        Me.icbAnnualIncome.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbAnnualIncome.Size = New System.Drawing.Size(321, 20)
        Me.icbAnnualIncome.TabIndex = 368
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl34.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl34.AutoEllipsis = True
        Me.LabelControl34.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl34.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl34.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl34.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(947, 32)
        Me.LabelControl34.TabIndex = 278
        Me.LabelControl34.Text = "Εδώ βλέπουμε όλες τις φορές που έκανε login & logout το logout βασίζετε στο end s" & _
    "ession "
        '
        'txOccupation
        '
        Me.txOccupation.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "OtherDetails_Occupation", True))
        Me.txOccupation.Location = New System.Drawing.Point(358, 95)
        Me.txOccupation.Name = "txOccupation"
        Me.txOccupation.Properties.LookAndFeel.SkinName = "Black"
        Me.txOccupation.Size = New System.Drawing.Size(525, 20)
        Me.txOccupation.TabIndex = 360
        '
        'btnAboutMe_DescribeYourselfChanged
        '
        Me.btnAboutMe_DescribeYourselfChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnAboutMe_DescribeYourselfChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnAboutMe_DescribeYourselfChanged.ImageIndex = 0
        Me.btnAboutMe_DescribeYourselfChanged.ImageList = Me.ImageList1
        Me.btnAboutMe_DescribeYourselfChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnAboutMe_DescribeYourselfChanged.Location = New System.Drawing.Point(889, 182)
        Me.btnAboutMe_DescribeYourselfChanged.Name = "btnAboutMe_DescribeYourselfChanged"
        Me.btnAboutMe_DescribeYourselfChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnAboutMe_DescribeYourselfChanged.TabIndex = 390
        '
        'lbOccupation
        '
        Me.lbOccupation.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbOccupation.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbOccupation.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbOccupation.Location = New System.Drawing.Point(58, 99)
        Me.lbOccupation.Name = "lbOccupation"
        Me.lbOccupation.Size = New System.Drawing.Size(294, 13)
        Me.lbOccupation.TabIndex = 357
        Me.lbOccupation.Text = "Occupation :"
        '
        'lbNetWorthID
        '
        Me.lbNetWorthID.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbNetWorthID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbNetWorthID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbNetWorthID.Location = New System.Drawing.Point(808, 520)
        Me.lbNetWorthID.Name = "lbNetWorthID"
        Me.lbNetWorthID.Size = New System.Drawing.Size(82, 13)
        Me.lbNetWorthID.TabIndex = 353
        Me.lbNetWorthID.Text = "Net Worth :"
        Me.lbNetWorthID.Visible = False
        '
        'btnAboutMe_DescribeAnIdealFirstDateChanged
        '
        Me.btnAboutMe_DescribeAnIdealFirstDateChanged.BackgroundImage = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_21
        Me.btnAboutMe_DescribeAnIdealFirstDateChanged.Image = Global.Dating.Client.Admin.APP.My.Resources.Resources.exclamation_2
        Me.btnAboutMe_DescribeAnIdealFirstDateChanged.ImageIndex = 0
        Me.btnAboutMe_DescribeAnIdealFirstDateChanged.ImageList = Me.ImageList1
        Me.btnAboutMe_DescribeAnIdealFirstDateChanged.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.btnAboutMe_DescribeAnIdealFirstDateChanged.Location = New System.Drawing.Point(889, 305)
        Me.btnAboutMe_DescribeAnIdealFirstDateChanged.Name = "btnAboutMe_DescribeAnIdealFirstDateChanged"
        Me.btnAboutMe_DescribeAnIdealFirstDateChanged.Size = New System.Drawing.Size(24, 21)
        Me.btnAboutMe_DescribeAnIdealFirstDateChanged.TabIndex = 389
        '
        'lbAnnualIncomeID
        '
        Me.lbAnnualIncomeID.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbAnnualIncomeID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbAnnualIncomeID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbAnnualIncomeID.Location = New System.Drawing.Point(58, 73)
        Me.lbAnnualIncomeID.Name = "lbAnnualIncomeID"
        Me.lbAnnualIncomeID.Size = New System.Drawing.Size(294, 13)
        Me.lbAnnualIncomeID.TabIndex = 352
        Me.lbAnnualIncomeID.Text = "Annual Income :"
        '
        'lbDescribeYourself
        '
        Me.lbDescribeYourself.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbDescribeYourself.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbDescribeYourself.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbDescribeYourself.Location = New System.Drawing.Point(141, 173)
        Me.lbDescribeYourself.Name = "lbDescribeYourself"
        Me.lbDescribeYourself.Size = New System.Drawing.Size(211, 13)
        Me.lbDescribeYourself.TabIndex = 327
        Me.lbDescribeYourself.Text = "Describe Yourself :"
        '
        'lbEducationID
        '
        Me.lbEducationID.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbEducationID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbEducationID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbEducationID.Location = New System.Drawing.Point(103, 47)
        Me.lbEducationID.Name = "lbEducationID"
        Me.lbEducationID.Size = New System.Drawing.Size(249, 13)
        Me.lbEducationID.TabIndex = 351
        Me.lbEducationID.Text = "Education :"
        '
        'lbHeading
        '
        Me.lbHeading.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbHeading.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbHeading.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbHeading.Location = New System.Drawing.Point(141, 123)
        Me.lbHeading.Name = "lbHeading"
        Me.lbHeading.Size = New System.Drawing.Size(211, 13)
        Me.lbHeading.TabIndex = 328
        Me.lbHeading.Text = "Heading :"
        '
        'icbEducation
        '
        Me.icbEducation.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "OtherDetails_EducationID", True))
        Me.icbEducation.Location = New System.Drawing.Point(358, 43)
        Me.icbEducation.Name = "icbEducation"
        Me.icbEducation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbEducation.Size = New System.Drawing.Size(321, 20)
        Me.icbEducation.TabIndex = 367
        '
        'lbDescribeAnIdealFirstDate
        '
        Me.lbDescribeAnIdealFirstDate.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbDescribeAnIdealFirstDate.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbDescribeAnIdealFirstDate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbDescribeAnIdealFirstDate.Location = New System.Drawing.Point(182, 269)
        Me.lbDescribeAnIdealFirstDate.Name = "lbDescribeAnIdealFirstDate"
        Me.lbDescribeAnIdealFirstDate.Size = New System.Drawing.Size(170, 13)
        Me.lbDescribeAnIdealFirstDate.TabIndex = 331
        Me.lbDescribeAnIdealFirstDate.Text = "Describe An Ideal First Date :"
        '
        'TabPagePersonal
        '
        Me.TabPagePersonal.Controls.Add(Me.LabelControl5)
        Me.TabPagePersonal.Controls.Add(Me.LabelControl35)
        Me.TabPagePersonal.Controls.Add(Me.icbDrinkingHabit)
        Me.TabPagePersonal.Controls.Add(Me.icbEthnicity)
        Me.TabPagePersonal.Controls.Add(Me.icbSmokingHabit)
        Me.TabPagePersonal.Controls.Add(Me.lbBodyTypeID)
        Me.TabPagePersonal.Controls.Add(Me.icbReligion)
        Me.TabPagePersonal.Controls.Add(Me.lbHairColorID)
        Me.TabPagePersonal.Controls.Add(Me.lbHeightID)
        Me.TabPagePersonal.Controls.Add(Me.icbChildrenID)
        Me.TabPagePersonal.Controls.Add(Me.lbEyeColorID)
        Me.TabPagePersonal.Controls.Add(Me.icbHairColorID)
        Me.TabPagePersonal.Controls.Add(Me.lbChildrenID)
        Me.TabPagePersonal.Controls.Add(Me.icbEyeColorID)
        Me.TabPagePersonal.Controls.Add(Me.LabelControl11)
        Me.TabPagePersonal.Controls.Add(Me.icbBodyTypeID)
        Me.TabPagePersonal.Controls.Add(Me.LabelControl9)
        Me.TabPagePersonal.Controls.Add(Me.icbHeightID)
        Me.TabPagePersonal.Controls.Add(Me.LabelControl3)
        Me.TabPagePersonal.ImageIndex = 404
        Me.TabPagePersonal.Name = "TabPagePersonal"
        Me.TabPagePersonal.Size = New System.Drawing.Size(947, 527)
        Me.TabPagePersonal.Text = "Personal"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl5.Location = New System.Drawing.Point(119, 263)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(170, 13)
        Me.LabelControl5.TabIndex = 377
        Me.LabelControl5.Text = "Drinking Habit :"
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl35.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl35.AutoEllipsis = True
        Me.LabelControl35.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl35.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl35.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl35.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(947, 32)
        Me.LabelControl35.TabIndex = 278
        Me.LabelControl35.Text = "Εδώ βλέπουμε όλες τις φορές που έκανε login & logout το logout βασίζετε στο end s" & _
    "ession "
        '
        'icbDrinkingHabit
        '
        Me.icbDrinkingHabit.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PersonalInfo_DrinkingHabitID", True))
        Me.icbDrinkingHabit.Location = New System.Drawing.Point(295, 256)
        Me.icbDrinkingHabit.Name = "icbDrinkingHabit"
        Me.icbDrinkingHabit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbDrinkingHabit.Size = New System.Drawing.Size(321, 20)
        Me.icbDrinkingHabit.TabIndex = 376
        '
        'icbEthnicity
        '
        Me.icbEthnicity.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PersonalInfo_EthnicityID", True))
        Me.icbEthnicity.Location = New System.Drawing.Point(295, 178)
        Me.icbEthnicity.Name = "icbEthnicity"
        Me.icbEthnicity.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbEthnicity.Size = New System.Drawing.Size(321, 20)
        Me.icbEthnicity.TabIndex = 373
        '
        'icbSmokingHabit
        '
        Me.icbSmokingHabit.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PersonalInfo_SmokingHabitID", True))
        Me.icbSmokingHabit.Location = New System.Drawing.Point(295, 230)
        Me.icbSmokingHabit.Name = "icbSmokingHabit"
        Me.icbSmokingHabit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbSmokingHabit.Size = New System.Drawing.Size(321, 20)
        Me.icbSmokingHabit.TabIndex = 375
        '
        'lbBodyTypeID
        '
        Me.lbBodyTypeID.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbBodyTypeID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbBodyTypeID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbBodyTypeID.Location = New System.Drawing.Point(78, 81)
        Me.lbBodyTypeID.Name = "lbBodyTypeID"
        Me.lbBodyTypeID.Size = New System.Drawing.Size(211, 13)
        Me.lbBodyTypeID.TabIndex = 317
        Me.lbBodyTypeID.Text = "Body Type :"
        '
        'icbReligion
        '
        Me.icbReligion.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PersonalInfo_ReligionID", True))
        Me.icbReligion.Location = New System.Drawing.Point(295, 204)
        Me.icbReligion.Name = "icbReligion"
        Me.icbReligion.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbReligion.Size = New System.Drawing.Size(321, 20)
        Me.icbReligion.TabIndex = 374
        '
        'lbHairColorID
        '
        Me.lbHairColorID.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbHairColorID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbHairColorID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbHairColorID.Location = New System.Drawing.Point(119, 133)
        Me.lbHairColorID.Name = "lbHairColorID"
        Me.lbHairColorID.Size = New System.Drawing.Size(170, 13)
        Me.lbHairColorID.TabIndex = 319
        Me.lbHairColorID.Text = "Hair Color :"
        '
        'lbHeightID
        '
        Me.lbHeightID.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbHeightID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbHeightID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbHeightID.Location = New System.Drawing.Point(78, 55)
        Me.lbHeightID.Name = "lbHeightID"
        Me.lbHeightID.Size = New System.Drawing.Size(211, 13)
        Me.lbHeightID.TabIndex = 318
        Me.lbHeightID.Text = "Height :"
        '
        'icbChildrenID
        '
        Me.icbChildrenID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PersonalInfo_ChildrenID", True))
        Me.icbChildrenID.Location = New System.Drawing.Point(295, 152)
        Me.icbChildrenID.Name = "icbChildrenID"
        Me.icbChildrenID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbChildrenID.Size = New System.Drawing.Size(321, 20)
        Me.icbChildrenID.TabIndex = 372
        '
        'lbEyeColorID
        '
        Me.lbEyeColorID.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbEyeColorID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbEyeColorID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbEyeColorID.Location = New System.Drawing.Point(119, 107)
        Me.lbEyeColorID.Name = "lbEyeColorID"
        Me.lbEyeColorID.Size = New System.Drawing.Size(170, 13)
        Me.lbEyeColorID.TabIndex = 320
        Me.lbEyeColorID.Text = "Eye Color :"
        '
        'icbHairColorID
        '
        Me.icbHairColorID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PersonalInfo_HairColorID", True))
        Me.icbHairColorID.Location = New System.Drawing.Point(295, 126)
        Me.icbHairColorID.Name = "icbHairColorID"
        Me.icbHairColorID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbHairColorID.Size = New System.Drawing.Size(321, 20)
        Me.icbHairColorID.TabIndex = 371
        '
        'lbChildrenID
        '
        Me.lbChildrenID.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lbChildrenID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lbChildrenID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbChildrenID.Location = New System.Drawing.Point(119, 159)
        Me.lbChildrenID.Name = "lbChildrenID"
        Me.lbChildrenID.Size = New System.Drawing.Size(170, 13)
        Me.lbChildrenID.TabIndex = 323
        Me.lbChildrenID.Text = "Children :"
        '
        'icbEyeColorID
        '
        Me.icbEyeColorID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PersonalInfo_EyeColorID", True))
        Me.icbEyeColorID.Location = New System.Drawing.Point(295, 100)
        Me.icbEyeColorID.Name = "icbEyeColorID"
        Me.icbEyeColorID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbEyeColorID.Size = New System.Drawing.Size(321, 20)
        Me.icbEyeColorID.TabIndex = 370
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl11.Location = New System.Drawing.Point(119, 185)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(170, 13)
        Me.LabelControl11.TabIndex = 324
        Me.LabelControl11.Text = "Ethnicity :"
        '
        'icbBodyTypeID
        '
        Me.icbBodyTypeID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PersonalInfo_BodyTypeID", True))
        Me.icbBodyTypeID.Location = New System.Drawing.Point(295, 74)
        Me.icbBodyTypeID.Name = "icbBodyTypeID"
        Me.icbBodyTypeID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbBodyTypeID.Size = New System.Drawing.Size(321, 20)
        Me.icbBodyTypeID.TabIndex = 369
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl9.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl9.Location = New System.Drawing.Point(119, 211)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(170, 13)
        Me.LabelControl9.TabIndex = 329
        Me.LabelControl9.Text = "Religion :"
        '
        'icbHeightID
        '
        Me.icbHeightID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PersonalInfo_HeightID", True))
        Me.icbHeightID.Location = New System.Drawing.Point(295, 48)
        Me.icbHeightID.Name = "icbHeightID"
        Me.icbHeightID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbHeightID.Size = New System.Drawing.Size(321, 20)
        Me.icbHeightID.TabIndex = 368
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(119, 237)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(170, 13)
        Me.LabelControl3.TabIndex = 330
        Me.LabelControl3.Text = "Smoking Habit :"
        '
        'TabPageLookingFor
        '
        Me.TabPageLookingFor.Controls.Add(Me.icbRelationshipStatusID)
        Me.TabPageLookingFor.Controls.Add(Me.LabelControl36)
        Me.TabPageLookingFor.Controls.Add(Me.chkMarriedDating)
        Me.TabPageLookingFor.Controls.Add(Me.LabelControl18)
        Me.TabPageLookingFor.Controls.Add(Me.LabelControl21)
        Me.TabPageLookingFor.Controls.Add(Me.LabelControl12)
        Me.TabPageLookingFor.Controls.Add(Me.chkAdultDating_Casual)
        Me.TabPageLookingFor.Controls.Add(Me.LabelControl13)
        Me.TabPageLookingFor.Controls.Add(Me.LabelControl20)
        Me.TabPageLookingFor.Controls.Add(Me.LabelControl14)
        Me.TabPageLookingFor.Controls.Add(Me.chkMutuallyBeneficialArrangements)
        Me.TabPageLookingFor.Controls.Add(Me.chkToMeetMaleID)
        Me.TabPageLookingFor.Controls.Add(Me.chkLongTermRelationship)
        Me.TabPageLookingFor.Controls.Add(Me.chkToMeetFemaleID)
        Me.TabPageLookingFor.Controls.Add(Me.LabelControl17)
        Me.TabPageLookingFor.Controls.Add(Me.LabelControl16)
        Me.TabPageLookingFor.Controls.Add(Me.LabelControl15)
        Me.TabPageLookingFor.Controls.Add(Me.chkFriendship)
        Me.TabPageLookingFor.Controls.Add(Me.chkShortTermRelationship)
        Me.TabPageLookingFor.ImageIndex = 420
        Me.TabPageLookingFor.Name = "TabPageLookingFor"
        Me.TabPageLookingFor.Size = New System.Drawing.Size(947, 527)
        Me.TabPageLookingFor.Text = "Looking For"
        '
        'icbRelationshipStatusID
        '
        Me.icbRelationshipStatusID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LookingFor_RelationshipStatusID", True))
        Me.icbRelationshipStatusID.Location = New System.Drawing.Point(373, 93)
        Me.icbRelationshipStatusID.Name = "icbRelationshipStatusID"
        Me.icbRelationshipStatusID.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.icbRelationshipStatusID.Size = New System.Drawing.Size(321, 20)
        Me.icbRelationshipStatusID.TabIndex = 384
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl36.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl36.AutoEllipsis = True
        Me.LabelControl36.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl36.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl36.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl36.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(947, 32)
        Me.LabelControl36.TabIndex = 278
        Me.LabelControl36.Text = "Εδώ βλέπουμε όλες τις φορές που έκανε login & logout το logout βασίζετε στο end s" & _
    "ession "
        '
        'chkMarriedDating
        '
        Me.chkMarriedDating.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LookingFor_TypeOfDating_MarriedDating", True))
        Me.chkMarriedDating.Location = New System.Drawing.Point(371, 217)
        Me.chkMarriedDating.Name = "chkMarriedDating"
        Me.chkMarriedDating.Properties.Caption = ""
        Me.chkMarriedDating.Size = New System.Drawing.Size(77, 19)
        Me.chkMarriedDating.TabIndex = 381
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl18.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl18.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl18.Location = New System.Drawing.Point(73, 172)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl18.TabIndex = 374
        Me.LabelControl18.Text = "Long Term Relationship :"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl21.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl21.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl21.Location = New System.Drawing.Point(71, 220)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl21.TabIndex = 380
        Me.LabelControl21.Text = "Married Dating :"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl12.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl12.Location = New System.Drawing.Point(71, 48)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl12.TabIndex = 361
        Me.LabelControl12.Text = "Meet Male :"
        '
        'chkAdultDating_Casual
        '
        Me.chkAdultDating_Casual.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LookingFor_TypeOfDating_AdultDating_Casual", True))
        Me.chkAdultDating_Casual.Location = New System.Drawing.Point(371, 240)
        Me.chkAdultDating_Casual.Name = "chkAdultDating_Casual"
        Me.chkAdultDating_Casual.Properties.Caption = ""
        Me.chkAdultDating_Casual.Size = New System.Drawing.Size(77, 19)
        Me.chkAdultDating_Casual.TabIndex = 379
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl13.Location = New System.Drawing.Point(71, 71)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl13.TabIndex = 363
        Me.LabelControl13.Text = "Meet Female :"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl20.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl20.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl20.Location = New System.Drawing.Point(71, 244)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(294, 10)
        Me.LabelControl20.TabIndex = 378
        Me.LabelControl20.Text = "Adult Dating - Casual :"
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl14.Location = New System.Drawing.Point(73, 100)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl14.TabIndex = 365
        Me.LabelControl14.Text = "Relationship Status :"
        '
        'chkMutuallyBeneficialArrangements
        '
        Me.chkMutuallyBeneficialArrangements.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LookingFor_TypeOfDating_MutuallyBeneficialArrangements", True))
        Me.chkMutuallyBeneficialArrangements.Location = New System.Drawing.Point(371, 194)
        Me.chkMutuallyBeneficialArrangements.Name = "chkMutuallyBeneficialArrangements"
        Me.chkMutuallyBeneficialArrangements.Properties.Caption = ""
        Me.chkMutuallyBeneficialArrangements.Size = New System.Drawing.Size(77, 19)
        Me.chkMutuallyBeneficialArrangements.TabIndex = 377
        '
        'chkToMeetMaleID
        '
        Me.chkToMeetMaleID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LookingFor_ToMeetMaleID", True))
        Me.chkToMeetMaleID.Location = New System.Drawing.Point(371, 45)
        Me.chkToMeetMaleID.Name = "chkToMeetMaleID"
        Me.chkToMeetMaleID.Properties.Caption = ""
        Me.chkToMeetMaleID.Size = New System.Drawing.Size(77, 19)
        Me.chkToMeetMaleID.TabIndex = 367
        '
        'chkLongTermRelationship
        '
        Me.chkLongTermRelationship.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LookingFor_TypeOfDating_LongTermRelationship", True))
        Me.chkLongTermRelationship.Location = New System.Drawing.Point(371, 169)
        Me.chkLongTermRelationship.Name = "chkLongTermRelationship"
        Me.chkLongTermRelationship.Properties.Caption = ""
        Me.chkLongTermRelationship.Size = New System.Drawing.Size(77, 19)
        Me.chkLongTermRelationship.TabIndex = 376
        '
        'chkToMeetFemaleID
        '
        Me.chkToMeetFemaleID.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LookingFor_ToMeetFemaleID", True))
        Me.chkToMeetFemaleID.Location = New System.Drawing.Point(371, 68)
        Me.chkToMeetFemaleID.Name = "chkToMeetFemaleID"
        Me.chkToMeetFemaleID.Properties.Caption = ""
        Me.chkToMeetFemaleID.Size = New System.Drawing.Size(77, 19)
        Me.chkToMeetFemaleID.TabIndex = 368
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl17.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl17.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl17.Location = New System.Drawing.Point(71, 197)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl17.TabIndex = 375
        Me.LabelControl17.Text = "Mutually Beneficial Arrangements :"
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl16.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl16.Location = New System.Drawing.Point(71, 122)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl16.TabIndex = 370
        Me.LabelControl16.Text = "Short Term Relationship :"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl15.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl15.Location = New System.Drawing.Point(71, 147)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl15.TabIndex = 371
        Me.LabelControl15.Text = "Friendship :"
        '
        'chkFriendship
        '
        Me.chkFriendship.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LookingFor_TypeOfDating_Friendship", True))
        Me.chkFriendship.Location = New System.Drawing.Point(371, 144)
        Me.chkFriendship.Name = "chkFriendship"
        Me.chkFriendship.Properties.Caption = ""
        Me.chkFriendship.Size = New System.Drawing.Size(77, 19)
        Me.chkFriendship.TabIndex = 373
        '
        'chkShortTermRelationship
        '
        Me.chkShortTermRelationship.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "LookingFor_TypeOfDating_ShortTermRelationship", True))
        Me.chkShortTermRelationship.Location = New System.Drawing.Point(371, 119)
        Me.chkShortTermRelationship.Name = "chkShortTermRelationship"
        Me.chkShortTermRelationship.Properties.Caption = ""
        Me.chkShortTermRelationship.Size = New System.Drawing.Size(77, 19)
        Me.chkShortTermRelationship.TabIndex = 372
        '
        'TabPageSettings
        '
        Me.TabPageSettings.Controls.Add(Me.chkNotificationsSettings_WhenNewMembersNearMe)
        Me.TabPageSettings.Controls.Add(Me.lblNotificationsSettings_WhenNewMembersNearMe)
        Me.TabPageSettings.Controls.Add(Me.chkNotificationsSettings_WhenNewOfferReceived)
        Me.TabPageSettings.Controls.Add(Me.lblNotificationsSettings_WhenNewOfferReceived)
        Me.TabPageSettings.Controls.Add(Me.chkNotificationsSettings_WhenLikeReceived)
        Me.TabPageSettings.Controls.Add(Me.lblNotificationsSettings_WhenLikeReceived)
        Me.TabPageSettings.Controls.Add(Me.chkNotificationsSettings_WhenNewMessageReceived)
        Me.TabPageSettings.Controls.Add(Me.lblNotificationsSettings_WhenNewMessageReceived)
        Me.TabPageSettings.Controls.Add(Me.chkHideMyLastLoginDateTime)
        Me.TabPageSettings.Controls.Add(Me.LabelControl37)
        Me.TabPageSettings.Controls.Add(Me.LabelControl27)
        Me.TabPageSettings.Controls.Add(Me.LabelControl22)
        Me.TabPageSettings.Controls.Add(Me.chkNotShowMyGifts)
        Me.TabPageSettings.Controls.Add(Me.LabelControl26)
        Me.TabPageSettings.Controls.Add(Me.LabelControl25)
        Me.TabPageSettings.Controls.Add(Me.chkNotShowInOtherUsersViewedList)
        Me.TabPageSettings.Controls.Add(Me.chkHideMeFromSearchResults)
        Me.TabPageSettings.Controls.Add(Me.chkNotShowInOtherUsersFavoritedList)
        Me.TabPageSettings.Controls.Add(Me.chkHideMeFromMembersIHaveBlocked)
        Me.TabPageSettings.Controls.Add(Me.LabelControl23)
        Me.TabPageSettings.Controls.Add(Me.LabelControl24)
        Me.TabPageSettings.ImageIndex = 412
        Me.TabPageSettings.Name = "TabPageSettings"
        Me.TabPageSettings.Size = New System.Drawing.Size(947, 527)
        Me.TabPageSettings.Text = "Settings"
        '
        'chkNotificationsSettings_WhenNewMembersNearMe
        '
        Me.chkNotificationsSettings_WhenNewMembersNearMe.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "NotificationsSettings_WhenNewMembersNearMe", True))
        Me.chkNotificationsSettings_WhenNewMembersNearMe.Location = New System.Drawing.Point(413, 271)
        Me.chkNotificationsSettings_WhenNewMembersNearMe.Name = "chkNotificationsSettings_WhenNewMembersNearMe"
        Me.chkNotificationsSettings_WhenNewMembersNearMe.Properties.Caption = ""
        Me.chkNotificationsSettings_WhenNewMembersNearMe.Size = New System.Drawing.Size(77, 19)
        Me.chkNotificationsSettings_WhenNewMembersNearMe.TabIndex = 396
        '
        'lblNotificationsSettings_WhenNewMembersNearMe
        '
        Me.lblNotificationsSettings_WhenNewMembersNearMe.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblNotificationsSettings_WhenNewMembersNearMe.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblNotificationsSettings_WhenNewMembersNearMe.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblNotificationsSettings_WhenNewMembersNearMe.Location = New System.Drawing.Point(8, 273)
        Me.lblNotificationsSettings_WhenNewMembersNearMe.Name = "lblNotificationsSettings_WhenNewMembersNearMe"
        Me.lblNotificationsSettings_WhenNewMembersNearMe.Size = New System.Drawing.Size(399, 13)
        Me.lblNotificationsSettings_WhenNewMembersNearMe.TabIndex = 395
        Me.lblNotificationsSettings_WhenNewMembersNearMe.Text = "Notify me when new member registered and fills my criterias :"
        '
        'chkNotificationsSettings_WhenNewOfferReceived
        '
        Me.chkNotificationsSettings_WhenNewOfferReceived.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "NotificationsSettings_WhenNewOfferReceived", True))
        Me.chkNotificationsSettings_WhenNewOfferReceived.Location = New System.Drawing.Point(413, 246)
        Me.chkNotificationsSettings_WhenNewOfferReceived.Name = "chkNotificationsSettings_WhenNewOfferReceived"
        Me.chkNotificationsSettings_WhenNewOfferReceived.Properties.Caption = ""
        Me.chkNotificationsSettings_WhenNewOfferReceived.Size = New System.Drawing.Size(77, 19)
        Me.chkNotificationsSettings_WhenNewOfferReceived.TabIndex = 394
        '
        'lblNotificationsSettings_WhenNewOfferReceived
        '
        Me.lblNotificationsSettings_WhenNewOfferReceived.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblNotificationsSettings_WhenNewOfferReceived.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblNotificationsSettings_WhenNewOfferReceived.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblNotificationsSettings_WhenNewOfferReceived.Location = New System.Drawing.Point(113, 248)
        Me.lblNotificationsSettings_WhenNewOfferReceived.Name = "lblNotificationsSettings_WhenNewOfferReceived"
        Me.lblNotificationsSettings_WhenNewOfferReceived.Size = New System.Drawing.Size(294, 13)
        Me.lblNotificationsSettings_WhenNewOfferReceived.TabIndex = 393
        Me.lblNotificationsSettings_WhenNewOfferReceived.Text = "Notify me when new offer received :"
        '
        'chkNotificationsSettings_WhenLikeReceived
        '
        Me.chkNotificationsSettings_WhenLikeReceived.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "NotificationsSettings_WhenLikeReceived", True))
        Me.chkNotificationsSettings_WhenLikeReceived.Location = New System.Drawing.Point(413, 221)
        Me.chkNotificationsSettings_WhenLikeReceived.Name = "chkNotificationsSettings_WhenLikeReceived"
        Me.chkNotificationsSettings_WhenLikeReceived.Properties.Caption = ""
        Me.chkNotificationsSettings_WhenLikeReceived.Size = New System.Drawing.Size(77, 19)
        Me.chkNotificationsSettings_WhenLikeReceived.TabIndex = 392
        '
        'lblNotificationsSettings_WhenLikeReceived
        '
        Me.lblNotificationsSettings_WhenLikeReceived.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblNotificationsSettings_WhenLikeReceived.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblNotificationsSettings_WhenLikeReceived.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblNotificationsSettings_WhenLikeReceived.Location = New System.Drawing.Point(113, 223)
        Me.lblNotificationsSettings_WhenLikeReceived.Name = "lblNotificationsSettings_WhenLikeReceived"
        Me.lblNotificationsSettings_WhenLikeReceived.Size = New System.Drawing.Size(294, 13)
        Me.lblNotificationsSettings_WhenLikeReceived.TabIndex = 391
        Me.lblNotificationsSettings_WhenLikeReceived.Text = "Notify me when like received :"
        '
        'chkNotificationsSettings_WhenNewMessageReceived
        '
        Me.chkNotificationsSettings_WhenNewMessageReceived.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "NotificationsSettings_WhenNewMessageReceived", True))
        Me.chkNotificationsSettings_WhenNewMessageReceived.Location = New System.Drawing.Point(413, 198)
        Me.chkNotificationsSettings_WhenNewMessageReceived.Name = "chkNotificationsSettings_WhenNewMessageReceived"
        Me.chkNotificationsSettings_WhenNewMessageReceived.Properties.Caption = ""
        Me.chkNotificationsSettings_WhenNewMessageReceived.Size = New System.Drawing.Size(77, 19)
        Me.chkNotificationsSettings_WhenNewMessageReceived.TabIndex = 390
        '
        'lblNotificationsSettings_WhenNewMessageReceived
        '
        Me.lblNotificationsSettings_WhenNewMessageReceived.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblNotificationsSettings_WhenNewMessageReceived.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.lblNotificationsSettings_WhenNewMessageReceived.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lblNotificationsSettings_WhenNewMessageReceived.Location = New System.Drawing.Point(113, 200)
        Me.lblNotificationsSettings_WhenNewMessageReceived.Name = "lblNotificationsSettings_WhenNewMessageReceived"
        Me.lblNotificationsSettings_WhenNewMessageReceived.Size = New System.Drawing.Size(294, 13)
        Me.lblNotificationsSettings_WhenNewMessageReceived.TabIndex = 389
        Me.lblNotificationsSettings_WhenNewMessageReceived.Text = "Notify me when new message received :"
        '
        'chkHideMyLastLoginDateTime
        '
        Me.chkHideMyLastLoginDateTime.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PrivacySettings_HideMyLastLoginDateTime", True))
        Me.chkHideMyLastLoginDateTime.Location = New System.Drawing.Point(413, 173)
        Me.chkHideMyLastLoginDateTime.Name = "chkHideMyLastLoginDateTime"
        Me.chkHideMyLastLoginDateTime.Properties.Caption = ""
        Me.chkHideMyLastLoginDateTime.Size = New System.Drawing.Size(77, 19)
        Me.chkHideMyLastLoginDateTime.TabIndex = 388
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl37.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl37.AutoEllipsis = True
        Me.LabelControl37.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl37.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl37.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl37.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(947, 32)
        Me.LabelControl37.TabIndex = 278
        Me.LabelControl37.Text = "Εδώ βλέπουμε όλες τις φορές που έκανε login & logout το logout βασίζετε στο end s" & _
    "ession "
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl27.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl27.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl27.Location = New System.Drawing.Point(113, 175)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl27.TabIndex = 387
        Me.LabelControl27.Text = "Hide My Last Login DateTime :"
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl22.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl22.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl22.Location = New System.Drawing.Point(113, 152)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl22.TabIndex = 385
        Me.LabelControl22.Text = "Not Show My Gifts :"
        '
        'chkNotShowMyGifts
        '
        Me.chkNotShowMyGifts.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PrivacySettings_NotShowMyGifts", True))
        Me.chkNotShowMyGifts.Location = New System.Drawing.Point(413, 150)
        Me.chkNotShowMyGifts.Name = "chkNotShowMyGifts"
        Me.chkNotShowMyGifts.Properties.Caption = ""
        Me.chkNotShowMyGifts.Size = New System.Drawing.Size(77, 19)
        Me.chkNotShowMyGifts.TabIndex = 386
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl26.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl26.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl26.Location = New System.Drawing.Point(113, 53)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl26.TabIndex = 377
        Me.LabelControl26.Text = "Hide Me From Search Results :"
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl25.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl25.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl25.Location = New System.Drawing.Point(113, 78)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl25.TabIndex = 378
        Me.LabelControl25.Text = "Hide Me From Members I Have Blocked :"
        '
        'chkNotShowInOtherUsersViewedList
        '
        Me.chkNotShowInOtherUsersViewedList.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PrivacySettings_NotShowInOtherUsersViewedList", True))
        Me.chkNotShowInOtherUsersViewedList.Location = New System.Drawing.Point(413, 125)
        Me.chkNotShowInOtherUsersViewedList.Name = "chkNotShowInOtherUsersViewedList"
        Me.chkNotShowInOtherUsersViewedList.Properties.Caption = ""
        Me.chkNotShowInOtherUsersViewedList.Size = New System.Drawing.Size(77, 19)
        Me.chkNotShowInOtherUsersViewedList.TabIndex = 384
        '
        'chkHideMeFromSearchResults
        '
        Me.chkHideMeFromSearchResults.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PrivacySettings_HideMeFromSearchResults", True))
        Me.chkHideMeFromSearchResults.Location = New System.Drawing.Point(413, 50)
        Me.chkHideMeFromSearchResults.Name = "chkHideMeFromSearchResults"
        Me.chkHideMeFromSearchResults.Properties.Caption = ""
        Me.chkHideMeFromSearchResults.Size = New System.Drawing.Size(77, 19)
        Me.chkHideMeFromSearchResults.TabIndex = 379
        '
        'chkNotShowInOtherUsersFavoritedList
        '
        Me.chkNotShowInOtherUsersFavoritedList.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PrivacySettings_NotShowInOtherUsersFavoritedList", True))
        Me.chkNotShowInOtherUsersFavoritedList.Location = New System.Drawing.Point(413, 100)
        Me.chkNotShowInOtherUsersFavoritedList.Name = "chkNotShowInOtherUsersFavoritedList"
        Me.chkNotShowInOtherUsersFavoritedList.Properties.Caption = ""
        Me.chkNotShowInOtherUsersFavoritedList.Size = New System.Drawing.Size(77, 19)
        Me.chkNotShowInOtherUsersFavoritedList.TabIndex = 383
        '
        'chkHideMeFromMembersIHaveBlocked
        '
        Me.chkHideMeFromMembersIHaveBlocked.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.EUS_ProfilesBindingSource, "PrivacySettings_HideMeFromMembersIHaveBlocked", True))
        Me.chkHideMeFromMembersIHaveBlocked.Location = New System.Drawing.Point(413, 75)
        Me.chkHideMeFromMembersIHaveBlocked.Name = "chkHideMeFromMembersIHaveBlocked"
        Me.chkHideMeFromMembersIHaveBlocked.Properties.Caption = ""
        Me.chkHideMeFromMembersIHaveBlocked.Size = New System.Drawing.Size(77, 19)
        Me.chkHideMeFromMembersIHaveBlocked.TabIndex = 380
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl23.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl23.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl23.Location = New System.Drawing.Point(113, 127)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl23.TabIndex = 382
        Me.LabelControl23.Text = "Not Show In Other Users Viewed List :"
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl24.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl24.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl24.Location = New System.Drawing.Point(113, 102)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(294, 13)
        Me.LabelControl24.TabIndex = 381
        Me.LabelControl24.Text = "Not Show In Other Users Favorited List :"
        '
        'TABPhotosCarousel
        '
        Me.TABPhotosCarousel.Controls.Add(Me.ucLayout3D1)
        Me.TABPhotosCarousel.ImageIndex = 39
        Me.TABPhotosCarousel.Name = "TABPhotosCarousel"
        Me.TABPhotosCarousel.Size = New System.Drawing.Size(1049, 528)
        Me.TABPhotosCarousel.Text = "Photos"
        '
        'ucLayout3D1
        '
        Me.ucLayout3D1.DefaultView = Dating.Server.Core.DLL.PhotosFilterEnum.None
        Me.ucLayout3D1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ucLayout3D1.Location = New System.Drawing.Point(0, 0)
        Me.ucLayout3D1.Name = "ucLayout3D1"
        Me.ucLayout3D1.ShowEditProfileButton = False
        Me.ucLayout3D1.Size = New System.Drawing.Size(1049, 528)
        Me.ucLayout3D1.TabIndex = 0
        '
        'TABLogins
        '
        Me.TABLogins.Controls.Add(Me.gcLoginHistory)
        Me.TABLogins.Controls.Add(Me.LabelControl1)
        Me.TABLogins.ImageIndex = 256
        Me.TABLogins.Name = "TABLogins"
        Me.TABLogins.Size = New System.Drawing.Size(1049, 528)
        Me.TABLogins.Text = "Logins"
        '
        'gcLoginHistory
        '
        Me.gcLoginHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gcLoginHistory.Controls.Add(Me.UcGridLoginHistory)
        Me.gcLoginHistory.Location = New System.Drawing.Point(-1, 30)
        Me.gcLoginHistory.Name = "gcLoginHistory"
        Me.gcLoginHistory.Size = New System.Drawing.Size(1049, 499)
        Me.gcLoginHistory.TabIndex = 305
        Me.gcLoginHistory.Text = "Login History"
        '
        'UcGridLoginHistory
        '
        Me.UcGridLoginHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcGridLoginHistory.IsLoaded = False
        Me.UcGridLoginHistory.Location = New System.Drawing.Point(2, 21)
        Me.UcGridLoginHistory.Name = "UcGridLoginHistory"
        Me.UcGridLoginHistory.Size = New System.Drawing.Size(1045, 476)
        Me.UcGridLoginHistory.TabIndex = 0
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl1.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl1.AutoEllipsis = True
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl1.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(1049, 32)
        Me.LabelControl1.TabIndex = 275
        Me.LabelControl1.Text = "  Εδώ βλέπουμε όλες τις φορές που έκανε login & logout το logout βασίζετε στο end" & _
    " session "
        '
        'TABCredits
        '
        Me.TABCredits.Controls.Add(Me.gcCreditsHistory)
        Me.TABCredits.Controls.Add(Me.txtAllCreditsExpDate)
        Me.TABCredits.Controls.Add(Me.LabelControl32)
        Me.TABCredits.Controls.Add(Me.txtAvailableCredits)
        Me.TABCredits.Controls.Add(Me.LabelControl31)
        Me.TABCredits.Controls.Add(Me.LabelControl29)
        Me.TABCredits.ImageIndex = 147
        Me.TABCredits.Name = "TABCredits"
        Me.TABCredits.Size = New System.Drawing.Size(1049, 528)
        Me.TABCredits.Text = "Credits"
        '
        'gcCreditsHistory
        '
        Me.gcCreditsHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gcCreditsHistory.Controls.Add(Me.UcGridCreditsHistory)
        Me.gcCreditsHistory.Location = New System.Drawing.Point(5, 38)
        Me.gcCreditsHistory.Name = "gcCreditsHistory"
        Me.gcCreditsHistory.Size = New System.Drawing.Size(1041, 433)
        Me.gcCreditsHistory.TabIndex = 304
        Me.gcCreditsHistory.Text = "Credits History"
        '
        'UcGridCreditsHistory
        '
        Me.UcGridCreditsHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcGridCreditsHistory.IsLoaded = False
        Me.UcGridCreditsHistory.Location = New System.Drawing.Point(2, 21)
        Me.UcGridCreditsHistory.Name = "UcGridCreditsHistory"
        Me.UcGridCreditsHistory.Size = New System.Drawing.Size(1037, 410)
        Me.UcGridCreditsHistory.TabIndex = 0
        '
        'txtAllCreditsExpDate
        '
        Me.txtAllCreditsExpDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtAllCreditsExpDate.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.CustomerAvailableCreditsBindingSource, "maxValidExpirationDate", True))
        Me.txtAllCreditsExpDate.Location = New System.Drawing.Point(858, 477)
        Me.txtAllCreditsExpDate.Name = "txtAllCreditsExpDate"
        Me.txtAllCreditsExpDate.Properties.Appearance.BackColor = System.Drawing.Color.LightPink
        Me.txtAllCreditsExpDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.txtAllCreditsExpDate.Properties.Appearance.Options.UseBackColor = True
        Me.txtAllCreditsExpDate.Properties.Appearance.Options.UseFont = True
        Me.txtAllCreditsExpDate.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtAllCreditsExpDate.Properties.DisplayFormat.FormatString = "dd/MM/yyyy HH:mm"
        Me.txtAllCreditsExpDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.txtAllCreditsExpDate.Properties.LookAndFeel.SkinName = "Black"
        Me.txtAllCreditsExpDate.Size = New System.Drawing.Size(183, 26)
        Me.txtAllCreditsExpDate.TabIndex = 303
        '
        'CustomerAvailableCreditsBindingSource
        '
        Me.CustomerAvailableCreditsBindingSource.DataMember = "CustomerAvailableCredits"
        Me.CustomerAvailableCreditsBindingSource.DataSource = GetType(Dating.Client.Admin.APP.AdminWS.DSMembers)
        '
        'LabelControl32
        '
        Me.LabelControl32.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl32.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl32.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl32.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl32.Location = New System.Drawing.Point(683, 485)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(169, 13)
        Me.LabelControl32.TabIndex = 302
        Me.LabelControl32.Text = "All credits expire date:"
        '
        'txtAvailableCredits
        '
        Me.txtAvailableCredits.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtAvailableCredits.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.CustomerAvailableCreditsBindingSource, "AvailableCredits", True))
        Me.txtAvailableCredits.EditValue = "3000"
        Me.txtAvailableCredits.Location = New System.Drawing.Point(204, 477)
        Me.txtAvailableCredits.Name = "txtAvailableCredits"
        Me.txtAvailableCredits.Properties.Appearance.BackColor = System.Drawing.Color.PaleGreen
        Me.txtAvailableCredits.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.txtAvailableCredits.Properties.Appearance.Options.UseBackColor = True
        Me.txtAvailableCredits.Properties.Appearance.Options.UseFont = True
        Me.txtAvailableCredits.Properties.Appearance.Options.UseTextOptions = True
        Me.txtAvailableCredits.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.txtAvailableCredits.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.txtAvailableCredits.Properties.LookAndFeel.SkinName = "Black"
        Me.txtAvailableCredits.Size = New System.Drawing.Size(99, 26)
        Me.txtAvailableCredits.TabIndex = 299
        '
        'LabelControl31
        '
        Me.LabelControl31.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelControl31.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.LabelControl31.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl31.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl31.Location = New System.Drawing.Point(91, 485)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(106, 13)
        Me.LabelControl31.TabIndex = 298
        Me.LabelControl31.Text = "Available credits :"
        '
        'LabelControl29
        '
        Me.LabelControl29.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl29.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl29.AutoEllipsis = True
        Me.LabelControl29.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl29.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl29.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl29.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(1049, 32)
        Me.LabelControl29.TabIndex = 276
        Me.LabelControl29.Text = "  Εδώ βλέπουμε τα διαθέσιμα credits του χρήστη."
        '
        'TABTransactions
        '
        Me.TABTransactions.Controls.Add(Me.ucGridTransactions)
        Me.TABTransactions.Controls.Add(Me.LabelControl10)
        Me.TABTransactions.ImageIndex = 81
        Me.TABTransactions.Name = "TABTransactions"
        Me.TABTransactions.Size = New System.Drawing.Size(1049, 528)
        Me.TABTransactions.Text = "Transactions"
        '
        'ucGridTransactions
        '
        Me.ucGridTransactions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ucGridTransactions.IsLoaded = False
        Me.ucGridTransactions.Location = New System.Drawing.Point(0, 32)
        Me.ucGridTransactions.Name = "ucGridTransactions"
        Me.ucGridTransactions.Size = New System.Drawing.Size(1049, 496)
        Me.ucGridTransactions.TabIndex = 277
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl10.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl10.AutoEllipsis = True
        Me.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl10.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl10.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl10.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(1049, 32)
        Me.LabelControl10.TabIndex = 276
        Me.LabelControl10.Text = "Εδώ βλέπουμε όλες τις συναλλαγές που πραγματοποίησε ο χρήστης."
        '
        'TabPageLikes
        '
        Me.TabPageLikes.Controls.Add(Me.UcLikes1)
        Me.TabPageLikes.Controls.Add(Me.LabelControl2)
        Me.TabPageLikes.ImageIndex = 54
        Me.TabPageLikes.Name = "TabPageLikes"
        Me.TabPageLikes.Size = New System.Drawing.Size(1049, 528)
        Me.TabPageLikes.Text = "Likes"
        '
        'UcLikes1
        '
        Me.UcLikes1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcLikes1.Location = New System.Drawing.Point(0, 32)
        Me.UcLikes1.Name = "UcLikes1"
        Me.UcLikes1.ProfileID = 0
        Me.UcLikes1.ShowSendingReceiving = True
        Me.UcLikes1.Size = New System.Drawing.Size(1049, 496)
        Me.UcLikes1.TabIndex = 276
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl2.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl2.AutoEllipsis = True
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl2.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(1049, 32)
        Me.LabelControl2.TabIndex = 275
        Me.LabelControl2.Text = "  Εδώ βλέπουμε συνοπτικές πληροφορίες που έχουν επεξεργαστεί είναι οι πιο σημαντι" & _
    "κές πληροφορίες για την μελέτη τις συμπεριφοράς του χρηστή μας."
        '
        'TabPageMessages
        '
        Me.TabPageMessages.Controls.Add(Me.LabelControl6)
        Me.TabPageMessages.Controls.Add(Me.UcMessages1)
        Me.TabPageMessages.ImageIndex = 264
        Me.TabPageMessages.Name = "TabPageMessages"
        Me.TabPageMessages.Size = New System.Drawing.Size(1049, 528)
        Me.TabPageMessages.Text = "Messages"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl6.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl6.AutoEllipsis = True
        Me.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl6.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl6.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(1049, 32)
        Me.LabelControl6.TabIndex = 276
        Me.LabelControl6.Text = "  Εδώ βλέπουμε συνοπτικές πληροφορίες που έχουν επεξεργαστεί είναι οι πιο σημαντι" & _
    "κές πληροφορίες για την μελέτη τις συμπεριφοράς του χρηστή μας."
        '
        'UcMessages1
        '
        Me.UcMessages1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.UcMessages1.Location = New System.Drawing.Point(0, 32)
        Me.UcMessages1.Name = "UcMessages1"
        Me.UcMessages1.ProfileID = 0
        Me.UcMessages1.ShowSendingReceiving = True
        Me.UcMessages1.Size = New System.Drawing.Size(1049, 496)
        Me.UcMessages1.TabIndex = 0
        '
        'TabPageOffers
        '
        Me.TabPageOffers.Controls.Add(Me.LabelControl7)
        Me.TabPageOffers.Controls.Add(Me.UcOffers1)
        Me.TabPageOffers.ImageIndex = 154
        Me.TabPageOffers.Name = "TabPageOffers"
        Me.TabPageOffers.Size = New System.Drawing.Size(1049, 528)
        Me.TabPageOffers.Text = "Offers"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.LabelControl7.Appearance.BorderColor = System.Drawing.SystemColors.ActiveBorder
        Me.LabelControl7.AutoEllipsis = True
        Me.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.LabelControl7.Dock = System.Windows.Forms.DockStyle.Top
        Me.LabelControl7.Location = New System.Drawing.Point(0, 0)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(1049, 32)
        Me.LabelControl7.TabIndex = 277
        Me.LabelControl7.Text = "  Εδώ βλέπουμε συνοπτικές πληροφορίες που έχουν επεξεργαστεί είναι οι πιο σημαντι" & _
    "κές πληροφορίες για την μελέτη τις συμπεριφοράς του χρηστή μας."
        '
        'UcOffers1
        '
        Me.UcOffers1.Location = New System.Drawing.Point(0, 31)
        Me.UcOffers1.Name = "UcOffers1"
        Me.UcOffers1.ProfileID = 0
        Me.UcOffers1.ShowSendingReceiving = True
        Me.UcOffers1.Size = New System.Drawing.Size(1049, 497)
        Me.UcOffers1.TabIndex = 0
        '
        'pcMainCommands
        '
        Me.pcMainCommands.Appearance.Options.UseBackColor = True
        Me.pcMainCommands.Controls.Add(Me.cmdCancel1)
        Me.pcMainCommands.Controls.Add(Me.cmdSave1)
        Me.pcMainCommands.Controls.Add(Me.cmdReject)
        Me.pcMainCommands.Controls.Add(Me.cmdApprove)
        Me.pcMainCommands.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pcMainCommands.Location = New System.Drawing.Point(2, 604)
        Me.pcMainCommands.Name = "pcMainCommands"
        Me.pcMainCommands.Size = New System.Drawing.Size(1059, 45)
        Me.pcMainCommands.TabIndex = 250
        '
        'cmdCancel1
        '
        Me.cmdCancel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.cmdCancel1.Appearance.Options.UseFont = True
        Me.cmdCancel1.ImageIndex = 399
        Me.cmdCancel1.ImageList = Me.ImageList1
        Me.cmdCancel1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.cmdCancel1.Location = New System.Drawing.Point(959, 7)
        Me.cmdCancel1.Name = "cmdCancel1"
        Me.cmdCancel1.Size = New System.Drawing.Size(92, 34)
        Me.cmdCancel1.TabIndex = 244
        Me.cmdCancel1.Text = "Cancel"
        '
        'cmdSave1
        '
        Me.cmdSave1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSave1.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.cmdSave1.Appearance.Options.UseFont = True
        Me.cmdSave1.ImageIndex = 107
        Me.cmdSave1.ImageList = Me.ImageList1
        Me.cmdSave1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.cmdSave1.Location = New System.Drawing.Point(861, 7)
        Me.cmdSave1.Name = "cmdSave1"
        Me.cmdSave1.Size = New System.Drawing.Size(92, 34)
        Me.cmdSave1.TabIndex = 243
        Me.cmdSave1.Text = "Save"
        '
        'cmdReject
        '
        Me.cmdReject.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdReject.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.cmdReject.Appearance.Options.UseFont = True
        Me.cmdReject.ImageIndex = 322
        Me.cmdReject.ImageList = Me.ImageList1
        Me.cmdReject.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.cmdReject.Location = New System.Drawing.Point(108, 7)
        Me.cmdReject.Name = "cmdReject"
        Me.cmdReject.Size = New System.Drawing.Size(92, 32)
        Me.cmdReject.TabIndex = 242
        Me.cmdReject.Text = "Reject"
        '
        'cmdApprove
        '
        Me.cmdApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdApprove.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.cmdApprove.Appearance.Options.UseFont = True
        Me.cmdApprove.ImageIndex = 384
        Me.cmdApprove.ImageList = Me.ImageList1
        Me.cmdApprove.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft
        Me.cmdApprove.Location = New System.Drawing.Point(5, 7)
        Me.cmdApprove.Name = "cmdApprove"
        Me.cmdApprove.Size = New System.Drawing.Size(92, 32)
        Me.cmdApprove.TabIndex = 241
        Me.cmdApprove.Text = "Approve"
        '
        'EUS_CustomerPhotosBindingSource
        '
        Me.EUS_CustomerPhotosBindingSource.DataMember = "EUS_CustomerPhotos"
        Me.EUS_CustomerPhotosBindingSource.DataSource = Me.DSMembers
        '
        'DSLists
        '
        Me.DSLists.DataSetName = "DSLists"
        Me.DSLists.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EUS_LISTS_EducationBindingSource
        '
        Me.EUS_LISTS_EducationBindingSource.DataMember = "EUS_LISTS_Education"
        Me.EUS_LISTS_EducationBindingSource.DataSource = Me.DSLists
        '
        'EUS_LISTS_IncomeBindingSource
        '
        Me.EUS_LISTS_IncomeBindingSource.DataMember = "EUS_LISTS_Income"
        Me.EUS_LISTS_IncomeBindingSource.DataSource = Me.DSLists
        '
        'EUS_LISTS_NetWorthBindingSource
        '
        Me.EUS_LISTS_NetWorthBindingSource.DataMember = "EUS_LISTS_NetWorth"
        Me.EUS_LISTS_NetWorthBindingSource.DataSource = Me.DSLists
        '
        'EUS_CustomerTransactionBindingSource
        '
        Me.EUS_CustomerTransactionBindingSource.DataMember = "EUS_CustomerTransaction"
        Me.EUS_CustomerTransactionBindingSource.DataSource = Me.DSMembers
        '
        'CustomerCreditsHistoryBindingSource
        '
        Me.CustomerCreditsHistoryBindingSource.DataMember = "CustomerCreditsHistory"
        Me.CustomerCreditsHistoryBindingSource.DataSource = Me.DSMembers
        '
        'BarManager1
        '
        Me.BarManager1.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Images = Me.ImageList1
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItemSendEmail, Me.BarButtonItemLoginToProfile})
        Me.BarManager1.MaxItemId = 2
        '
        'Bar1
        '
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.FloatLocation = New System.Drawing.Point(1052, 173)
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemSendEmail, True), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemLoginToProfile, True)})
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'BarButtonItemSendEmail
        '
        Me.BarButtonItemSendEmail.Caption = "Send Email"
        Me.BarButtonItemSendEmail.Id = 0
        Me.BarButtonItemSendEmail.ImageIndex = 264
        Me.BarButtonItemSendEmail.Name = "BarButtonItemSendEmail"
        Me.BarButtonItemSendEmail.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemLoginToProfile
        '
        Me.BarButtonItemLoginToProfile.Caption = "Login To User Profile"
        Me.BarButtonItemLoginToProfile.Id = 1
        Me.BarButtonItemLoginToProfile.Name = "BarButtonItemLoginToProfile"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(1063, 39)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 690)
        Me.barDockControlBottom.Size = New System.Drawing.Size(1063, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 39)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 651)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1063, 39)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 651)
        '
        'CustomerCreditsHistoryAdminBindingSource
        '
        Me.CustomerCreditsHistoryAdminBindingSource.DataMember = "CustomerCreditsHistoryAdmin"
        Me.CustomerCreditsHistoryAdminBindingSource.DataSource = Me.DSMembers
        '
        'frmProfileCart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1063, 690)
        Me.Controls.Add(Me.lbProfileCart)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmProfileCart"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Profile : myusername"
        CType(Me.lbProfileCart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.lbProfileCart.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.TABS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TABS.ResumeLayout(False)
        Me.TABGeneral.ResumeLayout(False)
        CType(Me.PictureBoxDefaultPhoto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcPoints, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcPoints.ResumeLayout(False)
        Me.gcPoints.PerformLayout()
        CType(Me.tbcPointsVerification.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcPointsVerification, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_ProfilesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSMembers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcPointsBeauty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbcPointsBeauty, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txTheme.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txGEOTwoLetterCountry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCustomReferrer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txReferrer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txIPLastLogin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtLastLogin.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtLastLogin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtRegistration.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtRegistration.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txLoginName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txIPRegistration.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TABAccount.ResumeLayout(False)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.TabPageAccount.ResumeLayout(False)
        CType(Me.DateEdit1.Properties.VistaTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txRegion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbAccountTypeId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTWName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtFBName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbGenderId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txMirrorProfileID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCityArea.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txFirstName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txLastName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckIsMaster.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txLoginName2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txPassword2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCountry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txZip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckAreYouWillingToTravel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txCellular.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txTelephone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txeMail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageAboutMe.ResumeLayout(False)
        CType(Me.MemoEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbNetWorth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbAnnualIncome.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txOccupation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbEducation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPagePersonal.ResumeLayout(False)
        CType(Me.icbDrinkingHabit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbEthnicity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbSmokingHabit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbReligion.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbChildrenID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbHairColorID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbEyeColorID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbBodyTypeID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.icbHeightID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageLookingFor.ResumeLayout(False)
        CType(Me.icbRelationshipStatusID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMarriedDating.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAdultDating_Casual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkMutuallyBeneficialArrangements.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkToMeetMaleID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkLongTermRelationship.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkToMeetFemaleID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkFriendship.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkShortTermRelationship.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageSettings.ResumeLayout(False)
        CType(Me.chkNotificationsSettings_WhenNewMembersNearMe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNotificationsSettings_WhenNewOfferReceived.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNotificationsSettings_WhenLikeReceived.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNotificationsSettings_WhenNewMessageReceived.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkHideMyLastLoginDateTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNotShowMyGifts.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNotShowInOtherUsersViewedList.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkHideMeFromSearchResults.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkNotShowInOtherUsersFavoritedList.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkHideMeFromMembersIHaveBlocked.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TABPhotosCarousel.ResumeLayout(False)
        Me.TABLogins.ResumeLayout(False)
        CType(Me.gcLoginHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcLoginHistory.ResumeLayout(False)
        Me.TABCredits.ResumeLayout(False)
        CType(Me.gcCreditsHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcCreditsHistory.ResumeLayout(False)
        CType(Me.txtAllCreditsExpDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerAvailableCreditsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtAvailableCredits.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TABTransactions.ResumeLayout(False)
        Me.TabPageLikes.ResumeLayout(False)
        Me.TabPageMessages.ResumeLayout(False)
        Me.TabPageOffers.ResumeLayout(False)
        CType(Me.pcMainCommands, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pcMainCommands.ResumeLayout(False)
        CType(Me.EUS_CustomerPhotosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_LISTS_EducationBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_LISTS_IncomeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_LISTS_NetWorthBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_CustomerTransactionBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerCreditsHistoryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CustomerCreditsHistoryAdminBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lbProfileCart As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents TABS As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents TABGeneral As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents txTheme As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txGEOTwoLetterCountry As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCustomReferrer As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txReferrer As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbGEOTwoLetterCountry As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbCustomReferrer As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbReferrer As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txIPLastLogin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbIPLastLogin As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dtLastLogin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents lbIPRegistration As DevExpress.XtraEditors.LabelControl
    Friend WithEvents dtRegistration As DevExpress.XtraEditors.DateEdit
    Friend WithEvents txPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txLoginName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl66 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txIPRegistration As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbRegisterDateTime As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbLoginName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbDateTimeLastLogin As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbPassword As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TABAccount As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents txLastName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txFirstName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbLastName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbFirstName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txMirrorProfileID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbLoginName2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbIsMaster As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbMirrorProfileID As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbPassword2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbStatus As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TABLogins As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TABTransactions As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ckIsMaster As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txLoginName2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txPassword2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ckAreYouWillingToTravel As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents txCellular As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbAreYouWillingToTravel As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbCellular As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txAddress As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbAddress As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txeMail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txTelephone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCity As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbeMail As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbTelephone As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbCity As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txZip As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txRegion As DevExpress.XtraEditors.TextEdit
    Friend WithEvents txCountry As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbZip As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbRegion As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbCountry As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbGender As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbDescribeAnIdealFirstDate As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbHeading As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbDescribeYourself As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txOccupation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lbOccupation As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbNetWorthID As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbAnnualIncomeID As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbEducationID As DevExpress.XtraEditors.LabelControl
    Friend WithEvents icbNetWorth As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents icbAnnualIncome As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents icbDrinkingHabit As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents icbSmokingHabit As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents icbReligion As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents icbEthnicity As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents icbChildrenID As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents icbHairColorID As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents icbEyeColorID As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents icbBodyTypeID As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents icbHeightID As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbChildrenID As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbEyeColorID As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbHeightID As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbHairColorID As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lbBodyTypeID As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DSMembers As Dating.Client.Admin.APP.AdminWS.DSMembers
    Friend WithEvents EUS_ProfilesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkToMeetFemaleID As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkToMeetMaleID As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkMutuallyBeneficialArrangements As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkLongTermRelationship As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkFriendship As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkShortTermRelationship As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkMarriedDating As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkAdultDating_Casual As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkNotShowMyGifts As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkNotShowInOtherUsersViewedList As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkNotShowInOtherUsersFavoritedList As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkHideMeFromMembersIHaveBlocked As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkHideMeFromSearchResults As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkHideMyLastLoginDateTime As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DSLists As Dating.Client.Admin.APP.AdminWS.DSLists
    Friend WithEvents EUS_LISTS_EducationBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EUS_LISTS_IncomeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EUS_LISTS_NetWorthBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents icbEducation As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents cmdReject As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdApprove As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents icbRelationshipStatusID As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents cbStatus As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents btnLoginName2Changed As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnZipChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCellularChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnEmailChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTelephoneChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAddressChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCityChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnRegionChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCountry2Changed As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLastNameChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnFirstNameChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnThemeChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnLoginNameChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnGEOTwoLetterCountryChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAboutMe_HeadingChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAboutMe_DescribeYourselfChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAboutMe_DescribeAnIdealFirstDateChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnOtherDetails_OccupationChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdCancel1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdSave1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents EUS_CustomerPhotosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents btnCityAreaChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txCityArea As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblCityArea As DevExpress.XtraEditors.LabelControl
    Friend WithEvents icbGenderId As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents icbAccountTypeId As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents TABPhotosCarousel As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents pcMainCommands As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ucLayout3D1 As Dating.Client.Admin.APP.ucLayout3D
    Friend WithEvents EUS_CustomerTransactionBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TABCredits As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents gcPoints As DevExpress.XtraEditors.GroupControl
    Friend WithEvents lblPointsBeauty As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbcPointsBeauty As DevExpress.XtraEditors.TrackBarControl
    Friend WithEvents lblPointsUnlocksValue As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblPointsUnlocks As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblPointsCreditsValue As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblPointsCredits As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblPointsVerification As DevExpress.XtraEditors.LabelControl
    Friend WithEvents tbcPointsVerification As DevExpress.XtraEditors.TrackBarControl
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents gcCreditsHistory As DevExpress.XtraEditors.GroupControl
    Friend WithEvents txtAllCreditsExpDate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtAvailableCredits As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents UcGridCreditsHistory As Dating.Client.Admin.APP.ucGrid
    Friend WithEvents btnTWNameChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtTWName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTWName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnFBNameChanged As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtFBName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblFBName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CustomerAvailableCreditsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents TabPageAccount As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TabPageAboutMe As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents TabPagePersonal As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents TabPageLookingFor As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents TabPageSettings As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents MemoEdit3 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents MemoEdit2 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents chkNotificationsSettings_WhenNewMessageReceived As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblNotificationsSettings_WhenNewMessageReceived As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkNotificationsSettings_WhenLikeReceived As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblNotificationsSettings_WhenLikeReceived As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkNotificationsSettings_WhenNewOfferReceived As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblNotificationsSettings_WhenNewOfferReceived As DevExpress.XtraEditors.LabelControl
    Friend WithEvents chkNotificationsSettings_WhenNewMembersNearMe As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents lblNotificationsSettings_WhenNewMembersNearMe As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CustomerCreditsHistoryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PictureBoxDefaultPhoto As System.Windows.Forms.PictureBox
    Friend WithEvents TabPageLikes As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents TabPageMessages As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents TabPageOffers As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents UcLikes1 As Dating.Client.Admin.APP.UCLikes
    Friend WithEvents gcLoginHistory As DevExpress.XtraEditors.GroupControl
    Friend WithEvents UcGridLoginHistory As Dating.Client.Admin.APP.ucGrid
    Friend WithEvents UcMessages1 As Dating.Client.Admin.APP.UCMessages
    Friend WithEvents UcOffers1 As Dating.Client.Admin.APP.UCOffers
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarButtonItemSendEmail As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemLoginToProfile As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents CustomerCreditsHistoryAdminBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ucGridTransactions As Dating.Client.Admin.APP.ucGrid
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
End Class

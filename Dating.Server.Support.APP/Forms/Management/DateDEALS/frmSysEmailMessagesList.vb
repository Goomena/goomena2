﻿Public Class frmSysEmailMessagesList

    Private Sub frmSysEmailMessagesList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.gpMain.Text = Me.Text
        'RepositoryItemImageComboBoxLAG.OwnerEdit.SelectedText = "US"
        LoadGrid()
    End Sub




    Private Sub LoadGrid()
        Try
            ShowWaitWin("Working please wait...")
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetSYS_EmailMessages")
            CheckWebServiceCallResult(gAdminWS.GetSYS_EmailMessages(gAdminWSHeaders, Me.UniCMSDB))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetSYS_EmailMessages")

            Dim FocusedRowHandle As Integer = gvEmailMessages.FocusedRowHandle
            Me.SYS_EmailMessagesBindingSource.DataSource = Me.UniCMSDB.SYS_EmailMessages

            If (FocusedRowHandle > -1 AndAlso FocusedRowHandle < Me.UniCMSDB.SYS_EmailMessages.Rows.Count) Then
                gvEmailMessages.FocusedRowHandle = FocusedRowHandle
            End If
            HideWaitWin()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmSysEmailMessages->LoadGrid")
        End Try
    End Sub


    Private Sub Save()
        Try
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateSYS_EmailMessages")
            ShowWaitWin("Working please wait...")
            CheckWebServiceCallResult(gAdminWS.UpdateSYS_EmailMessages(gAdminWSHeaders, Me.UniCMSDB))
            HideWaitWin()
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateSYS_EmailMessages")
        Catch ex As Exception
            ErrorMsgBox(ex, "frmSysEmailMessages->Save")
        End Try
    End Sub



    Private Sub ADD()
        Try
            Dim frm As New frmSysEmailMessagesItem()
            frm.ADD(-1, cbLAG.EditValue)
            frm.ShowDialog()
            If (frm.IsOK) Then
                LoadGrid()
            End If
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub EDIT()
        Try
            Dim MessageID As Integer = DirectCast(DirectCast(gvEmailMessages.GetFocusedRow(), System.Data.DataRowView).Row, AdminWS.UniCMSDB.SYS_EmailMessagesRow).MessageID
            Dim frm As New frmSysEmailMessagesItem()
            frm.EDIT(MessageID, cbLAG.EditValue)
            frm.ShowDialog()
            If (frm.IsOK) Then
                LoadGrid()
            End If
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub DELETE()
        Try
            If MsgBox("Are you sure delete selected record?", MsgBoxStyle.Exclamation + vbYesNo) = vbYes Then
                gvEmailMessages.DeleteSelectedRows()
                Save()
                LoadGrid()
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub gvEmailMessages_DoubleClick(sender As System.Object, e As System.EventArgs) Handles gvEmailMessages.DoubleClick
        Me.EDIT()
    End Sub

    Private Sub BarAddNew_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarAddNew.ItemClick
        Me.ADD()
    End Sub

    Private Sub BarEdit_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarEdit.ItemClick
        Me.EDIT()
    End Sub

    Private Sub BarDelete_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarDelete.ItemClick
        Me.DELETE()
    End Sub

    Private Sub BarRefresh_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarRefresh.ItemClick
        Me.LoadGrid()
    End Sub

    Private Sub cbLAG_EditValueChanging(sender As System.Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles cbLAG.EditValueChanging
        'If (e.OldValue <> "US") Then
        '    'Dim col As DevExpress.XtraGrid.Columns.GridColumn = gvEmailMessages.Columns(e.OldValue)
        '    'col.Visible = False
        '    Dim col2 As DevExpress.XtraGrid.Columns.GridColumn = gvEmailMessages.Columns("MessageText" & e.OldValue)
        '    col2.Visible = False
        'End If
        'If (e.NewValue <> "US") Then
        '    'Dim col As DevExpress.XtraGrid.Columns.GridColumn = gvEmailMessages.Columns(e.NewValue)
        '    'col.Visible = True
        '    'col.VisibleIndex = 2
        '    Dim col2 As DevExpress.XtraGrid.Columns.GridColumn = gvEmailMessages.Columns("MessageText" & e.NewValue)
        '    col2.Visible = True
        '    col2.VisibleIndex = 3
        'End If
    End Sub

End Class
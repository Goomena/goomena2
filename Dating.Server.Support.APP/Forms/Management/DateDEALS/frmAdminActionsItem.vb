﻿Imports System.ComponentModel

Public Class frmAdminActionsItem

    Public AdminActionsIdEdit As Integer
    Public IsOK As Boolean = False

    Private Sub frmAdminActionsItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IsOK = False
    End Sub

    Private Sub UpdateToHTML()
        Try

            UchtmlEditor1.HTMLBody = Library.Public.Common.CheckNULL(AdminActionsViewBindingSource.Current("NotesFromUser"), "")
            UchtmlEditor1.PageID = Library.Public.Common.CheckNULL(AdminActionsViewBindingSource.Current("AdminActionsId"), "")
            UchtmlEditor1.FileName = Globals.GetFileName(Library.Public.Common.CheckNULL(AdminActionsViewBindingSource.Current("AdminActionsId"), ""))

            UchtmlEditor1.TABDesign.PageVisible = False
            UchtmlEditor1.TABExternalEditor.PageVisible = False
            UchtmlEditor1.TABHTML.PageVisible = False
            UchtmlEditor1.RefreshEditor()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub UpdateFromHTML()
        Try

            UchtmlEditor1.RefreshEditorToDB()
            AdminActionsViewBindingSource.Current("NotesFromUser") = UchtmlEditor1.HTMLBody
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub Load(ByVal AdminActionsId As Integer)
        Try
            AdminActionsIdEdit = AdminActionsId
            LoadDataSource()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub LoadDataSource()
        Try
            Dim Params As New AdminWS.AdminActionsRetrieveParams()
            Params.AdminActionsId = AdminActionsIdEdit

            Dim WebErr As New AdminWS.clsDataRecordErrorsReturn
            WebErr = CheckWebServiceCallResult(gAdminWS.GetAdminActionsView(gAdminWSHeaders, Me.DsSecurity, Params))

            If WebErr.HasErrors Then
                Exit Sub
            End If

            AdminActionsViewBindingSource.DataSource = Me.DsSecurity.AdminActionsView

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub



    Public Sub ADD(ByVal AdminActionsId As Integer, ByVal ActiveLAG As String)
        Try
            Load(AdminActionsId)

            AdminActionsViewBindingSource.AddNew()
            AdminActionsViewBindingSource.Current("AdminActionsId") = AdminActionsId
            AdminActionsViewBindingSource.Current("SubjectUS") = ""
            AdminActionsViewBindingSource.Current("MessageTextUS") = ""
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Public Sub EDIT(ByVal AdminActionsId As Integer)
        Try
            Load(AdminActionsId)
            UpdateToHTML()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Public Sub DELETE(ByVal AdminActionsId As Integer)
        Try
            Load(AdminActionsId)
            Dim sz As String = "Deleted " & Me.AdminActionsViewBindingSource.Current("SubjectUS")
            Me.AdminActionsViewBindingSource.RemoveCurrent()
            Me.Validate()
            Me.AdminActionsViewBindingSource.EndEdit()
            MsgBox(sz)
            IsOK = True
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdClose.Click
        Try
            IsOK = False
            Close()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


End Class
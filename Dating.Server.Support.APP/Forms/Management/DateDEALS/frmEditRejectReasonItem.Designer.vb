﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditRejectReasonItem
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DSLists = New Dating.Client.Admin.APP.AdminWS.DSLists()
        Me.EUS_LISTS_RejectingReasonsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EUS_LISTS_RejectingReasonsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DSLists
        '
        Me.DSLists.DataSetName = "DSLists"
        Me.DSLists.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EUS_LISTS_RejectingReasonsBindingSource
        '
        Me.EUS_LISTS_RejectingReasonsBindingSource.DataMember = "EUS_LISTS_RejectingReasons"
        Me.EUS_LISTS_RejectingReasonsBindingSource.DataSource = Me.DSLists
        '
        'frmEditRejectReasonItem
        '
        Me.ClientSize = New System.Drawing.Size(595, 443)
        Me.Name = "frmEditRejectReasonItem"
        CType(Me.DSLists, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EUS_LISTS_RejectingReasonsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txMainTitle As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents UchtmlEditor1 As UCHTMLEditor
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CmdOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbLAG As DevExpress.XtraEditors.ImageComboBoxEdit
    Friend WithEvents CmdSAVE As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DSLists As Dating.Client.Admin.APP.AdminWS.DSLists
    Friend WithEvents EUS_LISTS_RejectingReasonsBindingSource As System.Windows.Forms.BindingSource
End Class

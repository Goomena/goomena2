﻿Public Class frmSysEmailOfferSettingsList

    Private Sub frmDDManagerLists_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.gpMain.Text = Me.Text
        LoadGrid()
    End Sub




    Private Sub LoadGrid()
        Try
            ShowWaitWin("Working please wait...")
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetSYS_EmailOfferSettings")
            CheckWebServiceCallResult(gAdminWS.GetSYS_EmailOfferSettings(gAdminWSHeaders, Me.UniCMSDB))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetSYS_EmailOfferSettings")

            Dim FocusedRowHandle As Integer = gvEmailOfferSettings.FocusedRowHandle
            Me.SYS_EmailOfferSettingsBindingSource.DataSource = Me.UniCMSDB.SYS_EmailOfferSettings

            If (FocusedRowHandle > -1 AndAlso FocusedRowHandle < Me.UniCMSDB.SYS_EmailOfferSettings.Rows.Count) Then
                gvEmailOfferSettings.FocusedRowHandle = FocusedRowHandle
            End If
            HideWaitWin()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmSysEmailOfferSettingsList->LoadGrid")
        End Try
    End Sub


    Private Sub Save()
        Try
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateSYS_EmailOfferSettings")
            ShowWaitWin("Working please wait...")
            CheckWebServiceCallResult(gAdminWS.UpdateSYS_EmailOfferSettings(gAdminWSHeaders, Me.UniCMSDB))
            HideWaitWin()
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateSYS_EmailOfferSettings")
        Catch ex As Exception
            ErrorMsgBox(ex, "frmSysEmailOfferSettingsList->Save")
        End Try
    End Sub



    Private Sub ADD()
        Try
            Me.gvEmailOfferSettings.AddNewRow()
            'Dim frm As New frmSysEmailMessagesItem()
            'frm.ADD(-1, cbLAG.EditValue)
            'frm.ShowDialog()
            'If (frm.IsOK) Then
            '    LoadGrid()
            'End If
            'frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub EDIT()
        Try
            'Dim SettingID As Integer = DirectCast(DirectCast(gvEmailOfferSettings.GetFocusedRow(), System.Data.DataRowView).Row, AdminWS.UniCMSDB.SYS_EmailOfferSettingsRow).SettingID
            'Dim frm As New frmSysEmailMessagesItem()
            'frm.EDIT(SettingID, cbLAG.EditValue)
            'frm.ShowDialog()
            'If (frm.IsOK) Then
            '    LoadGrid()
            'End If
            'frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub DELETE()
        Try
            If MsgBox("Are you sure delete selected record?", MsgBoxStyle.Exclamation + vbYesNo) = vbYes Then
                gvEmailOfferSettings.DeleteSelectedRows()
                Save()
                LoadGrid()
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarAddNew_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarAddNew.ItemClick
        Me.ADD()
    End Sub

    Private Sub BarEdit_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarEdit.ItemClick
        Me.EDIT()
    End Sub

    Private Sub BarDelete_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarDelete.ItemClick
        Me.DELETE()
    End Sub

    Private Sub BarRefresh_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarRefresh.ItemClick
        Me.LoadGrid()
    End Sub

    Private Sub cbLAG_EditValueChanging(sender As System.Object, e As DevExpress.XtraEditors.Controls.ChangingEventArgs)
        If (e.OldValue <> "US") Then
            Dim col As DevExpress.XtraGrid.Columns.GridColumn = gvEmailOfferSettings.Columns(e.OldValue)
            col.Visible = False
            Dim col2 As DevExpress.XtraGrid.Columns.GridColumn = gvEmailOfferSettings.Columns("MessageText" & e.OldValue)
            col2.Visible = False
        End If
        If (e.NewValue <> "US") Then
            Dim col As DevExpress.XtraGrid.Columns.GridColumn = gvEmailOfferSettings.Columns(e.NewValue)
            col.Visible = True
            col.VisibleIndex = 2
            Dim col2 As DevExpress.XtraGrid.Columns.GridColumn = gvEmailOfferSettings.Columns("MessageText" & e.NewValue)
            col2.Visible = True
            col2.VisibleIndex = 3
        End If
    End Sub


    Private Sub gvEmailOfferSettings_DoubleClick(sender As System.Object, e As System.EventArgs) Handles gvEmailOfferSettings.DoubleClick
        Me.EDIT()
    End Sub

    Private Sub gvEmailOfferSettings_KeyDown(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles gvEmailOfferSettings.KeyDown
        If e.KeyCode = System.Windows.Forms.Keys.Delete Then
            DELETE()
        ElseIf e.KeyCode = System.Windows.Forms.Keys.Enter Then
            gvEmailOfferSettings.CloseEditor()
            gvEmailOfferSettings.UpdateCurrentRow()
        End If
    End Sub

    Private Sub gvEmailOfferSettings_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles gvEmailOfferSettings.RowUpdated
        System.Diagnostics.Debug.Print("GridView_RowUpdated")
        Save()
        LoadGrid()
        'RefreshGrid()
    End Sub
End Class
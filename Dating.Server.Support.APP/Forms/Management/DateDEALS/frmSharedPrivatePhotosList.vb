﻿Public Class frmSharedPrivatePhotosList

    Private Sub frmSharedPrivatePhotosList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.gpMain.Text = Me.Text
        'RepositoryItemImageComboBoxLAG.OwnerEdit.SelectedText = "US"
        LoadGrid()

        Me.BarButtonItemEditFromProfile.Enabled = False
        Me.BarButtonItemEditToProfile.Enabled = False
        BarButtonItemLastSevenDays_ItemClick(BarButtonItemLastSevenDays, Nothing)
        BarEditItemSpinMonth.EditValue = Date.UtcNow.Month
        BarEditItemSpinYear.EditValue = Date.UtcNow.Year

    End Sub




    Private Sub LoadGrid()
        Try
            ShowWaitWin("Working please wait...")

            Dim Params As New AdminWS.ProfilePhotosLevelRetrieveParams()
            Params.DateFrom = BarEditItemDateFrom.EditValue
            If (BarEditItemDateTo.EditValue IsNot Nothing) Then Params.DateTo = CType(BarEditItemDateTo.EditValue, DateTime).AddDays(1)

            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetProfilePhotosLevelView")
            CheckWebServiceCallResult(gAdminWS.GetProfilePhotosLevelView(gAdminWSHeaders, Me.DSMembersViews, Params))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetProfilePhotosLevelView")

            Dim FocusedRowHandle As Integer = gv1.FocusedRowHandle
            Me.ProfilePhotosLevelViewBindingSource.DataSource = Me.DSMembersViews.ProfilePhotosLevelView

            If (FocusedRowHandle > -1 AndAlso FocusedRowHandle < Me.DSMembersViews.ProfilePhotosLevelView.Rows.Count) Then
                gv1.FocusedRowHandle = FocusedRowHandle
            End If
            HideWaitWin()
        Catch ex As Exception
            ErrorMsgBox(ex, "frmSysEmailMessages->LoadGrid")
        End Try
    End Sub


    Private Sub Save()
        Try
            'System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateProfilePhotosLevelView")
            'ShowWaitWin("Working please wait...")
            'CheckWebServiceCallResult(gAdminWS.UpdateProfilePhotosLevelView(gAdminWSHeaders, Me.DSMembersViews))
            'HideWaitWin()
            'System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateProfilePhotosLevelView")
        Catch ex As Exception
            ErrorMsgBox(ex, "frmSysEmailMessages->Save")
        End Try
    End Sub



    Private Sub ADD()
        Try
            'Dim frm As New frmSysEmailMessagesItem()
            'frm.ADD(-1, cbLAG.EditValue)
            'frm.ShowDialog()
            'If (frm.IsOK) Then
            '    LoadGrid()
            'End If
            'frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub EDIT()
        Try
            'Dim MessageID As Integer = DirectCast(DirectCast(gv1.GetFocusedRow(), System.Data.DataRowView).Row, AdminWS.DSMembersViews.ProfilePhotosLevelViewRow).CustomerTransactionID
            'Dim frm As New frmSysEmailMessagesItem()
            'frm.EDIT(MessageID, cbLAG.EditValue)
            'frm.ShowDialog()
            'If (frm.IsOK) Then
            '    LoadGrid()
            'End If
            'frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub DELETE()
        Try
            'If MsgBox("Are you sure delete selected record?", MsgBoxStyle.Exclamation + vbYesNo) = vbYes Then
            '    gv1.DeleteSelectedRows()
            '    Save()
            '    LoadGrid()
            'End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub gv1_DoubleClick(sender As System.Object, e As System.EventArgs) Handles gv1.DoubleClick
        Me.EDIT()
    End Sub

    Private Sub BarAddNew_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarAddNew.ItemClick
        Me.ADD()
    End Sub

    Private Sub BarEdit_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarEdit.ItemClick
        Me.EDIT()
    End Sub

    Private Sub BarDelete_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarDelete.ItemClick
        Me.DELETE()
    End Sub

    Private Sub BarRefresh_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarRefresh.ItemClick
        Me.LoadGrid()
    End Sub


    Private Sub BarButtonItemEditToProfile_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemEditToProfile.ItemClick
        Try
            Dim row As AdminWS.DSMembersViews.ProfilePhotosLevelViewRow = DirectCast(Me.gv1.GetFocusedRow(), System.Data.DataRowView).Row
            Dim profileId As Integer = row.ToProfileID
            Dim frm As New frmProfileCart
            frm.EDIT(profileId)
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarButtonItemEditFromProfile_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemEditFromProfile.ItemClick
        Try
            Dim row As AdminWS.DSMembersViews.ProfilePhotosLevelViewRow = DirectCast(Me.gv1.GetFocusedRow(), System.Data.DataRowView).Row
            Dim profileId As Integer = row.FromProfileID
            Dim frm As New frmProfileCart
            frm.EDIT(profileId)
            frm.Dispose()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub gv1_FocusedRowChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs) Handles gv1.FocusedRowChanged
        BarButtonItemEditFromProfile.Enabled = True
        BarButtonItemEditToProfile.Enabled = True
    End Sub

    Private Sub BarButtonItemToday_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemToday.ItemClick
        Try
            BarEditItemDateFrom.EditValue = Date.UtcNow.Date
            BarEditItemDateTo.EditValue = Date.UtcNow.Date
            Me.LoadGrid()

            BarButtonItemToday.Enabled = False
            BarButtonItemTwoDaysAgo.Enabled = True
            BarButtonItemYesterday.Enabled = True
            BarButtonItemLastSevenDays.Enabled = True
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarButtonItemTwoDaysAgo_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemTwoDaysAgo.ItemClick
        Try
            BarEditItemDateFrom.EditValue = Date.UtcNow.AddDays(-2).Date
            BarEditItemDateTo.EditValue = Date.UtcNow.AddDays(-2).Date
            Me.LoadGrid()

            BarButtonItemToday.Enabled = True
            BarButtonItemTwoDaysAgo.Enabled = False
            BarButtonItemYesterday.Enabled = True
            BarButtonItemLastSevenDays.Enabled = True
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarButtonItemYesterday_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemYesterday.ItemClick
        Try
            BarEditItemDateFrom.EditValue = Date.UtcNow.AddDays(-1).Date
            BarEditItemDateTo.EditValue = Date.UtcNow.AddDays(-1).Date
            Me.LoadGrid()

            BarButtonItemToday.Enabled = True
            BarButtonItemTwoDaysAgo.Enabled = True
            BarButtonItemYesterday.Enabled = False
            BarButtonItemLastSevenDays.Enabled = True
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarButtonItemLastSevenDays_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemLastSevenDays.ItemClick
        Try
            BarEditItemDateFrom.EditValue = Date.UtcNow.AddDays(-7).Date
            BarEditItemDateTo.EditValue = Date.UtcNow.Date
            Me.LoadGrid()

            BarButtonItemToday.Enabled = True
            BarButtonItemTwoDaysAgo.Enabled = True
            BarButtonItemYesterday.Enabled = True
            BarButtonItemLastSevenDays.Enabled = False
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarButtonItemSpinGo_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemSpinGo.ItemClick
        Try
            Dim dFromDate As New Date(BarEditItemSpinYear.EditValue, BarEditItemSpinMonth.EditValue, 1)
            Dim dToDate As New Date(BarEditItemSpinYear.EditValue, BarEditItemSpinMonth.EditValue, DateTime.DaysInMonth(dFromDate.Year, dFromDate.Month))

            BarEditItemDateFrom.EditValue = dFromDate
            BarEditItemDateTo.EditValue = dToDate

            Me.LoadGrid()

            BarButtonItemToday.Enabled = True
            BarButtonItemTwoDaysAgo.Enabled = True
            BarButtonItemYesterday.Enabled = True
            BarButtonItemLastSevenDays.Enabled = True

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarEditItemSpinYear_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles BarEditItemSpinYear.EditValueChanged
        If (BarEditItemSpinYear.EditValue <= 1899) Then
            BarEditItemSpinYear.EditValue = 2200
        ElseIf (BarEditItemSpinYear.EditValue >= 2201) Then
            BarEditItemSpinYear.EditValue = 1900
        End If
    End Sub

    Private Sub BarEditItemSpinMonth_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles BarEditItemSpinMonth.EditValueChanged
        If (BarEditItemSpinMonth.EditValue <= 0) Then
            BarEditItemSpinMonth.EditValue = 12
        ElseIf (BarEditItemSpinMonth.EditValue >= 13) Then
            BarEditItemSpinMonth.EditValue = 1
        End If
    End Sub

End Class
﻿Imports System.ComponentModel

Public Class frmRejectReasonItem

    Public RejectingReasonsIdEdit As Integer
    Public IsOK As Boolean = False

    Private Sub frmSitePageCustomItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        IsOK = False
        txtOtherLagTitle.Text = ""
        txtOtherLagTitle.Enabled = False
    End Sub

    Private Sub UpdateToHTML(ByVal LAGID As String)
        Try

            UchtmlEditor1.HTMLBody = Library.Public.Common.CheckNULL(EUS_LISTS_RejectingReasonsBindingSource.Current("Text" & LAGID), "")
            UchtmlEditor1.MessageID = Library.Public.Common.CheckNULL(EUS_LISTS_RejectingReasonsBindingSource.Current("RejectingReasonsId"), "")
            UchtmlEditor1.PageID = Library.Public.Common.CheckNULL(EUS_LISTS_RejectingReasonsBindingSource.Current("RejectingReasonsId"), "")
            UchtmlEditor1.FileName = Globals.GetFileName(Library.Public.Common.CheckNULL(EUS_LISTS_RejectingReasonsBindingSource.Current("US"), ""))

            ' select by default tabdesign
            'UchtmlEditor1.TABDesign.Focus()

            UchtmlEditor1.RefreshEditor()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub UpdateFromHTML(ByVal LAGID As String)
        Try

            UchtmlEditor1.RefreshEditorToDB()
            EUS_LISTS_RejectingReasonsBindingSource.Current("Text" & LAGID) = UchtmlEditor1.HTMLBody

            'Select Case cbLAG.EditValue
            '    Case "US"
            '        EUS_LISTS_RejectingReasonsBindingSource.Current("US") = UchtmlEditor1.HTMLBody
            '    Case "GR"
            '        EUS_LISTS_RejectingReasonsBindingSource.Current("GR") = UchtmlEditor1.HTMLBody
            '    Case "HU"
            '        EUS_LISTS_RejectingReasonsBindingSource.Current("HU") = UchtmlEditor1.HTMLBody
            '    Case "ES"
            '        EUS_LISTS_RejectingReasonsBindingSource.Current("ES") = UchtmlEditor1.HTMLBody
            'End Select
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub cbLAG_EditValueChanging(ByVal sender As Object, ByVal e As DevExpress.XtraEditors.Controls.ChangingEventArgs) Handles cbLAG.EditValueChanging
        Try

            UpdateFromHTML(e.OldValue)
            UpdateToHTML(e.NewValue)

            If (e.NewValue <> "US") Then
                txtOtherLagTitle.Text = EUS_LISTS_RejectingReasonsBindingSource.Current(e.NewValue).ToString()
                txtOtherLagTitle.Enabled = True
            Else
                txtOtherLagTitle.Text = ""
                txtOtherLagTitle.Enabled = False
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub Save()
        Try
            UpdateFromHTML(cbLAG.EditValue)
            If (txtOtherLagTitle.Enabled) Then
                EUS_LISTS_RejectingReasonsBindingSource.Current(cbLAG.EditValue) = txtOtherLagTitle.Text
            End If
            Me.Validate()
            Me.EUS_LISTS_RejectingReasonsBindingSource.EndEdit()
            ' Me.TableAdapterManager.UpdateAll(Me.DSLists)

            Dim WebErr As New AdminWS.clsDataRecordErrorsReturn
            WebErr = CheckWebServiceCallResult(gAdminWS.UpdateEUS_LISTS_RejectingReasons(gAdminWSHeaders, Me.DSLists))
            If WebErr.HasErrors Then
                Exit Sub
            End If
            LoadDataSource()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub Load(ByVal RejectingReasonsId As Integer)
        Try
            RejectingReasonsIdEdit = RejectingReasonsId
            LoadDataSource()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub LoadDataSource()
        Try
            Dim WebErr As New AdminWS.clsDataRecordErrorsReturn
            If (RejectingReasonsIdEdit > 0) Then
                WebErr = CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_RejectingReasons_FillByRejectingReasonsId(gAdminWSHeaders, Me.DSLists, RejectingReasonsIdEdit))
            Else
                WebErr = CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_RejectingReasons(gAdminWSHeaders, Me.DSLists))
            End If

            If WebErr.HasErrors Then
                Exit Sub
            End If


            EUS_LISTS_RejectingReasonsBindingSource.DataSource = Me.DSLists.EUS_LISTS_RejectingReasons

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub



    Public Sub ADD(ByVal RejectingReasonsId As Integer, ByVal ActiveLAG As String)
        Try
            Load(RejectingReasonsId)

            EUS_LISTS_RejectingReasonsBindingSource.AddNew()
            EUS_LISTS_RejectingReasonsBindingSource.Current("RejectingReasonsId") = RejectingReasonsId
            EUS_LISTS_RejectingReasonsBindingSource.Current("US") = ""
            'EUS_LISTS_RejectingReasonsBindingSource.Current("USText") = ""

            UpdateToHTML(cbLAG.EditValue)
            cbLAG.EditValue = ActiveLAG
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Public Sub EDIT(ByVal RejectingReasonsId As Integer, ByVal ActiveLAG As String)
        Try
            Load(RejectingReasonsId)
            UpdateToHTML(cbLAG.EditValue)
            cbLAG.EditValue = ActiveLAG
            'UpdateToHTML(ActiveLAG)
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Public Sub DELETE(ByVal RejectingReasonsId As Integer)
        Try
            Load(RejectingReasonsId)
            Dim sz As String = "Deleted " & Me.EUS_LISTS_RejectingReasonsBindingSource.Current("US")
            Me.EUS_LISTS_RejectingReasonsBindingSource.RemoveCurrent()
            Me.Validate()
            Me.EUS_LISTS_RejectingReasonsBindingSource.EndEdit()
            Save()
            MsgBox(sz)
            IsOK = True
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdOK.Click
        Try
            Save()
            IsOK = True
            Close()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub CmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdCancel.Click
        Try
            IsOK = False
            Close()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub CmdSAVE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdSAVE.Click
        Try
            Save()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try

    End Sub


End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdminActionsItem
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txActionDescription = New DevExpress.XtraEditors.TextEdit()
        Me.AdminActionsViewBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DsSecurity = New Dating.Client.Admin.APP.AdminWS.dsSecurity()
        Me.SYS_AdminActionsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.UchtmlEditor1 = New Dating.Client.Admin.APP.UCHTMLEditor()
        Me.CmdClose = New DevExpress.XtraEditors.SimpleButton()
        Me.txMessageSubject = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.txtPaymentMethod = New DevExpress.XtraEditors.TextEdit()
        CType(Me.txActionDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AdminActionsViewBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DsSecurity, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SYS_AdminActionsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.txMessageSubject.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txActionDescription
        '
        Me.txActionDescription.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.AdminActionsViewBindingSource, "ActionDescription", True))
        Me.txActionDescription.Location = New System.Drawing.Point(188, 13)
        Me.txActionDescription.Name = "txActionDescription"
        Me.txActionDescription.Size = New System.Drawing.Size(607, 20)
        Me.txActionDescription.TabIndex = 0
        '
        'AdminActionsViewBindingSource
        '
        Me.AdminActionsViewBindingSource.DataMember = "AdminActionsView"
        Me.AdminActionsViewBindingSource.DataSource = Me.DsSecurity
        '
        'DsSecurity
        '
        Me.DsSecurity.DataSetName = "dsSecurity"
        Me.DsSecurity.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SYS_AdminActionsBindingSource
        '
        Me.SYS_AdminActionsBindingSource.DataMember = "SYS_AdminActions"
        Me.SYS_AdminActionsBindingSource.DataSource = GetType(Dating.Client.Admin.APP.AdminWS.dsSecurity)
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(12, 12)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(170, 22)
        Me.LabelControl1.TabIndex = 6
        Me.LabelControl1.Text = "Action Description:"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.UchtmlEditor1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 66)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(837, 398)
        Me.GroupControl1.TabIndex = 8
        Me.GroupControl1.Text = "Body Message"
        '
        'UchtmlEditor1
        '
        Me.UchtmlEditor1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UchtmlEditor1.Location = New System.Drawing.Point(2, 21)
        Me.UchtmlEditor1.Name = "UchtmlEditor1"
        Me.UchtmlEditor1.Size = New System.Drawing.Size(833, 375)
        Me.UchtmlEditor1.TabIndex = 0
        '
        'CmdClose
        '
        Me.CmdClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdClose.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.CmdClose.Appearance.Options.UseFont = True
        Me.CmdClose.Location = New System.Drawing.Point(706, 470)
        Me.CmdClose.Name = "CmdClose"
        Me.CmdClose.Size = New System.Drawing.Size(128, 39)
        Me.CmdClose.TabIndex = 10
        Me.CmdClose.Text = "Close"
        '
        'txMessageSubject
        '
        Me.txMessageSubject.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.AdminActionsViewBindingSource, "UserLoginName", True))
        Me.txMessageSubject.Location = New System.Drawing.Point(188, 39)
        Me.txMessageSubject.Name = "txMessageSubject"
        Me.txMessageSubject.Size = New System.Drawing.Size(221, 20)
        Me.txMessageSubject.TabIndex = 0
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(59, 40)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(123, 22)
        Me.LabelControl2.TabIndex = 6
        Me.LabelControl2.Text = "User Login Name:"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
        Me.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl3.Location = New System.Drawing.Point(447, 39)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(136, 22)
        Me.LabelControl3.TabIndex = 15
        Me.LabelControl3.Text = "Profile Login Name:"
        '
        'txtPaymentMethod
        '
        Me.txtPaymentMethod.DataBindings.Add(New System.Windows.Forms.Binding("EditValue", Me.AdminActionsViewBindingSource, "ProfileLoginName", True))
        Me.txtPaymentMethod.Location = New System.Drawing.Point(589, 41)
        Me.txtPaymentMethod.Name = "txtPaymentMethod"
        Me.txtPaymentMethod.Size = New System.Drawing.Size(206, 20)
        Me.txtPaymentMethod.TabIndex = 14
        '
        'frmAdminActionsItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(854, 521)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.txtPaymentMethod)
        Me.Controls.Add(Me.CmdClose)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.txMessageSubject)
        Me.Controls.Add(Me.txActionDescription)
        Me.Name = "frmAdminActionsItem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Page Custom Item"
        CType(Me.txActionDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AdminActionsViewBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DsSecurity, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SYS_AdminActionsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.txMessageSubject.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPaymentMethod.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txActionDescription As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CmdClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txMessageSubject As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtPaymentMethod As DevExpress.XtraEditors.TextEdit
    Friend WithEvents UchtmlEditor1 As Dating.Client.Admin.APP.UCHTMLEditor
    Friend WithEvents SYS_AdminActionsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DsSecurity As Dating.Client.Admin.APP.AdminWS.dsSecurity
    Friend WithEvents AdminActionsViewBindingSource As System.Windows.Forms.BindingSource
End Class

﻿Imports System.Net
Imports System.IO

Public Class frmProfilePhoto

    'Dim UL As New clsUserLog
    Public IsSaved As Boolean

    Public PhotoID As Integer
    Public ProfileID As Integer

    Private FormAction As FormActionEnum
    Private ImagePath As String




    Private Sub frmDDProfilePhoto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            IsSaved = False
            Me._LoadPhotoData()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub


    Private Sub frmDDProfilePhoto_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed

        ' delete local file from disk
        If (Not String.IsNullOrEmpty(Me.ImagePath)) Then
            Try
                Me.picPhoto.Dispose()
                Me.picPhoto.Image = Nothing
                Me.picPhoto = Nothing

                Dim fi As New FileInfo(Me.ImagePath)
                If (fi.Exists()) Then
                    fi.Delete()
                End If
            Catch ex As Exception
                ErrorMsgBox(ex, "")
            End Try
        End If

    End Sub



    Public Sub ADD()
        Try
            Me.FormAction = FormActionEnum.ADD
            Me.PhotoID = 0
            Me.ShowDialog()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Public Sub EDIT(ByVal PhotoID As Integer, ProfileID As Integer)
        Try
            Me.FormAction = FormActionEnum.EDIT
            Me.PhotoID = PhotoID
            Me.ProfileID = ProfileID
            Me.ShowDialog()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Public Sub DELETE(ByVal PhotoID As Integer, ProfileID As Integer)
        Try

            Me.FormAction = FormActionEnum.DELETE
            Me.PhotoID = PhotoID
            Me.ProfileID = ProfileID

            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetEUS_CustomerPhotos_ByCustomerPhotosID")
            ShowWaitWin("Working please wait...")
            CheckWebServiceCallResult(gAdminWS.GetEUS_CustomerPhotos_ByCustomerPhotosID(gAdminWSHeaders, Me.DSMembers, Me.PhotoID))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetEUS_CustomerPhotos_ByCustomerPhotosID")

            Me.EUS_CustomerPhotosBindingSource.DataSource = Me.DSMembers
            Dim sz As String = "Deleted permanently photo with id " & Me.EUS_CustomerPhotosBindingSource.Current("CustomerPhotosID")
            Me.EUS_CustomerPhotosBindingSource.RemoveCurrent()
            _Save()
            MsgBox(sz)

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub



    Private Sub _LoadPhotoData()

        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetEUS_LISTS_PhotosDisplayLevel")
        ShowWaitWin("Working please wait...")
        CheckWebServiceCallResult(gAdminWS.GetEUS_LISTS_PhotosDisplayLevel(gAdminWSHeaders, Me.DSLists))
        HideWaitWin()
        System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetEUS_LISTS_PhotosDisplayLevel")

        ClsCombos.FillComboUsingDatatable(DSLists.EUS_LISTS_PhotosDisplayLevel, "US", "PhotosDisplayLevelId", icbDisplayLevelId, True)

        If (FormAction = FormActionEnum.ADD) Then

        ElseIf (FormAction = FormActionEnum.EDIT) Then
            Dim fstr As FileStream = Nothing
            Try

                'UL.BeginWork(202, "Edit cart ProfileID:" & ProfileID, "")
                System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " GetEUS_CustomerPhotos_ByCustomerPhotosID")
                ShowWaitWin("Working please wait...")
                CheckWebServiceCallResult(gAdminWS.GetEUS_CustomerPhotos_ByCustomerPhotosID(gAdminWSHeaders, Me.DSMembers, Me.PhotoID))
                System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END GetEUS_CustomerPhotos_ByCustomerPhotosID")

                Me.EUS_CustomerPhotosBindingSource.DataSource = Me.DSMembers


                Dim fileName As String = Me.EUS_CustomerPhotosBindingSource.Current("FileName").ToString()
                Me.Text = fileName
                Dim urlDownloadFile As String = String.Format(gMemberPhotoURL, Me.ProfileID, fileName)

                Try
                    Me.ImagePath = GetWebFile(urlDownloadFile)

                    Dim fi As New FileInfo(Me.ImagePath)
                    fstr = fi.OpenRead()

                    picPhoto.Image = System.Drawing.Image.FromStream(fstr)

                    lblWidthHeightInfo.Text = picPhoto.Image.Width & " x " & picPhoto.Image.Height
                    lblSizeInfo.Text = FormatFileSize(fi.Length)
                    lblTypeInfo.Text = fi.Extension.TrimStart("."c).ToUpper()
                Catch ex As Exception

                End Try

                txtPhotoURL.Text = urlDownloadFile

            Catch ex As Exception
                ErrorMsgBox(ex, "")
            Finally
                If (fstr IsNot Nothing) Then fstr.Dispose()
            End Try


        ElseIf (FormAction = FormActionEnum.DELETE) Then

        End If

    End Sub

    Private Sub _Save()
        Try
            Me.Validate()
            Me.EUS_CustomerPhotosBindingSource.EndEdit()
            Me.DSMembers = Me.EUS_CustomerPhotosBindingSource.DataSource

            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " UpdateEUS_CustomerPhotos")
            ShowWaitWin("Working please wait...")
            CheckWebServiceCallResult(gAdminWS.UpdateEUS_CustomerPhotos(gAdminWSHeaders, Me.DSMembers))
            System.Diagnostics.Debug.Print(Date.Now.ToString("HH:mm.fff") & " END UpdateEUS_CustomerPhotos")

            IsSaved = True
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub



    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel1.Click
        'UL.EndWork(False, ProfileID, "ProfileID Cart", 0, 0, 0)
        IsSaved = False
        Hide()
        Close()
        Me.Dispose()
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave1.Click
        'Me.ProfileID = Me.EUS_ProfilesBindingSource.Current("ProfileID")
        ''UL.EndWork(1, ProfileID, "ProfileID Cart", 0, 0, 0)
        _Save()
        Hide()
        Close()
        Me.Dispose()
    End Sub

End Class
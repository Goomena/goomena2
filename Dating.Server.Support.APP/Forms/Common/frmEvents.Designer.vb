<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEvents
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.CmdViewEvents = New System.Windows.Forms.Panel
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TABEvents = New System.Windows.Forms.TabPage
        Me.lsEvents = New System.Windows.Forms.ListBox
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.ClearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TABSummary = New System.Windows.Forms.TabPage
        Me.lsSummary = New System.Windows.Forms.ListBox
        Me.MenuStrip2 = New System.Windows.Forms.MenuStrip
        Me.ClearToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TimerRunner = New System.Windows.Forms.Timer(Me.components)
        Me.CmdViewEvents.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TABEvents.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.TABSummary.SuspendLayout()
        Me.MenuStrip2.SuspendLayout()
        Me.SuspendLayout()
        '
        'CmdViewEvents
        '
        Me.CmdViewEvents.Controls.Add(Me.TabControl1)
        Me.CmdViewEvents.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CmdViewEvents.Location = New System.Drawing.Point(0, 0)
        Me.CmdViewEvents.Name = "CmdViewEvents"
        Me.CmdViewEvents.Size = New System.Drawing.Size(829, 390)
        Me.CmdViewEvents.TabIndex = 1
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TABEvents)
        Me.TabControl1.Controls.Add(Me.TABSummary)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(829, 390)
        Me.TabControl1.TabIndex = 37
        '
        'TABEvents
        '
        Me.TABEvents.Controls.Add(Me.lsEvents)
        Me.TABEvents.Controls.Add(Me.MenuStrip1)
        Me.TABEvents.Location = New System.Drawing.Point(4, 22)
        Me.TABEvents.Name = "TABEvents"
        Me.TABEvents.Padding = New System.Windows.Forms.Padding(3)
        Me.TABEvents.Size = New System.Drawing.Size(821, 364)
        Me.TABEvents.TabIndex = 2
        Me.TABEvents.Text = "Events"
        Me.TABEvents.UseVisualStyleBackColor = True
        '
        'lsEvents
        '
        Me.lsEvents.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsEvents.FormattingEnabled = True
        Me.lsEvents.HorizontalScrollbar = True
        Me.lsEvents.Location = New System.Drawing.Point(3, 27)
        Me.lsEvents.Name = "lsEvents"
        Me.lsEvents.Size = New System.Drawing.Size(815, 329)
        Me.lsEvents.TabIndex = 43
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(3, 3)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(815, 24)
        Me.MenuStrip1.TabIndex = 44
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ClearToolStripMenuItem
        '
        Me.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem"
        Me.ClearToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.ClearToolStripMenuItem.Text = "Clear"
        '
        'TABSummary
        '
        Me.TABSummary.Controls.Add(Me.lsSummary)
        Me.TABSummary.Controls.Add(Me.MenuStrip2)
        Me.TABSummary.Location = New System.Drawing.Point(4, 22)
        Me.TABSummary.Name = "TABSummary"
        Me.TABSummary.Padding = New System.Windows.Forms.Padding(3)
        Me.TABSummary.Size = New System.Drawing.Size(821, 364)
        Me.TABSummary.TabIndex = 3
        Me.TABSummary.Text = "Summary"
        Me.TABSummary.UseVisualStyleBackColor = True
        '
        'lsSummary
        '
        Me.lsSummary.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsSummary.FormattingEnabled = True
        Me.lsSummary.HorizontalScrollbar = True
        Me.lsSummary.Location = New System.Drawing.Point(3, 27)
        Me.lsSummary.Name = "lsSummary"
        Me.lsSummary.Size = New System.Drawing.Size(815, 329)
        Me.lsSummary.TabIndex = 43
        '
        'MenuStrip2
        '
        Me.MenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearToolStripMenuItem1, Me.RefreshToolStripMenuItem})
        Me.MenuStrip2.Location = New System.Drawing.Point(3, 3)
        Me.MenuStrip2.Name = "MenuStrip2"
        Me.MenuStrip2.Size = New System.Drawing.Size(815, 24)
        Me.MenuStrip2.TabIndex = 44
        Me.MenuStrip2.Text = "MenuStrip2"
        '
        'ClearToolStripMenuItem1
        '
        Me.ClearToolStripMenuItem1.Name = "ClearToolStripMenuItem1"
        Me.ClearToolStripMenuItem1.Size = New System.Drawing.Size(44, 20)
        Me.ClearToolStripMenuItem1.Text = "Clear"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(63, 20)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'TimerRunner
        '
        Me.TimerRunner.Interval = 10000
        '
        'frmEvents
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(829, 390)
        Me.Controls.Add(Me.CmdViewEvents)
        Me.Name = "frmEvents"
        Me.Opacity = 0
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "SRF MAIN Events"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        Me.CmdViewEvents.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TABEvents.ResumeLayout(False)
        Me.TABEvents.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.TABSummary.ResumeLayout(False)
        Me.TABSummary.PerformLayout()
        Me.MenuStrip2.ResumeLayout(False)
        Me.MenuStrip2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CmdViewEvents As System.Windows.Forms.Panel
    Friend WithEvents TimerRunner As System.Windows.Forms.Timer
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TABEvents As System.Windows.Forms.TabPage
    Friend WithEvents TABSummary As System.Windows.Forms.TabPage
    Friend WithEvents lsEvents As System.Windows.Forms.ListBox
    Friend WithEvents lsSummary As System.Windows.Forms.ListBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ClearToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip2 As System.Windows.Forms.MenuStrip
    Friend WithEvents ClearToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class

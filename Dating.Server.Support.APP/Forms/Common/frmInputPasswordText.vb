﻿Public Class frmInputPasswordText

    Public Property IsOK As Boolean
    Public Function GetUserInput()
        Return txtInput.Text
    End Function

    Private Sub btnOk_Click(sender As System.Object, e As System.EventArgs) Handles btnOk.Click
        IsOK = True
        Me.Hide()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        IsOK = False
        Me.Hide()
    End Sub


    Private Sub frmInputPasswordText_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Library.Public.Common.CenterScreen(Me)
        'Me.Focus()
        'gcInputText.Focus()
        txtInput.Focus()
    End Sub
End Class
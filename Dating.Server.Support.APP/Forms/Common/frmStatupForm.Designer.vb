﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStatupForm
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStatupForm))
        Me.GroupControlframemain = New DevExpress.XtraEditors.GroupControl()
        Me.ListBoxControl1 = New DevExpress.XtraEditors.ListBoxControl()
        Me.TimerStartup = New System.Windows.Forms.Timer(Me.components)
        Me.defaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.CheckEditNotAutomaticalyCheckforUpdates = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.GroupControlframemain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControlframemain.SuspendLayout()
        CType(Me.ListBoxControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditNotAutomaticalyCheckforUpdates.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControlframemain
        '
        Me.GroupControlframemain.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.GroupControlframemain.AppearanceCaption.Options.UseFont = True
        Me.GroupControlframemain.Controls.Add(Me.ListBoxControl1)
        Me.GroupControlframemain.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControlframemain.Location = New System.Drawing.Point(0, 0)
        Me.GroupControlframemain.Name = "GroupControlframemain"
        Me.GroupControlframemain.Size = New System.Drawing.Size(582, 245)
        Me.GroupControlframemain.TabIndex = 191
        Me.GroupControlframemain.Text = "C.A.S Central Administrate System"
        '
        'ListBoxControl1
        '
        Me.ListBoxControl1.Location = New System.Drawing.Point(12, 48)
        Me.ListBoxControl1.Name = "ListBoxControl1"
        Me.ListBoxControl1.Size = New System.Drawing.Size(558, 182)
        Me.ListBoxControl1.TabIndex = 0
        '
        'TimerStartup
        '
        '
        'defaultLookAndFeel1
        '
        Me.defaultLookAndFeel1.LookAndFeel.SkinName = "Black"
        '
        'CheckEditNotAutomaticalyCheckforUpdates
        '
        Me.CheckEditNotAutomaticalyCheckforUpdates.Location = New System.Drawing.Point(10, 251)
        Me.CheckEditNotAutomaticalyCheckforUpdates.Name = "CheckEditNotAutomaticalyCheckforUpdates"
        Me.CheckEditNotAutomaticalyCheckforUpdates.Properties.Caption = "Not Automaticaly Check for Updates"
        Me.CheckEditNotAutomaticalyCheckforUpdates.Size = New System.Drawing.Size(318, 19)
        Me.CheckEditNotAutomaticalyCheckforUpdates.TabIndex = 194
        '
        'frmStatupForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(582, 282)
        Me.Controls.Add(Me.CheckEditNotAutomaticalyCheckforUpdates)
        Me.Controls.Add(Me.GroupControlframemain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmStatupForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "C.A.S Central Administrate System"
        CType(Me.GroupControlframemain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControlframemain.ResumeLayout(False)
        CType(Me.ListBoxControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditNotAutomaticalyCheckforUpdates.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControlframemain As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ListBoxControl1 As DevExpress.XtraEditors.ListBoxControl
    Friend WithEvents TimerStartup As System.Windows.Forms.Timer
    Public WithEvents defaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents CheckEditNotAutomaticalyCheckforUpdates As DevExpress.XtraEditors.CheckEdit
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAutomaticUpdates

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAutomaticUpdates))
        Me.GroupControlAutomaticUpdatesOptions = New DevExpress.XtraEditors.GroupControl()
        Me.CmdOK = New DevExpress.XtraEditors.SimpleButton()
        Me.CmdCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.CheckEditNotAutomaticalyCheckforUpdates = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.GroupControlAutomaticUpdatesOptions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditNotAutomaticalyCheckforUpdates.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupControlAutomaticUpdatesOptions
        '
        Me.GroupControlAutomaticUpdatesOptions.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.GroupControlAutomaticUpdatesOptions.AppearanceCaption.Options.UseFont = True
        Me.GroupControlAutomaticUpdatesOptions.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupControlAutomaticUpdatesOptions.Location = New System.Drawing.Point(0, 0)
        Me.GroupControlAutomaticUpdatesOptions.Name = "GroupControlAutomaticUpdatesOptions"
        Me.GroupControlAutomaticUpdatesOptions.Size = New System.Drawing.Size(437, 36)
        Me.GroupControlAutomaticUpdatesOptions.TabIndex = 15
        Me.GroupControlAutomaticUpdatesOptions.Text = " Automatic Updates Options"
        '
        'CmdOK
        '
        Me.CmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdOK.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.CmdOK.Appearance.Options.UseFont = True
        Me.CmdOK.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.CmdOK.Location = New System.Drawing.Point(169, 125)
        Me.CmdOK.Name = "CmdOK"
        Me.CmdOK.Size = New System.Drawing.Size(126, 29)
        Me.CmdOK.TabIndex = 57
        Me.CmdOK.Text = "OK"
        '
        'CmdCancel
        '
        Me.CmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CmdCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.CmdCancel.Appearance.Options.UseFont = True
        Me.CmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.CmdCancel.Location = New System.Drawing.Point(301, 125)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.Size = New System.Drawing.Size(126, 29)
        Me.CmdCancel.TabIndex = 56
        Me.CmdCancel.Text = "Cancel"
        '
        'CheckEditNotAutomaticalyCheckforUpdates
        '
        Me.CheckEditNotAutomaticalyCheckforUpdates.Location = New System.Drawing.Point(43, 63)
        Me.CheckEditNotAutomaticalyCheckforUpdates.Name = "CheckEditNotAutomaticalyCheckforUpdates"
        Me.CheckEditNotAutomaticalyCheckforUpdates.Properties.Caption = "Not Automaticaly Check for Updates"
        Me.CheckEditNotAutomaticalyCheckforUpdates.Size = New System.Drawing.Size(318, 19)
        Me.CheckEditNotAutomaticalyCheckforUpdates.TabIndex = 196
        '
        'frmAutomaticUpdates
        '
        Me.AcceptButton = Me.CmdOK
        Me.CancelButton = Me.CmdCancel
        Me.ClientSize = New System.Drawing.Size(437, 162)
        Me.Controls.Add(Me.CheckEditNotAutomaticalyCheckforUpdates)
        Me.Controls.Add(Me.CmdOK)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.GroupControlAutomaticUpdatesOptions)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAutomaticUpdates"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Automatic Updates Options"
        CType(Me.GroupControlAutomaticUpdatesOptions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditNotAutomaticalyCheckforUpdates.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupControlAutomaticUpdatesOptions As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CmdOK As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CmdCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckEditNotAutomaticalyCheckforUpdates As DevExpress.XtraEditors.CheckEdit
End Class

﻿Public Class frmStatupForm 

    Public WithEvents ApplicationTAG As License.Client.Core.clsApplicationTAG = gApplicationTAG

    Dim IsHide As Boolean = False

    Private Sub ApplicationTAG_OnEndUpdateCheck() Handles ApplicationTAG.OnEndUpdateCheck

    End Sub

    Private Sub ApplicationTAG_OnWorkingInfomation(ByVal Message As String) Handles ApplicationTAG.OnWorkingInfomation
        Try
            If Not IsHide Then
                StatusAdd(Message)
            End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub TimerStartup_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerStartup.Tick
        Try
            TimerStartup.Enabled = False
            IsHide = False

            If Not CheckEditNotAutomaticalyCheckforUpdates.Checked Then
                StatusAdd("Check for update...")
                gApplicationTAG.Init(RegistryAPP, True)
                Select Case gApplicationTAG.UpdateProgram()
                    Case License.Client.Core.clsApplicationTAG.UpdateStatus.ErrorOnUpdate
                        MsgBox("Errors on download update!")
                    Case License.Client.Core.clsApplicationTAG.UpdateStatus.IsLastVersion

                    Case License.Client.Core.clsApplicationTAG.UpdateStatus.UpdateSuccess
                        Application.Restart()
                End Select
            Else
                StatusAdd("Disable Check for updates Canceling for fast starting...")
            End If

            StatusAdd("Read HardwareID...")
            Dim cHardwareID As New Library.Public.clsHardwareID
            gHardwareID = cHardwareID.HardwareID
            StatusAdd("Machine HardwareID :" & gHardwareID)

            '   Dim Ret As String = ""

ReCheckHardwareID:
            'Dim TokenID As String = GetSetting(RegistryAPP, "Settings", "TokenID", "")
            'If Len(TokenID) > 0 Then gClearToken = Library.Public.Crypto.DecryptTripleDES(TokenID, G.gTokenPassword)

            StatusAdd("Login to Server...")

            IsHide = True
            Hide()


            'frmCASLogin.ShowDialog()
            'If frmCASLogin.LoginOK Then
            '    'StatusAdd("Loading Main Application...")
            frmMain.ShowDialog()
            'End If

            My.Computer.Registry.SetValue(RegBase, "NotAutomaticalyCheckforUpdates", CheckEditNotAutomaticalyCheckforUpdates.Checked, 0)

            Close()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
            Close()
        End Try
    End Sub

    Private Sub frmStatupForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '    DevExpress.UserSkins.OfficeSkins.Register()
            DevExpress.UserSkins.BonusSkins.Register()
            DevExpress.Skins.SkinManager.EnableFormSkins()
            TimerStartup.Enabled = True
            CheckEditNotAutomaticalyCheckforUpdates.Checked = My.Computer.Registry.GetValue(RegBase, "NotAutomaticalyCheckforUpdates", 0)
            ProcessCommandLine()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

#Region "ProcessCommandLine"
    ' Dim RunApp As String = ""

    Shared Function ProcessCommandLine() As Boolean
        Try
            Dim szRunCommand As String = ""
            Dim szSiteCommand As String = ""
            '  'Process Command Line.....
            For Each s As String In My.Application.CommandLineArgs
                'Select Case s.ToLower
                '    Case "run:sitesadmin"
                '        RunApp = "sitesadmin"
                '    Case "-cms"
                '        gCallingFromService = True
                '        ' Site Name 
                '        If Mid(s.ToLower, 1, 5) = "-site:" Then szExtraInfoString = Mid(s, 6, s.Length - 5)
                'End Select

                If Mid(s.ToLower, 1, 5) = "-run:" Then szRunCommand = Mid(s, 6, s.Length - 5)
                If Mid(s.ToLower, 1, 5) = "-site:" Then szSiteCommand = Mid(s, 6, s.Length - 6)
            Next

            If Len(szRunCommand) Then
                Select Case szRunCommand
                    Case "sitesadmin"

                    Case "cms"

                End Select

            End If

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region

    Private Sub StatusAdd(ByVal szMessage As String)
        Try
            ListBoxControl1.Items.Add(szMessage)
            ListBoxControl1.SelectedIndex = ListBoxControl1.Items.Count - 1
            My.Application.DoEvents()

        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub GroupControlframemain_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles GroupControlframemain.Paint

    End Sub

    Private Sub ListBoxControl1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListBoxControl1.SelectedIndexChanged

    End Sub

    Private Sub CheckEditNotAutomaticalyCheckforUpdates_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditNotAutomaticalyCheckforUpdates.CheckedChanged
        My.Computer.Registry.SetValue(RegBase, "NotAutomaticalyCheckforUpdates", CheckEditNotAutomaticalyCheckforUpdates.Checked, 0)
    End Sub

End Class
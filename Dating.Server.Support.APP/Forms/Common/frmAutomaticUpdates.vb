
Public Class frmAutomaticUpdates
    Inherits DevExpress.XtraEditors.XtraForm

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        'Add any initialization after the InitializeComponent() call

    End Sub

    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Library.Public.Common.CenterScreen(Me)
        CheckEditNotAutomaticalyCheckforUpdates.Checked = My.Computer.Registry.GetValue(RegBase, "NotAutomaticalyCheckforUpdates", 0)
    End Sub

    Private Sub CmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdCancel.Click
        Close()
    End Sub


    Private Sub CmdOK_Click(sender As System.Object, e As System.EventArgs) Handles CmdOK.Click
        Try
            My.Computer.Registry.SetValue(RegBase, "NotAutomaticalyCheckforUpdates", CheckEditNotAutomaticalyCheckforUpdates.Checked, 0)
            Close()
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

End Class
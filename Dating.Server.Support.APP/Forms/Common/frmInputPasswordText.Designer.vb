﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInputPasswordText
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gcInputText = New DevExpress.XtraEditors.GroupControl()
        Me.btnCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.btnOk = New DevExpress.XtraEditors.SimpleButton()
        Me.txtInput = New DevExpress.XtraEditors.TextEdit()
        CType(Me.gcInputText, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gcInputText.SuspendLayout()
        CType(Me.txtInput.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcInputText
        '
        Me.gcInputText.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold)
        Me.gcInputText.AppearanceCaption.Options.UseFont = True
        Me.gcInputText.Controls.Add(Me.txtInput)
        Me.gcInputText.Controls.Add(Me.btnCancel)
        Me.gcInputText.Controls.Add(Me.btnOk)
        Me.gcInputText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcInputText.Location = New System.Drawing.Point(0, 0)
        Me.gcInputText.Name = "gcInputText"
        Me.gcInputText.Size = New System.Drawing.Size(538, 103)
        Me.gcInputText.TabIndex = 0
        Me.gcInputText.Text = "Input Text"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(451, 68)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'btnOk
        '
        Me.btnOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOk.Location = New System.Drawing.Point(370, 68)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.TabIndex = 1
        Me.btnOk.Text = "OK"
        '
        'txtInput
        '
        Me.txtInput.Location = New System.Drawing.Point(13, 37)
        Me.txtInput.Name = "txtInput"
        Me.txtInput.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtInput.Properties.UseSystemPasswordChar = True
        Me.txtInput.Size = New System.Drawing.Size(513, 20)
        Me.txtInput.TabIndex = 3
        '
        'frmInputPasswordText
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(538, 103)
        Me.Controls.Add(Me.gcInputText)
        Me.Name = "frmInputPasswordText"
        Me.Text = "Input Text"
        CType(Me.gcInputText, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gcInputText.ResumeLayout(False)
        CType(Me.txtInput.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gcInputText As DevExpress.XtraEditors.GroupControl
    Friend WithEvents btnOk As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtInput As DevExpress.XtraEditors.TextEdit
End Class

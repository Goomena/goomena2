﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmWaitProccess
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWaitProccess))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lbMainText = New DevExpress.XtraEditors.LabelControl()
        Me.ProgressBar = New DevExpress.XtraEditors.ProgressBarControl()
        Me.CmdCancel = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProgressBar.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(15, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(38, 36)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'lbMainText
        '
        Me.lbMainText.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        '   Me.lbMainText.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.lbMainText.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.lbMainText.Location = New System.Drawing.Point(59, 9)
        Me.lbMainText.Name = "lbMainText"
        Me.lbMainText.Size = New System.Drawing.Size(388, 20)
        Me.lbMainText.TabIndex = 52
        Me.lbMainText.Text = "..."
        '
        'ProgressBar
        '
        Me.ProgressBar.Location = New System.Drawing.Point(59, 31)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(388, 17)
        Me.ProgressBar.TabIndex = 53
        '
        'CmdCancel
        '
        Me.CmdCancel.Location = New System.Drawing.Point(171, 54)
        Me.CmdCancel.Name = "CmdCancel"
        Me.CmdCancel.Size = New System.Drawing.Size(152, 32)
        Me.CmdCancel.TabIndex = 54
        Me.CmdCancel.Text = "Cancel"
        '
        'frmWaitProccess
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(459, 91)
        Me.Controls.Add(Me.CmdCancel)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.lbMainText)
        Me.Controls.Add(Me.PictureBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmWaitProccess"
        Me.Text = "Wait Proccess"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProgressBar.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lbMainText As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ProgressBar As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents CmdCancel As DevExpress.XtraEditors.SimpleButton
End Class

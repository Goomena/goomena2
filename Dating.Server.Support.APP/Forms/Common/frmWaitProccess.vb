﻿Public Class frmWaitProccess 
    Public CancelCommand As Boolean
    Public ShowCancelCommand As Boolean = True

    Private Sub frmWaitProccess_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        On Error GoTo er

        CmdCancel.Visible = ShowCancelCommand
        Library.Public.Common.CenterScreen(Me)

        Exit Sub
er:     ErrorMsgBox("frmWaitProccess_Load")
    End Sub

    Private Sub CmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CmdCancel.Click
        On Error GoTo er
        CancelCommand = True
        Exit Sub
er:     ErrorMsgBox("CmdCancel_Click")
    End Sub

End Class
﻿Imports Dating.Server.Datasets.DLL
Imports Library.Public

Public Class frmManagerTemplateXX

    Private Sub frmTerminalsManager_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        RefreshGrid()
    End Sub

    Private Sub RefreshGrid()
        Dim FocusedRowHandle As Integer = GridView1.FocusedRowHandle
        GridView1.FocusedRowHandle = FocusedRowHandle
    End Sub

    Private Shared Sub ADD()
        Try
            'Dim FocusedRowHandle As Integer = GridView1.FocusedRowHandle
            'frmTerminalsCart.ADD()
            'If frmTerminalsCart.IsSaved Then
            '    RefreshGrid()
            '    Me.TerminalGridBindingSource.Position = Me.TerminalGridBindingSource.Find("TerminalID", frmTerminalsCart.TerminalID)
            'End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Shared Sub EDIT()
        Try
            'Dim TerminalID As Integer
            'TerminalID = GridView1.GetFocusedRowCellValue("TerminalID")
            'Dim FocusedRowHandle As Integer = GridView1.FocusedRowHandle
            'frmTerminalsCart.EDIT(TerminalID)
            'If frmTerminalsCart.IsSaved Then
            '    RefreshGrid()
            '    Me.TerminalGridBindingSource.Position = Me.TerminalGridBindingSource.Find("TerminalID", frmTerminalsCart.TerminalID)
            'End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Shared Sub DELETE()
        Try
            'If MsgBox("You are about to DELETE selected record." & vbCrLf & "Continue?", MsgBoxStyle.Exclamation + vbYesNo) = vbYes Then
            '    Dim TerminalID As Integer
            '    TerminalID = GridView1.GetFocusedRowCellValue("TerminalID")
            '    Dim FocusedRowHandle As Integer = GridView1.FocusedRowHandle
            '    frmTerminalsCart.DELETE(TerminalID)
            '    If frmTerminalsCart.IsSaved Then
            '        RefreshGrid()
            '        GridView1.FocusedRowHandle = FocusedRowHandle - 1
            '    End If
            'End If
        Catch ex As Exception
            ErrorMsgBox(ex, "")
        End Try
    End Sub

    Private Sub BarDELETE_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarDELETE.ItemClick
        DELETE()
    End Sub

    Private Sub GridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridView1.KeyDown
        If e.KeyCode = System.Windows.Forms.Keys.Delete Then
            DELETE()
        End If
    End Sub

    Private Sub BarRefresh_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarRefresh.ItemClick
        RefreshGrid()
    End Sub

    Private Sub GridView1_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GridView1.DoubleClick
        EDIT()
    End Sub
    Private Sub BarAddNEW_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarAddNEW.ItemClick
        ADD()
    End Sub
    Private Sub BarEDIT_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarEDIT.ItemClick
        EDIT()
    End Sub





End Class
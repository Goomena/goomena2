﻿Imports Dating.Referrals.Server.Core.DLL
Imports System.Configuration
Imports System.Web
Imports System.Text.RegularExpressions

Public Class clsCustomer


    Public Shared Function GetPriceFromCredits(credits As Integer) As EUS_Price
        Dim _Price As EUS_Price = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            _Price = _CMSDBDataContext.EUS_Prices.Where(Function(itm) itm.Credits = credits).FirstOrDefault()

            'If (rec IsNot Nothing) Then
            '    _Price = rec.Amount
            'End If


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return _Price
    End Function


    Public Shared Function GetEUS_PricesByAmount(amount As Integer) As EUS_Price
        Dim _Price As EUS_Price = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            _Price = _CMSDBDataContext.EUS_Prices.Where(Function(itm) itm.Amount = amount).FirstOrDefault()

            'If (rec IsNot Nothing) Then
            '    _Price = rec.Amount
            'End If


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return _Price
    End Function




    Public Shared Sub sendPayment(ByVal amount As String,
                           ByVal type As String,
                           ByVal quantity As String,
                           ByVal loginName As String,
                           ByVal CustomerID As Integer,
                           ByVal CustomReferrer As String,
                           ByVal oURL As String, ByVal paymentMethod As String,
                           ByVal promoCode As String, ByVal SalesSiteID As Integer,
                           ByVal paymentProviderType As String,
                           ByVal productCode As String)

        If (String.IsNullOrEmpty(productCode)) Then
            productCode = "dd" & quantity & type
        End If

        If (paymentMethod = "regnow") Then
            Dim payTransID As String = Date.UtcNow.ToFileTime().ToString()
            RegNowPayment(productCode, amount, payTransID)
        Else
            Dim qsInfo As String = ""
            Dim prof As EUS_Profile = Nothing
            Try
                prof = DataHelpers.GetEUS_ProfileByProfileID(CustomerID)
                qsInfo = qsInfo & "&fn=" & prof.FirstName & " " & prof.LastName
                qsInfo = qsInfo & "&zip=" & prof.Zip
                qsInfo = qsInfo & "&cit=" & prof.City
                qsInfo = qsInfo & "&reg=" & prof.Region
                qsInfo = qsInfo & "&cntr=" & prof.Country
                qsInfo = qsInfo & "&eml=" & prof.eMail
                qsInfo = qsInfo & "&mob=" & prof.Cellular
            Catch ex As Exception

            End Try

            Try
                Dim oRemotePost As New RemotePost()
                Dim postURL As String = ""
                Dim flag As Boolean = True

                If paymentMethod = "AlertPay" Or paymentMethod = "paysafecard" Or paymentMethod = "epoch" Then
                    flag = False
                End If
                If flag Then
                    postURL = "http://www.paymix.net/pay/loader.aspx"
                End If
                If postURL = "" Then postURL = "http://www.soundsoft.com/pay/initPayment.ashx"
                If productCode.ToLower = "zfreemoney" Then
                    '  postURL = "http://www.digi-store.net/pay/initPayment.ashx"
                End If
                If paymentMethod.ToLower = "paypal" Then
                    postURL = "http://www.digi-store.net/pay/initPayment.ashx"
                End If
                If paymentMethod.ToLower = "epoch" Then
                    postURL = "https://www.goomena.com/pay/initPayment.ashx"
                End If
                oRemotePost.Url = postURL
                'oRemotePost.Url = "http://localhost:15837/initPayment.ashx"

                oRemotePost.Add("PaymentMethod", paymentMethod)
                If (paymentProviderType Is Nothing) Then
                    If (paymentMethod.ToUpper() = "PAYMENTWALL") Then
                        oRemotePost.Add("Widget", "p4_1")
                    End If
                End If
                oRemotePost.Add("ProductCode", productCode)
                oRemotePost.Add("CustomerID", CustomerID)
                oRemotePost.Add("LoginName", loginName)
                oRemotePost.Add("promoCode", promoCode)
                oRemotePost.Add("CustomReferrer", CustomReferrer)
                oRemotePost.Add("downloadURL", oURL)
                oRemotePost.Add("Amount", amount)
                oRemotePost.Add("type", paymentProviderType)
                oRemotePost.Add("SalesSiteID", SalesSiteID)
                If (prof IsNot Nothing) Then
                    Try
                        oRemotePost.Add("fn", prof.FirstName & " " & prof.LastName)
                        oRemotePost.Add("zip", prof.Zip)
                        oRemotePost.Add("cit", prof.City)
                        oRemotePost.Add("reg", prof.Region)
                        oRemotePost.Add("cntr", prof.Country)
                        oRemotePost.Add("eml", prof.eMail)
                        oRemotePost.Add("mob", prof.Cellular)
                    Catch ex As Exception

                    End Try
                End If
                oRemotePost.Post()
            Catch ex As Exception
                Dim flag As Boolean = True
                If paymentMethod.ToLower() = "alertpay" Or paymentMethod.ToLower() = "paysafecard" Or paymentMethod.ToLower() = "epoch" Then
                    flag = False
                End If
                Dim Widget As String = ""
                If (paymentProviderType Is Nothing) Then
                    If (paymentMethod.ToUpper() = "PAYMENTWALL") Then
                        Widget = "&Widget=p4_1"
                    End If
                End If

                If productCode.ToLower = "zfreemoney" Then
                    '  HttpContext.Current.Response.Redirect("http://www.digi-store.net/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType)

                End If
                If paymentMethod.ToLower = "paypal" Then
                    Dim url As String = "http://www.digi-store.net/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType
                    HttpContext.Current.Response.Redirect(url)

                End If
                If flag Then
                    Dim url As String = "http://www.paymix.net/pay/loader.aspx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType & Widget
                    HttpContext.Current.Response.Redirect(url)

                End If
                If paymentMethod.ToLower = "epoch" Then
                    Dim url2 As String = "https://www.goomena.com/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType & "" & Widget & qsInfo
                    HttpContext.Current.Response.Redirect(url2)
                End If
                Dim url1 As String = "http://www.soundsoft.com/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "" & Widget
                HttpContext.Current.Response.Redirect(url1)

                'HttpContext.Current.Response.Redirect("http://localhost:15837/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "")
            End Try

            'Dim productCode As String = "dd" & quantity & type
            'Dim SalesSiteID As String = ConfigurationManager.AppSettings("siteID")
            'Try
            '    Dim oRemotePost As New RemotePost()
            '    Dim postURL As String = ""
            '    Dim flag As Boolean = True

            '    If paymentMethod = "AlertPay" Or paymentMethod = "paysafecard" Then
            '        flag = False
            '    End If
            '    If flag Then
            '        postURL = "http://www.paymix.net/pay/initPayment.ashx"
            '    End If
            '    If postURL = "" Then postURL = "http://www.soundsoft.com/pay/initPayment.ashx"
            '    oRemotePost.Url = postURL
            '    'oRemotePost.Url = "http://localhost:15837/initPayment.ashx"

            '    oRemotePost.Add("PaymentMethod", paymentMethod)
            '    If (paymentMethod.ToUpper() = "PAYMENTWALL") Then
            '        oRemotePost.Add("Widget", "p4_1")
            '    End If
            '    oRemotePost.Add("ProductCode", productCode)
            '    oRemotePost.Add("CustomerID", CustomerID)
            '    oRemotePost.Add("LoginName", loginName)
            '    oRemotePost.Add("promoCode", promoCode)
            '    oRemotePost.Add("CustomReferrer", CustomReferrer)
            '    oRemotePost.Add("downloadURL", oURL)
            '    oRemotePost.Add("Amount", amount)
            '    oRemotePost.Add("type", paymentProviderType)
            '    oRemotePost.Add("SalesSiteID", SalesSiteID)
            '    oRemotePost.Post()
            'Catch ex As Exception
            '    Dim flag As Boolean = True
            '    If paymentMethod = "AlertPay" Or paymentMethod = "paysafecard" Then
            '        flag = False
            '    End If
            '    If flag Then
            '        System.Web.HttpContext.Current.Response.Redirect("http://www.paymix.net/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "&type=" & paymentProviderType & "&Widget=p4_1")

            '    End If
            '    System.Web.HttpContext.Current.Response.Redirect("http://www.soundsoft.com/pay/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=" & SalesSiteID & "" & "&Widget=p4_1")
            '    'System.Web.HttpContext.Current.Response.Redirect("http://localhost:15837/initPayment.ashx?PaymentMethod=" & paymentMethod & "&ProductCode=" & productCode & "&CustomerID=" & CustomerID & "&LoginName=" & loginName & "&promoCode=" & promoCode & "&CustomReferrer=" & CustomReferrer & "&downloadURL=" & oURL & "&amount=" & amount & "&SalesSiteID=5")
            'End Try
        End If

    End Sub


    Public Shared Sub RegNowPayment(productID As String, amount As String, payTransID As String)

        Dim pgUrl As String = "https://www.regnow.com/checkout/cart/new/42221-"

        'Dim productID As String = Request("itemName")
        'Dim amount As Decimal = Request("amount")
        'Dim payTransID As String = Request("TransID")

        Dim oRemotePost As New RemotePost()
        oRemotePost.Url = pgUrl & productID

        oRemotePost.Add("linkid", payTransID)
        oRemotePost.Add("currency", "EUR")
        oRemotePost.Post()

    End Sub


    Shared Function ResellerAddFunds(Money As Double, CustomerID As Integer) As Boolean
        Return True
    End Function


    'clsCustomer.AddTransaction(Description, Money, TransactionID, "", PurchaseDays, PurchaseGigabytes, REMOTE_ADDR, HTTP_USER_AGENT, HTTP_REFERER, CustomerID, 
    'CustomReferrer, cDataRecordIPN.BuyerInfo.PayerEmail, paymethod, cDataRecordIPN.PromoCode, cDataRecordIPN.TransactionTypeID)
    Shared Function AddTransaction(Description As String, Money As Double, TransactionInfo As String, p4 As String,
                                   CreditsPurchased As Integer?,
                                   REMOTE_ADDR As String,
                                    HTTP_USER_AGENT As String,
                                    HTTP_REFERER As String,
                                    CustomerID As Integer,
                                    CustomReferrer As String,
                                    PayerEmail As String,
                                    paymethod As PaymentMethods,
                                    PromoCode As String,
                                    TransactionTypeID As Integer,
                                    productCode As String,
                                    Optional freeUnlocks As Integer = 0,
                                    Optional useVoucherCredits As Boolean = False,
                                    Optional durationDays As Integer = 0,
                                    Optional voucherCredits As Integer = 0) As Integer

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Dim CustomerTransactionID As Integer = -1
        Try
            If (Not String.IsNullOrEmpty(HTTP_REFERER)) Then
                If (HTTP_REFERER.Length > 1020) Then
                    HTTP_REFERER = HTTP_REFERER.Remove(1020)
                End If
            End If

            Dim payment_provider_ratio As Double = clsConfigValues.Get__payment_provider_percents() / 100

            Dim transaction As New EUS_CustomerTransaction()
            transaction.CreditAmount = 0
            transaction.CustomerID = CustomerID
            transaction.CustomReferrer = CustomReferrer
            transaction.Date_Time = DateTime.UtcNow
            transaction.DebitAmount = Money - (Money * payment_provider_ratio)
            transaction.DebitAmountReceived = Money
            transaction.DebitReducePercent = clsConfigValues.Get__payment_provider_percents()
            transaction.Description = Description
            transaction.IP = REMOTE_ADDR
            transaction.Referrer = HTTP_REFERER
            transaction.TransactionInfo = TransactionInfo
            transaction.TransactionType = TransactionTypeID
            transaction.Notes = ""
            transaction.ReceiptID = ""


            Dim freeCredits As Integer
            If (freeUnlocks > 0) Then
                Dim creditsRequired As EUS_CreditsType = clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_CONVERSATION)
                freeCredits = freeUnlocks * creditsRequired.CreditsAmount
            End If


            Dim _Price As New EUS_Price()

            If (Not String.IsNullOrEmpty(productCode)) Then
                _Price = clsPricing.GetPriceForProductCode_Copy(productCode)
                _Price.Credits = (_Price.Credits + freeCredits)

            ElseIf (Not String.IsNullOrEmpty(PromoCode)) Then

                _Price.Credits = CreditsPurchased
                _Price.duration = 45
                _Price.Amount = Money

            ElseIf (useVoucherCredits) Then
                _Price.Credits = voucherCredits + freeCredits
                _Price.duration = durationDays
                _Price.Amount = Money

            Else
                _Price = GetPriceFromCredits(CreditsPurchased)
                _Price.Credits = (CreditsPurchased + freeCredits)

            End If


            If (_Price.Currency = "LEK") Then
                Dim ratio As Double = clsConfigValues.Get__ratio_eur_lek()
                transaction.DebitAmount = transaction.DebitAmount / ratio
            End If


            Dim credit As New EUS_CustomerCredit()
            credit.Credits = _Price.Credits
            credit.CustomerId = CustomerID
            credit.DateTimeCreated = DateTime.UtcNow
            credit.DateTimeExpiration = DateTime.UtcNow.AddDays(_Price.duration)
            credit.ValidForDays = _Price.duration
            credit.EuroAmount = transaction.DebitAmount
            credit.Currency = "EUR" '_Price.Currency


            transaction.Currency = _Price.Currency
            transaction.EUS_CustomerCredits.Add(credit)

            _CMSDBDataContext.EUS_CustomerTransactions.InsertOnSubmit(transaction)
            _CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(credit)

            _CMSDBDataContext.SubmitChanges()


            If (credit.EuroAmount > 0) Then
                Dim REF_CRD2EURO_Rate As Double = 0
                If (credit.Credits > 0) Then REF_CRD2EURO_Rate = (credit.EuroAmount / credit.Credits)
                UpdateEUS_Profiles_REF_CRD2EURO_Rate(CustomerID, REF_CRD2EURO_Rate)
            End If


            CustomerTransactionID = transaction.CustomerTransactionID



            Dim profile As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(CustomerID)

            If (Not String.IsNullOrEmpty(PromoCode)) Then
                SendBonusNotification(profile, Money, _Price, PayerEmail, CustomerID)
            Else
                SendPaymentNotification(profile, Money, _Price, PayerEmail, CustomerID)
            End If


        Catch ex As Exception
            Dim body As String = GetMessageBody(Description, Money, TransactionInfo, p4,
                           CreditsPurchased, REMOTE_ADDR,
                           HTTP_USER_AGENT, HTTP_REFERER, CustomerID, CustomReferrer,
                           PayerEmail, paymethod, PromoCode, TransactionTypeID)

            body = ex.ToString & vbCrLf & vbCrLf & body
            'Dim toAddress As String = ConfigurationManager.AppSettings("gToEmail")
            clsMyMail.SendMail(ConfigurationManager.AppSettings("gToEmail"), "Payment service", body, True)

            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return CustomerTransactionID
    End Function


    Public Shared Function SendPaymentNotification(profile As DSMembers.EUS_ProfilesRow, Money As Double, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        Dim toAddress As String = ConfigurationManager.AppSettings("gToEmail")
        Dim subject As String = "Payment successfull"

        ' default body content
        Dim body As String = <body><![CDATA[
Your payment at Goomena.com was successfull. <BR>You payed ###MONEYAMOUNT### and you gained an amount of ###CREDITS### credits. These credits are available during ###DURATION###. <BR><BR><BR>For any information don't hesitate to contact us at <A href="mailto:info@goomena.com">info@goomena.com</A>. <BR><BR>At your disposal, <BR>Goomena.com website. 
]]></body>.Value

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "PaymentMessageSubject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "PaymentMessageText", DataHelpers.ConnectionString)
            body = Regex.Replace(body, "###LOGINNAME###", profile.LoginName)
        Catch ex As Exception

        End Try


        If (body IsNot Nothing) Then
            body = body.Replace("###MONEYAMOUNT###", Money & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)
        End If

        Try
            clsMyMail.SendMail(PayerEmail, subject, body, True)
            clsMyMail.SendMail(toAddress, subject, body, True)
        Catch ex As Exception

        End Try

        clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
    End Function


    Public Shared Function SendBonusNotification(profile As DSMembers.EUS_ProfilesRow, Money As Double, _Price As EUS_Price, PayerEmail As String, CustomerID As Integer)

        Dim toAddress As String = ConfigurationManager.AppSettings("gToEmail")
        Dim subject As String = "Payment successfull"

        ' default body content
        Dim body As String = <body><![CDATA[
Your payment at Goomena.com was successfull. <BR>You payed ###MONEYAMOUNT### and you gained an amount of ###CREDITS### credits. These credits are available during ###DURATION###. <BR><BR><BR>For any information don't hesitate to contact us at <A href="mailto:info@goomena.com">info@goomena.com</A>. <BR><BR>At your disposal, <BR>Goomena.com website. 
]]></body>.Value

        Try
            Dim lagid As String = "US"
            If (Not profile.IsLAGIDNull()) Then
                lagid = profile.LAGID
            End If
            Dim o As New clsSiteLAG()
            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "BonusCreditsObtainedSubject", DataHelpers.ConnectionString)
            body = o.GetCustomStringFromPage(lagid, "GlobalStrings", "BonusCreditsObtainedText", DataHelpers.ConnectionString)
            body = Regex.Replace(body, "###LOGINNAME###", profile.LoginName)
        Catch ex As Exception

        End Try


        If (body IsNot Nothing) Then
            body = body.Replace("###MONEYAMOUNT###", Money & "euro")
            body = body.Replace("###CREDITS###", _Price.Credits)

            Dim durStr As String = GetDurationString(_Price.duration)
            body = body.Replace("###DURATION###", durStr)
        End If

        Try
            clsMyMail.SendMail(PayerEmail, subject, body, True)
            clsMyMail.SendMail(toAddress, subject, body, True)
        Catch ex As Exception

        End Try

        clsUserDoes.SendMessageFromAdministrator(subject, body, CustomerID)
    End Function


    Public Shared Function GetMessageBody(Description As String,
                                          Money As Double,
                                          TransactionID As String,
                                          p4 As String,
                                          CreditsPurchased As Integer,
                                          REMOTE_ADDR As String,
                                        HTTP_USER_AGENT As String,
                                        HTTP_REFERER As String,
                                        CustomerID As Integer,
                                        CustomReferrer As String,
                                        PayerEmail As Object,
                                        paymethod As PaymentMethods,
                                        PromoCode As Object,
                                        TransactionTypeID As Integer)

        Dim body1 As String = <sql><![CDATA[
<table>
    <tr><td align="right"><strong>Login:</strong></td><td>@Login</td></tr>
    <tr><td align="right"><strong>Money:</strong></td><td>@Money</td></tr>
    <tr><td align="right"><strong>TransactionID:</strong></td><td>@TransactionID</td></tr>
    <tr><td align="right"><strong>CreditsPurchased:</strong></td><td>@CreditsPurchased</td></tr>
    <tr><td align="right"><strong>CustomerID:</strong></td><td>@CustomerID</td></tr>
    <tr><td align="right"><strong>PayerEmail:</strong></td><td>@PayerEmail</td></tr>
    <tr><td align="right"><strong>paymethod:</strong></td><td>@paymethod (@paymethodCode)</td></tr>
    <tr><td align="right"><strong>PromoCode:</strong></td><td>@PromoCode</td></tr>
    <tr><td align="right"><strong>TransactionTypeID:</strong></td><td>@TransactionTypeID</td></tr>
    <tr><td align="right"><strong>CustomReferrer:</strong></td><td>@CustomReferrer</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right"><strong>Country:</strong></td><td>[Country]</td></tr>
    <tr><td align="right"><strong>Region:</strong></td><td>[Region]</td></tr>
    <tr><td align="right"><strong>City:</strong></td><td>[City]</td></tr>
    <tr><td align="right"><strong>Zip:</strong></td><td>[Zip]</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right"><strong>Remote Address:</strong></td><td>@REMOTE_ADDR</td></tr>
    <tr><td align="right"><strong>User Agent:</strong></td><td>@HTTP_USER_AGENT</td></tr>
    <tr><td align="right"><strong>HTTP Referer:</strong></td><td>@HTTP_REFERER</td></tr>
    <tr><td colspan="2"><hr></td></tr>
    <tr><td align="right" font="small"><strong>Description:</strong></td><td>@Description</td></tr>

    <!--<tr><td align="right" font="small"><strong>p4:</strong></td><td>@p4</td></tr>-->
</table>
]]></sql>.Value

        body1 = body1.Replace("@Description", IIf(Description IsNot Nothing, Description, ""))
        body1 = body1.Replace("@Money", Money)
        body1 = body1.Replace("@TransactionID", IIf(TransactionID IsNot Nothing, TransactionID, ""))
        body1 = body1.Replace("@p4", IIf(p4 IsNot Nothing, p4, ""))
        body1 = body1.Replace("@CreditsPurchased", CreditsPurchased)
        body1 = body1.Replace("@CustomerID", CustomerID)
        body1 = body1.Replace("@CustomReferrer", IIf(CustomReferrer IsNot Nothing, CustomReferrer, ""))
        body1 = body1.Replace("@PayerEmail", IIf(PayerEmail IsNot Nothing, PayerEmail, ""))
        body1 = body1.Replace("@paymethodCode", CType(paymethod, Integer).ToString())
        body1 = body1.Replace("@paymethod", paymethod.ToString())
        body1 = body1.Replace("@PromoCode", IIf(PromoCode IsNot Nothing, PromoCode, ""))
        body1 = body1.Replace("@TransactionTypeID", TransactionTypeID)
        body1 = body1.Replace("@REMOTE_ADDR", IIf(REMOTE_ADDR IsNot Nothing, REMOTE_ADDR, ""))
        body1 = body1.Replace("@HTTP_USER_AGENT", IIf(HTTP_USER_AGENT IsNot Nothing, HTTP_USER_AGENT, ""))
        body1 = body1.Replace("@HTTP_REFERER", IIf(HTTP_REFERER IsNot Nothing, HTTP_REFERER, ""))


        Try
            Dim cust As DSMembers.EUS_ProfilesRow = DataHelpers.GetEUS_Profiles_ByProfileIDRow(CustomerID)
            If (cust IsNot Nothing) Then
                body1 = body1.Replace("@Login", cust.LoginName)

                Try
                    If (Not cust.IsCountryNull() AndAlso Not String.IsNullOrEmpty(cust.Country)) Then
                        body1 = body1.Replace("[Country]", ProfileHelper.GetCountryName(cust.Country))
                    Else
                        body1 = body1.Replace("[Country]", "Country not found in profile")
                    End If
                Catch ex As Exception
                End Try

                If (Not cust.Is_RegionNull() AndAlso Not String.IsNullOrEmpty(cust._Region)) Then
                    body1 = body1.Replace("[Region]", cust._Region)
                Else
                    body1 = body1.Replace("[Region]", "Region not found in profile")
                End If

                If (Not cust.IsCityNull() AndAlso Not String.IsNullOrEmpty(cust.City)) Then
                    body1 = body1.Replace("[City]", cust.City)
                Else
                    body1 = body1.Replace("[City]", "City not found in profile")
                End If

                If (Not cust.IsZipNull() AndAlso Not String.IsNullOrEmpty(cust.Zip)) Then
                    body1 = body1.Replace("[Zip]", cust.Zip)
                Else
                    body1 = body1.Replace("[Zip]", "Zip not found in profile")
                End If

            Else
                body1 = body1.Replace("@Login", "Customer NOT FOUND")
                body1 = body1.Replace("[Country]", "Customer NOT FOUND")
                body1 = body1.Replace("[Region]", "Customer NOT FOUND")
                body1 = body1.Replace("[City]", "Customer NOT FOUND")
                body1 = body1.Replace("[Zip]", "Customer NOT FOUND")
            End If

        Catch ex As Exception
            body1 = body1.Replace("@Login", ex.Message)
            body1 = body1.Replace("[Country]", "Exception")
            body1 = body1.Replace("[Region]", "Exception")
            body1 = body1.Replace("[City]", "Exception")
            body1 = body1.Replace("[Zip]", "Exception")
        End Try

        'Dim body As String = "Description:" & vbCrLf & Description & vbCrLf & vbCrLf & _
        '    "Money:" & vbCrLf & Money & vbCrLf & vbCrLf & _
        '    "TransactionID:" & vbCrLf & TransactionID & vbCrLf & vbCrLf & _
        '    "p4:" & vbCrLf & p4 & vbCrLf & vbCrLf & _
        '    "CreditsPurchased:" & vbCrLf & CreditsPurchased & vbCrLf & vbCrLf & _
        '    "REMOTE_ADDR:" & vbCrLf & REMOTE_ADDR & vbCrLf & vbCrLf & _
        '    "HTTP_USER_AGENT:" & vbCrLf & HTTP_USER_AGENT & vbCrLf & vbCrLf & _
        '    "HTTP_REFERER:" & vbCrLf & HTTP_REFERER & vbCrLf & vbCrLf & _
        '    "CustomerID:" & vbCrLf & CustomerID & vbCrLf & vbCrLf & _
        '    "CustomReferrer:" & vbCrLf & CustomReferrer & vbCrLf & vbCrLf & _
        '    "PayerEmail:" & vbCrLf & PayerEmail & vbCrLf & vbCrLf & _
        '    "paymethod:" & vbCrLf & paymethod & vbCrLf & vbCrLf & _
        '    "PromoCode:" & vbCrLf & PromoCode & vbCrLf & vbCrLf & _
        '    "TransactionTypeID:" & vbCrLf & TransactionTypeID

        Return body1
    End Function


    Public Shared Function GetDurationString(duration As Integer, Optional LAGID As String = "US") As String
        Dim text As String = ""
        Dim o As New clsSiteLAG()

        If (duration = 120) Then
            text = "3 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 180) Then
            text = "6 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 270) Then
            text = "9 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 365) Then
            text = "1 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Year", DataHelpers.ConnectionString)
        ElseIf (duration = 548) Then
            text = "18 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Months", DataHelpers.ConnectionString)
        ElseIf (duration = 730) Then
            text = "2 " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Years", DataHelpers.ConnectionString)
        Else
            text = duration & " " & o.GetCustomStringFromPage(LAGID, "GlobalStrings", "Days", DataHelpers.ConnectionString)
        End If

        Return text
    End Function


    Public Shared Function IsProfileActive(profileId As Integer) As Boolean
        Dim isactive As Boolean = False
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            Dim rec = From itm In _CMSDBDataContext.EUS_Profiles
            Where itm.ProfileID = profileId AndAlso _
                  (((itm.Status = ProfileStatusEnum.NewProfile OrElse itm.Status = ProfileStatusEnum.Updating) AndAlso itm.IsMaster = False) OrElse _
                   (itm.Status = ProfileStatusEnum.Approved AndAlso itm.IsMaster = True))
            Select itm

            isactive = rec.Count() > 0
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return isactive
    End Function


End Class

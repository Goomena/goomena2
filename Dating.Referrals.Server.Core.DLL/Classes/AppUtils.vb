﻿Public Class AppUtils

    Public Shared Function FormatFileSize(ByVal FileSizeBytes As Long) As String
        Dim sizeTypes() As String = {"B", "KB", "MB", "GB"}
        Dim Len As Decimal = FileSizeBytes
        Dim sizeType As Integer = 0
        Do While Len > 1024
            Len = Decimal.Round(Len / 1024, 2)
            sizeType += 1
            If sizeType >= sizeTypes.Length - 1 Then Exit Do
        Loop

        Dim Resp As String = Len.ToString & " " & sizeTypes(sizeType)
        Return Resp
    End Function

    Public Shared Function IsNull_ToString(o As Object, Optional defaultValue As String = "") As String
        If (o IsNot System.DBNull.Value) Then
            Return o.ToString()
        Else
            Return defaultValue
        End If
    End Function

    ''' <summary>
    ''' Strip HTML tags.
    ''' </summary>
    Public Shared Function StripTags(ByVal html As String) As String
        ' Remove HTML tags.
        Dim str As String = Text.RegularExpressions.Regex.Replace(html, "&nbsp;", "")
        str = Text.RegularExpressions.Regex.Replace(str, "&euro;", "EUR")
        str = Text.RegularExpressions.Regex.Replace(str, "<.*?>", "")
        Return str
    End Function



    Public Shared Function StringToStream(ByVal str As String) As System.IO.MemoryStream
        Dim ms As New System.IO.MemoryStream
        Dim enc As New System.Text.UTF8Encoding
        Dim arrBytData() As Byte = enc.GetBytes(str)
        ms.Write(arrBytData, 0, arrBytData.Length)
        ms.Position = 0
        Return ms
    End Function

    Public Shared Function MakeMD5(ByVal str As String) As String
        str = (BitConverter.ToString(New System.Security.Cryptography.MD5CryptoServiceProvider().ComputeHash(AppUtils.StringToStream(str)))).Trim.ToUpper.Replace("-", "").Trim
        Return str
    End Function

End Class

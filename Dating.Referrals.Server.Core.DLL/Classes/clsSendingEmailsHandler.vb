﻿Public Class clsSendingEmailsHandler
    Implements IDisposable

    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)


    Public Sub AddEmail(emailTo As String, emailSubject As String,
                        emailBody As String, customerid As Integer?,
                        IsHtmlBody As Boolean, EmailAction As String,
                        SMTPServerCredentialsID As Integer?)
        Try
            Dim email As New Core.DLL.SYS_EmailsQueue


            email.DateCreated = Date.UtcNow
            email.Body = emailBody
            email.EmailTo = emailTo
            email.Subject = emailSubject
            email.CustomerId = customerid
            email.EmailAction = EmailAction
            email.IsHtmlBody = IsHtmlBody
            email.SMTPServerCredentialsID = SMTPServerCredentialsID

            _CMSDBDataContext.SYS_EmailsQueues.InsertOnSubmit(email)
            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub


    Public Function CheckEmailIfExists(emailTo As String, emailSubject As String, customerid As Integer?, Optional EmailAction As String = Nothing) As Boolean
        Dim result As Boolean
        Try
            Dim email As Core.DLL.SYS_EmailsQueue = (From itm In _CMSDBDataContext.SYS_EmailsQueues
                    Where itm.EmailTo = emailTo AndAlso itm.Subject = emailSubject AndAlso itm.CustomerId = customerid
                   Select itm).FirstOrDefault()
            result = (email IsNot Nothing)

        Catch ex As Exception
            Throw
        Finally
        End Try
        Return result
    End Function


    Public Function GetNextEmail() As Core.DLL.SYS_EmailsQueue
        Dim file As Core.DLL.SYS_EmailsQueue
        Try
            file = (From itm In _CMSDBDataContext.SYS_EmailsQueues
                    Where itm.DateSent Is Nothing
                   Select itm).FirstOrDefault()

            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
        End Try

        Return file
    End Function


    Public Function GetNextEmailToSend() As Core.DLL.SYS_EmailsQueue
        Dim file As Core.DLL.SYS_EmailsQueue
        Try
            file = (From itm In _CMSDBDataContext.SYS_EmailsQueues
                    Where itm.DateSent Is Nothing AndAlso (itm.Exception Is Nothing OrElse itm.Exception = "")
                   Select itm).FirstOrDefault()

            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
        End Try

        Return file
    End Function


    Public Sub SetEmailSent(email1 As Core.DLL.SYS_EmailsQueue)
        Try
            Dim email As Core.DLL.SYS_EmailsQueue = (From itm In _CMSDBDataContext.SYS_EmailsQueues
                   Where itm.MessageQueueId = email1.MessageQueueId
                   Select itm).FirstOrDefault()


            email.DateSent = email1.DateSent

            If (Len(email1.Exception) > 1023) Then email1.Exception = email1.Exception.Remove(1023)
            email.Exception = email1.Exception


            _CMSDBDataContext.SubmitChanges()
        Catch ex As Exception
            Throw
        Finally
        End Try

    End Sub


    Public Sub DeleteSentEmails()
        Try
            Dim email = From itm In _CMSDBDataContext.SYS_EmailsQueues
                   Where itm.DateSent IsNot Nothing
                   Select itm


            _CMSDBDataContext.SYS_EmailsQueues.DeleteAllOnSubmit(email)
            _CMSDBDataContext.SubmitChanges()
        Catch ex As Exception
            Throw
        Finally
        End Try

    End Sub


    Public Sub Dispose() Implements IDisposable.Dispose
        If (_CMSDBDataContext IsNot Nothing) Then _CMSDBDataContext.Dispose()
    End Sub

End Class

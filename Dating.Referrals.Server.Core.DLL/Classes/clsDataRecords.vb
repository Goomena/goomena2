﻿
Public Class clsDataRecordLogin

    Public LoginName As String
    Public Password As String
    'Public SiteName As String
    Public HardwareID As String
    Public HASH As String

End Class


Public Class clsDataRecordLoginMember

    Public LoginName As String
    Public Password As String
    'Public SiteName As String
    'Public HardwareID As String
    Public HASH As String
    Public RecordLastLogin As Integer = -10
    Public AllowReferral As Boolean
End Class

Public Class clsDataRecordHeader

    Public Token As String
    Public LoginName As String
    Public Password As String
    Public SiteName As String
    Public HardwareID As String
    Public HASH As String

End Class

<Serializable()> _
Public Class clsDataRecordLoginMemberReturn

    Public IsValid As Boolean
    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public Message As String
    Public ExtraMessage As String

    ' Public UsersRow As UniCMSDB.SYS_UsersRow

    ' Public AllowSiteManage As Boolean
    '  Public AllowCMS As Boolean

    Public ReferrerCustomerID As Integer
    Public ReferrerParentID As Integer

    Public Status As Integer
    Public ProfileID As Integer
    Public MirrorProfileID As Integer
    Public LoginName As String
    Public FirstName As String
    Public LastName As String
    Public GenderId As Integer
    Public Zip As String
    Public latitude As Double?
    Public longitude As Double?
    Public _Region As String
    Public Birthday As Date
    Public Role As String
    Public LAGID As String
    Public CustomReferrer As String
    Public LookingFor_ToMeetMaleID As Boolean
    Public LookingFor_ToMeetFemaleID As Boolean

    Public Token As String
    Public HASH As String




    Public Sub Fill(REF_ReferrerProfilesRow As DSReferrers.REF_ReferrerProfilesRow)

        Me.ReferrerCustomerID = REF_ReferrerProfilesRow.CustomerID

        If (Not REF_ReferrerProfilesRow.IsSiteProfileIDNull()) Then Me.ProfileID = REF_ReferrerProfilesRow.SiteProfileID
        If (Not REF_ReferrerProfilesRow.IsSiteMirrorProfileIDNull()) Then Me.MirrorProfileID = IIf(REF_ReferrerProfilesRow.SiteMirrorProfileID = 0, Me.ProfileID, REF_ReferrerProfilesRow.SiteMirrorProfileID)


        Me.LoginName = REF_ReferrerProfilesRow.LoginName

        If (Not REF_ReferrerProfilesRow.IsStatusNull()) Then Me.Status = REF_ReferrerProfilesRow.Status
        If (Not REF_ReferrerProfilesRow.IsReferrerParentIdNull()) Then Me.ReferrerParentID = REF_ReferrerProfilesRow.ReferrerParentId
        If (Not REF_ReferrerProfilesRow.IsFirstNameNull()) Then Me.FirstName = REF_ReferrerProfilesRow.FirstName
        If (Not REF_ReferrerProfilesRow.IsLastNameNull()) Then Me.LastName = REF_ReferrerProfilesRow.LastName
        If (Not REF_ReferrerProfilesRow.IsGenderIdNull()) Then Me.GenderId = REF_ReferrerProfilesRow.GenderId
        If (Not REF_ReferrerProfilesRow.IsZipNull()) Then Me.Zip = REF_ReferrerProfilesRow.Zip
        If (Not REF_ReferrerProfilesRow.Is_RegionNull()) Then Me._Region = REF_ReferrerProfilesRow._Region
        If (Not REF_ReferrerProfilesRow.IsLAGIDNull()) Then Me.LAGID = REF_ReferrerProfilesRow.LAGID
        If (Not REF_ReferrerProfilesRow.IsCustomReferrerNull()) Then Me.CustomReferrer = REF_ReferrerProfilesRow.CustomReferrer


        If (Not REF_ReferrerProfilesRow.IsRoleNull()) Then
            If (Not String.IsNullOrEmpty(REF_ReferrerProfilesRow.Role)) Then
                Me.Role = REF_ReferrerProfilesRow.Role.ToUpper()
            Else
                Me.Role = "MEMBER"
            End If
        End If

        Dim Token As String = Library.Public.clsHash.ComputeHash(Me.LoginName & "^Token$$$", "SHA1") 'DataRecordLogin.HardwareID & "^^" &
        Me.Token = Token

        If REF_ReferrerProfilesRow.Status > ProfileStatusEnum.None Then
            Me.HasErrors = False
            Me.Message = "OK"
            Me.IsValid = True
        Else
            Me.HasErrors = True
            Me.Message = "The user is not Active"
            Me.IsValid = False
        End If
    End Sub


End Class


Public Class clsDataRecordLoginReturn

    Public IsValid As Boolean
    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public Message As String
    Public ExtraMessage As String

    'Public UsersRow As DSSYSData.SYS_UsersRow

    ' Public AllowSiteManage As Boolean
    '  Public AllowCMS As Boolean

    Public UserID As Integer
    Public LoginName As String
    Public UserRole As Integer
    'Public LAGID As String

    'Public AdminRights As Boolean
    'Public ManagerRights As Boolean
    'Public ContextRights As Boolean

    ' Public CMSAllowAddContext As Boolean
    ' Public CMSAllowEditContext As Boolean
    ' Public CMSAllowDeleteContext As Boolean

    ' Public CMSAllowAddMenuItems As Boolean
    ' Public CMSAllowEditMenuItems As Boolean
    ' Public CMSAllowDeleteMenuItems As Boolean

    Public Token As String


    Public HASH As String
End Class


Public Class clsWSHeaders
    Public HardwareID As String
    Public LoginName As String
    Public Password As String
    Public Token As String
    Public ServerStartUTCTime As String
    Public ServerEndUTCTime As String
End Class


Public Class clsDataRecordErrorsReturn

    Public HasErrors As Boolean
    Public ErrorCode As Integer
    Public Message As String
    Public ExtraMessage As String

End Class


'Public Class clsDataRecordIPN
'    Property PayProviderAmount As String
'    Property PaymentDateTime As String
'    Property PayTransactionID As String
'    Property CustomerID As String
'    Property SalesSiteProductID As String
'    Property SaleDescription As Object
'    Property PayProviderTransactionID As String
'    Property SaleQuantity As Integer
'    Property custopmerIP As String
'    Property CustomReferrer As String
'    Property PromoCode As Object
'    Property TransactionTypeID As Object
'    Property BuyerInfo As clsBuyerInfo
'    Property VerifyHASH As String

'    'Function VerifyHASH() As String
'    '    Dim sData As String
'    '    sData = Me.PayProviderAmount & "+" & Me.PaymentDateTime & "+" & Me.PayTransactionID & "+" & Me.CustomerID & "+" & Me.SalesSiteProductID & "Extr@Ded0men@"
'    '    Dim h As New Library.Public.clsHash
'    '    Dim Code As String = Library.Public.clsHash.ComputeHash(sData, "SHA512")
'    '    Return Code
'    'End Function

'End Class



'Public Class clsDataRecordIPNReturn
'    Property HasErrors As Boolean
'    Property ErrorCode As Integer
'    Property Message As String
'End Class

'Public Class clsBuyerInfo

'    Property PayerEmail As Object

'End Class

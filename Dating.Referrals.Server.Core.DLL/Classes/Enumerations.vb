﻿
Public Enum NotificationType
    None = 0
    Likes = 1
    Offers = 2
    Message = 4
    NewMembersNear = 5
    Unlock = 6
    AutoNotification = 7
    AutoNotificationForPhoto = 8
End Enum


Public Enum SearchSortEnum
    None = 0
    NewestMember = 1
    OldestMember = 2
    RecentlyLoggedIn = 3
    NearestDistance = 4
End Enum



Public Enum MyListsSortEnum
    None = 0
    Recent = 3
    Oldest = 4
End Enum



Public Enum OffersSortEnum
    None = 0
    OfferAmountHighest = 1
    OfferAmountLowest = 2
    RecentOffers = 3
    OldestOffers = 4
End Enum


Public Enum DatesSortEnum
    None = 0
    NewDates = 2
    ByDate = 4
    ByStatus = 5
    ByLastDating = 6
End Enum




''' <summary>
''' Match to EUS_LISTS_AccountType database table,
''' Integer value maps to AccountTypeId column
''' </summary>
''' <remarks></remarks>
Public Enum GenerousAttractiveEnum
    None = 1
    Generous = 2
    Attractive = 3
End Enum



Public Enum PhotosFilterEnum
    None = 0
    AllPhotos = 1
    NewPhotos = 2
    ApprovedPhotos = 3
    RejectedPhotos = 4
    DeletedPhotos = 5
    AutoApprovedPhotos = 6
    CustomerDefaultPhoto = 7
End Enum




Public Enum ProfileStatusEnum
    None = 0
    NewProfile = 1
    Updating = 2
    Approved = 4
    Rejected = 8
    Deleted = 16
    DeletedByUser = 18
    AutoApprovedProfile = -100
    ApprovedProfileWithPhotos = -200
    ApprovedProfileWithoutPhotos = -300
    MembersOnline = -400
    ApprovedOrUpdating = -500
    ApprovedOrUpdatingProfileWithPhotos = -510
    ApprovedOrUpdatingProfileWithoutPhotos = -520
    ReferralUpdating = -530
End Enum






Public Enum TypeOfDatingEnum
    None = 0
    ShortTermRelationship = 1
    Friendship = 2
    LongTermRelationship = 3
    MutuallyBeneficialArrangements = 4
    MarriedDating = 5
    AdultDating_Casual = 6
End Enum



Public Enum PaymentMethods
    PayPal = 1
    DaoPay = 2
    MoneyBookers = 3
    AlertPay = 4
    PaySafe = 5
    CreditCard = 6
    WebMoney = 7
    fastspring = 8
    'TwoCO = 101
    'Regnow = 102
End Enum

'Public Enum PaymentMethods
'    None = 0
'    AlertPay = 1
'    PaySafe = 2
'    CreditCard = 3
'    WebMoney = 4
'    PayPal = 5
'    TwoCO = 6
'    Regnow = 7
'End Enum


Public Enum UnlockType
    None = 0
    UNLOCK_CONVERSATION = 1
    UNLOCK_MESSAGE_READ = 2
    UNLOCK_MESSAGE_SEND = 3
End Enum


Public Enum MessagesViewEnum
    None = 0
    INBOX = 1
    SENT = 2
    CONVERSATION = 4
    NEWMESSAGES = 8
    TRASH = 16
    SEND_NEW_MESSAGE = 32
End Enum



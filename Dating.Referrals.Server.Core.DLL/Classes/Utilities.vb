﻿Public Class Utilities


    ''' <summary>
    ''' Append to file
    ''' </summary>
    ''' <param name="file_name">file path</param>
    ''' <param name="message">string to add</param>
    ''' <remarks></remarks>
    Public Shared Sub Append_File(ByVal file_name As String, ByVal message As String)
        Dim numberofretrys As Integer = 5
        Dim trys As Integer = 0
retry:

        Try
            If Not IO.File.Exists(file_name) Then
                Try
                    IO.File.WriteAllText(file_name, "")
                Catch ex As Exception

                End Try
            End If
        Catch ex As Exception

        End Try

        Try

            If Not String.IsNullOrEmpty(file_name) Then
                Dim objWriter As New System.IO.StreamWriter(file_name, True)
                objWriter.WriteLine(message)
                objWriter.Close()
            End If
        Catch ex As Exception
            trys += 1
            If trys < numberofretrys Then
                Threading.Thread.Sleep(2000)
                GoTo retry
            End If
        End Try
    End Sub


End Class



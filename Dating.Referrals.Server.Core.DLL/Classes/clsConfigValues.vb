﻿Public Class clsConfigValues

    Public Property auto_approve_new_profiles As String
    Public Property auto_approve_photos As String
    Public Property auto_approve_updating_profiles As String
    Public Property site_name As String
    Public Property image_thumb_url As String
    Public Property image_url As String
    Public Property image_thumb_server_path As String
    Public Property image_server_path As String
    Public Property view_profile_url As String
    Public Property zone_a As String
    Public Property zone_b As String
    Public Property zone_c As String
    Public Property members_online_minutes As String
    Public Property ip_to_map As String

    Public Property gref_documents_server_path As String
    Public Property gref_documents_url_path As String

    Public Property referrer_commission_conv_rate As Double
    Public Property referrer_commission_level_0 As Double
    Public Property referrer_commission_level_1 As Double
    Public Property referrer_commission_level_2 As Double
    Public Property referrer_commission_level_3 As Double
    Public Property referrer_commission_level_4 As Double
    Public Property referrer_commission_level_5 As Double
    Public Property referrer_commission_pending_percent As Double
    Public Property referrer_commission_pending_period As Integer

    Public Property ratio_eur_usd As Double
    Public Property ratio_eur_lek As Double

    Public Property credits_bonus_codes As Integer
    Public Property validate_email_address_normal As Boolean
    Public Property photos_max_approved As Integer


    Public Sub New()
        Dim configValue As String = Nothing
        Dim _CMSDBDataContext As CMSDBDataContext = Nothing

        Try
            _CMSDBDataContext = New CMSDBDataContext(DataHelpers.ConnectionString)
            Dim access As List(Of Core.DLL.SYS_Config) = (From itm In _CMSDBDataContext.SYS_Configs
                                                 Select itm).ToList()

            For Each itm In access
                Select Case itm.ConfigName
                    Case "members_online_minutes" : members_online_minutes = itm.ConfigValue
                    Case "ip_to_map" : ip_to_map = itm.ConfigValue
                    Case "auto_approve_new_profiles" : auto_approve_new_profiles = itm.ConfigValue
                    Case "auto_approve_photos" : auto_approve_photos = itm.ConfigValue
                    Case "auto_approve_updating_profiles" : auto_approve_updating_profiles = itm.ConfigValue
                    Case "image_thumb_url" : image_thumb_url = itm.ConfigValue
                    Case "image_url" : image_url = itm.ConfigValue
                    Case "image_thumb_server_path" : image_thumb_server_path = itm.ConfigValue
                    Case "image_server_path" : image_server_path = itm.ConfigValue
                    Case "site_name" : site_name = itm.ConfigValue
                    Case "view_profile_url" : view_profile_url = itm.ConfigValue
                    Case "zone_a" : zone_a = itm.ConfigValue
                    Case "zone_b" : zone_b = itm.ConfigValue
                    Case "zone_c" : zone_c = itm.ConfigValue

                    Case "gref_documents_server_path" : gref_documents_server_path = itm.ConfigValue
                    Case "gref_documents_url_path" : gref_documents_url_path = itm.ConfigValue

                    Case "ratio_eur_usd" : ratio_eur_usd = itm.ConfigValueDouble
                    Case "ratio_eur_lek" : ratio_eur_lek = itm.ConfigValueDouble
                    Case "credits_bonus_codes" : credits_bonus_codes = itm.ConfigValueDouble
                    Case "photos_max_approved" : photos_max_approved = itm.ConfigValueDouble

                    Case "validate_email_address_normal" : validate_email_address_normal = itm.ConfigValueDouble

                    Case "referrer_commission_conv_rate"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_conv_rate = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_0"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_0 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_1"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_1 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_2"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_2 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_3"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_3 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_4"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_4 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_level_5"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_level_5 = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_pending_percent"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_pending_percent = itm.ConfigValueDouble.Value
                        End If

                    Case "referrer_commission_pending_period"
                        If (itm.ConfigValueDouble.HasValue) Then
                            referrer_commission_pending_period = itm.ConfigValueDouble.Value
                        End If

                End Select
            Next

        Catch ex As Exception
            Throw
        Finally
            If (_CMSDBDataContext IsNot Nothing) Then _CMSDBDataContext.Dispose()
        End Try

    End Sub



    Public Shared Function GetSYS_ConfigValue(key As String) As String
        Dim configValue As String = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim access As Core.DLL.SYS_Config = (From itm In _CMSDBDataContext.SYS_Configs
                                                 Where itm.ConfigName = key.ToLower()
                                                 Select itm).FirstOrDefault()

            configValue = access.ConfigValue
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return configValue
    End Function

    Public Shared Function GetSYS_ConfigValueDouble(ByVal key As String) As String
        Dim configValue As String = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim access As Core.DLL.SYS_Config = (From itm In _CMSDBDataContext.SYS_Configs
                                                Where itm.ConfigName = key.ToLower()
                                                Select itm).FirstOrDefault()

            configValue = access.ConfigValueDouble
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return configValue
    End Function


    Private Shared __ratio_eur_lek As Double = -1
    Public Shared Function Get__ratio_eur_lek() As Double
        If (__ratio_eur_lek = -1) Then
            __ratio_eur_lek = clsConfigValues.GetSYS_ConfigValueDouble("ratio_eur_lek")
        End If
        Return __ratio_eur_lek
    End Function


    Private Shared __credits_bonus_codes As Integer = -1
    Public Shared Function Get__credits_bonus_codes() As Integer
        If (__credits_bonus_codes = -1) Then
            __credits_bonus_codes = clsConfigValues.GetSYS_ConfigValueDouble("credits_bonus_codes")
        End If
        Return __credits_bonus_codes
    End Function


    Private Shared __validate_email_address_normal As Boolean?
    Public Shared Function Get__validate_email_address_normal() As Boolean
        If (Not __validate_email_address_normal.HasValue) Then
            __validate_email_address_normal = clsConfigValues.GetSYS_ConfigValueDouble("validate_email_address_normal")
        End If
        Return clsNullable.NullTo(__validate_email_address_normal)
    End Function


    Private Shared __photos_max_approved As Integer = -1
    Public Shared Function Get__photos_max_approved() As Integer
        If (__photos_max_approved = -1) Then
            __photos_max_approved = clsConfigValues.GetSYS_ConfigValueDouble("photos_max_approved")
        End If
        Return __photos_max_approved
    End Function


    Private Shared __members_online_minutes As Integer = -1
    Public Shared Function Get__members_online_minutes() As Integer
        If (__members_online_minutes = -1) Then
            Dim tmp As String = clsConfigValues.GetSYS_ConfigValue("members_online_minutes")
            If (Not Integer.TryParse(tmp, __members_online_minutes)) Then __members_online_minutes = 20
        End If
        Return __members_online_minutes
    End Function


    Private Shared __payment_provider_percents As Integer = -1
    Public Shared Function Get__payment_provider_percents() As Integer
        If (__payment_provider_percents = -1) Then
            __payment_provider_percents = clsConfigValues.GetSYS_ConfigValueDouble("payment_provider_percents")
        End If
        Return __payment_provider_percents
    End Function

    Public Shared Sub ResetCache()
        __ratio_eur_lek = -1
        __credits_bonus_codes = -1
        __validate_email_address_normal = Nothing
        __photos_max_approved = -1
        __members_online_minutes = -1
        __payment_provider_percents = -1
    End Sub

End Class

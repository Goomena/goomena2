﻿Imports Dating.Referrals.Server.Core.DLL
Imports System.Web
Imports Dating.Referrals.Server.Core.DLL.DSMembers
Imports System.Text.RegularExpressions

Public Class clsUserDoes


#Region "Types"

    Public Class NewOfferParameters
        Public Property userIdReceiver As Integer
        Public Property userIdWhoDid As Integer
        Public Property offerAmount As Integer
        Public Property parentOfferId As Integer
        Public Property messageText1 As String
        Public Property messageText2 As String

        Public Property childOfferId As Integer

    End Class



    Public Class clsLoadProfilesViewsResult

        Property CurrentMemberProfileID As Integer
        Property CurrentMemberProfileViewUrl As String
        Property CurrentMemberLoginName As String
        Property CurrentMemberImageUrl As String

        Property OtherMemberProfileID As Integer
        Property OtherMemberProfileViewUrl As String
        Property OtherMemberLoginName As String
        Property OtherMemberImageUrl As String

    End Class

#End Region


    Public Shared Sub MarkAsViewed(userIdViewed As Integer, userIdWhoDid As Integer)

        If (userIdViewed = 0 OrElse userIdWhoDid = 0 OrElse userIdViewed = userIdWhoDid) Then
            Return
        End If


        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            Dim rec = _CMSDBDataContext.EUS_ProfilesVieweds.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso itm.ToProfileID = userIdViewed).Count()

            If (rec = 0) Then
                Dim _EUS_ProfilesViewedRec As New EUS_ProfilesViewed()
                _EUS_ProfilesViewedRec.DateTimeToCreate = DateTime.UtcNow
                _EUS_ProfilesViewedRec.FromProfileID = userIdWhoDid
                _EUS_ProfilesViewedRec.ToProfileID = userIdViewed


                _CMSDBDataContext.EUS_ProfilesVieweds.InsertOnSubmit(_EUS_ProfilesViewedRec)
                _CMSDBDataContext.SubmitChanges()
            End If


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub



    Public Shared Sub MarkAsFavorite(userIdFavorited As Integer, userIdWhoDid As Integer)

        If (userIdFavorited = 0 OrElse userIdWhoDid = 0 OrElse userIdFavorited = userIdWhoDid) Then
            Return
        End If

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            Dim rec = _CMSDBDataContext.EUS_ProfilesFavorites.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso _
                                                                        itm.ToProfileID = userIdFavorited).Count()

            If (rec = 0) Then
                'If (rec Is Nothing) Then
                Dim _EUS_ProfilesFavoriteRec As New EUS_ProfilesFavorite()
                _EUS_ProfilesFavoriteRec.DateTimeToCreate = DateTime.UtcNow
                _EUS_ProfilesFavoriteRec.FromProfileID = userIdWhoDid
                _EUS_ProfilesFavoriteRec.ToProfileID = userIdFavorited


                _CMSDBDataContext.EUS_ProfilesFavorites.InsertOnSubmit(_EUS_ProfilesFavoriteRec)
                _CMSDBDataContext.SubmitChanges()
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub


    Public Shared Sub MarkAsUnfavorite(userIdUnfavorited As Integer, userIdWhoDid As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            Dim rec As EUS_ProfilesFavorite = _CMSDBDataContext.EUS_ProfilesFavorites.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso _
                                                                                                itm.ToProfileID = userIdUnfavorited).FirstOrDefault()

            If (rec IsNot Nothing) Then
                _CMSDBDataContext.EUS_ProfilesFavorites.DeleteOnSubmit(rec)
                _CMSDBDataContext.SubmitChanges()
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub




    Public Shared Sub MarkAsBlocked(userIdBlocked As Integer, userIdWhoDid As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            Dim rec = _CMSDBDataContext.EUS_ProfilesBlockeds.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso _
                                                                       itm.ToProfileID = userIdBlocked).Count()

            If (rec = 0) Then
                'If (rec Is Nothing) Then
                Dim _EUS_ProfilesBlockedRec As New EUS_ProfilesBlocked()
                _EUS_ProfilesBlockedRec.DateTimeToCreate = DateTime.UtcNow
                _EUS_ProfilesBlockedRec.FromProfileID = userIdWhoDid
                _EUS_ProfilesBlockedRec.ToProfileID = userIdBlocked


                _CMSDBDataContext.EUS_ProfilesBlockeds.InsertOnSubmit(_EUS_ProfilesBlockedRec)
                _CMSDBDataContext.SubmitChanges()
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub



    Public Shared Sub MarkAsUnblocked(userIdUnblocked As Integer, userIdWhoDid As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            Dim rec As EUS_ProfilesBlocked = _CMSDBDataContext.EUS_ProfilesBlockeds.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso _
                                                                                              itm.ToProfileID = userIdUnblocked).FirstOrDefault()

            If (rec IsNot Nothing) Then
                _CMSDBDataContext.EUS_ProfilesBlockeds.DeleteOnSubmit(rec)
                _CMSDBDataContext.SubmitChanges()
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub



    Public Function IsViewed(userIdViewed As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Integer = 0

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = _CMSDBDataContext.EUS_ProfilesVieweds.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso itm.ToProfileID = userIdViewed).Count()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return (rec > 0)
    End Function


    Public Shared Function IsMessageSent(userIdRecipient As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Integer = 0

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = _CMSDBDataContext.EUS_Messages.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso itm.ToProfileID = userIdRecipient AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)).Count()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return (rec > 0)
    End Function



    Public Shared Function HasCommunication(otherUserId As Integer, currentUserId As Integer) As Boolean
        Dim rec As Integer = 0

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_UnlockedConversations
                  Where (
                            (itm.ToProfileId = otherUserId AndAlso itm.FromProfileId = currentUserId) OrElse _
                            (itm.FromProfileId = otherUserId AndAlso itm.ToProfileId = currentUserId)
                        ) AndAlso _
                    itm.DateTimeCreated <= New DateTime(2013, 2, 8)
                  Select itm).Count()

            'ModGlobals.AllowUnlimited
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try


        Return (rec > 0)
    End Function




    Public Shared Function GetUnlockedConversation(otherUserId As Integer, currentUserId As Integer) As EUS_UnlockedConversation
        Dim rec As EUS_UnlockedConversation = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_UnlockedConversations
                  Where (itm.ToProfileId = otherUserId AndAlso itm.FromProfileId = currentUserId) OrElse _
                            (itm.FromProfileId = otherUserId AndAlso itm.ToProfileId = currentUserId)
                  Order By itm.UnlockedConversationId Descending
                  Select itm).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function IsFavorited(userIdFavorited As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Integer = 0

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = _CMSDBDataContext.EUS_ProfilesFavorites.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso itm.ToProfileID = userIdFavorited).Count()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return (rec > 0)
    End Function


    Public Shared Function IsBlocked(userIdBlocked As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Integer = 0

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = _CMSDBDataContext.EUS_ProfilesBlockeds.Where(Function(itm) itm.FromProfileID = userIdWhoDid AndAlso itm.ToProfileID = userIdBlocked).Count()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return (rec > 0)
    End Function




    Public Shared Function GetLastOffer(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_Offers
                  Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                         (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                        (itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW OrElse itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER) AndAlso _
                        itm.ChildOfferID = 0
                  Select itm).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function



    Public Shared Function GetOfferByOfferId(userId As Integer, offerId As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_Offers
                  Where itm.OfferID = offerId AndAlso (itm.FromProfileID = userId OrElse itm.ToProfileID = userId)
                  Select itm).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function GetLastOfferWithAmount(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_Offers
                  Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                         (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                         itm.Amount > 0
                  Order By itm.DateTimeToCreate Descending
                  Select itm).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function GetAnyWinkPending(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_Offers
                  Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                         (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                         (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK) AndAlso _
                         itm.StatusID = ProfileHelper.OfferStatusID_PENDING
                  Select itm).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function GetAnyPokePending(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_Offers
                  Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                         (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                         (itm.OfferTypeID = ProfileHelper.OfferTypeID_POKE) AndAlso _
                         itm.StatusID = ProfileHelper.OfferStatusID_PENDING
                  Select itm).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function GetMyWink(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_Offers
                  Where (itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) AndAlso _
                        (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK)
                  Select itm).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function GetMyPendingWink(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_Offers
                  Where (itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) AndAlso _
                        (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK) AndAlso _
                        (itm.StatusID = ProfileHelper.OfferStatusID_PENDING)
                  Select itm).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function IsAnyWink(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Boolean

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = ((From itm In _CMSDBDataContext.EUS_Offers
                  Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                         (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                        (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK)
                  Select itm).Count() > 0)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function IsAnyWinkOrIsAnyOffer(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Boolean

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = ((From itm In _CMSDBDataContext.EUS_Offers
                  Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                         (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) AndAlso _
                        (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK OrElse itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW OrElse itm.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER)
                  Select itm).Count() > 0)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function IsAnyMessageExchanged(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Boolean

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            ' get any message
            rec = ((From itm In _CMSDBDataContext.EUS_Messages
                  Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                         (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver)) _
                         AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                  Select itm).Count() > 0)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function GetMyPoke(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_Offers
                  Where (itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) AndAlso _
                        (itm.OfferTypeID = ProfileHelper.OfferTypeID_POKE)
                  Select itm).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function GetMyPendingPoke(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_Offers
                  Where (itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) AndAlso _
                        (itm.OfferTypeID = ProfileHelper.OfferTypeID_POKE) AndAlso _
                        (itm.StatusID = ProfileHelper.OfferStatusID_PENDING)
                  Select itm).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Function HasAnyOffer(userIdOfferReceiver As Integer, userIdWhoDid As Integer) As Boolean
        Dim rec As Boolean

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = ((From itm In _CMSDBDataContext.EUS_Offers
                  Where ((itm.ToProfileID = userIdOfferReceiver AndAlso itm.FromProfileID = userIdWhoDid) OrElse _
                         (itm.ToProfileID = userIdWhoDid AndAlso itm.FromProfileID = userIdOfferReceiver))
                  Select itm).Count() > 0)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function


    Public Shared Sub SendWink(userIdReceiver As Integer, userIdWhoDid As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            'Dim rec = _CMSDBDataContext.EUS_Offers.Where(Function(itm) itm.ToProfileID = userIdReceiver AndAlso itm.FromProfileID = userIdWhoDid).Count()
            Dim rec As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                    Where itm.ToProfileID = userIdReceiver AndAlso itm.FromProfileID = userIdWhoDid
                                    Select itm).FirstOrDefault()

            If (rec Is Nothing) Then

                Dim newOffer As New EUS_Offer()
                newOffer.DateTimeToCreate = DateTime.UtcNow
                newOffer.FromProfileID = userIdWhoDid
                newOffer.OfferTypeID = ProfileHelper.OfferTypeID_WINK
                newOffer.StatusID = ProfileHelper.OfferStatusID_PENDING
                newOffer.ToProfileID = userIdReceiver
                newOffer.Amount = 0
                newOffer.ParrentOfferID = 0
                newOffer.ChildOfferID = 0

                _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
                _CMSDBDataContext.SubmitChanges()


                clsUserNotifications.SendEmailNotification(NotificationType.Likes, newOffer.FromProfileID, newOffer.ToProfileID)
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub


    Public Shared Function SendPoke(parms As NewOfferParameters) As Integer
        Dim newOfferID As Integer = 0

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim newOffer As New EUS_Offer()
            Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                            Where itm.OfferID = parms.parentOfferId
                                            Select itm).SingleOrDefault()



            newOffer.ToProfileID = parms.userIdReceiver
            newOffer.Amount = -1
            newOffer.FromProfileID = parms.userIdWhoDid
            newOffer.DateTimeToCreate = DateTime.UtcNow

            newOffer.Notes1 = parms.messageText1
            newOffer.Notes2 = parms.messageText2

            newOffer.OfferTypeID = ProfileHelper.OfferTypeID_POKE
            newOffer.StatusID = ProfileHelper.OfferStatusID_PENDING

            If parentOffer IsNot Nothing Then
                newOffer.ParrentOfferID = parms.parentOfferId
                If (newOffer.ToProfileID <= 0) Then newOffer.ToProfileID = parentOffer.FromProfileID
            End If

            newOffer.ChildOfferID = 0


            _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
            _CMSDBDataContext.SubmitChanges()

            newOfferID = newOffer.OfferID

            'If parentOffer IsNot Nothing Then
            '    parentOffer.StatusID = ProfileHelper.OfferStatusID_REPLACINGBYCOUNTER
            '    parentOffer.ChildOfferID = newOffer.OfferID
            '    _CMSDBDataContext.SubmitChanges()
            'End If

            'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return newOfferID
    End Function



    Public Shared Sub DeleteOfferAndParents(offerId As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (offerId > 0) Then
                Dim q As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                        Where itm.OfferID = offerId
                        Select itm).SingleOrDefault()

                If (q IsNot Nothing) Then
                    _CMSDBDataContext.EUS_Offers.DeleteOnSubmit(q)

                    Dim parentId As Integer = IIf(q.ParrentOfferID.HasValue, q.ParrentOfferID, 0)
                    While (parentId > 0)

                        Dim q2 As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                Where itm.OfferID = parentId
                                                Select itm).SingleOrDefault()
                        parentId = -1

                        If (q2 IsNot Nothing) Then
                            _CMSDBDataContext.EUS_Offers.DeleteOnSubmit(q2)

                            If (q2.ParrentOfferID > 0) Then parentId = q2.ParrentOfferID
                        End If

                    End While

                    _CMSDBDataContext.SubmitChanges()
                End If
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub


    Public Shared Sub CancelWinkOrOffer(offerId As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (offerId > 0) Then
                Dim q = (From itm In _CMSDBDataContext.EUS_Offers
                        Where itm.OfferID = offerId
                        Select itm).SingleOrDefault()

                If (q IsNot Nothing) Then
                    _CMSDBDataContext.EUS_Offers.DeleteOnSubmit(q)
                    _CMSDBDataContext.SubmitChanges()
                End If

                'If (q IsNot Nothing) Then
                '    q.StatusID = ProfileHelper.OfferStatusID_CANCELED
                '    _CMSDBDataContext.SubmitChanges()
                'End If
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub



    Public Shared Sub CancelPendingWink(userIdReceiver As Integer, userIdWhoDid As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            ' check
            Dim rec = (From itm In _CMSDBDataContext.EUS_Offers
                        Where (itm.ToProfileID = userIdReceiver AndAlso itm.FromProfileID = userIdWhoDid) AndAlso _
                            (itm.OfferTypeID = ProfileHelper.OfferTypeID_WINK) AndAlso _
                            (itm.StatusID = ProfileHelper.OfferStatusID_PENDING)
                        Select itm).FirstOrDefault()

            If (rec IsNot Nothing) Then
                _CMSDBDataContext.EUS_Offers.DeleteOnSubmit(rec)
                _CMSDBDataContext.SubmitChanges()
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub



    Public Shared Sub ResendWinkOrOffer(offerId As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (offerId > 0) Then
                Dim q = (From itm In _CMSDBDataContext.EUS_Offers
                        Where itm.OfferID = offerId
                        Select itm).SingleOrDefault()

                If (q IsNot Nothing) Then
                    q.StatusID = ProfileHelper.OfferStatusID_PENDING
                    _CMSDBDataContext.SubmitChanges()
                End If
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub


    'Public Shared Sub AcceptOffer(offerId As Integer)

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        If (offerId > 0) Then
    '            Dim q = (From itm In _CMSDBDataContext.EUS_Offers
    '                    Where itm.OfferID = offerId
    '                    Select itm).SingleOrDefault()

    '            If (q IsNot Nothing) Then
    '                q.StatusID = ProfileHelper.OfferStatusID_ACCEPTED
    '                _CMSDBDataContext.SubmitChanges()
    '            End If
    '        End If

    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try

    'End Sub

    Public Shared Sub AcceptOffer(offerId As Integer, AcceptedOfferStatus As OfferStatusEnum)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                            Where itm.OfferID = offerId
                                            Select itm).SingleOrDefault()

            If (AcceptedOfferStatus = OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE

            ElseIf (AcceptedOfferStatus = OfferStatusEnum.ACCEPTED) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_ACCEPTED

            End If

            _CMSDBDataContext.SubmitChanges()

            'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub


    Public Shared Sub RejectOffer(offerId As Integer, rejectionReason As String)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (offerId > 0) Then
                Dim q = (From itm In _CMSDBDataContext.EUS_Offers
                        Where itm.OfferID = offerId
                        Select itm).SingleOrDefault()

                If (q IsNot Nothing) Then
                    q.StatusID = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = rejectionReason).FirstOrDefault().EUS_OffersStatusID
                    _CMSDBDataContext.SubmitChanges()
                End If
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub




    Public Shared Function MakeOffer(parms As NewOfferParameters) As Integer
        Dim newOfferId As Integer


        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim newOffer As New EUS_Offer()
            Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                            Where itm.OfferID = parms.parentOfferId
                                            Select itm).SingleOrDefault()



            newOffer.ToProfileID = parms.userIdReceiver
            newOffer.Amount = parms.offerAmount
            newOffer.FromProfileID = parms.userIdWhoDid
            newOffer.DateTimeToCreate = DateTime.UtcNow

            newOffer.Notes1 = parms.messageText1
            newOffer.Notes2 = parms.messageText2

            newOffer.OfferTypeID = ProfileHelper.OfferTypeID_OFFERNEW
            newOffer.StatusID = ProfileHelper.OfferStatusID_PENDING

            If parentOffer IsNot Nothing Then
                newOffer.ParrentOfferID = parms.parentOfferId
            End If
            newOffer.ChildOfferID = 0


            _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
            _CMSDBDataContext.SubmitChanges()

            newOfferId = newOffer.OfferID

            'If parentOffer IsNot Nothing Then
            '    parentOffer.StatusID = ProfileHelper.OfferStatusID_REPLACINGBYCOUNTER
            '    parentOffer.ChildOfferID = newOffer.OfferID
            '    _CMSDBDataContext.SubmitChanges()
            'End If

            clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return newOfferId
    End Function


    Public Shared Function MakeNewDate(parms As NewOfferParameters) As Integer
        Dim newOfferId As Integer


        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim newOffer As New EUS_Offer()
            Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                            Where itm.OfferID = parms.parentOfferId
                                            Select itm).SingleOrDefault()



            newOffer.ToProfileID = parms.userIdReceiver
            newOffer.Amount = parms.offerAmount
            newOffer.FromProfileID = parms.userIdWhoDid
            newOffer.DateTimeToCreate = DateTime.UtcNow

            newOffer.Notes1 = parms.messageText1
            newOffer.Notes2 = parms.messageText2

            newOffer.OfferTypeID = ProfileHelper.OfferTypeID_NEWDATE
            newOffer.StatusID = ProfileHelper.OfferStatusID_UNLOCKED

            If parentOffer IsNot Nothing Then
                newOffer.ParrentOfferID = parms.parentOfferId
            End If
            newOffer.ChildOfferID = 0


            _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
            _CMSDBDataContext.SubmitChanges()

            newOfferId = newOffer.OfferID

            If parentOffer IsNot Nothing Then
                parentOffer.ChildOfferID = newOffer.OfferID
                _CMSDBDataContext.SubmitChanges()
            End If

            'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return newOfferId
    End Function


    Public Shared Function HasDateOfferAlready(FromProfileID As Integer, ToProfileID As Integer) As Boolean
        Dim rec As Integer = 0

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_Offers
                  Where ((itm.FromProfileID = FromProfileID AndAlso itm.ToProfileID = ToProfileID) OrElse (itm.FromProfileID = ToProfileID AndAlso itm.ToProfileID = FromProfileID)) AndAlso _
                        itm.OfferTypeID = ProfileHelper.OfferTypeID_NEWDATE AndAlso _
                        itm.StatusID = ProfileHelper.OfferStatusID_UNLOCKED
                        Select itm).Count()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return (rec > 0)
    End Function


    Public Shared Function GetDateOffer(FromProfileID As Integer, ToProfileID As Integer) As EUS_Offer
        Dim rec As EUS_Offer = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            rec = (From itm In _CMSDBDataContext.EUS_Offers
                  Where ((itm.FromProfileID = FromProfileID AndAlso itm.ToProfileID = ToProfileID) OrElse (itm.FromProfileID = ToProfileID AndAlso itm.ToProfileID = FromProfileID)) AndAlso _
                        itm.OfferTypeID = ProfileHelper.OfferTypeID_NEWDATE AndAlso _
                        itm.StatusID = ProfileHelper.OfferStatusID_UNLOCKED
                        Select itm).FirstOrDefault()
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return rec
    End Function

    ''' <summary>
    ''' Mark the date EUS_Offers record as viewed by ToProfileID (set field IsToProfileIDViewedDate to true).
    ''' So this date, if exists, is not new any more for ToProfileID
    ''' </summary>
    ''' <param name="FromProfileID"></param>
    ''' <param name="ToProfileID"></param>
    ''' <remarks></remarks>
    Public Shared Sub MarkDateOfferAsViewed(FromProfileID As Integer, ToProfileID As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim dateOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                  Where ((itm.FromProfileID = FromProfileID AndAlso itm.ToProfileID = ToProfileID)) AndAlso _
                        itm.OfferTypeID = ProfileHelper.OfferTypeID_NEWDATE AndAlso _
                        itm.StatusID = ProfileHelper.OfferStatusID_UNLOCKED
                        Select itm).FirstOrDefault()

            If (dateOffer IsNot Nothing) Then
                dateOffer.IsToProfileIDViewedDate = True
            End If
            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub

    Public Shared Sub AcceptLike(parms As NewOfferParameters, AcceptedOfferStatus As OfferStatusEnum)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                            Where itm.OfferID = parms.parentOfferId
                                            Select itm).SingleOrDefault()

            If (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_OFFER) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER

            ElseIf (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE

            ElseIf (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_POKE) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_POKE

            End If

            parentOffer.ChildOfferID = parms.childOfferId
            _CMSDBDataContext.SubmitChanges()

            'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub


    Public Shared Sub AcceptLike(offerId As Integer, AcceptedOfferStatus As OfferStatusEnum)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                            Where itm.OfferID = offerId
                                            Select itm).SingleOrDefault()

            If (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_OFFER) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER

            ElseIf (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE

            ElseIf (AcceptedOfferStatus = OfferStatusEnum.LIKE_ACCEEPTED_WITH_POKE) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_POKE

            End If

            _CMSDBDataContext.SubmitChanges()

            'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub


    Public Shared Sub AcceptPoke(parms As NewOfferParameters, AcceptedOfferStatus As OfferStatusEnum)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                            Where itm.OfferID = parms.parentOfferId
                                            Select itm).SingleOrDefault()

            If (AcceptedOfferStatus = OfferStatusEnum.POKE_ACCEEPTED_WITH_OFFER) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_POKE_ACCEEPTED_WITH_OFFER

            ElseIf (AcceptedOfferStatus = OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE

            End If

            parentOffer.ChildOfferID = parms.childOfferId
            _CMSDBDataContext.SubmitChanges()

            'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub



    Public Shared Sub AcceptPoke(offerId As Integer, AcceptedOfferStatus As OfferStatusEnum)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                            Where itm.OfferID = offerId
                                            Select itm).SingleOrDefault()

            If (AcceptedOfferStatus = OfferStatusEnum.POKE_ACCEEPTED_WITH_OFFER) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_POKE_ACCEEPTED_WITH_OFFER

            ElseIf (AcceptedOfferStatus = OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE) Then
                parentOffer.StatusID = ProfileHelper.OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE

            End If

            _CMSDBDataContext.SubmitChanges()

            'clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub


    Public Shared Sub CounterOffer(offerID As Integer, newAmount As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (offerID > 0) Then


                Dim parentOffer As EUS_Offer = (From itm In _CMSDBDataContext.EUS_Offers
                                                Where itm.OfferID = offerID
                                                Select itm).SingleOrDefault()

                Dim newOffer As New EUS_Offer()
                newOffer.ToProfileID = parentOffer.FromProfileID
                newOffer.Amount = newAmount
                newOffer.FromProfileID = parentOffer.ToProfileID
                newOffer.DateTimeToCreate = DateTime.UtcNow
                newOffer.OfferTypeID = ProfileHelper.OfferTypeID_OFFERCOUNTER
                newOffer.StatusID = ProfileHelper.OfferStatusID_COUNTER

                newOffer.ParrentOfferID = offerID
                newOffer.ChildOfferID = 0

                _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
                _CMSDBDataContext.SubmitChanges()


                If (parentOffer IsNot Nothing) Then
                    parentOffer.StatusID = ProfileHelper.OfferStatusID_REPLACINGBYCOUNTER
                    parentOffer.ChildOfferID = newOffer.OfferID
                    _CMSDBDataContext.SubmitChanges()
                End If

                clsUserNotifications.SendEmailNotification(NotificationType.Offers, newOffer.FromProfileID, newOffer.ToProfileID)
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub


    'Public Sub MakeOffer(offerId As Integer)

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        'If (offerId > 0) Then
    '        '    Dim q = (From itm In _CMSDBDataContext.EUS_Offers
    '        '            Where itm.OfferID = offerId
    '        '            Select itm).SingleOrDefault()

    '        '    If (q IsNot Nothing) Then
    '        '        q.StatusID = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.ACCEPTED.ToString()).FirstOrDefault().EUS_OffersStatusID
    '        '        _CMSDBDataContext.SubmitChanges()
    '        '    End If
    '        'End If


    '        ' check
    '        Dim q = (From itm In _CMSDBDataContext.EUS_Offers
    '                Where itm.OfferID = offerId
    '                Select itm).SingleOrDefault()

    '        If (q IsNot Nothing) Then


    '            Dim newOffer As New EUS_Offer()
    '            newOffer.DateTimeToCreate = DateTime.UtcNow
    '            newOffer.FromProfileID = q.ToProfileID
    '            newOffer.OfferTypeID =  ProfileHelper.OfferTypeID_OFFERNEW
    '            newOffer.StatusID = OffersStatusID_PENDING
    '            newOffer.ToProfileID = q.FromProfileID
    '            newOffer.Amount = 0
    '            newOffer.ParrentOfferID = 0

    '            _CMSDBDataContext.EUS_Offers.InsertOnSubmit(newOffer)
    '            _CMSDBDataContext.SubmitChanges()

    '            q.ChildOfferID = newOffer.OfferID
    '        End If


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try

    'End Sub




    Public Shared Sub DeleteMessage(MsgID As Integer, MakeHidden As Boolean)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (MsgID > 0) Then


                Dim message As EUS_Message = (From itm In _CMSDBDataContext.EUS_Messages
                                                Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                                Select itm).SingleOrDefault()


                If (message IsNot Nothing) Then
                    If (MakeHidden) Then
                        message.IsHidden = True
                        _CMSDBDataContext.SubmitChanges()
                    Else
                        _CMSDBDataContext.EUS_Messages.DeleteOnSubmit(message)
                        _CMSDBDataContext.SubmitChanges()
                    End If
                End If
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub

    ''' <summary>
    ''' Returns EUS_Message object received by another user
    ''' </summary>
    ''' <param name="Subject"></param>
    ''' <param name="Body"></param>
    ''' <param name="FromProfileID"></param>
    ''' <param name="ToProfileID"></param>
    ''' <param name="replyToMessageId"></param>
    ''' <remarks></remarks>
    Public Shared Function SendMessage(Subject As String, Body As String, FromProfileID As Integer, ToProfileID As Integer, Optional replyToMessageId As Integer = 0) As EUS_Message
        Dim messageReceived As New EUS_Message()
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim messageSent As New EUS_Message()
            messageSent.Body = Body
            messageSent.Subject = Subject
            messageSent.DateTimeToCreate = DateTime.UtcNow
            messageSent.FromProfileID = FromProfileID
            messageSent.ToProfileID = ToProfileID
            messageSent.StatusID = 1
            messageSent.ReplyToMessageId = replyToMessageId
            messageSent.IsSent = True
            messageSent.ProfileIDOwner = FromProfileID

            messageReceived.Body = Body
            messageReceived.Subject = Subject
            messageReceived.DateTimeToCreate = DateTime.UtcNow
            messageReceived.FromProfileID = FromProfileID
            messageReceived.ToProfileID = ToProfileID
            messageReceived.StatusID = 0
            messageReceived.ReplyToMessageId = replyToMessageId
            messageReceived.IsReceived = True
            messageReceived.ProfileIDOwner = ToProfileID
            messageReceived.IsHidden = False

            _CMSDBDataContext.EUS_Messages.InsertOnSubmit(messageSent)
            _CMSDBDataContext.EUS_Messages.InsertOnSubmit(messageReceived)
            _CMSDBDataContext.SubmitChanges()

            messageSent.CopyMessageID = messageReceived.EUS_MessageID
            messageReceived.CopyMessageID = messageSent.EUS_MessageID
            _CMSDBDataContext.SubmitChanges()


            clsUserNotifications.SendEmailNotification(NotificationType.Message, FromProfileID, ToProfileID)
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return messageReceived
    End Function


    Public Shared Sub SendMessageFromAdministrator(Subject As String, Body As String, ToProfileID As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim messageReceived As New EUS_Message()
            messageReceived.Body = Body
            messageReceived.Subject = Subject
            messageReceived.DateTimeToCreate = DateTime.UtcNow
            messageReceived.ToProfileID = ToProfileID
            messageReceived.StatusID = 0
            messageReceived.IsReceived = True
            messageReceived.ProfileIDOwner = ToProfileID

            messageReceived.FromProfileID = 1
            messageReceived.ReplyToMessageId = 0
            messageReceived.CopyMessageID = 0
            messageReceived.IsHidden = False

            _CMSDBDataContext.EUS_Messages.InsertOnSubmit(messageReceived)
            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub




    Public Shared Sub MarkMessageRead(MsgID As Integer)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (MsgID > 0) Then


                Dim message As EUS_Message = (From itm In _CMSDBDataContext.EUS_Messages
                                                Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                                Select itm).SingleOrDefault()


                If (message IsNot Nothing) Then
                    message.StatusID = 1
                    _CMSDBDataContext.SubmitChanges()
                End If
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub


    Public Shared Function GetLastMessageForProfiles(ProfileIDOwner As Integer, ProfileIDOther As Integer) As EUS_Message
        Dim message As EUS_Message = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (ProfileIDOwner > 0 AndAlso ProfileIDOther > 0) Then

                message = (From itm In _CMSDBDataContext.EUS_Messages
                            Where ((itm.FromProfileID = ProfileIDOwner AndAlso itm.ToProfileID = ProfileIDOther) OrElse _
                                    (itm.ToProfileID = ProfileIDOwner AndAlso itm.FromProfileID = ProfileIDOther)) AndAlso _
                                    itm.ProfileIDOwner = ProfileIDOwner _
                                   AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                            Order By itm.DateTimeToCreate Descending
                            Select itm).FirstOrDefault()

            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return message
    End Function


    Public Shared Function GetMessage(MsgID As Integer) As EUS_Message
        Dim message As EUS_Message = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (MsgID > 0) Then


                message = (From itm In _CMSDBDataContext.EUS_Messages
                                                Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                                Select itm).SingleOrDefault()

            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return message
    End Function



    Public Shared Function GetAllUnreadMessages(FromProfileID As Integer, ToProfileID As Integer) As List(Of EUS_Message)
        Dim messages As New List(Of EUS_Message)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (FromProfileID > 0 AndAlso ToProfileID > 0) Then


                messages = (From itm In _CMSDBDataContext.EUS_Messages
                            Where itm.FromProfileID = FromProfileID AndAlso itm.ToProfileID = ToProfileID AndAlso itm.StatusID = 0 AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                            Select itm).ToList()

            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return messages
    End Function



    'Public Shared Function VerifyUserMaySendMessage(_CMSDBDataContext As CMSDBDataContext, messageSendToLoginName As String, currentUserProfileID As Integer)
    '    Dim result As New clsVerifyUserMaySendMessageResult()

    '    Dim prof As EUS_Profile = DataHelpers.GetEUS_ProfileByLoginName(_CMSDBDataContext, messageSendToLoginName, ProfileStatusEnum.Approved)
    '    If (prof IsNot Nothing) Then
    '        Dim acceptedOffer As EUS_Offer = (From ofr In _CMSDBDataContext.EUS_Offers
    '                                         Where ofr.StatusID = 50 AndAlso _
    '                                         ((ofr.ToProfileID = prof.ProfileID And ofr.FromProfileID = currentUserProfileID) OrElse _
    '                                          (ofr.ToProfileID = currentUserProfileID And ofr.FromProfileID = prof.ProfileID))
    '                                         Select ofr).FirstOrDefault()
    '        If (acceptedOffer IsNot Nothing) Then

    '            Dim defaultOtherPhoto As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(currentUserProfileID)
    '            result.OtherMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & prof.LoginName)
    '            result.OtherMemberLoginName = prof.LoginName

    '            If (Not defaultOtherPhoto.IsFileNameNull()) Then
    '                result.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(prof.ProfileID, defaultOtherPhoto.FileName, prof.GenderId, True)
    '            Else
    '                result.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(prof.GenderId)
    '            End If


    '            If (acceptedOffer IsNot Nothing) Then
    '                result.DatingAmount = "&euro;" & acceptedOffer.Amount
    '            End If
    '        Else
    '            result.ErrorReason = clsVerifyUserMaySendMessageResult.ErrorReasonEnum.ErrorNoOfferCannotSendMessage
    '        End If
    '    Else
    '        result.ErrorReason = clsVerifyUserMaySendMessageResult.ErrorReasonEnum.ErrorMemberDoesNotExistCannotSendMessage
    '    End If

    '    Return result
    'End Function


    Public Shared Function GetProfilesThumbViews(fromProf As EUS_Profile, fromProfPhoto As DSMembers.EUS_CustomerPhotosRow, toProf As EUS_Profile, toProfPhoto As DSMembers.EUS_CustomerPhotosRow) As clsLoadProfilesViewsResult
        Dim result As New clsLoadProfilesViewsResult()

        ' current user data
        result.CurrentMemberProfileID = fromProf.ProfileID
        result.CurrentMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & fromProf.LoginName)
        result.CurrentMemberLoginName = fromProf.LoginName

        If (fromProfPhoto IsNot Nothing AndAlso Not fromProfPhoto.IsFileNameNull()) Then
            result.CurrentMemberImageUrl = ProfileHelper.GetProfileImageURL(fromProfPhoto.CustomerID, fromProfPhoto.FileName, fromProf.GenderId, True)
        Else
            result.CurrentMemberImageUrl = ProfileHelper.GetDefaultImageURL(fromProf.GenderId)
        End If


        ' other user data
        result.OtherMemberProfileID = toProf.ProfileID
        result.OtherMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & toProf.LoginName)
        result.OtherMemberLoginName = toProf.LoginName

        If (toProfPhoto IsNot Nothing AndAlso toProfPhoto IsNot Nothing AndAlso Not toProfPhoto.IsFileNameNull()) Then
            result.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(toProfPhoto.CustomerID, toProfPhoto.FileName, toProf.GenderId, True)
        Else
            result.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(toProf.GenderId)
        End If

        Return result
    End Function



    Public Shared Function GetProfilesThumbViews(fromProf As EUS_Profile, fromProfPhoto As EUS_CustomerPhoto, toProf As EUS_Profile, toProfPhoto As EUS_CustomerPhoto) As clsLoadProfilesViewsResult
        Dim result As New clsLoadProfilesViewsResult()

        ' current user data
        result.CurrentMemberProfileID = fromProf.ProfileID
        result.CurrentMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & fromProf.LoginName)
        result.CurrentMemberLoginName = fromProf.LoginName

        If (Not String.IsNullOrEmpty(fromProfPhoto.FileName)) Then
            result.CurrentMemberImageUrl = ProfileHelper.GetProfileImageURL(fromProf.ProfileID, fromProfPhoto.FileName, fromProf.GenderId, True)
        Else
            result.CurrentMemberImageUrl = ProfileHelper.GetDefaultImageURL(fromProf.GenderId)
        End If


        ' other user data
        result.OtherMemberProfileID = toProf.ProfileID
        result.OtherMemberProfileViewUrl = System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & toProf.LoginName)
        result.OtherMemberLoginName = toProf.LoginName

        If (toProfPhoto IsNot Nothing AndAlso Not String.IsNullOrEmpty(toProfPhoto.FileName)) Then
            result.OtherMemberImageUrl = ProfileHelper.GetProfileImageURL(toProf.ProfileID, toProfPhoto.FileName, toProf.GenderId, True)
        Else
            result.OtherMemberImageUrl = ProfileHelper.GetDefaultImageURL(toProf.GenderId)
        End If

        Return result
    End Function


    Shared Function UnlockOfferConversation(offerID As Integer, creditsAmount As Integer, userIdWhoUnlocks As Integer, EUS_CreditsTypeID As Integer)
        Dim UnlockedConversationId As Integer
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (offerID > 0 AndAlso creditsAmount > 0) Then



                Dim q = (From itm In _CMSDBDataContext.EUS_Offers
                        Where itm.OfferID = offerID
                        Select itm).SingleOrDefault()



                If (q IsNot Nothing) Then
                    Dim conversationProfileid As Integer
                    If (userIdWhoUnlocks = q.FromProfileID) Then
                        conversationProfileid = q.ToProfileID
                    Else
                        conversationProfileid = q.FromProfileID
                    End If

                    Dim _HasCommunication As Boolean = clsUserDoes.HasCommunication(conversationProfileid, userIdWhoUnlocks)
                    If (Not _HasCommunication) Then


                        'Dim qEUS_CustomerCredit = Nothing
                        'If (userIdWhoUnlocks = q.FromProfileID) Then

                        '    qEUS_CustomerCredit = (From itm In _CMSDBDataContext.EUS_CustomerCredits
                        '       Where itm.CustomerId = userIdWhoUnlocks AndAlso itm.ConversationWithCustomerId = q.ToProfileID
                        '       Select itm).SingleOrDefault()

                        'Else
                        '    qEUS_CustomerCredit = (From itm In _CMSDBDataContext.EUS_CustomerCredits
                        '       Where itm.CustomerId = userIdWhoUnlocks AndAlso itm.ConversationWithCustomerId = q.FromProfileID
                        '       Select itm).SingleOrDefault()

                        'End If

                        'If (qEUS_CustomerCredit Is Nothing) Then

                        'End If
                        Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(userIdWhoUnlocks)

                        Dim customerCredits As New EUS_CustomerCredit()
                        customerCredits.Credits = -(creditsAmount)
                        customerCredits.CustomerId = userIdWhoUnlocks
                        customerCredits.DateTimeCreated = DateTime.UtcNow
                        customerCredits.CreditsTypeId = EUS_CreditsTypeID
                        customerCredits.ConversationWithCustomerId = conversationProfileid
                        customerCredits.EuroAmount = customerCredits.Credits * _REF_CRD2EURO_Rate

                        'q.StatusID = OfferStatusID_UNLOCKED
                        _CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(customerCredits)


                        Dim unlockedConvers As New EUS_UnlockedConversation()
                        If (Not _HasCommunication) Then
                            unlockedConvers.DateTimeCreated = DateTime.UtcNow
                            unlockedConvers.FromProfileId = userIdWhoUnlocks
                            unlockedConvers.ToProfileId = customerCredits.ConversationWithCustomerId
                            unlockedConvers.CurrentOfferId = q.OfferID
                            unlockedConvers.CurrentOfferAmount = q.Amount

                            _CMSDBDataContext.EUS_UnlockedConversations.InsertOnSubmit(unlockedConvers)

                            clsUserNotifications.SendEmailNotification(NotificationType.Unlock, userIdWhoUnlocks, customerCredits.ConversationWithCustomerId)
                        End If

                        _CMSDBDataContext.SubmitChanges()
                        UnlockedConversationId = unlockedConvers.UnlockedConversationId
                    End If



                    If (UnlockedConversationId > 0 AndAlso Not clsUserDoes.HasDateOfferAlready(q.FromProfileID, q.ToProfileID)) Then
                        Dim parms As New NewOfferParameters()
                        parms.messageText1 = ""
                        parms.messageText2 = ""
                        parms.offerAmount = q.Amount
                        parms.parentOfferId = q.OfferID
                        parms.userIdReceiver = q.ToProfileID
                        parms.userIdWhoDid = q.FromProfileID

                        clsUserDoes.MakeNewDate(parms)
                    End If

                End If

            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try


        Return UnlockedConversationId
    End Function

    Shared Function UnlockMessageOnce(ToProfileID As Integer, FromProfileID As Integer, creditsAmount As Integer, unlockType As UnlockType) As Long
        Dim customerCreditsId As Long = 0

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            'Dim creditsAmount As Integer = ProfileHelper.Config_UNLOCK_MESSAGE
            Dim EUS_CreditsTypeID As Integer = clsUserDoes.GetRequiredCredits(unlockType).EUS_CreditsTypeID
            Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(FromProfileID)

            Dim customerCredits As New EUS_CustomerCredit()
            customerCredits.Credits = -(creditsAmount)
            customerCredits.CustomerId = FromProfileID
            customerCredits.DateTimeCreated = DateTime.UtcNow
            customerCredits.CreditsTypeId = EUS_CreditsTypeID
            customerCredits.EuroAmount = customerCredits.Credits * _REF_CRD2EURO_Rate
            customerCredits.ConversationWithCustomerId = ToProfileID
            _CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(customerCredits)
            _CMSDBDataContext.SubmitChanges()

            customerCreditsId = customerCredits.CustomerCreditsId

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return customerCreditsId
    End Function



    Shared Function UnlockMessageUnlimited(ToProfileID As Integer, FromProfileID As Integer, OfferId As Integer) As Long
        Dim customerCreditsId As Long = 0

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (Not clsUserDoes.HasCommunication(ToProfileID, FromProfileID)) Then

                Dim creditsAmount As Integer = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS
                Dim EUS_CreditsTypeID As Integer = clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_CONVERSATION).EUS_CreditsTypeID
                Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(FromProfileID)

                Dim customerCredits As New EUS_CustomerCredit()
                customerCredits.Credits = -(creditsAmount)
                customerCredits.CustomerId = FromProfileID
                customerCredits.DateTimeCreated = DateTime.UtcNow
                customerCredits.CreditsTypeId = EUS_CreditsTypeID
                customerCredits.EuroAmount = customerCredits.Credits * _REF_CRD2EURO_Rate
                customerCredits.ConversationWithCustomerId = ToProfileID
                _CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(customerCredits)


                Dim unlockedConvers As New EUS_UnlockedConversation()
                unlockedConvers.DateTimeCreated = DateTime.UtcNow
                unlockedConvers.FromProfileId = FromProfileID
                unlockedConvers.ToProfileId = ToProfileID
                unlockedConvers.CurrentOfferId = OfferId
                _CMSDBDataContext.EUS_UnlockedConversations.InsertOnSubmit(unlockedConvers)


                _CMSDBDataContext.SubmitChanges()
                customerCreditsId = customerCredits.CustomerCreditsId

                clsUserNotifications.SendEmailNotification(NotificationType.Unlock, FromProfileID, customerCredits.ConversationWithCustomerId)
            End If


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return customerCreditsId
    End Function


    Shared Function UnlockMessageConversation(messageId As Integer, creditsAmount As Integer, userIdWhoUnlocks As Integer, EUS_CreditsTypeID As Integer) As Long
        Dim customerCreditsId As Long = 0

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (messageId > 0 AndAlso creditsAmount > 0) Then



                Dim q = (From itm In _CMSDBDataContext.EUS_Messages
                        Where itm.EUS_MessageID = messageId AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                        Select itm).SingleOrDefault()

                If (q IsNot Nothing) Then

                    Dim _REF_CRD2EURO_Rate As Double = DataHelpers.GetEUS_Profiles_REF_CRD2EURO_Rate(userIdWhoUnlocks)

                    Dim customerCredits As New EUS_CustomerCredit()
                    customerCredits.Credits = -(creditsAmount)
                    customerCredits.CustomerId = userIdWhoUnlocks
                    customerCredits.DateTimeCreated = DateTime.UtcNow
                    customerCredits.CreditsTypeId = EUS_CreditsTypeID
                    customerCredits.EuroAmount = customerCredits.Credits * _REF_CRD2EURO_Rate

                    If (userIdWhoUnlocks = q.FromProfileID) Then
                        customerCredits.ConversationWithCustomerId = q.ToProfileID
                    Else
                        customerCredits.ConversationWithCustomerId = q.FromProfileID
                    End If

                    'q.StatusID = OfferStatusID_UNLOCKED
                    _CMSDBDataContext.EUS_CustomerCredits.InsertOnSubmit(customerCredits)


                    If (Not clsUserDoes.HasCommunication(customerCredits.ConversationWithCustomerId, userIdWhoUnlocks)) Then
                        Dim unlockedConvers As New EUS_UnlockedConversation()
                        unlockedConvers.DateTimeCreated = DateTime.UtcNow
                        unlockedConvers.FromProfileId = userIdWhoUnlocks
                        unlockedConvers.ToProfileId = customerCredits.ConversationWithCustomerId


                        Dim qoffer = (From itm In _CMSDBDataContext.EUS_Offers
           Where ((itm.FromProfileID = q.FromProfileID AndAlso itm.ToProfileID = q.ToProfileID) OrElse _
                (itm.FromProfileID = q.ToProfileID AndAlso itm.ToProfileID = q.FromProfileID)) AndAlso _
               itm.StatusID = ProfileHelper.OfferStatusID_ACCEPTED
           Select itm).SingleOrDefault()

                        If (qoffer IsNot Nothing) Then
                            unlockedConvers.CurrentOfferId = qoffer.OfferID
                            unlockedConvers.CurrentOfferAmount = qoffer.Amount
                        End If


                        _CMSDBDataContext.EUS_UnlockedConversations.InsertOnSubmit(unlockedConvers)

                        clsUserNotifications.SendEmailNotification(NotificationType.Unlock, userIdWhoUnlocks, customerCredits.ConversationWithCustomerId)
                    End If

                    _CMSDBDataContext.SubmitChanges()

                    customerCreditsId = customerCredits.CustomerCreditsId

                End If

            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return customerCreditsId
    End Function



    Shared Function GetAcceptedOrUnlockedOffer(otherUserId As Integer, currentUserId As Integer) As EUS_Offer
        Dim acceptedOffer As EUS_Offer = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (otherUserId > 0 AndAlso currentUserId > 0) Then
                acceptedOffer = (From ofr In _CMSDBDataContext.EUS_Offers
                                         Where (ofr.StatusID = ProfileHelper.OfferStatusID_ACCEPTED OrElse ofr.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_POKE OrElse ofr.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE OrElse ofr.StatusID = ProfileHelper.OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER) AndAlso _
                                         ((ofr.ToProfileID = otherUserId And ofr.FromProfileID = currentUserId) OrElse _
                                          (ofr.ToProfileID = currentUserId And ofr.FromProfileID = otherUserId))
                                         Select ofr).FirstOrDefault()
                ' OrElse ofr.StatusID = OfferStatusID_UNLOCKED

            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try
        Return acceptedOffer

    End Function


    Public Shared Function HasPhotos(userProfileId As Integer, userMirrorProfileId As Integer) As Boolean
        Dim currentUserHasPhotos As Integer

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (userProfileId > 0 OrElse userMirrorProfileId > 0) Then
                currentUserHasPhotos = (From phot In _CMSDBDataContext.EUS_CustomerPhotos
                                       Where (phot.CustomerID = userProfileId OrElse phot.CustomerID = userMirrorProfileId) AndAlso _
                                       phot.HasAproved = True AndAlso (phot.IsDeleted Is Nothing Or phot.IsDeleted = 0)
                                       Select phot).Count()
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return (currentUserHasPhotos > 0)
    End Function


    'Public Shared Function GetRequiredCredits_UnlockConversation() As EUS_CreditsType
    '    Dim creditsRequired As EUS_CreditsType

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
    '                     Where itm.CreditsType = "UNLOCK_CONVERSATION"
    '                     Select itm).FirstOrDefault()


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try

    '    Return creditsRequired
    'End Function

    'Public Shared Function GetRequiredCredits_UnlockMessage() As EUS_CreditsType
    '    Dim creditsRequired As EUS_CreditsType

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
    '                     Where itm.CreditsType = "UNLOCK_MESSAGE"
    '                     Select itm).FirstOrDefault()


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try

    '    Return creditsRequired
    'End Function


    Public Shared Function GetRequiredCredits(type As UnlockType) As EUS_CreditsType
        Dim creditsRequired As EUS_CreditsType = Nothing

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            If (type = UnlockType.UNLOCK_CONVERSATION) Then
                creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                             Where itm.CreditsType = "UNLOCK_CONVERSATION"
                             Select itm).FirstOrDefault()

            ElseIf (type = UnlockType.UNLOCK_MESSAGE_READ) Then
                creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                             Where itm.CreditsType = "UNLOCK_MESSAGE_READ"
                             Select itm).FirstOrDefault()

            ElseIf (type = UnlockType.UNLOCK_MESSAGE_SEND) Then
                creditsRequired = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                             Where itm.CreditsType = "UNLOCK_MESSAGE_SEND"
                             Select itm).FirstOrDefault()
            End If

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return creditsRequired
    End Function



    Public Shared Function HasRequiredCredits(MasterProfileId As Integer, type As UnlockType) As Boolean
        Dim success = False

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim gotoBilling = False

            Dim sqlProc As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
            Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)

            Dim totalMemberCredits As Integer = 0
            If (Not dt.Rows(0).IsNull("AvailableCredits")) Then
                totalMemberCredits = dt.Rows(0)("AvailableCredits")
            End If

            Try


                Dim creditsRequired As Integer = 10000000

                If (type = UnlockType.UNLOCK_CONVERSATION) Then
                    creditsRequired = ProfileHelper.Config_UNLOCK_CONVERSATION_CREDITS

                ElseIf (type = UnlockType.UNLOCK_MESSAGE_READ) Then
                    creditsRequired = ProfileHelper.Config_UNLOCK_MESSAGE_READ_CREDITS

                ElseIf (type = UnlockType.UNLOCK_MESSAGE_SEND) Then
                    creditsRequired = ProfileHelper.Config_UNLOCK_MESSAGE_SEND_CREDITS
                End If



                If (totalMemberCredits >= creditsRequired) Then success = True
            Catch ex As Exception

            End Try


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try


        Return success
    End Function

    'Public Shared Function HasRequiredCredits_To_UnlockConversation(MasterProfileId As Integer) As Boolean
    '    Dim success = False

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try
    '        Dim gotoBilling = False

    '        Dim sqlProc As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
    '        Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)

    '        Dim totalMemberCredits As Integer = 0
    '        If (Not dt.Rows(0).IsNull("AvailableCredits")) Then
    '            totalMemberCredits = dt.Rows(0)("AvailableCredits")
    '        End If

    '        Try

    '            Dim creditsRequired As EUS_CreditsType = (From itm In _CMSDBDataContext.EUS_CreditsTypes
    '                     Where itm.CreditsType = "UNLOCK_CONVERSATION"
    '                     Select itm).FirstOrDefault()


    '            If (totalMemberCredits >= creditsRequired.CreditsAmount) Then success = True
    '        Catch ex As Exception

    '        End Try


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try


    '    Return success
    'End Function

    'Public Shared Function HasRequiredCredits_To_UnlockMessage(MasterProfileId As Integer) As Boolean
    '    Dim success = False

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try
    '        Dim gotoBilling = False

    '        Dim sqlProc As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
    '        Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)

    '        Dim totalMemberCredits As Integer = 0
    '        If (Not dt.Rows(0).IsNull("AvailableCredits")) Then
    '            totalMemberCredits = dt.Rows(0)("AvailableCredits")
    '        End If

    '        Try

    '            Dim creditsRequired As EUS_CreditsType = (From itm In _CMSDBDataContext.EUS_CreditsTypes
    '                     Where itm.CreditsType = "UNLOCK_MESSAGE"
    '                     Select itm).FirstOrDefault()


    '            If (totalMemberCredits >= creditsRequired.CreditsAmount) Then success = True
    '        Catch ex As Exception

    '        End Try


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try


    '    Return success
    'End Function




    Public Shared Function PerformOfferUnlock(offerID As Integer, MasterProfileId As Integer) As Boolean
        Dim success = False

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim gotoBilling = False

            Dim sqlProc As String = "EXEC [CustomerAvailableCredits] @CustomerId = " & MasterProfileId
            Dim dt As DataTable = DataHelpers.GetDataTable(sqlProc)

            Dim totalMemberCredits As Integer = 0
            If (Not dt.Rows(0).IsNull("AvailableCredits")) Then
                totalMemberCredits = dt.Rows(0)("AvailableCredits")
            End If

            Try
                gotoBilling = True

                Dim creditsRequired As EUS_CreditsType = (From itm In _CMSDBDataContext.EUS_CreditsTypes
                         Where itm.CreditsType = "UNLOCK_CONVERSATION"
                         Select itm).FirstOrDefault()


                If (totalMemberCredits >= creditsRequired.CreditsAmount) Then
                    clsUserDoes.UnlockOfferConversation(offerID, creditsRequired.CreditsAmount, MasterProfileId, creditsRequired.EUS_CreditsTypeID)

                    gotoBilling = False
                End If
            Catch ex As Exception

            End Try


            If (gotoBilling) Then
                clsSessionVariables.GetCurrent().UserUnlockInfo.OfferId = offerID
                clsSessionVariables.GetCurrent().UserUnlockInfo.ReturnUrl = HttpContext.Current.Request.Url.AbsoluteUri

                Dim billingurl As String = System.Web.VirtualPathUtility.ToAbsolute("~/Members/SelectProduct.aspx") & _
                        ("?offer=" & offerID) & _
                        ("&returnurl=" & System.Web.HttpUtility.UrlEncode(HttpContext.Current.Request.Url.AbsoluteUri))

                HttpContext.Current.Response.Redirect(billingurl, False)
            Else
                success = True
            End If
        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try


        Return success
    End Function


    Public Shared Sub LogReporting(FromProfileId As Integer, ToProfileId As Integer, ReportingReason As Integer, OtherText As String)

        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            Dim report As New EUS_ReportingProfile()
            report.ToProfileId = ToProfileId
            report.FromProfileId = FromProfileId
            report.DateCreated = DateTime.UtcNow
            report.OtherText = OtherText
            report.ReportingReasonId = ReportingReason


            _CMSDBDataContext.EUS_ReportingProfiles.InsertOnSubmit(report)
            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

    End Sub



End Class

﻿Imports System.Text

Public Class clsHTMLHelper

    Public Shared Function RenderImageHtml(imgUrl As String, Optional attributes As Dictionary(Of String, String) = Nothing) As String
        Dim html As String
        ' html = "<img src=""/forums/images/template/header.jpg"" alt=""Velocity Reviews - Computer Hardware Reviews""
        ' width=""500"" height=""100"" title=""Velocity Reviews - Computer Hardware Reviews"">"

        Dim sb As New StringBuilder()
        sb.AppendFormat("<img src=""{0}""", imgUrl)
        If (attributes IsNot Nothing) Then
            For Each itm As KeyValuePair(Of String, String) In attributes
                sb.AppendFormat(" {0}=""{1}""", itm.Key, itm.Value)
            Next
        End If
        sb.Append(" >")

        html = sb.ToString()
        Return html
    End Function

    Public Shared Function RenderLinkHtml(href As String, innerContent As String, Optional attributes As Dictionary(Of String, String) = Nothing) As String
        Dim html As String
        ' <a href="http://www.velocityreviews.com/forums/f16-newsgroups.html">Newsgroups</a>
        '

        Dim sb As New StringBuilder()
        sb.Append("<a")
        sb.AppendFormat(" href=""{0}""", href)

        If (attributes IsNot Nothing) Then
            For Each itm As KeyValuePair(Of String, String) In attributes
                sb.AppendFormat(" {0}=""{1}""", itm.Key, itm.Value)
            Next
        End If

        sb.Append(">")
        sb.Append(innerContent)
        sb.Append("</a>")

        html = sb.ToString()
        Return html
    End Function

End Class

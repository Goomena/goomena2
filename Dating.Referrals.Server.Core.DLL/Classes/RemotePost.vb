﻿Imports System.IO
Imports System.Net

Public Class RemotePost
    Private Inputs As System.Collections.Specialized.NameValueCollection = New System.Collections.Specialized.NameValueCollection

    Public Url As String = ""
    Public Method As String = "post"
    Public FormName As String = "form1"
    Public Target As String = "_self"

    Public Sub Add(ByVal name As String, ByVal value As String)
        Inputs.Add(name, value)
    End Sub

    Public Sub Post()
        System.Web.HttpContext.Current.Response.Clear()
        System.Web.HttpContext.Current.Response.Write("<html><head>")
        System.Web.HttpContext.Current.Response.Write(String.Format("</head><body onload=""document.{0}.submit()"">", FormName))
        System.Web.HttpContext.Current.Response.Write(String.Format("<form name=""{0}"" method=""{1}"" action=""{2}"" target=""{3}"">", FormName, Method, Url, Target))
        Dim i As Integer = 0
        Do While i < Inputs.Keys.Count
            System.Web.HttpContext.Current.Response.Write(String.Format("<input name=""{0}"" type=""hidden"" value=""{1}"">", Inputs.Keys(i), Inputs(Inputs.Keys(i))))
            i += 1
        Loop
        System.Web.HttpContext.Current.Response.Write("</form>")
        System.Web.HttpContext.Current.Response.Write("</body></html>")
        System.Web.HttpContext.Current.Response.End()
    End Sub





    Public Shared Function GetWebFile(urlDownloadFile As String) As String
        Dim localFilePath As String = ""

        Dim hwr As HttpWebRequest = DirectCast(System.Net.WebRequest.Create(urlDownloadFile), HttpWebRequest)
        hwr.Method = "GET"
        hwr.KeepAlive = True
        hwr.UserAgent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; InfoPath.3)"


        Dim responseStream As Stream = hwr.GetResponse().GetResponseStream()
        Dim fstr As Stream = Nothing
        Try
            Dim fileName As String = Path.GetFileName(urlDownloadFile)
            fileName = DateTime.Now.ToFileTimeUtc().ToString() & "__" & fileName
            localFilePath = Path.Combine(Environment.GetEnvironmentVariable("Temp"), fileName)

            fstr = File.Create(localFilePath)

            ' Allocate a 1k buffer
            Dim buffer As Byte() = New Byte(1024) {}
            Dim bytesRead As Integer
            Dim bytesProcessed As Integer = 0


            ' Simple do/while loop to read from stream until
            ' no bytes are returned
            Do
                ' Read data (up to 1k) from the stream
                bytesRead = responseStream.Read(buffer, 0, buffer.Length)

                ' Write the data to the local file
                fstr.Write(buffer, 0, bytesRead)

                ' Increment total bytes processed
                bytesProcessed += bytesRead
            Loop While (bytesRead > 0)

        Catch ex As Exception
            Throw
        Finally
            responseStream.Close()
            fstr.Close()
        End Try

        Return localFilePath
    End Function


End Class

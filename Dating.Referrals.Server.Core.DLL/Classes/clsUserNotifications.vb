﻿Imports System.Text.RegularExpressions
Imports Dating.Referrals.Server.Core.DLL
Imports System.Text

Public Class clsUserNotifications


    'Public Shared Function SendEmailNotification(notif As NotificationType, fromProfileID As Integer, toProfileId As Integer) As Boolean
    '    Dim success As Boolean = False
    '    If (notif = NotificationType.None) Then Return success


    '    Try
    '        Dim memberTo As DSMembers = DataHelpers.GetEUS_Profiles_ByProfileID(toProfileId)
    '        Dim rowTo As EUS_ProfilesRow = memberTo.EUS_Profiles.Rows(0)
    '        ' If(rowTo)

    '        Dim memberFrom As DSMembers = DataHelpers.GetEUS_Profiles_ByProfileID(fromProfileID)
    '        Dim rowFrom As EUS_ProfilesRow = memberFrom.EUS_Profiles.Rows(0)


    '        If (rowTo.Status = ProfileStatusEnum.Approved) Then

    '            If (notif = NotificationType.Likes AndAlso rowTo.NotificationsSettings_WhenLikeReceived = True) Then
    '                Dim o As New clsSiteLAG()
    '                Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewLikeSubject", DataHelpers.ConnectionString)
    '                Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewLike", DataHelpers.ConnectionString)

    '                body = Regex.Replace(body, "###RECIPIENTLOGINNAME###", rowTo.LoginName)
    '                body = Regex.Replace(body, "###LOGINNAME###", rowFrom.LoginName)

    '                'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
    '                clsMyMail.SendMailFromQueue(rowTo.eMail, subject, body, rowTo.ProfileID, "NotificationType.Likes", True)
    '                success = True

    '            ElseIf (notif = NotificationType.Message AndAlso rowTo.NotificationsSettings_WhenNewMessageReceived = True) Then
    '                Dim o As New clsSiteLAG()


    '                If (Not clsUserDoes.HasCommunication(rowTo.ProfileID, rowFrom.ProfileID) AndAlso ProfileHelper.IsMale(rowTo.GenderId)) Then

    '                    Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewMessageSubject", DataHelpers.ConnectionString)
    '                    Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "MALE_NotificationEmailForNewMessage", DataHelpers.ConnectionString)

    '                    body = Regex.Replace(body, "###RECIPIENTLOGINNAME###", rowTo.LoginName)
    '                    body = Regex.Replace(body, "###LOGINNAME###", rowFrom.LoginName)

    '                    'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
    '                    clsMyMail.SendMailFromQueue(rowTo.eMail, subject, body, rowTo.ProfileID, "NotificationType.Message.ToMale", True)
    '                Else

    '                    Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewMessageSubject", DataHelpers.ConnectionString)
    '                    Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewMessage", DataHelpers.ConnectionString)

    '                    body = Regex.Replace(body, "###RECIPIENTLOGINNAME###", rowTo.LoginName)
    '                    body = Regex.Replace(body, "###LOGINNAME###", rowFrom.LoginName)

    '                    'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
    '                    clsMyMail.SendMailFromQueue(rowTo.eMail, subject, body, rowTo.ProfileID, "NotificationType.Message", True)
    '                End If

    '                success = True
    '            ElseIf (notif = NotificationType.Offers AndAlso rowTo.NotificationsSettings_WhenNewOfferReceived = True) Then
    '                Dim o As New clsSiteLAG()
    '                Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewOfferSubject", DataHelpers.ConnectionString)
    '                Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForNewOffer", DataHelpers.ConnectionString)

    '                body = Regex.Replace(body, "###RECIPIENTLOGINNAME###", rowTo.LoginName)
    '                body = Regex.Replace(body, "###LOGINNAME###", rowFrom.LoginName)

    '                'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
    '                clsMyMail.SendMailFromQueue(rowTo.eMail, subject, body, rowTo.ProfileID, "NotificationType.Offers", True)
    '                success = True

    '            ElseIf (notif = NotificationType.Unlock AndAlso rowTo.NotificationsSettings_WhenNewOfferReceived = True) Then
    '                Dim o As New clsSiteLAG()
    '                Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForUnlockSubject", DataHelpers.ConnectionString)
    '                Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForUnlock", DataHelpers.ConnectionString)

    '                body = Regex.Replace(body, "###RECIPIENTLOGINNAME###", rowTo.LoginName)
    '                body = Regex.Replace(body, "###LOGINNAME###", rowFrom.LoginName)

    '                'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
    '                clsMyMail.SendMailFromQueue(rowTo.eMail, subject, body, rowTo.ProfileID, "NotificationType.Unlock", True)
    '                success = True

    '            ElseIf (notif = NotificationType.AutoNotification AndAlso rowTo.NotificationsSettings_WhenNewMembersNearMe = True) Then
    '                success = clsUserNotifications.SendEmailNotification_AutoNotification(rowTo, rowFrom)

    '            ElseIf (notif = NotificationType.AutoNotificationForPhoto AndAlso rowTo.NotificationsSettings_WhenNewMembersNearMe = True) Then
    '                success = clsUserNotifications.SendEmailNotification_AutoNotificationForPhoto(rowTo, rowFrom)

    '            End If

    '        End If 'rowTo.Status = ProfileStatusEnum.Approved

    '    Catch ex As Exception
    '        'Throw
    '    End Try

    '    Return success
    'End Function


    'Public Shared Function SendEmailNotification_AutoNotificationForPhoto(rowTo As EUS_ProfilesRow, rowFrom As EUS_ProfilesRow) As Boolean
    '    Dim success As Boolean = False
    '    Dim insertToEmailsQueue As Boolean = True
    '    Try

    '        Dim photo As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(rowFrom.ProfileID)
    '        If (photo Is Nothing) Then
    '            Return success
    '        End If


    '        Dim o As New clsSiteLAG()
    '        Dim config As New clsConfigValues()
    '        Dim site_name As String = config.site_name
    '        Dim image_thumb_url As String = config.image_thumb_url
    '        Dim image_url As String = config.image_url

    '        Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForAutoNotificationSubject", DataHelpers.ConnectionString)
    '        Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForAutoNotificationPhoto", DataHelpers.ConnectionString)

    '        body = Regex.Replace(body, "###RECIPIENTLOGINNAME###", rowTo.LoginName)
    '        body = Regex.Replace(body, "###LOGINNAME###", rowFrom.LoginName)


    '        ' get default photo
    '        Try
    '            Dim photoFileName As String = ""

    '            'Dim fileName As String = ""
    '            'fileName = photo.FileName
    '            'photoFileName = ProfileHelper.GetProfileImageURL(rowFrom.ProfileID, fileName, rowFrom.GenderId, True)

    '            photoFileName = String.Format(image_thumb_url, rowFrom.ProfileID, photo.FileName)

    '            body = Regex.Replace(body, "blank###DEFAULTPHOTO###", photoFileName) ' cms editor issue, adds word blank
    '            body = Regex.Replace(body, "###DEFAULTPHOTO###", photoFileName)
    '        Catch ex As Exception

    '        End Try

    '        ' get default photo
    '        Try
    '            Dim height As String = ProfileHelper.GetHeightString(rowFrom.PersonalInfo_HeightID, rowTo.LAGID)
    '            If (String.IsNullOrEmpty(height)) Then height = "-"
    '            body = Regex.Replace(body, "###HEIGHT###", height)
    '        Catch ex As Exception

    '        End Try

    '        ' GetBodyTypeString
    '        Try
    '            Dim str As String = ProfileHelper.GetBodyTypeString(rowFrom.PersonalInfo_BodyTypeID, rowTo.LAGID)
    '            If (String.IsNullOrEmpty(str)) Then str = "-"
    '            body = Regex.Replace(body, "###BODYTYPE###", str)
    '        Catch ex As Exception

    '        End Try

    '        ' GetEyeColorString
    '        Try
    '            Dim str As String = ProfileHelper.GetEyeColorString(rowFrom.PersonalInfo_EyeColorID, rowTo.LAGID)
    '            If (String.IsNullOrEmpty(str)) Then str = "-"
    '            body = Regex.Replace(body, "###EYECOLOR###", str)
    '        Catch ex As Exception

    '        End Try

    '        ' GetHairColorString
    '        Try
    '            Dim str As String = ProfileHelper.GetHairColorString(rowFrom.PersonalInfo_HairColorID, rowTo.LAGID)
    '            If (String.IsNullOrEmpty(str)) Then str = "-"
    '            body = Regex.Replace(body, "###HAIRCOLOR###", str)
    '        Catch ex As Exception

    '        End Try

    '        ' get default photo
    '        Try
    '            Dim age As Integer = ProfileHelper.GetCurrentAge(rowFrom.Birthday)
    '            body = Regex.Replace(body, "###AGE###", age.ToString())
    '        Catch ex As Exception

    '        End Try

    '        'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
    '        Dim hdl As New Core.DLL.clsSendingEmailsHandler()
    '        Try
    '            If (insertToEmailsQueue) Then
    '                'If (Not hdl.CheckEmailIfExists(rowTo.eMail, subject, rowTo.ProfileID, "AutoNotification")) Then
    '                hdl.AddEmail(rowTo.eMail, subject, body, rowTo.ProfileID, True, "AutoNotification")
    '                'End If
    '            Else
    '                clsMyMail.SendMail(rowTo.eMail, subject, body, True)
    '            End If
    '        Catch ex As Exception
    '            Throw
    '        Finally
    '            hdl.Dispose()
    '        End Try

    '        success = True
    '    Catch ex As Exception
    '        'WebErrorMessageBox(Me, ex, "")
    '    End Try

    '    Return success
    'End Function



    'Public Shared Function SendEmailNotification_AutoNotification(rowTo As EUS_ProfilesRow, rowFrom As EUS_ProfilesRow) As Boolean
    '    Dim success As Boolean = False
    '    Dim insertToEmailsQueue As Boolean = True
    '    Try
    '        Dim o As New clsSiteLAG()

    '        Dim subject As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForAutoNotificationSubject", DataHelpers.ConnectionString)
    '        Dim body As String = o.GetCustomStringFromPage(rowTo.LAGID, "GlobalStrings", "NotificationEmailForAutoNotification", DataHelpers.ConnectionString)

    '        body = Regex.Replace(body, "###RECIPIENTLOGINNAME###", rowTo.LoginName)
    '        body = Regex.Replace(body, "###LOGINNAME###", rowFrom.LoginName)


    '        ' get default photo
    '        Try
    '            Dim photoFileName As String = ""
    '            Dim fileName As String = ""
    '            Dim photo As DSMembers.EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(rowFrom.ProfileID)
    '            If (photo IsNot Nothing) Then
    '                fileName = photo.FileName
    '            End If
    '            photoFileName = ProfileHelper.GetProfileImageURL(rowFrom.ProfileID, fileName, rowFrom.GenderId, True)
    '            body = Regex.Replace(body, "###DEFAULTPHOTO###", photoFileName)
    '        Catch ex As Exception

    '        End Try

    '        ' get default photo
    '        Try
    '            Dim height As String = ProfileHelper.GetHeightString(rowFrom.PersonalInfo_HeightID, rowTo.LAGID)
    '            If (String.IsNullOrEmpty(height)) Then height = "-"
    '            body = Regex.Replace(body, "###HEIGHT###", height)
    '        Catch ex As Exception

    '        End Try

    '        ' GetBodyTypeString
    '        Try
    '            Dim str As String = ProfileHelper.GetBodyTypeString(rowFrom.PersonalInfo_BodyTypeID, rowTo.LAGID)
    '            If (String.IsNullOrEmpty(str)) Then str = "-"
    '            body = Regex.Replace(body, "###BODYTYPE###", str)
    '        Catch ex As Exception

    '        End Try

    '        ' GetEyeColorString
    '        Try
    '            Dim str As String = ProfileHelper.GetEyeColorString(rowFrom.PersonalInfo_EyeColorID, rowTo.LAGID)
    '            If (String.IsNullOrEmpty(str)) Then str = "-"
    '            body = Regex.Replace(body, "###EYECOLOR###", str)
    '        Catch ex As Exception

    '        End Try

    '        ' GetHairColorString
    '        Try
    '            Dim str As String = ProfileHelper.GetHairColorString(rowFrom.PersonalInfo_HairColorID, rowTo.LAGID)
    '            If (String.IsNullOrEmpty(str)) Then str = "-"
    '            body = Regex.Replace(body, "###HAIRCOLOR###", str)
    '        Catch ex As Exception

    '        End Try

    '        ' get default photo
    '        Try
    '            Dim age As Integer = ProfileHelper.GetCurrentAge(rowFrom.Birthday)
    '            body = Regex.Replace(body, "###AGE###", age.ToString())
    '        Catch ex As Exception

    '        End Try

    '        'clsMyMail.SendMail(rowTo.eMail, subject, body, True)
    '        Dim hdl As New Core.DLL.clsSendingEmailsHandler()
    '        Try
    '            If (insertToEmailsQueue) Then
    '                'If (Not hdl.CheckEmailIfExists(rowTo.eMail, subject, rowTo.ProfileID, "AutoNotification")) Then
    '                hdl.AddEmail(rowTo.eMail, subject, body, rowTo.ProfileID, True, "AutoNotification")
    '                'End If
    '            Else
    '                clsMyMail.SendMail(rowTo.eMail, subject, body, True)
    '            End If
    '        Catch ex As Exception
    '            Throw
    '        Finally
    '            hdl.Dispose()
    '        End Try

    '        success = True
    '    Catch ex As Exception
    '        'WebErrorMessageBox(Me, ex, "")
    '    End Try

    '    Return success
    'End Function



    Public Shared Sub SendEmailNotification_OnAdminApprove(profile As DSReferrers.REF_ReferrerProfilesRow, lagid As String)
        Try
            Dim SMTPServerCredentialsID As Integer?
            Dim result As DataSet = DataHelpers.SYS_SMTPServers_GetByName("GREF")
            If (result.Tables.Count > 0) Then
                If (result.Tables(0).Rows.Count > 0) Then
                    SMTPServerCredentialsID = result.Tables(0).Rows(0)("SMTPServerCredentialsID")
                End If
            End If

            Dim toEmail As String = profile.eMail
            Dim subject As String = ""
            Dim Content As String = ""
            Dim o As New clsSiteLAG()

            subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "Referrer.RegistrationMessageTextSubject", DataHelpers.ConnectionString)
            Content = o.GetCustomStringFromPage(lagid, "GlobalStrings", "Referrer.RegistrationMessageText", DataHelpers.ConnectionString)
            Content = Content.Replace("###LOGINNAME###", profile.LoginName)
            Content = Content.Replace("###PASSWORD###", profile.Password)
            clsMyMail.SendMailFromQueue(toEmail, subject, Content, -1, "Referrer.AdminApprove", True, SMTPServerCredentialsID)

        Catch ex As Exception
            'WebErrorMessageBox(Me, ex, "")
        End Try
    End Sub


    'Public Shared Sub SendEmailNotification_OnAdminRejectedNewProfile(profile As EUS_ProfilesRow, lagid As String, subject As String, content As String)
    '    Try
    '        Dim toEmail As String = profile.eMail
    '        Dim o As New clsSiteLAG()

    '        'subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "RegistrationMessageTextSubject", DataHelpers.ConnectionString)
    '        'content = o.GetCustomStringFromPage(lagid, "GlobalStrings", "RegistrationMessageText", DataHelpers.ConnectionString)
    '        'content = content.Replace("###LOGINNAME###", profile.LoginName)
    '        'content = content.Replace("###PASSWORD###", profile.Password)

    '        'clsMyMail.SendMail(toEmail, subject, content, True)
    '        clsMyMail.SendMailFromQueue(toEmail, subject, content, profile.ProfileID, "AdminRejectedNewProfile", True)

    '    Catch ex As Exception
    '        'WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


    'Public Shared Sub SendEmailNotification_OnAdminRejectedProfileChanges(profile As EUS_ProfilesRow, lagid As String, subject As String, content As String)
    '    Try
    '        Dim toEmail As String = profile.eMail
    '        Dim o As New clsSiteLAG()

    '        'subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "RegistrationMessageTextSubject", DataHelpers.ConnectionString)
    '        'content = o.GetCustomStringFromPage(lagid, "GlobalStrings", "RegistrationMessageText", DataHelpers.ConnectionString)
    '        'content = content.Replace("###LOGINNAME###", profile.LoginName)
    '        'content = content.Replace("###PASSWORD###", profile.Password)

    '        'clsMyMail.SendMail(toEmail, subject, content, True)
    '        clsMyMail.SendMailFromQueue(toEmail, subject, content, profile.ProfileID, "AdminRejectedProfileChanges", True)

    '    Catch ex As Exception
    '        'WebErrorMessageBox(Me, ex, "")
    '    End Try
    'End Sub


    ''Public Shared Sub SendEmailNotification_OnRegisterWithAutoApprove(profile As EUS_ProfilesRow, lagid As String)
    ''    Try
    ''        Dim toEmail As String = profile.eMail
    ''        Dim subject As String = ""
    ''        Dim Content As String = ""
    ''        Dim o As New clsSiteLAG()

    ''        subject = o.GetCustomStringFromPage(lagid, "GlobalStrings", "AutoApproveMessageSubject", DataHelpers.ConnectionString)
    ''        Content = o.GetCustomStringFromPage(lagid, "GlobalStrings", "AutoApproveMessage", DataHelpers.ConnectionString)
    ''        Content = Content.Replace("###LOGINNAME###", profile.LoginName)
    ''        Content = Content.Replace("###PASSWORD###", profile.Password)

    ''        clsMyMail.SendMail(toEmail, subject, Content, True)

    ''    Catch ex As Exception
    ''        'WebErrorMessageBox(Me, ex, "")
    ''    End Try
    ''End Sub



    'Public Shared Function SendNotificationsToMembers(sendMessageParams As SendMessageToProfilesParams, Optional insertToEmailsQueue As Boolean = False) As String

    '    Dim sb As New StringBuilder
    '    Dim o As New clsSiteLAG()
    '    Dim hdl As New Core.DLL.clsSendingEmailsHandler()
    '    Dim resultCounter As Integer

    '    Dim dc As CMSDBDataContext = Nothing
    '    Try

    '        dc = New CMSDBDataContext(DataHelpers.ConnectionString)

    '        'Dim dsMem As New DSMembers()

    '        'Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
    '        'ta.Connection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
    '        'ta.FillBy_Approved_HasOrNotPhotos(dsMem.EUS_Profiles, False, True)


    '        'Dim tbl As DSMembers.EUS_ProfilesDataTable = dsMem.EUS_Profiles
    '        'Dim view_profile_url As String = config.view_profile_url
    '        'Dim site_name As String = config.site_name
    '        'Dim image_thumb_url As String = config.image_thumb_url
    '        'Dim image_url As String = config.image_url

    '        Dim SYS_EmailMessagesRow As UniCMSDB.SYS_EmailMessagesRow = sendMessageParams.GetMessageRow()
    '        Dim config As New clsConfigValues
    '        Dim cnt As Integer

    '        For cnt = 0 To sendMessageParams.ProfilesList.Count - 1
    '            Try
    '                Dim _profileid As Integer = sendMessageParams.ProfilesList(cnt)
    '                Dim prof = (From itm In dc.EUS_Profiles
    '                            Where itm.ProfileID = _profileid
    '                            Select itm.eMail, itm.LAGID, itm.ProfileID, itm.LoginName).FirstOrDefault()

    '                'OrElse itm.MirrorProfileID = profileid
    '                'ta.FillByProfileID(dsMem.EUS_Profiles, sendMessageParams.ProfilesList(cnt))

    '                'Dim row As DSMembers.EUS_ProfilesRow = dsMem.EUS_Profiles.Rows(0)

    '                If (Not String.IsNullOrEmpty(prof.eMail)) Then 'AndAlso row.eMail = "love@goomena.com"

    '                    Dim subject As String = ""
    '                    Dim body As String = ""
    '                    Dim MessageAction As String = ""

    '                    If (SYS_EmailMessagesRow.MessageAction.ToString() <> "") Then
    '                        MessageAction = SYS_EmailMessagesRow.MessageAction
    '                    End If

    '                    If (SYS_EmailMessagesRow("Subject" & prof.LAGID).ToString() <> "") Then
    '                        subject = SYS_EmailMessagesRow("Subject" & prof.LAGID)
    '                    Else
    '                        subject = SYS_EmailMessagesRow("SubjectUS")
    '                    End If


    '                    If (SYS_EmailMessagesRow("MessageText" & prof.LAGID).ToString() <> "") Then
    '                        body = SYS_EmailMessagesRow("MessageText" & prof.LAGID)
    '                    Else
    '                        body = SYS_EmailMessagesRow("MessageTextUS")
    '                    End If

    '                    If (String.IsNullOrEmpty(MessageAction)) Then
    '                        MessageAction = subject
    '                    End If


    '                    ' send email message
    '                    If (sendMessageParams.SendEmail) Then
    '                        If (insertToEmailsQueue) Then
    '                            If (Not hdl.CheckEmailIfExists(prof.eMail, subject, prof.ProfileID, MessageAction)) Then
    '                                hdl.AddEmail(prof.eMail, subject, body, prof.ProfileID, True, MessageAction)
    '                                sb.AppendLine("Notification " & MessageAction & " added to queue for " & prof.eMail & " - login """ & prof.LoginName & """")
    '                            Else
    '                                sb.AppendLine("Notification " & MessageAction & " already exists for " & prof.eMail & " - login """ & prof.LoginName & """")
    '                            End If
    '                            resultCounter = resultCounter + 1
    '                        Else
    '                            clsMyMail.SendMail(prof.eMail, subject, body, True)
    '                            sb.AppendLine("Notification " & MessageAction & " sent to " & prof.eMail & " - login """ & prof.LoginName & """")
    '                            resultCounter = resultCounter + 1
    '                        End If
    '                    End If


    '                    ' send application message
    '                    If (sendMessageParams.SendApplicationMessage) Then
    '                        clsUserDoes.SendMessageFromAdministrator(subject, body, prof.ProfileID)
    '                    End If

    '                Else
    '                    sb.AppendLine("Email not specified for - login """ & prof.LoginName & """")
    '                End If

    '            Catch ex As Exception
    '                sb.AppendLine(ex.ToString())
    '            End Try
    '        Next


    '    Catch ex As Exception
    '        sb.AppendLine(ex.ToString())
    '    Finally
    '        If (dc IsNot Nothing) Then dc.Dispose()
    '        hdl.Dispose()
    '    End Try

    '    sb.Insert(0, "Profiles processed :" & resultCounter.ToString() & vbCrLf & vbCrLf)

    '    Return sb.ToString()
    'End Function



    'Public Shared Function SendNotificationsToMembers_WithoutPhoto(Optional insertToEmailsQueue As Boolean = False) As String

    '    Dim sb As New StringBuilder
    '    Dim o As New clsSiteLAG()
    '    Dim hdl As New Core.DLL.clsSendingEmailsHandler()
    '    Dim resultCounter As Integer

    '    Try

    '        Dim dsMem As New DSMembers()
    '        Dim isreferrer As Boolean? = Nothing

    '        Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
    '        ta.Connection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
    '        ta.FillBy_Approved_HasOrNotPhotos(dsMem.EUS_Profiles, False, True, Nothing, Nothing, isreferrer)

    '        Dim tbl As DSMembers.EUS_ProfilesDataTable = dsMem.EUS_Profiles
    '        Dim cnt As Integer

    '        Dim config As New clsConfigValues
    '        'Dim view_profile_url As String = config.view_profile_url
    '        'Dim site_name As String = config.site_name
    '        'Dim image_thumb_url As String = config.image_thumb_url
    '        'Dim image_url As String = config.image_url

    '        For cnt = 0 To dsMem.EUS_Profiles.Rows.Count - 1
    '            Dim row As DSMembers.EUS_ProfilesRow = dsMem.EUS_Profiles.Rows(cnt)
    '            If (Not row.IseMailNull()) Then 'AndAlso row.eMail = "love@goomena.com"
    '                Dim subject As String = o.GetCustomStringFromPage(row.LAGID, "GlobalStrings", "NotificationEmailForMemberWithoutPhotoSubject", DataHelpers.ConnectionString)
    '                Dim body As String = ""

    '                If (ProfileHelper.IsFemale(row.GenderId)) Then
    '                    body = o.GetCustomStringFromPage(row.LAGID, "GlobalStrings", "NotificationEmailForMemberWithoutPhoto_FEMALE", DataHelpers.ConnectionString)
    '                Else
    '                    body = o.GetCustomStringFromPage(row.LAGID, "GlobalStrings", "NotificationEmailForMemberWithoutPhoto_MALE", DataHelpers.ConnectionString)
    '                End If
    '                body = body.Replace("###LOGINNAME###", row.LoginName)


    '                If (body.Contains("###GIRLPHOTOS###")) Then
    '                    Try
    '                        Dim photosListHtml As String = RenderPhotosHTML(row, config)
    '                        body = body.Replace("###GIRLPHOTOS###", photosListHtml)
    '                    Catch ex As Exception
    '                        sb.AppendLine(ex.ToString())
    '                    End Try

    '                End If


    '                If (insertToEmailsQueue) Then
    '                    If (Not hdl.CheckEmailIfExists(row.eMail, subject, row.ProfileID, "No Photo Notification")) Then
    '                        hdl.AddEmail(row.eMail, subject, body, row.ProfileID, True, "No Photo Notification")
    '                    End If
    '                    resultCounter = resultCounter + 1
    '                Else
    '                    clsMyMail.SendMail(row.eMail, subject, body, True)
    '                    sb.AppendLine("Notification ""Without Photo"" sent to " & row.eMail & " - login """ & row.LoginName & """")
    '                    resultCounter = resultCounter + 1
    '                End If

    '            End If
    '        Next


    '    Catch ex As Exception
    '        'Throw
    '        sb.AppendLine(ex.ToString())
    '    Finally
    '        hdl.Dispose()
    '    End Try

    '    sb.Insert(0, "Profiles processed :" & resultCounter.ToString() & vbCrLf & vbCrLf)

    '    Return sb.ToString()
    'End Function

    'Shared Function RenderPhotosHTML(row As DSMembers.EUS_ProfilesRow, config As clsConfigValues) As String

    '    Dim photosListHtml As New StringBuilder()
    '    photosListHtml.AppendLine("<div>")

    '    Dim prms As New clsSearchHelperParameters()
    '    prms.CurrentProfileId = row.ProfileID
    '    prms.ReturnRecordsWithStatus = ProfileStatusEnum.Approved
    '    prms.SearchSort = SearchSortEnum.NearestDistance
    '    prms.NumberOfRecordsToReturn = 10
    '    If (Not row.IsZipNull()) Then prms.zipstr = row.Zip
    '    If (Not row.IslatitudeNull()) Then prms.latitudeIn = row.latitude
    '    If (Not row.IslongitudeNull()) Then prms.longitudeIn = row.longitude
    '    If (Not row.IsLookingFor_ToMeetMaleIDNull()) Then prms.LookingFor_ToMeetMaleID = row.LookingFor_ToMeetMaleID
    '    If (Not row.IsLookingFor_ToMeetFemaleIDNull()) Then prms.LookingFor_ToMeetFemaleID = row.LookingFor_ToMeetFemaleID
    '    prms.performCount = False

    '    Dim count As Integer
    '    Dim ds As DataSet = clsSearchHelper.GetMembersToSearchDataTable(prms)
    '    Dim dt As DataTable = ds.Tables(0)
    '    If (dt.Rows.Count > 0) Then
    '        For Each dr As DataRow In dt.Rows

    '            count = count + 1
    '            If count > 5 Then
    '                Exit For
    '            End If

    '            Dim photoUrl As String = ""
    '            Dim drDefaultPhoto As EUS_CustomerPhotosRow = DataHelpers.GetProfilesDefaultPhoto(dr("ProfileID"))
    '            If (drDefaultPhoto IsNot Nothing) Then
    '                photoUrl = String.Format(config.image_thumb_url, drDefaultPhoto.CustomerID, drDefaultPhoto.FileName)
    '            Else
    '                Continue For
    '            End If

    '            Dim atributes As New Dictionary(Of String, String)
    '            atributes.Add("border", "0")
    '            atributes.Add("alt", dr("LoginName"))
    '            atributes.Add("width", "100")
    '            atributes.Add("title", dr("LoginName"))
    '            Dim photoHtml As String = clsHTMLHelper.RenderImageHtml(photoUrl, atributes)

    '            Dim profileUrl As String = String.Format(config.view_profile_url, dr("LoginName"))
    '            Dim linkHtml As String = clsHTMLHelper.RenderLinkHtml(profileUrl, photoHtml)

    '            photosListHtml.AppendLine(linkHtml & "&nbsp;")
    '        Next
    '    End If

    '    photosListHtml.AppendLine("</div>")
    '    Return photosListHtml.ToString()
    'End Function


    'Public Shared Sub SendNotificationsToMembers_AboutNewMember(customerid As Integer, photoUploaded As Boolean)
    '    Try
    '        If (photoUploaded) Then

    '            Dim OnUpdateAutoNotificationSentForPhoto As Boolean = GetEUS_Profiles_OnUpdateAutoNotificationSentForPhoto(customerid)

    '            If (Not OnUpdateAutoNotificationSentForPhoto) Then
    '                Dim dt As DataTable = clsSearchHelper.GetMembersThatShouldBeNotifiedForNewMember(customerid)
    '                If (dt.Rows.Count > 0) Then

    '                    Dim cnt As Integer
    '                    Dim isAnySent As Boolean = False


    '                    For cnt = 0 To dt.Rows.Count - 1
    '                        Dim success As Boolean = False
    '                        Dim toProfileId As Integer = dt.Rows(cnt)("CustomerID")

    '                        success = clsUserNotifications.SendEmailNotification(NotificationType.AutoNotificationForPhoto, customerid, toProfileId)
    '                        If (success) Then isAnySent = True
    '                    Next


    '                    If (isAnySent) Then
    '                        UpdateEUS_Profiles_OnUpdateAutoNotificationSentForPhoto(customerid, True)
    '                    End If

    '                End If
    '            End If

    '        Else

    '            Dim OnUpdateAutoNotificationSent As Boolean = GetEUS_Profiles_OnUpdateAutoNotificationSent(customerid)

    '            If (Not OnUpdateAutoNotificationSent) Then
    '                Dim dt As DataTable = clsSearchHelper.GetMembersThatShouldBeNotifiedForNewMember(customerid)
    '                If (dt.Rows.Count > 0) Then

    '                    Dim cnt As Integer
    '                    Dim isAnySent As Boolean = False


    '                    For cnt = 0 To dt.Rows.Count - 1
    '                        Dim success As Boolean = False
    '                        Dim toProfileId As Integer = dt.Rows(cnt)("CustomerID")

    '                        success = clsUserNotifications.SendEmailNotification(NotificationType.AutoNotification, customerid, toProfileId)

    '                        If (success) Then isAnySent = True
    '                    Next


    '                    If (isAnySent) Then
    '                        UpdateEUS_Profiles_OnUpdateAutoNotificationSent(customerid, True)
    '                    End If

    '                End If
    '            End If

    '        End If


    '    Catch ex As Exception
    '        Throw
    '    End Try

    'End Sub



End Class

﻿Imports System.Text.RegularExpressions
Imports System.Text


Public Class clsSearchHelperParameters
    Public Property CurrentProfileId As Integer
    Public Property ReturnRecordsWithStatus As Integer
    Public Property SearchSort As SearchSortEnum
    Public Property zipstr As String
    Public Property Distance As Integer
    Public Property LookingFor_ToMeetMaleID As Boolean
    Public Property LookingFor_ToMeetFemaleID As Boolean
    Public Property NumberOfRecordsToReturn As Integer = 0
    Public Property AdditionalWhereClause As String = ""
    Public Property latitudeIn As Double?
    Public Property longitudeIn As Double?
    Public Property performCount As Boolean
End Class

Public Class clsSearchHelper

    Public Const DISTANCE_DEFAULT = 1000000


    '    Public Shared Function GetGEOLatLng() As String
    '        Dim geo As String = <sql><![CDATA[

    '    declare @lat as decimal(13,9),@lng as decimal(13,9)

    '    if(isnull(@zip,'') = '')
    '	    SELECT  @lat = latitude,@lng = longitude
    '	      FROM SYS_GEO_GR 
    '	      WHERE  postcode=(select MIN(postcode) FROM SYS_GEO_GR )
    '    else
    '	    SELECT  @lat = latitude,@lng = longitude
    '	      FROM SYS_GEO_GR 
    '	      WHERE  postcode=@zip

    ']]></sql>.Value

    '        Return geo
    '    End Function



    '    Public Shared Function GetGEOLeftJoin() As String
    '        Dim geo As String = <sql><![CDATA[

    '	LEFT OUTER JOIN (			
    '		SELECT DISTINCT 
    '			t.postcode as postcode, 
    '			MIN(SQRT(POWER((@lat-t.latitude)*110.7,2)+POWER((@lng - t.longitude)*75.6,2))) AS distance 
    '		FROM SYS_GEO_GR  as t
    '		WHERE  SQRT(POWER((@lat-t.latitude)*110.7,2)+POWER(( @lng -t.longitude)*75.6,2)) <= @distance
    '		group by t.postcode) as geo ON geo.postcode = EUS_Profiles.zip

    ']]></sql>.Value

    '        Return geo
    '    End Function


    Public Shared Function GetMembersToSearchDataTable(prms As clsSearchHelperParameters) As DataSet

        Dim CurrentProfileId As Integer = prms.CurrentProfileId
        Dim ReturnRecordsWithStatus As Integer = prms.ReturnRecordsWithStatus
        Dim SearchSort As SearchSortEnum = prms.SearchSort
        'Dim zipstr As String = prms.zipstr
        Dim Distance As Integer = prms.Distance
        Dim LookingFor_ToMeetMaleID As Boolean = prms.LookingFor_ToMeetMaleID
        Dim LookingFor_ToMeetFemaleID As Boolean = prms.LookingFor_ToMeetFemaleID
        Dim NumberOfRecordsToReturn As Integer = prms.NumberOfRecordsToReturn
        'Dim AdditionalWhereClause As String = prms.AdditionalWhereClause


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[

--fn:GetMembersToSearchDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;

if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
        WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND (
			    (EUS_Profiles.GenderId = 1  AND 
			    (Select p.LookingFor_ToMeetMaleID from EUS_Profiles p where ProfileID=@CurrentProfileId) = 1)
		    OR
			    (EUS_Profiles.GenderId = 2  AND 
			    (Select p.LookingFor_ToMeetFemaleID from EUS_Profiles p where ProfileID=@CurrentProfileId) = 1)
	    )
	    AND (
		    EUS_Profiles.ProfileID <> @CurrentProfileId or 
		    EUS_Profiles.ProfileID <> (Select p.MirrorProfileID from EUS_Profiles p where ProfileID=@CurrentProfileId)
	    )
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0) = 0
	    AND  not exists(select * 
							    from EUS_ProfilesBlocked bl 
							    where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								    (bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	    )
[AdditionalWhereClause]
    ) as EUS_Profiles
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end

	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
        ,CASE 
			WHEN ISNULL(EUS_Profiles.Birthday, '') = '' then null
			ELSE dbo.fn_GetAge(EUS_Profiles.Birthday, GETDATE())
		End as Age,
		EUS_Profiles.PointsBeauty,    
		EUS_Profiles.PointsVerification,     
		EUS_Profiles.PointsCredits,     
        EUS_Profiles.PointsUnlocks,
        EUS_Profiles.ProfileID, 
        EUS_Profiles.IsMaster, 
        EUS_Profiles.MirrorProfileID, 
        EUS_Profiles.Status,
        EUS_Profiles.LoginName, 
        EUS_Profiles.Password, 
        EUS_Profiles.FirstName,
        EUS_Profiles.LastName, 
        EUS_Profiles.GenderId, 
        EUS_Profiles.Country, 
        EUS_Profiles.Region, 
        EUS_Profiles.City, 
        EUS_Profiles.Zip, 
        EUS_Profiles.CityArea, 
        EUS_Profiles.Address, 
        EUS_Profiles.Telephone, 
        EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, 
        EUS_Profiles.AreYouWillingToTravel, 
        EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, 
        EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, 
        EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, 
        EUS_Profiles.OtherDetails_Occupation, 
        EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, 
        EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, 
        EUS_Profiles.PersonalInfo_ChildrenID, 
        EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, 
        EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, 
        EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 
        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, 
        EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName,
        EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, 
        EUS_Profiles.IsOnline, 
        EUS_Profiles.AccountTypeId, 
        EUS_Profiles.LastActivityDateTime, 
        EUS_Profiles.AvailableCredits, 
        EUS_Profiles.DefPhotoID, 
/*
        IsDefault = case 
		    when DefPhotoID >= 1 then 1
		    else 0
		end,
        CustomerID=EUS_Profiles.ProfileID,
*/
phot.CustomerPhotosID, 
phot.CustomerID, 
phot.DateTimeToUploading, 
phot.FileName, 
phot.DisplayLevel, 
phot.HasAproved, phot.HasDeclined, 
phot.CheckedContextID, 
phot.IsDefault, 
        HasFavoritedMe = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = EUS_Profiles.ProfileID 
                            ),0),
        DidIFavorited = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = EUS_Profiles.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                            ),0),
        CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
		                            ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0)
	FROM		
        dbo.EUS_Profiles AS EUS_Profiles 
		with (nolock)
	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
    WHERE
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.Profileid <> 1
	AND (
			(EUS_Profiles.GenderId = 1  AND 
			(Select p.LookingFor_ToMeetMaleID from EUS_Profiles p where ProfileID=@CurrentProfileId) = 1)
		OR
			(EUS_Profiles.GenderId = 2  AND 
			(Select p.LookingFor_ToMeetFemaleID from EUS_Profiles p where ProfileID=@CurrentProfileId) = 1)
	)
	AND (
		EUS_Profiles.ProfileID <> @CurrentProfileId or 
		EUS_Profiles.ProfileID <> (Select p.MirrorProfileID from EUS_Profiles p where ProfileID=@CurrentProfileId)
	)
	AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0) = 0
	AND  not exists(select * 
							from EUS_ProfilesBlocked bl 
							where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	)
[AdditionalWhereClause]
) as EUS_Profiles
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
]]></sql>.Value



            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())

            If (Not String.IsNullOrEmpty(prms.AdditionalWhereClause)) Then
                'sql = sql & vbCrLf & prms.AdditionalWhereClause
                sql = sql.Replace("[AdditionalWhereClause]", vbCrLf & prms.AdditionalWhereClause)
                'prms.performCount = False
                'NumberOfRecordsToReturn = 0
            Else
                sql = sql.Replace("[AdditionalWhereClause]", "")
            End If


            If (LookingFor_ToMeetFemaleID) Then
                If (SearchSort = SearchSortEnum.NewestMember) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, DateTimeToRegister Desc, distance asc, Age asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.OldestMember) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, DateTimeToRegister Asc, distance asc, Age asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, LastLoginDateTime Desc, distance asc, Age asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                Else
                    'SearchSortEnum.NearestDistance
                    'SearchSortEnum.None
                    sql = sql & vbCrLf & _
                        "order by isdefault desc, distance asc, Age asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
                End If
            Else
                If (SearchSort = SearchSortEnum.NewestMember) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, DateTimeToRegister Desc, distance asc, PointsVerification desc, Age asc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.OldestMember) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, DateTimeToRegister Asc, distance asc, PointsVerification desc, Age asc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, LastLoginDateTime Desc, distance asc, PointsVerification desc, Age asc, PointsCredits desc, PointsUnlocks desc"
                Else
                    'SearchSortEnum.NearestDistance
                    'SearchSortEnum.None
                    sql = sql & vbCrLf & _
                        "order by isdefault desc, distance asc, PointsVerification desc, Age asc, PointsCredits desc, PointsUnlocks desc"
                End If
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", prms.performCount))

            If (prms.zipstr Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", prms.zipstr))
            End If

            If (prms.latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", prms.latitudeIn))
            End If

            If (prms.longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", prms.longitudeIn))
            End If


            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function


    Public Shared Function GetMembersToSearchDataTable_Public(prms As clsSearchHelperParameters) As DataSet

        Dim ReturnRecordsWithStatus As Integer = prms.ReturnRecordsWithStatus
        Dim SearchSort As SearchSortEnum = prms.SearchSort
        Dim Distance As Integer = prms.Distance
        Dim LookingFor_ToMeetMaleID As Boolean = prms.LookingFor_ToMeetMaleID
        Dim LookingFor_ToMeetFemaleID As Boolean = prms.LookingFor_ToMeetFemaleID
        Dim NumberOfRecordsToReturn As Integer = prms.NumberOfRecordsToReturn


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataSet()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try


            sql = <sql><![CDATA[

--fn:GetMembersToSearchDataTable

	SET NOCOUNT ON;

if(@performCount=1)
begin

    select Count(*) from (
	    SELECT    
		    distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
	    FROM		
            dbo.EUS_Profiles AS EUS_Profiles 
		    with (nolock)
	    LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
        WHERE
		    EUS_Profiles.IsMaster = 1
	    AND EUS_Profiles.Profileid <> 1
	    AND (
			    (EUS_Profiles.GenderId = 1  AND  @LookingFor_ToMeetMaleID=1)
		    OR
			    (EUS_Profiles.GenderId = 2  AND  @LookingFor_ToMeetFemaleID=1)
	    )
	    AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	    AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0) = 0
    ) as EUS_Profiles
    where 
      ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
end

	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000)
        ,CASE 
			WHEN ISNULL(EUS_Profiles.Birthday, '') = '' then null
			ELSE dbo.fn_GetAge(EUS_Profiles.Birthday, GETDATE())
		End as Age,
		EUS_Profiles.PointsBeauty,    
		EUS_Profiles.PointsVerification,     
		EUS_Profiles.PointsCredits,     
        EUS_Profiles.PointsUnlocks,
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 
        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, 
        EUS_Profiles.Role, 
        EUS_Profiles.IsOnline, 
        EUS_Profiles.ShowOnFrontPage, 
        EUS_Profiles.ShowOnMosaic, 
        EUS_Profiles.DefPhotoID, 
        IsReferrer = case 
            when ISNULL(EUS_Profiles.ReferrerParentId,0)>0 then 1
            else 0
        end, 
        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, 
        phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, 
        phot.IsDefault, 
        HasFavoritedMe = 0,
        DidIFavorited =0,
        CommunicationUnl = 0,
        VerID = ver.ID,
        VerPhoto = ver.Photo,
        VerTel = ver.Tel,
        VerComplete = ver.Complete,
        VerFacebook = ver.Facebook
	FROM		
        dbo.EUS_Profiles AS EUS_Profiles 
		with (nolock)
	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
	LEFT OUTER JOIN dbo.EUS_ProfilesVerification AS ver ON ver.ProfileID = EUS_Profiles.ProfileID
    WHERE
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.Profileid <> 1
	AND (
			(EUS_Profiles.GenderId = 1  AND  @LookingFor_ToMeetMaleID=1)
		OR
			(EUS_Profiles.GenderId = 2  AND  @LookingFor_ToMeetFemaleID=1)
	)
	AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	AND ISNULL(EUS_Profiles.PrivacySettings_HideMeFromSearchResults,0) = 0
) as EUS_Profiles
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
[AdditionalWhereClause]
]]></sql>.Value


            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())

            If (Not String.IsNullOrEmpty(prms.AdditionalWhereClause)) Then
                'sql = sql & vbCrLf & prms.AdditionalWhereClause
                sql = sql.Replace("[AdditionalWhereClause]", vbCrLf & prms.AdditionalWhereClause)
                prms.performCount = False
                NumberOfRecordsToReturn = 0
            Else
                sql = sql.Replace("[AdditionalWhereClause]", "")
            End If


            If (LookingFor_ToMeetFemaleID) Then
                If (SearchSort = SearchSortEnum.NewestMember) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, ShowOnMosaic desc, ShowOnFrontPage desc, DateTimeToRegister Desc, PointsBeauty desc, Age asc, IsReferrer desc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.OldestMember) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, ShowOnMosaic desc, ShowOnFrontPage desc, DateTimeToRegister Asc, PointsBeauty desc, Age asc, IsReferrer desc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, ShowOnMosaic desc, ShowOnFrontPage desc, LastLoginDateTime Desc, PointsBeauty desc, Age asc, IsReferrer desc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsUnlocks desc"
                Else
                    'SearchSortEnum.NearestDistance
                    'SearchSortEnum.None
                    sql = sql & vbCrLf & _
                        "order by isdefault desc, ShowOnMosaic desc, ShowOnFrontPage desc, distance asc, PointsBeauty desc, Age asc, IsReferrer desc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, Age asc, PointsVerification desc, PointsUnlocks desc"
                End If
            Else
                If (SearchSort = SearchSortEnum.NewestMember) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, ShowOnMosaic desc, ShowOnFrontPage desc, DateTimeToRegister Desc, PointsBeauty desc, Age asc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.OldestMember) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, ShowOnMosaic desc, ShowOnFrontPage desc, DateTimeToRegister Asc, PointsBeauty desc, Age asc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsCredits desc, PointsUnlocks desc"
                ElseIf (SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
                    sql = sql & vbCrLf & _
                        "Order By isdefault desc, ShowOnMosaic desc, ShowOnFrontPage desc, LastLoginDateTime Desc, PointsBeauty desc, Age asc, VerComplete desc, VerID desc, VerPhoto desc, VerTel desc, VerFacebook desc, distance asc, PointsVerification desc, PointsCredits desc, PointsUnlocks desc"
                Else
                    'SearchSortEnum.NearestDistance
                    'SearchSortEnum.None
                    sql = sql & vbCrLf & _
                        "order by isdefault desc, ShowOnMosaic desc, ShowOnFrontPage desc, distance asc, PointsBeauty desc, Age asc, PointsVerification desc,  PointsCredits desc, PointsUnlocks desc"
                End If
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@performCount", prms.performCount))

            If (prms.zipstr Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@zip", prms.zipstr))
            End If

            If (prms.latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", prms.latitudeIn))
            End If

            If (prms.longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", prms.longitudeIn))
            End If

            If (Not prms.LookingFor_ToMeetFemaleID AndAlso Not prms.LookingFor_ToMeetMaleID) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@LookingFor_ToMeetFemaleID", True))
                command.Parameters.Add(New SqlClient.SqlParameter("@LookingFor_ToMeetMaleID", False))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@LookingFor_ToMeetFemaleID", prms.LookingFor_ToMeetFemaleID))
                command.Parameters.Add(New SqlClient.SqlParameter("@LookingFor_ToMeetMaleID", prms.LookingFor_ToMeetMaleID))
            End If

            dt = DataHelpers.GetDataSet(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function


    '    Public Shared Function GetMembersToSearchForDefaultMosaicDataTable(prms As clsSearchHelperParameters) As DataTable

    '        Dim CurrentProfileId As Integer = prms.CurrentProfileId
    '        Dim ReturnRecordsWithStatus As Integer = prms.ReturnRecordsWithStatus
    '        Dim SearchSort As SearchSortEnum = prms.SearchSort
    '        Dim zipstr As String = prms.zipstr
    '        Dim Distance As Integer = prms.Distance
    '        Dim LookingFor_ToMeetMaleID As Boolean = prms.LookingFor_ToMeetMaleID
    '        Dim LookingFor_ToMeetFemaleID As Boolean = prms.LookingFor_ToMeetFemaleID
    '        Dim NumberOfRecordsToReturn As Integer = prms.NumberOfRecordsToReturn
    '        'Dim AdditionalWhereClause As String = prms.AdditionalWhereClause


    '        If (Distance = 0) Then
    '            Distance = DISTANCE_DEFAULT
    '        End If

    '        Dim sql As String = ""
    '        Dim dt As New DataTable()

    '        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
    '        Try

    '            sql = <sql><![CDATA[
    '--fn:GetMembersToSearchForDefaultMosaicDataTable
    '	--@CurrentProfileId int
    '	--,@ReturnRecordsWithStatus int
    '	--,@NumberOfRecordsToReturn int =0
    '	--,@Distance int=0


    '	SET NOCOUNT ON;
    '	if(@NumberOfRecordsToReturn>0)
    '		SET ROWCOUNT @NumberOfRecordsToReturn;

    'select * from (
    '	SELECT    
    '		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
    '        --isNull(geo.postcode, '') as postcode, 
    '        --convert(decimal(13,2),isNull(geo.distance, '1000')) as distance,
    '        CASE 
    '			WHEN ISNULL(EUS_Profiles.Birthday, '') = '' then null
    '			ELSE dbo.fn_GetAge(EUS_Profiles.Birthday, GETDATE())
    '		End as Age,
    '		EUS_Profiles.PointsBeauty,    
    '		EUS_Profiles.PointsVerification,     
    '		EUS_Profiles.PointsCredits,     
    '        EUS_Profiles.PointsUnlocks,
    '        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
    '        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
    '        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
    '        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
    '        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
    '        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
    '        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
    '        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
    '        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
    '        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
    '        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
    '        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
    '        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
    '        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
    '        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
    '        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
    '        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
    '        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
    '        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 
    '        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
    '        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
    '        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
    '        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
    '        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

    '        phot.CustomerPhotosID, 
    '        phot.CustomerID, 
    '        phot.DateTimeToUploading, phot.FileName, 
    '        phot.DisplayLevel, 
    '        phot.HasAproved, phot.HasDeclined, 
    '        phot.CheckedContextID, phot.IsDefault, 

    '        HasFavoritedMe = ISNULL((select COUNT(*)
    '                            from 	dbo.EUS_ProfilesFavorite favMe
    '                            where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = EUS_Profiles.ProfileID 
    '                            ),0),
    '        DidIFavorited = ISNULL((select COUNT(*)
    '                            from 	dbo.EUS_ProfilesFavorite favMe
    '                            where   favMe.ToProfileID = EUS_Profiles.ProfileID AND favMe.FromProfileID = @CurrentProfileId
    '                            ),0),
    '        CommunicationUnl = ISNULL((select COUNT(*)
    '                            from 	dbo.EUS_UnlockedConversations unl
    '                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
    '		                            ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
    '                            ),0)
    '	FROM		
    '        dbo.EUS_Profiles AS EUS_Profiles 
    '		with (nolock)
    '	LEFT OUTER JOIN
    '			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
    '	WHERE
    '		EUS_Profiles.IsMaster = 1
    '	AND EUS_Profiles.Profileid <> 1
    '	AND (
    '		EUS_Profiles.ProfileID <> @CurrentProfileId or 
    '		EUS_Profiles.ProfileID <> (Select p.MirrorProfileID from EUS_Profiles p where ProfileID=@CurrentProfileId)
    '	)
    '	AND EUS_Profiles.Status = @ReturnRecordsWithStatus
    '	AND (EUS_Profiles.PrivacySettings_HideMeFromSearchResults = 'false' or EUS_Profiles.PrivacySettings_HideMeFromSearchResults is null)
    '	AND  not exists(select * 
    '							from EUS_ProfilesBlocked bl 
    '							where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
    '								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
    '	)
    ') as t
    'where 
    '  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
    ']]></sql>.Value


    '            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
    '            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())

    '            If (Not String.IsNullOrEmpty(prms.AdditionalWhereClause)) Then
    '                sql = sql & vbCrLf & prms.AdditionalWhereClause
    '            End If


    '            If (LookingFor_ToMeetFemaleID) Then
    '                If (SearchSort = SearchSortEnum.NewestMember) Then
    '                    sql = sql & vbCrLf & _
    '                        "Order By EUS_Profiles.DateTimeToRegister Desc, distance asc, Age asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
    '                ElseIf (SearchSort = SearchSortEnum.OldestMember) Then
    '                    sql = sql & vbCrLf & _
    '                        "Order By EUS_Profiles.DateTimeToRegister Asc, distance asc, Age asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
    '                ElseIf (SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
    '                    sql = sql & vbCrLf & _
    '                        "Order By phot.isdefault desc, EUS_Profiles.LastLoginDateTime Desc, distance asc, Age asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
    '                Else
    '                    'SearchSortEnum.NearestDistance
    '                    'SearchSortEnum.None
    '                    sql = sql & vbCrLf & _
    '                        "order by phot.isdefault desc, NewID(), distance asc, Age asc, PointsBeauty desc, PointsVerification desc, PointsUnlocks desc"
    '                End If
    '            Else
    '                If (SearchSort = SearchSortEnum.NewestMember) Then
    '                    sql = sql & vbCrLf & _
    '                        "Order By EUS_Profiles.DateTimeToRegister Desc, distance asc, PointsVerification desc, Age asc, PointsCredits desc, PointsUnlocks desc"
    '                ElseIf (SearchSort = SearchSortEnum.OldestMember) Then
    '                    sql = sql & vbCrLf & _
    '                        "Order By EUS_Profiles.DateTimeToRegister Asc, distance asc, PointsVerification desc, Age asc, PointsCredits desc, PointsUnlocks desc"
    '                ElseIf (SearchSort = SearchSortEnum.RecentlyLoggedIn) Then
    '                    sql = sql & vbCrLf & _
    '                        "Order By phot.isdefault desc, EUS_Profiles.LastLoginDateTime Desc, distance asc, PointsVerification desc, Age asc, PointsCredits desc, PointsUnlocks desc"
    '                Else
    '                    'SearchSortEnum.NearestDistance
    '                    'SearchSortEnum.None
    '                    sql = sql & vbCrLf & _
    '                        "order by phot.isdefault desc, NewID(), distance asc, PointsVerification desc, Age asc, PointsCredits desc, PointsUnlocks desc"
    '                End If
    '            End If

    '            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
    '            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", prms.latitudeIn))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", prms.longitudeIn))


    '            dt = DataHelpers.GetDataTable(command)

    '        Catch ex As Exception

    '        End Try

    '        Return dt
    '    End Function



    Public Shared Function GetMembersToSearchUsingGenderDataTable(
                                              ReturnRecordsWithStatus As Integer,
                                              GenderId As Integer,
                                              zipstr As String,
                                              latitudeIn As Double?,
                                              longitudeIn As Double?,
                                              ShowOnFrontPageRequired As Boolean?,
                                              Optional NumberOfRecordsToReturn As Integer = 0,
                                              Optional Distance As Integer = DISTANCE_DEFAULT) As DataTable



        Dim sql As String = ""
        Dim dt As New DataTable()

        Try

            sql = <sql><![CDATA[
    --fn:GetMembersToSearchUsingGenderDataTable
        --@ReturnRecordsWithStatus int
        --,@Gender int
        --,@NumberOfRecordsToReturn int =0


        SET NOCOUNT ON;
        if(@NumberOfRecordsToReturn>0)
        	SET ROWCOUNT @NumberOfRecordsToReturn;


    select * from (
        SELECT    
            --isNull(geo.postcode, '') as postcode, 
            --convert(decimal(13,2),isNull(geo.distance, '1000')) as distance,
        	distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
            CASE 
        		WHEN ISNULL(EUS_Profiles.Birthday, '') = '' then null
        		ELSE dbo.fn_GetAge(EUS_Profiles.Birthday, GETDATE())
        	End as Age,
            EUS_Profiles.ProfileID, 
            EUS_Profiles.IsMaster, 
            EUS_Profiles.MirrorProfileID, 
            EUS_Profiles.Status, 
            EUS_Profiles.LoginName, 
            EUS_Profiles.GenderId, 
            City,
            Region,
            Genderid,
            AboutMe_Heading,
            PersonalInfo_HeightID,
            PersonalInfo_BodyTypeID,
            PersonalInfo_HairColorID,
            PersonalInfo_EyeColorID,
            PersonalInfo_EthnicityID,
            Birthday,
        	EUS_Profiles.PointsBeauty,    
        	EUS_Profiles.PointsVerification,     
        	EUS_Profiles.PointsCredits,     
            EUS_Profiles.PointsUnlocks,
            phot.CustomerPhotosID, 
            phot.CustomerID, 
            phot.DateTimeToUploading, phot.FileName, 
            phot.DisplayLevel, 
            phot.HasAproved, phot.HasDeclined, 
            phot.CheckedContextID, phot.IsDefault
        FROM		
        	dbo.EUS_Profiles AS EUS_Profiles 
        	with (nolock)
    	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
        LEFT OUTER JOIN
        			dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
        WHERE
        	EUS_Profiles.IsMaster = 1
        AND EUS_Profiles.Profileid <> 1
        AND EUS_Profiles.Status = @ReturnRecordsWithStatus
        AND EUS_Profiles.GenderId=@Gender 
        AND (EUS_Profiles.ShowOnFrontPage=@ShowOnFrontPage OR @ShowOnFrontPage is null)
        AND (EUS_Profiles.PrivacySettings_HideMeFromSearchResults='false' or EUS_Profiles.PrivacySettings_HideMeFromSearchResults is null)
    ) as t
    where 
        ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
    ORDER BY  IsDefault desc, NEWID(),LastActivityDateTime Desc, PointsBeauty desc, DateTimeToRegister Desc
    ]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Gender", GenderId))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (ShowOnFrontPageRequired Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@ShowOnFrontPage", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@ShowOnFrontPage", ShowOnFrontPageRequired))
            End If
            command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception

        End Try

        Return dt
    End Function





    Public Shared Function GetWhoViewedMeMembersDataTable(MyListsSort As MyListsSortEnum,
                                                        CurrentProfileId As Integer,
                                                        NumberOfRecordsToReturn As Integer,
                                                        ReturnRecordsWithStatus As Integer,
                                                        zipstr As String,
                                                        latitudeIn As Double?,
                                                        longitudeIn As Double?,
                                                        Optional Distance As Integer = DISTANCE_DEFAULT) As DataTable


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetWhoViewedMeMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

	    viewed.EUS_ProfilesViewedID as ProfilesViewedID, 
	    viewed.DateTimeToCreate as ProfilesViewedDateTimeToCreate, 
	    viewed.FromProfileID as ProfilesViewedFromProfileID, 
	    viewed.ToProfileID as ProfilesViewedToProfileID,

        --fav.EUS_ProfilesFavoriteID as FavoriteID, 
        --fav.DateTimeToCreate as FavoriteDateTimeToCreate, 
        --fav.FromProfileID as FavoriteFromProfileID, 
        --fav.ToProfileID as FavoriteToProfileID, 

        --fav2.EUS_ProfilesFavoriteID as FavoriteID2, 
        --fav2.DateTimeToCreate as FavoriteDateTimeToCreate2, 
        --fav2.FromProfileID as FavoriteFromProfileID2, 
        --fav2.ToProfileID as FavoriteToProfileID2,

	    OffersCount = ISNULL((select COUNT(*) 
		                    from EUS_Offers 
		                    where (ToProfileID=EUS_Profiles.ProfileID and  FromProfileID=@CurrentProfileId)
		                    or (ToProfileID=@CurrentProfileId and FromProfileID=EUS_Profiles.ProfileID)
	                    ),0),
        HasFavoritedMe = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = EUS_Profiles.ProfileID 
                            ),0),
        DidIFavorited = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = EUS_Profiles.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                            ),0),
        CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
		                            ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0)

	FROM		dbo.EUS_Profiles AS EUS_Profiles 
		with (nolock)
	INNER JOIN
			  dbo.EUS_ProfilesViewed AS viewed ON viewed.FromProfileID = EUS_Profiles.ProfileID AND viewed.ToProfileID = @CurrentProfileId  
  	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE
		EUS_Profiles.IsMaster = 1
	 AND EUS_Profiles.[ProfileID] <> 1 
	AND EUS_Profiles.Status = @ReturnRecordsWithStatus

	/* blocked filter */
	AND  not exists(select * 
						from EUS_ProfilesBlocked bl 
						where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
					)

	/* hide me from other users' who viewed me list */
	AND (		(EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 1 and
					not exists(select * 
						from EUS_ProfilesViewed bl 
						where bl.FromProfileID = EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId
				)
				OR 
					EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 0
				OR
					EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList is null
			)
		)
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())

            'If (Not String.IsNullOrEmpty(AdditionalWhereClause)) Then
            '    sql = sql & vbCrLf & AdditionalWhereClause
            'End If

            If (MyListsSort = MyListsSortEnum.Recent) Then
                sql = sql & vbCrLf & _
                    "Order By ProfilesViewedDateTimeToCreate desc"
            ElseIf (MyListsSort = MyListsSortEnum.Oldest) Then
                sql = sql & vbCrLf & _
                    "Order By ProfilesViewedDateTimeToCreate Asc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function


    Public Shared Function GetMyViewedMembersDataTable(MyListsSort As MyListsSortEnum,
                                                        CurrentProfileId As Integer,
                                                        NumberOfRecordsToReturn As Integer,
                                                        ReturnRecordsWithStatus As Integer,
                                                        zipstr As String,
                                                        latitudeIn As Double?,
                                                        longitudeIn As Double?,
                                                        Optional Distance As Integer = DISTANCE_DEFAULT) As DataTable


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetMyViewedMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

	    viewed.EUS_ProfilesViewedID as ProfilesViewedID, 
	    viewed.DateTimeToCreate as ProfilesViewedDateTimeToCreate, 
	    viewed.FromProfileID as ProfilesViewedFromProfileID, 
	    viewed.ToProfileID as ProfilesViewedToProfileID,

	    OffersCount = ISNULL((select COUNT(*) 
		                    from EUS_Offers 
		                    where (ToProfileID=EUS_Profiles.ProfileID and  FromProfileID=@CurrentProfileId)
		                    or (ToProfileID=@CurrentProfileId and FromProfileID=EUS_Profiles.ProfileID)
	                    ),0),
        HasFavoritedMe = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = EUS_Profiles.ProfileID 
                            ),0),
        DidIFavorited = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = EUS_Profiles.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                            ),0),
        CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
		                            ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0)

	FROM		dbo.EUS_Profiles AS EUS_Profiles 
		with (nolock)
	INNER JOIN
			  dbo.EUS_ProfilesViewed AS viewed ON viewed.ToProfileID = EUS_Profiles.ProfileID AND viewed.FromProfileID = @CurrentProfileId  
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE
		EUS_Profiles.IsMaster = 1
	 AND EUS_Profiles.[ProfileID] <> 1 
	AND EUS_Profiles.Status = @ReturnRecordsWithStatus

	/* blocked filter */
	AND  not exists(select * 
						from EUS_ProfilesBlocked bl 
						where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
							(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
					)

	/* hide me from other users' who viewed me list */
	AND (		(EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 1 and
					not exists(select * 
						from EUS_ProfilesViewed bl 
						where bl.FromProfileID = EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId
				)
				OR 
					EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList = 0
				OR
					EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList is null
			)
		)
) as t

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())

            'If (Not String.IsNullOrEmpty(AdditionalWhereClause)) Then
            '    sql = sql & vbCrLf & AdditionalWhereClause
            'End If

            If (MyListsSort = MyListsSortEnum.Recent) Then
                sql = sql & vbCrLf & _
                    "Order By ProfilesViewedDateTimeToCreate desc"
            ElseIf (MyListsSort = MyListsSortEnum.Oldest) Then
                sql = sql & vbCrLf & _
                    "Order By ProfilesViewedDateTimeToCreate Asc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function



    Public Shared Function GetMyBlockedMembersDataTable(MyListsSort As MyListsSortEnum,
                                                        CurrentProfileId As Integer,
                                                        NumberOfRecordsToReturn As Integer,
                                                        ReturnRecordsWithStatus As Integer,
                                                        zipstr As String,
                                                        latitudeIn As Double?,
                                                        longitudeIn As Double?,
                                                        Optional Distance As Integer = DISTANCE_DEFAULT) As DataTable


        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetMyBlockedMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

	    blocked.EUS_ProfilesBlockedID as ProfilesBlockedID, 
	    blocked.DateTimeToCreate as ProfilesBlockedDateTimeToCreate, 
	    blocked.FromProfileID as ProfilesBlockedFromProfileID, 
	    blocked.ToProfileID as ProfilesBlockedToProfileID,

	    OffersCount = ISNULL((select COUNT(*) 
		                    from EUS_Offers 
		                    where (ToProfileID=EUS_Profiles.ProfileID and  FromProfileID=@CurrentProfileId)
		                    or (ToProfileID=@CurrentProfileId and FromProfileID=EUS_Profiles.ProfileID)
	                    ),0),
        HasFavoritedMe = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = EUS_Profiles.ProfileID 
                            ),0),
        DidIFavorited = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = EUS_Profiles.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                            ),0),
        CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
		                            ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0)

	FROM		dbo.EUS_Profiles AS EUS_Profiles 
		with (nolock)
	INNER JOIN
			  dbo.EUS_ProfilesBlocked AS blocked ON blocked.ToProfileID = EUS_Profiles.ProfileID AND blocked.FromProfileID = @CurrentProfileId  
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE
		EUS_Profiles.IsMaster = 1
	 AND EUS_Profiles.[ProfileID] <> 1 
	AND EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())

            'If (Not String.IsNullOrEmpty(AdditionalWhereClause)) Then
            '    sql = sql & vbCrLf & AdditionalWhereClause
            'End If

            If (MyListsSort = MyListsSortEnum.Recent) Then
                sql = sql & vbCrLf & _
                    "Order By ProfilesBlockedDateTimeToCreate desc"
            ElseIf (MyListsSort = MyListsSortEnum.Oldest) Then
                sql = sql & vbCrLf & _
                    "Order By ProfilesBlockedDateTimeToCreate Asc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function


    Public Shared Function GetWinksDataTable(CurrentProfileId As Integer,
                                                  ReturnRecordsWithStatus As Integer,
                                                  sorting As OffersSortEnum,
                                                  zipstr As String,
                                                  latitudeIn As Double?,
                                                  longitudeIn As Double?,
                                                  Optional Distance As Integer = DISTANCE_DEFAULT,
                                                  Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetWinksDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID
	FROM
			dbo.EUS_Profiles EUS_Profiles
	INNER JOIN		
			dbo.EUS_Offers offr ON offr.FromProfileID = EUS_Profiles.ProfileID
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	 AND EUS_Profiles.[ProfileID] <> 1 
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='PENDING')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							 FROM [dbo].[EUS_OffersTypes]  
							 WHERE [ConstantName]='WINK' OR [ConstantName]='POKE')
	AND offr.ToProfileID = @CurrentProfileId
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function



    '    Public Shared Function GetPokesDataTable(CurrentProfileId As Integer,
    '                                                  ReturnRecordsWithStatus As Integer,
    '                                                  sorting As OffersSortEnum,
    '                                                  zipstr As String,
    '                                                  Optional Distance As Integer = DISTANCE_DEFAULT,
    '                                                  Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

    '        If (Distance = 0) Then
    '            Distance = DISTANCE_DEFAULT
    '        End If

    '        Dim sql As String = ""
    '        Dim dt As New DataTable()

    '        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
    '        Try

    '            sql = <sql><![CDATA[
    '	--@CurrentProfileId int
    '	--,@ReturnRecordsWithStatus int
    '	--,@NumberOfRecordsToReturn int =0
    '	--,@Distance int=0

    '	###GetGEOLatLng###

    '	SET NOCOUNT ON;
    '	if(@NumberOfRecordsToReturn>0)
    '		SET ROWCOUNT @NumberOfRecordsToReturn;



    '	SELECT    
    '         isNull(geo.postcode, '') as postcode, isNull(geo.distance, '1000') as distance,
    '        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
    '        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
    '        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
    '        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
    '        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
    '        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
    '        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
    '        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
    '        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
    '        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
    '        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
    '        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
    '        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
    '        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
    '        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
    '        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
    '        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
    '        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
    '        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 
    '
    '        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
    '        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
    '        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
    '        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
    '        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

    '        phot.CustomerPhotosID, 
    '        phot.CustomerID, 
    '        phot.DateTimeToUploading, phot.FileName, 
    '        phot.DisplayLevel, 
    '        phot.HasAproved, phot.HasDeclined, 
    '        phot.CheckedContextID, phot.IsDefault, 

    '		offr.OfferID, 
    '		offr.DateTimeToCreate as OffersDateTimeToCreate , 
    '		offr.OfferTypeID as OffersOfferTypeID, 
    '		offr.FromProfileID as OffersFromProfileID, 
    '		offr.ToProfileID as OffersToProfileID, 
    '		offr.Amount as OffersAmount, 
    '		offr.StatusID as OffersStatusID
    '	FROM
    '			dbo.EUS_Profiles EUS_Profiles
    '	INNER JOIN		
    '			dbo.EUS_Offers offr ON offr.FromProfileID = EUS_Profiles.ProfileID
    '	LEFT OUTER JOIN
    '			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
    '	###GetGEOLeftJoin###
    '	WHERE     
    '		EUS_Profiles.IsMaster = 1
    '	 AND EUS_Profiles.[ProfileID] <> 1 
    '	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
    '						 FROM [dbo].[EUS_OffersStatus]  
    '						 WHERE [ConstantName]='PENDING')
    '	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
    '							 FROM [dbo].[EUS_OffersTypes]  
    '							 WHERE [ConstantName]='POKE')
    '	AND offr.ToProfileID = @CurrentProfileId
    '	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
    '    --AND (distance <= @Distance  OR distance is null)
    '    AND ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT or distance is null) and @Distance =@DISTANCE_DEFAULT))

    ']]></sql>.Value

    '            sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
    '            sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


    '            If (sorting = OffersSortEnum.OfferAmountHighest) Then
    '                sql = sql & vbCrLf & _
    '                    "order by OffersAmount desc"
    '            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
    '                sql = sql & vbCrLf & _
    '                    "Order By OffersAmount asc"
    '            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
    '                sql = sql & vbCrLf & _
    '                    "Order By OffersDateTimeToCreate Asc"
    '            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
    '                sql = sql & vbCrLf & _
    '                    "Order By OffersDateTimeToCreate Desc"
    '            End If

    '            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
    '            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
    '            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))


    '            dt = DataHelpers.GetDataTable(command)

    '        Catch ex As Exception

    '        End Try

    '        Return dt
    '    End Function



    Public Shared Function GetNewOffersDataTable(CurrentProfileId As Integer,
                                                  ReturnRecordsWithStatus As Integer,
                                                  sorting As OffersSortEnum,
                                                  zipstr As String,
                                                  latitudeIn As Double?,
                                                  longitudeIn As Double?,
                                                  Optional Distance As Integer = DISTANCE_DEFAULT,
                                                  Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetNewOffersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID
	FROM
			dbo.EUS_Profiles EUS_Profiles
	INNER JOIN		
			dbo.EUS_Offers offr ON offr.FromProfileID = EUS_Profiles.ProfileID
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.[ProfileID] <> 1 
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='PENDING' OR [ConstantName]='COUNTER')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							 FROM [dbo].[EUS_OffersTypes]  
							 WHERE [ConstantName]='OFFERCOUNTER' OR  [ConstantName]='OFFERNEW')
	AND offr.ToProfileID = @CurrentProfileId
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function




    Public Shared Function GetPendingOffersDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetPendingOffersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID
	FROM
			dbo.EUS_Profiles EUS_Profiles
	INNER JOIN		
			dbo.EUS_Offers offr ON offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.[ProfileID] <> 1 
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='PENDING' OR [ConstantName]='COUNTER')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]
                            FROM [EUS_OffersTypes] 
						    WHERE [ConstantName]='OFFERNEW' OR [ConstantName]='OFFERCOUNTER')
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try


        Return dt
    End Function





    Public Shared Function GetPendingLikesDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetPendingLikesDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0



	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;


select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID
	FROM
			dbo.EUS_Profiles EUS_Profiles
	INNER JOIN		
			dbo.EUS_Offers offr ON offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.[ProfileID] <> 1 
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='PENDING' OR [ConstantName]='COUNTER')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]
                            FROM [EUS_OffersTypes] 
						    WHERE [ConstantName]='WINK' OR [ConstantName]='POKE')
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try


        Return dt
    End Function



    Public Shared Function GetRejectedOffersDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetRejectedOffersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0



	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID

	FROM
			dbo.EUS_Profiles EUS_Profiles
	LEFT OUTER JOIN		
			dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID 
				OR offr.FromProfileID = EUS_Profiles.ProfileID AND offr.ToProfileID =@CurrentProfileId)
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	 AND EUS_Profiles.[ProfileID] <> 1 
	AND (
		(offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName] like 'REJECT%' )
		AND (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID 
				OR offr.FromProfileID = EUS_Profiles.ProfileID AND offr.ToProfileID =@CurrentProfileId)
		) OR (
		offr.StatusID IN (SELECT [EUS_OffersStatusID]  
							 FROM [dbo].[EUS_OffersStatus]  
							 WHERE [ConstantName] like 'CANCELED' )
			AND offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID
		))
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							 FROM [dbo].[EUS_OffersTypes]  
							 WHERE [ConstantName]='OFFERCOUNTER' OR  [ConstantName]='OFFERNEW')
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function



    Public Shared Function GetRejectedLikesDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetRejectedLikesDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0



	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID

	FROM
			dbo.EUS_Profiles EUS_Profiles
	LEFT OUTER JOIN		
			dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID 
				OR offr.FromProfileID = EUS_Profiles.ProfileID AND offr.ToProfileID =@CurrentProfileId)
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	 AND EUS_Profiles.[ProfileID] <> 1 
	AND (
		(offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName] like 'REJECT%' )
		AND (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID 
				OR offr.FromProfileID = EUS_Profiles.ProfileID AND offr.ToProfileID =@CurrentProfileId)
		) OR (
		offr.StatusID IN (SELECT [EUS_OffersStatusID]  
							 FROM [dbo].[EUS_OffersStatus]  
							 WHERE [ConstantName] like 'CANCELED' )
			AND offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID
		))
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							 FROM [dbo].[EUS_OffersTypes]  
							 WHERE [ConstantName]='WINK' OR [ConstantName]='POKE')
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try


        Return dt
    End Function


    Public Shared Function GetNewDatesDataTable(CurrentProfileId As Integer,
                                                ReturnRecordsWithStatus As Integer,
                                                sorting As DatesSortEnum,
                                                zipstr As String,
                                                latitudeIn As Double?,
                                                longitudeIn As Double?,
                                                Optional Distance As Integer = DISTANCE_DEFAULT,
                                                Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetNewDatesDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, 
        EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, 
        EUS_Profiles.Country, EUS_Profiles.Region, EUS_Profiles.City, 
        EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, 
        EUS_Profiles.Telephone, EUS_Profiles.eMail, EUS_Profiles.Cellular, 
        EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, 
        EUS_Profiles.PersonalInfo_HeightID, EUS_Profiles.PersonalInfo_BodyTypeID, 
        EUS_Profiles.PersonalInfo_EyeColorID, EUS_Profiles.PersonalInfo_HairColorID, 
        EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
        EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 
EUS_Profiles.DateTimeToRegister, 
        EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, 
        EUS_Profiles.LastUpdateProfileGEOInfo, EUS_Profiles.LAGID, EUS_Profiles.ThemeName, 
        EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, EUS_Profiles.Birthday, 
        EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

        offr.OfferID, 
        offr.DateTimeToCreate as OffersDateTimeToCreate , 
        offr.OfferTypeID as OffersOfferTypeID, 
        offr.FromProfileID as OffersFromProfileID, 
        offr.ToProfileID as OffersToProfileID, 
        offr.Amount as OffersAmount, 
        offr.StatusID as OffersStatusID,
        offr.IsToProfileIDViewedDate as OffersIsToProfileIDViewedDate,

        CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
		                            ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0),
        LastMessageDate = (select top(1) DateTimeToCreate 
                            from 	dbo.EUS_Messages msgLast
                            where   (
                                        ( msgLast.FromProfileID = @CurrentProfileId AND msgLast.ToProfileID = EUS_Profiles.ProfileID) OR
		                                ( msgLast.FromProfileID = EUS_Profiles.ProfileID  AND msgLast.ToProfileID = @CurrentProfileId)
                                    )
                                    and ISNULL(msgLast.IsHidden,0)=0
		                    order by  DateTimeToCreate desc )
	FROM
			dbo.EUS_Profiles EUS_Profiles --, dbo.EUS_Offers offr
	LEFT OUTER JOIN		
			dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID 
				OR offr.FromProfileID = EUS_Profiles.ProfileID AND offr.ToProfileID =@CurrentProfileId)
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.Profileid <> 1
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='UNLOCKED')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							 FROM [dbo].[EUS_OffersTypes]  
							 WHERE  [ConstantName]='NEWDATE')
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	AND  not exists(select * 
							from EUS_ProfilesBlocked bl 
							where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	)
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = DatesSortEnum.NewDates) Then
                sql = sql & vbCrLf & _
                    "order by OffersIsToProfileIDViewedDate asc, OffersDateTimeToCreate Desc"

            ElseIf (sorting = DatesSortEnum.ByDate) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"

            ElseIf (sorting = DatesSortEnum.ByStatus) Then
                sql = sql & vbCrLf & _
                    "Order By CommunicationUnl desc"

            ElseIf (sorting = DatesSortEnum.ByLastDating) Then
                sql = sql & vbCrLf & _
                    "Order By LastMessageDate Desc"

            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try


        Return dt
    End Function



    Public Shared Function GetNewDatesDataTable(CurrentProfileId As Integer,
                                                OtherProfileId As Integer,
                                                ReturnRecordsWithStatus As Integer,
                                                sorting As DatesSortEnum,
                                                zipstr As String,
                                                latitudeIn As Double?,
                                                longitudeIn As Double?,
                                                Optional Distance As Integer = DISTANCE_DEFAULT,
                                                Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetNewDatesDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, 
        EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, 
        EUS_Profiles.Country, EUS_Profiles.Region, EUS_Profiles.City, 
        EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, 
        EUS_Profiles.Telephone, EUS_Profiles.eMail, EUS_Profiles.Cellular, 
        EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, 
        EUS_Profiles.PersonalInfo_HeightID, EUS_Profiles.PersonalInfo_BodyTypeID, 
        EUS_Profiles.PersonalInfo_EyeColorID, EUS_Profiles.PersonalInfo_HairColorID, 
        EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, 
        EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 
        EUS_Profiles.DateTimeToRegister, 
        EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, 
        EUS_Profiles.LastUpdateProfileGEOInfo, EUS_Profiles.LAGID, EUS_Profiles.ThemeName, 
        EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, EUS_Profiles.Birthday, 
        EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

        offr.OfferID, 
        offr.DateTimeToCreate as OffersDateTimeToCreate , 
        offr.OfferTypeID as OffersOfferTypeID, 
        offr.FromProfileID as OffersFromProfileID, 
        offr.ToProfileID as OffersToProfileID, 
        offr.Amount as OffersAmount, 
        offr.StatusID as OffersStatusID,

        CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
		                            ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0),
        LastMessageDate = (select top(1) DateTimeToCreate 
                            from 	dbo.EUS_Messages msgLast
                            where   (
                                        ( msgLast.FromProfileID = @CurrentProfileId AND msgLast.ToProfileID = EUS_Profiles.ProfileID) OR
		                                ( msgLast.FromProfileID = EUS_Profiles.ProfileID  AND msgLast.ToProfileID = @CurrentProfileId)
                                    )
                                    and ISNULL(msgLast.IsHidden,0)=0
		                    order by  DateTimeToCreate desc )
	FROM
			dbo.EUS_Profiles EUS_Profiles 
	LEFT OUTER JOIN		
			dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID 
				OR offr.FromProfileID = EUS_Profiles.ProfileID AND offr.ToProfileID =@CurrentProfileId)
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.Profileid <> 1
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='UNLOCKED')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							 FROM [dbo].[EUS_OffersTypes]  
							 WHERE  [ConstantName]='NEWDATE')
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
    AND EUS_Profiles.ProfileId = @OtherProfileId
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@OtherProfileId", OtherProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function


    Public Shared Function GetAcceptedOffersDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetAcceptedOffersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0



	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;


select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID

	FROM
			dbo.EUS_Profiles EUS_Profiles --, dbo.EUS_Offers offr
	LEFT OUTER JOIN		
			dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID 
				OR offr.FromProfileID = EUS_Profiles.ProfileID AND offr.ToProfileID =@CurrentProfileId)
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.Profileid <> 1
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='ACCEPTED' OR  [ConstantName]='OFFER_ACCEEPTED_WITH_MESSAGE')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							 FROM [dbo].[EUS_OffersTypes]  
							 WHERE  [ConstantName]='OFFERCOUNTER' OR  [ConstantName]='OFFERNEW')
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function



    Public Shared Function GetAcceptedLikesDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable

        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetAcceptedLikesDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0



	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID

	FROM
			dbo.EUS_Profiles EUS_Profiles --, dbo.EUS_Offers offr
	LEFT OUTER JOIN		
			dbo.EUS_Offers offr on (offr.FromProfileID = @CurrentProfileId AND offr.ToProfileID = EUS_Profiles.ProfileID 
				OR offr.FromProfileID = EUS_Profiles.ProfileID AND offr.ToProfileID =@CurrentProfileId)
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.Profileid <> 1
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='LIKE_ACCEEPTED_WITH_OFFER' OR [ConstantName]='LIKE_ACCEEPTED_WITH_MESSAGE' OR [ConstantName]='LIKE_ACCEEPTED_WITH_POKE'
								OR [ConstantName]='POKE_ACCEEPTED_WITH_OFFER' OR [ConstantName]='POKE_ACCEEPTED_WITH_MESSAGE')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
					 	 FROM [dbo].[EUS_OffersTypes]  
					 	 WHERE [ConstantName]='WINK' OR [ConstantName]='POKE')
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function



    Public Shared Function GetNewDatesQuickDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable
        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetNewDatesQuickDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;


select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		ISNULL(offr.Amount, 0) as OffersAmount, 
		offr.StatusID as OffersStatusID
	FROM
			dbo.EUS_Profiles EUS_Profiles
	INNER JOIN		
		    dbo.EUS_Offers offr	on offr.ToProfileID= @CurrentProfileId AND offr.FromProfileID= EUS_Profiles.ProfileID
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.Profileid <> 1
    AND (offr.StatusID=200) --UNLOCKED
    AND (offr.OfferTypeID=100) --NEWDATE
    AND (ISNULL(offr.IsToProfileIDViewedDate,0)=0)
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function



    Public Shared Function GetNewOffersQuickDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable
        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetNewOffersQuickDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0

	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID


	FROM
			dbo.EUS_Profiles EUS_Profiles
	INNER JOIN		
			dbo.EUS_Offers offr ON offr.FromProfileID = EUS_Profiles.ProfileID
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	 AND EUS_Profiles.[ProfileID] <> 1 
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='PENDING' OR [ConstantName]='COUNTER')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							 FROM [dbo].[EUS_OffersTypes]  
							 WHERE [ConstantName]='OFFERNEW' OR [ConstantName]='OFFERCOUNTER')
	AND offr.ToProfileID = @CurrentProfileId
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))

]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "order by OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "Order By OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "Order By OffersDateTimeToCreate Desc"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If

            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function



    Public Shared Function GetLikesQuickDataTable(CurrentProfileId As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      sorting As OffersSortEnum,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?,
                                                      Optional Distance As Integer = DISTANCE_DEFAULT,
                                                      Optional NumberOfRecordsToReturn As Integer = 0) As DataTable
        If (Distance = 0) Then
            Distance = DISTANCE_DEFAULT
        End If

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetLikesQuickDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

        phot.CustomerPhotosID, 
        phot.CustomerID, 
        phot.DateTimeToUploading, phot.FileName, 
        phot.DisplayLevel, 
        phot.HasAproved, phot.HasDeclined, 
        phot.CheckedContextID, phot.IsDefault, 

		offr.OfferID, 
		offr.DateTimeToCreate as OffersDateTimeToCreate , 
		offr.OfferTypeID as OffersOfferTypeID, 
		offr.FromProfileID as OffersFromProfileID, 
		offr.ToProfileID as OffersToProfileID, 
		offr.Amount as OffersAmount, 
		offr.StatusID as OffersStatusID
	FROM
			dbo.EUS_Profiles EUS_Profiles
	INNER JOIN		
			dbo.EUS_Offers offr ON offr.FromProfileID = EUS_Profiles.ProfileID
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE     
		EUS_Profiles.IsMaster = 1
	 AND EUS_Profiles.[ProfileID] <> 1 
	AND offr.StatusID IN (SELECT [EUS_OffersStatusID]  
						 FROM [dbo].[EUS_OffersStatus]  
						 WHERE [ConstantName]='PENDING')
	AND offr.OfferTypeID IN (SELECT [EUS_OffersTypeID]  
							 FROM [dbo].[EUS_OffersTypes]  
							 WHERE [ConstantName]='WINK' OR [ConstantName]='POKE')
	AND offr.ToProfileID = @CurrentProfileId
	AND	EUS_Profiles.Status = @ReturnRecordsWithStatus
	AND  not exists(select * 
							from EUS_ProfilesBlocked bl 
							where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	)
) as t
where 
  ((distance <= @Distance and @Distance <@DISTANCE_DEFAULT)  OR ((distance <= @DISTANCE_DEFAULT) and @Distance =@DISTANCE_DEFAULT))
]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())


            If (sorting = OffersSortEnum.OfferAmountHighest) Then
                sql = sql & vbCrLf & _
                    "ORDER BY NEWID(), OffersAmount desc"
            ElseIf (sorting = OffersSortEnum.OfferAmountLowest) Then
                sql = sql & vbCrLf & _
                    "ORDER BY NEWID(), OffersAmount asc"
            ElseIf (sorting = OffersSortEnum.OldestOffers) Then
                sql = sql & vbCrLf & _
                    "ORDER BY NEWID(), OffersDateTimeToCreate Asc"
            ElseIf (sorting = OffersSortEnum.RecentOffers) Then
                sql = sql & vbCrLf & _
                    "ORDER BY NEWID(), OffersDateTimeToCreate Desc"
            Else
                sql = sql & vbCrLf & _
                    "ORDER BY NEWID()"
            End If

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", CurrentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", Distance))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function



    Public Shared Function GetMembersThatShouldBeNotifiedForNewMember(NewProfileID As Integer) As DataTable

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            '            sql = <sql><![CDATA[

            'declare
            '	@crGenderId varchar(50),
            '	@crCountry nvarchar(50),
            '	@crRegion nvarchar(250),
            '	@crCity nvarchar(250),
            '	@PersonalInfo_BodyTypeID varchar(50),
            '	@PersonalInfo_EyeColorID varchar(50),
            '	@PersonalInfo_HairColorID varchar(50),
            '	@PersonalInfo_HeightID varchar(50),
            '	@PersonalInfo_EthnicityID varchar(50),
            '	@PersonalInfo_ReligionID varchar(50),
            '	@PersonalInfo_SmokingHabitID varchar(50),
            '	@PersonalInfo_DrinkingHabitID varchar(50),
            '	@LookingFor_RelationshipStatusID varchar(50),
            '	@PersonalInfo_ChildrenID varchar(50),
            '	@Age varchar(50),
            '	@zip varchar(50)	
            '--	,@customerid int
            '--	set @customerid=269


            'select 
            '	@Age = CASE 
            '			WHEN ISNULL(Birthday, '') = '' then null
            '			ELSE dbo.fn_GetAge(Birthday, GETDATE())
            '	End,
            '	@zip = Zip,
            '	@crGenderId=CAST(GenderId as varchar(50)),  
            '	@crCountry=Country,  
            '	@crRegion=Region,
            '	@crCity=City,
            '	@PersonalInfo_BodyTypeID=CAST(PersonalInfo_BodyTypeID as varchar(50)), 
            '	@PersonalInfo_EyeColorID=CAST(PersonalInfo_EyeColorID as varchar(50)),  
            '	@PersonalInfo_HairColorID=CAST(PersonalInfo_HairColorID as varchar(50)),
            '	@PersonalInfo_HeightID=CAST(PersonalInfo_HeightID as varchar(50)),
            '	@PersonalInfo_EthnicityID=CAST(PersonalInfo_EthnicityID as varchar(50)), 
            '	@PersonalInfo_ReligionID=CAST(PersonalInfo_ReligionID as varchar(50)),
            '	@PersonalInfo_SmokingHabitID=CAST(PersonalInfo_SmokingHabitID as varchar(50)), 
            '	@PersonalInfo_DrinkingHabitID=CAST(PersonalInfo_DrinkingHabitID as varchar(50)),
            '	@LookingFor_RelationshipStatusID=CAST(LookingFor_RelationshipStatusID as varchar(50)),  
            '	@PersonalInfo_ChildrenID=CAST(PersonalInfo_ChildrenID as varchar(50))
            'from EUS_Profiles
            'where (ProfileID = @customerid or MirrorProfileID = @customerid ) and ismaster=1 and status=4


            'declare @lat as decimal(13,9),@lng as decimal(13,9)

            'if(isnull(@zip,'') = '')
            '    SELECT  @lat = latitude,@lng = longitude
            '      FROM SYS_GEO_GR 
            '      WHERE  postcode=(select MIN(postcode) FROM SYS_GEO_GR )
            'else
            '    SELECT  @lat = latitude,@lng = longitude
            '      FROM SYS_GEO_GR 
            '      WHERE  postcode=@zip



            'select
            '    EUS_AutoNotificationSettings.CustomerId,
            '    EUS_Profiles.OnUpdateAutoNotificationSent
            '    --distance, EUS_AutoNotificationSettings.* 
            'from EUS_AutoNotificationSettings 
            '		with (nolock)
            'inner join EUS_Profiles as EUS_Profiles on EUS_Profiles.ProfileID = EUS_AutoNotificationSettings.CustomerID 
            'LEFT OUTER JOIN (			
            '	SELECT DISTINCT 
            '		t.postcode as postcode, 
            '		MIN(SQRT(POWER((@lat-t.latitude)*110.7,2)+POWER((@lng - t.longitude)*75.6,2))) AS distance 
            '	FROM SYS_GEO_GR  as t
            '	--WHERE  SQRT(POWER((@lat-t.latitude)*110.7,2)+POWER(( @lng -t.longitude)*75.6,2)) <= EUS_AutoNotificationSettings.crDistance
            '	group by t.postcode) as geo ON geo.postcode = EUS_Profiles.zip
            'where 
            '    EUS_Profiles.NotificationsSettings_WhenNewMembersNearMe = 1
            'and (
            '	crGenderId = @crGenderId
            ')			
            'and(
            '	crCountry = @crCountry
            '	or ISNULL(crCountry,'') = ''
            ')
            'and(
            '    crRegion in (
            '        select distinct lag1.region1 
            '        from dbo.SYS_GEO_GR as lag1
            '        left join SYS_GEO_GR as lag2 on lag1.postcode= lag2.postcode
            '        where lag2.region1 = @crRegion
            '        and lag1.language <> lag2.language
            '    )  or 
            '    crRegion = @crRegion or
            '    ISNULL(crRegion,'') = ''
            ')
            'and(
            '    crCity in (
            '        select distinct lag1.city 
            '        from dbo.SYS_GEO_GR as lag1
            '        left join SYS_GEO_GR as lag2 on lag1.postcode= lag2.postcode
            '        where lag2.city = @crCity
            '        and lag1.language <> lag2.language
            '    ) or 
            '    crCity = @crCity or
            '    ISNULL(crCity,'') = ''
            ')
            'and(
            '	crBodyTypeId like @PersonalInfo_BodyTypeID
            '	or	crBodyTypeId like @PersonalInfo_BodyTypeID+',%'
            '	or	crBodyTypeId like '%,'+@PersonalInfo_BodyTypeID+',%'
            '	or	crBodyTypeId like '%,'+@PersonalInfo_BodyTypeID
            '	or ISNULL(crBodyTypeId,'') = ''
            ')
            'and(
            '	crEyeColorId like @PersonalInfo_EyeColorID
            '	or	crEyeColorId like @PersonalInfo_EyeColorID+',%'
            '	or	crEyeColorId like '%,'+@PersonalInfo_EyeColorID+',%'
            '	or	crEyeColorId like '%,'+@PersonalInfo_EyeColorID
            '	or ISNULL(crEyeColorId,'') = ''
            ')
            'and(
            '	crHairColorId like @PersonalInfo_HairColorID
            '	or	crHairColorId like @PersonalInfo_HairColorID+',%'
            '	or	crHairColorId like '%,'+@PersonalInfo_HairColorID+',%'
            '	or	crHairColorId like '%,'+@PersonalInfo_HairColorID
            '	or ISNULL(crHairColorId,'') = ''
            ')
            'and(
            '	crChildrenId  like @PersonalInfo_ChildrenId
            '	or	crChildrenId like @PersonalInfo_ChildrenId+',%'
            '	or	crChildrenId like '%,'+@PersonalInfo_ChildrenId+',%'
            '	or	crChildrenId like '%,'+@PersonalInfo_ChildrenId
            '	or ISNULL(crChildrenId,'') = ''
            ')
            'and(
            '	crEthnicityId like @PersonalInfo_EthnicityID
            '	or	crEthnicityId like @PersonalInfo_EthnicityID+',%'
            '	or	crEthnicityId like '%,'+@PersonalInfo_EthnicityID+',%'
            '	or	crEthnicityId like '%,'+@PersonalInfo_EthnicityID
            '	or ISNULL(crEthnicityId,'') = ''
            ')
            'and(
            '	crReligionId like @PersonalInfo_ReligionID
            '	or	crReligionId like @PersonalInfo_ReligionID+',%'
            '	or	crReligionId like '%,'+@PersonalInfo_ReligionID+',%'
            '	or	crReligionId like '%,'+@PersonalInfo_ReligionID
            '	or ISNULL(crReligionId,'') = ''
            ')
            'and(
            '	crSmokingId  like @PersonalInfo_SmokingHabitID
            '	or	crSmokingId like @PersonalInfo_SmokingHabitID+',%'
            '	or	crSmokingId like '%,'+@PersonalInfo_SmokingHabitID+',%'
            '	or	crSmokingId like '%,'+@PersonalInfo_SmokingHabitID
            '	or ISNULL(crSmokingId,'') = ''
            ')
            'and(
            '	crDrinkingId  like @PersonalInfo_DrinkingHabitID
            '	or	crDrinkingId like @PersonalInfo_DrinkingHabitID+',%'
            '	or	crDrinkingId like '%,'+@PersonalInfo_DrinkingHabitID+',%'
            '	or	crDrinkingId like '%,'+@PersonalInfo_DrinkingHabitID
            '	or ISNULL(crDrinkingId,'') = ''
            ')
            'and(
            '	crRelationshipStatusId  like @LookingFor_RelationshipStatusID
            '	or	crRelationshipStatusId like @LookingFor_RelationshipStatusID+',%'
            '	or	crRelationshipStatusId like '%,'+@LookingFor_RelationshipStatusID+',%'
            '	or	crRelationshipStatusId like '%,'+@LookingFor_RelationshipStatusID
            '	or ISNULL(crRelationshipStatusId,'') = ''
            ')
            'and(
            '	crHeightId  like @PersonalInfo_HeightID
            '	or	crHeightId like @PersonalInfo_HeightID+',%'
            '	or	crHeightId like '%,'+@PersonalInfo_HeightID+',%'
            '	or	crHeightId like '%,'+@PersonalInfo_HeightID
            '	or ISNULL(crHeightId,'') = ''
            ')
            'and(
            '	crDistance >= distance
            '	or ISNULL(crDistance,0) = 0
            ')
            'and(
            '	crAgeMax >= @Age
            '	or ISNULL(crAgeMax,0) = 0
            ')
            'and(
            '	crAgeMin <= @Age
            '	or ISNULL(crAgeMin,0) = 0
            ')

            ']]></sql>.Value


            sql = "exec GetMembersThatShouldBeNotifiedForNewMember @CustomerID=@CustomerID;"
            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CustomerID", NewProfileID))


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function


    Public Shared Function GetMembersDistance(CurrentProfileID As Integer, OtherProfileID As Integer) As Single
        Dim result As Single = DISTANCE_DEFAULT
        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            '            sql = <sql><![CDATA[
            '    set nocount on;

            '    declare @otherZip nvarchar(20), @zip nvarchar(20)

            '    select @otherZip = Zip
            '    from EUS_Profiles p
            '    where p.ProfileID=@otherProfileId

            '    select @zip = Zip
            '    from EUS_Profiles p
            '    where p.ProfileID=@profileId

            '    declare @lat as decimal(13,3),@lng as decimal(13,3)

            '    if(isnull(@zip,'') = '')
            '        SELECT  @lat = latitude,@lng = longitude
            '          FROM SYS_GEO_GR 
            '          WHERE  postcode=(select MIN(postcode) FROM SYS_GEO_GR )
            '    else
            '        SELECT  @lat = latitude,@lng = longitude
            '          FROM SYS_GEO_GR 
            '          WHERE  postcode=@zip

            '    SELECT DISTINCT 
            '	    t.postcode as postcode, 
            '	    cast(MIN(SQRT(POWER((@lat-t.latitude)*110.7,2)+POWER((@lng - t.longitude)*75.6,2))) as  decimal(13, 3)) AS distance 
            '    FROM SYS_GEO_GR  as t
            '    where t.postcode = @otherZip
            '    group by t.postcode

            ']]></sql>.Value

            sql = "exec GetMembersDistance @customerId=@profileId, @otherCustomerId=@otherProfileId;"
            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@otherProfileId", OtherProfileID))
            command.Parameters.Add(New SqlClient.SqlParameter("@profileId", CurrentProfileID))

            dt = DataHelpers.GetDataTable(command)

            If (dt.Rows.Count > 0) Then
                If (Not dt.Rows(0).IsNull("distance")) Then
                    result = dt.Rows(0)("distance")
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return result
    End Function



    Public Shared Function GetWhoFavoritedMeMembersDataTable(sorting As MyListsSortEnum,
                                                      currentProfileId As Integer,
                                                      NumberOfRecordsToReturn As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?) As DataTable

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetWhoFavoritedMeMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        CASE 
			WHEN ISNULL(EUS_Profiles.Birthday, '') = '' then null
			ELSE dbo.fn_GetAge(EUS_Profiles.Birthday, GETDATE())
		End as Age,
		EUS_Profiles.PointsBeauty,    
		EUS_Profiles.PointsVerification,     
		EUS_Profiles.PointsCredits,     
        EUS_Profiles.PointsUnlocks,
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 

        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

		phot.CustomerPhotosID, 
		phot.CustomerID, 
		phot.DateTimeToUploading, 
		phot.FileName, 
		phot.DisplayLevel, 
		phot.HasAproved, 
		phot.HasDeclined, 
		phot.CheckedContextID, 
		phot.IsDefault, 

		fav.EUS_ProfilesFavoriteID as ProfilesFavoriteID, 
		fav.DateTimeToCreate as ProfilesFavoriteDateTimeToCreate, 
		fav.FromProfileID as ProfilesFavoriteFromProfileID, 
		fav.ToProfileID as ProfilesFavoriteToProfileID,
		OffersCount = ISNULL((select COUNT(*)
		                    from EUS_Offers 
		                    where (ToProfileID=EUS_Profiles.ProfileID and  FromProfileID=@CurrentProfileId)
			                or (ToProfileID=@CurrentProfileId and FromProfileID=EUS_Profiles.ProfileID)
                            ),0),
        HasFavoritedMe = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = EUS_Profiles.ProfileID 
                            ),0),
        DidIFavorited = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = EUS_Profiles.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                            ),0),
        CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
		                            ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0)
	FROM		
        dbo.EUS_Profiles AS EUS_Profiles 
		with (nolock)
	INNER JOIN
			  dbo.EUS_ProfilesFavorite AS fav ON fav.FromProfileID = EUS_Profiles.ProfileID AND fav.ToProfileID = @CurrentProfileId  
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.Profileid <> 1
	AND (
		EUS_Profiles.ProfileID <> @CurrentProfileId or 
		EUS_Profiles.ProfileID <> (Select p.MirrorProfileID from EUS_Profiles p where ProfileID=@CurrentProfileId)
	)
	AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	AND  not exists(select * 
							from EUS_ProfilesBlocked bl 
							where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	)
) as t
]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())

            If (sorting = MyListsSortEnum.Recent) Then
                sql = sql & vbCrLf & _
                    "Order By isdefault desc, ProfilesFavoriteDateTimeToCreate Desc"

            ElseIf (sorting = MyListsSortEnum.Oldest) Then
                sql = sql & vbCrLf & _
                    "Order By isdefault desc, ProfilesFavoriteDateTimeToCreate Asc"
            End If


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", currentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function



    Public Shared Function GetMyFavoriteMembersDataTable(sorting As MyListsSortEnum,
                                                      currentProfileId As Integer,
                                                      NumberOfRecordsToReturn As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?) As DataTable

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetMyFavoriteMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        CASE 
			WHEN ISNULL(EUS_Profiles.Birthday, '') = '' then null
			ELSE dbo.fn_GetAge(EUS_Profiles.Birthday, GETDATE())
		End as Age,
		EUS_Profiles.PointsBeauty,    
		EUS_Profiles.PointsVerification,     
		EUS_Profiles.PointsCredits,     
        EUS_Profiles.PointsUnlocks,
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 
        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

		phot.CustomerPhotosID, 
		phot.CustomerID, 
		phot.DateTimeToUploading, 
		phot.FileName, 
		phot.DisplayLevel, 
		phot.HasAproved, 
		phot.HasDeclined, 
		phot.CheckedContextID, 
		phot.IsDefault, 

		fav.EUS_ProfilesFavoriteID as ProfilesFavoriteID, 
		fav.DateTimeToCreate as ProfilesFavoriteDateTimeToCreate, 
		fav.FromProfileID as ProfilesFavoriteFromProfileID, 
		fav.ToProfileID as ProfilesFavoriteToProfileID,
		OffersCount = ISNULL((select COUNT(*)
		                    from EUS_Offers 
		                    where (ToProfileID=EUS_Profiles.ProfileID and  FromProfileID=@CurrentProfileId)
			                or (ToProfileID=@CurrentProfileId and FromProfileID=EUS_Profiles.ProfileID)
                            ),0),
        HasFavoritedMe = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = EUS_Profiles.ProfileID 
                            ),0),
        DidIFavorited = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = EUS_Profiles.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                            ),0),
        CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
		                            ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0)
	FROM		
        dbo.EUS_Profiles AS EUS_Profiles 
		with (nolock)
	INNER JOIN
			  dbo.EUS_ProfilesFavorite AS fav ON fav.ToProfileID = EUS_Profiles.ProfileID AND fav.FromProfileID = @CurrentProfileId  
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.Profileid <> 1
	AND (
		EUS_Profiles.ProfileID <> @CurrentProfileId or 
		EUS_Profiles.ProfileID <> (Select p.MirrorProfileID from EUS_Profiles p where ProfileID=@CurrentProfileId)
	)
	AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	AND  not exists(select * 
							from EUS_ProfilesBlocked bl 
							where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	)
    AND ISNULL(EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList,0) = 0
) as t
]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())

            If (sorting = MyListsSortEnum.Recent) Then
                sql = sql & vbCrLf & _
                    "Order By isdefault desc, ProfilesFavoriteDateTimeToCreate Desc"

            ElseIf (sorting = MyListsSortEnum.Oldest) Then
                sql = sql & vbCrLf & _
                    "Order By isdefault desc, ProfilesFavoriteDateTimeToCreate Asc"
            End If


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", currentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function


    Public Shared Function GetWhoSharedPhotoMembersDataTable(sorting As MyListsSortEnum,
                                                      currentProfileId As Integer,
                                                      NumberOfRecordsToReturn As Integer,
                                                      ReturnRecordsWithStatus As Integer,
                                                      zipstr As String,
                                                      latitudeIn As Double?,
                                                      longitudeIn As Double?) As DataTable

        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            sql = <sql><![CDATA[
--fn:GetWhoSharedPhotoMembersDataTable
	--@CurrentProfileId int
	--,@ReturnRecordsWithStatus int
	--,@NumberOfRecordsToReturn int =0
	--,@Distance int=0


	SET NOCOUNT ON;
	if(@NumberOfRecordsToReturn>0)
		SET ROWCOUNT @NumberOfRecordsToReturn;



select * from (
	SELECT    
		distance =isNull(SQRT(POWER((@latitudeIn-EUS_Profiles.latitude)*110.7,2)+POWER((@longitudeIn - EUS_Profiles.longitude)*75.6,2)), 1000),
        CASE 
			WHEN ISNULL(EUS_Profiles.Birthday, '') = '' then null
			ELSE dbo.fn_GetAge(EUS_Profiles.Birthday, GETDATE())
		End as Age,
		EUS_Profiles.PointsBeauty,    
		EUS_Profiles.PointsVerification,     
		EUS_Profiles.PointsCredits,     
        EUS_Profiles.PointsUnlocks,
        EUS_Profiles.ProfileID, EUS_Profiles.IsMaster, EUS_Profiles.MirrorProfileID, EUS_Profiles.Status, EUS_Profiles.LoginName, EUS_Profiles.Password, 
        EUS_Profiles.FirstName, EUS_Profiles.LastName, EUS_Profiles.GenderId, EUS_Profiles.Country, EUS_Profiles.Region, 
        EUS_Profiles.City, EUS_Profiles.Zip, EUS_Profiles.CityArea, EUS_Profiles.Address, EUS_Profiles.Telephone, EUS_Profiles.eMail, 
        EUS_Profiles.Cellular, EUS_Profiles.AreYouWillingToTravel, EUS_Profiles.AboutMe_Heading, 
        EUS_Profiles.AboutMe_DescribeYourself, EUS_Profiles.AboutMe_DescribeAnIdealFirstDate, 
        EUS_Profiles.OtherDetails_EducationID, EUS_Profiles.OtherDetails_AnnualIncomeID, 
        EUS_Profiles.OtherDetails_NetWorthID, EUS_Profiles.OtherDetails_Occupation, EUS_Profiles.PersonalInfo_HeightID, 
        EUS_Profiles.PersonalInfo_BodyTypeID, EUS_Profiles.PersonalInfo_EyeColorID, 
        EUS_Profiles.PersonalInfo_HairColorID, EUS_Profiles.PersonalInfo_ChildrenID, EUS_Profiles.PersonalInfo_EthnicityID,
        EUS_Profiles.PersonalInfo_ReligionID, EUS_Profiles.PersonalInfo_SmokingHabitID, 
        EUS_Profiles.PersonalInfo_DrinkingHabitID, EUS_Profiles.LookingFor_ToMeetMaleID, 
        EUS_Profiles.LookingFor_ToMeetFemaleID, EUS_Profiles.LookingFor_RelationshipStatusID, 
        EUS_Profiles.LookingFor_TypeOfDating_ShortTermRelationship, EUS_Profiles.LookingFor_TypeOfDating_Friendship,
        EUS_Profiles.LookingFor_TypeOfDating_LongTermRelationship, 
        EUS_Profiles.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, EUS_Profiles.LookingFor_TypeOfDating_MarriedDating, 
        EUS_Profiles.LookingFor_TypeOfDating_AdultDating_Casual, 
        EUS_Profiles.PrivacySettings_HideMeFromSearchResults, EUS_Profiles.PrivacySettings_HideMeFromMembersIHaveBlocked, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersFavoritedList, 
        EUS_Profiles.PrivacySettings_NotShowInOtherUsersViewedList, EUS_Profiles.PrivacySettings_NotShowMyGifts, 
        EUS_Profiles.DateTimeToRegister, EUS_Profiles.RegisterIP, EUS_Profiles.RegisterGEOInfos, EUS_Profiles.RegisterUserAgent,
        EUS_Profiles.LastLoginDateTime, EUS_Profiles.LastLoginIP, EUS_Profiles.LastLoginGEOInfos, 
        EUS_Profiles.LastUpdateProfileDateTime, EUS_Profiles.LastUpdateProfileIP, EUS_Profiles.LastUpdateProfileGEOInfo, 
        EUS_Profiles.LAGID, EUS_Profiles.ThemeName, EUS_Profiles.Referrer, EUS_Profiles.CustomReferrer, 
        EUS_Profiles.Birthday, EUS_Profiles.Role, EUS_Profiles.AccountTypeId, 

		phot.CustomerPhotosID, 
		phot.CustomerID, 
		phot.DateTimeToUploading, 
		phot.FileName, 
		phot.DisplayLevel, 
		phot.HasAproved, 
		phot.HasDeclined, 
		phot.CheckedContextID, 
		phot.IsDefault, 

		fav.ProfilePhotosLevelID as SharePhotosID, 
		fav.DateTimeCreated as SharePhotosDateTimeCreated, 
		fav.FromProfileID as SharePhotosFromProfileID, 
		fav.ToProfileID as SharePhotosToProfileID,
		OffersCount = ISNULL((select COUNT(*)
		                    from EUS_Offers 
		                    where (ToProfileID=EUS_Profiles.ProfileID and  FromProfileID=@CurrentProfileId)
			                or (ToProfileID=@CurrentProfileId and FromProfileID=EUS_Profiles.ProfileID)
                            ),0),
        HasFavoritedMe = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = @CurrentProfileId AND favMe.FromProfileID = EUS_Profiles.ProfileID 
                            ),0),
        DidIFavorited = ISNULL((select COUNT(*)
                            from 	dbo.EUS_ProfilesFavorite favMe
                            where   favMe.ToProfileID = EUS_Profiles.ProfileID AND favMe.FromProfileID = @CurrentProfileId
                            ),0),
        CommunicationUnl = ISNULL((select COUNT(*)
                            from 	dbo.EUS_UnlockedConversations unl
                            where   ( unl.FromProfileID = @CurrentProfileId AND unl.ToProfileID = EUS_Profiles.ProfileID) OR
		                            ( unl.FromProfileID = EUS_Profiles.ProfileID  AND unl.ToProfileID = @CurrentProfileId)
                            ),0)
	FROM		
        dbo.EUS_Profiles AS EUS_Profiles 
		with (nolock)
	INNER JOIN
			  dbo.EUS_ProfilePhotosLevel AS fav ON fav.FromProfileID = EUS_Profiles.ProfileID AND fav.ToProfileID = @CurrentProfileId  
   	LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.CustomerPhotosID = EUS_Profiles.DefPhotoID
/*
	LEFT OUTER JOIN
			  dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = EUS_Profiles.ProfileID AND phot.IsDefault = 1 AND phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
*/
	WHERE
		EUS_Profiles.IsMaster = 1
	AND EUS_Profiles.Profileid <> 1
	AND (
		EUS_Profiles.ProfileID <> @CurrentProfileId or 
		EUS_Profiles.ProfileID <> (Select p.MirrorProfileID from EUS_Profiles p where ProfileID=@CurrentProfileId)
	)
	AND EUS_Profiles.Status = @ReturnRecordsWithStatus
	AND  not exists(select * 
							from EUS_ProfilesBlocked bl 
							where (bl.FromProfileID =EUS_Profiles.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = EUS_Profiles.ProfileID)
	)
	AND exists(
		select CustomerPhotosID 
		from EUS_CustomerPhotos p
		where p.CustomerID = fav.FromProfileID 
		and p.DisplayLevel>0
		and p.HasAproved=1
		and ISNULL(p.IsDeleted,0)=0
	)
) as t
]]></sql>.Value

            'sql = sql.Replace("###GetGEOLatLng###", GetGEOLatLng())
            'sql = sql.Replace("###GetGEOLeftJoin###", GetGEOLeftJoin())

            If (sorting = MyListsSortEnum.Recent) Then
                sql = sql & vbCrLf & _
                    "Order By isdefault desc, SharePhotosDateTimeCreated Desc"

            ElseIf (sorting = MyListsSortEnum.Oldest) Then
                sql = sql & vbCrLf & _
                    "Order By isdefault desc, SharePhotosDateTimeCreated Asc"
            End If


            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            command.Parameters.Add(New SqlClient.SqlParameter("@CurrentProfileId", currentProfileId))
            command.Parameters.Add(New SqlClient.SqlParameter("@ReturnRecordsWithStatus", ReturnRecordsWithStatus))
            command.Parameters.Add(New SqlClient.SqlParameter("@NumberOfRecordsToReturn", NumberOfRecordsToReturn))
            command.Parameters.Add(New SqlClient.SqlParameter("@Distance", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@DISTANCE_DEFAULT", DISTANCE_DEFAULT))
            command.Parameters.Add(New SqlClient.SqlParameter("@zip", zipstr))
            If (latitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@latitudeIn", latitudeIn))
            End If
            If (longitudeIn Is Nothing) Then
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", System.DBNull.Value))
            Else
                command.Parameters.Add(New SqlClient.SqlParameter("@longitudeIn", longitudeIn))
            End If


            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        End Try

        Return dt
    End Function


End Class

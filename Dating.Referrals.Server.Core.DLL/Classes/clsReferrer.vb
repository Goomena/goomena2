﻿Imports System.Text

Public Class clsReferrer

    Public Const StopLevel As Integer = 5
    Public Const DefaultDaysRange As Integer = 30

    Public Sub GetProfilesReferrers(ByRef ds As DSMembers, targetLevel As Integer, profileId As Integer)
        ' get persons list for target level

        Dim mySubReferrers As New List(Of Integer)
        If (targetLevel > 0) Then
            mySubReferrers = GetPersonsId(targetLevel, profileId)
        Else
            mySubReferrers.Add(profileId)
        End If


        If (mySubReferrers.Count > 0) Then
            Dim sb As New StringBuilder()
            Dim c As Integer
            For c = 0 To mySubReferrers.Count - 1
                Dim _profileId As Integer = mySubReferrers(c)
                sb.Append(_profileId & ",")
            Next
            If (sb.Length > 0) Then
                sb.Remove(sb.Length - 1, 1)
            End If

            Dim sql As String = <sql><![CDATA[
select 
    *
from eus_profiles
where ProfileID in (@SubReferrers)
and IsMaster = 1
]]></sql>.Value '
            sql = sql.Replace("@SubReferrers", sb.ToString())
            Dim sdr As SqlClient.SqlDataReader = DataHelpers.GetDataReader(sql)
            ds.EUS_Profiles.Load(sdr, LoadOption.OverwriteChanges)
        End If

        ' Return ds.EUS_Profiles
    End Sub



    Private Function GetPersonsId(targetLevel As Integer, profileId As Integer) As List(Of Integer)
        Dim mySubReferrers As New List(Of Integer)

        Dim level As Integer = 0

        If (profileId > 1) Then
            level = 1
            mySubReferrers.Add(profileId)
        Else
            level = 0
            mySubReferrers.Add(profileId)
        End If


        '        ' level 1
        '        Try

        '            Dim sql As String = <sql><![CDATA[
        'select 
        '    --COUNT(ProfileID) as ProfilesCount
        '    ProfileID
        'from eus_profiles
        'where referrerparentid in (@ProfileID)
        'and IsMaster = 1
        ']]></sql>.Value '
        '            sql = sql.Replace("@ProfileID", profileId)
        '            Dim _dt As DataTable = DataHelpers.GetDataTable(sql)

        '            Dim c As Integer
        '            For c = 0 To _dt.Rows.Count - 1
        '                Dim _profileId As Integer = _dt.Rows(c)("ProfileID")
        '                mySubReferrers.Add(_profileId)
        '            Next

        '        Catch ex As Exception
        '            Throw
        '        End Try


        ' next levels
        While (mySubReferrers.Count > 0 AndAlso level < targetLevel AndAlso level < StopLevel)

            Try

                Dim sb As New StringBuilder()
                Dim c As Integer
                For c = 0 To mySubReferrers.Count - 1
                    Dim _profileId As Integer = mySubReferrers(c)
                    sb.Append(_profileId & ",")
                Next
                If (sb.Length > 0) Then
                    sb.Remove(sb.Length - 1, 1)
                End If
                mySubReferrers.Clear()


                Dim sql As String = <sql><![CDATA[
select 
    ProfileID
from eus_profiles
with(nolock)
where referrerparentid in (@SubReferrers)
and IsMaster = 1
]]></sql>.Value
                sql = sql.Replace("@SubReferrers", sb.ToString())
                Dim _dt As DataTable = DataHelpers.GetDataTable(sql)

                For c = 0 To _dt.Rows.Count - 1
                    Dim _profileId As Integer = _dt.Rows(c)("ProfileID")
                    mySubReferrers.Add(_profileId)
                Next

            Catch ex As Exception
                Throw
            End Try

            level = level + 1
        End While


        Return mySubReferrers
    End Function





    Public Sub GetProfilesForReferrerParentIdList(ByRef ds As DSMembers, mySubReferrers As List(Of Integer))

        Dim mySubReferrersLevelDown As New List(Of Integer)

        Try
            ds.EUS_Profiles.Clear()

            Dim sb As New StringBuilder()
            Dim c As Integer
            For c = 0 To mySubReferrers.Count - 1
                Dim _profileId As Integer = mySubReferrers(c)
                sb.Append(_profileId & ",")
            Next
            If (sb.Length > 0) Then
                sb.Remove(sb.Length - 1, 1)
            End If
            mySubReferrers.Clear()


            Dim sql As String = <sql><![CDATA[
select 
    ProfileID
from eus_profiles
with(nolock)
where referrerparentid in (@SubReferrers)
and IsMaster = 1
]]></sql>.Value
            sql = sql.Replace("@SubReferrers", sb.ToString())
            Dim _dt As DataTable = DataHelpers.GetDataTable(sql)

            For c = 0 To _dt.Rows.Count - 1
                Dim _profileId As Integer = _dt.Rows(c)("ProfileID")
                mySubReferrersLevelDown.Add(_profileId)
            Next

        Catch ex As Exception
            Throw
        End Try


        If (mySubReferrersLevelDown.Count > 0) Then
            Dim sb As New StringBuilder()
            Dim c As Integer
            For c = 0 To mySubReferrersLevelDown.Count - 1
                Dim _profileId As Integer = mySubReferrersLevelDown(c)
                sb.Append(_profileId & ",")
            Next
            If (sb.Length > 0) Then
                sb.Remove(sb.Length - 1, 1)
            End If

            Dim sql As String = <sql><![CDATA[
select 
    *
from eus_profiles
where ProfileID in (@SubReferrers)
and IsMaster = 1
]]></sql>.Value '
            sql = sql.Replace("@SubReferrers", sb.ToString())
            Dim sdr As SqlClient.SqlDataReader = DataHelpers.GetDataReader(sql)
            ds.EUS_Profiles.Load(sdr, LoadOption.OverwriteChanges)
        End If

        'Return ds.EUS_Profiles
    End Sub


    Public Shared Sub SetCommissionForMessage(MsgID As Integer, RefID As Integer, cost As Double, creditsAmount As Integer, unlockType As UnlockType, CustomerCreditsId As Long)

        If (MsgID > 0 AndAlso RefID > 0) Then

            Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
            Try

                Dim parentRefId As Integer? = (From itm In _CMSDBDataContext.EUS_Profiles
                                                    Where itm.ProfileID = RefID
                                                    Select itm.ReferrerParentId).SingleOrDefault()

                If (parentRefId > 0) Then

                    Dim message As EUS_Message = (From itm In _CMSDBDataContext.EUS_Messages
                                                    Where itm.EUS_MessageID = MsgID AndAlso (itm.IsHidden Is Nothing OrElse itm.IsHidden = False)
                                                    Select itm).SingleOrDefault()


                    If (message IsNot Nothing) Then
                        message.Cost = cost
                        message.CreditsSpent = creditsAmount
                        message.CustomerCreditsId = CustomerCreditsId

                        Dim EUS_CreditsTypeID As Integer = clsUserDoes.GetRequiredCredits(unlockType).EUS_CreditsTypeID
                        message.EUS_CreditsTypeID = EUS_CreditsTypeID


                        Dim refComm As New clsReferrerCommissions()

                        If (RefID > 0) Then
                            refComm.Load(RefID)
                            message.REF_L1_Commission = refComm.REF_CommissionLevel1
                            message.REF_L1_Money = refComm.REF_CommissionLevel1 * cost
                        End If


                        Dim RefID2 As Integer, RefID3 As Integer, RefID4 As Integer, RefID5 As Integer
                        RefID2 = DataHelpers.GetEUS_Profiles_ReferrerParentId(RefID)


                        If (RefID2 > 1) Then
                            refComm.Load(RefID2)
                            message.REF_L2_Commission = refComm.REF_CommissionLevel2
                            message.REF_L2_Money = refComm.REF_CommissionLevel2 * cost

                            RefID3 = DataHelpers.GetEUS_Profiles_ReferrerParentId(RefID2)
                        End If

                        If (RefID3 > 1) Then
                            refComm.Load(RefID3)
                            message.REF_L3_Commission = refComm.REF_CommissionLevel3
                            message.REF_L3_Money = refComm.REF_CommissionLevel3 * cost

                            RefID4 = DataHelpers.GetEUS_Profiles_ReferrerParentId(RefID3)
                        End If

                        If (RefID4 > 1) Then
                            refComm.Load(RefID4)
                            message.REF_L4_Commission = refComm.REF_CommissionLevel4
                            message.REF_L4_Money = refComm.REF_CommissionLevel4 * cost

                            RefID5 = DataHelpers.GetEUS_Profiles_ReferrerParentId(RefID4)
                        End If

                        If (RefID5 > 1) Then
                            ' initial referrer has parent  at level 5,  above of himself
                            refComm.Load(RefID5)
                            message.REF_L5_Commission = refComm.REF_CommissionLevel5
                            message.REF_L5_Money = refComm.REF_CommissionLevel5 * cost
                        End If


                        _CMSDBDataContext.SubmitChanges()
                    End If


                End If 'parentRefId > 0


            Catch ex As Exception
                Throw
            Finally
                _CMSDBDataContext.Dispose()
            End Try

        End If

    End Sub


End Class

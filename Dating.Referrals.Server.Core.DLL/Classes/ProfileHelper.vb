﻿Imports Dating.Referrals.Server.Core.DLL

Public Class ProfileHelper

    Public Const gMemberPhotosDirectory As String = "~/Photos/{0}/"
    Public Const gMemberPhotosThumbsDirectory As String = "~/Photos/{0}/thumbs/"
    Public Const gMemberPhotosDirectoryView As String = "http://photos.goomena.com/{0}/"
    Public Const gMemberPhotosThumbsDirectoryView As String = "http://photos.goomena.com/{0}/thumbs/"

    Public Const gMemberDocsDirectory As String = "~/Documents/{0}/"
    Public Const gMemberDocsDirectoryView As String = "~/Documents/{0}/"

    '' Lazy loading
    Private Shared _gMaleGender As Core.DLL.DSLists.EUS_LISTS_GenderRow
    Public Shared ReadOnly Property gMaleGender As Core.DLL.DSLists.EUS_LISTS_GenderRow
        Get
            If (_gMaleGender Is Nothing) Then
                _gMaleGender = Lists.gDSLists.EUS_LISTS_Gender.Single(Function(itm) itm.US = "Male")
            End If
            Return _gMaleGender
        End Get
    End Property


    Public Shared Sub gMaleGenderReset()
        _gMaleGender = Nothing
    End Sub



    '' Lazy loading
    Private Shared _gFemaleGender As Core.DLL.DSLists.EUS_LISTS_GenderRow
    Public Shared ReadOnly Property gFemaleGender As Core.DLL.DSLists.EUS_LISTS_GenderRow
        Get
            If (_gFemaleGender Is Nothing) Then
                _gFemaleGender = Lists.gDSLists.EUS_LISTS_Gender.Single(Function(itm) itm.US = "Female")
            End If
            Return _gFemaleGender
        End Get
    End Property


    Public Shared Sub gFemaleGenderReset()
        _gFemaleGender = Nothing
    End Sub


    Private Shared _gGenerousMember As Core.DLL.DSLists.EUS_LISTS_AccountTypeRow
    Public Shared ReadOnly Property gGenerousMember As Core.DLL.DSLists.EUS_LISTS_AccountTypeRow
        Get
            If (_gGenerousMember Is Nothing) Then
                _gGenerousMember = Lists.gDSLists.EUS_LISTS_AccountType.Single(Function(itm) itm.US.StartsWith("Generous:"))
            End If
            Return _gGenerousMember
        End Get
    End Property


    Public Shared Sub gGenerousMemberReset()
        _gGenerousMember = Nothing
    End Sub


    Private Shared _gAttractiveMember As Core.DLL.DSLists.EUS_LISTS_AccountTypeRow
    Public Shared ReadOnly Property gAttractiveMember As Core.DLL.DSLists.EUS_LISTS_AccountTypeRow
        Get
            If (_gAttractiveMember Is Nothing) Then
                _gAttractiveMember = Lists.gDSLists.EUS_LISTS_AccountType.Single(Function(itm) itm.US.StartsWith("Attractive:"))
            End If
            Return _gAttractiveMember
        End Get
    End Property


    Public Shared Sub gAttractiveMemberReset()
        _gAttractiveMember = Nothing
    End Sub



    Public Shared Function IsMale(GenderId As Integer)
        If (GenderId = ProfileHelper.gMaleGender.GenderId) Then
            Return True
        End If
        Return False
    End Function


    Public Shared Function IsFemale(GenderId As Integer)
        If (GenderId = ProfileHelper.gFemaleGender.GenderId) Then
            Return True
        End If
        Return False
    End Function


    Public Shared Function IsGenerous(AccountTypeId As Integer)
        If (AccountTypeId = Generous_AccountTypeId) Then
            Return True
        End If
        Return False
    End Function


    Public Shared Function IsAttractive(AccountTypeId As Integer)
        If (AccountTypeId = Attractive_AccountTypeId) Then
            Return True
        End If
        Return False
    End Function


    Private Shared _Generous_AccountTypeId As Integer
    Public Shared ReadOnly Property Generous_AccountTypeId As Integer
        Get
            If (_Generous_AccountTypeId = 0) Then
                _Generous_AccountTypeId = (From itm In Lists.gDSLists.EUS_LISTS_AccountType
                                          Where Not itm.IsConstantNameNull AndAlso itm.ConstantName = "GENEROUS"
                                            Select itm.AccountTypeId).FirstOrDefault()
            End If
            Return _Generous_AccountTypeId
        End Get
    End Property


    Private Shared _Attractive_AccountTypeId As Integer
    Public Shared ReadOnly Property Attractive_AccountTypeId As Integer
        Get
            If (_Attractive_AccountTypeId = 0) Then
                _Attractive_AccountTypeId = (From itm In Lists.gDSLists.EUS_LISTS_AccountType
                                            Where Not itm.IsConstantNameNull AndAlso itm.ConstantName = "ATTRACTIVE"
                                                Select itm.AccountTypeId).FirstOrDefault()
            End If
            Return _Attractive_AccountTypeId
        End Get
    End Property


    Public Shared ReadOnly Property Male_DefaultImage As String
        Get
            Return "~/Images/guy.jpg"
        End Get
    End Property

    Public Shared ReadOnly Property Female_DefaultImage As String
        Get
            Return "~/Images/girl.jpg"
        End Get
    End Property



    Public Shared Function GetDefaultImageURL(GenderId As Integer) As String
        Dim photoPath As String = ""
        If ProfileHelper.IsMale(GenderId) Then

            photoPath = System.Web.VirtualPathUtility.ToAbsolute(ProfileHelper.Male_DefaultImage)

        ElseIf ProfileHelper.IsFemale(GenderId) Then

            photoPath = System.Web.VirtualPathUtility.ToAbsolute(ProfileHelper.Female_DefaultImage)

        End If
        Return photoPath
    End Function


    Public Shared Function GetProfileImageURL(ProfileId As Integer?, FileName As String, GenderId As Integer, isThumb As Boolean) As String
        Dim photoPath As String = ""

        If (ProfileId > 0 AndAlso Not String.IsNullOrEmpty(FileName)) Then
            If (isThumb) Then
                'Dim thumbPath As String = String.Format(gMemberPhotosThumbsDirectory, ProfileId)
                'photoPath = System.Web.VirtualPathUtility.ToAbsolute(thumbPath & "/" & FileName)
                photoPath = String.Format(gMemberPhotosThumbsDirectoryView, ProfileId) & FileName
            Else
                'Dim path As String = String.Format(gMemberPhotosDirectory, ProfileId)
                'photoPath = System.Web.VirtualPathUtility.ToAbsolute(path & "/" & FileName)
                photoPath = String.Format(gMemberPhotosDirectoryView, ProfileId) & FileName
            End If
        Else
            photoPath = ProfileHelper.GetDefaultImageURL(GenderId)
        End If

        Return photoPath
    End Function



    Public Shared Function GetCurrentAge(birthdate As DateTime) As Integer
        Dim dateDiff As TimeSpan = (DateTime.Now - birthdate)
        Return Math.Floor((dateDiff.TotalDays / 365.255)).ToString()
    End Function


    Public Shared Function GetProfileNavigateUrl(profileLoginName As String) As String
        Return System.Web.VirtualPathUtility.ToAbsolute("~/Members/Profile.aspx?p=" & profileLoginName)
    End Function



    'Public Shared Function CalculateDistance(ByVal currentProfile As EUS_Profile, ByVal othersProfile As DataRow) As Integer
    '    Return 111
    'End Function


    'Public Shared Function CalculateDistance(ByVal currentProfile As EUS_Profile, ByVal othersProfile As EUS_Profile) As Integer
    '    Return 111
    'End Function


    'Shared Function CalculateDistance(eUS_Profile As EUS_Profile, othersProfile As DataRowView) As Integer
    '    Return 111
    'End Function





    Private Shared _OfferTypeID_WINK As Integer
    Public Shared ReadOnly Property OfferTypeID_WINK As Integer
        Get
            If (_OfferTypeID_WINK = 0) Then
                _OfferTypeID_WINK = Lists.gDSLists.EUS_OffersTypes.Where(Function(itm) itm.ConstantName = OfferTypeEnum.WINK.ToString()).FirstOrDefault().EUS_OffersTypeID()
            End If
            Return _OfferTypeID_WINK
        End Get
    End Property


    Private Shared _OfferTypeID_OFFERNEW As Integer
    Public Shared ReadOnly Property OfferTypeID_OFFERNEW As Integer
        Get
            If (_OfferTypeID_OFFERNEW = 0) Then
                _OfferTypeID_OFFERNEW = Lists.gDSLists.EUS_OffersTypes.Where(Function(itm) itm.ConstantName = OfferTypeEnum.OFFERNEW.ToString()).FirstOrDefault().EUS_OffersTypeID()
            End If
            Return _OfferTypeID_OFFERNEW
        End Get
    End Property


    Private Shared _OfferTypeID_OFFERCOUNTER As Integer
    Public Shared ReadOnly Property OfferTypeID_OFFERCOUNTER As Integer
        Get
            If (_OfferTypeID_OFFERCOUNTER = 0) Then
                _OfferTypeID_OFFERCOUNTER = Lists.gDSLists.EUS_OffersTypes.Where(Function(itm) itm.ConstantName = OfferTypeEnum.OFFERCOUNTER.ToString()).FirstOrDefault().EUS_OffersTypeID()
            End If
            Return _OfferTypeID_OFFERCOUNTER
        End Get
    End Property


    Private Shared _OfferTypeID_POKE As Integer
    Public Shared ReadOnly Property OfferTypeID_POKE As Integer
        Get
            If (_OfferTypeID_POKE = 0) Then
                _OfferTypeID_POKE = Lists.gDSLists.EUS_OffersTypes.Where(Function(itm) itm.ConstantName = OfferTypeEnum.POKE.ToString()).FirstOrDefault().EUS_OffersTypeID()
            End If
            Return _OfferTypeID_POKE
        End Get
    End Property

    Private Shared _OfferTypeID_NEWDATE As Integer
    Public Shared ReadOnly Property OfferTypeID_NEWDATE As Integer
        Get
            If (_OfferTypeID_NEWDATE = 0) Then
                _OfferTypeID_NEWDATE = Lists.gDSLists.EUS_OffersTypes.Where(Function(itm) itm.ConstantName = OfferTypeEnum.NEWDATE.ToString()).FirstOrDefault().EUS_OffersTypeID()
            End If
            Return _OfferTypeID_NEWDATE
        End Get
    End Property



    Private Shared _OfferStatusID_ACCEPTED As Integer
    Public Shared ReadOnly Property OfferStatusID_ACCEPTED As Integer
        Get
            If (_OfferStatusID_ACCEPTED = 0) Then
                _OfferStatusID_ACCEPTED = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.ACCEPTED.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_ACCEPTED
        End Get
    End Property


    Private Shared _OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE As Integer
    Public Shared ReadOnly Property OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE As Integer
        Get
            If (_OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE = 0) Then
                _OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.OFFER_ACCEEPTED_WITH_MESSAGE.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_OFFER_ACCEEPTED_WITH_MESSAGE
        End Get
    End Property


    Private Shared _OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER As Integer
    Public Shared ReadOnly Property OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER As Integer
        Get
            If (_OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER = 0) Then
                _OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.LIKE_ACCEEPTED_WITH_OFFER.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_LIKE_ACCEEPTED_WITH_OFFER
        End Get
    End Property


    Private Shared _OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE As Integer
    Public Shared ReadOnly Property OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE As Integer
        Get
            If (_OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE = 0) Then
                _OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.LIKE_ACCEEPTED_WITH_MESSAGE.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_LIKE_ACCEEPTED_WITH_MESSAGE
        End Get
    End Property


    Private Shared _OfferStatusID_LIKE_ACCEEPTED_WITH_POKE As Integer
    Public Shared ReadOnly Property OfferStatusID_LIKE_ACCEEPTED_WITH_POKE As Integer
        Get
            If (_OfferStatusID_LIKE_ACCEEPTED_WITH_POKE = 0) Then
                _OfferStatusID_LIKE_ACCEEPTED_WITH_POKE = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.LIKE_ACCEEPTED_WITH_POKE.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_LIKE_ACCEEPTED_WITH_POKE
        End Get
    End Property


    Private Shared _OfferStatusID_POKE_ACCEEPTED_WITH_OFFER As Integer
    Public Shared ReadOnly Property OfferStatusID_POKE_ACCEEPTED_WITH_OFFER As Integer
        Get
            If (_OfferStatusID_POKE_ACCEEPTED_WITH_OFFER = 0) Then
                _OfferStatusID_POKE_ACCEEPTED_WITH_OFFER = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.POKE_ACCEEPTED_WITH_OFFER.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_POKE_ACCEEPTED_WITH_OFFER
        End Get
    End Property


    Private Shared _OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE As Integer
    Public Shared ReadOnly Property OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE As Integer
        Get
            If (_OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE = 0) Then
                _OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.POKE_ACCEEPTED_WITH_MESSAGE.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_POKE_ACCEEPTED_WITH_MESSAGE
        End Get
    End Property


    Private Shared _OffersStatusID_COUNTER As Integer
    Public Shared ReadOnly Property OfferStatusID_COUNTER As Integer
        Get
            If (_OffersStatusID_COUNTER = 0) Then
                _OffersStatusID_COUNTER = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.COUNTER.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OffersStatusID_COUNTER
        End Get
    End Property

    Private Shared _OfferStatusID_PENDING As Integer
    Public Shared ReadOnly Property OfferStatusID_PENDING As Integer
        Get
            If (_OfferStatusID_PENDING = 0) Then
                _OfferStatusID_PENDING = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.PENDING.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_PENDING
        End Get
    End Property


    Private Shared _OfferStatusID_REJECTBAD As Integer
    Public Shared ReadOnly Property OfferStatusID_REJECTBAD As Integer
        Get
            If (_OfferStatusID_REJECTBAD = 0) Then
                _OfferStatusID_REJECTBAD = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REJECTBAD.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REJECTBAD
        End Get
    End Property


    Private Shared _OfferStatusID_REJECTEXPECTATIONS As Integer
    Public Shared ReadOnly Property OfferStatusID_REJECTEXPECTATIONS As Integer
        Get
            If (_OfferStatusID_REJECTEXPECTATIONS = 0) Then
                _OfferStatusID_REJECTEXPECTATIONS = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REJECTEXPECTATIONS.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REJECTEXPECTATIONS
        End Get
    End Property


    Private Shared _OfferStatusID_REJECTED As Integer
    Public Shared ReadOnly Property OfferStatusID_REJECTED As Integer
        Get
            If (_OfferStatusID_REJECTED = 0) Then
                _OfferStatusID_REJECTED = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REJECTED.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REJECTED
        End Get
    End Property

    Private Shared _OfferStatusID_REJECTFAR As Integer
    Public Shared ReadOnly Property OfferStatusID_REJECTFAR As Integer
        Get
            If (_OfferStatusID_REJECTFAR = 0) Then
                _OfferStatusID_REJECTFAR = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REJECTFAR.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REJECTFAR
        End Get
    End Property


    Private Shared _OfferStatusID_REJECTTYPE As Integer
    Public Shared ReadOnly Property OfferStatusID_REJECTTYPE As Integer
        Get
            If (_OfferStatusID_REJECTTYPE = 0) Then
                _OfferStatusID_REJECTTYPE = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REJECTTYPE.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REJECTTYPE
        End Get
    End Property

    Private Shared _OfferStatusID_REPLACINGBYCOUNTER As Integer
    Public Shared ReadOnly Property OfferStatusID_REPLACINGBYCOUNTER As Integer
        Get
            If (_OfferStatusID_REPLACINGBYCOUNTER = 0) Then
                _OfferStatusID_REPLACINGBYCOUNTER = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.REPLACINGBYCOUNTER.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_REPLACINGBYCOUNTER
        End Get
    End Property


    'Private Shared _OfferStatusID_UNLOCKED As Integer
    'Public Shared ReadOnly Property OfferStatusID_UNLOCKED As Integer
    '    Get
    '        If (_OfferStatusID_UNLOCKED = 0) Then
    '            _OfferStatusID_UNLOCKED = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.UNLOCKED.ToString()).FirstOrDefault().EUS_OffersStatusID
    '        End If
    '        Return _OfferStatusID_UNLOCKED
    '    End Get
    'End Property


    Private Shared _OfferStatusID_CANCELED As Integer
    Public Shared ReadOnly Property OfferStatusID_CANCELED As Integer
        Get
            If (_OfferStatusID_CANCELED = 0) Then
                _OfferStatusID_CANCELED = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.CANCELED.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If

            Return _OfferStatusID_CANCELED
        End Get
    End Property


    Private Shared _OfferStatusID_UNLOCKED As Integer
    Public Shared ReadOnly Property OfferStatusID_UNLOCKED As Integer
        Get
            If (_OfferStatusID_UNLOCKED = 0) Then
                _OfferStatusID_UNLOCKED = Lists.gDSLists.EUS_OffersStatus.Where(Function(itm) itm.ConstantName = OfferStatusEnum.UNLOCKED.ToString()).FirstOrDefault().EUS_OffersStatusID
            End If
            Return _OfferStatusID_UNLOCKED
        End Get
    End Property




    Public Shared Function GetHeightString(heightId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = heightId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_Height.Single(Function(itm As DSLists.EUS_LISTS_HeightRow) itm.HeightId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetBodyTypeString(BodyTypeId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var As Object = BodyTypeId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.BodyTypeId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.BodyTypeId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_BodyType.Single(Function(itm As DSLists.EUS_LISTS_BodyTypeRow) itm.BodyTypeId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetHairColorString(HairColorId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var As Object = HairColorId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.HairColorId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.HairColorId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_HairColor.Single(Function(itm As DSLists.EUS_LISTS_HairColorRow) itm.HairColorId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetEyeColorString(EyeColorId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = EyeColorId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_EyeColor.Single(Function(itm As DSLists.EUS_LISTS_EyeColorRow) itm.EyeColorId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_EyeColor.Single(Function(itm As DSLists.EUS_LISTS_EyeColorRow) itm.EyeColorId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_EyeColor.Single(Function(itm As DSLists.EUS_LISTS_EyeColorRow) itm.EyeColorId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetEthnicityString(EthnicityId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = EthnicityId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_Ethnicity.Single(Function(itm As DSLists.EUS_LISTS_EthnicityRow) itm.EthnicityId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_Ethnicity.Single(Function(itm As DSLists.EUS_LISTS_EthnicityRow) itm.EthnicityId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_Ethnicity.Single(Function(itm As DSLists.EUS_LISTS_EthnicityRow) itm.EthnicityId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt

    End Function


    Public Shared Function GetEducationString(EducationId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = EducationId

        Try

            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_Education.Single(Function(itm As DSLists.EUS_LISTS_EducationRow) itm.EducationId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_Education.Single(Function(itm As DSLists.EUS_LISTS_EducationRow) itm.EducationId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_Education.Single(Function(itm As DSLists.EUS_LISTS_EducationRow) itm.EducationId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetChildrenNumberString(ChildrenNumberId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = ChildrenNumberId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_ChildrenNumber.Single(Function(itm As DSLists.EUS_LISTS_ChildrenNumberRow) itm.ChildrenNumberId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_ChildrenNumber.Single(Function(itm As DSLists.EUS_LISTS_ChildrenNumberRow) itm.ChildrenNumberId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_ChildrenNumber.Single(Function(itm As DSLists.EUS_LISTS_ChildrenNumberRow) itm.ChildrenNumberId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetReligionString(ReligionId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = ReligionId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_Religion.Single(Function(itm As DSLists.EUS_LISTS_ReligionRow) itm.ReligionId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_Religion.Single(Function(itm As DSLists.EUS_LISTS_ReligionRow) itm.ReligionId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_Religion.Single(Function(itm As DSLists.EUS_LISTS_ReligionRow) itm.ReligionId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetSmokingString(SmokingId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = SmokingId

        Try

            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_Smoking.Single(Function(itm As DSLists.EUS_LISTS_SmokingRow) itm.SmokingId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_Smoking.Single(Function(itm As DSLists.EUS_LISTS_SmokingRow) itm.SmokingId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_Smoking.Single(Function(itm As DSLists.EUS_LISTS_SmokingRow) itm.SmokingId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetDrinkingString(DrinkingId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = DrinkingId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_Drinking.Single(Function(itm As DSLists.EUS_LISTS_DrinkingRow) itm.DrinkingId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_Drinking.Single(Function(itm As DSLists.EUS_LISTS_DrinkingRow) itm.DrinkingId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_Drinking.Single(Function(itm As DSLists.EUS_LISTS_DrinkingRow) itm.DrinkingId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetIncomeString(IncomeId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = IncomeId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_Income.Single(Function(itm As DSLists.EUS_LISTS_IncomeRow) itm.IncomeId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_Income.Single(Function(itm As DSLists.EUS_LISTS_IncomeRow) itm.IncomeId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_Income.Single(Function(itm As DSLists.EUS_LISTS_IncomeRow) itm.IncomeId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetNetWorthString(NetWorthId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = NetWorthId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_NetWorth.Single(Function(itm As DSLists.EUS_LISTS_NetWorthRow) itm.NetWorthId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_NetWorth.Single(Function(itm As DSLists.EUS_LISTS_NetWorthRow) itm.NetWorthId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_NetWorth.Single(Function(itm As DSLists.EUS_LISTS_NetWorthRow) itm.NetWorthId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try


        Return txt
    End Function


    Public Shared Function GetRelationshipStatusString(RelationshipStatusId As Object, LagID As String) As String
        Dim txt As String = ""
        Dim var = RelationshipStatusId

        Try
            If (TypeOf var Is Integer AndAlso var <> -1) Then
                Select Case LagID
                    Case "GR"
                        txt = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.RelationshipStatusId = var).GR
                    Case "HU"
                        txt = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.RelationshipStatusId = var).HU
                    Case Else '"US"
                        txt = Lists.gDSLists.EUS_LISTS_RelationshipStatus.Single(Function(itm As DSLists.EUS_LISTS_RelationshipStatusRow) itm.RelationshipStatusId = var).US
                End Select
            ElseIf var = -1 Then

            End If
        Catch ex As Exception

        End Try

        Return txt
    End Function


    Public Shared Function GetLookingToMeet_Male_String(LagID As String) As String
        Dim txt As String = ""

        Select Case LagID
            Case "GR"
                txt = ProfileHelper.gMaleGender.GR
            Case "HU"
                txt = ProfileHelper.gMaleGender.HU
            Case Else '"US"
                txt = ProfileHelper.gMaleGender.US
        End Select

        Return txt
    End Function


    Public Shared Function GetLookingToMeet_Female_String(LagID As String) As String
        Dim txt As String = ""

        Select Case LagID
            Case "GR"
                txt = ProfileHelper.gFemaleGender.GR
            Case "HU"
                txt = ProfileHelper.gFemaleGender.HU
            Case Else '"US"
                txt = ProfileHelper.gFemaleGender.US
        End Select

        Return txt
    End Function


    Public Shared Function GetGenderString(genderId As Integer, LagID As String) As String
        Dim txt As String = ""
        If (IsMale(genderId)) Then

            Select Case LagID
                Case "GR"
                    txt = ProfileHelper.gMaleGender.GR
                Case "HU"
                    txt = ProfileHelper.gMaleGender.HU
                Case Else '"US"
                    txt = ProfileHelper.gMaleGender.US
            End Select

        Else

            Select Case LagID
                Case "GR"
                    txt = ProfileHelper.gFemaleGender.GR
                Case "HU"
                    txt = ProfileHelper.gFemaleGender.HU
                Case Else '"US"
                    txt = ProfileHelper.gFemaleGender.US
            End Select

        End If

        Return txt
    End Function


    Public Shared Function GetCountryName(countryCode As String) As String
        Dim txt As String = ""
        If (Not String.IsNullOrEmpty(countryCode) AndAlso countryCode <> "-1") Then
            Try

                Dim row As DSGEO.SYS_CountriesGEORow = Lists.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO.SingleOrDefault(Function(itm As DSGEO.SYS_CountriesGEORow) itm.Iso = countryCode)
                If (row IsNot Nothing) Then
                    txt = row.PrintableName
                End If
            Catch ex As Exception

            End Try
        End If
        Return txt
    End Function



    Public Shared Function GetCountryCode(countryName As String) As String
        Dim txt As String = ""
        If (Not String.IsNullOrEmpty(countryName)) Then
            Try
                Dim row As DSGEO.SYS_CountriesGEORow = Lists.gDSListsGEO__IsEnabled_PrintableNameASC.SYS_CountriesGEO.Single(Function(itm As DSGEO.SYS_CountriesGEORow) itm.Name = countryName.ToUpper())
                If (row IsNot Nothing) Then
                    txt = row.Iso
                End If
            Catch ex As Exception

            End Try
        End If
        Return txt
    End Function


    'Shared Function ZodiacName2(ByVal Birthday As Date) As String
    '    Dim year As Integer = Birthday.Year
    '    Dim zodiacs = {
    '      New With {.From = New Date(year, 1, 1), .[To] = New Date(year, 1, 19), .Zodiac = "Capricorn"},
    '      New With {.From = New Date(year, 1, 20), .[To] = New Date(year, 2, 18), .Zodiac = "Aquarius"},
    '      New With {.From = New Date(year, 2, 19), .[To] = New Date(year, 3, 20), .Zodiac = "Pisces"},
    '      New With {.From = New Date(year, 3, 21), .[To] = New Date(year, 4, 19), .Zodiac = "Aries"},
    '      New With {.From = New Date(year, 4, 20), .[To] = New Date(year, 5, 20), .Zodiac = "Taurus"},
    '      New With {.From = New Date(year, 5, 21), .[To] = New Date(year, 6, 20), .Zodiac = "Gemini"},
    '      New With {.From = New Date(year, 6, 21), .[To] = New Date(year, 7, 22), .Zodiac = "Cancer"},
    '      New With {.From = New Date(year, 7, 23), .[To] = New Date(year, 8, 22), .Zodiac = "Leo"},
    '      New With {.From = New Date(year, 8, 23), .[To] = New Date(year, 9, 22), .Zodiac = "Virgo"},
    '      New With {.From = New Date(year, 9, 23), .[To] = New Date(year, 10, 22), .Zodiac = "Libra"},
    '      New With {.From = New Date(year, 10, 23), .[To] = New Date(year, 11, 21), .Zodiac = "Scorpio"},
    '      New With {.From = New Date(year, 11, 22), .[To] = New Date(year, 12, 21), .Zodiac = "Sagittarius"},
    '      New With {.From = New Date(year, 12, 22), .[To] = New Date(year, 12, 31), .Zodiac = "Capricorn"}}

    '    Dim str As String = (From z In zodiacs Where (z.From <= Birthday And Birthday <= z.[To])).Single.Zodiac
    '    Return str
    'End Function



    Public Shared Function ZodiacName(pBirthDate As Date) As String
        Dim pMonth As Integer
        Dim pDay As Integer
        pMonth = Month(pBirthDate)
        pDay = Day(pBirthDate)

        'Aries - March 21 - April 20
        'Taurus - April 21 - May 21
        'Gemini - May 22 - June 21
        'Cancer - June 22 - July 22
        'Leo - July 23 -August 21
        'Virgo - August 22 - September 23
        'Libra - September 24 - October 23
        'Scorpio - October 24 - November 22
        'Sagittarius - November 23 - December 22
        'Capricorn - December 23 - January 20
        'Aquarius - January 21 - February 19
        'Pisces - February 20- March 20


        If pMonth = 3 Then
            If pDay >= 21 Then
                ZodiacName = "Aries"
            Else
                ZodiacName = "Pisces"
            End If
        ElseIf pMonth = 4 Then
            If pDay >= 21 Then
                ZodiacName = "Taurus"
            Else
                ZodiacName = "Aries"
            End If
        ElseIf pMonth = 5 Then
            If pDay >= 22 Then
                ZodiacName = "Gemini"
            Else
                ZodiacName = "Taurus"
            End If
        ElseIf pMonth = 6 Then
            If pDay >= 22 Then
                ZodiacName = "Cancer"
            Else
                ZodiacName = "Gemini"
            End If
        ElseIf pMonth = 7 Then
            If pDay >= 23 Then
                ZodiacName = "Leo"
            Else
                ZodiacName = "Cancer"
            End If
        ElseIf pMonth = 8 Then
            If pDay >= 23 Then
                ZodiacName = "Virgo"
            Else
                ZodiacName = "Leo"
            End If
        ElseIf pMonth = 9 Then
            If pDay >= 24 Then
                ZodiacName = "Libra"
            Else
                ZodiacName = "Virgo"
            End If
        ElseIf pMonth = 10 Then
            If pDay >= 24 Then
                ZodiacName = "Scorpio"
            Else
                ZodiacName = "Libra"
            End If
        ElseIf pMonth = 11 Then
            If pDay >= 23 Then
                ZodiacName = "Sagittarius"
            Else
                ZodiacName = "Scorpio"
            End If
        ElseIf pMonth = 12 Then
            If pDay >= 23 Then
                ZodiacName = "Capricorn"
            Else
                ZodiacName = "Sagittarius"
            End If
        ElseIf pMonth = 1 Then
            If pDay >= 21 Then
                ZodiacName = "Aquarius"
            Else
                ZodiacName = "Capricorn"
            End If
        Else 'If pMonth = 2 Then
            If pDay >= 20 Then
                ZodiacName = "Pisces"
            Else
                ZodiacName = "Aquarius"
            End If
        End If
    End Function


    'Public Shared Function GetProfilesNextToPostalCode(countryCode As String, zip As Integer, radius As Integer, lagid As String)


    'End Function


    'Shared _Config_UNLOCK_CONVERSATION As Double?
    'Public Shared ReadOnly Property Config_UNLOCK_CONVERSATION_CREDITS As Double
    '    Get
    '        Try
    '            If (_Config_UNLOCK_CONVERSATION Is Nothing) Then _Config_UNLOCK_CONVERSATION = clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_CONVERSATION).CreditsAmount
    '        Catch ex As Exception
    '            _Config_UNLOCK_CONVERSATION = -1
    '        End Try

    '        Return _Config_UNLOCK_CONVERSATION
    '    End Get
    'End Property


    'Shared _Config_UNLOCK_MESSAGE_READ As Double?
    'Public Shared ReadOnly Property Config_UNLOCK_MESSAGE_READ_CREDITS As Double
    '    Get
    '        Try
    '            If (_Config_UNLOCK_MESSAGE_READ Is Nothing) Then _Config_UNLOCK_MESSAGE_READ = clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_MESSAGE_READ).CreditsAmount
    '        Catch ex As Exception
    '            _Config_UNLOCK_MESSAGE_READ = -1
    '        End Try
    '        Return _Config_UNLOCK_MESSAGE_READ
    '    End Get
    'End Property


    'Shared _Config_UNLOCK_MESSAGE_SEND As Double?
    'Public Shared ReadOnly Property Config_UNLOCK_MESSAGE_SEND_CREDITS As Double
    '    Get
    '        Try
    '            If (_Config_UNLOCK_MESSAGE_SEND Is Nothing) Then _Config_UNLOCK_MESSAGE_SEND = clsUserDoes.GetRequiredCredits(UnlockType.UNLOCK_MESSAGE_SEND).CreditsAmount
    '        Catch ex As Exception
    '            _Config_UNLOCK_MESSAGE_SEND = -1
    '        End Try
    '        Return _Config_UNLOCK_MESSAGE_SEND
    '    End Get
    'End Property



    Public Shared Sub ResetInfo()
        '_Config_UNLOCK_MESSAGE_READ = Nothing
        '_Config_UNLOCK_MESSAGE_SEND = Nothing
        '_Config_UNLOCK_CONVERSATION = Nothing
    End Sub


End Class



Public Enum OfferStatusEnum
    None = 0
    PENDING = 1
    COUNTER = 10
    ACCEPTED = 50
    CANCELED = 60
    REPLACINGBYCOUNTER = 80
    REJECTED = 90
    REJECTBAD = 100
    REJECTEXPECTATIONS = 101
    REJECTFAR = 102
    REJECTTYPE = 103
    UNLOCKED = 200

    LIKE_ACCEEPTED_WITH_OFFER = 300
    LIKE_ACCEEPTED_WITH_MESSAGE = 301
    LIKE_ACCEEPTED_WITH_POKE = 302

    POKE_ACCEEPTED_WITH_OFFER = 400
    POKE_ACCEEPTED_WITH_MESSAGE = 401

    OFFER_ACCEEPTED_WITH_MESSAGE = 500
End Enum



Public Enum OfferTypeEnum
    None = 0
    WINK = 1
    OFFERNEW = 10
    OFFERCOUNTER = 11
    POKE = 90
    NEWDATE = 100
End Enum


﻿Public Class clsNullable

    Public Shared Function NullTo(val As Boolean?, Optional defValue As Boolean = False) As Boolean
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function


    Public Shared Function NullTo(val As Integer?, Optional defValue As Integer = 0) As Integer
        If (val Is Nothing) Then Return defValue
        Return val.Value
    End Function

End Class

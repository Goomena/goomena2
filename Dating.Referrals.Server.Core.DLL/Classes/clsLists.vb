﻿

Public Class Lists

    '' Lazy loading
    Private Shared _gDSLists As DSLists
    Public Shared ReadOnly Property gDSLists As DSLists
        Get
            If (_gDSLists Is Nothing) Then _gDSLists = DataHelpers.GetEUS_LISTS()
            Return _gDSLists
        End Get
    End Property


    Private Shared _gDSListsGEO__IsEnabled_PrintableNameASC As DSGEO
    Public Shared ReadOnly Property gDSListsGEO__IsEnabled_PrintableNameASC As DSGEO
        Get
            If (_gDSListsGEO__IsEnabled_PrintableNameASC Is Nothing) Then _gDSListsGEO__IsEnabled_PrintableNameASC = DataHelpers.GetSYS_CountriesGEO__IsEnabled_PrintableNameASC()
            Return _gDSListsGEO__IsEnabled_PrintableNameASC
        End Get
    End Property


    Public Shared Sub gDSListsReset()
        If (_gDSLists IsNot Nothing) Then
            _gDSLists.Dispose()
        End If
        If (_gDSListsGEO__IsEnabled_PrintableNameASC IsNot Nothing) Then
            _gDSListsGEO__IsEnabled_PrintableNameASC.Dispose()
        End If
        _gDSLists = Nothing
        _gDSListsGEO__IsEnabled_PrintableNameASC = Nothing
    End Sub


End Class

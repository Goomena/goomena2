﻿Imports System.Net.Mail
Imports System.Net



Public Class clsMyMail

    Public Class SMTPSettings
        Public gSMTPServerName As String
        Public gSMTPLoginName As String
        Public gSMTPPassword As String
        Public gSMTPOutPort As String
    End Class

    Public Shared MySMTPSettings As SMTPSettings

    Shared Sub New()
        MySMTPSettings = New clsMyMail.SMTPSettings()
    End Sub


    ''' <summary>
    '''  Sends email using system.net/mailSettings configuration of web.config
    ''' </summary>
    ''' <param name="toAddress"></param>
    ''' <param name="subject"></param>
    ''' <param name="content"></param>
    ''' <remarks></remarks>
    Shared Sub SendMail(toAddress As String, subject As String, content As String, Optional IsHtmlBody As Boolean = False)

        '(1) Create the MailMessage instance
        Dim mm As New MailMessage()

        '(2) Assign the MailMessage's properties
        mm.Subject = subject
        mm.Body = content
        mm.IsBodyHtml = IsHtmlBody
        mm.To.Add(toAddress)

        '(3) Create the SmtpClient object
        Dim smtp As New SmtpClient

        '(4) Send the MailMessage (will use the Web.config settings)
        smtp.Send(mm)
    End Sub


    Public Shared Function SendMail2(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String, Optional IsBodyHtml As Boolean = False) As Boolean
        Try

            ' Dim mailMessage As New System.Net.Mail.MailMessage
            'Dim loginInfo As New NetworkCredential("viv.pisces@gmail.com", "vivekraj2484")
            'mailMessage.From = New Mail.MailAddress("viv.pisces@gmail.com")
            'mailMessage.To.Add(New Mail.MailAddress(MailTo))

            'mailMessage.Subject = Subject
            'mailMessage.Body = Body

            Dim loginInfo As New NetworkCredential(MySMTPSettings.gSMTPLoginName, MySMTPSettings.gSMTPPassword)
            Dim client As New Mail.SmtpClient()
            client.DeliveryMethod = Mail.SmtpDeliveryMethod.Network
            client.Host = MySMTPSettings.gSMTPServerName
            client.Port = MySMTPSettings.gSMTPOutPort
            client.EnableSsl = False
            client.Credentials = loginInfo


            Dim mm As New MailMessage()

            mm.From = New System.Net.Mail.MailAddress(strFrom)
            mm.To.Add(strTo)
            mm.Subject = strSubject
            mm.Body = strBody
            mm.IsBodyHtml = IsBodyHtml

            client.Send(mm)

            Return True
        Catch ex As Exception
            Throw
            'gER.ErrorMsgBox(ex, "")
        End Try

        Return False
    End Function


    Shared Sub SendMailFromQueue(toAddress As String, subject As String, content As String, ProfileID As Integer, EmailAction As String, IsHtmlBody As Boolean,
                       SMTPServerCredentialsID As Integer?)

        Dim hdl As New Core.DLL.clsSendingEmailsHandler()
        Try
            hdl.AddEmail(toAddress, subject, content, ProfileID, IsHtmlBody, EmailAction, SMTPServerCredentialsID)
        Catch ex As Exception
            Throw
        Finally
            hdl.Dispose()
        End Try

    End Sub

End Class

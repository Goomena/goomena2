﻿Imports System.Text.RegularExpressions
Imports System.Text


Public Class clsMessagesHelper

    Public Shared Function GetMessagesViewEnum(IsReceived As Boolean, statusID As Integer) As MessagesViewEnum
        Dim MessagesView As MessagesViewEnum = MessagesViewEnum.NEWMESSAGES

        If (IsReceived = True AndAlso statusID = 0) Then
            MessagesView = MessagesViewEnum.NEWMESSAGES

        ElseIf (IsReceived = True AndAlso statusID = 1) Then
            MessagesView = MessagesViewEnum.INBOX

        ElseIf (IsReceived = False) Then
            MessagesView = MessagesViewEnum.SENT

        End If
        Return MessagesView
    End Function


    Shared ReadOnly Property GetINBOXMessagesSQL
        Get
            Dim sql As String = ""
            sql = <sql><![CDATA[

select distinct
	msg.EUS_MessageID,
	msg.Subject,
	msg1.MAXDateTimeToCreate, 
	ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
		where	(con.ToProfileID = prof.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = prof.ProfileId)
	),0) as CommunicationStatus,	 
	
	ISNULL((select COUNT(*) 
		from EUS_Messages  msg1
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1	-- incoming
		and msg1.StatusID=1	-- read
		and msg1.FromProfileID=prof.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	), 0) as ItemsCount,
	
	ItemsCountRead = 0,
	
	ISNULL((select  top(1)  Amount
		from eus_offers  ofr
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = prof.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = prof.ProfileId)
		)
	), 0) as OfferAmount,
    IsReceived = 1,
    StatusID = 1,

	prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, 
    prof.Password, prof.FirstName, prof.LastName, prof.GenderId, prof.Country, prof.Region, 
	prof.City, prof.Zip, prof.CityArea, prof.Address, prof.Telephone, prof.eMail, 
    prof.Cellular, prof.AreYouWillingToTravel, prof.AboutMe_Heading, 
	prof.AboutMe_DescribeYourself, prof.AboutMe_DescribeAnIdealFirstDate, prof.OtherDetails_EducationID, 
    prof.OtherDetails_AnnualIncomeID, prof.OtherDetails_NetWorthID, prof.OtherDetails_Occupation, prof.PersonalInfo_HeightID, 
    prof.PersonalInfo_BodyTypeID, prof.PersonalInfo_EyeColorID, prof.PersonalInfo_HairColorID, prof.PersonalInfo_ChildrenID, 
    prof.PersonalInfo_EthnicityID, prof.PersonalInfo_ReligionID, prof.PersonalInfo_SmokingHabitID, 
	prof.PersonalInfo_DrinkingHabitID, prof.LookingFor_ToMeetMaleID, prof.LookingFor_ToMeetFemaleID, prof.LookingFor_RelationshipStatusID, 
	prof.LookingFor_TypeOfDating_ShortTermRelationship, prof.LookingFor_TypeOfDating_Friendship, prof.LookingFor_TypeOfDating_LongTermRelationship, 
	prof.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, prof.LookingFor_TypeOfDating_MarriedDating, prof.LookingFor_TypeOfDating_AdultDating_Casual, 
	prof.PrivacySettings_HideMeFromSearchResults, prof.PrivacySettings_HideMeFromMembersIHaveBlocked, prof.PrivacySettings_NotShowInOtherUsersFavoritedList, 
	prof.DateTimeToRegister, prof.RegisterIP, prof.RegisterGEOInfos, prof.RegisterUserAgent, prof.LastLoginDateTime, prof.LastLoginIP, prof.LastLoginGEOInfos, 
	prof.LastUpdateProfileDateTime, prof.LastUpdateProfileIP, prof.LastUpdateProfileGEOInfo, prof.LAGID, prof.ThemeName, prof.Referrer, prof.CustomReferrer, 
	prof.Birthday, prof.Role, prof.AccountTypeId, 
		
	phot.CustomerID, 
	phot.FileName 

from EUS_Messages msg
inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		from EUS_Messages  msg1
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1   -- incoming
        and msg1.statusID=1     -- read
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
--left join EUS_CustomerPhotos phot on phot.CustomerId=prof.ProfileId and phot.IsDefault= 1 and phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
where msg.ToProfileID=@CurrentProfileId
    and msg.IsReceived=1 and msg.statusID=1  -- incoming, read
    and ISNULL(msg.IsDeleted, 0)=0
    and ISNULL(msg.IsHidden,0)=0
	AND  not exists(select * 
							from EUS_ProfilesBlocked bl 
							where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
	)
]]></sql>.Value


            Return sql
        End Get
    End Property


    Shared ReadOnly Property GetNEWMessagesSQL
        Get
            Dim sql As String = ""
            sql = <sql><![CDATA[

select distinct
	msg.EUS_MessageID,
	msg.Subject,
	msg1.MAXDateTimeToCreate, 
	ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
		where	(con.ToProfileID = prof.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = prof.ProfileId)
	),0) as CommunicationStatus,	 
	
	ISNULL((select COUNT(*) 
		from EUS_Messages  msg1
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1	-- incoming
		and msg1.StatusID=0	-- read
		and msg1.FromProfileID=prof.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	), 0) as ItemsCount,
	
	ItemsCountRead = 0,
	
	ISNULL((select  top(1)  Amount
		from eus_offers  ofr
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = prof.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = prof.ProfileId)
		)
	), 0) as OfferAmount,
    IsReceived = 0,
    StatusID = 0,

	prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, 
    prof.Password, prof.FirstName, prof.LastName, prof.GenderId, prof.Country, prof.Region, 
	prof.City, prof.Zip, prof.CityArea, prof.Address, prof.Telephone, prof.eMail, 
    prof.Cellular, prof.AreYouWillingToTravel, prof.AboutMe_Heading, 
	prof.AboutMe_DescribeYourself, prof.AboutMe_DescribeAnIdealFirstDate, prof.OtherDetails_EducationID, 
    prof.OtherDetails_AnnualIncomeID, prof.OtherDetails_NetWorthID, prof.OtherDetails_Occupation, prof.PersonalInfo_HeightID, 
    prof.PersonalInfo_BodyTypeID, prof.PersonalInfo_EyeColorID, prof.PersonalInfo_HairColorID, prof.PersonalInfo_ChildrenID, 
    prof.PersonalInfo_EthnicityID, prof.PersonalInfo_ReligionID, prof.PersonalInfo_SmokingHabitID, 
	prof.PersonalInfo_DrinkingHabitID, prof.LookingFor_ToMeetMaleID, prof.LookingFor_ToMeetFemaleID, prof.LookingFor_RelationshipStatusID, 
	prof.LookingFor_TypeOfDating_ShortTermRelationship, prof.LookingFor_TypeOfDating_Friendship, prof.LookingFor_TypeOfDating_LongTermRelationship, 
	prof.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, prof.LookingFor_TypeOfDating_MarriedDating, prof.LookingFor_TypeOfDating_AdultDating_Casual, 
	prof.PrivacySettings_HideMeFromSearchResults, prof.PrivacySettings_HideMeFromMembersIHaveBlocked, prof.PrivacySettings_NotShowInOtherUsersFavoritedList, 
	prof.DateTimeToRegister, prof.RegisterIP, prof.RegisterGEOInfos, prof.RegisterUserAgent, prof.LastLoginDateTime, prof.LastLoginIP, prof.LastLoginGEOInfos, 
	prof.LastUpdateProfileDateTime, prof.LastUpdateProfileIP, prof.LastUpdateProfileGEOInfo, prof.LAGID, prof.ThemeName, prof.Referrer, prof.CustomReferrer, 
	prof.Birthday, prof.Role, prof.AccountTypeId, 
		
	phot.CustomerID, 
	phot.FileName 

from EUS_Messages msg
inner join EUS_Profiles prof on prof.ProfileId = msg.FromProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.FromProfileID
		from EUS_Messages  msg1
		where	msg1.ToProfileID=@CurrentProfileId
		and msg1.IsReceived=1 and msg1.statusID=0	-- incoming, unread
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId = msg1.FromProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
--left join EUS_CustomerPhotos phot on phot.CustomerId=prof.ProfileId and phot.IsDefault= 1 and phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
where msg.ToProfileID=@CurrentProfileId
    and msg.IsReceived=1 and msg.statusID=0	-- incoming, unread
    and ISNULL(msg.IsHidden,0)=0
	AND  not exists(select * 
							from EUS_ProfilesBlocked bl 
							where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
	)
]]></sql>.Value


            Return sql
        End Get
    End Property


    Shared ReadOnly Property GetSENTMessagesSQL
        Get
            Dim sql As String = ""
            sql = <sql><![CDATA[

select distinct
	msg.EUS_MessageID,
	msg.Subject,
	msg1.MAXDateTimeToCreate, 
	ISNULL((
		select top(1) UnlockedConversationId
		from EUS_UnlockedConversations  con
		where	(con.ToProfileID = prof.ProfileId and con.FromProfileID =@CurrentProfileId) or
				(con.ToProfileID =@CurrentProfileId and con.FromProfileID = prof.ProfileId)
	),0) as CommunicationStatus,	 
	
	(select COUNT(*) 
		from EUS_Messages  msg1
		where	msg1.FromProfileID=@CurrentProfileId
		and msg1.IsReceived=0	-- outgoing
		and msg1.ToProfileID=prof.ProfileId
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.FromProfileID 
	) as ItemsCount,
	
    ItemsCountRead = null,

	(select  top(1)  Amount
		from eus_offers  ofr
		where ofr.StatusID=50 and (
			(ofr.ToProfileID = prof.ProfileId and ofr.FromProfileID =@CurrentProfileId) or
			(ofr.ToProfileID =@CurrentProfileId and ofr.FromProfileID = prof.ProfileId)
		)
	) as OfferAmount,
    IsReceived = 0,
    StatusID = 1,

	prof.ProfileID, prof.IsMaster, prof.MirrorProfileID, prof.Status, prof.LoginName, prof.Password, prof.FirstName, prof.LastName, prof.GenderId, prof.Country, prof.Region, 
	prof.City, prof.Zip, prof.CityArea, prof.Address, prof.Telephone, prof.eMail, prof.Cellular, prof.AreYouWillingToTravel, prof.AboutMe_Heading, 
	prof.AboutMe_DescribeYourself, prof.AboutMe_DescribeAnIdealFirstDate, prof.OtherDetails_EducationID, prof.OtherDetails_AnnualIncomeID, 
	prof.OtherDetails_NetWorthID, prof.OtherDetails_Occupation, prof.PersonalInfo_HeightID, prof.PersonalInfo_BodyTypeID, prof.PersonalInfo_EyeColorID, 
	prof.PersonalInfo_HairColorID, prof.PersonalInfo_ChildrenID, prof.PersonalInfo_EthnicityID, prof.PersonalInfo_ReligionID, prof.PersonalInfo_SmokingHabitID, 
	prof.PersonalInfo_DrinkingHabitID, prof.LookingFor_ToMeetMaleID, prof.LookingFor_ToMeetFemaleID, prof.LookingFor_RelationshipStatusID, 
	prof.LookingFor_TypeOfDating_ShortTermRelationship, prof.LookingFor_TypeOfDating_Friendship, prof.LookingFor_TypeOfDating_LongTermRelationship, 
	prof.LookingFor_TypeOfDating_MutuallyBeneficialArrangements, prof.LookingFor_TypeOfDating_MarriedDating, prof.LookingFor_TypeOfDating_AdultDating_Casual, 
	prof.PrivacySettings_HideMeFromSearchResults, prof.PrivacySettings_HideMeFromMembersIHaveBlocked, prof.PrivacySettings_NotShowInOtherUsersFavoritedList, 
	prof.DateTimeToRegister, prof.RegisterIP, prof.RegisterGEOInfos, prof.RegisterUserAgent, prof.LastLoginDateTime, prof.LastLoginIP, prof.LastLoginGEOInfos, 
	prof.LastUpdateProfileDateTime, prof.LastUpdateProfileIP, prof.LastUpdateProfileGEOInfo, prof.LAGID, prof.ThemeName, prof.Referrer, prof.CustomReferrer, 
	prof.Birthday, prof.Role, prof.AccountTypeId, 
		
	phot.CustomerID, 
	phot.FileName 

from EUS_Messages msg
inner join EUS_Profiles prof on prof.ProfileId = msg.ToProfileID
inner join (
	select MAX(msg1.DateTimeToCreate) as MAXDateTimeToCreate,msg1.ToProfileID
		from EUS_Messages  msg1
		where	msg1.FromProfileID=@CurrentProfileId
		and msg1.IsReceived=0	-- outgoing
        and ISNULL(msg1.IsHidden,0)=0
		group by msg1.ToProfileID
	)  as msg1 on msg1.MAXDateTimeToCreate= msg.DateTimeToCreate and prof.ProfileId=msg1.ToProfileID
LEFT OUTER JOIN dbo.EUS_CustomerPhotos AS phot ON phot.CustomerID = prof.ProfileID AND phot.CustomerPhotosID = prof.DefPhotoID
--left join EUS_CustomerPhotos phot on phot.CustomerId=prof.ProfileId and phot.IsDefault= 1 and phot.HasAproved = 1 and ISNULL(phot.IsDeleted,0) = 0
where msg.FromProfileID=@CurrentProfileId
    and msg.IsReceived=0
    and ISNULL(msg.IsHidden,0)=0
	AND  not exists(select * 
							from EUS_ProfilesBlocked bl 
							where (bl.FromProfileID =prof.ProfileID and bl.ToProfileID = @CurrentProfileId) or
								(bl.FromProfileID =@CurrentProfileId and bl.ToProfileID = prof.ProfileID)
	)
]]></sql>.Value


            Return sql
        End Get
    End Property

    Public Shared Function GetMessagesDataTable(CurrentProfileId As Integer, MessagesView As MessagesViewEnum, Optional cmdFilter As String = Nothing) As DataTable



        Dim sql As String = ""
        Dim dt As New DataTable()

        'This script is based on kilometer. If you use miles simply convert the radius to kilometer (radius=radius*1.609344)
        Try

            If (MessagesViewEnum.INBOX = MessagesView) Then
                ' incoming messages


                sql = GetINBOXMessagesSQL & _
<sql><![CDATA[
and ISNULL(msg.IsDeleted, 0)=0
{0}
order by MAXDateTimeToCreate desc
]]></sql>.Value

            ElseIf (MessagesViewEnum.SENT = MessagesView) Then
                ' sent messages

                sql = GetSENTMessagesSQL & _
<sql><![CDATA[
and ISNULL(msg.IsDeleted, 0)=0
{0}
order by MAXDateTimeToCreate desc

]]></sql>.Value
            ElseIf (MessagesViewEnum.TRASH = MessagesView) Then
                ' trash messages -- start


                sql = <sql><![CDATA[
(

-- sent messages
]]></sql>.Value & _
         GetSENTMessagesSQL & _
<sql><![CDATA[
and ISNULL(msg.IsDeleted, 0)=1
{0}

)
UNION (

 -- received and read messages

]]></sql>.Value & _
        GetINBOXMessagesSQL & _
<sql><![CDATA[
and ISNULL(msg.IsDeleted, 0)=1
{0}

)
UNION (

 -- new messages

]]></sql>.Value & _
        GetNEWMessagesSQL & _
<sql><![CDATA[

and ISNULL(msg.IsDeleted, 0)=1
{0}

)
order by MAXDateTimeToCreate desc
]]></sql>.Value

                ' trash messages -- end


            Else
                ' new messages

                sql = GetNEWMessagesSQL & _
<sql><![CDATA[
and ISNULL(msg.IsDeleted, 0)=0
{0}
order by MAXDateTimeToCreate desc
]]></sql>.Value
            End If


            If (cmdFilter = "UNREAD") Then
                sql = String.Format(sql, "and StatusID = 0")
            ElseIf (cmdFilter = "READ") Then
                sql = String.Format(sql, "and StatusID = 1")
            Else
                sql = String.Format(sql, "")
            End If


            sql = sql.Replace("@CurrentProfileId", CurrentProfileId)

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            dt = DataHelpers.GetDataTable(command)

        Catch ex As Exception
            Throw New System.Exception(ex.Message, ex)
        End Try

        Return dt
    End Function



    Public Shared Function DeleteMessagesFromMemberInView(OwnerProfileID As Integer, FromProfileID As Integer, ToProfileID As Integer, MessagesView As MessagesViewEnum, OnDelete_MakeHidden As Boolean) As Integer

        Dim sql As String = ""

        Try

            If (MessagesViewEnum.INBOX = MessagesView) Then
                ' incoming messages

                sql = <sql><![CDATA[
UPDATE EUS_Messages
SET IsDeleted = 1
WHERE EUS_MessageID IN (
    select 
	    msg.EUS_MessageID
    from EUS_Messages msg
    where msg.ToProfileID=@ToProfileID
    and msg.FromProfileID=@FromProfileID
    and msg.ProfileIDOwner=@OwnerProfileID
    and msg.IsReceived=1 and statusID=1
    and ISNULL(msg.IsDeleted, 0)=0
    and ISNULL(msg.IsHidden,0)=0
)
]]></sql>.Value
            ElseIf (MessagesViewEnum.SENT = MessagesView) Then
                ' sent messages

                sql = <sql><![CDATA[
UPDATE EUS_Messages
SET IsDeleted = 1
WHERE EUS_MessageID IN (
    select 
	    msg.EUS_MessageID
    from EUS_Messages msg
    where msg.ToProfileID=@ToProfileID
    and msg.FromProfileID=@FromProfileID
    and msg.ProfileIDOwner=@OwnerProfileID
    and msg.IsReceived=0
    and ISNULL(msg.IsDeleted, 0)=0
    and ISNULL(msg.IsHidden,0)=0
)
]]></sql>.Value
            ElseIf (MessagesViewEnum.TRASH = MessagesView) Then
                ' trash messages

                If (OnDelete_MakeHidden) Then
                    sql = <sql><![CDATA[
UPDATE EUS_Messages
SET IsHidden = 1
WHERE EUS_MessageID IN (
    select 
	    msg.EUS_MessageID
    from EUS_Messages msg
    where (
        (msg.ToProfileID=@ToProfileID and msg.FromProfileID=@FromProfileID) OR 
        (msg.ToProfileID=@FromProfileID and msg.FromProfileID=@ToProfileID)
    )
    and msg.ProfileIDOwner=@OwnerProfileID
    and msg.IsDeleted=1
    and ISNULL(msg.IsHidden,0)=0
)
]]></sql>.Value
                Else
                    sql = <sql><![CDATA[
DELETE FROM EUS_Messages
WHERE EUS_MessageID IN (
    select 
	    msg.EUS_MessageID
    from EUS_Messages msg
    where (
        (msg.ToProfileID=@ToProfileID and msg.FromProfileID=@FromProfileID) OR 
        (msg.ToProfileID=@FromProfileID and msg.FromProfileID=@ToProfileID)
    )
    and msg.ProfileIDOwner=@OwnerProfileID
    and msg.IsDeleted=1
    and ISNULL(msg.IsHidden,0)=0
)
]]></sql>.Value
                End If

            Else
                ' new messages

                sql = <sql><![CDATA[
UPDATE EUS_Messages
SET IsDeleted = 1
WHERE EUS_MessageID IN (
    select 
	    msg.EUS_MessageID
    from EUS_Messages msg
    where msg.ToProfileID=@ToProfileID
    and msg.FromProfileID=@FromProfileID
    and msg.ProfileIDOwner=@OwnerProfileID
    and msg.IsReceived=1 and statusID=0
    and ISNULL(msg.IsDeleted, 0)=0
    and ISNULL(msg.IsHidden,0)=0
)
]]></sql>.Value
            End If


            sql = sql.Replace("@FromProfileID", FromProfileID)
            sql = sql.Replace("@ToProfileID", ToProfileID)
            sql = sql.Replace("@OwnerProfileID", OwnerProfileID)

            Dim command As SqlClient.SqlCommand = DataHelpers.GetSqlCommand(sql)
            Dim result = DataHelpers.ExecuteNonQuery(command)
            Return result

        Catch ex As Exception

        End Try

        Return -1
    End Function

End Class

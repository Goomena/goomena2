﻿Imports Dating.Referrals.Server.Core.DLL
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Linq


Public Module DataHelpers

    Public Const WAIT_DB_RESTART_MILLISEC As Integer = 15000

    Private _ConnectionString As String
    Friend Property ConnectionString As String
        Get
            Return _ConnectionString
        End Get
        Set(value As String)
            _ConnectionString = value
        End Set
    End Property

    Public Sub SetConnectionString(connString)
        _ConnectionString = connString
    End Sub

#Region "EUS_Profiles"

    'Public Function GetEUS_Profiles() As DSMembers
    '    Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
    '    Dim ds As New DSMembers()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.EUS_Profiles)

    '    Return ds
    'End Function


    'Public Function GetEUS_Profiles_ByProfileID(profileId As Integer) As DSMembers
    '    Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
    '    Dim ds As New DSMembers()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.FillByProfileID(ds.EUS_Profiles, profileId)

    '    Return ds
    'End Function


    'Public Function GetEUS_Profiles_ByProfileIDRow(profileId As Integer) As DSMembers.EUS_ProfilesRow
    '    Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
    '    Dim ds As New DSMembers()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.FillByProfileID(ds.EUS_Profiles, profileId)

    '    Dim row As DSMembers.EUS_ProfilesRow = ds.EUS_Profiles.Rows(0)
    '    Return row
    'End Function


    Public Function GetEUS_Profiles_ForReferrerBalance(profileId As Integer?, dateFrom As DateTime?, dateTo As DateTime?) As DSReferrers
        Dim ta As New DSReferrersTableAdapters.GetReferrersBalance_ADMINTableAdapter
        Dim ds As New DSReferrers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.GetReferrersBalance_ADMIN, profileId, dateFrom, dateTo)

        Return ds
    End Function


    'Public Function GetEUS_Profiles_ByProfileOrMirrorID(profileId As Integer) As DSMembers
    '    Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
    '    Dim ds As New DSMembers()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.FillByProfileOrMirrorID(ds.EUS_Profiles, profileId)

    '    Return ds
    'End Function


    Public Function GetREF_ReferrerProfiles_ByLoginName(loginName As String) As DSReferrers
        Dim ta As New DSReferrersTableAdapters.REF_ReferrerProfilesTableAdapter
        Dim ds As New DSReferrers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_LoginName(ds.REF_ReferrerProfiles, loginName)

        Return ds
    End Function


    'Public Function GetEUS_Profiles_ByStatus(profileStatus As Integer) As DSMembers
    '    Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
    '    Dim ds As New DSMembers()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.FillByStatus(ds.EUS_Profiles, profileStatus)

    '    Return ds
    'End Function


    Public Function GetREF_ReferrerProfiles_ByLoginPass(ByVal LoginName As String, ByVal Password As String) As DSReferrers
        Dim ta As New DSReferrersTableAdapters.REF_ReferrerProfilesTableAdapter
        Dim ds As New DSReferrers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_LoginPass(ds.REF_ReferrerProfiles, LoginName, Password)

        Return ds
    End Function


    Public Function GetREF_ReferrerProfiles_ByLoginNameOrEmail(ByVal LoginName As String) As DSReferrers
        Dim ta As New DSReferrersTableAdapters.REF_ReferrerProfilesTableAdapter
        Dim ds As New DSReferrers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_LoginNameOrEmail(ds.REF_ReferrerProfiles, LoginName)

        Return ds
    End Function


    Public Function GetREF_ReferrerProfiles_ByCustomerID(ByVal CustomerID As Integer) As DSReferrers
        Dim ta As New DSReferrersTableAdapters.REF_ReferrerProfilesTableAdapter
        Dim ds As New DSReferrers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_CustomerID(ds.REF_ReferrerProfiles, CustomerID)

        Return ds
    End Function


    'Public Function GetProfileStatistics(CustomerId As Integer, ByVal StartDaysBefore As Integer) As DSMembersViews
    '    Dim ds As New DSMembersViews()

    '    Dim ta As New DSMembersViewsTableAdapters.GetProfileStatisticsTableAdapter
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.GetProfileStatistics, CustomerId, StartDaysBefore)

    '    Return ds
    'End Function


    '' NEW - using messages
    'Public Function GetReferrersCreditsSum_Admin2(ReferrerID As Integer?, dateFrom As DateTime?, dateTo As DateTime?) As DSReferrers
    '    Dim ds As New DSReferrers()

    '    Dim ta As New DSReferrersTableAdapters.GetReferrersCreditsSum_Admin2TableAdapter
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.GetReferrersCreditsSum_Admin2, ReferrerID, dateFrom, dateTo)

    '    Return ds
    'End Function


    'Public Function GetReferrersCreditsSum_Admin3(ReferrerID As Integer, SelectReferrerTree As Boolean, dateFrom As DateTime?, dateTo As DateTime?) As DSReferrers
    '    Dim ds As New DSReferrers()

    '    Dim ta As New DSReferrersTableAdapters.GetReferrersCreditsSum_Admin3TableAdapter
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.GetReferrersCreditsSum_Admin3, ReferrerID, SelectReferrerTree, dateFrom, dateTo)

    '    Return ds
    'End Function


    Public Function GetReferrerCommissions_Admin() As DSReferrers
        Dim ds As New DSReferrers()

        Dim ta As New DSReferrersTableAdapters.GetReferrerCommissions_AdminTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.GetReferrerCommissions_Admin)

        Return ds
    End Function


    Public Function GetReferrersTransactions(ByVal DateFrom As Date?, ByVal DateTo As Date?, ByVal AffiliateID As Integer?) As DSReferrers
        Dim ds As New DSReferrers()

        Dim ta As New DSReferrersTableAdapters.GetReferrersTransactionsTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.GetReferrersTransactions, AffiliateID, DateFrom, DateTo, True, False)

        Return ds
    End Function


    Public Function GetReferrerCreditsHistory(ByVal DateFrom As Date?, ByVal DateTo As Date?, ByVal AffiliateID As Integer?, ByVal LevelParam As Integer?) As DSReferrers
        Dim ds As New DSReferrers()

        Dim ta As New DSReferrersTableAdapters.GetReferrerCreditsHistory2TableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.GetReferrerCreditsHistory2, AffiliateID, DateFrom, DateTo, LevelParam)

        Return ds
    End Function



    Public Sub UpdateREF_ReferrerProfiles(ByRef ds As DSReferrers)
        Dim ta As New DSReferrersTableAdapters.REF_ReferrerProfilesTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Update(ds.REF_ReferrerProfiles)
    End Sub


    'Public Sub UpdateEUS_Profiles(ByRef ds As DSMembers)
    '    Dim ta As New DSMembersTableAdapters.EUS_ProfilesTableAdapter
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Update(ds.EUS_Profiles)
    'End Sub



    'Public Function GetEUS_ProfileMasterByLoginName(ctx As CMSDBDataContext, loginNameOrEmail As String, Optional status As ProfileStatusEnum = ProfileStatusEnum.None) As EUS_Profile
    '    Dim prof As EUS_Profile = Nothing
    '    'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        If (status > ProfileStatusEnum.None) Then
    '            prof = (From itm In ctx.EUS_Profiles
    '                    Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) _
    '                    AndAlso itm.IsMaster = True _
    '                    AndAlso itm.Status = status
    '                    Select itm).SingleOrDefault()

    '        Else
    '            prof = (From itm In ctx.EUS_Profiles
    '                    Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) _
    '                    AndAlso itm.IsMaster = True _
    '                    Select itm).SingleOrDefault()

    '        End If

    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        '_CMSDBDataContext.Dispose()
    '    End Try

    '    Return prof

    'End Function



    'Public Function GetEUS_ProfileByLoginNameOrEmail(ctx As CMSDBDataContext, loginNameOrEmail As String) As EUS_Profile
    '    Dim prof As EUS_Profile = Nothing
    '    'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        prof = (From itm In ctx.EUS_Profiles
    '                    Where (itm.LoginName.ToUpper() = loginNameOrEmail.ToUpper() OrElse itm.eMail.ToUpper() = loginNameOrEmail.ToUpper()) _
    '                    Select itm).FirstOrDefault()


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        '_CMSDBDataContext.Dispose()
    '    End Try

    '    Return prof

    'End Function

    'Public Function GetEUS_ProfileByProfileID(profileID As Integer) As EUS_Profile
    '    Dim ctx As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Dim prof As EUS_Profile = Nothing
    '    Try

    '        prof = GetEUS_ProfileByProfileID(ctx, profileID)


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        ctx.Dispose()
    '    End Try
    '    Return prof
    'End Function

    'Public Function GetEUS_ProfileByProfileID(ctx As CMSDBDataContext, profileID As Integer) As EUS_Profile
    '    Dim prof As EUS_Profile = Nothing
    '    Try

    '        prof = (From itm In ctx.EUS_Profiles
    '                    Where (itm.ProfileID = profileID) _
    '                    Select itm).SingleOrDefault()


    '    Catch ex As Exception
    '        Throw
    '    Finally

    '    End Try

    '    Return prof
    'End Function


    'Public Function GetEUS_Profile_LoginName_ByProfileID(ctx As CMSDBDataContext, profileID As String) As String
    '    Dim LogName As String
    '    Try

    '        LogName = (From itm In ctx.EUS_Profiles
    '                    Where (itm.ProfileID = profileID) _
    '                    Select itm.LoginName).SingleOrDefault()

    '    Catch ex As Exception
    '        Throw
    '    Finally

    '    End Try

    '    Return LogName
    'End Function


    'Public Function EUS_Profile_GetIsReferrer(ctx As CMSDBDataContext, profileID As String) As Boolean
    '    Dim prof As Integer = 0
    '    Try

    '        prof = (From itm In ctx.EUS_Profiles
    '                    Where (itm.ProfileID = profileID AndAlso itm.ReferrerParentId > 0) _
    '                    Select itm).Count()


    '    Catch ex As Exception
    '        Throw
    '    Finally

    '    End Try

    '    Return prof > 0
    'End Function

    Public Sub UpdateREF_ReferrerProfiles_LoginData(CustomerID As Integer, REMOTE_ADDR As String, GEO_COUNTRY_CODE As String)

        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update REF_ReferrerProfiles set")
            sb.AppendLine("LastLoginDateTime=@LastLoginDateTime, LastLoginIP=@LastLoginIP, LastLoginGEOInfos=@LastLoginGEOInfos")
            sb.AppendLine("where CustomerID=" & CustomerID)
            If (sb.Length > 0) Then

                Dim command As SqlClient.SqlCommand = GetSqlCommand(sb.ToString())
                command.Parameters.AddWithValue("@LastLoginDateTime", DateTime.UtcNow)
                command.Parameters.AddWithValue("@LastLoginIP", REMOTE_ADDR)
                If (String.IsNullOrEmpty(GEO_COUNTRY_CODE)) Then
                    command.Parameters.AddWithValue("@LastLoginGEOInfos", "")
                Else
                    command.Parameters.AddWithValue("@LastLoginGEOInfos", GEO_COUNTRY_CODE)
                End If

                DataHelpers.ExecuteNonQuery(command)
            End If

        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub


    Public Sub UpdateEUS_Profiles_FacebookData(profileid As Integer, fbUid As String, fbName As String, fbUserName As String)
        Try
            Dim sql As String = "update EUS_Profiles set FacebookUserId=@FacebookUserId, FacebookName=@FacebookName, FacebookUserName=@FacebookUserName where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & ""
            Dim command As SqlClient.SqlCommand = GetSqlCommand(sql)
            If (fbUid IsNot Nothing) Then
                command.Parameters.AddWithValue("@FacebookUserId", fbUid)
            Else
                command.Parameters.AddWithValue("@FacebookUserId", System.DBNull.Value)
            End If
            If (fbName IsNot Nothing) Then
                command.Parameters.AddWithValue("@FacebookName", fbName)
            Else
                command.Parameters.AddWithValue("@FacebookName", System.DBNull.Value)
            End If
            If (fbUserName IsNot Nothing) Then
                command.Parameters.AddWithValue("@FacebookUserName", fbUserName)
            Else
                command.Parameters.AddWithValue("@FacebookUserName", System.DBNull.Value)
            End If

            DataHelpers.ExecuteNonQuery(command)

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub

    Public Sub UpdateEUS_Profiles_FacebookPhoto(profileid As Integer, FacebookPhoto As Boolean)
        Try
            Dim sb As String = "update EUS_Profiles set FacebookPhoto=@FacebookPhoto where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & ""
            Dim command As SqlClient.SqlCommand = GetSqlCommand(sb.ToString())
            command.Parameters.AddWithValue("@FacebookPhoto", FacebookPhoto)

            DataHelpers.ExecuteNonQuery(command)

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub



    Public Sub UpdateEUS_Profiles_LogoutData(profileid As Integer)
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set IsOnline=0 where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                DataHelpers.ExecuteNonQuery(sb.ToString())
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Sub Update_EUS_Profiles_UpdateAvailableCredits(profileid As Integer, AvailableCredits As Integer, Optional ReadAvailableCreditsFromDB As Boolean = False)
        Try
            Dim sql As String = "EXEC EUS_Profiles_UpdateAvailableCredits @customerid=@customerid, @AvailableCredits=@AvailableCredits, @ReadAvailableCredits=@ReadAvailableCredits"
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@customerid", profileid)
            cmd.Parameters.AddWithValue("@AvailableCredits", AvailableCredits)
            cmd.Parameters.AddWithValue("@ReadAvailableCredits", ReadAvailableCreditsFromDB)
            DataHelpers.ExecuteNonQuery(cmd)
        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Sub UpdateEUS_Profiles_AffiliateParentId(profileid As Integer, AffiliateParentId As Integer)
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set AffiliateParentId=" & AffiliateParentId & " where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                DataHelpers.ExecuteNonQuery(sb.ToString())
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    'Public Function GetEUS_Profiles_ReferrerParentId(profileid As Integer) As Integer
    '    Dim _ReferrerParentId As Integer?
    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        _ReferrerParentId = (From itm In _CMSDBDataContext.EUS_Profiles
    '                                Where itm.ProfileID = profileid
    '                                Select itm.ReferrerParentId).SingleOrDefault()


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try
    '    If (Not _ReferrerParentId.HasValue) Then _ReferrerParentId = 0

    '    Return _ReferrerParentId
    'End Function


    Public Sub UpdateEUS_Profiles_ReferrerParentId(profileid As Integer, ReferrerParentId As Integer)
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set ReferrerParentId=" & ReferrerParentId & " where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                DataHelpers.ExecuteNonQuery(sb.ToString())
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Sub UpdateEUS_Profiles_LAGID(profileid As Integer, lagId As String)
        Try
            ClsSQLInjectionClear.ClearString(lagId, 3)

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set LAGID='" & lagId & "' where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                DataHelpers.ExecuteNonQuery(sb.ToString())
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    Public Sub UpdateEUS_Profiles_REF_CRD2EURO_Rate(profileid As Integer, REF_CRD2EURO_Rate As Double)
        Try

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set REF_CRD2EURO_Rate=@REF_CRD2EURO_Rate where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sb.ToString())
                cmd.Parameters.Add(New SqlParameter("REF_CRD2EURO_Rate", REF_CRD2EURO_Rate))
                DataHelpers.ExecuteNonQuery(cmd)
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub

    Public Sub UpdateEUS_Profiles_ShowOnFirstPage(profileid As Integer, show As Boolean)
        Try

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set ShowOnFrontPage=@ShowOnFrontPage where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sb.ToString())
                cmd.Parameters.Add(New SqlParameter("ShowOnFrontPage", show))
                DataHelpers.ExecuteNonQuery(cmd)
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub


    'Public Function GetEUS_Profiles_REF_CRD2EURO_Rate(profileid As Integer) As Double
    '    Dim _REF_CRD2EURO_Rate As Double?
    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        _REF_CRD2EURO_Rate = (From itm In _CMSDBDataContext.EUS_Profiles
    '                                Where itm.ProfileID = profileid
    '                                Select itm.REF_CRD2EURO_Rate).SingleOrDefault()


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try

    '    If (Not _REF_CRD2EURO_Rate.HasValue) Then
    '        Dim config As New clsConfigValues()
    '        _REF_CRD2EURO_Rate = config.referrer_commission_conv_rate
    '    End If

    '    Return _REF_CRD2EURO_Rate.Value
    'End Function


    Public Sub UpdateEUS_Profiles_Activity(profileid As Integer, LastActivityIP As String, isOnline As Boolean?)
        Try
            Dim sql As String = <sql><![CDATA[
update EUS_Profiles set 
    LastActivityDateTime=@LastActivityDateTime, 
    IsOnline=isnull(@IsOnline,IsOnline) , 
    LastActivityIP=isnull(@LastActivityIP,LastActivityIP) 
where ProfileID=@CustomerId or MirrorProfileID=@CustomerId
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.Add(New SqlParameter("@CustomerId", profileid))
            cmd.Parameters.Add(New SqlParameter("@LastActivityDateTime", Date.UtcNow))
            If (isOnline.HasValue) Then
                cmd.Parameters.Add(New SqlParameter("@IsOnline", isOnline))
            Else
                cmd.Parameters.Add(New SqlParameter("@IsOnline", System.DBNull.Value))
            End If
            If (Not String.IsNullOrEmpty(LastActivityIP)) Then
                cmd.Parameters.Add(New SqlParameter("@LastActivityIP", LastActivityIP))
            Else
                cmd.Parameters.Add(New SqlParameter("@LastActivityIP", System.DBNull.Value))
            End If
            DataHelpers.ExecuteNonQuery(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
    End Sub
    'Public Sub UpdateEUS_Profiles_Activity(profileid As Integer)
    '    Try
    '        Dim sb As New System.Text.StringBuilder()
    '        sb.AppendLine("update EUS_Profiles set LastActivityDateTime='" & Date.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") & "' where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
    '        If (sb.Length > 0) Then
    '            DataHelpers.ExecuteNonQuery(sb.ToString())
    '        End If

    '    Catch ex As Exception
    '        Throw
    '    Finally

    '    End Try
    'End Sub


    'Public Function GetEUS_Profiles_Status(profileid As Integer) As Integer
    '    Dim Status As Integer

    '    Try
    '        Dim dt As New DataTable()
    '        Dim sb As New System.Text.StringBuilder()
    '        sb.AppendLine("select Status from EUS_Profiles where ProfileID=" & profileid)
    '        If (sb.Length > 0) Then
    '            Dim o = DataHelpers.ExecuteScalar(sb.ToString())
    '            If (o IsNot Nothing AndAlso Not o Is System.DBNull.Value) Then Status = o

    '        End If
    '    Catch ex As Exception
    '        Throw
    '    Finally

    '    End Try

    '    Return Status
    'End Function



    Public Sub UpdateEUS_Profiles_OnUpdateAutoNotificationSent(profileid As Integer, value As Boolean)
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set OnUpdateAutoNotificationSent='" & value & "' where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                DataHelpers.ExecuteNonQuery(sb.ToString())
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub


    Public Function GetEUS_Profiles_OnUpdateAutoNotificationSent(profileid As Integer) As Boolean
        Dim value As Boolean
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("select top(1) ISNULL(OnUpdateAutoNotificationSent,0) as OnUpdateAutoNotificationSent from EUS_Profiles where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                value = DataHelpers.ExecuteScalar(sb.ToString())
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try
        Return value
    End Function



    Public Sub UpdateEUS_Profiles_OnUpdateAutoNotificationSentForPhoto(profileid As Integer, value As Boolean)
        Try
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("update EUS_Profiles set OnUpdateAutoNotificationSentForPhoto='" & value & "' where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & "")
            If (sb.Length > 0) Then
                DataHelpers.ExecuteNonQuery(sb.ToString())
            End If
        Catch ex As Exception
            Throw
        Finally
        End Try
    End Sub

    Public Function UpdateREF_ReferrerProfiles_SetStatusUpdating(CustomerID As Integer, Status As ProfileStatusEnum) As Integer
        Dim ROWCOUNT As Integer
        Try
            Dim sql As String = <sql><![CDATA[
update REF_ReferrerProfiles 
    set Status=@Status 
where 
    (CustomerID=@CustomerID)
    and Status=@Approved
]]></sql>
            Dim cmd As SqlClient.SqlCommand = GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@Status", Status)
            cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
            cmd.Parameters.AddWithValue("@Approved", CInt(ProfileStatusEnum.Approved))
            ROWCOUNT = DataHelpers.ExecuteNonQuery(cmd)
        Catch ex As Exception
            Throw
        Finally
        End Try
        Return ROWCOUNT
    End Function

    Public Function GetEUS_Profiles_OnUpdateAutoNotificationSentForPhoto(profileid As Integer) As Boolean
        Dim value As Boolean
        Try
            Dim sql As String = "select top(1) ISNULL(OnUpdateAutoNotificationSentForPhoto,0) as OnUpdateAutoNotificationSentForPhoto from EUS_Profiles where ProfileID=" & profileid & " or MirrorProfileID=" & profileid & ""
            value = DataHelpers.ExecuteScalar(sql)
        Catch ex As Exception
            Throw
        Finally
        End Try
        Return value
    End Function

#End Region

#Region "EUS_CustomerPhotos"

    'Public Function GetEUS_CustomerPhotos() As DSMembers
    '    Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
    '    Dim ds As New DSMembers()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.EUS_CustomerPhotos)

    '    Return ds
    'End Function


    'Public Function GetEUS_CustomerPhotos_ByProfileOrMirrorID(customerId As Integer) As DSMembers
    '    Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
    '    Dim ds As New DSMembers()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.FillByProfileIDOrMirrorID(ds.EUS_CustomerPhotos, customerId)
    '    Return ds
    'End Function


    'Public Function GetEUS_CustomerPhotosByCustomerPhotosID(photoId As Integer) As DSMembers
    '    Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
    '    Dim ds As New DSMembers()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.FillByCustomerPhotosID(ds.EUS_CustomerPhotos, photoId)
    '    Return ds
    'End Function


    'Public Function GetEUS_CustomerVerificationDocsByID(CustomerVerificationDocsID As Integer) As DSCustomer
    '    Dim ta As New DSCustomerTableAdapters.EUS_CustomerVerificationDocsTableAdapter
    '    Dim ds As New DSCustomer()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.FillBy_CustomerVerificationDocsID(ds.EUS_CustomerVerificationDocs, CustomerVerificationDocsID)
    '    Return ds
    'End Function


    Public Sub UpdateREF_CustomerVerificationDocs(ByRef ds As DSReferrers)
        Dim ta As New DSReferrersTableAdapters.REF_CustomerVerificationDocsTableAdapter
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Update(ds.REF_CustomerVerificationDocs)
    End Sub

    'Public Sub UpdateEUS_CustomerPhotos(ByRef ds As DSMembers)
    '    Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Update(ds.EUS_CustomerPhotos)
    'End Sub

    'Public Sub UpdateEUS_CustomerVerificationDocs(ByRef ds As DSCustomer)
    '    Dim ta As New DSCustomerTableAdapters.EUS_CustomerVerificationDocsTableAdapter
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Update(ds.EUS_CustomerVerificationDocs)
    'End Sub


    'Public Function GetProfilesDefaultPhoto(profileId As Integer) As DSMembers.EUS_CustomerPhotosRow
    '    Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
    '    Dim ds As New DSMembers()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.FillBy_CustomerID_IsDefault(ds.EUS_CustomerPhotos, True, profileId)

    '    If (ds.EUS_CustomerPhotos.Rows.Count > 0) Then
    '        Return ds.EUS_CustomerPhotos.Rows(0)
    '    End If

    '    Return Nothing
    'End Function


    'Public Function GetProfilesDefaultPhotoLINQ(profileId As Integer) As EUS_CustomerPhoto
    '    Dim photo As EUS_CustomerPhoto = Nothing

    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try
    '        photo =
    '                (From phot In _CMSDBDataContext.EUS_CustomerPhotos
    '                Join pr In _CMSDBDataContext.EUS_Profiles On phot.CustomerID Equals pr.ProfileID
    '                Where (pr.ProfileID = profileId OrElse pr.MirrorProfileID = profileId) AndAlso _
    '                phot.IsDefault = True AndAlso phot.HasAproved = True AndAlso (phot.IsDeleted Is Nothing Or phot.IsDeleted = 0)
    '                Select phot).FirstOrDefault()


    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try

    '    Return photo
    'End Function


    Public Function GetREF_CustomerVerificationDocsByID(CustomerVerificationDocsID As Integer) As DSReferrers
        Dim ta As New DSReferrersTableAdapters.REF_CustomerVerificationDocsTableAdapter
        Dim ds As New DSReferrers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_CustomerVerificationDocsID(ds.REF_CustomerVerificationDocs, CustomerVerificationDocsID)

        Return ds
    End Function

#End Region



    Public Function GetSYS_CountriesGEO__IsEnabled_PrintableNameASC() As DSGEO
        Dim ds As New DSGEO()
        Dim ta As New DSGEOTableAdapters.TableAdapterManager
        Dim connection As New SqlConnection(DataHelpers.ConnectionString)

        ta.SYS_CountriesGEOTableAdapter = New DSGEOTableAdapters.SYS_CountriesGEOTableAdapter()
        ta.SYS_CountriesGEOTableAdapter.Connection = connection
        ta.SYS_CountriesGEOTableAdapter.FillBy_IsEnabled_PrintableNameASC(ds.SYS_CountriesGEO)
        Return ds
    End Function


    Public Function GetEUS_LISTS() As DSLists
        Dim ds As New DSLists()
        Dim ta As New DSListsTableAdapters.TableAdapterManager
        Dim connection As New SqlConnection(DataHelpers.ConnectionString)

        ta.EUS_LISTS_BodyTypeTableAdapter = New DSListsTableAdapters.EUS_LISTS_BodyTypeTableAdapter()
        ta.EUS_LISTS_BodyTypeTableAdapter.Connection = connection
        ta.EUS_LISTS_BodyTypeTableAdapter.Fill(ds.EUS_LISTS_BodyType)

        ta.EUS_LISTS_ChildrenNumberTableAdapter = New DSListsTableAdapters.EUS_LISTS_ChildrenNumberTableAdapter()
        ta.EUS_LISTS_ChildrenNumberTableAdapter.Connection = connection
        ta.EUS_LISTS_ChildrenNumberTableAdapter.Fill(ds.EUS_LISTS_ChildrenNumber)

        ta.EUS_LISTS_DrinkingTableAdapter = New DSListsTableAdapters.EUS_LISTS_DrinkingTableAdapter()
        ta.EUS_LISTS_DrinkingTableAdapter.Connection = connection
        ta.EUS_LISTS_DrinkingTableAdapter.Fill(ds.EUS_LISTS_Drinking)

        ta.EUS_LISTS_EducationTableAdapter = New DSListsTableAdapters.EUS_LISTS_EducationTableAdapter()
        ta.EUS_LISTS_EducationTableAdapter.Connection = connection
        ta.EUS_LISTS_EducationTableAdapter.Fill(ds.EUS_LISTS_Education)

        ta.EUS_LISTS_EthnicityTableAdapter = New DSListsTableAdapters.EUS_LISTS_EthnicityTableAdapter()
        ta.EUS_LISTS_EthnicityTableAdapter.Connection = connection
        ta.EUS_LISTS_EthnicityTableAdapter.Fill(ds.EUS_LISTS_Ethnicity)

        ta.EUS_LISTS_EyeColorTableAdapter = New DSListsTableAdapters.EUS_LISTS_EyeColorTableAdapter()
        ta.EUS_LISTS_EyeColorTableAdapter.Connection = connection
        ta.EUS_LISTS_EyeColorTableAdapter.Fill(ds.EUS_LISTS_EyeColor)

        ta.EUS_LISTS_HairColorTableAdapter = New DSListsTableAdapters.EUS_LISTS_HairColorTableAdapter()
        ta.EUS_LISTS_HairColorTableAdapter.Connection = connection
        ta.EUS_LISTS_HairColorTableAdapter.Fill(ds.EUS_LISTS_HairColor)

        ta.EUS_LISTS_HeightTableAdapter = New DSListsTableAdapters.EUS_LISTS_HeightTableAdapter()
        ta.EUS_LISTS_HeightTableAdapter.Connection = connection
        ta.EUS_LISTS_HeightTableAdapter.Fill(ds.EUS_LISTS_Height)

        ta.EUS_LISTS_IncomeTableAdapter = New DSListsTableAdapters.EUS_LISTS_IncomeTableAdapter()
        ta.EUS_LISTS_IncomeTableAdapter.Connection = connection
        ta.EUS_LISTS_IncomeTableAdapter.Fill(ds.EUS_LISTS_Income)

        ta.EUS_LISTS_NetWorthTableAdapter = New DSListsTableAdapters.EUS_LISTS_NetWorthTableAdapter()
        ta.EUS_LISTS_NetWorthTableAdapter.Connection = connection
        ta.EUS_LISTS_NetWorthTableAdapter.Fill(ds.EUS_LISTS_NetWorth)

        ta.EUS_LISTS_RelationshipStatusTableAdapter = New DSListsTableAdapters.EUS_LISTS_RelationshipStatusTableAdapter()
        ta.EUS_LISTS_RelationshipStatusTableAdapter.Connection = connection
        ta.EUS_LISTS_RelationshipStatusTableAdapter.Fill(ds.EUS_LISTS_RelationshipStatus)

        ta.EUS_LISTS_ReligionTableAdapter = New DSListsTableAdapters.EUS_LISTS_ReligionTableAdapter()
        ta.EUS_LISTS_ReligionTableAdapter.Connection = connection
        ta.EUS_LISTS_ReligionTableAdapter.Fill(ds.EUS_LISTS_Religion)

        ta.EUS_LISTS_SmokingTableAdapter = New DSListsTableAdapters.EUS_LISTS_SmokingTableAdapter()
        ta.EUS_LISTS_SmokingTableAdapter.Connection = connection
        ta.EUS_LISTS_SmokingTableAdapter.Fill(ds.EUS_LISTS_Smoking)

        ta.EUS_LISTS_RejectingReasonsTableAdapter = New DSListsTableAdapters.EUS_LISTS_RejectingReasonsTableAdapter()
        ta.EUS_LISTS_RejectingReasonsTableAdapter.Connection = connection
        ta.EUS_LISTS_RejectingReasonsTableAdapter.Fill(ds.EUS_LISTS_RejectingReasons)

        ta.SYS_CountriesGEOTableAdapter = New DSListsTableAdapters.SYS_CountriesGEOTableAdapter()
        ta.SYS_CountriesGEOTableAdapter.Connection = connection
        ta.SYS_CountriesGEOTableAdapter.Fill(ds.SYS_CountriesGEO)

        ta.EUS_LISTS_AccountTypeTableAdapter = New DSListsTableAdapters.EUS_LISTS_AccountTypeTableAdapter()
        ta.EUS_LISTS_AccountTypeTableAdapter.Connection = connection
        ta.EUS_LISTS_AccountTypeTableAdapter.Fill(ds.EUS_LISTS_AccountType)

        ta.EUS_LISTS_GenderTableAdapter = New DSListsTableAdapters.EUS_LISTS_GenderTableAdapter()
        ta.EUS_LISTS_GenderTableAdapter.Connection = connection
        ta.EUS_LISTS_GenderTableAdapter.Fill(ds.EUS_LISTS_Gender)

        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter = New DSListsTableAdapters.EUS_LISTS_PhotosDisplayLevelTableAdapter()
        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Connection = connection
        ta.EUS_LISTS_PhotosDisplayLevelTableAdapter.Fill(ds.EUS_LISTS_PhotosDisplayLevel)

        ta.EUS_LISTS_TypeOfDatingTableAdapter = New DSListsTableAdapters.EUS_LISTS_TypeOfDatingTableAdapter()
        ta.EUS_LISTS_TypeOfDatingTableAdapter.Connection = connection
        ta.EUS_LISTS_TypeOfDatingTableAdapter.Fill(ds.EUS_LISTS_TypeOfDating)

        ta.EUS_OffersStatusTableAdapter = New DSListsTableAdapters.EUS_OffersStatusTableAdapter()
        ta.EUS_OffersStatusTableAdapter.Connection = connection
        ta.EUS_OffersStatusTableAdapter.Fill(ds.EUS_OffersStatus)

        ta.EUS_OffersTypesTableAdapter = New DSListsTableAdapters.EUS_OffersTypesTableAdapter()
        ta.EUS_OffersTypesTableAdapter.Connection = connection
        ta.EUS_OffersTypesTableAdapter.Fill(ds.EUS_OffersTypes)

        Return ds
    End Function



    Public Function GetEUS_LISTS_ReportingReason() As DSLists
        Dim ds As New DSLists()
        Dim ta As New DSListsTableAdapters.EUS_LISTS_ReportingReasonTableAdapter

        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.Fill(ds.EUS_LISTS_ReportingReason)
        Return ds
    End Function


    'Public  Function GetEUS_LISTS_Gender() As DSLists
    '    Dim ta As New DSListsTableAdapters.EUS_LISTS_GenderTableAdapter
    '    Dim ds As New DSLists()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.EUS_LISTS_Gender)

    '    Return ds
    'End Function


    'Public  Function GetEUS_LISTS_AccountType() As DSLists
    '    Dim ta As New DSListsTableAdapters.EUS_LISTS_AccountTypeTableAdapter
    '    Dim ds As New DSLists()
    '    ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '    ta.Fill(ds.EUS_LISTS_AccountType)

    '    Return ds
    'End Function




    '    Public Function GetApprovedUsersDatatable(topRecords As Integer) As DataTable
    '        Dim dataTbl As DataTable = Nothing

    '        Dim sql As String = <sql><![CDATA[
    'SELECT TOP({0}) 
    '    [ProfileID], 
    '    [LoginName], 
    '    [Genderid],
    '    (Select top(1) [FileName] from EUS_CustomerPhotos where [CustomerID]=[ProfileID] and IsDefault=1 and HasAproved=1 and ISNULL(phot.IsDeleted,0) = 0) as ImageFileName
    'FROM [dbo].[EUS_Profiles]
    'WHERE [Status]= {1} and [IsMaster]=1 and 
    'Genderid = {2}
    'UNION
    'SELECT TOP({0}) 
    '    [ProfileID], 
    '    [LoginName], 
    '    [Genderid],
    '    (Select top(1) [FileName] from EUS_CustomerPhotos where [CustomerID]=[ProfileID] and IsDefault=1 and HasAproved=1 and ISNULL(phot.IsDeleted,0) = 0) as ImageFileName
    'FROM [dbo].[EUS_Profiles]
    'WHERE [Status]={1} and [IsMaster]=1 and  
    'Genderid = {3}
    ']]></sql>.Value


    '        sql = String.Format(sql, topRecords.ToString(), CType(ProfileStatusEnum.Approved, Integer).ToString(), ProfileHelper.gMaleGender.GenderId.ToString(), ProfileHelper.gFemaleGender.GenderId.ToString())

    '        Dim command As SqlCommand = GetSqlCommand(sql)
    '        Dim adapter As New SqlDataAdapter(command)
    '        dataTbl = New DataTable()
    '        Try
    '            adapter.Fill(dataTbl)
    '        Catch ex As Exception
    '            Throw
    '        Finally
    '            command.Connection.Close()
    '            command.Dispose()
    '        End Try

    '        Return dataTbl
    '    End Function


    Public Function CheckSqlException(exMessage As String) As Boolean
        Dim rerunTran As Boolean = False
        If (exMessage.Contains("Rerun the transaction.")) Then
            System.Threading.Thread.Sleep(600)
            rerunTran = True
        End If

        If (exMessage.Contains("Cannot open database") OrElse exMessage.Contains("A connection was successfully")) Then
            'System.Data.SqlClient.SqlException (0x80131904): A connection was successfully established with the server, but then an error occurred during the pre-login handshake. 
            'System.Data.SqlClient.SqlException (0x80131904): Cannot open database "CMSGoomena" requested by the login. The login failed.

            ' wait 15 seconds, while server is restarting
            System.Threading.Thread.Sleep(WAIT_DB_RESTART_MILLISEC)
            rerunTran = True
        End If
        Return rerunTran
    End Function


    Public Function GetSqlCommand(Optional ByVal sqlStatement As String = "") As SqlClient.SqlCommand
        If (sqlStatement Is Nothing) Then sqlStatement = ""

        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

        Return command
    End Function


    Public Function GetDataSet(command As SqlClient.SqlCommand) As DataSet

        Dim ds As New DataSet()
        Dim adapter As New SqlDataAdapter(command)

        Try
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(ds)
            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    adapter.Fill(ds)
                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False
                adapter.Fill(ds)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            command.Connection.Close()
            command.Dispose()
            adapter.Dispose()
        End Try

        Return ds
    End Function

    Public Function GetDataSet(sqlStatement As String) As DataSet

        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

        Dim ds As New DataSet()
        Dim adapter As New SqlDataAdapter(command)

        Try
            'adapter.Fill(ds)
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(ds)
            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    adapter.Fill(ds)
                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False
                adapter.Fill(ds)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            command.Connection.Close()
            command.Dispose()
            adapter.Dispose()
        End Try

        Return ds
    End Function


    Public Function GetDataTable(sqlStatement As String) As DataTable


        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

        Dim dt As New DataTable()
        Dim adapter As New SqlDataAdapter(command)

        Try
            'adapter.Fill(dt)
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(dt)
            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                adapter.Fill(dt)
            End If

        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
            adapter.Dispose()
        End Try

        Return dt
    End Function

    Public Function GetDataTable(command As SqlClient.SqlCommand) As DataTable

        Dim dt As New DataTable()
        Dim adapter As New SqlDataAdapter(command)

        Try
            'adapter.Fill(dt)
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(dt)
            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    adapter.Fill(dt)
                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False
                adapter.Fill(dt)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            command.Connection.Close()
            command.Dispose()
            adapter.Dispose()
        End Try

        Return dt
    End Function


    Public Sub FillDataTable(sqlStatement As String, ByRef table As DataTable)

        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

        Dim adapter As New SqlDataAdapter(command)
        Try
            'adapter.Fill(table)
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(table)
            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    adapter.Fill(table)
                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False
                adapter.Fill(table)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

    End Sub

    Public Sub FillDataTable(command As SqlClient.SqlCommand, ByRef table As DataTable)

        Dim adapter As New SqlDataAdapter(command)
        Try
            'adapter.Fill(table)
            Dim rerunTran As Boolean = False
            Try
                adapter.Fill(table)
            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    adapter.Fill(table)
                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False
                adapter.Fill(table)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

    End Sub


    Public Function ExecuteScalar(command As SqlClient.SqlCommand) As Object
        Dim result As Long = -1
        Dim rerunTran As Boolean = False
        command.Connection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)

        Try

            command.Connection.Open()
            result = command.ExecuteScalar()

        Catch ex As System.Data.SqlClient.SqlException
            rerunTran = DataHelpers.CheckSqlException(ex.Message)
            If (Not rerunTran) Then Throw
        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            If (Not rerunTran) Then command.Dispose()
        End Try


        If (rerunTran) Then
            Try
                command.Connection.Open()
                result = command.ExecuteScalar()
            Catch ex As Exception
                Throw
            Finally
                command.Connection.Close()
                command.Dispose()
            End Try
        End If

        Return result
    End Function

    Public Function ExecuteScalar(sqlStatement As String) As Object

        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
        Dim result As Integer = -1

        Try
            command.Connection.Open()

            Dim rerunTran As Boolean = False
            Try
                result = command.ExecuteScalar()
            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    result = command.ExecuteScalar()
                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False
                result = command.ExecuteScalar()
            End If

        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

        Return result
    End Function

    Public Function ExecuteScalar(sqlStatement As String, newConnectionString As String) As Object

        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(newConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
        Dim result As Integer = -1

        Try
            command.Connection.Open()

            Dim rerunTran As Boolean = False
            Try
                result = command.ExecuteScalar()
            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    result = command.ExecuteScalar()
                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False
                result = command.ExecuteScalar()
            End If

        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

        Return result
    End Function


    Public Function ExecuteNonQuery(command As SqlClient.SqlCommand) As Integer
        Dim result As Integer = -1
        Dim rerunTran As Boolean = False
        command.Connection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)


        Try

            command.Connection.Open()
            result = command.ExecuteNonQuery()

        Catch ex As System.Data.SqlClient.SqlException
            rerunTran = DataHelpers.CheckSqlException(ex.Message)
            If (Not rerunTran) Then Throw
        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            If (Not rerunTran) Then command.Dispose()
        End Try


        If (rerunTran) Then
            Try
                command.Connection.Open()
                result = command.ExecuteNonQuery()
            Catch ex As Exception
                Throw
            Finally
                command.Connection.Close()
                command.Dispose()
            End Try
        End If

        Return result
    End Function

    Public Function ExecuteNonQuery(command As SqlClient.SqlCommand, newConnectionString As String) As Integer
        Dim result As Integer = -1
        Dim rerunTran As Boolean = False
        command.Connection = New SqlClient.SqlConnection(newConnectionString)


        Try

            command.Connection.Open()
            result = command.ExecuteNonQuery()

        Catch ex As System.Data.SqlClient.SqlException
            rerunTran = DataHelpers.CheckSqlException(ex.Message)
            If (Not rerunTran) Then Throw
        Catch ex As Exception
            Throw
        Finally
            command.Connection.Close()
            If (Not rerunTran) Then command.Dispose()
        End Try


        If (rerunTran) Then
            Try
                command.Connection.Open()
                result = command.ExecuteNonQuery()
            Catch ex As Exception
                Throw
            Finally
                command.Connection.Close()
                command.Dispose()
            End Try
        End If

        Return result
    End Function

    Public Function ExecuteNonQuery(sqlStatement As String) As Integer


        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
        Dim result As Integer = -1

        Try
            command.Connection.Open()
            Dim rerunTran As Boolean = False
            Try
                result = command.ExecuteNonQuery()
            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    result = command.ExecuteNonQuery()
                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False
                result = command.ExecuteNonQuery()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

        Return result
    End Function

    Public Function ExecuteNonQuery(sqlStatement As String, newConnectionString As String) As Integer


        Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(newConnectionString)
        Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
        Dim result As Integer = -1

        Try
            command.Connection.Open()
            Dim rerunTran As Boolean = False
            Try
                result = command.ExecuteNonQuery()
            Catch ex As System.Data.SqlClient.SqlException
                rerunTran = DataHelpers.CheckSqlException(ex.Message)
                If (Not rerunTran) Then Throw
            End Try

            If (rerunTran) Then
                rerunTran = False
                Try
                    result = command.ExecuteNonQuery()
                Catch ex As System.Data.SqlClient.SqlException
                    rerunTran = DataHelpers.CheckSqlException(ex.Message)
                    If (Not rerunTran) Then Throw
                End Try
            End If

            If (rerunTran) Then
                rerunTran = False
                result = command.ExecuteNonQuery()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message, ex)
        Finally
            command.Connection.Close()
            command.Dispose()
        End Try

        Return result
    End Function



    'Public Function GetDataTable(sqlStatement As String) As DataTable


    '    Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
    '    Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

    '    Dim dt As New DataTable()
    '    Dim adapter As New SqlDataAdapter(command)

    '    Try
    '        adapter.Fill(dt)
    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        command.Connection.Close()
    '        command.Dispose()
    '        adapter.Dispose()
    '    End Try

    '    Return dt
    'End Function


    'Public Function GetDataReader(sqlStatement As String) As SqlDataReader


    '    Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
    '    Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

    '    Dim sdr As SqlDataReader
    '    Try
    '        command.Connection.Open()
    '        sdr = command.ExecuteReader()
    '    Catch ex As Exception
    '        Throw
    '    End Try

    '    Return sdr
    'End Function


    'Public Function GetDataSet(command As SqlClient.SqlCommand) As DataSet

    '    Dim ds As New DataSet()
    '    Dim adapter As New SqlDataAdapter(command)

    '    Try
    '        adapter.Fill(ds)
    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        command.Connection.Close()
    '        command.Dispose()
    '        adapter.Dispose()
    '    End Try

    '    Return ds
    'End Function

    'Public Function GetDataTable(command As SqlClient.SqlCommand) As DataTable

    '    Dim dt As New DataTable()
    '    Dim adapter As New SqlDataAdapter(command)

    '    Try
    '        adapter.Fill(dt)
    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        command.Connection.Close()
    '        command.Dispose()
    '        adapter.Dispose()
    '    End Try

    '    Return dt
    'End Function



    'Public Sub FillDataTable(sqlStatement As String, ByRef table As DataTable)

    '    Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
    '    Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)

    '    Dim adapter As New SqlDataAdapter(command)
    '    Try
    '        adapter.Fill(table)
    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        command.Connection.Close()
    '        command.Dispose()
    '    End Try

    'End Sub


    'Public Function ExecuteNonQuery(command As SqlClient.SqlCommand) As Integer
    '    Dim result As Integer = -1
    '    Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
    '    command.Connection = connection

    '    Try
    '        command.Connection.Open()
    '        result = command.ExecuteNonQuery()
    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        command.Connection.Close()
    '        command.Dispose()
    '    End Try

    '    Return result
    'End Function



    'Public Function ExecuteNonQuery(sqlStatement As String)

    '    Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
    '    Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
    '    Dim result As Integer = -1

    '    Try
    '        command.Connection.Open()
    '        result = command.ExecuteNonQuery()
    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        command.Connection.Close()
    '        command.Dispose()
    '    End Try

    '    Return result
    'End Function



    'Public Function ExecuteScalar(command As SqlClient.SqlCommand) As Object
    '    Dim result As Integer = -1
    '    Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
    '    command.Connection = connection

    '    Try
    '        command.Connection.Open()
    '        result = command.ExecuteScalar()
    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        command.Connection.Close()
    '        command.Dispose()
    '    End Try

    '    Return result
    'End Function

    'Public Function ExecuteScalar(sqlStatement As String) As Object

    '    Dim connection As SqlClient.SqlConnection = New SqlClient.SqlConnection(DataHelpers.ConnectionString)
    '    Dim command As SqlClient.SqlCommand = New SqlClient.SqlCommand(sqlStatement, connection)
    '    Dim result As Integer = -1

    '    Try
    '        command.Connection.Open()
    '        result = command.ExecuteScalar()
    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        command.Connection.Close()
    '        command.Dispose()
    '    End Try

    '    Return result
    'End Function



    Public Function ToDataTable(ctx As System.Data.Linq.DataContext, query As Object) As DataTable
        If query Is Nothing Then
            Throw New ArgumentNullException("query")
        End If

        Dim cmd As IDbCommand = ctx.GetCommand(TryCast(query, IQueryable))
        Dim adapter As New SqlClient.SqlDataAdapter()
        adapter.SelectCommand = DirectCast(cmd, SqlClient.SqlCommand)
        Dim dt As New DataTable("Generated")

        Try
            cmd.Connection.Open()
            adapter.FillSchema(dt, SchemaType.Source)
            adapter.Fill(dt)
        Finally
            cmd.Connection.Close()
        End Try
        Return dt
    End Function



    Public Function GetExecutionStatement(cmd As SqlCommand)

        Dim quotedParameterTypes As DbType() = New DbType() {DbType.AnsiString, DbType.Date, DbType.DateTime, DbType.Guid, DbType.String, DbType.AnsiStringFixedLength, _
 DbType.StringFixedLength}
        Dim query As String = cmd.CommandText

        Dim arrParams = New SqlParameter(cmd.Parameters.Count - 1) {}
        cmd.Parameters.CopyTo(arrParams, 0)

        For Each prm As SqlParameter In arrParams.OrderByDescending(Function(p) p.ParameterName.Length)
            Dim value As String = prm.Value.ToString()

            If (prm.DbType = DbType.DateTime) Then
                value = "'" & DirectCast(prm.Value, DateTime).ToString("yyyy-MM-dd hh:mm:ss.mmm") & "'"
                'value = "'" & value & "'"
            ElseIf quotedParameterTypes.Contains(prm.DbType) Then
                value = "'" & value & "'"
            End If


            query = query.Replace("@" & prm.ParameterName, value)
        Next

        Return query
    End Function




    'Public Function SetFirstDefaultEUS_CustomerPhotos(CustomerID As Integer) As Boolean
    '    Dim success As Boolean

    '    Try

    '        Dim ta As New DSMembersTableAdapters.EUS_CustomerPhotosTableAdapter
    '        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
    '        'ByRef ds As DSMembers,

    '        Dim ds As New DSMembers
    '        ta.FillByProfileIDOrMirrorID(ds.EUS_CustomerPhotos, CustomerID)

    '        Dim firstPhotoIndex As Integer = -1
    '        Dim defaultPhotoFound As Boolean = False

    '        Dim counter As Integer = 0
    '        For counter = 0 To (ds.EUS_CustomerPhotos.Count - 1)
    '            Dim dr As DSMembers.EUS_CustomerPhotosRow = ds.EUS_CustomerPhotos(counter)


    '            ''''''''''''''''''''''
    '            ' get the first photo that is not default and not declined
    '            ''''''''''''''''''''''
    '            If (dr.DisplayLevel = 0 AndAlso dr.HasAproved = True AndAlso dr.HasDeclined = False AndAlso dr.IsDefault = False AndAlso (dr.IsIsDeletedNull() OrElse dr.IsDeleted = False)) Then
    '                firstPhotoIndex = counter
    '            End If

    '            ''''''''''''''''''''''
    '            ' search for available default photos
    '            ''''''''''''''''''''''
    '            If (dr.DisplayLevel = 0 AndAlso dr.HasAproved = True AndAlso dr.HasDeclined = False AndAlso dr.IsDefault = True AndAlso (dr.IsIsDeletedNull() OrElse dr.IsDeleted = False)) Then
    '                defaultPhotoFound = True
    '            End If
    '        Next


    '        If (Not defaultPhotoFound AndAlso firstPhotoIndex >= 0 AndAlso firstPhotoIndex < ds.EUS_CustomerPhotos.Count) Then
    '            Dim dr As DSMembers.EUS_CustomerPhotosRow = ds.EUS_CustomerPhotos.Rows(firstPhotoIndex)
    '            dr.IsDefault = True
    '            ta.Update(ds)
    '            success = True
    '        End If
    '    Catch ex As Exception
    '        Throw
    '    End Try

    '    Return success
    'End Function



    'Public Sub CopyEUS_ProfilesRowsData(ByRef EUS_Profiles As DSMembers.EUS_ProfilesDataTable, ByRef TargetDataRow As DataRow, ByRef SourceDataRow As DataRow)
    '    ''''''''''''''''''''''''''''''''''''''''''''
    '    ' copy data from mirror record to master, overwrite old user data with new
    '    ''''''''''''''''''''''''''''''''''''''''''''

    '    ' copy data that is not read only
    '    Dim cnt As Integer = 0
    '    For cnt = 0 To EUS_Profiles.Columns.Count - 1
    '        Dim dc As DataColumn = EUS_Profiles.Columns(cnt)
    '        If (dc.ReadOnly OrElse dc.ColumnName = "IsMaster" OrElse dc.ColumnName = "MirrorProfileID") Then
    '            Continue For
    '        End If

    '        TargetDataRow(cnt) = SourceDataRow(cnt)
    '    Next

    'End Sub


    'Public Function ApproveProfile(ByRef members As DSMembers, Optional IsAutoApproved As Boolean = False) As Boolean
    '    Dim success As Boolean
    '    Try

    '        If (members.EUS_Profiles.Rows.Count = 1) Then
    '            success = ApproveNewProfile(members, IsAutoApproved)
    '        Else
    '            success = ApproveExistingProfile(members, IsAutoApproved)
    '        End If
    '    Catch ex As Exception
    '        Throw
    '    End Try
    '    Return success
    'End Function

    'Public Function ApproveProfile(loginName As String, Optional IsAutoApproved As Boolean = False) As Boolean
    '    Dim success As Boolean
    '    Try
    '        Dim members As DSMembers = DataHelpers.GetEUS_Profiles_ByLoginName(loginName)

    '        If (members.EUS_Profiles.Rows.Count = 1) Then
    '            success = ApproveNewProfile(members, IsAutoApproved)

    '            'Dim customerid As Integer = members.EUS_Profiles.Rows(0)("ProfileID")
    '            'Dim dt As DataTable = clsSearchHelper.GetMembersThatShouldBeNotifiedForNewMember(customerid)

    '            'If (dt.Rows.Count > 0) Then
    '            '    Dim cnt As Integer
    '            '    For cnt = 0 To dt.Rows.Count - 1
    '            '        Dim toProfileId As Integer = dt.Rows(cnt)("CustomerID")
    '            '        clsUserDoes.SendEmailNotification(NotificationType.AutoNotification, customerid, toProfileId)
    '            '    Next
    '            'End If


    '        Else
    '            success = ApproveExistingProfile(members, IsAutoApproved)
    '        End If
    '    Catch ex As Exception
    '        Throw
    '    End Try
    '    Return success
    'End Function

    'Public Function ApproveNewProfile(members As DSMembers, Optional IsAutoApproved As Boolean = False) As Boolean
    '    Dim success As Boolean
    '    Try

    '        Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)

    '        Dim sql As String = "exec [EUS_Profiles_ApproveNewProfile] @CustomerId=@CustomerId"
    '        Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
    '        cmd.Parameters.AddWithValue("@CustomerId", masterRow.ProfileID)
    '        DataHelpers.ExecuteNonQuery(cmd)
    '        success = True

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message, ex)
    '    End Try
    '    Return success
    'End Function

    'Public Function ApproveExistingProfile(members As DSMembers, Optional IsAutoApproved As Boolean = False) As Boolean
    '    Dim success As Boolean
    '    Try

    '        Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
    '        Dim mirrorRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(1)

    '        Dim isvalid = True
    '        If (masterRow.ProfileID = mirrorRow.ProfileID) Then
    '            isvalid = False
    '            Throw New System.Exception("Invalid profile data: " & vbCrLf & vbTab & "the mirror and master is same table record." & vbCrLf & vbTab & "Cannot approve.")
    '        End If

    '        If (isvalid) Then

    '            ''''''''''''''''''''''''''''''''''''''''''''
    '            ' copy data from mirror record to master, overwrite old user data with new
    '            ''''''''''''''''''''''''''''''''''''''''''''

    '            CopyEUS_ProfilesRowsData(members.EUS_Profiles, masterRow, mirrorRow)
    '            '' copy data that is not read only
    '            'Dim cnt As Integer = 0
    '            'For cnt = 0 To members.EUS_Profiles.Columns.Count - 1
    '            '    If (Not members.EUS_Profiles.Columns(cnt).ReadOnly) Then
    '            '        masterRow(cnt) = mirrorRow(cnt)
    '            '    End If
    '            'Next


    '            mirrorRow.Status = ProfileStatusEnum.Approved
    '            masterRow.Status = ProfileStatusEnum.Approved

    '            'mirrorRow.IsMaster = False
    '            'masterRow.IsMaster = True

    '            'mirrorRow.MirrorProfileID = masterRow.ProfileID
    '            'masterRow.MirrorProfileID = mirrorRow.ProfileID

    '            mirrorRow.IsAutoApproved = IsAutoApproved
    '            masterRow.IsAutoApproved = IsAutoApproved

    '            '' update db
    '            'PergormReLogin()
    '            'CheckWebServiceCallResult(gAdminWS.UpdateEUS_Profiles(gLoginRec.Token, Me.DSMembers))

    '            '' send email to user
    '            '_SendEmailMessage(mirrorRow.eMail, "Your changes to " & My.MySettings.Default.SiteNameForCustomers & " profile are accepted.", "Your changes to " & My.MySettings.Default.SiteNameForCustomers & " profile are accepted.")

    '            DataHelpers.UpdateEUS_Profiles(members)

    '            'clsMyMail.SendMail(masterRow.eMail, "Your changes to Goomena.com profile are accepted.", "Your changes to Goomena.com profile are accepted.")

    '            success = True
    '        End If

    '    Catch ex As Exception
    '        Throw
    '    End Try
    '    Return success
    'End Function




    'Public Function RejectProfile(ByRef members As DSMembers) As Boolean
    '    Dim success As Boolean
    '    Try
    '        If (members.EUS_Profiles.Rows.Count = 1) Then
    '            success = RejectNewProfile(members)
    '        Else
    '            success = RejectExistingProfile(members)
    '        End If
    '    Catch ex As Exception
    '        Throw
    '    End Try
    '    Return success
    'End Function


    'Public Function RejectProfile(loginName As String) As Boolean
    '    Dim success As Boolean
    '    Try
    '        Dim members As DSMembers = DataHelpers.GetEUS_Profiles_ByLoginName(loginName)

    '        If (members.EUS_Profiles.Rows.Count = 1) Then
    '            success = RejectNewProfile(members)
    '        Else
    '            success = RejectExistingProfile(members)
    '        End If
    '    Catch ex As Exception
    '        Throw
    '    End Try
    '    Return success
    'End Function


    'Public Function RejectNewProfile(members As DSMembers)
    '    Dim success As Boolean
    '    Try

    '        Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
    '        masterRow.Status = ProfileStatusEnum.Rejected

    '        DataHelpers.UpdateEUS_Profiles(members)

    '        success = True
    '    Catch ex As Exception
    '        Throw
    '    End Try
    '    Return success
    'End Function


    'Public Function RejectExistingProfile(members As DSMembers)
    '    Dim success As Boolean
    '    Try

    '        Dim masterRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(0)
    '        Dim mirrorRow As DSMembers.EUS_ProfilesRow = members.EUS_Profiles.Rows(1)

    '        If (masterRow.IsMaster = False) Then
    '            masterRow = members.EUS_Profiles.Rows(1)
    '            mirrorRow = members.EUS_Profiles.Rows(0)
    '        End If

    '        Dim isvalid = True
    '        If (masterRow.ProfileID = mirrorRow.ProfileID) Then
    '            isvalid = False
    '            Throw New System.Exception("Invalid profile data: " & vbCrLf & vbTab & "the mirror and master is same table record." & vbCrLf & vbTab & "Cannot approve.")
    '        End If

    '        If (isvalid) Then
    '            ''''''''''''''''''''''''''''''''''''''''''''
    '            ' copy data from master record to mirror, overwrite user changes
    '            ''''''''''''''''''''''''''''''''''''''''''''

    '            '' copy data that is not read only
    '            'Dim cnt As Integer = 0
    '            'For cnt = 0 To members.EUS_Profiles.Columns.Count - 1
    '            '    If (Not members.EUS_Profiles.Columns(cnt).ReadOnly) Then
    '            '        mirrorRow(cnt) = masterRow(cnt)
    '            '    End If
    '            'Next

    '            'mirrorRow.IsMaster = False
    '            'masterRow.IsMaster = True

    '            'mirrorRow.MirrorProfileID = masterRow.ProfileID
    '            'masterRow.MirrorProfileID = mirrorRow.ProfileID

    '            CopyEUS_ProfilesRowsData(members.EUS_Profiles, mirrorRow, masterRow)

    '            ' update db
    '            DataHelpers.UpdateEUS_Profiles(members)

    '            success = True
    '        End If

    '    Catch ex As Exception
    '        Throw
    '    End Try
    '    Return success
    'End Function



    'Public Function ApprovePhoto(photoId As Integer, Optional IsAutoApproved As Boolean = False) As Boolean
    '    Dim IsDefaultPhotoSet As Boolean
    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try

    '        Dim photo As Core.DLL.EUS_CustomerPhoto = (From itm In _CMSDBDataContext.EUS_CustomerPhotos
    '                                          Where itm.CustomerPhotosID = photoId
    '                                          Select itm).FirstOrDefault()

    '        If (IsAutoApproved) Then
    '            photo.HasAproved = True
    '            photo.HasDeclined = False
    '        End If
    '        photo.IsAutoApproved = IsAutoApproved

    '        _CMSDBDataContext.SubmitChanges()

    '        IsDefaultPhotoSet = DataHelpers.SetFirstDefaultEUS_CustomerPhotos(photo.CustomerID)

    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try
    '    Return IsDefaultPhotoSet
    'End Function


    'Public Function RejectPhoto(photoId As Integer) As Boolean
    '    Dim IsDefaultPhotoSet As Boolean
    '    Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
    '    Try


    '        Dim photo As Core.DLL.EUS_CustomerPhoto = (From itm In _CMSDBDataContext.EUS_CustomerPhotos
    '                                          Where itm.CustomerPhotosID = photoId
    '                                          Select itm).FirstOrDefault()

    '        photo.HasAproved = False
    '        photo.HasDeclined = True
    '        photo.IsDefault = False
    '        photo.IsAutoApproved = False

    '        _CMSDBDataContext.SubmitChanges()

    '        IsDefaultPhotoSet = DataHelpers.SetFirstDefaultEUS_CustomerPhotos(photo.CustomerID)

    '    Catch ex As Exception
    '        Throw
    '    Finally
    '        _CMSDBDataContext.Dispose()
    '    End Try
    '    Return IsDefaultPhotoSet
    'End Function



    Public Sub LogProfileAccess(ProfileID As Integer, KeybLoginName As String, keybPassword As String, Success As Boolean, Title As String)
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try
            Dim access As New Core.DLL.SYS_ProfilesAccess

            access.DateTimeAccess = Date.UtcNow
            access.KeybLoginName = KeybLoginName
            access.keybPassword = keybPassword
            access.ProfileID = ProfileID
            access.Success = Success
            access.Title = Title

            _CMSDBDataContext.SYS_ProfilesAccesses.InsertOnSubmit(access)
            _CMSDBDataContext.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try
    End Sub



    Public Function GetAvailable_AFF_PayoutTypes() As DataTable
        Dim dt As DataTable = Nothing
        dt = DataHelpers.GetDataTable("SELECT * FROM AFF_PayoutTypes WHERE IsActive=1 ORDER BY Title")
        Return dt
    End Function


    Public Function EUS_CreditsBonusCodes_IsCodeAvailable(ProfileId As Integer, Code As String) As Boolean
        Dim dt As Long = 0
        'Dim sql As String = "select isnull((SELECT case when CreditsCodeId > 0 then 0 else 1 end  FROM EUS_CreditsBonusCodes WHERE Code=@Code and ProfileID=@ProfileID), 1) as IsCodeAvailable"
        'Dim sql As String = "select isnull((SELECT case when SubmittedDate is null then 1 else 0 end FROM EUS_CreditsBonusCodes WHERE Code=@Code and ProfileID=@ProfileID), 0) as IsCodeAvailable"

        Dim sql As String = <sql><![CDATA[
if('Q5JKLRS4T'=@Code and not exists(SELECT  CreditsCodeId 
									FROM EUS_CreditsBonusCodes 
									WHERE Code=@Code and ProfileID=@ProfileID))	
	select 1 as IsCodeAvailable
else
	select 0 as IsCodeAvailable
]]></sql>

        Dim command As SqlClient.SqlCommand = GetSqlCommand(sql)
        command.Parameters.AddWithValue("@Code", Code)
        command.Parameters.AddWithValue("@ProfileID", ProfileId)
        dt = DataHelpers.ExecuteScalar(command)

        Return (dt = 1)
    End Function

    Public Function EUS_CreditsBonusCodes_DisableCode(ProfileId As Integer, Code As String) As Boolean
        Dim dt As Long = 0
        Dim sql As String = <sql><![CDATA[
if(exists(SELECT * FROM EUS_CreditsBonusCodes WHERE Code=@Code and ProfileID=@ProfileID))
begin
    update EUS_CreditsBonusCodes	set SubmittedDate = GETUTCDATE(),ProfileID=@ProfileID	WHERE Code=@Code
end
else
begin
    INSERT INTO [EUS_CreditsBonusCodes]
           ([Code]
           ,[SubmittedDate]
           ,[ProfileID])
     VALUES
           (@Code
           ,GETUTCDATE()
           ,@ProfileID)
end
]]></sql>
        '""

        Dim command As SqlClient.SqlCommand = GetSqlCommand(sql)
        command.Parameters.AddWithValue("@Code", Code)
        command.Parameters.AddWithValue("@ProfileID", ProfileId)
        dt = DataHelpers.ExecuteNonQuery(command)

        Return (dt > 0)
    End Function

    Public Function GetEUS_VoucherActivatedByVoucher(ctx As CMSDBDataContext, voucher As String) As EUS_VoucherActivated
        Dim prof As EUS_VoucherActivated = Nothing
        'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            prof = (From itm In ctx.EUS_VoucherActivateds
                        Where (itm.Voucher.ToUpper() = voucher.ToUpper()) _
                        Select itm).FirstOrDefault()


        Catch ex As Exception
            Throw
        Finally
            '_CMSDBDataContext.Dispose()
        End Try

        Return prof
    End Function


    Public Sub InsertToEUS_VoucherActivated(ctx As CMSDBDataContext, voucher As String, profileid As Integer, CustomerTransactionID As Integer)
        Dim prof As EUS_VoucherActivated = Nothing
        'Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            prof = New EUS_VoucherActivated()
            prof.DateActivated = Date.UtcNow
            prof.ProfileID = profileid
            prof.Voucher = voucher
            prof.CustomerTransactionID = CustomerTransactionID

            ctx.EUS_VoucherActivateds.InsertOnSubmit(prof)
            ctx.SubmitChanges()

        Catch ex As Exception
            Throw
        Finally
            '_CMSDBDataContext.Dispose()
        End Try
    End Sub


    Public Function EUS_CustomerVerificationDocs_GetByCustomerID(CustomerID As Integer) As DSCustomer
        Dim ta As New DSCustomerTableAdapters.EUS_CustomerVerificationDocsTableAdapter
        Dim ds As New DSCustomer()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_CustomerID(ds.EUS_CustomerVerificationDocs, CustomerID)

        Return ds
    End Function



    Public Function GetAffiliateBy_SiteName(SiteName As String) As String
        Dim result As String = ""
        Try
            Dim sql As String = <sql><![CDATA[
select dbo.EUS_Profiles.aff_code
from AFF_Sites 
inner join dbo.EUS_Profiles on AFF_Sites.CustomerID=EUS_Profiles.ProfileID 
where [Site] like @SiteName or [Site] like 'www.'+@SiteName
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@SiteName", SiteName)
            Dim dt As DataTable = DataHelpers.GetDataTable(cmd)
            If (dt.Rows.Count > 0) Then
                result = dt.Rows(0)("aff_code").ToString()
            End If

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Function EUS_CustomerPhotos_GetVisiblePhotos(CustomerID As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
select COUNT(*) as VisiblePhotos
from dbo.EUS_CustomerPhotos
where CustomerID=@CustomerID
and isnull(IsDeleted,0)=0
and isnull(HasDeclined,0)=0 
and isnull(HasAproved,0)=1
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
            result = DataHelpers.ExecuteScalar(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Function REF_CustomerVerificationDocs_GetByCustomerID(CustomerID As Integer) As DSReferrers
        Dim ta As New DSReferrersTableAdapters.REF_CustomerVerificationDocsTableAdapter
        Dim ds As New DSReferrers()
        ta.Connection = New SqlConnection(DataHelpers.ConnectionString)
        ta.FillBy_CustomerID(ds.REF_CustomerVerificationDocs, CustomerID)

        Return ds
    End Function

    Public Function EUS_Profiles_GetREF_RegStep(CustomerID As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
select isnull((
    select top (1) REF_RegStep
    from EUS_Profiles
    where profileid=@CustomerID
),0) as REF_RegStep
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
            result = DataHelpers.ExecuteScalar(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Function EUS_Profiles_UpdateREF_RegStep(CustomerID As Integer, REF_RegStep As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
update dbo.EUS_Profiles set REF_RegStep=@REF_RegStep where referrerParentId>0 and profileid=@CustomerID and REF_RegStep<>@REF_RegStep
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@CustomerID", CustomerID)
            cmd.Parameters.AddWithValue("@REF_RegStep", REF_RegStep)
            result = DataHelpers.ExecuteNonQuery(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function


    Public Function EUS_Profiles_GetMasterProfileID(CustomerID As Integer) As Integer
        Dim result As Integer = 0
        Try
            Dim sql As String = <sql><![CDATA[
declare @FoundProfileID int =0
select @FoundProfileID = 
		ISNULL((select profileID from dbo.EUS_Profiles 
				where (profileID=@profileID or mirrorprofileID=@profileID)
				and ismaster=1),0)
				
if(@FoundProfileID = 0)
	select @FoundProfileID=profileID from dbo.EUS_Profiles 
	where (profileID=@profileID or mirrorprofileID=@profileID)

select @FoundProfileID as ProfileID
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@profileID", CustomerID)
            result = DataHelpers.ExecuteScalar(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function



    Public Function SYS_SMTPServers_GetByName(name As String) As DataSet
        Dim result As New DataSet()
        Try
            Dim sql As String = <sql><![CDATA[
SELECT [SMTPServerCredentialsID]
      ,[Name]
      ,[SMTPServerName]
      ,[SMTPLoginName]
      ,[SMTPPassword]
      ,[SMTPOutPort]
      ,[SMTPFromEmail]
  FROM [SYS_SMTPServers]
WHERE [Name]=@Name
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            cmd.Parameters.AddWithValue("@Name", name)
            result = DataHelpers.GetDataSet(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Function SYS_SMTPServers() As DataSet
        Dim result As New DataSet()
        Try
            Dim sql As String = <sql><![CDATA[
SELECT [SMTPServerCredentialsID]
      ,[Name]
      ,[SMTPServerName]
      ,[SMTPLoginName]
      ,[SMTPPassword]
      ,[SMTPOutPort]
      ,[SMTPFromEmail]
  FROM [SYS_SMTPServers]
]]></sql>
            Dim cmd As SqlCommand = DataHelpers.GetSqlCommand(sql)
            result = DataHelpers.GetDataSet(cmd)

        Catch ex As Exception
            Throw
        Finally

        End Try
        Return result
    End Function

    Public Function SYS_SMTPServers_GetByID(SMTPServerCredentialsID As Integer) As SYS_SMTPServer
        Dim result As SYS_SMTPServer = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            result = (From itm In _CMSDBDataContext.SYS_SMTPServers
                    Where itm.SMTPServerCredentialsID = SMTPServerCredentialsID
                    Select itm).SingleOrDefault()


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return result
    End Function

    Public Function SYS_SMTPServers_GetByNameLINQ(name As String) As SYS_SMTPServer
        Dim result As SYS_SMTPServer = Nothing
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            result = (From itm In _CMSDBDataContext.SYS_SMTPServers
                    Where itm.Name = name
                    Select itm).SingleOrDefault()


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return result
    End Function

    Public Function REF_ReferrerProfiles_CheckIfApproved(customerID As Integer) As Boolean
        Dim result As Integer = 0
        Dim _CMSDBDataContext As New CMSDBDataContext(DataHelpers.ConnectionString)
        Try

            result = (From itm In _CMSDBDataContext.REF_ReferrerProfiles
                    Where itm.CustomerID = customerID AndAlso itm.Status = ProfileStatusEnum.Approved
                    Select itm).Count()


        Catch ex As Exception
            Throw
        Finally
            _CMSDBDataContext.Dispose()
        End Try

        Return result > 0
    End Function

End Module
